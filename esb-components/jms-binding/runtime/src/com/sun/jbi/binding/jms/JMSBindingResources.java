/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSBindingResources.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 * JMSBindingResources.java SUN PROPRIETARY/CONFIDENTIAL. This software is the
 * proprietary information of Sun Microsystems, Inc. Use is subject to license
 * terms.
 */
package com.sun.jbi.binding.jms;

/**
 * Resource strings.
 *
 * @author Sun Microsystems Inc.
  */
public interface JMSBindingResources
{
    /**
     *    
     */
    String JMS_ACTIVATE_ENDPOINTS_DONE = "JMS_ACTIVATE_ENDPOINTS_DONE";

    /**
     *    
     */
    String JMS_LOAD_DD_FAILED = "JMS_LOAD_DD_FAILED";

    /**
     *    
     */
    String JMS_INVALID_DD = "JMS_INVALID_DD";

    /**
     *    
     */
    String JMS_INCONSISTENT_PROVIDER_DATA = "JMS_INCONSISTENT_PROVIDER_DATA";

    /**
     *    
     */
    String JMS_INCONSISTENT_CONSUMER_DATA = "JMS_INCONSISTENT_CONSUMER_DATA";

    /**
     *    
     */
    String JMS_INCONSISTENT_DATA = "JMS_INCONSISTENT_DATA";

    /**
     *    
     */
    String JMS_MQ_CANNOT_CLOSE_FACTORY = "JMS_MQ_CANNOT_CLOSE_FACTORY";

    /**
     *    
     */
    String JMS_MQ_CANNOT_START_CONNECTION = "JMS_MQ_CANNOT_START_CONNECTION";

    /**
     *    
     */
    String JMS_NO_ERROR_RETURNED = "JMS_NO_ERROR_RETURNED";

    /**
     *    
     */
    String JMS_TEXT_ADAPTOR_INVOKED = "JMS_TEXT_ADAPTOR_INVOKED";

    /**
     *    
     */
    String JMS_ERROR_JMS_NMR = "JMS_ERROR_JMS_NMR";

    /**
     *    
     */
    String JMS_SESSION_NOT_INITIALIZED = "JMS_SESSION_NOT_INITIALIZED";

    /**
     *    
     */
    String JMS_MQ_CANNOT_CREATE_CONSUMER = "JMS_MQ_CANNOT_CREATE_CONSUMER";

    /**
     *    
     */
    String JMS_CANNOT_CLOSE_SESSION = "JMS_CANNOT_CLOSE_SESSION";

    /**
     *    
     */
    String JMS_MQ_CANNOT_GET_SESSION = "JMS_MQ_CANNOT_GET_SESSION";

    /**
     *    
     */
    String JMS_MESSAGE_FAILURE = "JMS_MESSAGE_FAILURE";

    /**
     *    
     */
    String JMS_JMS_NMR_CONVERSION_FAILED = "JMS_JMS_NMR_CONVERSION_FAILED";

    /**
     *    
     */
    String JMS_CANNOT_GET_SESSION = "JMS_CANNOT_GET_SESSION";

    /**
     *    
     */
    String JMS_CANNOT_GET_TEMP_DEST = "JMS_CANNOT_GET_TEMP_DEST";

    /**
     *    
     */
    String JMS_SEND_STATUS_NMR = "JMS_SEND_STATUS_NMR";

    /**
     *    
     */
    String JMS_INVALID_CONIFG_INFO = "JMS_INVALID_CONIFG_INFO";

    /**
     *    
     */
    String JMS_NO_CORRELATION_ID = "JMS_NO_CORRELATION_ID";

    /**
     *    
     */
    String JMS_DEREGISTER_CON_FAILED = "JMS_DEREGISTER_CON_FAILED";

    /**
     *    
     */
    String JMS_DEREGISTER_DEST_FAILED = "JMS_DEREGISTER_DEST_FAILED";

    /**
     *    
     */
    String JMS_ACTIVATE_INBOUND_FAILED = "JMS_ACTIVATE_INBOUND_FAILED";

    /**
     *    
     */
    String JMS_ACTIVATE_INBOUND_SUCCESS = "JMS_ACTIVATE_INBOUND_SUCCESS";

    /**
     *    
     */
    String JMS_ACTIVATE_OUTBOUND_FAILED = "JMS_ACTIVATE_OUTBOUND_FAILED";

    /**
     *    
     */
    String JMS_ACTIVATE_OUTBOUND_SUCCESS = "JMS_ACTIVATE_OUTBOUND_SUCCESS";

    /**
     *    
     */
    String JMS_ACTIVATING_ENDPOINTS = "JMS_ACTIVATING_ENDPOINTS";

    /**
     *    
     */
    String JMS_ADMIN_MBEAN_FAILED = "JMS_ADMIN_MBEAN_FAILED";

    /**
     *    
     */
    String JMS_ARTIFACT_NOT_FOUND = "JMS_ARTIFACT_NOT_FOUND";

    /**
     *    
     */
    String JMS_BINDINGCHANNEL_FAILED = "JMS_BINDINGCHANNEL_FAILED";

    /**
     *    
     */
    String JMS_CANNOT_ACTIVATE = "JMS_CANNOT_ACTIVATE";

    /**
     *    
     */
    String JMS_CANNOT_ACTIVATE_ENDPOINT = "JMS_CANNOT_ACTIVATE_ENDPOINT";

    /**
     *    
     */
    String JMS_CANNOT_CREATE_FOLDER = "JMS_CANNOT_CREATE_FOLDER";

    /**
     *    
     */
    String JMS_CANNOT_DEACTIVATE_ENDPOINT = "JMS_CANNOT_DEACTIVATE_ENDPOINT";

    /**
     *    
     */
    String JMS_CANNOT_GET_NAMESPACE = "JMS_CANNOT_GET_NAMESPACE";

    /**
     *    
     */
    String JMS_CANNOT_GET_SERVICE = "JMS_CANNOT_GET_SERVICE";

    /**
     *    
     */
    String JMS_CANNOT_PARSE = "JMS_CANNOT_PARSE";

    /**
     *    
     */
    String JMS_CANNOT_PROCESS1 = "JMS_CANNOT_PROCESS1";

    /**
     *    
     */
    String JMS_CANNOT_PROCESSRESPONSE = "JMS_CANNOT_PROCESSRESPONSE";

    /**
     *    
     */
    String JMS_CANNOT_SEND = "JMS_CANNOT_SEND";

    /**
     *    
     */
    String JMS_CANNOT_SEND_STATUS = "JMS_CANNOT_SEND_STATUS";

    /**
     *    
     */
    String JMS_CANNOT_SET_JMS_STATUS = "JMS_CANNOT_SET_JMS_STATUS";

    /**
     *    
     */
    String JMS_CANNOT_START_EP = "JMS_CANNOT_START_EP";

    /**
     *    
     */
    String JMS_CLEAN_FAILED = "JMS_CLEAN_FAILED";

    /**
     *    
     */
    String JMS_CLOSE_FAILED = "JMS_CLOSE_FAILED";

    /**
     *    
     */
    String JMS_CONFIGDATA_ERROR = "JMS_CONFIGDATA_ERROR";

    /**
     *    
     */
    String JMS_CONFIGFILE_PARSE_ERROR = "JMS_CONFIGFILE_PARSE_ERROR";

    /**
     *    
     */
    String JMS_CONFIGFILE_UNAVAILABLE = "JMS_CONFIGFILE_UNAVAILABLE";

    /**
     *    
     */
    String JMS_CONFIGFILE_VALID = "JMS_CONFIGFILE_VALID";

    /**
     *    
     */
    String JMS_CONFIGFILE_WARNING = "JMS_CONFIGFILE_WARNING";

    /**
     *    
     */
    String JMS_CONFIG_FILE_CHECK = "JMS_CONFIG_FILE_CHECK";

    /**
     *    
     */
    String JMS_CONTENT_NULL = "JMS_CONTENT_NULL";

    /**
     *    
     */
    String JMS_CREATED_FOLDER = "JMS_CREATED_FOLDER";

    /**
     *    
     */
    String JMS_DEPLOY_FAILED = "JMS_DEPLOY_FAILED";

    /**
     *    
     */
    String JMS_DEREGISTER_DEPLOYERMBEAN_FAILED =
        "JMS_DEREGISTER_DEPLOYERMBEAN_FAILED";

    /**
     *    
     */
    String JMS_DEREGISTER_ENDPOINT = "JMS_DEREGISTER_ENDPOINT";

    /**
     *    
     */
    String JMS_DEREGISTER_ENDPOINT_FAILED = "JMS_DEREGISTER_ENDPOINT_FAILED";

    /**
     *    
     */
    String JMS_DESTINATION_NAME_NULL = "JMS_DESTINATION_NAME_NULL";

    /**
     *    
     */
    String JMS_DONE_MESSAGE = "JMS_DONE_MESSAGE";

    /**
     *    
     */
    String JMS_DUPLICATE_DEPLOYMENT = "JMS_DUPLICATE_DEPLOYMENT";

    /**
     *    
     */
    String JMS_DUPLICATE_ENDPOINT = "JMS_DUPLICATE_ENDPOINT";

    /**
     *    
     */
    String JMS_ENDPOINT_VALIDATION_ERROR = "JMS_ENDPOINT_VALIDATION_ERROR";

    /**
     *    
     */
    String JMS_ERROR_LOADING_DEPLOYMENTS = "JMS_ERROR_LOADING_DEPLOYMENTS";

    /**
     *    
     */
    String JMS_EXCHANGE_DONE = "JMS_EXCHANGE_DONE";

    /**
     *    
     */
    String JMS_EXCHANGE_ERROR = "JMS_EXCHANGE_ERROR";

    /**
     *    
     */
    String JMS_FAILED_WRITE = "JMS_FAILED_WRITE";

    /**
     *    
     */
    String JMS_FILE_WRITE_FAILED = "JMS_FILE_WRITE_FAILED";

    /**
     *    
     */
    String JMS_FINISHED_DODEPLOY = "JMS_FINISHED_DODEPLOY";

    /**
     *    
     */
    String JMS_FRAMEWORK_BUSYPOOL_SIZE = "JMS_FRAMEWORK_BUSYPOOL_SIZE";

    /**
     *    
     */
    String JMS_FRAMEWORK_FREEPOOL_SIZE = "JMS_FRAMEWORK_FREEPOOL_SIZE";

    /**
     *    
     */
    String JMS_FRAMEWORK_INVALID_SERVICENAME =
        "JMS_FRAMEWORK_INVALID_SERVICENAME";

    /**
     *    
     */
    String JMS_FRAMEWORK_STARTPOOL = "JMS_FRAMEWORK_STARTPOOL";

    /**
     *    
     */
    String JMS_FRAMEWORK_THREAD_PROCESS = "JMS_FRAMEWORK_THREAD_PROCESS";

    /**
     *    
     */
    String JMS_FRAMWEORK_RUNNING_THREAD = "JMS_FRAMWEORK_RUNNING_THREAD";

    /**
     *    
     */
    String JMS_FRAMWEORK_SHUTDOWN_THREAD = "JMS_FRAMWEORK_SHUTDOWN_THREAD";

    /**
     *    
     */
    String JMS_HANDLER_BASE = "JMS_HANDLER_BASE";

    /**
     *    
     */
    String JMS_HANDLER_INVALID_OPERATION = "JMS_HANDLER_INVALID_OPERATION";

    /**
     *    
     */
    String JMS_IGNORE_ENDPOINT = "JMS_IGNORE_ENDPOINT";

    /**
     *    
     */
    String JMS_INBOUND_HANDLER = "JMS_INBOUND_HANDLER";

    /**
     *    
     */
    String JMS_INCONSISTENT_MESSAGE_TYPE = "JMS_INCONSISTENT_MESSAGE_TYPE";

    /**
     *    
     */
    String JMS_INCONSISTENT_OPERATION = "JMS_INCONSISTENT_OPERATION";

    /**
     *    
     */
    String JMS_INIT_FAILED = "JMS_INIT_FAILED";

    /**
     *    
     */
    String JMS_INIT_START = "JMS_INIT_START";

    /**
     *    
     */
    String JMS_INONLY_DONE = "JMS_INONLY_DONE";

    /**
     *    
     */
    String JMS_INONLY_ERROR = "JMS_INONLY_ERROR";

    /**
     *    
     */
    String JMS_INOPTIONALOUT_NOT_SUPPORTED = "JMS_INOPTIONALOUT_NOT_SUPPORTED";

    /**
     *    
     */
    String JMS_INOPTOUT_SUPPORT = "JMS_INOPTOUT_SUPPORT";

    /**
     *    
     */
    String JMS_INOUT_DONE = "JMS_INOUT_DONE";

    /**
     *    
     */
    String JMS_INOUT_ERROR = "JMS_INOUT_ERROR";

    /**
     *    
     */
    String JMS_INPUTFOLDER_NOTEXIST = "JMS_INPUTFOLDER_NOTEXIST";

    /**
     *    
     */
    String JMS_INPUT_TYPE_NAME_NULL = "JMS_INPUT_TYPE_NAME_NULL";

    /**
     *    
     */
    String JMS_INVALID_BINDINGCONTEXT = "JMS_INVALID_BINDINGCONTEXT";

    /**
     *    
     */
    String JMS_INVALID_COMPROOT = "JMS_INVALID_COMPROOT";

    /**
     *    
     */
    String JMS_INVALID_CONFIG_FILE = "JMS_INVALID_CONFIG_FILE";

    /**
     *    
     */
    String JMS_INVALID_DEPLOYMENT_PROPERTIES =
        "JMS_INVALID_DEPLOYMENT_PROPERTIES";

    /**
     *    
     */
    String JMS_INVALID_ENDPOINT = "JMS_INVALID_ENDPOINT";

    /**
     *    
     */
    String JMS_INVALID_ENDPOINTTYPE = "JMS_INVALID_ENDPOINTTYPE";

    /**
     *    
     */
    String JMS_INVALID_ENDPOINT_TYPE = "JMS_INVALID_ENDPOINT_TYPE";

    /**
     *    
     */
    String JMS_INVALID_FACTORY_NAME = "JMS_INVALID_FACTORY_NAME";

    /**
     *    
     */
    String JMS_INVALID_INPUTFOLDER = "JMS_INVALID_INPUTFOLDER";

    /**
     *    
     */
    String JMS_INVALID_INSTALL_CONFIG_FILE = "JMS_INVALID_INSTALL_CONFIG_FILE";

    /**
     *    
     */
    String JMS_INVALID_MEP = "JMS_INVALID_MEP";

    /**
     *    
     */
    String JMS_INVALID_MESSAGE = "JMS_INVALID_MESSAGE";

    /**
     *    
     */
    String JMS_INVALID_MESSAGE_STATE = "JMS_INVALID_MESSAGE_STATE";

    /**
     *    
     */
    String JMS_INVALID_OPERATION = "JMS_INVALID_OPERATION";

    /**
     *    
     */
    String JMS_INVALID_OUTPUTFOLDER = "JMS_INVALID_OUTPUTFOLDER";

    /**
     *    
     */
    String JMS_INVALID_PATTERN = "JMS_INVALID_PATTERN";

    /**
     *    
     */
    String JMS_INVALID_PROCESSEDFOLDER = "JMS_INVALID_PROCESSEDFOLDER";

    /**
     *    
     */
    String JMS_INVALID_SERVICE = "JMS_INVALID_SERVICE";

    /**
     *    
     */
    String JMS_INVALID_STYLE = "JMS_INVALID_STYLE";

    /**
     *    
     */
    String JMS_INVALID_TIMEOUT = "JMS_INVALID_TIMEOUT";

    /**
     *    
     */
    String JMS_INVALID_WSDL_FILE = "JMS_INVALID_WSDL_FILE";

    /**
     *    
     */
    String JMS_INVALID_XML = "JMS_INVALID_XML";

    /**
     *    
     */
    String JMS_IRRECOVERABLE_ERROR = "JMS_IRRECOVERABLE_ERROR";

    /**
     *    
     */
    String JMS_JMS_TO_NMS_FAILED = "JMS_JMS_TO_NMS_FAILED";

    /**
     *    
     */
    String JMS_LOAD_CONFIG_FAILED = "JMS_LOAD_CONFIG_FAILED";

    /**
     *    
     */
    String JMS_MEP_NULL = "JMS_MEP_NULL";

    /**
     *    
     */
    String JMS_MESSAGE_CREATION_FAILED = "JMS_MESSAGE_CREATION_FAILED";

    /**
     *    
     */
    String JMS_MESSAGE_RECEIVED = "JMS_MESSAGE_RECEIVED";

    /**
     *    
     */
    String JMS_MISSING_CONNECTION_PARAMS = "JMS_MISSING_CONNECTION_PARAMS";

    /**
     *    
     */
    String JMS_MOVE_FAILED = "JMS_MOVE_FAILED";

    /**
     *    
     */
    String JMS_NMS_HEADERS_FAILED = "JMS_NMS_HEADERS_FAILED";

    /**
     *    
     */
    String JMS_NOT_VALID = "JMS_NOT_VALID";

    /**
     *    
     */
    String JMS_NO_DEPLOYMENTS = "JMS_NO_DEPLOYMENTS";

    /**
     *    
     */
    String JMS_NO_DESTINATION = "JMS_NO_DESTINATION";

    /**
     *    
     */
    String JMS_NO_ENDPOINT = "JMS_NO_ENDPOINT";

    /**
     *    
     */
    String JMS_NO_ENDPOINTS = "JMS_NO_ENDPOINTS";

    /**
     *    
     */
    String JMS_NO_FREE_THREAD = "JMS_NO_FREE_THREAD";

    /**
     *    
     */
    String JMS_NO_NAMESPACE = "JMS_NO_NAMESPACE";

    /**
     *    
     */
    String JMS_NO_OPERATIONS = "JMS_NO_OPERATIONS";

    /**
     *    
     */
    String JMS_NO_SERVICE = "JMS_NO_SERVICE";

    /**
     *    
     */
    String JMS_NO_WRITE_PERMISSION = "JMS_NO_WRITE_PERMISSION";

    /**
     *    
     */
    String JMS_NULL_EXCHANGE = "JMS_NULL_EXCHANGE";

    /**
     *    
     */
    String JMS_OPERATION_NAMESPACE_NULL = "JMS_OPERATION_NAMESPACE_NULL";

    /**
     *    
     */
    String JMS_OPERATION_NAME_NULL = "JMS_OPERATION_NAME_NULL";

    /**
     *    
     */
    String JMS_OUTIN_DONE = "JMS_OUTIN_DONE";

    /**
     *    
     */
    String JMS_OUTIN_ERROR = "JMS_OUTIN_ERROR";

    /**
     *    
     */
    String JMS_OUTOPTIN_SUPPORT = "JMS_OUTOPTIN_SUPPORT";

    /**
     *    
     */
    String JMS_OUTPUTFOLDER_NOTEXIST = "JMS_OUTPUTFOLDER_NOTEXIST";

    /**
     *    
     */
    String JMS_OUTPUT_TYPE_NAME_NULL = "JMS_OUTPUT_TYPE_NAME_NULL";

    /**
     *    
     */
    String JMS_PROCESSEDFOLDER_NOTEXIST = "JMS_PROCESSEDFOLDER_NOTEXIST";

    /**
     *    
     */
    String JMS_PROCESSED_FAILED = "JMS_PROCESSED_FAILED";

    /**
     *    
     */
    String JMS_PROCESSED_SUCCESS = "JMS_PROCESSED_SUCCESS";

    /**
     *    
     */
    String JMS_PROCESS_INONLY_FAILED = "JMS_PROCESS_INONLY_FAILED";

    /**
     *    
     */
    String JMS_PROCESS_INOUT_FAILED = "JMS_PROCESS_INOUT_FAILED";

    /**
     *    
     */
    String JMS_PROCESS_OUTONLY_FAILED = "JMS_PROCESS_OUTONLY_FAILED";

    /**
     *    
     */
    String JMS_PROCESS_ROBUSTINONLY_FAILED = "JMS_PROCESS_ROBUSTINONLY_FAILED";

    /**
     *    
     */
    String JMS_PROCESS_ROBUSTOUTONLY_FAILED =
        "JMS_PROCESS_ROBUSTOUTONLY_FAILED";

    /**
     *    
     */
    String JMS_REASON1 = "JMS_REASON1";

    /**
     *    
     */
    String JMS_RECEIVED_INBOUND = "JMS_RECEIVED_INBOUND";

    /**
     *    
     */
    String JMS_RECEIVED_INONLY = "JMS_RECEIVED_INONLY";

    /**
     *    
     */
    String JMS_RECEIVED_INOPTIONALOUT = "JMS_RECEIVED_INOPTIONALOUT";

    /**
     *    
     */
    String JMS_RECEIVED_INOUT = "JMS_RECEIVED_INOUT";

    /**
     *    
     */
    String JMS_RECEIVED_INVALID_MESSAGE = "JMS_RECEIVED_INVALID_MESSAGE";

    /**
     *    
     */
    String JMS_RECEIVED_OUTBOUND = "JMS_RECEIVED_OUTBOUND";

    /**
     *    
     */
    String JMS_RECEIVED_OUTIN = "JMS_RECEIVED_OUTIN";

    /**
     *    
     */
    String JMS_RECEIVED_OUTONLY = "JMS_RECEIVED_OUTONLY";

    /**
     *    
     */
    String JMS_RECEIVED_OUTOPTIONALIN = "JMS_RECEIVED_OUTOPTIONALIN";

    /**
     *    
     */
    String JMS_RECEIVED_ROBUSTINONLY = "JMS_RECEIVED_ROBUSTINONLY";

    /**
     *    
     */
    String JMS_RECEIVED_ROBUSTOUTONLY = "JMS_RECEIVED_ROBUSTOUTONLY";

    /**
     *    
     */
    String JMS_RECEIVED_UNSUPPORTED_MEP = "JMS_RECEIVED_UNSUPPORTED_MEP";

    /**
     *    
     */
    String JMS_RECEIVER_ERROR = "JMS_RECEIVER_ERROR";

    /**
     *    
     */
    String JMS_RECEIVER_START = "JMS_RECEIVER_START";

    /**
     *    
     */
    String JMS_RECEIVER_STOP = "JMS_RECEIVER_STOP";

    /**
     *    
     */
    String JMS_REGISTER_DEPLOYERMBEAN_FAILED =
        "JMS_REGISTER_DEPLOYERMBEAN_FAILED";

    /**
     *    
     */
    String JMS_REGISTER_ENDPOINT = "JMS_REGISTER_ENDPOINT";

    /**
     *    
     */
    String JMS_REG_FAILED = "JMS_REG_FAILED";

    /**
     *    
     */
    String JMS_REG_FILE_NAME = "JMS_REG_FILE_NAME";

    /**
     *    
     */
    String JMS_REG_FILE_NOT_FOUND = "JMS_REG_FILE_NOT_FOUND";

    /**
     *    
     */
    String JMS_REG_FILE_NOT_READABLE = "JMS_REG_FILE_NOT_READABLE";

    /**
     *    
     */
    String JMS_REG_FILE_NOT_WRITABLE = "JMS_REG_FILE_NOT_WRITABLE";

    /**
     *    
     */
    String JMS_ROBUSTINONLY_DONE = "JMS_ROBUSTINONLY_DONE";

    /**
     *    
     */
    String JMS_ROBUSTINONLY_ERROR = "JMS_ROBUSTINONLY_ERROR";

    /**
     *    
     */
    String JMS_ROBUSTINONLY_NOTSUPPORTED = "JMS_ROBUSTINONLY_NOTSUPPORTED";

    /**
     *    
     */
    String JMS_ROBUSTOUTONLY_DONE = "JMS_ROBUSTOUTONLY_DONE";

    /**
     *    
     */
    String JMS_ROBUSTOUTONLY_ERROR = "JMS_ROBUSTOUTONLY_ERROR";

    /**
     *    
     */
    String JMS_SENDING_MESSAGE = "JMS_SENDING_MESSAGE";

    /**
     *    
     */
    String JMS_SEND_FAILED = "JMS_SEND_FAILED";

    /**
     *    
     */
    String JMS_SHUTDOWN_BEGIN = "JMS_SHUTDOWN_BEGIN";

    /**
     *    
     */
    String JMS_SHUTDOWN_END = "JMS_SHUTDOWN_END";

    /**
     *    
     */
    String JMS_START_BEGIN = "JMS_START_BEGIN";

    /**
     *    
     */
    String JMS_START_DEPLOYMENT = "JMS_START_DEPLOYMENT";

    /**
     *    
     */
    String JMS_START_DEPLOYMENT_FAILED = "JMS_START_DEPLOYMENT_FAILED";

    /**
     *    
     */
    String JMS_START_DEPLOYMENT_FAILED_BEANNULL =
        "JMS_START_DEPLOYMENT_FAILED_BEANNULL";

    /**
     *    
     */
    String JMS_START_DEPLOYMENT_SUCCESS = "JMS_START_DEPLOYMENT_SUCCESS";

    /**
     *    
     */
    String JMS_START_END = "JMS_START_END";

    /**
     *    
     */
    String JMS_START_ENDPOINT_FAILED = "JMS_START_ENDPOINT_FAILED";

    /**
     *    
     */
    String JMS_STOP_BEGIN = "JMS_STOP_BEGIN";

    /**
     *    
     */
    String JMS_STOP_DEPLOYMENT = "JMS_STOP_DEPLOYMENT";

    /**
     *    
     */
    String JMS_STOP_DEPLOYMENT_SUCCESS = "JMS_STOP_DEPLOYMENT_SUCCESS";

    /**
     *    
     */
    String JMS_STOP_END = "JMS_STOP_END";

    /**
     *    
     */
    String JMS_STOP_ENDPOINTTHREAD = "JMS_STOP_ENDPOINTTHREAD";

    /**
     *    
     */
    String JMS_STOP_ENDPOINT_FAILED = "JMS_STOP_ENDPOINT_FAILED";

    /**
     *    
     */
    String JMS_STOP_ENDPOINT_SUCCESS = "JMS_STOP_ENDPOINT_SUCCESS";

    /**
     *    
     */
    String JMS_STOP_POLLING = "JMS_STOP_POLLING";

    /**
     *    
     */
    String JMS_STOP_WARNING = "JMS_STOP_WARNING";

    /**
     *    
     */
    String JMS_TASK_FAILED = "JMS_TASK_FAILED";

    /**
     *    
     */
    String JMS_TRY_LATER = "JMS_TRY_LATER";

    /**
     *    
     */
    String JMS_UNJAR_FAILED = "JMS_UNJAR_FAILED";

    /**
     *    
     */
    String JMS_UNSUPPORED_OPERATION = "JMS_UNSUPPORED_OPERATION";

    /**
     *    
     */
    String JMS_UNSUPPORTED_MESSAGE = "JMS_UNSUPPORTED_MESSAGE";

    /**
     *    
     */
    String JMS_WRITE_ERROR_FAILED = "JMS_WRITE_ERROR_FAILED";

    /**
     *    
     */
    String JMS_WRITE_ERROR_SUCCESS = "JMS_WRITE_ERROR_SUCCESS";

    /**
     *    
     */
    String JMS_WRONG_HANDLER = "JMS_WRONG_HANDLER";

    /**
     *    
     */
    String JMS_WSDLFILE_UNAVAILABLE = "JMS_WSDLFILE_UNAVAILABLE";

    /**
     *    
     */
    String JMS_WSDL_INVALID_DIRECTION = "JMS_WSDL_INVALID_DIRECTION";

    /**
     *    
     */
    String JMS_WSDL_INVALID_URI = "JMS_WSDL_INVALID_URI";

    /**
     *    
     */
    String JMS_XML_STRING_CREATION_FAILED = "JMS_XML_STRING_CREATION_FAILED";

    /**
     *    
     */
    String JMS_SERVICE_NAMESPACE_NULL = "JMS_SERVICE_NAMESPACE_NULL";

    /**
     *    
     */
    String JMS_SERVICE_NAME_NULL = "JMS_SERVICE_NAME_NULL";

    /**
     *    
     */
    String JMS_ENDPOINT_NAME_NULL = "JMS_ENDPOINT_NAME_NULL";

    /**
     *    
     */
    String JMS_CONNECTION_FACTORY_NULL = "JMS_CONNECTION_FACTORY_NULL";

    /**
     *    
     */
    String JMS_INTERFACE_NAMESPACE_NULL = "JMS_INTERFACE_NAMESPACE_NULL";

    /**
     *    
     */
    String JMS_INTERFACE_NAME_NULL = "JMS_INTERFACE_NAME_NULL";

    /**
     *    
     */
    String JMS_DUPLICATE_ENDPOINTS = "JMS_DUPLICATE_ENDPOINTS";

    /**
     *    
     */
    String JMS_ENDPOINT_NOT_STARTED = "JMS_ENDPOINT_NOT_STARTED";

    /**
     *    
     */
    String JMS_REQUIRED_ATTR_ERROR = "JMS_REQUIRED_ATTR_ERROR";

    /**
     *    
     */
    String JMS_INVALID_ROLE = "JMS_INVALID_ROLE";

    /**
     *    
     */
    String JMS_OUTPUT_MSG_ERROR = "JMS_OUTPUT_MSG_ERROR";

    /**
     *    
     */
    String JMS_INITIAL_CONTEXT_ERROR = "JMS_INITIAL_CONTEXT_ERROR";
    
    String JMS_DESCRIPTOR_NOT_FOUND = "JMS_DESCRIPTOR_NOT_FOUND";
    
    String JMS_OBJECTSTORE_NOT_FOUND = "JMS_OBJECTSTORE_NOT_FOUND";
    
    String JMS_INVALID_LOG_LEVEL = "JMS_INVALID_LOG_LEVEL";
    
    String JMS_MESSAGE_SELECTOR_FINE = "JMS_MESSAGE_SELECTOR_FINE";
    
    String JMS_CANNOT_CREATE_MESSAGE = "JMS_CANNOT_CREATE_MESSAGE";
    
    String JMS_CANNOT_CREATE_SESSION = "JMS_CANNOT_CREATE_SESSION";
    
    String JMS_DEFAULT_OBJECT_STORE = "JMS_DEFAULT_OBJECT_STORE";
}
