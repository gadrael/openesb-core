/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MQSessionPool.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.mq;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import java.util.logging.Logger;

/**
 * Pool of MQ sessions.
 *
 * @author Sun Microsystems Inc.
 */
public final class MQSessionPool implements JMSBindingResources
{
    /**
     * Table of active sessions.
     */
    private Hashtable mLocked;
    /**
     * Table of passive sessions.
     */
    private Hashtable mUnlocked;
    /**
     * Logger object.   
     */
    private Logger mLogger;
    /**
     * i18n.
     */
    private StringTranslator mTranslator;
    /**
     *  Maximum sessions per connection.
     */
    private int mMaxSessionsPerConnection = 5;
    /**
     *    Minimum sessions per connection.
     */
    private int mMinSessionsPerConnection = 2;

    /**
     * Creates a new MQSessionPool object.
     */
    public MQSessionPool()
    {
        mLocked = new Hashtable();
        mUnlocked = new Hashtable();
        mLogger = JMSBindingContext.getInstance().getLogger();
        mTranslator = JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Gets the active session.
     *
     * @param con Wrapper to MQ connection.
     *
     * @return number of active sessions for this connection.
     */
    public int getActiveSessions(MQConnection con)
    {
        int count = 0;

        try
        {
            Collection col = mLocked.values();
            Iterator iter = col.iterator();

            while (iter.hasNext())
            {
                String fac = (String) iter.next();

                if (fac.trim().equals(con.getFactory()))
                {
                    count++;
                }
            }
        }
        catch (Exception e)
        {
            count = -1;
        }

        return count;
    }

    /**
     * Gets the number of inactive sessions.
     *
     * @param con MQ connectin.
     *
     * @return number of inactive sessions.
     */
    public int getInActiveSessions(MQConnection con)
    {
        int count = 0;

        try
        {
            Collection col = mUnlocked.values();
            Iterator iter = col.iterator();

            while (iter.hasNext())
            {
                String fac = (String) iter.next();

                if (fac.trim().equals(con.getFactory()))
                {
                    count++;
                }
            }
        }
        catch (Exception e)
        {
            count = -1;
        }

        return count;
    }

    /**
     * Sets the max sessions per connection.
     *
     * @param max max sessions.
     */
    public void setMaxSessionsPerConnection(int max)
    {
        mMaxSessionsPerConnection = max;
    }

    /**
     * Sets the minmum sessions per connection.
     *
     * @param min min sessions.
     */
    public void setMinSessionsPerConnection(int min)
    {
        mMinSessionsPerConnection = min;
    }

    /**
     * Gets a session for an endpoint. 
     *
     * @param eb bean.
     *
     * @return Wrapper to JMS session.
     */
    public synchronized MQSession getSession(EndpointBean eb)
    {
        MQSession session;

        if (mUnlocked.size() > 0)
        {
            Enumeration e = mUnlocked.keys();

            while (e.hasMoreElements())
            {
                session = (MQSession) e.nextElement();

                if (validate(session))
                {
                    mUnlocked.remove(session);
                    mLocked.put(session, eb.getConnection().getFactory());

                    return session;
                }
                else
                {
                    mUnlocked.remove(session);
                    session = null;
                }
            }
        }

        // no new objects available make some
        if (exhausted(eb.getConnection()))
        {
            mLogger.severe("**CONNECTIONS EXHAUSTED**");

            return null;
        }

        session = create(eb);

        if (session == null)
        {
            mLogger.severe(mTranslator.getString(JMS_MQ_CANNOT_GET_SESSION, 
                    eb.getConnection().getFactory()));
        }

        return session;
    }

    /**
     * Closes all sessions and cleans up.
     *
     * @param con connection for which sessions hahve tobe cleaned.
     */
    public void cleanupSessions(MQConnection con)
    {
        try
        {
            Enumeration col = mUnlocked.keys();

            while (col.hasMoreElements())
            {
                MQSession fac = (MQSession) col.nextElement();

                if (fac.getConnection().getFactory().trim().equals(con
                            .getFactory()))
                {
                    fac.close();
                    mUnlocked.remove(fac);
                }
            }
        }
        catch (Exception e)
        {
            mLogger.severe(e.getMessage());
        }
    }

    /**
     * Releases session.
     *
     * @param session wrapper to JMS session.
     */
    public synchronized void releaseSession(MQSession session)
    {
        if (session != null)
        {
            mLocked.remove(session);
            mUnlocked.put(session, session.getConnection().getFactory());
        }
    }

    /**
     * Creates a new JMS session.
     *
     * @param eb bean.
     *
     * @return wrapper to JMS session.
     */
    private MQSession create(EndpointBean eb)
    {
        MQSession session = new MQSession();

        return session;
    }

    /**
     * Checks if sessions for connction are exhausted.
     *
     * @param con connection.
     *
     * @return true if exhausted.
     */
    private boolean exhausted(MQConnection con)
    {
        int count = getActiveSessions(con);

        if (count >= mMaxSessionsPerConnection)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Validates a session.
     *
     * @param session mq session.
     *
     * @return true if valid.
     */
    private boolean validate(MQSession session)
    {
        return true;
    }
}
