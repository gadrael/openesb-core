/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageAdaptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

import javax.jbi.messaging.MessageExchange;


import javax.jms.Message;

/**
 * Interface for  a message adaptor. 
 *
 * @author Sun Microsystems Inc.
  */
interface MessageAdaptor
{
    /**
     * Set error.
     *
     * @param err error.
     */
    void setError(String err);

    /**
     * Get error.
     *
     * @return error.
     */
    String getError();

    /**
     * Set exception.
     *
     * @param ex exception.
     */
    void setException(Exception ex);

    /**
     * Get exception.
     *
     * @return exception.
     */
    Exception getException();

    /**
     * Set reply to.
     *
     * @param dest destination name.
     */
    void setJMSReplyTo(javax.jms.Destination dest);

    /**
     * Get reply to.
     *
     * @return reply to.
     */
    javax.jms.Destination getJMSReplyTo();

    /**
     * Get name.
     *
     * @return name.
     */
    String getName();

    /**
     * Sets the status.
     *
     * @param msg JMS message.
     * @param status status.
     */
    void setStatus(
        Message msg,
        String status);

    /**
     * Validity.
     *
     * @return  true if valid.
     */
    boolean isValid();
    
    /**
     * Sets the epilogue message processor, Could be a WSDL 11 wrap/unwrap.
     */
    void setEpilogueProcessor(Object epiloguprocessor);
    
    /**
     * Gets the epilogue processor
     */
    Object getEpilogueProcessor();

    /**
     * Clears all errors.
     */
    void clear();
    
    /**
     *  Converts JMS to NMR message.
     *
     * @param msg JMS message.
     * @param exch exchange.
     */
    void convertJMStoNMSMessage(
        Message msg,
        MessageExchange exch);

    /**
     * Converts NMR to JMS message.
     * 
     * @param exch NMR message.
     * @param msg JMS message.
     */
    void convertNMStoJMSMessage(
        MessageExchange exch,
        Message msg);

    /**
     * Updates error message.
     *
     * @param msg JMS message.
     * @param err error.
     */
    void updateJMSErrorMessage(
        Message msg,
        Object err);

    /**
     * Updates message.
     *
     * @param msg JMS message.
     * @param exch NMR message.
     */
    void updateNMSOutMessage(
        Message msg,
        MessageExchange exch);
}
