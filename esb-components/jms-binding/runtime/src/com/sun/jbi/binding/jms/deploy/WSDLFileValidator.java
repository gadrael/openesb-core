/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDLFileValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;
import com.sun.jbi.binding.jms.JMSConstants;
import com.sun.jbi.binding.jms.config.ConfigConstants;
import com.sun.jbi.binding.jms.util.UtilBase;

import com.sun.jbi.wsdl2.Binding;
import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Endpoint;
import com.sun.jbi.wsdl2.WsdlFactory;

import com.sun.jbi.wsdl2.Interface;
import com.sun.jbi.wsdl2.InterfaceOperation;
import com.sun.jbi.wsdl2.BindingOperation;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.logging.Logger;

import javax.xml.namespace.QName;


/**
 * This class is uses to validate the configuration file supplied during
 * deployment conforms to the schema.
 *
 * @author Sun Microsystems Inc.
 */
public final class WSDLFileValidator
    extends com.sun.jbi.binding.jms.util.UtilBase
    implements JMSBindingResources
{
    /**
     * Description object.
     */
    private static Description sDefinition;

    /**
     * Logger object.
     */
    private Logger mLogger;
    /**
     * Current value.
     */
    private String mCurValue = "";

    /**
     * Name of the file to parse.
     */
    private String mFileName;

    /**
     * Translator for i18n messages.
     */
    private StringTranslator mStringTranslator;

    /**
     * Creates a new WSDLFileValidator object.
     *
     * @param wsdlfile schema file name.
     */
    public WSDLFileValidator(String wsdlfile)
    {
        mFileName = wsdlfile;
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Returns the document object obtained as a result of parsing.
     *
     * @return document object.
     */
    public Description getDocument()
    {
        return sDefinition;
    }

    /**
     * This method has to be invoked to check the validity of the input
     * document.
     */
    public void validate()
    {
        parse();

        if (isValid())
        {
            validateEndpoints();
        }

        mLogger.info(mStringTranslator.getString(JMS_CONFIG_FILE_CHECK)
            + isValid());
    }

    /**
     * Checks if the binding is a JMS binding.
     *
     * @param df document fragment.
     *
     * @return true if JMS binding.
     */
    private boolean isJMSBinding(DocumentFragment df)
    {
        NamedNodeMap n = df.getFirstChild().getAttributes();

        for (int g = 0; g < n.getLength(); g++)
        {
            if (n.item(g).getLocalName().equals("type"))
            {
                if (n.item(g).getNodeValue().equals(ConfigConstants.JMS_NAMESPACE))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Gets the role.
     *
     * @param map map object.
     *
     * @return  role.
     */
    private int getRole(NamedNodeMap map)
    {
        Node attrnode =
            map.getNamedItemNS(ConfigConstants.JMS_NAMESPACE,
                ConfigConstants.ENDPOINT_TYPE);

        if ((attrnode == null))
        {
            setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR,
                    ConfigConstants.ENDPOINT_TYPE));

            return -1;
        }

        try
        {
            mCurValue = attrnode.getNodeValue().trim();

            if (mCurValue.equals(""))
            {
                setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR,
                    ConfigConstants.ENDPOINT_TYPE));

                return -1;
            }
        }
        catch (Exception e)
        {
            setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR,
                    ConfigConstants.ENDPOINT_TYPE));
            setException(e);
        }

        if (mCurValue.trim().equalsIgnoreCase(ConfigConstants.CONSUMER_STRING))
        {
            return ConfigConstants.CONSUMER;
        }
        else if (mCurValue.trim().equalsIgnoreCase(ConfigConstants.PROVIDER_STRING))
        {
            return ConfigConstants.PROVIDER;
        }
        else
        {
            return -1;
        }
    }

    /**
     * Checks the endpoint for valid information.
     *
     * @param ep endpoint.
     */
    private void checkEndpoint(Endpoint ep)
    {
        DocumentFragment docfrag = ep.toXmlDocumentFragment();

        Node epnode = docfrag.getFirstChild();
        NamedNodeMap epattributes = epnode.getAttributes();
        checkRequired(epattributes, ConfigConstants.DESTINATION_NAME);
        checkRequired(epattributes, ConfigConstants.DESTINATION_STYLE);
        checkRequired(epattributes, ConfigConstants.CONNECTION_FACTORY);

        // checkRequired(epattributes, ConfigConstants.ENDPOINT_TYPE);
        int role = getRole(epattributes);

        if (role == -1)
        {
            setError(mStringTranslator.getString(JMS_INVALID_ROLE));
        }
        else if (role == ConfigConstants.PROVIDER)
        {
            checkRequired(epattributes, ConfigConstants.TIME_TO_LIVE);
        }
        else if (role == ConfigConstants.CONSUMER)
        {
            mLogger.fine("Role is consumer");
        }

        if (!isValid())
        {
            setError("\n\tEndpoint : " + ep.getName());
        }
    }

    /**
     * Checks the madatory information.
     *
     * @param map map object.
     * @param attr attribute to be checked.
     */
    private void checkRequired(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItemNS(ConfigConstants.JMS_NAMESPACE, attr);

        if ((attrnode == null))
        {
            
            setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR, attr));       

            return;
        }

        try
        {
            mCurValue = attrnode.getNodeValue().trim();

            if (mCurValue.equals(""))
            {
                setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR, attr));
            }
        }
        catch (Exception e)
        {
            setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR, attr));
            setException(e);
        }
    }

    /**
     * Finds if endpoint is provider or consumer.
     *
     * @param ep endpoint.
     *
     * @return provider or consumer.
     */
    private int findDirection(Endpoint ep)
    {
        Binding b = ep.getBinding();
        Interface bindingif = b.getInterface();

        for (int oper = 0; oper < b.getOperationsLength(); oper++)
        {
            BindingOperation boper = b.getOperation(oper);
            QName bindingopername = boper.getInterfaceOperation();
            int ifoperlength = bindingif.getOperationsLength();

            for (int j = 0; j < ifoperlength; j++)
            {
                InterfaceOperation op = bindingif.getOperation(j);
                String pat = op.getPattern();

                if (pat.trim().equals(ConfigConstants.IN_OUT)
                        || pat.trim().equals(ConfigConstants.IN_ONLY)
                        || pat.trim().equals(ConfigConstants.ROBUST_IN_ONLY)
                        || pat.trim().equals(ConfigConstants.IN_OPTIONAL_OUT))
                {
                    return 1;
                }
                else if (pat.trim().equals(ConfigConstants.OUT_IN)
                        || pat.trim().equals(ConfigConstants.OUT_ONLY)
                        || pat.trim().equals(ConfigConstants.ROBUST_OUT_ONLY)
                        || pat.trim().equals(ConfigConstants.OUT_OPTIONAL_IN))
                {
                    return 0;
                }
            }
        }

        return -1;
    }

    /**
     * Parses the input XML file.
     */
    private void parse()
    {
        try
        {
	    com.sun.jbi.component.ComponentContext ctx =
                     (com.sun.jbi.component.ComponentContext)
                     JMSBindingContext.getInstance().getContext();
	    WsdlFactory wsdlFactory = ctx.getWsdlFactory ();
            com.sun.jbi.wsdl2.WsdlReader wsdlRdr = wsdlFactory.newWsdlReader();
            sDefinition = wsdlRdr.readDescription(mFileName);
        }
        catch (Exception e)
        {
            setError("WSDL file " + mFileName + " parse error, reason "
                + e.getMessage());
            setException(e);
            e.printStackTrace();
        }
    }

    /**
     * Validates the endpoints.
     */
    private void validateEndpoints()
    {
        int services = sDefinition.getServicesLength();
        boolean found = false;

        for (int i = 0; i < services; i++)
        {
            try
            {
                com.sun.jbi.wsdl2.Service ser = sDefinition.getService(i);
                int endpoints = ser.getEndpointsLength();

                for (int k = 0; k < endpoints; k++)
                {
                    Endpoint ep = ser.getEndpoint(k);
                    Binding b = ep.getBinding();

                    if ((b == null)
                            || (!isJMSBinding(b.toXmlDocumentFragment())))
                    {
                        continue;
                    }

                    found = true;
                    checkEndpoint(ep);

                    if (!isValid())
                    {
                        setError("\n\tBinding " + " : " + b.getName());

                        return;
                    }
                }
            }
            catch (Exception e)
            {
                setError("Error occurred during endpoint validation "
                    + e.getMessage());
                setException(e);
                e.printStackTrace();

                return;
            }
        }

        if (!found)
        {
            setError(mStringTranslator.getString(JMS_NO_ENDPOINTS));
        }
    }
}
