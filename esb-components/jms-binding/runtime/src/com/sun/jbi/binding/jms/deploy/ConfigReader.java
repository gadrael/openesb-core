/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;
import com.sun.jbi.binding.jms.config.ConfigConstants;
import java.util.logging.Logger;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Reads the configuration file endpoints.xml and loads the data into the
 * EndpointBean objects.
 *
 * @author Sun Microsystems Inc.
 */
public class ConfigReader
    extends com.sun.jbi.binding.jms.util.UtilBase
    implements JMSBindingResources
{
    /**
     * Document object of the XML config file.
     */
    private Document mDoc;

    /**
     * Logger Object.
     */
    private Logger mLogger;

    /**
     * Namespace.
     */
    private String mNameSpace = "";

    /**
     * i18n.
     */
    private StringTranslator mTranslator;

    /**
     * The list of endpoints as configured in the config file. This list
     * contains all the encpoints and their attibutes.
     */
    private EndpointBean [] mEndpointInfoList = null;

    /**
     * The total number of end points in the config file.
     */
    private int mTotalEndpoints = 0;

    /**
     * Creates a new ConfigReader object.
     */
    public ConfigReader()
    {
        mLogger = JMSBindingContext.getInstance().getLogger();
        mTranslator =
            JMSBindingContext.getInstance().getStringTranslator();

        setValid(true);
    }

    /**
     * Returns the Bean object corresponding to the endpoint.
     *
     * @param endpoint endpoint name.
     *
     * @return endpoint Bean object.
     */
    public EndpointBean getBean(String endpoint)
    {
        /*Search for the bean corresponding to the service and endpoint name
         */
        for (int j = 0; j < mEndpointInfoList.length; j++)
        {
            String tmp = mEndpointInfoList[j].getUniqueName();

            if (tmp.trim().equals(endpoint))
            {
                return mEndpointInfoList[j];
            }
        }

        return null;
    }

    /**
     * Gets the endpoint list corresponding to a config file.
     *
     * @return End point Bean list.
     */
    public EndpointBean [] getEndpoint()
    {
        return mEndpointInfoList;
    }

    /**
     * Returns the total number of endpoints in the config file.
     *
     * @return int number of endpoints.
     */
    public int getEndpointCount()
    {
        return mTotalEndpoints;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Document doc)
    {
        try
        {
            mDoc = doc;
            mDoc.getDocumentElement().normalize();
            loadServicesList();
        }
        catch (Exception genException)
        {
            mLogger.severe(mTranslator.getString(JMS_LOAD_CONFIG_FAILED));
            genException.printStackTrace();
            setException(genException);
            setError(mTranslator.getString(JMS_LOAD_CONFIG_FAILED) + "\n"
                + genException.getMessage());
        }
    }

    /**
     * Sets the connection parameters.
     *
     * @param node params node.
     * @param eb endpoint bean.
     *
     * @throws Exception exception.
     */
    private void setConnectionParams(
        Node node,
        EndpointBean eb)
        throws Exception
    {
        try
        {
            Element ele = (Element) node;
            NodeList list =
                ele.getElementsByTagName(ConfigConstants.CONNECTION_PARAMS);

            if (list.getLength() == 0)
            {
                setError(mTranslator.getString(JMS_MISSING_CONNECTION_PARAMS));

                return;
            }

            Node nd = (Node) list.item(0);
            String factory = null;
            String user = null;
            String pass = null;

            factory = getValue(nd, ConfigConstants.CONNECTION_FACTORY);
            user = getValue(nd, ConfigConstants.CONNECTION_USER_ID);
            pass = getValue(nd, ConfigConstants.CONNECTION_PASSWORD);

            if ((factory == null) || (factory.trim().equals("")))
            {
                setError(mTranslator.getString(JMS_INVALID_FACTORY_NAME));
            }

            if ((user == null) || (user.trim().equals("")))
            {
                user = "";
            }

            if ((pass == null) || (pass.trim().equals(""))
                    || (user.trim().equals("")))
            {
                pass = "";
            }

            eb.setValue(ConfigConstants.CONNECTION_FACTORY, factory);
            eb.setValue(ConfigConstants.CONNECTION_USER_ID, user);
            eb.setValue(ConfigConstants.CONNECTION_PASSWORD, pass);
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    /**
     * Utility method for setting the Bean object from the XML Nodes.
     *
     * @param node The node which needs to be.
     * @param sb The end point bean object which has to be updated.
     * @param tagName the tag name which needs to be read.
     */
    private void setEndpointBeanByTagName(
        Node node,
        EndpointBean sb,
        String tagName)
    {
        Element ele = (Element) node;
        NodeList namelist = ele.getElementsByTagName(tagName);

        if (namelist == null)
        {
            /* This means the tag is not present
             */
            return;
        }

        Element name = (Element) namelist.item(0);
        String sValue = null;

        try
        {
            sValue =
                ((Node) (name.getChildNodes().item(0))).getNodeValue().trim();
        }
        catch (NullPointerException ne)
        {
            sb.setValue(tagName, sValue);

            return;
        }

        sb.setValue(tagName, sValue);
    }

    /**
     * Sets the interface name for this endpoint.
     *
     * @param nd  Node.
     * @param eb  endpoint bean.
     */
    private void setInterface(
        Node nd,
        EndpointBean eb)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigConstants.INTERFACE);

        if (namelist == null)
        {
            /* This means the tag is not present
             */
            return;
        }

        Node node = (Node) namelist.item(0);

        try
        {
            eb.setValue(ConfigConstants.INTERFACE_NAMESPACE,
                getValue(node, ConfigConstants.NAMESPACE_URI));
            eb.setValue(ConfigConstants.INTERFACE_LOCALNAME,
                getValue(node, ConfigConstants.LOCAL_PART));
        }
        catch (Exception e)
        {
            mLogger.severe(mTranslator.getString(JMS_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Sets the namespace.
     */
    private void setNamespace()
    {
        mNameSpace =
            mDoc.getDocumentElement().getAttribute(ConfigConstants.TARGET_NAMESPACE);

        if (mNameSpace == null)
        {
            mLogger.warning(mTranslator.getString(JMS_CANNOT_GET_NAMESPACE));

            return;
        }
    }

    /**
     * Gets the operation name.
     *
     * @param nd  operation node.
     *
     * @return  String operation name.
     */
    private String getOperationName(Node nd)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigConstants.NAME);

        Node node = (Node) namelist.item(0);

        return getValue(node, ConfigConstants.LOCAL_PART);
    }

    /**
     * Returns the operation namespace.
     *
     * @param nd  Node namespace node.
     *
     * @return  operation namespace.
     */
    private String getOperationNamespace(Node nd)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigConstants.NAME);
        Node node = (Node) namelist.item(0);

        return getValue(node, ConfigConstants.NAMESPACE_URI);
    }

    /**
     * Sets the operations.
     *
     * @param node node.
     * @param eb endpoint bean.
     *
     * @throws Exception exception.
     */
    private void setOperations(
        Node node,
        EndpointBean eb)
        throws Exception
    {
        try
        {
            Element ele = (Element) node;
            NodeList list = ele.getElementsByTagName(ConfigConstants.OPERATION);

            if (list.getLength() == 0)
            {
                setError(mTranslator.getString(JMS_NO_OPERATIONS,
                        eb.getUniqueName()));

                return;
            }

            for (int i = 0; i < list.getLength(); i++)
            {
                Node nd = (Node) list.item(i);
                String namespace = null;
                String name = null;
                String mep = null;
                String input = null;
                String output = null;
                namespace = getOperationNamespace(nd);
                name = getOperationName(nd);
                mep = getValue(nd, ConfigConstants.MEP);
                input = getValue(nd, ConfigConstants.INPUT_MESSAGE_TYPE);
                output = getValue(nd, ConfigConstants.OUTPUT_MESSAGE_TYPE);

                if ((namespace == null) || (namespace.trim().equals("")))
                {
                    setError(mTranslator.getString(JMS_OPERATION_NAMESPACE_NULL));
                }

                if ((name == null) || (name.trim().equals("")))
                {
                    setError(mTranslator.getString(JMS_OPERATION_NAME_NULL));
                }

                if ((mep == null) || (mep.trim().equals("")))
                {
                    setError(mTranslator.getString(JMS_MEP_NULL));
                }

                if ((input == null) || (input.trim().equals("")))
                {
                    setError(mTranslator.getString(JMS_INPUT_TYPE_NAME_NULL));
                }

                if ((output == null) || (output.trim().equals("")))
                {
                    setError(mTranslator.getString(JMS_OUTPUT_TYPE_NAME_NULL));
                }

                if (!isValid())
                {
                    return;
                }

                if (operationAllowed(eb, mep))
                {
                    eb.addOperation(namespace, name, mep, input, output);
                }
                else
                {
                    setWarning(mTranslator.getString(
                            JMS_INCONSISTENT_OPERATION, name));
                }
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    /**
     * Sets the role.
     *
     * @param eb Endpoint bean.
     */
    private void setRole(EndpointBean eb)
    {
        String role = (String) eb.getValue(ConfigConstants.ENDPOINT_TYPE);

        if (role == null)
        {
            eb.setRole(ConfigConstants.CONSUMER);
        }
        else
        {
            if (role.trim().equalsIgnoreCase(ConfigConstants.PROVIDER_STRING))
            {
                eb.setRole(ConfigConstants.PROVIDER);
            }
            else
            {
                eb.setRole(ConfigConstants.CONSUMER);
            }
        }
    }

    /**
     * Sets the service.
     *
     * @param nd  service node.
     * @param eb  endpoint bean.
     */
    private void setService(
        Node nd,
        EndpointBean eb)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigConstants.SERVICE);

        if (namelist == null)
        {
            /* This means the tag is not present
             */
            return;
        }

        Node node = (Node) namelist.item(0);

        try
        {
            eb.setValue(ConfigConstants.SERVICE_NAMESPACE,
                getValue(node, ConfigConstants.NAMESPACE_URI));
            eb.setValue(ConfigConstants.SERVICENAME,
                getValue(node, ConfigConstants.LOCAL_PART));
        }
        catch (Exception e)
        {
            mLogger.severe(mTranslator.getString(JMS_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Sets the style.
     *
     * @param eb Endpoint bean.
     */
    private void setStyle(EndpointBean eb)
    {
        if (eb.getValue(ConfigConstants.DESTINATION_STYLE).trim()
                  .equalsIgnoreCase(ConfigConstants.QUEUE_STRING))
        {
            eb.setStyle(ConfigConstants.QUEUE);
        }
        else if (eb.getValue(ConfigConstants.DESTINATION_STYLE).trim()
                       .equalsIgnoreCase(ConfigConstants.TOPIC_STRING))
        {
            eb.setStyle(ConfigConstants.TOPIC);
        }
        else
        {
            setError(mTranslator.getString(JMS_INVALID_STYLE));
        }
    }

    /**
     * Gets the value for an element.
     *
     * @param n node node element.
     * @param name string.
     *
     * @return value of that node.
     */
    private String getValue(
        Node n,
        String name)
    {
        String s = null;

        try
        {
            Element ele = (Element) n;
            NodeList list = ele.getElementsByTagName(name);
            Element found = (Element) list.item(0);
            s = (String) found.getFirstChild().getNodeValue();
        }
        catch (Exception e)
        {
          ; //  e.printStackTrace();
        }

        return s;
    }

    /**
     * Parses the config files and loads them into bean objects.
     */
    private void loadServicesList()
    {
        NodeList list = mDoc.getElementsByTagName(ConfigConstants.ENDPOINT);
        mTotalEndpoints = list.getLength();
        mEndpointInfoList = new EndpointBean[mTotalEndpoints];

        try
        {
            for (int i = 0; i < mTotalEndpoints; i++)
            {
                Node node = list.item(i);
                EndpointBean sb = new EndpointBean();
                sb.setDeploymentType("XML");
                sb.setWsdlDefinition(mDoc);
                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    setEndpointBeanByTagName(node, sb,
                        ConfigConstants.ENDPOINTNAME);
                    setEndpointBeanByTagName(node, sb,
                        ConfigConstants.DESTINATION_NAME);
                    setEndpointBeanByTagName(node, sb,
                        ConfigConstants.DESTINATION_STYLE);
                    setEndpointBeanByTagName(node, sb,
                        ConfigConstants.DURABILITY);
                    setEndpointBeanByTagName(node, sb,
                        ConfigConstants.MESSAGE_SELECTOR);
                    setEndpointBeanByTagName(node, sb,
                        ConfigConstants.ENDPOINT_TYPE);
                    setEndpointBeanByTagName(node, sb, ConfigConstants.REPLY_TO);
                    setEndpointBeanByTagName(node, sb,
                        ConfigConstants.TIME_TO_LIVE);
                }

                setRole(sb);
                setService(node, sb);
                setInterface(node, sb);
                setConnectionParams(node, sb);
                setStyle(sb);
                setOperations(node, sb);
                checkRequired(sb, i);

                if (!isValid())
                {
                    setError(mTranslator.getString(JMS_LOAD_CONFIG_FAILED));

                    return;
                }

                mEndpointInfoList[i] = sb;
            }
        }
        catch (Exception ee)
        {
            mLogger.severe(mTranslator.getString(JMS_LOAD_CONFIG_FAILED));
            ee.printStackTrace();
            setException(ee);
            setError(mTranslator.getString(JMS_LOAD_CONFIG_FAILED) + "\n"
                + ee.getMessage());
        }
    }

    /**
     * Checks if the operation is allowed.
     *
     * @param eb  endpoint bean.
     * @param mep  mep.
     *
     * @return  true if operation is allowed.
     */
    private boolean operationAllowed(
        EndpointBean eb,
        String mep)
    {
        int style = eb.getStyle();
        boolean allowed = true;

        if (style == ConfigConstants.TOPIC)
        {
            if (mep.trim().equals(ConfigConstants.IN_OUT)
                    || mep.trim().equals(ConfigConstants.IN_OPTIONAL_OUT)
                    || mep.trim().equals(ConfigConstants.OUT_IN)
                    || mep.trim().equals(ConfigConstants.OUT_OPTIONAL_IN)
                    || mep.trim().equals(ConfigConstants.ROBUST_OUT_ONLY))
            {
                allowed = false;
            }
        }

        return allowed;
    }
    
    /**
     * Checks if all non null attributes are defined.
     *
     * @param eb endpoint bean.
     * @param counter index of endpoint.
     */
    
    private void checkRequired(
        EndpointBean eb,
        int counter)
    {
        clear();
        if (eb.getValue(ConfigConstants.SERVICE_NAMESPACE).trim().equals(""))
        {
            setError("Endpoint " + counter + " "
                + mTranslator.getString(JMS_SERVICE_NAMESPACE_NULL, ""));
        }

        if (eb.getValue(ConfigConstants.SERVICENAME).trim().equals(""))
        {
            setError("Endpoint " + counter + " "
                + mTranslator.getString(JMS_SERVICE_NAME_NULL, ""));
        }

        if (eb.getValue(ConfigConstants.ENDPOINTNAME).trim().equals(""))
        {
            setError("Endpoint " + counter + " "
                + mTranslator.getString(JMS_ENDPOINT_NAME_NULL,
                    eb.getValue(ConfigConstants.SERVICENAME)));
        }

        if (eb.getValue(ConfigConstants.CONNECTION_FACTORY).trim().equals(""))
        {
            setError("Endpoint " + counter + " "
                + mTranslator.getString(JMS_CONNECTION_FACTORY_NULL,
                    eb.getValue(ConfigConstants.ENDPOINTNAME)));
        }

        if (eb.getValue(ConfigConstants.INTERFACE_NAMESPACE).trim().equals(""))
        {
            setError("Endpoint " + counter + " "
                + mTranslator.getString(JMS_INTERFACE_NAMESPACE_NULL,
                    eb.getValue(ConfigConstants.ENDPOINTNAME)));
        }

        if (eb.getValue(ConfigConstants.INTERFACE_LOCALNAME).trim().equals(""))
        {
            setError("Endpoint " + counter + " "
                + mTranslator.getString(JMS_INTERFACE_NAME_NULL,
                    eb.getValue(ConfigConstants.ENDPOINTNAME)));
        }
        if (eb.getValue(ConfigConstants.DESTINATION_NAME).trim().equals(""))
        {
            setError("Endpoint " + counter + " "
                + mTranslator.getString(JMS_DESTINATION_NAME_NULL,
                    eb.getValue(ConfigConstants.ENDPOINTNAME)));
        }
    }
}
