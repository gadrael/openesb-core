/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MQManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.mq;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;

import com.sun.jbi.binding.jms.config.Config;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import com.sun.jbi.binding.jms.util.UtilBase;

import java.util.Hashtable;

import java.util.logging.Logger;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSSecurityException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;

import javax.naming.InitialContext;
import javax.naming.Context;

/**
 * 
 *
 * @author Sun Microsystems Inc.
 */
public final class MQManager
    extends UtilBase implements JMSBindingResources
{
    /**
     * Connection table.
     */
    private Hashtable mConnectionTable;
    /**
     * Destination table.
     */
    private Hashtable mDestinationTable;
    /**
     * Initial context.
     */
    private InitialContext mInitialContext;

    /**
     * Logger.
     */
    private Logger mLogger;

    /**
     * Temporary connection.
     */
    private MQConnection mTempCon;
    /**
     * Temporary session.
     */
    private MQSession mTmpSession;
    /**
     * Session pool.
     */
    private MQSessionPool mSessionPool;
    /**
     * Host name.
     */
    private String mHostName;
    /**
     *  Initial factory.
     */
    private String mInitialFactory;
    /**
     *  Protocol string.
     */
    private String mProtocol;
    /**
     *  Provider url.
     */
    private String mProviderUrl;
    /**
     * Temporary password.
     */
    private String mTempPassword;
    /**
     * Temporary user.
     */
    private String mTempUser;
    /**
     *  Temporary destination.
     */
    private String mTemporaryDestination;
    /**
     * Temporary factory.
     */
    private String mTemporaryFactory;
    
    /**
     * i18n.
     */
    private StringTranslator mStringTranslator;
    
    /**
     * Security principal.
     */
    private String mSecurityPrincipal;
    
    /**
     * Credentials.
     */
    private String mCredentials;
    
    /**
     * Security level.
     */
    private String mSecurityLevel;

    /**
     * Creates a new MQManager object.
     *
     * @param intitialfactory initial factory.
     * @param providerurl provider url string.
     */
    public MQManager(
        String intitialfactory,
        String providerurl,
        String principal,
        String credentials,
        String level)
    {
        mProviderUrl = providerurl;
        mInitialFactory = intitialfactory;
        mSecurityPrincipal = principal;
        mCredentials = credentials;
        mSecurityLevel = level;
        mConnectionTable = new Hashtable();
        mDestinationTable = new Hashtable();
        mSessionPool = new MQSessionPool();
        mSessionPool.setMaxSessionsPerConnection(5);
        mSessionPool.setMinSessionsPerConnection(2);
        mTemporaryDestination =
            JMSBindingContext.getInstance().getConfig().getReceiverDestination();
        mTemporaryFactory =
            JMSBindingContext.getInstance().getConfig().getReceiverFactory();
        mTempUser =
            JMSBindingContext.getInstance().getConfig()
                             .getReceiverConnectionUser();
        mTempPassword =
            JMSBindingContext.getInstance().getConfig()
                             .getReceiverConnectionPassword();

        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator = JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     *  Gets destination.
     *
     * @param lookupname lookup name.
     *
     * @return destination wrapper.
     */
    public MQDestination getDestination(Object lookupname)
    {
        MQDestination dest = null;
        clear();

            if (destinationExists(lookupname))
            {
                return (MQDestination) mDestinationTable.get(lookupname);
            }

            try
            {
                if (lookupname instanceof java.lang.String)
                {                
                    javax.jms.Destination jmsdest =
                    (javax.jms.Destination) mInitialContext.lookup(
                            (java.lang.String)lookupname);
                    dest = new  MQDestination(jmsdest);
                }
                else if (lookupname instanceof javax.jms.Destination)
                {                    
                    dest = new MQDestination(
                            (javax.jms.Destination)lookupname);
                }
                else 
                {
                    setError("INTERNAL ERROR : Wrong Destination Type ");
                }
            }
            catch (Exception e)
            {
                setError("Destination lookup failed ");
                setError(e.getMessage());
                e.printStackTrace();
            }
       

        return dest;
    }

    /**
     * Gets the destination name.
     *
     * @param style topic or q.
     * @param destlookupname destination lookup name.
     *
     * @return mq destination name.
     */
   /* public MQDestination getDestination(
        int style,
        String destlookupname)
    {
        if (style == ConfigConstants.QUEUE)
        {
            return getQueueDestination(destlookupname);
        }
        else if (style == ConfigConstants.TOPIC)
        {
            return getTopicDestination(destlookupname);
        }
        else
        {
            return null;
        }
    }
    */

    /**
     * Gets the provider url.
     *
     * @return provider url string.
     */
    public String getProviderURL()
    {
        return mProviderUrl;
    }

    /**
     * Gets the session.
     *
     * @param tran transacted or non-transacted.
     * @param eb endpoint bean.
     *
     * @return wrapper to a JMS session.
     */
    public MQSession getSession(
        boolean tran,
        EndpointBean eb)
    {
        MQSession session = mSessionPool.getSession(eb);

        if (session != null)
        {
            session.setBean(eb);

            if (tran)
            {
                session.setTransacted();
            }
        }

        return session;
    }

    /**
     * Get temporary queue.
     *
     * @return wrapper to MQ destination.
     */
    public MQDestination getTemporaryQueue()
    {
        MQDestination dest = getQueueDestination(mTemporaryDestination);

        return dest;
    }

    /**
     * Get temporary session.
     *
     * @return Wrapper to MQsession.
     */
    public MQSession getTemporarySession()
    {
        if (mTmpSession != null)
        {
            return mTmpSession;
        }
        mLogger.fine("Temporay connection factory " + mTemporaryFactory);
        mTempCon =
            createQueueConnection(mTemporaryFactory, mTempUser, mTempPassword);

        if (!isValid())
        {
            return null;
        }

        MQDestination dest = getQueueDestination(mTemporaryDestination);

        if (!isValid())
        {
            return null;
        }
        mLogger.fine("Temporary Session not available , Creating");
        createTmpSession(dest);

        return mTmpSession;
    }

    /**
     * Close the connection.
     *
     * @param con JMS connection wrapper.
     */
    public void closeConnection(MQConnection con)
    {
        if (con == null)
        {
            return;
        }

        con.decrementUsage();
        con.stop();
        con.close();
        deRegisterConnection(con);
    }

    /**
     * Closes the destination.
     *
     * @param dest wrapper to destination.
     */
    public void closeDestination(MQDestination dest)
    {
        if (dest != null)
        {
            deRegisterDestination(dest);
        }
    }

    /**
     * Creates  a connection.
     *
     * @param factory factory name.
     * @param user user id.
     * @param password password.
     * @param style topic or queue.
     *
     * @return wrapper connection.
     */
    public MQConnection createConnection(
        String factory,
        String user,
        String password,
        int style)
    {
        clear();

        if (style == ConfigConstants.QUEUE)
        {
            return createQueueConnection(factory, user, password);
        }
        else if (style == ConfigConstants.TOPIC)
        {
            return createTopicConnection(factory, user, password);
        }
        else
        {
            mLogger.severe("**UNSUPPORTED DESTINATION STYLE**");

            return null;
        }
    }

    /**
     * Creates queue connection.
     *
     * @param confactory connection.
     * @param user user id.
     * @param password password.
     *
     * @return wrapper to mq connection.
     */
    public MQConnection createQueueConnection(
        String confactory,
        String user,
        String password)
    {
        clear();

        MQConnection connection = null;
        connection = isAvailable(confactory);
        if (connection != null)
        {
            connection.incrementUsage();
            return connection;
        }

        try
        {

            javax.jms.QueueConnectionFactory mConnFactory =
                (javax.jms.QueueConnectionFactory) mInitialContext.lookup(confactory);

            QueueConnection conn = null;

            if (user.trim().equals(""))
            {
                conn = mConnFactory.createQueueConnection();
            }
            else
            {
                conn = mConnFactory.createQueueConnection(user, password);
            }
            mLogger.finest("Connection factory is " + conn.toString());
            connection =
                new MQConnection(confactory, conn, ConfigConstants.QUEUE);
        }
        catch (JMSSecurityException se)
        {
            mLogger.severe("**SECURITY EXCEPTION**");
            mLogger.severe(se.getMessage());
            setError("**SECURITY EXCEPTION**");
            setError(se.getMessage());

            return null;
        }
        catch (Exception e)
        {
            mLogger.info(e.getMessage());
            setError("**CONNECTION FAILED**");
            setError(e.getMessage());
            setException(e);
            e.printStackTrace();

            return null;
        }

        registerConnection(confactory, connection);

        return connection;
    }

    /**
     * Create topic connection.
     *
     * @param confactory connection factory.
     * @param user user id.
     * @param password password.
     *
     * @return Mq connection wrapper.
     */
    public MQConnection createTopicConnection(
        String confactory,
        String user,
        String password)
    {
        MQConnection connection = null;
        clear();
        connection = isAvailable(confactory);

        if (connection != null)
        {
            return connection;
        }

        try
        {
            javax.jms.TopicConnectionFactory mConnFactory =
                (javax.jms.TopicConnectionFactory) mInitialContext.lookup(confactory);

            if (user.trim().equals(""))
            {
                ;
            }

            TopicConnection conn =
                mConnFactory.createTopicConnection(user, password);

            connection =
                new MQConnection(confactory, conn, ConfigConstants.TOPIC);
        }
        catch (JMSSecurityException se)
        {
            se.printStackTrace();
            setError("Cannot create connection due to security error");
            setError(se.getMessage());

            return null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setError("Cannot create connection ");
            setError(e.getMessage());

            return null;
        }

        registerConnection(confactory, connection);

        return connection;
    }

    /**
     * Checks if destination exists.
     *
     * @param dest detsination name.
     *
     * @return true if destination exists.
     */
    public boolean destinationExists(Object dest)
    {
        try
        {
            if (dest instanceof java.lang.String)
            {
                return mDestinationTable.containsKey((java.lang.String)dest);
            }
            else if (dest instanceof javax.jms.Destination)
            {
                MQDestination tmpdest = new MQDestination(
                                (javax.jms.Destination)dest);
                String tmpdestname  = tmpdest.getDestinationName();
                tmpdest = null;
                return mDestinationTable.containsKey(
                                (java.lang.String)tmpdestname);                
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Initialises the manager.
     */
    public boolean init()
    {
        Hashtable env = new Hashtable();

        /* JNDI lookup code
           InitialContext ctx = JMSBindingContext.getInstance().getContext().getNamingContext();
         */
        try
        {
            clear();
            env.put(Context.INITIAL_CONTEXT_FACTORY, mInitialFactory);
            env.put(Context.PROVIDER_URL, mProviderUrl);
            if (mSecurityPrincipal != null)
            {
                env.put(Context.SECURITY_PRINCIPAL, mSecurityPrincipal);
            }
            if (mCredentials != null)
            {             
                env.put(Context.SECURITY_PRINCIPAL, mCredentials);   
            }
            if (mSecurityLevel != null)
            {                
                env.put(Context.SECURITY_PRINCIPAL, mSecurityLevel);   
            }
            
            mInitialContext = new InitialContext(env);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setException(e);
            setError("Cannot get Initial context with environment " 
                + env.toString());
            setError(e.getMessage());

            return false;
        }

        if (mInitialContext != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Releases the session.
     *
     * @param session wrapper to MQ session.
     */
    public void releaseSession(MQSession session)
    {
        mSessionPool.releaseSession(session);
    }

    /**
     * Reelases the temporary session.
     */
    public void releaseTemporarySession()
    {
        if (mTmpSession == null)
        {
            return;
        }

        mTmpSession.stopReceiving();

        closeConnection(mTempCon);
    }

    /**
     * Checks if a  connection is already available for the factory.
     *
     * @param fac connection factory.
     *
     * @return wrapper to JMS connection of oneis found.
     */
    private MQConnection isAvailable(String fac)
    {
        MQConnection con = null;

        try
        {
            con = (MQConnection) mConnectionTable.get(fac);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return con;
    }

    /**
     * Returns a queue destination.
     *
     * @param dest destination name.
     *
     * @return wrapper to destination.
     */
    private MQDestination getQueueDestination(String dest)
    {
        MQDestination destination = null;
        clear();

        if (destinationExists(dest))
        {
            return (MQDestination) mDestinationTable.get(dest);
        }

        try
        {
            javax.jms.Queue queue =
                (javax.jms.Queue) mInitialContext.lookup(dest);
            destination = new MQDestination(queue, ConfigConstants.QUEUE, dest);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            setError("Cannot look up queue ");
            setError(e.getMessage());

            return null;
        }

        registerDestination(dest, destination);

        return destination;
    }

    /**
     * Gets the topic destination.
     *
     * @param dest topic destination.
     *
     * @return wrapper to JMS destination.
     */
    private MQDestination getTopicDestination(String dest)
    {
        MQDestination destination = null;
        clear();

        if (destinationExists(dest))
        {

            return (MQDestination) mDestinationTable.get(dest);
        }

        try
        {
            javax.jms.Topic topic =
                (javax.jms.Topic) mInitialContext.lookup(dest);
            destination = new MQDestination(topic, ConfigConstants.TOPIC, dest);
        }
        catch (Exception e)
        {
            e.printStackTrace();

            setError("Cannot look up queue ");
            setError(e.getMessage());

            return null;
        }

        registerDestination(dest, destination);

        return destination;
    }

    /**
     * Creates temporary session.
     *
     * @param dest wrapper to JMS destination.
     */
    private void createTmpSession(MQDestination dest)
    {
        mLogger.fine("Creating Temporary Session");
        if ((mTempCon == null) || (dest == null))
        {
            return;
        }

        if (mTmpSession == null)
        {
            mTmpSession = new MQSession();
            mTmpSession.setConnection(mTempCon);
            mTmpSession.setDestination(dest);

            //   mTmpSession.setName("Temporary Session");
            mTmpSession.setStyle(mTempCon.getStyle());            
        }
        
        mLogger.fine("Created Temporary Session");
    }

    /**
     * Deregisters the connection.
     *
     * @param connection wrapper to JMS connection.
     */
    private void deRegisterConnection(MQConnection connection)
    {
        try
        {
            mConnectionTable.remove(connection.getFactory());
        }
        catch (Exception e)
        {
            mLogger.severe(mStringTranslator.getString(JMS_DEREGISTER_CON_FAILED,
                e.getMessage()));
            setWarning(mStringTranslator.getString(JMS_DEREGISTER_CON_FAILED,
                e.getMessage()));
        }
    }

    /**
     * Deregisters the destination.
     *
     * @param dest wrapper to JMS destination.
     */
    private void deRegisterDestination(MQDestination dest)
    {
        try
        {            
            mDestinationTable.remove(dest.getLookupName());
        }
        catch (Exception e)
        {
            mLogger.severe(mStringTranslator.getString(JMS_DEREGISTER_DEST_FAILED,
                e.getMessage()));
        }
    }

    /**
     * Registres the connection.
     *
     * @param factory connection factory.
     * @param connection Wrapper to JMS connection.
     */
    private void registerConnection(
        String factory,
        MQConnection connection)
    {
        try
        {
            mConnectionTable.put(factory, connection);
            connection.incrementUsage();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Registers destination.
     *
     * @param name destination name.
     * @param dest wrapper to destination.
     */
    private void registerDestination(
        String name,
        MQDestination dest)
    {
        try
        {
            mDestinationTable.put(name, dest);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
