/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InboundMessageHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;


import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.wsdl11wrapper.Wsdl11WrapperHelper;
import com.sun.jbi.binding.jms.mq.MQManager;
import com.sun.jbi.binding.jms.mq.MQSession;
import com.sun.jbi.binding.jms.config.ConfigConstants;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;

import javax.jms.JMSException;
import javax.jms.Message;

import javax.xml.namespace.QName;

/**
 * This class handles only In MEPS.
 * 
 * @author Sun Microsystems Inc.
 */
public class InboundMessageHandler
    extends MessageHandlerImpl
{
    /**
     * Creates a new InboundMessageHandler object.
     */
    public InboundMessageHandler()
    {
        super();
    }

    /**
     * Sanity.
     */
    public void check()
    {
        EndpointBean eb = getBean();
        Message msg = getJMSMessage();
        MessageExchange exch = getNMSMessage();
        QName operation = null;
        String oplocal = null;
        String opnamespace = null;

        if (msg != null)
        {
            try
            {
                oplocal =
                    msg.getStringProperty(MessageProperties.JBI_OPERATION_NAME);
                opnamespace =
                    msg.getStringProperty(MessageProperties.JBI_OPERATION_NAMESPACE);
            }
            catch (JMSException je)
            {
                ;
            }

            if (oplocal != null)
            {
                operation = new QName(opnamespace, oplocal);
            }

            if ((operation == null) || (
                        operation.toString().trim().equals("")
                    ))
            {
                operation = eb.getDefaultOperation();
            }

            String mep = eb.getMEP(operation.getLocalPart());
        }

        if (exch != null)
        {
            operation = exch.getOperation();
        }

        setCurrentOperation(operation);
    }

    /**
     * Handler contract.
     */
    public void execute()
    {
        super.execute();
        mLogger.fine(mStringTranslator.getString(JMS_INBOUND_HANDLER));

        /* do sanity first
         */
        check();

        if (!isValid())
        {
            mLogger.severe(mStringTranslator.getString(JMS_INVALID_MESSAGE));

            return;
        }

        if (getNMSMessage() != null)
        {
            processNMSMessage();

            if (!isValid())
            {
                if (getNMSMessage().getStatus() == ExchangeStatus.ACTIVE)
                {
                    sendNMSStatus();
                }
            }
        }
        else if (getJMSMessage() != null)
        {
            processJMSMessage();

            if (!isValid())
            {
                sendJMSError();
            }
        }
        else
        {
            mLogger.severe(mStringTranslator.getString(JMS_INVALID_MESSAGE));

            return;
        }
    }

    /**
     * Main task for JMS message processing is done here.
     */
    private void processJMSMessage()
    {
        Message msg = getJMSMessage();
        EndpointBean eb = getBean();
        MessageAdaptor adaptor = MessageAdaptorFactory.getAdaptor(
                                        msg, eb.getDeploymentType());      
        
        if (adaptor == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));
            setError(mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));

            return;
        }
        
        Wsdl11WrapperHelper wrapperhelper;
        if (eb.getDeploymentType().trim().equals("WSDL11"))
        {            
            wrapperhelper = new Wsdl11WrapperHelper(eb.getWsdlDefinition());
            adaptor.setEpilogueProcessor(wrapperhelper);
        }
        
        
        String inputtype = eb.getInputType(getCurrentOperation().getLocalPart());

        if (inputtype == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));
            setError(mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));

            return;
        }

        if (!adaptor.getName().equals(inputtype.trim()))
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_INCONSISTENT_MESSAGE_TYPE));
            setError(mStringTranslator.getString(JMS_INCONSISTENT_MESSAGE_TYPE));

            //send errorr
            return;
        }

        mLogger.fine("CURRENT OPERATION "
            + getCurrentOperation().getLocalPart());
        mLogger.fine("CURRENT MEP "
            + getBean().getMEP(getCurrentOperation().getLocalPart()));

        MessageExchange ex =
            MessageExchangeHelper.createExchange(getBean().getMEP(getCurrentOperation().
                                                                   getLocalPart()));
        setNMSMessage(ex);
        setNMSHeaders();

        if (getNMSMessage() == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_NMS_HEADERS_FAILED));
            setError(mStringTranslator.getString(JMS_NMS_HEADERS_FAILED));

            //free up any resources here. set them to null
            return;
        }

        adaptor.convertJMStoNMSMessage(getJMSMessage(), getNMSMessage());

        if (ex == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_JMS_TO_NMS_FAILED,
                    adaptor.getError()));
            setError(mStringTranslator.getString(JMS_JMS_TO_NMS_FAILED,
                    adaptor.getError()));

            //free up any resources here. set them to null
            return;
        }

        Object replyto =
            MessageExchangeHelper.getDestinationName(adaptor.getJMSReplyTo(), eb);
        registerMessage(ex.getExchangeId(), replyto);

        if (!send(ex))
        {
            mLogger.severe(mStringTranslator.getString(JMS_CANNOT_SEND));
            setError(mStringTranslator.getString(JMS_CANNOT_SEND) + "\n"
                + getError());
            deRegisterMessage(ex.getExchangeId());

            return;
        }

        return;
    }

    /**
     * Processes the NMR message.
     */
    private void processNMSMessage()
    {
        MessageExchange exch = getNMSMessage();

        if (canTerminateConsumer())
        {
            return;
        }

        String oper = getCurrentOperation().getLocalPart();
        mLogger.fine("OPERATION " + oper);

        EndpointBean eb = getBean();
        mLogger.fine("OUTPUT TYPE" + eb.getOutputType(oper));

        MessageAdaptor adaptor =
            MessageAdaptorFactory.getAdaptor(eb.getOutputType(oper), 
                                             eb.getDeploymentType());
        MQSession session = getSession();

        if (session == null)
        {
            return;
        }

        Message msg = session.createJMSMessage(eb.getOutputType(oper));

        if (adaptor == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));
            setError(mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));

            return;
        }

        Wsdl11WrapperHelper wrapperhelper = null;
        if (eb.getDeploymentType().trim().equals("WSDL11"))
        {            
            wrapperhelper = new Wsdl11WrapperHelper(eb.getWsdlDefinition());
            adaptor.setEpilogueProcessor(wrapperhelper);
        }
        
        adaptor.setEpilogueProcessor(wrapperhelper);
        adaptor.convertNMStoJMSMessage(exch, msg);

        MQManager man = JMSBindingContext.getInstance().getMQManager();
        Object lookup =  getArtifact(exch.getExchangeId());
        
        if (!send(msg, lookup, session))
        {            
            deRegisterMessage(exch.getExchangeId());

            return;
        }

        deRegisterMessage(exch.getExchangeId());
        clear();

        if (exch.getStatus() == ExchangeStatus.ACTIVE)
        {
            sendNMSStatus();
        }
    }

    /**
     * Sends an error to JMS.
     */
    private void sendJMSError()
    {
        EndpointBean eb = getBean();

        String mep = eb.getMEP(getCurrentOperation().getLocalPart());

        if ((mep == null) || (mep.trim().equals(ConfigConstants.IN_ONLY)))
        {
            return;
        }

        MessageAdaptor adaptor =
            MessageAdaptorFactory.getAdaptor(eb.getOutputType(getCurrentOperation().
                                                                  getLocalPart()),"");

        if (adaptor == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_IRRECOVERABLE_ERROR));

            return;
        }

        MQSession session = getSession();

        if (session == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_IRRECOVERABLE_ERROR));

            return;
        }

        Message msg =
            session.createJMSMessage(eb.getOutputType(getCurrentOperation().
                                                          getLocalPart()));
        adaptor.updateJMSErrorMessage(msg, getError());

        MessageExchange exch = getNMSMessage();
        Object replyto =
            MessageExchangeHelper.getDestinationName(adaptor.getJMSReplyTo(), eb);

        if (!send(msg, replyto, session))
        {
            ;
        }
    }
}
