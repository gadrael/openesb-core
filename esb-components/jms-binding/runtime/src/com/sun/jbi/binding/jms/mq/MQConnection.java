/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MQConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.mq;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;

import com.sun.jbi.binding.jms.util.UtilBase;

import java.util.logging.Logger;

import javax.jms.Connection;
import javax.jms.JMSException;

/**
 * MQ connection.
 *
 * @author Sun Microsystems Inc.
 */
public final class MQConnection
    extends UtilBase implements JMSBindingResources
{
    /**
     * JMS connection.
     */
    private javax.jms.Connection mConnection;
    /**
     *  Logger.
     */
    private Logger mLogger;
    /**
     * Factory.
     */
    private String mFactory;
    /**
     *  Style.
     */
    private int mStyle;
    /**
     *  Usage count.
     */
    private int mUsageCount = 0;
    /**
     * String translator.
     */    
    private StringTranslator mStringTranslator;

    /**
     * Creates a new MQConnection object.
     *
     * @param fac factory name.
     * @param con connection.
     * @param style style.
     */
    public MQConnection(
        String fac,
        javax.jms.Connection con,
        int style)
    {
        mFactory = fac;
        mConnection = con;
        mStyle = style;
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator = JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Gets the connection.
     *
     * @return JMS connection.
     */
    public javax.jms.Connection getConnection()
    {
        return mConnection;
    }

    /**
     * Get factory.
     *
     * @return factory.
     */
    public String getFactory()
    {
        return mFactory;
    }

    /**
     * Gets the style. 
     *
     * @return style.
     */
    public int getStyle()
    {
        return mStyle;
    }

    /**
     * Closes the connection.
     */
    public synchronized void close()
    {
        clear();

        if (mUsageCount > 0)
        {
            mLogger.warning(mStringTranslator.getString(
                    JMS_MQ_CANNOT_CLOSE_FACTORY, mFactory));

            return;
        }

        try
        {
            mConnection.close();
        }
        catch (JMSException je)
        {
            je.printStackTrace();
            setError(mStringTranslator.getString(
                    JMS_MQ_CANNOT_CLOSE_FACTORY, mFactory));
            setError(je.getMessage());
        }
    }

    /**
     * Decrements the connection usage.
     */
    public synchronized void decrementUsage()
    {
        mUsageCount--;
    }

    /**
     * Increments the usage.
     */
    public synchronized void incrementUsage()
    {
        mUsageCount++;
    }

    /**
     * Starts the connection.
     *
     * @return return code.
     */
    public synchronized int start()
    {
        clear();

        try
        {
            mConnection.start();
        }
        catch (Exception e)
        {
            setError(mStringTranslator.getString(
                    JMS_MQ_CANNOT_START_CONNECTION, mConnection));
            setError(e.getMessage());

            return MQCodes.FAILURE;
        }

        return MQCodes.SUCCESS;
    }

    /**
     * Stops the connection.
     */
    public synchronized void stop()
    {
        clear();

        if (mUsageCount > 0)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_MQ_CANNOT_CLOSE_FACTORY, mFactory));

            return;
        }

        try
        {
            mConnection.stop();
        }
        catch (JMSException je)
        {
            setError(mStringTranslator.getString(
                    JMS_MQ_CANNOT_CLOSE_FACTORY, mFactory));
            setError(je.getMessage());
            je.printStackTrace();
        }
    }
}
