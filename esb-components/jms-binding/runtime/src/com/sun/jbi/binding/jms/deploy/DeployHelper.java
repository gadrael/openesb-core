/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResolver;
import com.sun.jbi.binding.jms.JMSBindingResources;

import com.sun.jbi.binding.jms.config.ConfigConstants;

import com.sun.jbi.common.ConfigFileValidator;
import com.sun.jbi.common.FileListing;

import com.sun.jbi.wsdl2.Description;

import java.io.File;

import java.util.logging.Logger;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;

/**
 * This class ia  a Helper clas to do deployment.
 *
 * @author Sun Microsystems Inc.
 * 
 */
public class DeployHelper
    extends com.sun.jbi.binding.jms.util.UtilBase
    implements JMSBindingResources
{
 
    /**
     * Reader object to read config XML file.
     */
    private ConfigReader mConfigReader;
    
    /**
     * Resolver object.
     */
    private JMSBindingResolver mResolver;
    
    /**
     * Jbi.xml deployment descriptor file.
     */
    private String mDeploymentDescriptor;

    /**
     * Logger object.
     */
    private Logger mLogger;

    /**
     * Name of the endpoint config XMl file.
     */
    private String mEndpointFile;

    /**
     * Application Sub assembly of deployment.
     */
    private String mSUID;

    /**
     * Name of the schema file.
     */
    private String mSchemaFile;
    /**
     *    Status.
     */
    private String mStatus = "";

    /**
     * Path of the sub assembly.
     */
    private String mSuPath;

    /**
     * Wsdl file name.
     */
    private String mWsdlFile;

    /**
     * String translator.
     */
    private StringTranslator mStringTranslator;

    /**
     * Wsdl 2.0 reader.
     */
    private WSDLFileReader mWsdlReader;

    /**
     * WSDL 1.1 reader
     */
    private WSDL11FileReader mWsdl11Reader;
    /**
     * List of endpoints.
     */
    private EndpointBean [] mEndpointList;
    
    /**
     * Deployment descriptor schema.
     */
    private String mDDSchema;
    
    /**
     * Deploy descriptor reader.
     */
    private DeployDescriptorReader mDeployDescriptorReader;

    /**
     * Creates a new DeployHelper object.
     *
     * @param suId application sub assembly Id.
     * @param suPath service unit path.
     * @param context binding context.
     * @param reslv RESOLVER
     */
    public DeployHelper(
        String suId,
        String suPath,
        javax.jbi.component.ComponentContext context,
        JMSBindingResolver reslv)
    {
        mSUID = suId;
        mResolver = reslv;
        mEndpointFile = FileListing.getXMLFile(suPath);
        mWsdlFile = FileListing.getWSDLFile(suPath);
        mSchemaFile =
            context.getInstallRoot() + ConfigConstants.JMS_DEPLOY_SCHEMA_FILE;
        mDDSchema =
            JMSBindingContext.getInstance().getContext().getInstallRoot()
            + ConfigConstants.JMS_DD_FILE_SCHEMA;
        mLogger = JMSBindingContext.getInstance().getLogger();
        mDeploymentDescriptor =
            suPath + File.separatorChar + "META-INF" + File.separatorChar
            + ConfigConstants.DEPLOY_DESCRIPTOR;
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
        setValid(true);
    }

    /**
     * Returns the status message.
     *
     * @return sttaus message corresponding to the operation perfomed.
     */
    public String getStatusMessage()
    {
        return mStatus;
    }

    /**
     * Method which does the actual deployment.
     *
     * @param isDeploy denotes if its during deployment or start.
     */
    public void doDeploy(boolean isDeploy)
    {
        super.clear();

        try
        {            
            if ((mDeploymentDescriptor != null)
                    && (new File(mDeploymentDescriptor)).exists())
            {
                doDDParsing();
                if ((!isValid()) && isDeploy)
                {
                    return;
                } 
            }
            
            String type = "WSDL20";
            if (mDeployDescriptorReader != null)
            {
                type = mDeployDescriptorReader.getType();
                mLogger.warning(
                        mStringTranslator.getString(JMS_DESCRIPTOR_NOT_FOUND));
            }

            
            if (mDeployDescriptorReader.getType().equalsIgnoreCase("WSDL20") &&
                (mWsdlFile != null) && (new File(mWsdlFile)).exists())
            {
                doWSDLDeploy();
            }
            else if (mDeployDescriptorReader.getType().equalsIgnoreCase("WSDL11") &&
                (mWsdlFile != null) && (new File(mWsdlFile)).exists())
            {
                doWSDL11Deploy();
            }
            else if ((mEndpointFile != null)
                    && (new File(mEndpointFile)).exists())
            {
                doConfigDeploy();
            }           
            else
            {
                setError(mStringTranslator.getString(JMS_ARTIFACT_NOT_FOUND));
                mLogger.info(mStringTranslator.getString(JMS_ARTIFACT_NOT_FOUND));
                return;
            }
            if (mDeployDescriptorReader != null)
            {
                if (isValid())
                {
                    checkConsistency();
                }
            }
   
            /**
             * Here we have a violation of spec 1.0. The spec says that
             * a jbi.xml must be present in an SU, 
             * But we are making it optional here, just to make a backward
             * compatibility.  
             * This optionality will be made mandatory in future.
             *  
             */
            
           /*
            else
            {
                setError(mStringTranslator.getString(JMS_NO_DD) + "\n" + getError());
                mLog.info(mStringTranslator.getString(JMS_NO_DD));
                setValid(false);

                return;
            }
            */

            if ((!isValid()) && isDeploy)
            {
                return;
            }
            deployEndpoints();
            EndpointRegistry dr = JMSBindingContext.getInstance().getRegistry();
            dr.persist();
        }
        catch (Exception de)
        {
            setError(de.getMessage());
            setException(de);
            de.printStackTrace();
        }
    }

    /**
     * Sets the status message.
     *
     * @param status string for status.
     */
    private void setStatusMessage(String status)
    {
        mStatus = status;
    }

     /**
     * Deploys the endpoints.
     */
    private void deployEndpoints()
    {
        EndpointRegistry dr = JMSBindingContext.getInstance().getRegistry();
        int dups = 0;
        for (int i = 0; i < mEndpointList.length; i++)
        {
            String epName = mEndpointList[i].getUniqueName();

            if (dr.containsEndpoint(mEndpointList[i]) || 
                dr.containsDestination(mEndpointList[i]))
            {
                dups++;
                mLogger.warning(mStringTranslator.getString(
                        JMS_DUPLICATE_ENDPOINT, epName));
                setWarning(mStringTranslator.getString(JMS_DUPLICATE_ENDPOINT,
                        epName));
            }
            else
            {
                //Register the endpoint with the Deployment Registry   
                String regKey = dr.registerEndpoint(mEndpointList[i]);
            }
            
            if (mEndpointList.length == dups)
            {
                setError(mStringTranslator.getString(JMS_DUPLICATE_ENDPOINTS));
                return;
            }
        }
    }

    /**
     * Deployment in case of an XML file.
     */
    private void doConfigDeploy()
    {
        ConfigFileValidator validator =
            new ConfigFileValidator(mSchemaFile, mEndpointFile);
        validator.setValidating();
        validator.validate();

        Document d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_INVALID_CONFIG_FILE, mEndpointFile));
            setError(mStringTranslator.getString(JMS_INVALID_CONFIG_FILE,
                    mEndpointFile) + " " + validator.getError());

            return;
        }

        mConfigReader = new ConfigReader();
        mConfigReader.init(d);

        if (!mConfigReader.isValid())
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_INVALID_CONFIG_FILE, mEndpointFile));
            setError(mStringTranslator.getString(JMS_INVALID_CONFIG_FILE,
                    mEndpointFile) + " " + mConfigReader.getError());

            return;
        }

        setWarning(mConfigReader.getWarning());
        mEndpointList = mConfigReader.getEndpoint();

        for (int i = 0; i < mEndpointList.length; i++)
        {
            mEndpointList[i].setDeploymentId(mSUID);
        }
    }
    
    
    /**
     * Deployment for a WSDL 1.1 file.
     */
    private void doWSDL11Deploy()
    {
        mWsdl11Reader = new WSDL11FileReader(mResolver);
        mWsdl11Reader.init(mWsdlFile);

        if (!mWsdl11Reader.isValid())
        {
            mLogger.severe(mStringTranslator.getString(JMS_INVALID_WSDL_FILE,
                    mWsdlFile));
            setError(mStringTranslator.getString(JMS_INVALID_WSDL_FILE,
                    mWsdlFile) + " " + mWsdl11Reader.getError());

            return;
        }

        setWarning(mWsdl11Reader.getWarning());

        mEndpointList = mWsdl11Reader.getEndpoint();

        for (int i = 0; i < mEndpointList.length; i++)
        {
            mEndpointList[i].setDeploymentId(mSUID);
        }
    }

    /**
     * Deployment for a WSDL file.
     */
    private void doWSDLDeploy()
    {
        WSDLFileValidator validator = null;
        validator = new WSDLFileValidator(mWsdlFile);
        validator.validate();

        Description d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLogger.severe(mStringTranslator.getString(JMS_INVALID_WSDL_FILE,
                    mWsdlFile));
            setError(mStringTranslator.getString(JMS_INVALID_WSDL_FILE,
                    mWsdlFile) + " " + validator.getError());

            return;
        }

        mWsdlReader = new WSDLFileReader(mResolver);
        mWsdlReader.init(d);

        if (!mWsdlReader.isValid())
        {
            mLogger.severe(mStringTranslator.getString(JMS_INVALID_WSDL_FILE,
                    mWsdlFile));
            setError(mStringTranslator.getString(JMS_INVALID_WSDL_FILE,
                    mWsdlFile) + " " + mWsdlReader.getError());

            return;
        }

        setWarning(mWsdlReader.getWarning());

        mEndpointList = mWsdlReader.getEndpoint();

        for (int i = 0; i < mEndpointList.length; i++)
        {
            mEndpointList[i].setDeploymentId(mSUID);
        }
    }
    
      /**
     * Parses the deployment descriptor jbi.xml.
     */
    private void doDDParsing()
    {
        ConfigFileValidator validator =
            new ConfigFileValidator(mDDSchema, mDeploymentDescriptor);
        validator.setValidating();
        validator.validate();

        Document d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLogger.severe(mStringTranslator.getString(JMS_INVALID_DD,
                    mDeploymentDescriptor));
            setError(mStringTranslator.getString(JMS_INVALID_DD, mDeploymentDescriptor)
                + " " + validator.getError());
            setValid(false);

            return;
        }

        /**
         * Match service names in artifacts file ( endpoints.xml) and the DD.
         * Every endpoint in the the DD file should be present in the
         * artifacts file. We dont care if the artifacts file has more
         * endpoints. Those will be ignored. The condition is the DD XML file
         * should be a sub-set of artifacts file. The spec does not govern
         * this logic so it is upto the component to decide how to veryify
         * consistence between  DD and its artifacts.
         */
        /**
         * A valid question that arises here is why are we having 2 XML files
         * to decorate an endpoint. The artifacts XML file is the older form
         * which was used when the  spec did not accomodate any DD for an SU.
         * So that is still around.  Any component specific decoration for the
         * endpoints can be done  through the DD jbi.xml also. And that would
         * be the correct way to do it. We dont do it that way because there
         * are other modules that might           depend on the endpoints.xml
         * file, so to maintain their functionality  the older endpoints.xml
         * file is still used to decorate endpoints.
         */
        mDeployDescriptorReader = new DeployDescriptorReader();
        mDeployDescriptorReader.init(d);

        if (!mDeployDescriptorReader.isValid())
        {
            mLogger.severe(mStringTranslator.getString(JMS_INVALID_DD, mEndpointFile));
            setError(mStringTranslator.getString(JMS_INVALID_DD, mEndpointFile) + " "
                + mDeployDescriptorReader.getError());
            setValid(false);

            return;
        }
        
    }
    
    /**
     * Checks the consistency of information between the jbi.xml and
     * a configuration file.
     */  
    
    private void checkConsistency()
    {
        if (getProviderCount() != mDeployDescriptorReader.getProviderCount())
        {
            setError(mStringTranslator.getString(JMS_INCONSISTENT_PROVIDER_DATA));

            return;
        }

        if (getConsumerCount() != mDeployDescriptorReader.getConsumerCount())
        {
            setError(mStringTranslator.getString(JMS_INCONSISTENT_CONSUMER_DATA));

            return;
        }

        if (mEndpointList != null)
        {
            for (int i = 0; i < mEndpointList.length; i++)
            {
                EndpointBean eb = mEndpointList[i];
                QName sername =
                    new QName(eb.getValue(ConfigConstants.SERVICE_NAMESPACE),
                        eb.getValue(ConfigConstants.SERVICENAME));
                QName intername =
                    new QName(eb.getValue(ConfigConstants.INTERFACE_NAMESPACE),
                        eb.getValue(ConfigConstants.INTERFACE_LOCALNAME));
                String epname = eb.getValue(ConfigConstants.ENDPOINTNAME);
                int role = eb.getRole();

                if (!mDeployDescriptorReader.isPresent(sername, intername,
                            epname, role))
                {
                    setError(mStringTranslator.getString(JMS_INCONSISTENT_DATA, 
                            sername, intername, epname));

                    return;
                }
            }
        }
    }
    
        /**
     * Returns the consumer endpoint count.
     *
     * @return  consumer endpoints.
     */
    public int getConsumerCount()
    {
        int count = 0;
        if (mEndpointList == null)
        {
            return 0;
        }
        for (int i = 0; i < mEndpointList.length; i++)
        {
            EndpointBean eb = mEndpointList[i];

            if (eb.getRole() == ConfigConstants.CONSUMER)
            {
                count++;
            }
        }

        return count;
    }

    /**
     * Returns the number of provider endpoints in the artifacts file.
     *
     * @return  provider endpoint count.
     */
    public int getProviderCount()
    {
        int count = 0;

        if (mEndpointList == null)
        {
            return 0;
        }
        for (int i = 0; i < mEndpointList.length; i++)
        {
            EndpointBean eb = mEndpointList[i];

            if (eb.getRole() == ConfigConstants.PROVIDER)
            {
                count++;
            }
        }

        return count;
    }
}
