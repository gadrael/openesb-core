/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.config;

import java.io.File;

/**
 * This class holds all the  information used for deployment in JMS
 * binding.
 *
 * @author Sun Microsystems Inc.
  */
public interface ConfigConstants
{
    /**
     * String for provider role.
     */
     String PROVIDER_STRING = "provider";
    
    /**
     * The name of the jbi schema file for jbi.xml in an SU.
     */
     String JBI_SCHEMA_FILE = "jbi.xsd";

    /**
     * String representing the namespace-uri in a QName.
     */
     String NAMESPACE_URI = "namespace-uri";

    /**
     * String representing the local-part in a QName.
     */
     String LOCAL_PART = "local-part";

    /**
     *  String representing a name.
     */
     String NAME = "name";

    /**
     * String for consumer role.
     */
     String CONSUMER_STRING = "consumer";

    /**
     * Role of endpoint.
     */
     int CONSUMER = 0;

    /**
     * Role of endpoint.
     */
     int PROVIDER = 1;

    /**
     * String representing an interface.
     */
     String INTERFACE = "interface";

    /**
     * String representing an interface namespace.
     */
     String INTERFACE_NAMESPACE = "INTERFACE_NAMESPACE";

    /**
     * String representing an interface local name.
     */
     String INTERFACE_LOCALNAME = "INTERFACE_LOCALNAME";

    /**
     *  Connection string.
     */
    String CONNECTION = "connection";

    /**
     * Connection factory name in the XMl file.
     */
    String CONNECTION_FACTORY = "connection-factory-name";

    /**
     * Connection params tag in the config file.
     */
    String CONNECTION_PARAMS = "connection-params";

    /**
     * Connection password tag in config file.
     */
    String CONNECTION_PASSWORD = "connection-password";

    /**
     * Connection user name in the config file.
     */
    String CONNECTION_USER_ID = "connection-user-name";

    /**
     * Default value for durability.
     */
    String DEFAULT_DURABILITY = "non-durable";

    /**
     *  Default value for input message type.
     */
    String DEFAULT_INPUT_MESSAGE_TYPE = "TextMessage";

    /**
     *  Default value for output message type.
     */
    String DEFAULT_OUTPUT_MESSAGE_TYPE = "TextMessage";

    /**
     * Deployment id.
     */
    String DEPLOYMENTID = "deployment-id";

    /**
     * Deploy folder.
     */
    String DEPLOY_FOLDER =
        File.separator + "schemaorg_apache_xmlbeans.system" + File.separator + "deployment"
        + File.separator;

    /**
     * The name of the configuration file.
     */
    String DEPL_CONFIG_FILE_NAME = "endpoints.xml";

    /**
     * The name of the schema file.
     */
    String DEPL_CONFIG_SCHEMA_FILE = "endpoints.xsd";

    /**
     * Destination name in XML file.
     */
    String DESTINATION_NAME = "destination-name";

    /**
     * Destination style in XML file.
     */
    String DESTINATION_STYLE = "destination-style";

    /**
     * Durability tag in XML file.
     */
    String DURABILITY = "durability";

    /**
     * String representing Durable message.
     */
    String DURABLE = "DURABLE";

    /**
     * The element name for endpoint in the XML config file.
     */
    String ENDPOINT = "endpoint";

    /**
     * The element name for endpoint name in the XML config file.
     */
    String ENDPOINTNAME = "endpoint-name";

    /**
     * The element name for endpoint type in the XML config file.
     */
    String ENDPOINT_TYPE = "endpoint-role";

    /**
     * Input message type.
     */
    String INPUT_MESSAGE_TYPE = "input-message-type";

    /**
     * In Only MEP.
     */
    String IN_ONLY = "http://www.w3.org/ns/wsdl/in-only";

    /**
     * In Optional Out MEP.
     */
    String IN_OPTIONAL_OUT = "http://www.w3.org/ns/wsdl/in-opt-out";

    /**
     * In Out MEP.
     */
    String IN_OUT = "http://www.w3.org/ns/wsdl/in-out";
    
        /**
     * In Only MEP.
     */
    String OUT_ONLY = "http://www.w3.org/ns/wsdl/out-only";

    /**
     * In Optional Out MEP.
     */
    String OUT_OPTIONAL_IN = "http://www.w3.org/ns/wsdl/out-opt-in";

    /**
     * In Out MEP.
     */
    String OUT_IN = "http://www.w3.org/ns/wsdl/out-in";

    /**
     * Config file name.
     */
    String JMS_CONFIG_FILE_NAME = File.separator + "jms_config.xml";

    /**
     * Deploy file schema.
     */
    String JMS_DEPLOY_SCHEMA_FILE  = File.separator + "schema" +
                                    File.separator + "endpoints.xsd";
    /**
     *  Schema file for the config XML.
     */
    String JMS_CONFIG_FILE_SCHEMA =
        File.separator + "schema" + File.separator + "jms_config.xsd";
    /**
     * Schema file for the DD, given by JBI specificatrion.
     */

    String JMS_DD_FILE_SCHEMA = 
        File.separator + "schema" + File.separator + "jbi.xsd";
    /**
     * Namespace for JMS binding. This is also the binding type.
     */
    String JMS_NAMESPACE = "http://sun.com/jbi/wsdl/jms10";

    /**
     * Work manager for JMS binding.
     */
    String JMS_WORK_MANAGER = "JMS_WORK_MANAGER";

    /**
     * The element name for MEP in the XML config file.
     */
    String MEP = "mep";

    /**
     * Message selector.
     */
    String MESSAGE_SELECTOR = "message-selector";

    /**
     * Message type.
     */
    String MESSAGE_TYPE = "message-type";
    
    /**
     * Work Manager for requests from NMR.
     */
    String NMS_WORK_MANAGER = "NMS_WORK_MANAGER";

    /**
     * Represents a non-durbale message.
     */
    String NON_DURABLE = "NON_DURABLE";

    /**
     * The element name for operastion in the XML config file.
     */
    String OPERATION = "operation";

    /**
     * Operation name.
     */
    String OPERATION_NAME = "name";

    /**
     * Namespace for operation.
     */
    String OPERATION_NAMESPACE = "namespace";

    /**
     * output message type in deployment file.
     */
    String OUTPUT_MESSAGE_TYPE = "output-message-type";
    
    /**
     * Queue.
     */
    String QUEUE_STRING = "Queue";

    /**
     * Reply to in a deployment file.
     */
    String REPLY_TO = "reply-to";

    /**
     * Robust In Only MEP.
     */
    String ROBUST_IN_ONLY = "http://www.w3.org/ns/wsdl/robust-in-only";
    
    /**
     * Robust out Only MEP.
     */
    String ROBUST_OUT_ONLY = "http://www.w3.org/ns/wsdl/robust-out-only";

    /**
     * Service.
     */
    String SERVICE = "service";

    /**
     * The element name for service name in the XML config file.
     */
    String SERVICENAME = "service-name";

    /**
     * Service namespace value used internally.
     */
    String SERVICE_NAMESPACE = "service-namespace";

    /**
     * Target namespace.
     */
    String TARGET_NAMESPACE = "targetNamespace";

    /**
     * Time to live tag in deployment file.
     */
    String TIME_TO_LIVE = "time-to-live";

    /**
     * Topic.
     */
    String TOPIC_STRING = "Topic";

    /**
     * WSDL file name for the deployment.
     */
    String WSDL_FILE_NAME = "endpoints.wsdl";

    /**
     * Integer value for a Queue.
     */
    int QUEUE = 0;

    /**
     * Integer value for a topic.
     */
    int TOPIC = 1;
    
    /**
     * Deployment descriptor file.
     */
    public   String DEPLOY_DESCRIPTOR = "jbi.xml";
}
