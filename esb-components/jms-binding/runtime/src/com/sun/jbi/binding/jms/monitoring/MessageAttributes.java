/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageAttributes.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.monitoring;

/**
 * 
 *
 * @author Sun Microsystems Inc.
 */
public class MessageAttributes
{
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private MessageStatus mStatus;

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private String mEndpointName;

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private String mJMSMessageId;

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private String mNMSMessageId;

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private String mServicename;

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private String mTimeStamp;

    /**
     * Setter for property mEndpointName.
     *
     * @param mEndpointName New value of property mEndpointName.
     */
    public void setMEndpointName(java.lang.String mEndpointName)
    {
        this.mEndpointName = mEndpointName;
    }

    /**
     * Getter for property mEndpointName.
     *
     * @return Value of property mEndpointName.
     */
    public java.lang.String getMEndpointName()
    {
        return mEndpointName;
    }

    /**
     * Setter for property mJMSMessageId.
     *
     * @param mJMSMessageId New value of property mJMSMessageId.
     */
    public void setMJMSMessageId(java.lang.String mJMSMessageId)
    {
        this.mJMSMessageId = mJMSMessageId;
    }

    /**
     * Getter for property mJMSMessageId.
     *
     * @return Value of property mJMSMessageId.
     */
    public java.lang.String getMJMSMessageId()
    {
        return mJMSMessageId;
    }

    /**
     * Setter for property mNMSMessageId.
     *
     * @param mNMSMessageId New value of property mNMSMessageId.
     */
    public void setMNMSMessageId(java.lang.String mNMSMessageId)
    {
        this.mNMSMessageId = mNMSMessageId;
    }

    /**
     * Getter for property mNMSMessageId.
     *
     * @return Value of property mNMSMessageId.
     */
    public java.lang.String getMNMSMessageId()
    {
        return mNMSMessageId;
    }

    /**
     * Setter for property mServicename.
     *
     * @param mServicename New value of property mServicename.
     */
    public void setMServicename(java.lang.String mServicename)
    {
        this.mServicename = mServicename;
    }

    /**
     * Getter for property mServicename.
     *
     * @return Value of property mServicename.
     */
    public java.lang.String getMServicename()
    {
        return mServicename;
    }

    /**
     * Setter for property mStatus.
     *
     * @param mStatus New value of property mStatus.
     */
    public void setMStatus(MessageStatus mStatus)
    {
        this.mStatus = mStatus;
    }

    /**
     * Getter for property mStatus.
     *
     * @return Value of property mStatus.
     */
    public MessageStatus getMStatus()
    {
        return mStatus;
    }

    /**
     * Setter for property mTimeStamp.
     *
     * @param mTimeStamp New value of property mTimeStamp.
     */
    public void setMTimeStamp(java.lang.String mTimeStamp)
    {
        this.mTimeStamp = mTimeStamp;
    }

    /**
     * Getter for property mTimeStamp.
     *
     * @return Value of property mTimeStamp.
     */
    public java.lang.String getMTimeStamp()
    {
        return mTimeStamp;
    }
}
