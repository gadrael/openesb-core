/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegistryCacheImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;


import java.util.HashMap;
import java.util.Iterator;

import java.util.Set;

import java.util.logging.Logger;


/**
 * This is a registry which maintains the servicename, endpoint name    across
 * the bean object.
 *
 * @author Sun Microsystems Inc.
 */
public final class RegistryCacheImpl
    implements RegistryImplementor, JMSBindingResources
{
    /**
     * Singleton reference.
     */
    private static RegistryCacheImpl sMe;

    /**
     * List of all registered channels.
     */
    private HashMap mDeployments;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * Helper for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * Private constructor.
     */
    public RegistryCacheImpl()
    {
        mDeployments = new HashMap();
        mLog = JMSBindingContext.getInstance().getLogger();
        mTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Returns the keys.
     *
     * @return Iterator to keys.
     */
    public Iterator getAllKeys()
    {
      return mDeployments.keySet().iterator();
    }

    /**
     * Returns the values.
     *
     * @return Collection of values.
     */
    public java.util.Collection getAllValues()
    {
        return mDeployments.values();
    }

    /**
     * Used to find a registered endpoint which matches the specified service.
     *
     * @param str the service to match against.
     *
     * @return the appropriate endpoint bean, or no such endpoint exists.
     */
    public Object getValue(String str)
    {
        EndpointBean eb = null;

        try
        {
            eb = (EndpointBean) mDeployments.get(str);
        }
        catch (Exception e)
        {
            ;
        }

        return eb;
    }

    /**
     * Removes all the entries from the registry.
     */
    public void clearRegistry()
    {
        if (mDeployments == null)
        {
            return;
        }

        mDeployments = new HashMap();
    }

    /**
     * Checks if the registry contains the given key.
     *
     * @param str key.
     *
     * @return true if it contains.
     */
    public boolean containsKey(String str)
    {
        Set keyset = mDeployments.keySet();
        Iterator iter = keyset.iterator();

        while (iter.hasNext())
        {
            String ser = (String) iter.next();
            EndpointBean eb = (EndpointBean) mDeployments.get(ser);

            if (eb.getUniqueName().trim().equals(str))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Initialises the registry.
     *
     * @param properties properties.
     *
     * @throws javax.jbi.JBIException exception if registry cnnnot be cleared.
     */
    public void init(java.util.Properties properties)
        throws javax.jbi.JBIException
    {
        clearRegistry();
    }

    /**
     * Registers a key.
     *
     * @param str key.
     * @param obj value.
     *
     * @return registered key.
     */
    public synchronized String registerKey(
        String str,
        Object obj)
    {
        mLog.info(mTranslator.getString(JMS_REGISTER_ENDPOINT, str));

        try
        {
            mDeployments.put(str, obj);
        }
        catch (Exception e)
        {
            ;
        }

        return str;
    }

    /**
     * Removes the key.
     *
     * @param str key.
     */
    public synchronized void removeKey(String str)
    {
        mLog.info(mTranslator.getString(JMS_DEREGISTER_ENDPOINT, str));

        EndpointBean eb = null;

        try
        {
            eb = (EndpointBean) mDeployments.remove(str);
        }
        catch (Exception e)
        {
            ;
        }

        if (eb == null)
        {
            mLog.severe(mTranslator.getString(JMS_DEREGISTER_ENDPOINT_FAILED,
                    str));
        }
    }

    /**
     * Serializes the registry.
     *
     * @throws javax.jbi.JBIException jbi exception.
     */
    public void serialize()
        throws javax.jbi.JBIException
    {
    }
}
