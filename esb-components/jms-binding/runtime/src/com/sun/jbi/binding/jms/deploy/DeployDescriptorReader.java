/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployDescriptorReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import java.util.StringTokenizer;

import java.util.logging.Logger;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

/**
 * This class reads the SU jbi.xml and loads up the values.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeployDescriptorReader
    extends UtilBase
    implements JMSBindingResources
{
    /**
     * Document object of the XML config file.
     */
    private Document mDoc;

    /**
     * Logger Object.
     */
    private Logger mLog;

    /**
     * i18n.
     */
    private StringTranslator mTranslator;

    /**
     * List of endpoints in the SU jbi.xml.
     */
    private EndpointInfo [] mEPList;

    /**
     * Number of consumers.
     */
    private int mNoOfConsumers;

    /**
     * Number of providers.    
     */
    private int mNoOfProviders;
    
    /**
     * Type of artifact file, XML, WSDL11 or WSDL20
     */
    private String mType;

    /**
     * Creates a new ConfigReader object.
     */
    public DeployDescriptorReader()
    {
        mLog = JMSBindingContext.getInstance().getLogger();
        mTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
        setValid(true);
    }

    /**
     * Returns the number of consumer endpoints.
     *
     * @return consumer endpoint count.
     */
    public int getConsumerCount()
    {
        return mNoOfConsumers;
    }

    /**
     * Sets the endpoint attributes.
     *
     * @param node mProvider/consumer node.
     * @param ep endpoint information.
     */
    public void setEndpoint(
        Node node,
        EndpointInfo ep)
    {
        NamedNodeMap map = node.getAttributes();

        try
        {
            String epname = map.getNamedItem("endpoint-name").getNodeValue();
            String sername = map.getNamedItem("service-name").getNodeValue();
            String intername =
                map.getNamedItem("interface-name").getNodeValue();
            ep.setServiceName(new QName(getNamespace(sername),
                    getLocalName(sername)));
            ep.setInterfaceName(new QName(getNamespace(intername),
                    getLocalName(intername)));
            ep.setEndpointName(epname);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setError(getError() + mTranslator.getString(JMS_LOAD_DD_FAILED)
                + "\n" + e.getMessage());
            setValid(false);
        }
    }

    /**
     * Verifies if the endpoint in the artifacts XML file is present in
     * the deployment descriptor.
     *
     * @param sername  service name.
     * @param intername  interface name.
     * @param epname  endpoint name.
     * @param role  role.
     *
     * @return  true if present.
     */
    public boolean isPresent(
        QName sername,
        QName intername,
        String epname,
        int role)
    {

        for (int ep = 0; ep < mEPList.length; ep++)
        {

            if ((mEPList[ep].getServiceName().toString().equals(sername
                            .toString()))
                    && (
                        mEPList[ep].getInterfaceName().toString().equals(intername
                            .toString())
                    ) && (mEPList[ep].getEndpointName().equals(epname))
                    && (mEPList[ep].getStyle() == role))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the number of mProvider endpoints.
     *
     * @return mProvider endpoint count.
     */
    public int getProviderCount()
    {
        return mNoOfProviders;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Document doc)
    {
        try
        {
            mDoc = doc;
            mDoc.getDocumentElement().normalize();
            load();
        }
        catch (Exception genException)
        {
            mLog.severe(mTranslator.getString(JMS_LOAD_DD_FAILED));
            genException.printStackTrace();
            setException(genException);
            setError(getError() + mTranslator.getString(JMS_LOAD_DD_FAILED)
                + "\n" + genException.getMessage());
            setValid(false);
        }
    }
    
    /**
     * Sets the type of artifact.
     */    
    private void setType(String type)
    {
        mType = type;
    }
    
    /**
     * Gets the type of artifact
     */
    
    public String getType()
    {
        return mType;
    }
    
    private void setArtifacts()
    {
        NodeList namelist = mDoc.getElementsByTagNameNS("*", "artifactstype");

        if (namelist == null)
        {
            /* This means the tag is not present. default type is WSDL20
             */            
            setType("WSDL20");
            return;
        }

        Element name = (Element) namelist.item(0);
        String sValue = null;

        try
        {
            sValue =
                ((Node) (name.getChildNodes().item(0))).getNodeValue().trim();
        }
        catch (NullPointerException ne)
        {
            setType("WSDL20");
            return;
        }
        
        setType(sValue);
    }

    /**
     * Loads the data.
     */
    public void load()
    {
        try
        {
            
            NodeList providers = mDoc.getElementsByTagName("provides");
            mNoOfProviders = providers.getLength();

            NodeList consumers = mDoc.getElementsByTagName("consumes");
            mNoOfConsumers = consumers.getLength();
            mEPList = new EndpointInfo[mNoOfConsumers + mNoOfProviders];
            setArtifacts();
            for (int i = 0; i < mNoOfProviders; i++)
            {
                Node node = providers.item(i);
                EndpointInfo sb = new EndpointInfo();
                setEndpoint(node, sb);
                sb.setProvider();

                if (!isValid())
                {
                    setError(mTranslator.getString(JMS_LOAD_DD_FAILED) + "\n"
                        + getError());

                    return;
                }

                mEPList[i] = sb;
            }

            for (int i = 0; i < mNoOfConsumers; i++)
            {
                Node node = consumers.item(i);
                EndpointInfo sb = new EndpointInfo();
                setEndpoint(node, sb);

                if (!isValid())
                {
                    setError(mTranslator.getString(JMS_LOAD_DD_FAILED) + "\n"
                        + getError());

                    return;
                }

                mEPList[i + mNoOfProviders] = sb;
            }
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(JMS_LOAD_DD_FAILED));
            e.printStackTrace();
            setError(getError() + mTranslator.getString(JMS_LOAD_DD_FAILED)
                + "\n" + e.getMessage());
            setValid(false);
        }
    }

    /**
     * Gets the local name from the quname.
     *
     * @param qname Qualified name of service.
     *
     * @return String local name.
     */
    private String getLocalName(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");

        try
        {
            if (tok.countTokens() == 1)
            {
                return qname;
            }

            tok.nextToken();

            return tok.nextToken();
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * Gets the namespace from the qname.
     *
     * @param qname Qname of service.
     *
     * @return namespace namespace of service.
     */
    private String getNamespace(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");
        String prefix = null;

        try
        {
            if (tok.countTokens() == 1)
            {
                return "";
            }

            prefix = tok.nextToken();

            NamedNodeMap map = mDoc.getDocumentElement().getAttributes();

            for (int j = 0; j < map.getLength(); j++)
            {
                Node n = map.item(j);

                if (n.getLocalName().trim().equals(prefix.trim()))
                {
                    return n.getNodeValue();
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

        return "";
    }

    /**
     * Class which holds the endpoint information.
     *
     * @author Sun Microsystems, Inc.
     */
    public class EndpointInfo
    {
        /**
         * Interface name.
         */
        private QName mInterfaceName;

        /**
         * Service name.
         */
        private QName mServiceName;

        /**
         *  Endpoint name.
         */
        private String mEndpointName;

        /**
         * Provider endpoint.
         */
        private boolean mProvider = false;

        /**
         * Sets the endpoint name.
         *
         * @param epname endpoint name.
         */
        public void setEndpointName(String epname)
        {
            mEndpointName = epname;
        }

        /**
         * Returns the endpoint name.
         *
         * @return endpoint name.
         */
        public String getEndpointName()
        {
            return mEndpointName;
        }

        /**
         * Sets the interface name.
         *
         * @param intername interface name.
         */
        public void setInterfaceName(QName intername)
        {
            mInterfaceName = intername;
        }

        /**
         * Returns the interface name.
         *
         * @return interface name.
         */
        public QName getInterfaceName()
        {
            return mInterfaceName;
        }

        /**
         * Sets the endpoint as mProvider.
         */
        public void setProvider()
        {
            mProvider = true;
        }

        /**
         * Returns true if the endpoint is mProvider.
         *
         * @return true if mProvider endpoint.
         */
        public boolean getProvider()
        {
            return mProvider;
        }

        /**
         * Sets the service name.
         *
         * @param sername service name.
         */
        public void setServiceName(QName sername)
        {
            mServiceName = sername;
        }

        /**
         * Returns the service name.
         *
         * @return the service name.
         */
        public QName getServiceName()
        {
            return mServiceName;
        }

        /**
         * Returns the style of endpoint. 
         *
         * @return  mProvider or consumer.
         */
        public int getStyle()
        {
            if (getProvider())
            {
                return ConfigConstants.PROVIDER;
            }
            else
            {
                return ConfigConstants.CONSUMER;
            }
        }
    }
}
