/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UtilBase.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

/**
 * Base class for all utility. 
 *
 * @author Sun Microsystems Inc.
 */
public class UtilBase
{
    /**
     * Exception.
     */
    private Exception mException;
    /**
     *    Error.
     */
    private String mError = "";
    /**
     *    Warning.
     */
    private String mWarning = "";
    /**
     *  Validity.
     */
    private boolean mValid = true;
    /**
     * Creates a new UtilBase object.
     */
    public UtilBase()
    {
        ;
    }

    /**
     * Returns error.
     *
     * @return 
     */
    public String getError()
    {
        return mError;
    }

    /**
     * Returns exception.
     *
     * @return 
     */
    public Exception getException()
    {
        return mException;
    }

    /**
     * Validity.
     *
     * @return 
     */
    public boolean isValid()
    {
        return mValid;
    }

    /**
     * returns warning.
     *
     * @return 
     */
    public String getWarning()
    {
        return mWarning;
    }

    /**
     * 
     *
     * @param error 
     */
    protected void setError(String error)
    {
        mError = error;
    }

    /**
     * 
     *
     * @param e 
     */
    protected void setException(Exception e)
    {
        mException = e;
    }

    /**
     * 
     *
     * @param valid 
     */
    protected void setValid(boolean valid)
    {
        mValid = valid;
    }

    /**
     * 
     *
     * @param warn 
     */
    protected void setWarning(String warn)
    {
        mWarning = warn;
    }
}
