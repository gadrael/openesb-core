#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jms00005.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "jmsbinding00005.ksh- JMSTest MEPs - jms binding test"

. ./regress_defs.ksh

# create test engine

cd $JV_SVC_TEST_CLASSES/testengine
mkdir testengine
mv *.class testengine
mv payload.xml testengine
jar cvf testengine.jar . 

# now install this test engine

$JV_AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.install.file=$JV_SVC_TEST_CLASSES/testengine/testengine.jar install-component
echo "Starting JMS Test client in background"
# start out tests tests
export PWD
PWD=`pwd`
cd $JV_SVC_TEST_CLASSES

if [ "$FORTE_PORT" != "nt" ] && [ "$FORTE_PORT" != "cygwin" ]; then
$JAVA_HOME/bin/java -classpath .:$JV_AS8BASE/lib/j2ee.jar:$JV_AS8BASE/imq/lib/imq.jar:$JV_AS8BASE/imq/lib/fscontext.jar testclient.DummyClient file:///$JV_SVC_TEST_CLASSES/imqobjectstore &
else
$JAVA_HOME/bin/java -classpath ".;$JV_AS8BASE\lib\j2ee.jar;$JV_AS8BASE\imq\lib\imq.jar;$JV_AS8BASE\imq\lib\fscontext.jar" testclient.DummyClient "file:///$JV_SVC_TEST_CLASSES/imqobjectstore" &
fi

export PID
PID=`echo $!`


echo $PID

cd $PWD
sleep 3
$JV_AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=TESTENGINE_FOR_JMSBINDING start-component

echo "JMSTesting Provider inonly with valid message"
grep "JMSTest-GOODOUTONLY-Passed" $DOMAIN_LOG | wc -l

echo "JMSTesting Provider inout with valid message"
grep "JMSTest-GOODOUTIN-Passed" $DOMAIN_LOG | wc -l

echo "JMSTesting Provider inout with invalid message"
grep "JMSTest-BADOUTIN-Passed" $DOMAIN_LOG | wc -l

# starting in-out test


sleep 7

echo "JMSTesting Consumer inonly with invalid message"
grep "JMSTest-GOODINONLY-Passed" $DOMAIN_LOG | wc -l

echo "JMSTesting Consumer inout with valid message"
grep "JMSTest-GOODINOUT-Passed" $DOMAIN_LOG | wc -l

$JV_AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=TESTENGINE_FOR_JMSBINDING stop-component

$JV_AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=TESTENGINE_FOR_JMSBINDING shut-down-component

$JV_AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=TESTENGINE_FOR_JMSBINDING uninstall-component

# stop shutdown uninstall

echo "Killing the dummy client with pid $PID"
kill -9 $PID

echo "Completed JMSTesting jms binding MEPs"
