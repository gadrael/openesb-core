/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

/**
 * I18n Message Keys for the ProxyBinding.
 * @author Sun Microsystems, Inc
 */
public interface LocalStringKeys
{    
    /** Message. */
    String INIT_START = "INIT_START";
    
    /** Message. */
    String INIT_FAILED = "INIT_FAILED";
    
    /** Message. */
    String SHUTDOWN_BEGIN = "SHUTDOWN_BEGIN";

    /** Message. */
    String SHUTDOWN_END = "SHUTDOWN_END";

    /** Message. */
    String START_BEGIN = "START_BEGIN";

    /** Message. */
    String START_END = "START_END";

    /** Message. */
    String STOP_BEGIN = "STOP_BEGIN";

    /** Message. */
    String STOP_END = "STOP_END";
    
    /** Exception. */
    String DELIVERYCHANNEL_FAILED = "DELIVERYCHANNEL_FAILED";

    /** Exception. */
    String UNKNOWN_CONNECTION_PROVIDER = "UNKNOWN_CONNECTION_PROVIDER";
    
    /** Exception. */
    String NO_CONNECTION_PROVIDER = "NO_CONNECTION_PROVIDER";
    
    /** Exception. */
    String CANT_CREATE_JMS_QUEUESESSION = "CANT_CREATE_JMS_QUEUESESSION";
    
    /** Exception. */
    String CANT_CREATE_CLIENTCONNECTION = "CANT_CREATE_CLIENTCONNECTION";

    /** Exception. */
    String CANT_SEND_CLIENTCONNECTION = "CANT_SEND_CLIENTCONNECTION";

    /** Exception. */
    String CANT_RECEIVE_CLIENTCONNECTION = "CANT_RECEIVE_CLIENTCONNECTION";

    /** Exception. */
    String CANT_CREATE_SERVERCONNECTION = "CANT_CREATE_SERVERCONNECTION";

    /** Exception. */
    String CANT_ACCEPT_SERVERCONNECTION = "CANT_ACCEPT_SERVERCONNECTION";

    /** Exception. */
    String CLOSE_EVENTCONN = "CLOSE_EVENTCONN";
    
    /** Exception. */
    String CLOSE_CLIENTCONN = "CLOSE_CLIENTCONN";
    
    /** Exception. */
    String CLOSE_SERVERCONN = "CLOSE_SERVERCONN";
    
    /** Exception. */
    String MEPINPUT_TRANSFORMER_ERROR = "MEPINPUT_TRANSFORMER_ERROR";

    /** Exception. */
    String MEPINPUT_IO_ERROR = "MEPINPUT_IO_ERROR";

    /** Exception. */
    String MEPINPUT_SERVICEENDPOINT_NOTFOUND = "MEPINPUT_SERVICEENDPOINT_NOTFOUND";

    /** Exception. */
    String MEPINPUT_ME_PROP_ERROR = "MEPINPUT_ME_PROP_ERROR";

    /** Exception. */
    String MEPINPUT_ME_ATTACH_ERROR = "MEPINPUT_ME_ATTACH_ERROR";

    /** Exception. */
    String MEPINPUT_NM_PROP_ERROR = "MEPINPUT_NM_PROP_ERROR";

    /** Exception. */
    String MEPINPUT_START_STREAM_ERROR = "MEPINPUT_START_STREAM_ERROR";

    /** Exception. */
    String MEPINPUT_END_STREAM_ERROR = "MEPINPUT_END_STREAM_ERROR";

    /** Exception. */
    String MEPINPUT_FILL_STREAM_ERROR = "MEPINPUT_FILL_STREAM_ERROR";

    /** Exception. */
    String MEPINPUT_STREAM_SYNC_ERROR = "MEPINPUT_STREAM_SYNC_ERROR";
    
    /** Exception. */
    String MEPINPUT_UNKNOWN_EXCHANGE = "MEPINPUT_UNKNOWN_EXCHANGE";

    /** Exception. */
    String MEPOUTPUT_TRANSFORMER_ERROR = "MEPOUTPUT_TRANSFORMER_ERROR";

    /** Exception. */
    String MEPOUTPUT_IO_ERROR = "MEPOUTPUT_IO_ERROR";

    /** Exception. */
    String MEPOUTPUT_ME_PROP_ERROR = "MEPOUTPUT_ME_PROP_ERROR";

    /** Exception. */
    String MEPOUTPUT_ME_ATTACH_ERROR = "MEPOUTPUT_ME_ATTACH_ERROR";

    /** Exception. */
    String MEPOUTPUT_NM_PROP_ERROR = "MEPOUTPUT_NM_PROP_ERROR";

    /** Exception. */
    String DATASOURCE_NO_INPUT = "DATASOURCE_NO_INPUT";

    /** Exception. */
    String CANT_CONNECT_TO_TARGET = "CANT_CONNECT_TO_TARGET";
            
    /** Exception. */
    String NO_ENDPOINT_AVAILABLE = "NO_ENDPOINT_AVAILABLE";

    /** Stats MBean *. */
    String STATS_MBEAN_REGISTERING = "STATS_MBEAN_REGISTERING";

    /** Stats MBean *. */
    String STATS_MBEAN_CREATION_FAILED = "STATS_MBEAN_CREATION_FAILED";
            
    /** Stats MBean *. */
    String STATS_MBEAN_RESET = "STATS_MBEAN_RESET";

    /** Stats MBean *. */
    String STATS_MBEAN_UPDATE_IN_PROGRESS = "STATS_MBEAN_UPDATE_IN_PROGRESS";
    
    /** EventProcessor/ */
    String EP_EVENT_EXCEPTION = "EP_EVENT_EXCEPTION";
    
    /** EventProcessor/ */
    String EP_EXCEPTION = "EP_EXCEPTION";
    
    /** EventProcessor */
    String EP_REGISTER_EXCEPTION = "EP_REGISTER_EXCEPTION";
    
    /** EventProcessor */
    String EP_CONNECTION_EXCEPTION = "EP_CONNECTION_EXCEPTION";
    
    /** EventProcessor */
    String EP_JOIN_EXCEPTION = "EP_JOIN_EXCEPTION";
    
    /** EventProcessor */
    String EP_SENDTO_EXCEPTION = "EP_SENDTO_EXCEPTION";
    
    /** EventProcessor */
    String EP_SEND_EXCEPTION = "EP_SEND_EXCEPTION";
    
    /** NMRProcessor */
    String NMR_MESSAGING_EXCEPTION = "NMR_MESSAGING_EXCEPTION";
        
    /** NMRProcessor */
    String NMR_EXCEPTION = "NMR_EXCEPTION";
        
    /** ProxyBinding */
    String PROXYBINDING_START_EXCEPTION = "PROXYBINDING_START_EXCEPTION";
    
    /** RemoteProcessor */
    String RP_INIT_EXCEPTION = "RP_INIT_EXCEPTION";
    
    /** RemoteProcessor */
    String RP_MESSAGING_EXCEPTION = "RP_MESSAGING_EXCEPTION";
    
    /** RemoteProcessor */
    String RP_EXCEPTION = "RP_EXCEPTION";
    
}
