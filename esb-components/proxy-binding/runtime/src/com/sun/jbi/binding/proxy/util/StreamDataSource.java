/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StreamDataSource.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.util;

import com.sun.jbi.binding.proxy.LocalStringKeys;

import com.sun.jbi.binding.proxy.util.Translator;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.URI;

import java.util.HashMap;

import javax.activation.DataHandler;
import javax.activation.DataSource;


/** Implementation of an input stream that can deserialize a MEP.
 * @author Sun Microsystems, Inc
 */
public class StreamDataSource
        implements DataSource
{
    String                  mContentType;
    String                  mName;
    ByteArrayOutputStream   mOutput;
    
    public StreamDataSource(String name, String content)
    {
        mName = name;
        mContentType = content;
    }
    
    public InputStream getInputStream()
        throws java.io.IOException
    {
        if (mOutput != null)
        {
            InputStream is = new ByteArrayInputStream(mOutput.toByteArray());

            mOutput =  null;
            return (is);
        }
        throw new java.io.IOException(
            Translator.translate(LocalStringKeys.DATASOURCE_NO_INPUT));
    }
    
    public OutputStream getOutputStream()
        throws java.io.IOException
    {
        return (mOutput = new ByteArrayOutputStream());
    }
    
    public String getContentType()
    {
        return (mContentType);
    }
    
    public String getName()
    {
        return (mName);
    }
}
