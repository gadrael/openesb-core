/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSClientConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.jms;

import com.sun.jbi.binding.proxy.LocalStringKeys;

import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.binding.proxy.connection.ClientConnection;

import javax.jms.BytesMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueSender;
import javax.jms.Session;

/**
 * Connection as a JMS Client.
 * @author Sun Microsystems, Inc
 */
public class JMSClientConnection 
        implements ClientConnection
{
    private MessageProducer     mProducer;
    private BytesMessage        mSendBytes;
    private BytesMessage        mReceiveBytes;
    private String              mId;
    private int                mByteCount;
    
    /** 
     * Creates a new instance of JMSClientConnection
     * @param session to be used to create any JMS resources.
     * @param id to be assigned to this connection.
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    JMSClientConnection(JMSServerConnection reply, Session session, Queue queue) 
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {        
        try
        {
            mId = queue.getQueueName();
            mSendBytes = session.createBytesMessage();
            mSendBytes.setJMSReplyTo(reply.getReplyQueue());
            mProducer = session.createProducer(queue);
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.ConnectionException(
                Translator.translate(LocalStringKeys.CANT_CREATE_CLIENTCONNECTION), jEx);
        }
    }
    
    /**
     * Send array of bytes on the JMS connection.
     * @param bytes to be sent
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public void send(byte[] bytes)
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        try
        {
            mSendBytes.writeBytes(bytes);
            mProducer.send(mSendBytes);
            mSendBytes.clearBody();
            mSendBytes.clearProperties();
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.ConnectionException(
                Translator.translate(LocalStringKeys.CANT_SEND_CLIENTCONNECTION), jEx);
        }
    }
    
    /**
     * Receive array of bytes on the JMS connection.
     * @return bytes to be returned
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public byte[] receive()
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        int         count;
        byte[]      bytes;
        
        //
        //  Create a ME from the byte stream.
        //
        try
        {
            mByteCount = (int)mReceiveBytes.getBodyLength();
            bytes = new byte[mByteCount];
            mReceiveBytes.readBytes(bytes);
            mReceiveBytes = null;
            return (bytes);        
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.ConnectionException(
                Translator.translate(LocalStringKeys.CANT_RECEIVE_CLIENTCONNECTION), jEx);
        }

    }
    
    /**
     * Return the instanceId of this connection.
     * @return String containing instanceId.
     */
    public String getInstanceId()
    {
        return (mId);
    }

    void setMessage(BytesMessage bytes)
    {
        mReceiveBytes = bytes;
    }   
    
    public int receivedByteCount()
    {
        return (mByteCount);
    }
}
