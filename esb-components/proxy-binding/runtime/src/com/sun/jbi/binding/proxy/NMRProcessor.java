/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import com.sun.jbi.binding.proxy.connection.ConnectionManager;
import com.sun.jbi.binding.proxy.connection.ClientConnection;

import com.sun.jbi.binding.proxy.util.MEPOutputStream;
import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.messaging.DeliveryChannel;
import com.sun.jbi.messaging.MessageExchange;

import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NodeList;

/**
 *
 * Runnable class that implements the NMR receiver. All traffic from the NMR that is associated
 * with endpoints for which the ProxyBinding is acting as a proxy, are processed here. Also, the
 * ProxyBinding exposes a handful of services used locally and remotely, and these are handled
 * here. 
 * The basic flow is:
 *      Decide if message is for us or for a proxy.
 *      If the exchange is for us, process it.
 *      Else, lookup the instance that hosts the service.
 *      Send the exchange to the remote instance that is hosting the service.
 *      Remember the exchange if new, forget the exchange is this is the last send.
 *
 * @author Sun Microsystems, Inc
 */
class NMRProcessor
        implements  java.lang.Runnable,
            com.sun.jbi.messaging.EndpointListener,
            com.sun.jbi.messaging.TimeoutListener

                    
{
    private Logger                  mLog;
    private ProxyBinding            mPB;
    private ConnectionManager       mCM;
    private boolean                 mRunning;
    private DeliveryChannel         mChannel;
    private MessageExchangeFactory  mFactory;
    private MEPOutputStream         mMOS;
    
    /**
     * Operation names.
     */
    static final String             ACTIVATE = "Activate";
    static final String             ADDCONNECTION = "AddConnection";
    static final String             DEACTIVATE = "Deactivate";
    static final String             REMOVECONNECTION = "RemoveConnection";
    static final String             SERVICE_DESCRIPTION = "ServiceDescription";
    static final String             RESOLVE_ENDPOINT = "ResolveEndpoint";
    static final String             ISEXCHANGEOKAY = "IsExchangeOkay";
    static final String             ISEXCHANGEOKAY2 = "IsExchangeOkay2";
    static final String             TIMEOUT = "Timeout";

    /**
     * Property names used in above operations.
     */
    static final String             EXCHANGEID = "ExchangeId";
    static final String             TIMEOUTCHECK = "TimeoutCheck";
    static final String             CLASSLOADER = "ClassLoader";
    static final String             SERVICENAME = "ServiceName";
    static final String             SERVICELINKNAME = "ServiceLinkName";
    static final String             ENDPOINTNAME = "EndpointName";
    static final String             ENDPOINTLINKNAME = "EndpointLinkName";
    static final String             LINKTYPE = "LinkType";
    static final String             INSTANCENAME = "InstanceName";
    static final String             EXCHANGE = "Exchange";
    static final String             EXCHANGEOKAY = "ExchangeOkay";
    
    int                             mOperationsReceived;
    int                             mOperationsSent;

    NMRProcessor(ProxyBinding proxyBinding)
    {
         mPB = proxyBinding;
         mLog = mPB.getLogger("nmr");        
         mChannel = mPB.getDeliveryChannel();
         mFactory = mChannel.createExchangeFactoryForService(mPB.getService().getServiceName());
         mCM = mPB.getConnectionManager();
         mRunning = true;
         mChannel.setEndpointListener(this);
         mChannel.setTimeoutListener(this);
    }
    
    void stop()
    {
        mRunning = false;
    }
    
    public void run() 
    {
        MessageExchange         me = null;
        
        //
        // Set up JMS Queue based on our instance-id for inbound messages.
        //
        mLog.fine("PB:NMRProcessor starting.");
        try
        {
            mMOS = new MEPOutputStream(mPB);
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            mLog.fine("PB:NMRProcessor Init MessagingException:" + mEx);
        }

        for (;mRunning;)
        {
            try
            {
                ExchangeEntry           ee;
                ServiceEndpoint         se;
                
                //
                //  Wait for a message exchange.
                //
                me = null;
                me = (MessageExchange)mChannel.accept();
                if (me == null)
                {
                    continue;
                }

                //
                //  See if the ProxyBinding itself is the target of the request.
                //
                if ((se = me.getEndpoint()).getServiceName().getLocalPart().equals(mPB.SUNPROXYBINDINGSERVICE))
                {
                    processProxyServiceRequest(me);
                }
                else
                {
                    //
                    //  See if this is a new or known message exchange.
                    //  A new message is tracked for future use.
                    //
                    String          id = me.getExchangeId();
                    
                    ee = mPB.getExchangeEntry(id);
                    if (ee == null)
                    { 
                        ClientConnection       cc;
                        String                 target;

                        target = mPB.getInstanceForEndpoint(se);
                        if (target == null)
                        {
                            throw new javax.jbi.messaging.MessagingException(
                                Translator.translate(LocalStringKeys.NO_ENDPOINT_AVAILABLE, se.toString()));
                        }                    
                        cc = mCM.getClientConnection(target);                    
                        ee = mPB.trackExchange(id, me, true);
                        ee.setClientConnection(cc);
                        ee.setState(ExchangeEntry.STATE_ONGOING);
                    }
                    else
                    {
                        ee.updateExchange(me);
                    }

                    //
                    //  Send the MEP to its target PB.
                    //
                    writeMEP(ee);
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {
                //
                //  We will get MessagingException from an InterruptedException at shutdown.
                //
                if (mRunning)
                {
                    logExceptionInfo(Translator.translate(LocalStringKeys.NMR_MESSAGING_EXCEPTION), mEx);
                    if (me != null)
                    {
                        handleError(me, mEx);
                    }
                    else
                    {
                        mPB.stop();
                    }
                }
            }
            catch (Exception e)
            {
                logExceptionInfo(Translator.translate(LocalStringKeys.NMR_EXCEPTION), e);
                
                //
                //  Fail fast in the face of unanticipated problems.
                //
                mPB.stop();
            }
        }
    }
    
    void handleError(MessageExchange me, Exception e)
    {
        String              id = me.getExchangeId();
        ExchangeEntry       ee = mPB.getExchangeEntry(id);
        
        //
        //  Ignore this message if we know nothing about it. It could be a stale message from a
        //  restart.
        //
        if (ee != null)
        {
            if (mChannel.activeReference(me))
            {
                //
                //  Send error indication back to the local NMR based component.
                //  This is only possible if the ME is still active (I.E. final send() hasn't happened.)
                //
                try
                {
                    me.setError(e);
                    mChannel.send(me);
                }
                catch (javax.jbi.JBIException jEx)
                {

                }
            }
        
            //
            //  Send error indication back to remote NMR based component.
            //  This is only possible if we have an ongoing conversation with this ME.
            //
            ClientConnection    cc = ee.getClientConnection();

            try
            {
                cc.send(new MEPOutputStream(mPB).writeException(id, e));
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
        mLog.fine("PB:NMRProcessor drop Id(" + id + ") Reason(" + e.toString() + ")");
        mPB.purgeExchange(id);
    }
    
    void logExceptionInfo(String reason, Throwable t)
    {
        StringBuffer                    sb = new StringBuffer();
        java.io.ByteArrayOutputStream   b;
        Throwable                       nextT = t;
        
        sb.append(reason);
        while (t != null)
        {
            if (nextT != null)
            {
                java.io.PrintStream ps = new java.io.PrintStream(b = new java.io.ByteArrayOutputStream());
                t.printStackTrace(ps);
                sb.append(" Exception (");
                sb.append(b.toString());
                sb.append(") ");    
            }
            nextT = t.getCause();
            if (nextT != null)
            {
                sb.append("Caused by: ");
            }
            t = nextT;
        }
        
        mLog.warning(sb.toString());        
    }
    
    void writeMEP(ExchangeEntry ee)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me = ee.getMessageExchange();
        ClientConnection    cc = ee.getClientConnection();
        String              id = me.getExchangeId();
        byte[]             bytes;
        
        //
        //  Send message exchange to target.
        //
        bytes = mMOS.writeMEP(me, ee);
        mLog.fine("PB:NMRProcessor send Id(" + id + ") Target(" 
                + cc.getInstanceId() + ") Bytes(" + bytes.length + ")");
        cc.send(bytes);

        //
        //  Check to see if the Channel has completed all processing
        //  on this message exchange.
        //
        if (!mChannel.activeReference(me))
        {
            mLog.fine("PB:NMRProcessor forget Id(" + id + ")");
            mPB.purgeExchange(id);
        }

    }
     
    void writeIsMEPOk(ExchangeEntry ee)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me = ee.getMessageExchange();
        ClientConnection    cc = ee.getClientConnection();
        String              id = me.getExchangeId();
        byte[]             bytes;
        
        //
        //  Send message exchange to target.
        //
        bytes = mMOS.writeIsMEPOk(ee);
        mLog.fine("PB:NMRProcessor IsMEPOk send Id(" + ee.getRelatedExchange().getExchangeId() + ") Target(" 
                + cc.getInstanceId() + ") Bytes(" + bytes.length + ")");
        cc.send(bytes);

//        //
//        //  Check to see if the Channel has completed all processing
//        //  on this message exchange.
//        //
//        if (!mChannel.activeReference(me))
//        {
//            mLog.info("PB:NMRProcessor forget Id(" + id + ")");
//            mPB.purgeExchange(id);
//        }

    }
    
    void writeMEPOk(ExchangeEntry ee, boolean okay)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me = ee.getMessageExchange();
        ClientConnection    cc = ee.getClientConnection();
        String              id = me.getExchangeId();
        byte[]             bytes;
        
        //
        //  Send message exchange to target.
        //
        bytes = mMOS.writeMEPOk(id, okay);
        mLog.fine("PB:NMRProcessor MEPOk send Id(" + id + ") Target(" 
                + cc.getInstanceId() + ") Bytes(" + bytes.length + ")");
        cc.send(bytes);
        if (!okay)
        {
            mLog.fine("PB:NMRProcessor writeMEPOk forget Id(" + id + ")");
            mPB.purgeExchange(id);           
        }
    }


//
// ------------------- Methods that deal with ProxyBinding services  ------------------
//

    /**
     * Handle any operations that are targeted at the service exposed by the ProxyBinding.
     * @param me containing the messageExchange that should be handled.
     */
    void processProxyServiceRequest(MessageExchange me)
    {
        String  operation = me.getOperation().getLocalPart();
        
        mLog.fine("PB:ProxyBinding Request Operation(" + operation + ")");
        mOperationsReceived++;
        
        //
        //  Determine action needed.
        //
        if (operation.equals(ACTIVATE))
        {
            doActivate(me);
        }
        else if (operation.equals(DEACTIVATE))
        {
            doDeactivate(me);
        }
        else if (operation.equals(ADDCONNECTION))
        {
            doAddConnection(me);
        }
        else if (operation.equals(REMOVECONNECTION))
        {
            doRemoveConnection(me);
        }
        else if (operation.equals(TIMEOUT))
        {
            doTimeout(me);            
        }
        else if (operation.equals(SERVICE_DESCRIPTION))
        {
            doServiceDescription(me);
        }
        else if (operation.equals(RESOLVE_ENDPOINT))
        {
            doResolveEndpoint(me);
        }
        else if (operation.equals(ISEXCHANGEOKAY))
        {
            doIsExchangeOkay(me);
        }
        else if (operation.equals(ISEXCHANGEOKAY2))
        {
            doIsExchangeOkay2(me);
        }
        else
        {
            try
            {
                mLog.fine("PB:ProxyBinding Unknown Operation(" + operation + ")");  
                {
                    me.setStatus(ExchangeStatus.ERROR);
                    mChannel.send(me);
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {
                
            }
        }
    }

    void doActivate(MessageExchange me)
    {
        try
        {
            mPB.addLocalEndpoint((ServiceEndpoint)me.getProperty(ACTIVATE), (ClassLoader)me.getProperty(CLASSLOADER));
            me.setStatus(ExchangeStatus.DONE);
            mChannel.send(me);
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
    
    void doDeactivate(MessageExchange me)
    {
        try
        {
            ServiceEndpoint   er = (ServiceEndpoint)me.getProperty(DEACTIVATE);
            mPB.removeLocalEndpoint(er);
            me.setStatus(ExchangeStatus.DONE);
            mChannel.send(me);
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
    
    void doAddConnection(MessageExchange me)
    {
        try
        {
            mPB.addServiceConnection(
                (QName)me.getProperty(SERVICELINKNAME),
                (String)me.getProperty(ENDPOINTLINKNAME),
                (QName)me.getProperty(SERVICENAME),
                (String)me.getProperty(ENDPOINTNAME),
                (String)me.getProperty(LINKTYPE),
                mPB.getInstanceId());
            me.setStatus(ExchangeStatus.DONE);
            mChannel.send(me);
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
    
    void doRemoveConnection(MessageExchange me)
    {
        try
        {
            mPB.removeServiceConnection(
                (QName)me.getProperty(SERVICELINKNAME),
                (String)me.getProperty(ENDPOINTLINKNAME),
                (QName)me.getProperty(SERVICENAME),
                (String)me.getProperty(ENDPOINTNAME),
                mPB.getInstanceId());
            me.setStatus(ExchangeStatus.DONE);
            mChannel.send(me);
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
    
    void doTimeout(MessageExchange me)
    {
        String              id = (String)me.getProperty(EXCHANGEID);
        ExchangeEntry       ee = mPB.getExchangeEntry(id);
        
        try
        {
            if (me.isRemoteInvocation())
            {                    
                if (me.getRole().equals(MessageExchange.Role.PROVIDER))
                {
                    MessageExchange     oldme;
                    boolean             timedout;

                    mLog.fine("PB:Timeout check Id(" + id + ")");
                    if (ee != null)
                    {
                        oldme = ee.getMessageExchange();
                        if (oldme != null)
                        {
                            me.setProperty(TIMEOUTCHECK, new Boolean(timedout = oldme.checkTimeout()));
                            if (timedout)
                            {
                                //
                                //  Cleanup any ExchangeEntry for the message that has timed out.
                                //
                                mLog.fine("PB:Timeout terminate Id(" + id + ")");
                                oldme.terminate();
                                mPB.purgeExchange(oldme.getExchangeId());
                            }
                        }
                    }
                    me.setStatus(ExchangeStatus.DONE);
                    mChannel.send(me);
                }
                else
                {
                    ee = mPB.getExchangeEntry(me.getExchangeId());
                    if (ee != null)
                    {
                        mLog.fine("PB:Timeout remote  Id(" + id + ") Target(" + ee.getClientConnection().getInstanceId() + ")");
                        writeMEP(ee);
                    }
                }
            }
            else
            {
                ExchangeEntry       tee;

                //
                //  We use the instance determined by the exchange information. This allows the
                //  timeout message to be sent to the same instance as the original request
                //  instead of being subject to a load-balancing decision.
                //
                tee = mPB.trackExchange(me.getExchangeId(), me, true);
                tee.setClientConnection(ee.getClientConnection());
                tee.setState(ExchangeEntry.STATE_ONGOING);
                mLog.fine("PB:Timeout local Id(" + id + ") Target(" + ee.getClientConnection().getInstanceId() + ")");
                writeMEP(tee);
            }
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                mLog.fine("PB:Timeout Id(" + id + ") Exception(" + jEx + ")");
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
    
    void doServiceDescription(MessageExchange me)
    {
        try
        {
            QName               service = (QName)me.getProperty(SERVICENAME);
            String              endpoint = (String)me.getProperty(ENDPOINTNAME);
            ServiceEndpoint     se = ((com.sun.jbi.messaging.DeliveryChannel)mChannel).createEndpoint(service, endpoint);

            if (me.isRemoteInvocation())
            {                    
               if (me.getRole().equals(MessageExchange.Role.PROVIDER))
               {
                    //
                    //  This is an InOut request. We should get a DONE/ERROR back when the request is
                    //  acknowledged. There is nothing to do with the acknowledgement so just return
                    //  (and swallow the message.)
                    //
                    if (me.getStatus().equals(ExchangeStatus.ACTIVE))
                    {
                        Document            document;
                        NormalizedMessage   nm;

                        document = mPB.getComponentContext().getEndpointDescriptor(se);
                        nm = me.createMessage();
                        nm.setContent(new DOMSource(document));
                        ((InOut)me).setOutMessage(nm);
                        mChannel.send(me);
                    }
                }
                else
                {
                    ExchangeEntry   ee = mPB.getExchangeEntry(me.getExchangeId());
                    if (ee != null)
                    {
                        mLog.fine("PB:ServiceDescription Target(" + ee.getClientConnection().getInstanceId() + ")");
                        writeMEP(ee);
                    }
                }
            }
            else                
            {
                ExchangeEntry       ee = mPB.getExchangeEntry(me.getExchangeId());

                //
                //  Select the Instance based on the Service+Endpoint information.
                //  Note: This ME is InOut so we have to correctly handle the DONE message
                //  that most go back to the provoder.
                //
                if (ee == null)
                {
                    ee = mPB.trackExchange(me.getExchangeId(), me, true);
                    ee.setClientConnection(mCM.getClientConnection(mPB.getInstanceForEndpoint(se)));
                    ee.setState(ExchangeEntry.STATE_ONGOING);
                }
                mLog.fine("PB:ServiceDescription Target(" + ee.getClientConnection().getInstanceId() + ")");
                writeMEP(ee);
            }
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                mLog.fine("PB:ServiceDescription Exception(" + jEx + ")");
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
    
    void doResolveEndpoint(MessageExchange me)
    {
        try
        {
            if (me.isRemoteInvocation())
            {                    
                if (me.getRole().equals(MessageExchange.Role.PROVIDER))
                {
                    Document            document = (Document)((DOMSource)((InOnly)me).getInMessage().getContent()).getNode();
                    ServiceEndpoint     se;
                    DocumentFragment    df = document.createDocumentFragment();
                    NodeList            nodes = document.getChildNodes();
                    
                    for (int i = 0; i < nodes.getLength(); )
                    {
                        df.appendChild(nodes.item(i));
                    }
                    se = mPB.getComponentContext().resolveEndpointReference(df);
                    me.setProperty(SERVICENAME, se.getServiceName());
                    me.setProperty(ENDPOINTNAME, se.getEndpointName());
                    me.setStatus(ExchangeStatus.DONE);
                    mChannel.send(me);                    
                }
                else
                {
                    ExchangeEntry   ee = mPB.getExchangeEntry(me.getExchangeId());
                    if (ee != null)
                    {
                        mLog.fine("PB:ResolveEndpoint Target(" + ee.getClientConnection().getInstanceId() + ")");
                        writeMEP(ee);
                    }
                }
            }
            else                
            {
                ExchangeEntry       ee;
                
                ee = mPB.trackExchange(me.getExchangeId(), me, true);
                ee.setClientConnection(mCM.getClientConnection((String)me.getProperty(INSTANCENAME)));
                ee.setState(ExchangeEntry.STATE_ONGOING);
                mLog.fine("PB:ResolveEndpoint Target(" + ee.getClientConnection().getInstanceId() + ")");
                writeMEP(ee);
            }
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                mLog.fine("PB:ResolveEndpoint Exception(" + jEx + ")");
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
   }
    
   void doIsExchangeOkay(MessageExchange me)
   {
        try
        {
            MessageExchange     rme = (MessageExchange)me.getProperty(EXCHANGE);
            ExchangeEntry       ee;
            ClientConnection    cc;

            cc = mCM.getClientConnection((String)me.getProperty(INSTANCENAME));  

            ee = mPB.trackExchange(rme.getExchangeId(), rme, true);
            ee.setRelatedExchange(me);
            ee.setClientConnection(cc);
            ee.setState(ExchangeEntry.STATE_ONGOING);
            mLog.fine("PB:IsExchangeOkay Target(" + ee.getClientConnection().getInstanceId() + ")");
            writeIsMEPOk(ee);
        }
        catch (javax.jbi.JBIException jEx)
        {
        }
  }

   void doIsExchangeOkay2(MessageExchange me)
   {
        try
        {
            if (me.getStatus().equals(ExchangeStatus.ACTIVE))
            {
                MessageExchange     rme = (MessageExchange)me.getProperty(EXCHANGE);
                ExchangeEntry       ee = mPB.getExchangeEntry(rme.getExchangeId());
                
                writeMEPOk(ee, mChannel.isExchangeOkay(rme));
                me.setStatus(ExchangeStatus.DONE);
                mChannel.send(me);
            }
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                mLog.fine("PB:IsExchangeOkay2 Exception(" + jEx + ")");
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);  
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
  }

//
// ------------------- Methods defined in com.sun.jbi.messaging.EndpointListener ------------------
//
     
    /**
     * An endpoint is being activated on the given channel. We process this by sending a
     * InOnly message exchange to the ProxyBinding (remember we are running in the context of a
     * component that is activating an endpoint) with the endpoint information.
     * @param channel that is activating an endpoint.
     * @param endpoint that is being activated.
     */
    public void activate(DeliveryChannel channel, ServiceEndpoint endpoint)
    {
        InOnly      io;
        
        if (channel != mChannel)
        {
            try
            {
                io = mFactory.createInOnlyExchange();
                io.setOperation(new QName(ACTIVATE));
                io.setProperty(ACTIVATE, endpoint);
                io.setProperty(CLASSLOADER, channel.getClassLoader());
                mOperationsSent++;
                channel.sendSync(io);
                if (!io.getStatus().equals(ExchangeStatus.DONE))
                {
                    mLog.fine("PB:ProxyBinding activate failure");
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }

    /**
     * An endpoint is being deactivated on the given channel. We process this by sending a
     * InOnly message exchange to the ProxyBinding (remember we are running in the context of a
     * component that is activating an endpoint) with the endpoint information.
     * @param channel that is deactivating an endpoint.
     * @param endpoint that is being deactivated.
     */
    public void deactivate(DeliveryChannel channel, ServiceEndpoint endpoint)
    {
        InOnly      io;
        
        if (channel != mChannel)
        {
            try
            {
                io = mFactory.createInOnlyExchange();
                io.setOperation(new QName(DEACTIVATE));
                io.setProperty(DEACTIVATE, endpoint);
                mOperationsSent++;
                channel.sendSync(io);
                if (!io.getStatus().equals(ExchangeStatus.DONE))
                {
                    mLog.fine("PB:ProxyBinding deactivate failure");
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
 
    /**
     * An endpoint is being activated on the given channel. We process this by sending a
     * InOnly message exchange to the ProxyBinding (remember we are running in the context of a
     * component that is activating an endpoint) with the endpoint information.
     * @param channel that is activating an endpoint.
     * @param endpoint that is being activated.
     */
    public void addServiceConnection(QName serviceLink, String endpointLink,
            QName service, String endpoint, String linkType)
    {
        InOnly      io;
        
        try
        {
            io = mFactory.createInOnlyExchange();
            io.setOperation(new QName(ADDCONNECTION));
            io.setProperty(SERVICELINKNAME, serviceLink);
            io.setProperty(ENDPOINTLINKNAME, endpointLink);
            io.setProperty(SERVICENAME, service);
            io.setProperty(ENDPOINTNAME, endpoint);
            io.setProperty(LINKTYPE, linkType);
            mOperationsSent++;
            mChannel.sendSync(io);
            if (!io.getStatus().equals(ExchangeStatus.DONE))
            {
                mLog.fine("PB:ProxyBinding addConnection failure");
            }
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            
        }
    }

    /**
     * An endpoint is being deactivated on the given channel. We process this by sending a
     * InOnly message exchange to the ProxyBinding (remember we are running in the context of a
     * component that is activating an endpoint) with the endpoint information.
     * @param channel that is deactivating an endpoint.
     * @param endpoint that is being deactivated.
     */
    public void removeServiceConnection(QName serviceLink, String endpointLink,
            QName service, String endpoint)
    {
        InOnly      io;
        
        try
        {
            io = mFactory.createInOnlyExchange();
            io.setOperation(new QName(REMOVECONNECTION));
            io.setProperty(SERVICELINKNAME, serviceLink);
            io.setProperty(ENDPOINTLINKNAME, endpointLink);
            io.setProperty(SERVICENAME, service);
            io.setProperty(ENDPOINTNAME, endpoint);
            mOperationsSent++;
            mChannel.sendSync(io);
            if (!io.getStatus().equals(ExchangeStatus.DONE))
            {
                mLog.fine("PB:ProxyBinding removeConnection failure");
            }
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            
        }
    }
 
//
// ---------------- Methods defined in com.sun.jbi.messaging.TimeoutListener --------------
//

    /**
     * A message exchange has timed out on a sendSynch operation. We use an InOnly message exchange
     * to convey the request to the ProxyBinding (remember we are running in the context of a
     * component that is waiting in sendSynch().)
     * @param channel that is performing a timeout on an endpoint.
     * @param endpoint that is being timed out.
     */
    public boolean checkTimeout(DeliveryChannel channel, javax.jbi.messaging.MessageExchange exchange)
    {
        InOnly          io;
        boolean         timedout = false;
        
        try
        {
            io = mFactory.createInOnlyExchange();
            io.setOperation(new QName(TIMEOUT));
            io.setProperty(EXCHANGEID, exchange.getExchangeId());
            mOperationsSent++;
            channel.sendSync(io);
            if (io.getStatus().equals(ExchangeStatus.DONE))
            {
                timedout = ((Boolean)io.getProperty(TIMEOUTCHECK)).booleanValue();
                if (timedout)
                {
                    ((com.sun.jbi.messaging.MessageExchange)exchange).terminate();
                    mPB.purgeExchange(exchange.getExchangeId());
                }
            }
            else
            {
                mLog.fine("PB:ProxyBinding timeout failure");
            }
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            
        }
        
        return (timedout);
    }


    /**
     * A service description has been requested for a remote service. We process this by sending a
     * InOnly message exchange to the ProxyBinding (remember we are running in the context of a
     * component that asking for the service description) with the endpoint information.
     * @param endpoint that is being queried for its description
     */
    public Document getDescription(ServiceEndpoint endpoint)
    {
        InOut       io;
        Document    document = null;
        try
        {
            io = mFactory.createInOutExchange();
            io.setOperation(new QName(SERVICE_DESCRIPTION));
            io.setProperty(SERVICENAME, endpoint.getServiceName());
            io.setProperty(ENDPOINTNAME, endpoint.getEndpointName());
            mOperationsSent++;
            mChannel.sendSync(io);
            if (!io.getStatus().equals(ExchangeStatus.ACTIVE))
            {
                mLog.fine("PB:ProxyBinding serviceDescription failure");
            }
            else
            {
                document = (Document)((DOMSource)((InOut)io).getOutMessage().getContent()).getNode();
                io.setStatus(ExchangeStatus.DONE);
                mChannel.send(io);
            }
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            
        }
        
        return (document);
    }
    
    /**
     * An endpoint reference needs to be resolved.  We send this to each known instance and wait for an
     * answer. We process this by sending a InOnly message exchange to the remote ProxyBinding 
     * (remember we are running in the context of a component that is asking for the endpoint resolution)
     * with the endpoint information.
     * @param endpoint that is being queried for its description
     */
    ServiceEndpoint resolveEndpointReference(DocumentFragment document)
    {
        InOnly       io;
        ServiceEndpoint se = null;
        HashMap     instances = mPB.getInstances();
        
        for (Iterator i = instances.keySet().iterator(); i.hasNext(); )
        {
            String      instance = (String)i.next();
            
            //
            //  Ignore ourselves...
            //
            if (instance.equals(mPB.getInstanceId()))
            {
                continue;
            }
            
            try
            {
                NormalizedMessage       nm;
                io = mFactory.createInOnlyExchange();
                nm = io.createMessage();
                nm.setContent(new DOMSource(document));
                io.setInMessage(nm);
                io.setOperation(new QName(RESOLVE_ENDPOINT));
                io.setProperty(INSTANCENAME, instance);
                mOperationsSent++;
                mChannel.sendSync(io);
                if (!io.getStatus().equals(ExchangeStatus.DONE))
                {
                    mLog.fine("PB:ProxyBinding resolveEndpoint failure");
                }
                else
                {
                    if (io.getProperty(SERVICENAME) != null)
                    {
                        se = mChannel.createEndpoint((QName)io.getProperty(SERVICENAME),
                            (String)io.getProperty(ENDPOINTNAME));
                    }
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
            if (se != null)
            {
                break;
            }
        }

        return (se);
    }
    
    /**
     * A provider of a service needs to be queried about the suitability of the consumer request.
     * We process this by sending a InOnly message exchange to the remote ProxyBinding 
     * (remember we are running in the context of a component that is attempting to perform a send to the
     * provider)
     * @param endpoint that is being queried for its description
     */
    boolean isExchangeOkay(ServiceEndpoint endpoint, javax.jbi.messaging.MessageExchange exchange)
    {
        InOnly       io;
        ServiceEndpoint se = null;
        HashMap     instances = mPB.getInstances();
        Boolean     okay;
        
        for (Iterator i = instances.keySet().iterator(); i.hasNext(); )
        {
            String      instance = (String)i.next();
            
            //
            //  Ignore ourselves...
            //
            if (instance.equals(mPB.getInstanceId()))
            {
                continue;
            }
            
            try
            {
                io = mFactory.createInOnlyExchange();
                io.setOperation(new QName(ISEXCHANGEOKAY));
                io.setProperty(INSTANCENAME, instance);
                io.setProperty(EXCHANGE, exchange);
                mOperationsSent++;
                mChannel.sendSync(io);
                if (!io.getStatus().equals(ExchangeStatus.DONE))
                {
                    mLog.fine("PB:ProxyBinding resolveEndpoint failure");
                }
                else
                {
                    if ((okay = (Boolean)io.getProperty(EXCHANGEOKAY)) != null)
                    {
                        return (okay.booleanValue());
                    }
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }

        return (false);
    }
 
}
