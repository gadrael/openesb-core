/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestExchangeIdImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.util;

import com.sun.jbi.messaging.ExchangeIdGenerator;

import java.util.Set;

/**
 * Tests for the ExchangeIdImpl class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestExchangeIdImpl extends junit.framework.TestCase
{
    /** Test instance message */
    private ExchangeIdGenerator mGenerator;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestExchangeIdImpl(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
   }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * testNextId 
     * @throws Exception if an unexpected error occurs
     */
    public void testNextId()
           throws Exception
    {
        ExchangeIdGenerator     g;
        ExchangeIdGenerator     g2;
        String                  id;
        String                  id2;
        String[]                pieces;
        String[]                pieces2;
        
        g = (ExchangeIdGenerator)new ExchangeIdImpl();
        g2 = (ExchangeIdGenerator)new ExchangeIdImpl();
        id = g.nextId();
        id2 = g2.nextId();
        
        pieces = id.split("-");
        pieces2 = id2.split("-");
        
        //
        //  Test that the output has 3 pieces.
        //
        assertTrue(pieces.length == 3);
        assertTrue(pieces2.length == 3);
        
        //
        //  Test that all 3 components are different. There is a slight chance
        //  of this not being true. This mostly effects the last one which only has
        //  8 bits of randomness.
        //
        assertTrue(!pieces[0].equals(pieces2[0]));
        assertTrue(!pieces[1].equals(pieces2[1]));
        assertTrue(!pieces[2].equals(pieces2[2]));
        
    }    
    
    /**
     * testClockReset 
     * @throws Exception if an unexpected error occurs
     */
    public void testClockReset()
           throws Exception
    {
        ExchangeIdImpl          g;
        String                  id;
        String                  id2;
        String[]                pieces;
        String[]                pieces2;
        
        g = new ExchangeIdImpl();
        id = g.nextId();
        g.mLastTimestamp += 1000*60*60*24;
        id2 = g.nextId();
        
        pieces = id.split("-");
        pieces2 = id2.split("-");
        
        //
        //  After detecting a backwards clock, the node should stay the same but
        //  a new sequence number should be generated.
        //
        assertTrue(pieces[0].equals(pieces2[0]));
        assertTrue(!pieces[1].equals(pieces2[1]));
        assertTrue(!pieces[2].equals(pieces2[2]));
   }
    
    /**
     * testSequencing
     * @throws Exception if an unexpected error occurs
     */
    public void testSequencing()
           throws Exception
    {
        ExchangeIdImpl          g;
        String                  id;
        String                  id2;
        String[]                pieces;
        String[]                pieces2;
        
        g = new ExchangeIdImpl();
        id = g.nextId();
        id2 = g.nextId();
        
        pieces = id.split("-");
        pieces2 = id2.split("-");
        
        //
        //  Consecutive number should use the same node and sequence. The timestamp
        //  counter difference should be one except at clock tick boundaries.
        //
        assertTrue(pieces[0].equals(pieces2[0]));
        assertTrue(pieces[1].equals(pieces2[1]));
        assertTrue(!pieces[2].equals(pieces2[2]));
   }

}
