/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestLeaveInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

/**
 * Tests for the LeaveInfo class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestLeaveInfo extends junit.framework.TestCase
{    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestLeaveInfo(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
   }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * testBasics
     * @throws Exception if an unexpected error occurs
     */
    public void testBasics()
           throws Exception
    {
        LeaveInfo          li;
        LeaveInfo          li2;
        EventImpl          e = new EventImpl();
        
        li = new LeaveInfo("a", 1);
        assertEquals(li.getInstanceId(), "a");
        assertEquals(li.getBirthtime(), 1);
        assertEquals(li.getEventName(), LeaveInfo.EVENTNAME);
        li.encodeEvent(e);
        li2 = new LeaveInfo(e);
        assertEquals(li.getInstanceId(), li2.getInstanceId());
        assertEquals(li.getBirthtime(), li2.getBirthtime());
    }
}
