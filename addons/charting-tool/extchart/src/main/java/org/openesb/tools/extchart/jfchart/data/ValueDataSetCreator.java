/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ValueDataSetCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.jfchart.data;


import org.openesb.tools.extchart.exception.ChartException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Logger;
import org.jfree.data.general.Dataset;
import org.jfree.data.general.DefaultValueDataset;

/**
 *
 * @author rdwivedi
 */
public class ValueDataSetCreator extends DataSetCreator{
    
    /** Creates a new instance of ValueDataSetCreator */
     private static Logger mLogger = Logger.getLogger(ValueDataSetCreator.class.getName());
     private DefaultValueDataset  dataset = null;
    public ValueDataSetCreator() {
       
    }
    public Dataset getDefaultDataSet() {
        return dataset;
    }
    public void processResultSet(ResultSet resultSet) throws ChartException {
        dataset  = new DefaultValueDataset();
        
        try {
            if(resultSet.next()){ 
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            if (columnCount > 1 ) {
                throw new ChartException (
                    "Invalid sql generated.  Vlaue data set requires 1 column only."
                );
            }
            
            int columnType = metaData.getColumnType(1);
            switch (columnType) {
                    case Types.NUMERIC:
                    case Types.REAL:
                    case Types.INTEGER:
                    case Types.DOUBLE:
                    case Types.FLOAT:
                    case Types.DECIMAL:
                    case Types.BIGINT:
                        Number value = (Number) resultSet.getObject(1);
                        dataset.setValue(value);
                        break;
                default :
                    throw new ChartException("Data Type "+columnType + "Not Allowed ,please select a number column.");
            }
            }
        } catch(SQLException e) {
            throw new ChartException(e);
        }
    }
    
    public String getQuery() throws ChartException {
        
        return null;
    }
    
    
}
