/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JFChartRenderer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.jfchart;

import org.openesb.tools.extchart.jfchart.data.DataAccess;
import org.openesb.tools.extchart.jsf.ChartListener;
import org.openesb.tools.extchart.jsf.ExtChartComponent;
import org.openesb.tools.extchart.property.ChartDefaults;
import org.openesb.tools.extchart.property.ChartPropertyGroupsBeanBasic;
import org.openesb.tools.extchart.property.JFChartConstants;
import org.openesb.tools.extpropertysheet.IExtPropertyGroup;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;

/**
 *
 * @author rdwivedi
 */
public class JFChartRenderer {
    
    /** Creates a new instance of JFChartRenderer */
    public JFChartRenderer() {
    }
    
    public void encodeBegin(FacesContext context,ExtChartComponent comp) throws IOException {
        ChartPropertyGroupsBeanBasic mPropGroups =(ChartPropertyGroupsBeanBasic) comp.getPropertyGroups();
        DataAccess da = (DataAccess)comp.getDataAccess();
        String uID = comp.getClientId(context) + System.currentTimeMillis();
        // preEncoding(context);
        String imageMapName = "imageMap";
        int width = getWidth(mPropGroups);
        int height = getHeight(mPropGroups);
        ResponseWriter writer = context.getResponseWriter();
        ChartCreator creator = new ChartCreator();
        JFreeChart chart = creator.createJFreeChart(mPropGroups,da);
        ChartRenderingInfo  info = new ChartRenderingInfo(new StandardEntityCollection());
        BufferedImage image = chart.createBufferedImage(width, height, info);
        String imMap = ChartUtilities.getImageMap(imageMapName,info);
        saveImage(image,context,uID);
        writer.startElement("div", comp);
        writer.write("\n");
        writer.write(imMap);
        writer.write("\n");
        writer.startElement("img", comp);
        writer.writeAttribute("id", comp.getClientId(context), null);
        writer.writeAttribute("width", String.valueOf(width), null);
        writer.writeAttribute("height", String.valueOf(height), null);
        writer.writeAttribute("USEMAP" ,"\"#" + imageMapName,null);
        writer.writeAttribute("src", ChartListener.CHART_REQUEST + "?id=" + uID+"&rdm="+Math.random(), null);
        
        
    }
    
    private void saveImage(BufferedImage i, FacesContext context, String id) {
        Map session = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        session.put(id, i);
    }
    
    public void encodeEnd(FacesContext context) throws IOException {
        context.getResponseWriter().endElement("img");
        context.getResponseWriter().endElement("div");
    }
    
    private int getWidth(ChartPropertyGroupsBeanBasic mPropGroups) {
        return getChartDefaults(mPropGroups).getImageWidth();
    }
    
    private int getHeight(ChartPropertyGroupsBeanBasic mPropGroups) {
        return getChartDefaults(mPropGroups).getImageHeight();
    }
    
    public ChartDefaults getChartDefaults(ChartPropertyGroupsBeanBasic grp) {
        IExtPropertyGroup p = grp.getGroupByName(JFChartConstants.CHART_COMMON_PROPERTIES);
        return (ChartDefaults)p;
    }
    
}
