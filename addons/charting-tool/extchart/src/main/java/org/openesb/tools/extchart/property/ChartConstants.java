/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property;

/**
 * ChartConstants.java
 *
 * @author Wei
 * @version :$Revision: 1.4 $
 */

public interface ChartConstants {
    /** IS_3D_KEY is a key to a default. */
    public static final String IS_3D_KEY = "cd_is3d";

    /** IS_INCLUDE_LEGEND_KEY is a key to a default. */
    public static final String IS_INCLUDE_LEGEND_KEY = "cd_includeLegend";

    /** IMAGE_WIDTH_KEY is a key to a default. */
    public static final String IMAGE_WIDTH_KEY = "cd_imageWidth";

    /** IMAGE_HEIGHT_KEY is a key to a default. */
    public static final String IMAGE_HEIGHT_KEY = "cd_imageHeight";

    /** REPORT_WIDTH_KEY is a key to a default. */
    public static final String REPORT_WIDTH_KEY = "cd_reportWidth";

    /** REPORT_HEIGHT_KEY is a key to a default. */
    public static final String REPORT_HEIGHT_KEY = "cd_reportHeight";

    /** Key for legend anchor */
    public static final String LEGEND_ANCHOR_KEY = "legend-anchor";

    /** Key for 3D depth factor */
    public static final String DEPTH_FACTOR_KEY = "depth-factor";

    /** Key for Circular property */
    public static final String CIRCULAR_KEY = "circular";

    /** Key for Direction */
    public static final String DIRECTION_KEY = "direction";

    /** Key for InteriorGap */
    public static final String INTERIOR_GAP_KEY = "interior-gap";

    /** Key for determining if drilldown is enabled */
    public static final String DRILLDOWN_IS_ENABLED_KEY = "cd_drilldown_is_enabled";

    /** Key for determining chart creation format */
    public static final String CHART_FILE_CREATION_FORMAT_TYPE_KEY = "cd_chart_file_creation_format_type";

    /** Key for standard url */
    public static final String DRILLDOWN_CHART_NAME_KEY = "cd_drilldown_chart_name";

    /** Key for display of navigation buttons */
    public static final String DISPLAY_NAVIGATION_BUTTONS_KEY = "cd_display_navigation_buttons";

    /** Key for standard url */
    public static final String NAVIGATION_CHART_NAME_KEY = "cd_navigation_chart_name";

    /** servlet parameter for display of navigation buttons */
    public static final String NAVIGATION_BUTTONS = "displayNavigationButtons";
    /** Key for generation type * /
    public static final String DRILLDOWN_URL_TYPE_KEY = "cd_drilldown_url_type";
    */
    /** Key for letting null custom urls default to standard url * /
    public static final String DRILLDOWN_MULTIPLE_URL_DEFAULTS_TO_SINGLE_URL_KEY = "cd_drilldown_multiple_url_defaults_to_single_url";
    */

    /** DEFAULT_3D is used as a hard default. */
    public static final boolean DEFAULT_3D = true;

    /** DEFAULT_INCLUDE_LEGEND is used as a hard default. */
    public static final boolean DEFAULT_INCLUDE_LEGEND   = false;

    /** DEFAULT_CHART_TITLE is used as a hard default. */
    public static final String  DEFAULT_CHART_TITLE      = "Chart";

    /** DEFAULT_IMAGE_WIDTH is used as a hard default. */
    public static final int DEFAULT_IMAGE_WIDTH           =  680;

    /** DEFAULT_IMAGE_HEIGHT is used as a hard default. */
    public static final int DEFAULT_IMAGE_HEIGHT          =  420;

    /** DEFAULT_REPORT_WIDTH is used as a hard default. */
    public static final int DEFAULT_REPORT_WIDTH          =  680;

    /** DEFAULT_REPORT_HEIGHT is used as a hard default. */
    public static final int DEFAULT_REPORT_HEIGHT         =  420;

    /** DEFAULT_MINIMUM_IMAGE_WIDTH is used as a hard default. */
    public static final int DEFAULT_MINIMUM_IMAGE_WIDTH   =   10;

    /** DEFAULT_MAXIMUM_IMAGE_WIDTH is used as a hard default. */
    public static final int DEFAULT_MAXIMUM_IMAGE_WIDTH   = 2000;

    /** DEFAULT_MINIMUM_IMAGE_HEIGHT is used as a hard default. */
    public static final int DEFAULT_MINIMUM_IMAGE_HEIGHT  =   10;

    /** DEFAULT_MAXIMUM_IMAGE_HEIGHT is used as a hard default. */
    public static final int DEFAULT_MAXIMUM_IMAGE_HEIGHT  = 2000;

    /** DEFAULT_MINIMUM_REPORT_WIDTH is used as a hard default. */
    public static final int DEFAULT_MINIMUM_REPORT_WIDTH  =   10;

    /** DEFAULT_MAXIMUM_REPORT_WIDTH is used as a hard default. */
    public static final int DEFAULT_MAXIMUM_REPORT_WIDTH  = 2000;

    /** DEFAULT_MINIMUM_REPORT_HEIGHT is used as a hard default. */
    public static final int DEFAULT_MINIMUM_REPORT_HEIGHT =   10;

    /** DEFAULT_MAXIMUM_REPORT_HEIGHT is used as a hard default. */
    public static final int DEFAULT_MAXIMUM_REPORT_HEIGHT = 2000;

    /** Default 3D depth factor */
    public static final double DEFAULT_DEPTH_FACTOR = 0.3;

    /** Anchor south */
    public static final int ANCHOR_SOUTH = 0;

    /** Anchor north */
    public static final int ANCHOR_NORTH = 1;

    /** Anchor west */
    public static final int ANCHOR_WEST = 2;

    /** Anchor east */
    public static final int ANCHOR_EAST = 3;

    /** Direction - clockwise */
    public static final int CLOCKWISE = 0;

    /** Direction - anticlockwise */
    public static final int ANTICLOCKWISE = 1;

    /** Default legend anchor */
    public static final int DEFAULT_LEGEND_ANCHOR = ANCHOR_SOUTH;

    /** CHART_FILE_CREATION_FORMAT_JPEG_TYPE is a format for JPEG chart generation */
    public static final int CHART_FILE_CREATION_FORMAT_JPEG_TYPE = 0;
    /** CHART_FILE_CREATION_FORMAT_SVG_TYPE is a format for SVG chart generation */
    public static final int CHART_FILE_CREATION_FORMAT_SVG_TYPE  = 1;
    /** CHART_FILE_CREATION_FORMAT_PNG_TYPE is a format for PNG chart generation */
    public static final int CHART_FILE_CREATION_FORMAT_PNG_TYPE  = 2;
    /** CHART_FILE_CREATION_FORMAT_TYPES is used as a list of supported chart generations formats */
    public static int[] CHART_FILE_CREATION_FORMAT_TYPES = {
        CHART_FILE_CREATION_FORMAT_JPEG_TYPE,
        CHART_FILE_CREATION_FORMAT_SVG_TYPE,
        CHART_FILE_CREATION_FORMAT_PNG_TYPE };
    /** DEFAULT_CHART_FILE_CREATION_FORMAT_TYPE is the default file creation format for a chart */
    public static final int DEFAULT_CHART_FILE_CREATION_FORMAT_TYPE = CHART_FILE_CREATION_FORMAT_JPEG_TYPE;

    /** DEFAULT_DRILLDOWN_IS_ENABLED is the default for drilldown to be used */
    public static final boolean DEFAULT_DRILLDOWN_IS_ENABLED = true;
    /** DEFAULT_DRILLDOWN_CHART_NAME is the default drilldown chart to be used */
    public static final String DEFAULT_DRILLDOWN_CHART_NAME = "";//null;
    /** DEFAULT_DISPLAY_NAVIGATION_BUTTONS is a default */
    public static final boolean DEFAULT_DISPLAY_NAVIGATION_BUTTONS = false;
    /** DEFAULT_NAVIGATION_CHART_NAME is the default navigation chart to be used */
    public static final String DEFAULT_NAVIGATION_CHART_NAME = "";//null;

    /** DRILLDOWN_URL_SINGLE_TYPE is for single specified url, JFreeChart default * /
    public static final int DRILLDOWN_URL_SINGLE_TYPE = 0;
    /** DRILLDOWN_URL_MULTIPLE_TYPE is for multiple specified url * /
    public static final int DRILLDOWN_URL_MULTIPLE_TYPE = 1;
    /** DEFAULT_DRILLDOWN_URL_TYPE is the default type of URL generation to be used * /
    public static final int DEFAULT_DRILLDOWN_URL_TYPE = DRILLDOWN_URL_SINGLE_TYPE;
    /** DEFAULT_DRILLDOWN_MULTIPLE_URL_DEFAULTS_TO_SINGLE_URL is the default URL to be used * /
    public static final boolean DEFAULT_DRILLDOWN_MULTIPLE_URL_DEFAULTS_TO_SINGLE_URL = true;
    */
}
