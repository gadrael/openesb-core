/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartPropertyGroupsBeanBasic.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property;

import org.openesb.tools.extchart.property.bar.BarProperties;
import org.openesb.tools.extchart.property.bar.XYProperties;
import org.openesb.tools.extpropertysheet.IExtProperty;
import org.openesb.tools.extpropertysheet.IExtPropertyGroup;
import org.openesb.tools.extpropertysheet.impl.ExtPropertyGroupsBean;
import java.util.logging.Logger;

/**
 *
 * @author rdwivedi
 */
public class ChartPropertyGroupsBeanBasic extends ExtPropertyGroupsBean {
    
    private static Logger mLogger = Logger.getLogger(ChartPropertyGroupsBeanBasic.class.getName());
    private String cType = null;
    /** Creates a new instance of ChartPropertyGroupsBeanBasic */
    public ChartPropertyGroupsBeanBasic() {
    }
    
    public void repopulate(String chartType) {
        mLogger.info("The chart types is " + chartType);
        ChartDefaults commonProp =(ChartDefaults) getGroupByName(JFChartConstants.CHART_COMMON_PROPERTIES);
        if(commonProp == null){
            commonProp = new ChartDefaults();
            addGroup(commonProp);
        }
        commonProp.setProperty(JFChartConstants.CHART_TYPE,chartType,IExtProperty.HIDDEN_TEXT);
            
        //clear();
        XYProperties props =  (XYProperties) getGroupByName(JFChartConstants.CHART_SPECIFIC_PROPERTIES);
        if(props == null) {
            props = new XYProperties();
            addGroup(props);
        }
        BarProperties bProps =  (BarProperties) getGroupByName(JFChartConstants.CHART_BAR_PROPERTIES);
        if(bProps == null && chartType.equals(JFChartConstants.BAR_CHART)) {
            bProps = new BarProperties();
            addGroup(bProps);
        }
        if(bProps != null && !chartType.equals(JFChartConstants.BAR_CHART)){
            this.removeGroupByName(JFChartConstants.CHART_BAR_PROPERTIES);
        }
        
            
        cType = chartType;
        
    }
    
    public String getChartType(){
        return cType;
    }
    
   
  
}
