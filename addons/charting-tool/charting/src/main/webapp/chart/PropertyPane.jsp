<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" 
xmlns:h="http://java.sun.com/jsf/html" 
xmlns:jsp="http://java.sun.com/JSP/Page" 
xmlns:ui="http://www.sun.com/web/ui"
xmlns:extps="http://open-esb.dev.java.net/extps/extps">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <script><![CDATA[
        function retrieveChartPanel(url) {
	if (window.XMLHttpRequest) { // Non-IE browsers
	    req = new XMLHttpRequest();
	    req.onreadystatechange = chartPanelChanged;
		try {
			netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
		} catch (e) {
			//alert("Permission UniversalBrowserRead denied.");
		}
		try {
			req.open("GET", url, true);
		} catch (e) {
			alert(e);
		}
		req.send(null);
	} else if (window.ActiveXObject) { // IE
		req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req) {
			req.onreadystatechange = chartPanelChanged;
			req.open("GET", url, true);
			req.send();
		}
	}
}

function chartPanelChanged() {
	//displayWait();
	if (req.readyState == 4) { // Complete
		if (req.status == 200) { // OK response
			//ajaxAction();
                        updateChartPanel()
		} else {
			alert("Problem: " + req.statusText);
		}
	}
}

function updateChartPanel() {
	var ajaxResponse = req.responseText;	//.split("\n");
        
	var chartBlockDiv = parent.frames['chartPreviewFrame'].document.getElementById("_imgProp");
        
        //alert("The block id is " + chartBlockDiv);
	chartBlockDiv.innerHTML = ajaxResponse;
	 
}

function updateChart() {
//alert("update chart is called");
//alert("AS" + parent.frames['chartPreviewFrame'].document.getElementById("_imgProp"));
var randomnumber=Math.random()*5;
        retrieveChartPanel("chartPanel.jsp?chartBeanIdentifier=ChartEditorBean&rnd=" +randomnumber);
}

function doNothing() {
}
                         ]]></script>
                 
    <f:view>
        <ui:page binding="#{chart$PropertyPane.page1}" id="page1">
            <ui:html binding="#{chart$PropertyPane.html1}" id="html1">
                <ui:head binding="#{chart$PropertyPane.head1}" id="head1">
                    <ui:link binding="#{chart$PropertyPane.link1}" id="link1" url="/resources/stylesheet.css"/>
                </ui:head>
                
                <ui:body binding="#{chart$PropertyPane.body1}" id="body1" style="-rave-layout: grid">
                    <ui:form binding="#{chart$PropertyPane.form1}" id="form1">
                        <extps:extps  id="id1"   propertyGroups="#{ChartApplicationBeansContainer.currentChartBeanOnEditor.chartProperties}" resourceBundle = "ChartProperties"/> 
                        <script><![CDATA[
                        parent.frames['chartPreviewFrame'].document.location='../chart/ChartPanel.jsp';
                           // parent.frames['chartPreviewFrame'].src="../layout/ChartConfigLayout.jsp";
                        ]]></script>
                    </ui:form>
                </ui:body>
            </ui:html>
        </ui:page>
    </f:view>
</jsp:root>
