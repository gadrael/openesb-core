<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:ui="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <ui:page binding="#{layout$TestPage.page1}" id="page1">
            <ui:html binding="#{layout$TestPage.html1}" id="html1">
                <ui:head binding="#{layout$TestPage.head1}" id="head1">
                    <ui:link binding="#{layout$TestPage.link1}" id="link1" url="/resources/stylesheet.css"/>
                    <ui:meta binding="#{layout$TestPage.meta1}" id="meta1"/>
                </ui:head>
                <ui:body binding="#{layout$TestPage.body1}" id="body1" style="-rave-layout: grid">
                    <ui:form binding="#{layout$TestPage.form1}" id="form1">
                        <ui:pageAlert binding="#{layout$TestPage.pageAlert1}" id="pageAlert1" style="position: absolute; left: 288px; top: 120px" type="question"/>
                        <ui:message binding="#{layout$TestPage.message1}" id="message1" showDetail="false" showSummary="true" style="position: absolute; left: 312px; top: 288px"/>
                        <ui:hyperlink action="#{layout$TestPage.hyperlink1_action}" binding="#{layout$TestPage.hyperlink1}" id="hyperlink1"
                            style="position: absolute; left: 264px; top: 360px" text="testststst"/>
                    </ui:form>
                </ui:body>
            </ui:html>
        </ui:page>
    </f:view>
</jsp:root>
