<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:ui="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <jsp:directive.page import="java.util.logging.Logger,org.openesb.tools.charting.persist.ChartBean,org.openesb.tools.charting.persist.ChartApplicationBeansContainer"/>
    <jsp:scriptlet>
        Logger mLogger = Logger.getLogger("[WebJsp.configure.SelectConfigureeDataset_JSP");
        String cID = request.getParameter("_chartUID"); 
        boolean isNew = true; 
        if(cID == null || cID.length()&lt;1) { 
        } else {
        mLogger.info("CID :" + cID); 
        ChartApplicationBeansContainer container = (ChartApplicationBeansContainer)session.getAttribute("ChartApplicationBeansContainer");
        ChartBean bean = container.getChartBeanByID(cID);
        container.setCurrentChartBeanOnEditor(bean); 
        mLogger.info("The bean is " + bean); 
        if(bean!= null) { 
        if(bean.getChartDataBeanID()== null) 
        { isNew = true; 
        } else { 
        isNew = false; 
        }
        }
        }
        if(isNew) {
    </jsp:scriptlet>
    <f:view>
        <ui:page binding="#{configure$SelectDatasetForChart.page1}" id="page1">
            <ui:html binding="#{configure$SelectDatasetForChart.html1}" id="html1">
                <ui:head binding="#{configure$SelectDatasetForChart.head1}" id="head1">
                    <ui:link binding="#{configure$SelectDatasetForChart.link1}" id="link1" url="/resources/stylesheet.css"/>
                </ui:head>
                <ui:body binding="#{configure$SelectDatasetForChart.body1}" id="body1" style="-rave-layout: grid">
                    <ui:form binding="#{configure$SelectDatasetForChart.form1}" id="form1">
                        <ui:label binding="#{configure$SelectDatasetForChart.label1}" id="label1" style="position: absolute; left: 240px; top: 144px" text="Select Dataset"/>
                        <ui:label binding="#{configure$SelectDatasetForChart.label2}" id="label2" style="position: absolute; left: 240px; top: 216px" text="Select chart type"/>
                        <ui:dropDown binding="#{configure$SelectDatasetForChart.dropDown1}" id="dropDown1"
                            items="#{configure$SelectDatasetForChart.options.options}" onChange="common_timeoutSubmitForm(this.form, 'dropDown1');"
                            style="position: absolute; left: 408px; top: 144px" valueChangeListener="#{configure$SelectDatasetForChart.dropDown1_processValueChange}"/>
                        <ui:dropDown binding="#{configure$SelectDatasetForChart.dropDown2}" id="dropDown2"
                            items="#{configure$SelectDatasetForChart.chartOptions.options}" style="position: absolute; left: 408px; top: 216px"/>
                        <ui:button action="#{configure$SelectDatasetForChart.button1_action}" binding="#{configure$SelectDatasetForChart.button1}" id="button1"
                            style="position: absolute; left: 240px; top: 360px" text="Done"/>
                    </ui:form>
                </ui:body>
            </ui:html>
        </ui:page>
    </f:view>
    <jsp:scriptlet> } else { </jsp:scriptlet>
    <jsp:forward page="../layout/ChartConfigLayout.jsp"/>
    <jsp:scriptlet>}</jsp:scriptlet>
</jsp:root>
