/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartTreeBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.charting;
import org.openesb.tools.charting.exception.ChartingException;
import org.openesb.tools.charting.persist.ChartApplicationBeansContainer;
import org.openesb.tools.charting.persist.ChartBean;
import org.openesb.tools.charting.persist.ChartDataPersistence;
import org.openesb.tools.charting.persist.DataBean;

import com.sun.rave.web.ui.appbase.AbstractPageBean;

import com.sun.web.ui.component.TextField;
import com.sun.web.ui.component.TreeNode;
import com.sun.web.ui.component.Tree;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
 

/**
 *
 * @author rdwivedi
 */
public class ChartTreeBean extends AbstractPageBean implements ValueChangeListener,Validator {
    
    private TreeNode rootNode = null; 
    private static Logger mLogger = Logger.getLogger(ChartTreeBean.class.getName());
    private static String ROOT_IMGSRC = "/images/root.png";  //$NON-NLS-1$
    private static String C_NODE_IMAGE = "/images/chart.png";
    private static String TARGET_FRAME = "workspaceFrame";
    private static String GROUP_IMGSRC = "/images/Charts_group.png";  //$NON-NLS-1$
    
    
    
     
   
   
    /** Creates a new instance of TreeBuilder */
    public ChartTreeBean() {
        
    }
    

     
    public void init() {
        
        super.init();
      
      
    }
    
    /**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a postback request that
     * is processing a form submit.  Customize this method to allocate
     * resources that will be required in your event handlers.</p>
     */
    public void preprocess() {
    }
    
    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a postback and then navigated to a different page).  Customize
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     */
    public void prerender() {
    }
    
    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called (regardless of whether
     * or not this was the page that was actually rendered).  Customize this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     */
    public void destroy() {
    }

    /**
     * <p>Return a reference to the scoped data bean.</p>
     */
    protected RequestBean1 getRequestBean1() {
        return (RequestBean1)getBean("RequestBean1");
    }

    /**
     * <p>Return a reference to the scoped data bean.</p>
     */
    protected SessionBean1 getSessionBean1() {
        return (SessionBean1)getBean("SessionBean1");
    }

    /**
     * <p>Return a reference to the scoped data bean.</p>
     */
    protected ApplicationBean1 getApplicationBean1() {
        return (ApplicationBean1)getBean("ApplicationBean1");
    }
    
    
    private ChartApplicationBeansContainer getChartApplicationBeansContainer() {
        return (ChartApplicationBeansContainer)getBean("ChartApplicationBeansContainer");
        
    }

    /**
     * create a tree node
     * @param String id   - unique node id 
     * @param String name - name of node
     **/
    private TreeNode createNode(String id,String name,String imgsrc,String url,String targetFrame) { 
        TreeNode node = new TreeNode(); 
        node.setId(id); 
        node.setText(name); 
        node.setUrl(url);
        node.setExpanded(true);
        node.setTarget(targetFrame);        
        if ( imgsrc!=null ) {
            node.setImageURL(imgsrc);
        }
        
        return node;  
    } 
    
    
    
    public void addNode(TreeNode parent, TreeNode node) {
        List children = parent.getChildren();
        children.add(node);
    }
    
    /**
     * delete a tree node
     * @param String nodeId   -   node ID
     **/
    public void deleteNode(TreeNode root, String nodeId) { 
        UIComponent node = root.findComponent(nodeId); 
        if (node != null) { 
            UIComponent parent = node.getParent(); 
            List children = parent.getChildren(); 
            children.remove((Object) node); 
        } 
        
    } 

    public TreeNode getRootNode() throws ChartingException {
        mLogger.info("Get root node is called. The root node is " + rootNode);
        if (rootNode == null) { 
            rootNode = new TreeNode(); 
            rootNode.setUrl("#"); 
            rootNode.setText("tree root"); 
            rootNode.setId("root");     //$NON-NLS-1$
            rootNode.setExpanded(true); 
            rootNode.setImageURL(ROOT_IMGSRC);
            buildChartingTree(rootNode);
            TreeNode t = buildDAGroup("Some Name");
            addNode(rootNode,t);
        }         
        return rootNode;
    }
    
    
    private void buildChartingTree(TreeNode root) throws ChartingException {
       mLogger.info("Build tree node for chart called.") ;
       ChartApplicationBeansContainer  appC = getChartApplicationBeansContainer();
       Set s = appC.getAllChartGroups();
       mLogger.info("Build tree node for chart called.2") ;
       if(s != null) {
           Iterator iter = s.iterator();
           while(iter.hasNext()) {
               mLogger.info("Build tree node for chart called.3") ;
               String name =(String) iter.next();
               mLogger.info("Build tree node for chart called.4" + name) ;
               TreeNode cg = buildChartGroup(name);
               addNode(root,cg);
               Map m = appC.getAllChartsForGroup(name);
               if(m!= null) {
                    Iterator iter2 = m.values().iterator();
                    while(iter2.hasNext()) {
                        ChartBean b = (ChartBean)iter2.next();
                        TreeNode n = createNode(b.getChartID(),b.getChartName(),C_NODE_IMAGE,getURL(b),TARGET_FRAME);
                        addNode(cg,n);
                    }
               }
           }
       }
        
        
    }
    
    private TreeNode buildChartGroup(String gName) {
        TreeNode chNode = new TreeNode();
        chNode.setUrl("#"); 
        chNode.setText(gName); 
        chNode.setId("__CG" + gName.replaceAll(" ","_"));     //$NON-NLS-1$
        chNode.setExpanded(true); 
        chNode.setImageURL(GROUP_IMGSRC);
        return chNode;
    }
    private TreeNode buildDAGroup(String gName) {
        ChartApplicationBeansContainer  appC = getChartApplicationBeansContainer();
        gName = "Data Access";
        TreeNode chNode = new TreeNode();
        chNode.setUrl("#"); 
        chNode.setText(gName); 
        chNode.setId("__DAG" + gName.replaceAll(" ","_"));     //$NON-NLS-1$
        chNode.setExpanded(true); 
        chNode.setImageURL(GROUP_IMGSRC);
        Map m = appC.getAllDataBeans();
               if(m!= null) {
                    Iterator iter2 = m.values().iterator();
                    while(iter2.hasNext()) {
                        DataBean b = (DataBean)iter2.next();
                        TreeNode n = createNode(b.getID(),b.getDisplayName(),C_NODE_IMAGE,getURL(b),TARGET_FRAME);
                        addNode(chNode,n);
                    }
               }
        return chNode;
    }
    
    private String getURL(ChartBean b) {
        
        String url = "/faces/configure/processTreeNodeClick.jsp";
        if(b == null) {
            
            //url = "/faces/layout/preChartConfiguration.jsp";
        } else {
           // url = "/faces/layout/preChartConfiguration.jsp?_chartUID="+b.getChartID();
            url += "?_chartUID="+b.getChartID()+"&rdm="+Math.random();
        }
            
        return url;
    }

    
     
    
   public void setRootNode(TreeNode node) {
       mLogger.info("Set root node is called");
       rootNode = node;
    } 
    
    
    public TreeNode findTreeNode(String nodeId) {
        mLogger.warning("The Find TreeNode not implemented");
        TreeNode node = rootNode;
        return node;
    }

    private String createNewChildChartNode(TreeNode n) {
        if(n.getChildCount()== 0) {
            return "New Chart";
        } else {
            int ind = 0;
            Iterator iter =  n.getChildren().iterator();
            while(iter.hasNext()) {
                Object o = iter.next();
                if(o instanceof TreeNode) {
                TreeNode t = (TreeNode)o;
                if(t.getText().startsWith("New Chart")){
                    ind++;
                }
                }
            }
            if(ind == 0) {
                return "New Chart";
            } else {
                return "New Chart_"+ind;
            }
        }
    }
    
    private String createNewParentNodeName(TreeNode n) {
        if(n.getChildCount()== 0) {
            return "Chart Group";
        } else {
            int ind = 0;
            Iterator iter =  n.getChildren().iterator();
            while(iter.hasNext()) {
                Object o = iter.next();
                if(o instanceof TreeNode) {
                TreeNode t = (TreeNode)o;
                if(t.getText().startsWith("Chart Group")){
                    ind++;
                }
                }
            }
            if(ind == 0) {
                return "Chart Group";
            } else {
                return "Chart Group "+ind;
            }
        }
    }
    
    
    public String addNewChartNode() {
        String selectedNodeId = null;
        TreeNode selNode = null;
        if(rootNode.getParent() instanceof Tree) {
            Tree  tr = (Tree)rootNode.getParent();
           // mLogger.info("get Selected object is " +  tr.getCookieSelectedTreeNode());
            selectedNodeId = tr.getCookieSelectedTreeNode();
            //mLogger.info("get Selected is " + selectedNodeId);
        }
        if(selectedNodeId == null) {
            setErrorMessage("Please select a chart group in order to add a new chart.");
        } else {
            selNode = findNodeFromSelectedNodeIDString(selectedNodeId);
            if(selNode == null) {
                setErrorMessage("Error :: The node is not found.");
            } else {
                mLogger.info("Slected node id is " + selNode.getId());
                if(selNode.getId().startsWith("__CG")) {
                    String name = createNewChildChartNode(selNode);
                    //getChartDataPersistence().resetForNewChart(name,(String)selNode.getText());
                    //ChartBean bean = getChartDataPersistence().getChartInfoBean();
                    ChartBean bean = getChartApplicationBeansContainer().createNewChart(name,selNode.getText());
                    
                    TreeNode n = 
                            createNode(bean.getChartID(),bean.getChartName(),C_NODE_IMAGE,
                                getURL(bean),TARGET_FRAME);
                    
                    addNode(selNode,n);
                } else {
                    setErrorMessage("Please select a Chart Group to add a chart.");
                }
            }
        }
        return "success";
        
    }
    
    
    public String addNewChartParentNode() {
        
        // TO DO ::: persist as a secondary schemaorg_apache_xmlbeans.system table in the DB...
        TreeNode selNode = rootNode;
        String name = createNewParentNodeName(selNode);
        TreeNode chNode = new TreeNode();
        chNode.setUrl("#"); 
        chNode.setText(name); 
        chNode.setId("__CG" + selNode.getId()+"_" + selNode.getChildren().size());     //$NON-NLS-1$
        chNode.setExpanded(true); 
        chNode.setImageURL(GROUP_IMGSRC);
        addNode(selNode,chNode);
        return "success";
    }
    
    public String renameSelectedNode() {
        /// TO DO ::: take care of changing the name of chart group to persist for all charts in that group
        // as their parent group name...... currenly not done....
        
        
        
        String selectedNodeId = null;
        TreeNode selNode = null;
        if(rootNode.getParent() instanceof Tree) {
            Tree  tr = (Tree)rootNode.getParent();
            selectedNodeId = tr.getCookieSelectedTreeNode();
            //mLogger.info("get Selected is " + selectedNodeId);
        }
        if(selectedNodeId == null) {
            setErrorMessage("Please select a node to rename.");
        } else {
            selNode = findNodeFromSelectedNodeIDString(selectedNodeId);
        
            if(selNode == null) {
                setErrorMessage("Error :: The node is not found.");
            } if(selNode == rootNode) {
            
            setErrorMessage("This node can not be renamed.");
            
            }  else {
                //mLogger.info("Slected node id is " + selNode.getId());
                //mLogger.info("Slected node image key is " + selNode.getImageKeys().size());
                //mLogger.info("Content facet  " + selNode.getFacet(TreeNode.CONTENT_FACET_KEY));
                //mLogger.info("Image facet  " + selNode.getFacet(TreeNode.IMAGE_FACET_KEY));
               
                //UIComponent comp = selNode.getFacet(TreeNode.IMAGE_FACET_KEY);
                
                TextField field = new TextField();
                
                field.setId("_editTxt");
                field.setDisabled(false);
                field.setVisible(true);
                field.setValue(selNode.getText());
                field.setText(selNode.getText());
                field.setOnBlur("this.form.submit()");
                field.addValueChangeListener(this); 
                field.addValidator(this);
                selNode.setText(" ");
                selNode.setUrl("#");
                
                //MethodBinding binding = getFacesContext().getCurrentInstance().getApplication().createMethodBinding("#{ChartTreeBean.renameTextBoxAction}",null);
                //selNode.setAction(binding);
                
               // comp.getChildren().add(field);
                selNode.getFacets().put(TreeNode.CONTENT_FACET_KEY,field);
                //mLogger.info("The facet is" + comp.getId());
               
            }
        }
        return "success";
    }
    
    
    
    public String removeSelectedNode() {
        
        /// TO DO all of it ....
        mLogger.info("Remove node called ");
        setErrorMessage("remove method is not implemented.");
        return "success";
    }
    
    private void setErrorMessage(String error) {
        ErrorInfoBean infoBean = (ErrorInfoBean)getBean("ErrorInfoBean");
        infoBean.populate("ChartTree",error);
        // Need to implement.
        //mLogger.info("Set Error " + error);
    }
    
    private TreeNode findSelectedNode() {
        String selectedNodeId = null;
        TreeNode selNode = null;
        if(rootNode.getParent() instanceof Tree) {
            Tree  tr = (Tree)rootNode.getParent();
            
            selectedNodeId = tr.getCookieSelectedTreeNode();
            mLogger.info("get Selected is " + selectedNodeId);
        }
        if(selectedNodeId != null) {
            selNode = findNodeFromSelectedNodeIDString(selectedNodeId);
        }
        return selNode;
    } 
    
    private TreeNode findNodeFromSelectedNodeIDString(String str) {
        String[] strs = str.split(":");
        int index = 0;
        for(int i = strs.length ; i > 0  ; i--) {
            if(strs[i-1].equals("rootNode")) {
                index = i-1;
            }
        }
        TreeNode t = null;
        TreeNode temp = null;
        t = rootNode;
        for(int k =  index + 1 ; k < strs.length ; k++) {
            temp = findNodeAmongChildren(t,strs[k]);
            if(temp != null){
                t = temp;
            }
        }
        return t;
    }
    
    private TreeNode findNodeAmongChildren(TreeNode node , String id) {
        
        if(node.getChildCount()>0) {
            Iterator iter = node.getChildren().iterator();
            while(iter.hasNext()){
            UIComponent com = (UIComponent) iter.next();
            if(com.getId().equals(id)) {
                
                return (TreeNode)com;
            }
            }
        }
        return null;
    }
    
    
    
    public void processValueChange(ValueChangeEvent event) {
        UIComponent comp1 = event.getComponent().getParent();  
        TreeNode node =(TreeNode) comp1;
        UIComponent comp = node.getFacet(TreeNode.CONTENT_FACET_KEY);
        TextField field = (TextField)comp;
        String name = (String)field.getText();
        node.getFacets().remove(TreeNode.CONTENT_FACET_KEY);
        node.setText(name);
        ChartBean b = getChartApplicationBeansContainer().getChartBeanByID(node.getId());
        if(b !=  null) {
            b.setChartName(name);
            node.setUrl(this.getURL(b));
        } else {
            node.setUrl("#");
        }
         
         
     
        
    }
    
    
   public void validate(FacesContext context, UIComponent component, Object value) {
        String str = (String)value;
        if(str.startsWith("@")) {
            throw new ValidatorException(new FacesMessage("Not a valid value!"));
        } else {
        UIComponent comp1 = component.getParent();  
        TreeNode node =(TreeNode) comp1;
        UIComponent comp = node.getFacet(TreeNode.CONTENT_FACET_KEY);
       TextField field = (TextField)comp;
        if(((String)field.getText()).equals(str)) {
            // No change so go ahead with component adjustment else let the value change listener do adjustment.
            String name = str;
            node.setText(name);
            node.getFacets().remove(TreeNode.CONTENT_FACET_KEY);
            ChartBean b = getChartApplicationBeansContainer().getChartBeanByID(node.getId());
            if(b !=  null) {
                b.setChartName(name);
                node.setUrl(this.getURL(b));
            } else {
                node.setUrl("#");
            }
        }
        }
        
    }
   
   
   public String addNewDataAccessNode() {
        String selectedNodeId = null;
        TreeNode selNode = null;
        if(rootNode.getParent() instanceof Tree) {
            Tree  tr = (Tree)rootNode.getParent();
            mLogger.info("get Selected object is " +  tr.getCookieSelectedTreeNode());
            selectedNodeId = tr.getCookieSelectedTreeNode();
            mLogger.info("get Selected is " + selectedNodeId);
        }
        if(selectedNodeId == null) {
            setErrorMessage("Please select a Data Access group in order to add a new DA Object.");
        } else {
            selNode = findNodeFromSelectedNodeIDString(selectedNodeId);
            if(selNode == null) {
                setErrorMessage("Error :: The node is not found.");
            } else {
                mLogger.info("Slected node id is " + selNode.getId());
                if(selNode.getId().startsWith("__DAG")) {
                    String name = createNewChildDANode(selNode);
                    //getChartDataPersistence().resetForNewChart(name,(String)selNode.getText());
                    //ChartBean bean = getChartDataPersistence().getChartInfoBean();
                    DataBean bean = getChartApplicationBeansContainer().createNewDataBean(name,selNode.getText());
                    
                    TreeNode n = 
                            createNode(bean.getID(),bean.getDisplayName(),C_NODE_IMAGE,
                                getURL(bean),TARGET_FRAME);
                    
                    addNode(selNode,n);
                } else {
                    setErrorMessage("Please select a DA Group to add a DA Object.");
                }
            }
        }
        return "success";
        
    } 
   
   
   private String createNewChildDANode(TreeNode n) {
        if(n.getChildCount()== 0) {
            return "New Data Access";
        } else {
            int ind = 0;
            Iterator iter =  n.getChildren().iterator();
            while(iter.hasNext()) {
                Object o = iter.next();
                if(o instanceof TreeNode) {
                TreeNode t = (TreeNode)o;
                if(t.getText().startsWith("New Data Access")){
                    ind++;
                }
                }
            }
            if(ind == 0) {
                return "New Data Access";
            } else {
                return "New Data Access_"+ind;
            }
        }
    }
   
   private String getURL(DataBean b) {
        
        String url = "/faces/configure/processTreeNodeClick.jsp";
        if(b == null) {
            
            //url = "/faces/layout/preChartConfiguration.jsp";
        } else {
           // url = "/faces/layout/preChartConfiguration.jsp?_chartUID="+b.getChartID();
            url += "?_daUID="+b.getID()+"&rdm="+Math.random();
        }
            
        return url;
    }
}
