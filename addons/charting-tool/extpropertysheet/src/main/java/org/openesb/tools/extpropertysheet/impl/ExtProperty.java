/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtProperty.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.impl;

import org.openesb.tools.extpropertysheet.IExtProperty;
import org.openesb.tools.extpropertysheet.IExtPropertyOptions;

/**
 *
 * @author rdwivedi
 */
public class ExtProperty implements IExtProperty {
    
    /** Creates a new instance of ExtProperty */
    private String mName = null;
    private Object mValue = null;
    private IExtPropertyOptions mOptions = null;
    private String mValdRule = null;
    private short mType = -1;
    
    public ExtProperty() {
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Object getValue() {
        return mValue;
    }

    public void setValue(Object value) {
        mValue = value;
    }

    public IExtPropertyOptions getOptions() {
        return mOptions;
    }

    public void setOptions(IExtPropertyOptions opts) {
        mOptions = opts;
    }

    public short getType() {
        return mType;
    }

    public void setType(short type) {
        mType = type;
    }

    public String getValidationRule() {
        return mValdRule;
    }

    public void setvalidationRule(String rule) {
        mValdRule = rule;
    }

    public void toXML(StringBuffer buffer) {
        buffer.append("<PROP ");
        buffer.append("Name=").append(getName()).append(" ");
        buffer.append("Value=").append(getValue().toString()).append(" ");
        buffer.append("/>//n");
    }
    
    
    
}
