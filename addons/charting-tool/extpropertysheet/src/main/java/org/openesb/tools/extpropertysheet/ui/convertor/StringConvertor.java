/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StringConvertor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.ui.convertor;

import org.openesb.tools.extpropertysheet.impl.ExtProperty;
 

/**
 *
 * @author rdwivedi
 */
public class StringConvertor implements Convertor {
    
    /** Creates a new instance of StringConvertor */
    public StringConvertor() {
    }
    public String getStringValue(Object obj) {
        
        return (String)obj;
    }
    
    
    public Object getObjectFromString(String obj, short type){
        if(type == ExtProperty.INPUT_INT_TYPE ||type == ExtProperty.INPUT_INT_LIST_TYPE) {
            return new Integer(obj);
        } else if(type == ExtProperty.INPUT_FLOAT_TYPE || type == ExtProperty.INPUT_FLOAT_LIST_TYPE) {
            return new Float(obj);
        } else {
            return obj;
        }
    }
}
