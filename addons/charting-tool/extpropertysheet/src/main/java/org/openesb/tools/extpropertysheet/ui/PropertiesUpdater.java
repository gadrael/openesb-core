/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PropertiesUpdater.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.ui;

import org.openesb.tools.extpropertysheet.IExtProperty;
import org.openesb.tools.extpropertysheet.IExtPropertyGroup;
import org.openesb.tools.extpropertysheet.IExtPropertyGroupsBean;
import org.openesb.tools.extpropertysheet.ui.convertor.PropertyValueConvertor;
import com.sun.web.ui.component.TabSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;

/**
 *
 * @author rdwivedi
 */
public class PropertiesUpdater {
    private static Logger mLogger = Logger.getLogger(PropertiesUpdater.class.getName());
     
    /** Creates a new instance of PropertiesUpdater */
    public PropertiesUpdater() {
    }
     private void populateUIObjectMap(UIComponent comp , HashMap map) {
        if(comp.getChildCount() > 0 ) {
            Iterator iter = comp.getChildren().iterator();
            while(iter.hasNext()) {
                UIComponent compa = (UIComponent)iter.next();
                map.put(compa.getId(),compa);
                populateUIObjectMap(compa,map);
            }
        }
    }
    public void updateChartProperties(TabSet mTS, IExtPropertyGroupsBean b) {
        HashMap propComponents = new HashMap();
        populateUIObjectMap(mTS,propComponents);
        mLogger.info("The size of saved comp " + propComponents.size());
        if(b!= null) {
            Collection col = b.getAllGroups();
            if(col != null) {
                Iterator iter = col.iterator();
                while(iter.hasNext()) {
                    IExtPropertyGroup g = (IExtPropertyGroup ) iter.next();
                    updateChartProperties(g,propComponents);
                }
            }
            
            
        }
    }
    
    private void updateChartProperties(IExtPropertyGroup pg,HashMap propComponents) {
        Collection col = pg.getAllProperties();
        Iterator iter =  col.iterator();
        IExtProperty prop = null;
        while(iter.hasNext()) {
            prop = (IExtProperty ) iter.next();
            String id = prop.getName();
            String valFromReq = null;
            Object obj = propComponents.get(id);
            //mLogger.info("The component recived is " + obj + "  for id " + id);
            if(obj != null) {
                if(obj instanceof UIInput )  {
                    UIInput f = (UIInput)obj;
                    Object o = f.getValue();
                    if(o != null) {
                        valFromReq = f.getValue().toString();
                    }
                    
                    if(valFromReq == null) {
                        mLogger.info("The new value is null so ignoring update");
                    } else {
                        Object val= PropertyValueConvertor.getValueConvertor(prop.getType()).getObjectFromString(valFromReq,prop.getType());
                        prop.setValue(val);
                    }
                }
            }
            mLogger.info("The new value is " + valFromReq + " for comp name = " + id);
            
        }
    }
    
    
}
