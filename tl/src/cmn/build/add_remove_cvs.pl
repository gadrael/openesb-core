#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)add_remove_cvs.pl
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

$SRCROOT = $ENV{SRCROOT};

# Open the log of new dirs
sysopen(NEWDIRS, "$SRCROOT/cvs_repos_logs/cvs_dir_add.list", O_RDONLY) or die $!;
# Open the log of new files
sysopen(NEWFILES, "$SRCROOT/cvs_repos_logs/cvs_file_add.list", O_RDONLY) or die $!;
# Open the log of modified files
sysopen(MODFILES, "$SRCROOT/cvs_repos_logs/cvs_file_modified.list", O_RDONLY) or die $!;
# Open the log of file to remove
sysopen(DELFILES, "$SRCROOT/cvs_repos_logs/cvs_file_remove.list", O_RDONLY) or die $!;


### CAREFUL WITH THIS ONE .. ADDS DIRS
print "adding new cvs directories\n";
while (defined ($line = <NEWDIRS> )) {
 system "cd ${SRCROOT}/cvs_repos_external; $line"; 
# print "cd ${SRCROOT}/cvs_repos_external; $line \n"; 
}
print "\nDONE\n\n";


print "adding new cvs files\n";
while (defined ($line = <NEWFILES> )) 
{
  system "cd ${SRCROOT}/cvs_repos_external; $line"; 
  #print "cd ${SRCROOT}/cvs_repos_external; $line\n"; 
}
print "\nDONE\n\n";

print "removing deleted cvs files\n";
while (defined ($line = <DELFILES> )) 
{
  @tokens = split(/\s+/, $line);
  system "cd ${SRCROOT}/cvs_repos_external; chmod 777 $tokens[4]; rm $tokens[4]";
  system "cd ${SRCROOT}/cvs_repos_external; $line"; 
  #print "cd ${SRCROOT}/cvs_repos_external; rm $tokens[4] \n";
  #print "cd ${SRCROOT}/cvs_repos_external; $line \n"; 
}
print "\nDONE\n\n";
