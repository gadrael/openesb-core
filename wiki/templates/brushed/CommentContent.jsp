<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>

<wiki:TabbedSection defaultTab="addcomment">
  <wiki:Tab id="pagecontent" title="Discussion Page" accesskey="d">
    <wiki:Include page="PageTab.jsp"/>
  </wiki:Tab>
  <wiki:Tab id="addcomment" title="Add a Comment" accesskey="c" >
    <p>
      <label for="text">Please enter your comments below</label>
      <br />
      <i>The comment template is in a page 
         <wiki:EditLink page="CommentTemplate">CommentTemplate</wiki:EditLink> 
         which you can edit too!
      </i>
    </p>
    <wiki:Include page="editors/plain.jsp"/>
  </wiki:Tab>
  <wiki:Tab id="edithelp" title="Help" accesskey="h" >
    <wiki:Include page="HelpTab.jsp"/>
  </wiki:Tab>
</wiki:TabbedSection>