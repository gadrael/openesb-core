<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>

<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>

  <title><wiki:PageName /> - OpenESB: the Open Source ESB for SOA & Integration</title>
  <wiki:Include page="commonheader.jsp"/>
  <wiki:CheckVersion mode="notlatest">
        <meta name="robots" content="noindex,nofollow" />
  </wiki:CheckVersion>
</head>

<body class="view" id="gradient">
<!--
<p id="underline" class="barMargin"></p>
-->
<div id="wikibody" >
  <wiki:Include page="Header.jsp" />



  <wiki:Include page="PageActionsTop.jsp"/>




  <div id="page"><wiki:Content/></div> 


  <wiki:Include page="Favorites.jsp"/> 

  <wiki:Include page="PageActionsBottom.jsp"/>
  
 
  

  <wiki:Include page="Footer.jsp" />
  </div>
  <div style="clear:both; height:0;" > </div>



<!-- Page counters -->
<script type="text/javascript" language="javascript">
var sc_project=1965314; 
var sc_invisible=1; 
var sc_partition=17; 
var sc_security="c5f5efea"; 
</script>
<script type="text/javascript" language="javascript" src="http://www.statcounter.com/counter/counter.js"></script>



<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
_uacct = "UA-993823-3";
urchinTracker();
</script>
</body>
</html>
