<%@ page import="com.ecyrd.jspwiki.tags.InsertDiffTag" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>

<%-- Inserts page content for preview. --%>
<wiki:TabbedSection>
  <wiki:Tab id="diffcontent" title="Page Difference">
    <wiki:PageExists>
    <div class="diffnote">
    <p>
    <form action="<wiki:Link format="url" jsp="Diff.jsp"/>" 
          method="post"  accept-charset="UTF-8">
       <input type="hidden" name="page" value="<wiki:Variable var='pagename' />" />     
    Difference between version
    <select id="r1" name="r1" onchange="this.form.submit();" >
    <% 
       WikiContext c = WikiContext.findContext( pageContext );
       int latestVersion = c.getPage().getVersion();;
       int from = 0;
       from = ((Integer)pageContext.getAttribute(InsertDiffTag.ATTR_OLDVERSION, 
                                               PageContext.REQUEST_SCOPE)).intValue();
       if( from == WikiProvider.LATEST_VERSION ) from = latestVersion;
       for( int i = 1; i <= latestVersion; i++) 
       {
    %> 
       <option value="<%= i %>" <%= ((i==from) ? "selected='selected'" : "") %> ><%= i %></option>
    <%
       }    
    %>
    </select>
    and version
    <select id="r2" name="r2" onchange="this.form.submit();" >
    <% 
       int to = 0;
       to = ((Integer)pageContext.getAttribute(InsertDiffTag.ATTR_NEWVERSION, 
                                               PageContext.REQUEST_SCOPE)).intValue();
       for( int i = 1; i <= latestVersion; i++) 
       {
    %> 
       <option value="<%= i %>" <%= ((i==to) ? "selected='selected'" : "") %> ><%= i %></option>
    <%
       }    
    %>
    </select>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <% if( "ContextualDiffProvider".equals( c.getEngine().getVariable( c, "jspwiki.diffProvider" ) ) ) 
       { 
    %>
         <a class="diff-nextprev" 
            title="Go to first change in this document" 
              href="#change-1">View first change</a>&raquo;&raquo;
    <% } %>
    </form>
    </p>
    <p>Back to <wiki:Link><wiki:PageName/></wiki:Link>, or
    <a href="<wiki:Link format='url'/>&tab=pageinfo&start=<%=from%>"><wiki:PageName/> Info</a></p>
    </div>
    <br />
    <wiki:InsertDiff>
      <i>No difference detected.</i>
    </wiki:InsertDiff> 
    </wiki:PageExists>
    
    <wiki:NoSuchPage>
    <p>
    This page does not exist.  Why don't you go and <wiki:EditLink>create it</wiki:EditLink>?
    </p>
    </wiki:NoSuchPage>
  </wiki:Tab>
</wiki:TabbedSection>
