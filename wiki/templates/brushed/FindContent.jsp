<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="java.util.Collection" %>

<%
  String query = (String)pageContext.getAttribute( 
                   "query",
                   PageContext.REQUEST_SCOPE );
    if( query == null ) query = "";
%>

<wiki:TabbedSection>
<wiki:Tab id="findquery" title="Search Wiki" accesskey="s">

  <form action="<wiki:Link format='url' jsp='Search.jsp'/>"
          name="searchForm2" id="searchForm2" 
accept-charset="<wiki:ContentEncoding />">
      
  <p><label for="query">Enter your query here: </label></p>
  <p><input type="text"   name="query" id="query2" value="<%=query%>" size="40" />
     &nbsp; &nbsp;<input type="checkbox" name="details" id="details" onclick="updateSearchResult()" /> Show details
     <input type="hidden" name="start" id="start" value="0" />
     <input type="hidden" name="maxitems" id="maxitems" value="20" />
     <img id="spin" src="templates/<wiki:TemplateDir/>/images/spin.gif" 
       style="visibility:none; position:absolute;" 
         alt="Search busy" title="Search busy" />
  </p>
  </form>
<script type="text/javascript">
//<![CDATA[    
    function updateSearchResult( )
    {  
      Element.show("spin");
      new Ajax.Updater(
        'searchResult2', 
        '<wiki:Link format="url" templatefile="FindAjax.jsp"/>',
        { 
          asynchronous: true,
          parameters: Form.serialize( 'searchForm2' ),
          onComplete: function() { Element.hide("spin"); GraphBar.onPageLoad(); } 
        } ); 
    }

    Event.observe( window, "load", updateSearchResult, false );
    new Form.Element.Observer( 
      "query2", 1, 
      function() 
      { 
        $("start").value = 0;
        updateSearchResult(); 
      } ); 
//]]>
</script> 
<div id="searchResult2" ><h3>Retrieving search results...</h3></div>

</wiki:Tab>

<wiki:Tab id="findhelp"  title="Help" accesskey="h">
  <wiki:InsertPage page="LuceneHelp"/>
  <wiki:NoSuchPage page="LuceneHelp">
  <p>Use '+' to require a word, '-' to forbid a word.  For example:</p>
  <pre>+java -emacs jsp</pre>
  <p>
  finds pages that MUST include the word "java", and MAY NOT include
  the word "emacs".  Also, pages that contain the word "jsp" are
  ranked before the pages that don't.</p>
  <p>
  All searches are case insensitive.  If a page contains both
  forbidden and required keywords, it is not shown.</p>
  </wiki:NoSuchPage>
</wiki:Tab>

</wiki:TabbedSection>