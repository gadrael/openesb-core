<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>

<wiki:Permission permission="upload">
  <h3>Add new attachment</h3>

  <form action="<wiki:BaseURL/>attach" 
      onsubmit="return WikiForm.submitOnce( this );"
        method="post" accept-charset="<wiki:ContentEncoding/>"
        enctype="multipart/form-data" >

  <%-- Do NOT change the order of wikiname and content, otherwise the 
       servlet won't find its parts. --%>
    <input type="hidden" name="page" value="<wiki:Variable var="pagename"/>">
<%--<input type="file" name="content" style="display: none;">
    <input type="text" name="filename">
    <input type="button" value="Select a File..." 
           onClick="browse.click();filename.value=browse.value;return true;"> 
--%>
    <div id="uploadViewer">
    <input type="file"   name="content"  size="40" onchange="Upload.addFile(this);" >
    </div>
    <p>
    <input type="submit" name="upload"   value="Upload" style="display:none;"/>
    <input type="button" name="proxy1"   value="Upload" onclick="this.form.upload.click();"/>
    <input type="hidden" name="action"   value="upload">
    <input type="hidden" name="nextpage" value="<wiki:UploadLink format="url"/>">
    &nbsp;First select the file(s) to attach, then click "Upload"
    </p>
  </form>
  <wiki:Messages div="error" />
</wiki:Permission>

<wiki:CheckRequestContext context='upload'>
  <p>Back to <wiki:Link><wiki:PageName/></wiki:Link></p>
</wiki:CheckRequestContext>

<wiki:HasAttachments>
<div id="attachmentViewer">
  <div class="list">
    <h3>List of attachments &nbsp;&nbsp;&nbsp;&nbsp;
    </h3>
      <form action='#'>
        <select name="attachSelect" id="attachSelect" size="16" 
            onchange="Attach.showImage(this[this.selectedIndex], '%A4', 300, 300 )" >
        <option value="Attachment Info" selected="selected" >--- Select Image to preview ---</option>
        <wiki:AttachmentsIterator id="att">
          <%-- use %A4 as delimiter:  Name, Link-url, Info-url, Size, Version --%>
          <option value="<wiki:PageName />%A4<wiki:Link format='url' />%A4<wiki:PageInfoLink format='url' />%A4<wiki:PageSize /> bytes%A4<wiki:PageVersion />" >
            <wiki:PageName /> (<wiki:PageSize /> bytes , revision-<wiki:PageVersion />, by&nbsp;<wiki:Author />)
          </option>
        </wiki:AttachmentsIterator>
      </select>
    </form>
    <div class="small">&nbsp;</div>
  </div>  

  <div class="preview">
    <h3>Image preview</h3>
    <div id="attachImg" title="No Image Selected">No Image Selected</div>
    <a href="#" class="small" style="display:none;" id="attachInfo">More info, including version history</a>
  </div>

  <div style="clear:both; height:0px;" > </div>

</div>
</wiki:HasAttachments>
