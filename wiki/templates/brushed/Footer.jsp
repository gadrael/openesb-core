<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.Release" %>

<div id="footer">

<table><tr><td>
  <div class="wikiversion">
    <%=Release.APPNAME%> v<%=Release.getVersionString()%>                       
  </div>
</td><td>
  <div class="rssfeed">
    <wiki:RSSImageLink title="Aggregate the RSS feed" />
  </div>
</td></tr></table>
  <div style="clear:both; height:0px;" > </div>

</div>
  
