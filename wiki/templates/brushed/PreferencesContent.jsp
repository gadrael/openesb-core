<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>

<wiki:TabbedSection defaultTab='<%= request.getParameter( "tab" ) %>'>

  <wiki:Tab id="userPrefs" title="User Preferences" accesskey="p" >
     <wiki:Include page="PreferencesTab.jsp" />
  </wiki:Tab>

  <wiki:Permission permission="editProfile">
  <wiki:Tab id="userProfile" title="User Profile" accesskey="o" >
     <wiki:Include page="ProfileTab.jsp" />
  </wiki:Tab>
  </wiki:Permission>

  <wiki:Permission permission="createGroups">
  <wiki:Tab id="newGroup" title="Group" accesskey="g" >
    <wiki:Include page="GroupTab.jsp" />
  </wiki:Tab>
  </wiki:Permission>

  <wiki:Tab id="removePrefs" title="Remove User Preferences" >
    <form action="<wiki:Variable var='baseURL'/>UserPreferences.jsp"
        onsubmit="document.setCookie( 'JSPWikiUserPrefs', '' ); 
                  return WikiForm.submitOnce( this );" 
          method="POST" accept-charset="<wiki:ContentEncoding />" >
      <h3>Removing User Preferences</h3>
      <div class="formhelp">
      In some cases, you may need to remove the above preferences from the computer.
      Click the button below to do that.  Note that it will remove all user preferences
      you've set up, permanently.  You will need to enter them again.
      </div>
      <p>
      <input type="submit" value="Remove user preferences cookie from this computer" 
             name="clear" style="display:none;"/>
      <input type="button" value="Remove user preferences cookie from this computer" 
             name="proxy1" onclick="this.form.clear.click();" />
      <input type="hidden" name="action" value="clearAssertedName" />
      </p>
    </form>
  </wiki:Tab>

</wiki:TabbedSection>