<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>




<div id="header">
<!--
  <div class="applicationlogo"> 
    <a href="https://open-esb.dev.java.net/public/downloads.html" title="OpenESB Downloads"></a>
  </div>
-->  

<table width="100%" id="headertable"><tr>
  <td><a href="https://open-esb.dev.java.net" style="border: none;"><img style="border: none;" height="60" width="304" alt="Open ESB: the Open Enterprise Service Bus... Open Standards + Open Source" src="images/ESBLogoTag-304x60C.jpg"/></a></td>


  <td id="rhstop">
  

<div id="rhsuser" class="rhsusername">
  <wiki:UserCheck status="anonymous">Not logged in | 
    <wiki:Link page='Login'title="Hi, Anonymous Guest">Login</wiki:Link>
  </wiki:UserCheck>
  <%-- FIXME: asserted user sometimes gets "NULL" for wiki:UserName --%>

  <wiki:UserCheck status="asserted">
    Last logged in as: 
    <a href="<wiki:LinkTo page='UserPreferences' format='url' />"
      title="<wiki:UserName/> (not logged in)"><wiki:UserName/></a> | <wiki:Link jsp="Login.jsp" title="Login or Register new users">Log in</wiki:Link>
  </wiki:UserCheck>

  <wiki:UserCheck status="authenticated">
    Logged in on this wiki as: <wiki:Translate>[<wiki:UserName />]</wiki:Translate>
  </wiki:UserCheck>
</div>  
  
  
<div id="oesearch" style="vertical-align: bottom">
  <br/>
  <br/>
	<form action="https://open-esb.dev.java.net/SearchResults.html" id="cse-search-box"> 
	  <div>
	    <input type="hidden" name="cx" value="013607241216432041017:tcd_nlc0tem" />
	    <input type="hidden" name="cof" value="FORID:11" />
	    <input type="hidden" name="ie" value="UTF-8" />
	    <input type="text" name="q" size="20" />
	    <input type="submit" name="sa" value="Search" />
	  </div>
	</form>
</div>
  
  
  </td>
</tr></table>

<!-- ==================== Menu Structure ==================== -->
<script type="text/javascript" src="https://open-esb.dev.java.net/menu.js" ></script>
<div id="z_menu_02"><table width="100%"><tr><td>
	<div id="z_menu_01">
		<script>writeOEMenu();</script>
	</div>
</td></tr></table></div>
<!-- ==================== End menu ==================== -->










  <div class="companylogo">
  </div>
  
  
<!--
<ul class="iconlist">
<li class="documentation"><a href="http://wiki.open-esb.java.net/Wiki.jsp?page=Documentation"><img src="/openesbwiki/templates/brushed/skins/Smart/images/documentation.png" width="81" height="65"/></a></li>
<li class="link"><a href="http://wiki.open-esb.java.net/Wiki.jsp?page=UsefulLinks"><img src="/openesbwiki/templates/brushed/skins/Smart/images/links.png" width="64" height="65"/></a></li>
<li class="participate"><a href="http://wiki.open-esb.java.net/Wiki.jsp?page=GetInvolved"><img src="/openesbwiki/templates/brushed/skins/Smart/images/participate.png" width="61" height="68"/></a></li>
<li class="downloads"><a href="https://open-esb.dev.java.net/Downloads.html"><img src="/openesbwiki/templates/brushed/skins/Smart/images/downloads.png" width="61" height="65"/></a></li>
</ul>
<div style="clear: left;"></div>
 --> 

  <table width="100%">
  <tr>
  <td><div class="pagename2"><wiki:PageName /></div></td>
  </tr>
  </table>
  


  
  <div class="searchbox"><wiki:Include page="SearchBox.jsp" /></div>

  <div class="breadcrumbs">Your trail: <wiki:Breadcrumbs separator=' &gt; ' /></div>

  <div style="clear:both; height:0;" > </div>

</div>
