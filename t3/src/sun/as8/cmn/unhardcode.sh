#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)unhardcode.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#this script will parameterize a "virgin" glassfish unix installation.
#a virgin install has been unpacked but never started.
#
#USAGE: unhardcode.sh -port {linux|solsparc|macosx} virgin_install_location
#
#HINT:  to setup a new version, install it and tar it up.
#  then in the install location, run "walkdir -ftxt > ../list".
#  then grep for your path name, eg., "grep rtshasta `cat ../list`".
#  from this, you can create a list of files that need to be patched.
#  read this list into the EDIT_LIST variable.
#  DO NOT include files from samples or domains sub-dirs, as they are deleted.
#  RT 4/30/04.
#
#NOTES, 8/20/04
#-you must check out the files in SRC_LIST if you want to update them.
#-we no longer need the server.policy file - this is handled by make_jbiroot
# (via the install_jbi_domain.ant script).
#
#NOTES, 10/15/04
#-we now use asadmin to add JBI lifecycle properties, so we don't need
# to patch domain.xml anymore.
#
#NOTES, 3/28/05
#-we now compile against as8 libs directly, so copies in vl/src are eliminated.
#-script now detects if the install location is an enterprise edition (ee) kit.
# EE kits are tar'd to a separate location.
#
#NOTES, 9/10/06
#-Updated for glassfish 9.1 PE

p=`basename $0`
INSTALL_PORT="$2"
INSTALL_DIR="$3"
if [ "$INSTALL_PORT" = "" -o "$INSTALL_DIR" = "" ]; then
    echo USAGE:  "$p -port {linux|solsparc|macosx} virgin_install_location"
    exit 1
fi

echo INSTALL_PORT=$INSTALL_PORT
echo INSTALL_DIR=$INSTALL_DIR

TMPA=/tmp/${p}_tmpA.$$
TMPB=/tmp/${p}_tmpB.$$

cd "$INSTALL_DIR"

if [ ! -r config/asenv.conf ]; then
    echo $p: cannot open $INSTALL_DIR/config/asenv.conf
    exit 1
fi

. ./config/asenv.conf
PATH="$AS_INSTALL/bin:$PATH"

#sanity check:
if [ "$AS_INSTALL" = "" ]; then
    echo $p: invalid file: $1/config/asenv.conf - AS_INSTALL is not set
    exit 1
else
    rm -rf appserv_uninstall.class domains/domain1 domains/FOODOMAIN samples uninstall 
    mv jbi jbi.gf
fi

echo In `pwd`, AS_INSTALL=$AS_INSTALL

#######
#EE KIT?
#######
if [ -r LICENSE_EE.txt ]; then
    echo $p: detected EE kit
    IS_EE_INSTALL=1
else
    echo $p: detected PE kit
    IS_EE_INSTALL=0
fi

AS_INSTALL_PAT="`echo $AS_INSTALL | sed -e 's#/#\\\\/#g'`"
echo AS_INSTALL_PAT=$AS_INSTALL_PAT

AS_JAVA_PAT="`echo $AS_JAVA | sed -e 's#/#\\\\/#g'`"
echo AS_JAVA_PAT=$AS_JAVA_PAT

#don't add anything from samples or domains, as these are deleted. RT 10/14/04
if [ $IS_EE_INSTALL -eq 1 ]; then
    #EE kit:
    EDIT_LIST="bin/appclient
bin/asadmin
bin/asant
bin/asupgrade
bin/capture-schema
bin/deploytool
bin/jspc
bin/package-appclient
bin/uninstall
bin/verifier
bin/wscompile
bin/wsdeploy
config/asenv.conf
hadb/*/bin/ma.cfg
hadb/*/bin/ma-initd
imq/etc/imqenv.conf
lib/certutil.sh
lib/pk12util.sh
nodeagents/*/agent/bin/startserv
nodeagents/*/agent/bin/stopserv
nodeagents/*/agent/config/nodeagent.properties
pointbase/tools/serveroption/startcommander.sh
pointbase/tools/serveroption/startconsole.sh
pointbase/tools/serveroption/startserver.sh
pointbase/tools/serveroption/stopserver.sh
"
else
    #PE kit:
    EDIT_LIST="bin/appclient
bin/asadmin
bin/asant
bin/asapt
bin/asupgrade
bin/capture-schema
bin/jspc
bin/package-appclient
bin/schemagen
bin/uninstall
bin/verifier
bin/wscompile
bin/wsdeploy
bin/wsgen
bin/wsimport
bin/xjc
config/asenv.conf
imq/etc/imqenv.conf
"
fi

#echo EDIT_LIST=$EDIT_LIST

rm -rf patch
mkdir patch

echo creating parameterized files ...
for fn in $EDIT_LIST
do
    newfn=patch/$fn
    mkdir -p `dirname $newfn`
    sed -e "s/$AS_INSTALL_PAT/{=AS8BASE=}/g;s/$AS_JAVA_PAT/{=JAVA_HOME=}/g" $fn > $newfn
done

echo generating codegen script to patch installation ...
(cd patch; walkdir -f | sed -e 's#./##' > $TMPB)
sed -e 's#.*#patch/&	/&#' $TMPB > patch/as8patch.cg

cat >> patch/as8patch.cg << EOF
#add back jbi files now needed at create-domain time as of glassfish b26:
#copy these file without interpolating codegen macros.  RT 8/31/07
%pragma copy 1
jbi.gf/lib/install/templates/jbi-registry.xml.template   jbi/lib/install/templates/jbi-registry.xml.template
jbi.gf/lib/install/templates/config.properties           jbi/lib/install/templates/config.properties
jbi.gf/components/sun-http-binding/httpbc.jar        jbi/components/sun-http-binding/httpbc.jar
jbi.gf/components/sun-javaee-engine/appserv-jbise.jar    jbi/components/sun-javaee-engine/appserv-jbise.jar
jbi.gf/shared-libraries/sun-wsdl-library/wsdlsl.jar      jbi/shared-libraries/sun-wsdl-library/wsdlsl.jar 
%pragma copy 0

EOF

#######
#update SRCROOT:
#######

if [ $IS_EE_INSTALL -eq 1 ]; then
    SRC_LIST=t3/src/sun/as8/$INSTALL_PORT/gfee.tgz
else
    #SRC_LIST=t3/src/sun/as8/$INSTALL_PORT/gfpe.tgz
    SRC_LIST=t3/src/sun/as8/$INSTALL_PORT/gf${GF_VERSION}.tgz
fi

for fn in $SRC_LIST
do
    dst=$SRCROOT/$fn
    dstdir=`dirname $dst`
    dstfile=`basename $dst`
    mkdir -p $dstdir
    if [ -f $dst -a ! -w $dst ]; then
        bldmsg -warn "'$dst' is not writable, skipping distribution."
        continue
    fi

    case $dst in
    */gf*.tgz )
        bldmsg -p $p Creating parameterized tar file in $dst
        rm -f $dst
        tar czf $dst *
        continue
        ;;
    * )
        bldmsg -p $p -error "'$dst' is not configured for distribution"
        continue
        ;;
    esac

    echo "$from -> $dst"
    cp "$from" "$dst"

done
rm -f $TMPA $TMPB

exit 0
