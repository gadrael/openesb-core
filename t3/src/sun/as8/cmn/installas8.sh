#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)installas8.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#install glassfish using platform specific configuration files

############################### USAGE ROUTINES ################################

usage()
{
    status=$1
    cat << EOF

Usage:  $p [-help] [-ee|-pe] [-noclean] [test_domain...]

 Install the appserver and test domains in \$AS8BASE.
 If no test domains are supplied, installs all test domains:

       domain1, JBITest, ESBTest, CAS, ESBMember
 
Options:
 -help      Display this message.
 -ee        Install enterprise edition.
 -pe        Install personal edition (default).
 -noclean   Do not do a clean install - create domains in existing installation only

Environment:
 \$AS8BASE     base directory installation.
EOF

    exit $status
}

parse_args()
{
    DOHELP=0
    VERBOSE=0
    IS_EE_INSTALL=0
    DOMAIN_ARGS=
    DOCLEAN=1

    while [ $# -gt 0 -a "$1" != "" ]
    do
        arg=$1; shift

        case $arg in
        -h* )
            DOHELP=1
            ;;
        -v* )
            VERBOSE=1
            ;;
        -ee )
            IS_EE_INSTALL=1
            ;;
        -pe )
            IS_EE_INSTALL=0
            ;;
        -noclean )
            DOCLEAN=0
            ;;
        -* )
            echo "${p}: unknown option, $arg"
            usage 1
            ;;
        * )
            if [ -z "$DOMAIN_ARGS" ]; then
                DOMAIN_ARGS=$arg
            else
                DOMAIN_ARGS="$DOMAIN_ARGS $arg"
            fi
            ;;
        esac
    done

    if [ $IS_EE_INSTALL -eq 1 ]; then
        AS8TARFILE=as8ee.tgz
        INSTALL_TYPE="ENTERPRISE EDITION"
    else
        AS8TARFILE=gfpe.tgz
        INSTALL_TYPE="PERSONAL EDITION"
    fi

    if [ $DOCLEAN -eq 1 ]; then
        if [ ! -r "$AS8DIST"/$AS8TARFILE ]; then
            bldmsg -p $p -warn Install package $AS8DIST/$AS8TARFILE is missing, reverting to -noclean.
            DOCLEAN=0
        fi
    fi
}

check_env()
#make sure all the required environment variables are defined
{
    _return_status=0

    if [ x$SRCROOT = x ]; then
        bldmsg -p $p -error SRCROOT is not set.
        _return_status=1
    fi

    if [ x$TOOLROOT = x ]; then
        bldmsg -p $p -error TOOLROOT is not set.
        _return_status=1
    fi

    if [ x$FORTE_PORT = x ]; then
        bldmsg -p $p -error FORTE_PORT is not set.
        _return_status=1
    fi

    return $_return_status
}

################################ MAIN ################################

p=`basename $0`

#set AS8DIST, required by parse_args:
export AS8DIST AS8BASE
AS8DIST=$TOOLROOT/packages/$FORTE_PORT/as8

parse_args "$@"

if [ $DOHELP -eq 1 ]; then
    usage 0
fi

check_env
if [ $? -ne 0 ]; then
    bldmsg -p $p -error One or more environment variables is not set - ABORT.
    usage 1
fi

#make sure appserver bin commands are in our path:
unset PS; PS=':' ; _doscnt=`echo $PATH | grep -c ';'` ; [ $_doscnt -ne 0 ] && PS=';' ; unset _doscnt
export PATH
PATH="$AS8BASE/bin${PS}$PATH"

#echo ${p}: PATH="'$PATH'"

if [ "$AS8BASE" = "" ]; then
    AS8BASE=$SRCROOT/install/as8
fi

export JBI_PORT_OFFSET
if [ "$JBI_PORT_OFFSET" = "" ]; then
    JBI_PORT_OFFSET=`bldhost -userportoffset $JBI_USER_NAME`
    if [ $? -ne 0 ]; then
        JBI_PORT_OFFSET=0
    fi
fi

bldmsg -p $p -markbeg install appserver

if [ -d "$AS8BASE"/config ]; then
    #shutdown any running domains:
    bldmsg -p $p -mark Checking for running domains ...
    shutdownas8
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -p $p -error Could not run shutdownas8 command successfully.  ABORT.
        bldmsg -p $p -markend -status $status install appserver
        exit 1
    fi
else
    bldmsg -p $p INFO: No appserver installation found in "$AS8BASE"
fi

if [ $DOCLEAN -eq 1 ]; then
    bldmsg -p $p Installation type is $INSTALL_TYPE, tar file is $AS8TARFILE
    bldmsg -p $p -mark Removing appserver installation in $AS8BASE
    rm -rf "$AS8BASE"
    #if we could not remove the directory, then abort
    if [ -d "$AS8BASE" -o -r "$AS8BASE" ]; then
        status=1
        bldmsg -p $p -error Cannot remove existing installation directory, $AS8BASE - ABORT.
        bldmsg -p $p -markend -status $status install appserver
        exit 1
    fi

    mkdir -p "$AS8BASE"
    echo "$AS8DIST/$AS8TARFILE -> $AS8BASE"
    (cd "$AS8BASE" && tar xzf "$AS8DIST"/$AS8TARFILE)
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -p $p -error untar of $AS8DIST/$AS8TARFILE into $AS8BASE FAILED -  ABORT.
        bldmsg -p $p -markend -status $status install appserver
        exit 1
    fi

else
    bldmsg -p $p INFO: not cleaning appserver install - initializing jbi test domains only
fi

#if we have not distribution, and no appserver install, then abort:
if [ ! -d "$AS8BASE"/config ]; then
    status=1
    bldmsg -p $p -error Cannot find or create appserver installation in $AS8BASE -  ABORT.
    bldmsg -p $p -markend -status $status install appserver
fi

########
#codegen arguments:
########
verbose_arg=
[ $VERBOSE -eq 1 ] && verbose_arg=-v

#if set,  codegen script will run patch script:
export IS_CLEAN_INSTALL
IS_CLEAN_INSTALL=$DOCLEAN

codegen -u $verbose_arg "$AS8DIST"/fixtree.cg $DOMAIN_ARGS
status=$?
if [ $status -ne 0 ]; then
    bldmsg -p $p -error Installation script $AS8DIST/fixtree.cg FAILED.
    bldmsg -p $p -markend -status $status install appserver
    exit 1
fi

if [ $FORTE_PORT = nt -o $FORTE_PORT = cygwin ]; then
    fixasant
    if [ $? -ne 0 ]; then
        bldmsg -p $p -warn fixasant FAILED, asant may not work on this platform.
    fi
fi

bldmsg -p $p -markend -status $status install appserver
exit $status
