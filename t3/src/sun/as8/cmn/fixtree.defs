#
# BEGIN_HEADER - DO NOT EDIT
# 
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)fixtree.defs - ver 1.1 - 01/04/2006
#
# Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
# 
# END_HEADER - DO NOT EDIT
#

#
#fixtree.defs - definitions shared by port-specific as8 fixtree.cg scripts.
#

#this is just to avoid warning messages:
%ifndef CG_ROOT CG_ROOT = .

SRCROOT = $SRCROOT:nameof:env
%ifndef SRCROOT    %eecho ${CG_INFILE}: SRCROOT is not set.  Cannot proceed.
%ifndef SRCROOT    %halt 1

JBI_PORT_OFFSET = $JBI_PORT_OFFSET:nameof:env
%ifndef JBI_PORT_OFFSET    %eecho ${CG_INFILE}: JBI_PORT_OFFSET is not set.  Cannot proceed.
%ifndef JBI_PORT_OFFSET    %halt 1

#this can be set in the environment:
AS8BASE = $AS8BASE:nameof:env
%ifndef AS8BASE      %eecho WARNING: ${CG_INFILE}: AS8BASE is not set, defaulting to $SRCROOT/install/as8
%ifndef AS8BASE      AS8BASE=$SRCROOT/install/as8

IS_CLEAN_INSTALL = $IS_CLEAN_INSTALL:nameof:env
%ifndef IS_CLEAN_INSTALL      %eecho WARNING: ${CG_INFILE}: IS_CLEAN_INSTALL is not set, defaulting to true (1)
%ifndef IS_CLEAN_INSTALL      IS_CLEAN_INSTALL=1

#this is fixed location relative to toolroot:
AS8DIST = $TOOLROOT/packages/$FORTE_PORT/as8

#NOTE: we nedd AS8BASE in the template path because we also use codegen
#to patch the appserver install after it is expanded.
CG_TEMPLATE_PATH=.;$AS8DIST;$AS8BASE;$TOOLROOT/lib/cmn/templates/java

#set the location of the asadmin password file:
AS_ADMIN_PWFILE = $AS8BASE/passwords
JV_AS_ADMIN_PWFILE = $AS_ADMIN_PWFILE

#######
#cygwin definitions:
#######
IS_CYGWIN = 0
CG_COMPARE_SPEC = cygwin
%if $FORTE_PORT:eq IS_CYGWIN=1

#set shell arg to tell cygpath to take stdin, i.e: echo $AS8BASE | cygpath --path --windows -f -
%if $IS_CYGWIN CG_SHELL_COMMAND_ARGS = --windows -f -

#keep copy of original values:
SV_AS8BASE =   $AS8BASE
SV_JAVA_HOME = $JAVA_HOME

#set path of AS8BASE, JAVA_HOME to be substituted in as8 command & setup scripts:
JV_AS8BASE =   $AS8BASE
JV_JAVA_HOME = $JAVA_HOME
%if $IS_CYGWIN JV_AS8BASE =   $AS8BASE:cygpath
%if $IS_CYGWIN JV_JAVA_HOME = $JAVA_HOME:cygpath

#convert password file arg.  this is supplied to shell version of asadmin, so
#we don't want backslashes:
%if $IS_CYGWIN CG_SHELL_COMMAND_ARGS = --mixed -f -
%if $IS_CYGWIN JV_AS_ADMIN_PWFILE = $AS_ADMIN_PWFILE:cygpath

#### process arguments:
{
    STD_DOMAIN_LIST=
    %push STD_DOMAIN_LIST domain1
    %push STD_DOMAIN_LIST JBITest

    #used by installer tests:
    %push STD_DOMAIN_LIST ESBTest
    #used by jbi-admin tests:
    %push STD_DOMAIN_LIST CAS
    #formerly  used by esb tests - no longer used.  RT 10/12/06
    #%push STD_DOMAIN_LIST ESBMember

    %ifdef  CG_ARGV DOMAIN_LIST = $CG_ARGV
    %ifndef CG_ARGV DOMAIN_LIST = $STD_DOMAIN_LIST

    STD_INSTANCE_LIST=
    %push STD_INSTANCE_LIST instance1
    %push STD_INSTANCE_LIST sync
    %push STD_INSTANCE_LIST CAS-cluster1-inst1
    
    #For now, you cannot specify the instance names in the command line
    #%ifdef  CG_ARGV INSTANCES_LIST = $CG_ARGV
    #%ifndef CG_ARGV INSTANCES_LIST = $STD_INSTANCE_LIST
    INSTANCES_LIST = $STD_INSTANCE_LIST
}

#######
#macros to test domain names and set domain name offsets:
#######

set_domain_offset := << EOF
#set the domain offset, based on domain_name
{
    BAD_DOMAIN_NAME = 0

    CG_COMPARE_SPEC = domain1
    DOMAIN_PORT_OFFSET =	0
    HTML_ADAPTOR_PORT =	8088
    %if $domain_name:eq	%return

    CG_COMPARE_SPEC = JBITest
    DOMAIN_PORT_OFFSET =	1
    %undef HTML_ADAPTOR_PORT
    %if $domain_name:eq	%return

    CG_COMPARE_SPEC = ESBTest
    DOMAIN_PORT_OFFSET =	2
    %undef HTML_ADAPTOR_PORT
    %if $domain_name:eq	%return

    CG_COMPARE_SPEC = CAS
    DOMAIN_PORT_OFFSET =	3
    %undef HTML_ADAPTOR_PORT
    %if $domain_name:eq	%return

    CG_COMPARE_SPEC = ESBMember
    DOMAIN_PORT_OFFSET =	4
    %undef HTML_ADAPTOR_PORT
    %if $domain_name:eq	%return

    BAD_DOMAIN_NAME = 1
}
EOF

check_domain_name := << EOF
#check that the domain name is valid.
#if not, set BAD_DOMAIN_NAME to non-zero
{
    BAD_DOMAIN_NAME = 0

    theDomainName = $domain_name
    %shift domain_name STACK_TMP

    CG_COMPARE_SPEC = domain1
    %if $theDomainName:eq	%return

    CG_COMPARE_SPEC = JBITest
    %if $theDomainName:eq	%return

    CG_COMPARE_SPEC = ESBTest
    %if $theDomainName:eq	%return

    CG_COMPARE_SPEC = CAS
    %if $theDomainName:eq	%return

    CG_COMPARE_SPEC = ESBMember
    %if $theDomainName:eq	%return

    %eecho check_theDomainName: bad domain, "$theDomainName".
    BAD_DOMAIN_NAME = 1
    BAD_DOMAIN_NAMES = 1
}
EOF

check_domain_names := << EOF
#check that all of the domain names are valid.
#if not, set BAD_DOMAIN_NAMES to non-zero
{
    BAD_DOMAIN_NAMES = 0

    STACK_TMP = $DOMAIN_LIST
    %shift domain_name STACK_TMP
    %whiledef domain_name %call check_domain_name
}
EOF

set_instance_offset := << EOF
#set the instance offset, based on instance name
{
    BAD_INSTANCE_NAME = 0

    CG_COMPARE_SPEC = instance1
    INSTANCE_PORT_OFFSET =	4
    %undef HTML_ADAPTOR_PORT
    %if $instance_name:eq	%return

    CG_COMPARE_SPEC = sync
    INSTANCE_PORT_OFFSET =	5
    %undef HTML_ADAPTOR_PORT
    %if $instance_name:eq	%return

    CG_COMPARE_SPEC = CAS-cluster1-inst1
    INSTANCE_PORT_OFFSET =	6
    %undef HTML_ADAPTOR_PORT
    %if $instance_name:eq	%return

    BAD_INSTANCE_NAME = 1
}
EOF

check_instance_name := << EOF
#check that the instance name is valid.
#if not, set BAD_INSTANCE_NAME to non-zero
{
    BAD_INSTANCE_NAME = 0

    theInstanceName = $instance_name
    %shift instance_name STACK_TMP

    CG_COMPARE_SPEC = instance1
    %if $theInstanceName:eq	%return

    CG_COMPARE_SPEC = sync
    %if $theInstanceName:eq	%return

    CG_COMPARE_SPEC = CAS-cluster1-inst1
    %if $theInstanceName:eq	%return

    %eecho check_theInstanceName: bad instance, "$theInstanceName".
    BAD_INSTANCE_NAME = 1
    BAD_INSTANCE_NAMES = 1
}
EOF

check_instance_names := << EOF
#check that all of the instance names are valid.
#if not, set BAD_INSTANCE_NAMES to non-zero
{
    BAD_INSTANCE_NAMES = 0

    STACK_TMP = $INSTANCES_LIST
    %shift instance_name STACK_TMP
    %whiledef instance_name %call check_instance_name
}
EOF


#clean-up:
CG_SHELL_COMMAND_ARGS =
CG_COMPARE_SPEC =

FIXTREE_DEFS_INCLUDED = 1
