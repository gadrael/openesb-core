/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util;

/**
 * This interface contains the property keys used for looking up message
 * text in the LocalStrings resource bundle.
 *
 * IMPORTANT: The number of property keys defined in this file must always
 *            match the number of properties defined LocalStrings_en.properties.
 *            The keys defined in this file must all match the keys for the
 *            properties defined in LocalStrings_en.properties. The junit test
 *            for this class checks all of this and fails with detailed
 *            messages if any mismatches are found.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{
    //
    // Message keys for common messages.
    //

    /**
     * Null argument.
     */
    String NULL_ARGUMENT =
          "NULL_ARGUMENT";

    /**
     * Empty list argument.
     */
    String EMPTY_LIST_ARGUMENT =
          "EMPTY_LIST_ARGUMENT";

    //
    // Message keys for StringTranslator.
    //

    /**
     * Unable to print stack trace.
     */
    String STACK_TRACE_PRINT_FAILED =
          "STACK_TRACE_PRINT_FAILED";

    //
    // Message keys for JMX utilities.
    //

    /**
     * Null object.
     */
    String NULL_OBJECT =
          "NULL_OBJECT";
    /**
     * Invalid interface implementation.
     */
    String INVALID_INTERFACE_IMPL =
          "INVALID_INTERFACE_IMPL";
    /**
     * MBean already registered.
     */
    String MBEAN_ALREADY_REGISTERED =
          "MBEAN_ALREADY_REGISTERED";
    /**
     * MBean not JMX compliant.
     */
    String MBEAN_CREATION_NOT_JMX_COMPLIANT =
          "MBEAN_CREATION_NOT_JMX_COMPLIANT";
    /**
     * Exception during MBean registration.
     */
    String MBEAN_REGISTRATION_EXCEPTION =
          "MBEAN_REGISTRATION_EXCEPTION";
    /**
     * MBean not JMX compliant.
     */
    String MBEAN_REGISTRATION_NOT_JMX_COMPLIANT =
          "MBEAN_REGISTRATION_NOT_JMX_COMPLIANT";
    /**
     * Exception during MBean unregistration.
     */

    String MBEAN_UNREGISTRATION_EXCEPTION =
          "MBEAN_UNREGISTRATION_EXCEPTION";
    
    //
    // ComponentConfigurationHelper messages.
    //
    /**
     * Conversion to open type failed.
     */
    String CCFG_FAILED_CONVERSION_TO_OPEN_TYPE_AR =
        "CCFG_FAILED_CONVERSION_TO_OPEN_TYPE_AR";
    /**
     * Conversion to open type failed.
     */
    String CCFG_FAILED_CONVERSION_TO_OPEN_TYPE =
        "CCFG_FAILED_CONVERSION_TO_OPEN_TYPE";
    /**
     * CompositeData item missing.
     */
    String CCFG_CD_EXTRA_PROPS =
        "CCFG_CD_EXTRA_PROPS";
    
    String CCFG_APP_CFG_ATTR_DEF_MISSING_IN_XML =
        "CCFG_APP_CFG_ATTR_DEF_MISSING_IN_XML";
    
    String CCFG_ATTR_DEF_MISSING_IN_XML =
        "CCFG_ATTR_DEF_MISSING_IN_XML";
    
    String CCFG_APP_CFG_DEF_MISSING_IN_XML =
        "CCFG_APP_CFG_DEF_MISSING_IN_XML";
}
