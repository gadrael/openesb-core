/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StatisticsBaseImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util.monitoring;

import com.sun.jbi.monitoring.StatisticsBase;
import com.sun.jbi.util.jmx.MBeanUtils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.management.ObjectName;

/**
 * This class provides the common utility methods used by all of the statistics
 * gathering classes. The methods here provide the infrastucture for
 * creating an object tree that represents the statistics for the entire JBI
 * instance. Each instance of a statistics class has a parent and optionally
 * one or more children.
 *
 * @author Sun Microsystems, Inc.
 */
public class StatisticsBaseImpl
    implements StatisticsBase
{
    /**
     * Flag to indicate whether statistics gathering is enabled.
     */
    private boolean mEnabled = true;

    /**
     * Key for this statistics object.
     */
    private String mKey;

    /**
     * Parent object in the tree.
     */
    private StatisticsBase mParent;

    /**
     * Synchronized access to child objects.
     */
    private Hashtable mChildren;

    /**
     * JMX Object Name for this MBean.
     */
    private ObjectName mMBeanName;

    /**
     * Constructor.
     * @param key The key string for this statistics object.
     */
    public StatisticsBaseImpl(String key)
    {
        mKey = key;
    }

    /**
     * Get the key of this object.
     * @return the string key for this instance.
     */
    public String getKey()
    {
        return mKey;
    }

    /**
     * Set the parent of this object.
     * @param parent the StatisticsBase instance that is the parent of this
     * instance.
     */
    public void setParent(StatisticsBase parent)
    {
        mParent = parent;
    }

    /**
     * Get the parent of this object.
     * @return the StatisticsBase instance that is the parent of this instance.
     */
    public StatisticsBase getParent()
    {
        return mParent;
    }

    /**
     * Add a child.
     * @param child the StatisticsBase instance to be added to the list of
     * children of this instance.
     */
    public void addChild(StatisticsBase child)
    {
        if ( null == mChildren )
        {
            mChildren = new Hashtable();
        }
        if ( null != child )
        {
            child.setParent(this);
            mChildren.put((Object) child.getKey(), (Object) child);
        }
    }

    /**
     * Get a particular child of this object based on a key.
     * @param key the string key of the requested child.
     * @return the StatisticsBase instance referenced by the specified key,
     * or null if none is found.
     */
    public StatisticsBase getChild(String key)
    {
        StatisticsBase child = null;
        if ( null != mChildren )
        {
            child = (StatisticsBase) mChildren.get(key);
        }
        return child;
    }

    /**
     * Get the list of children of this object.
     * @return the list of instances that are children of this instance.
     */
    public List getChildren()
    {
        if ( null != mChildren )
        {
            return new ArrayList(mChildren.values());
        }
        else
        {
            return new ArrayList();
        }
    }

    /**
     * Test whether or not statistics are enabled.
     * @return true if statistics are enabled, false if not.
     */
    public boolean isEnabled()
    {
        return mEnabled;
    }

    /**
     * Disable statistics.
     */
    public void setDisabled()
    {
        mEnabled = false;
        if ( null != mChildren )
        {
            Iterator c = mChildren.values().iterator();
            StatisticsBase child;
            while ( c.hasNext() )
            {
                child = (StatisticsBase) c.next();
                child.setDisabled();
            }
        }
    }

    /**
     * Enable statistics.
     */
    public void setEnabled()
    {
        mEnabled = true;
        if ( null != mChildren )
        {
            Iterator c = mChildren.values().iterator();
            StatisticsBase child;
            while ( c.hasNext() )
            {
                child = (StatisticsBase) c.next();
                child.setEnabled();
            }
        }
    }

    /**
     * Create and register a StandardMBean for this instance, using the
     * specified interface and MBean ObjectName.
     * @param interfaceClass The interface implemented by the instance.
     * @param mbeanName The MBean ObjectName to use for this MBean.
     * @throws javax.jbi.JBIException If the MBean creation or registration
     * fails.
     */
    public void registerMBean(
        Class interfaceClass,
        Object instance,
        javax.management.ObjectName mbeanName)
        throws javax.jbi.JBIException
    {
        mMBeanName = mbeanName;
        MBeanUtils.registerStandardMBean(interfaceClass, instance,
            mbeanName, true);
    }

    /**
     * Unregister the MBean for this instance.
     * @throws javax.jbi.JBIException If the MBean unregistration fails.
     */
    public void unregisterMBean()
        throws javax.jbi.JBIException
    {
        MBeanUtils.unregisterMBean(mMBeanName);
    }
}
