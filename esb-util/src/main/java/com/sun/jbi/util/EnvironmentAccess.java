/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EnvironmentAccess.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util;

import com.sun.jbi.EnvironmentContext;

/**
 * This class provides a way for any service to obtain a reference to the
 * EnvironmentContext created by the framework. The reason this class is
 * necessary is that classes in the framework service are not directly
 * accessible to other services. This is provided as a convenience for
 * various runtime classes to have easy access to the EnvironmentContext.
 *
 * @author Mark S White
 */
public class EnvironmentAccess
{
    /**
     * EnvironmentContext reference.
     */
    private static EnvironmentContext sContext = null;

    /**
     * Private constructor (never used).
     */
    private EnvironmentAccess() 
    {
    }

    /**
     * Get a reference to the EnvironmentContext.
     * @return the EnvironmentContext or null if the framework has not been
     * initialized.
     */
    public static EnvironmentContext getContext()
    {
        return sContext;
    }

    /**
     * Set a reference to the EnvironmentContext. Note that after the first
     * time this is set, subsequent calls are ignored. Only the constructor
     * in the EnvironmentContext should call this method.
     * @param context the EnvironmentContext reference.
     */
    public static synchronized void setContext(EnvironmentContext context)
    {
        if ( null == sContext )
        {
            sContext = context;
        }
    }
}
