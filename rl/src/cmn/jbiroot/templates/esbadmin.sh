#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)esbadmin.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
# Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
PS="@path.separator@"
java -classpath "@com.sun.jbi.home@/appserver/ant/lib/esb-admin-client.jar${PS}@com.sun.aas.installRoot@/lib/appserv-rt.jar${PS}@com.sun.aas.installRoot@/lib/j2ee.jar${PS}@com.sun.aas.installRoot@/lib/jmxremote.jar${PS}@com.sun.aas.installRoot@/lib/jmxremote_optional.jar${PS}@com.sun.jbi.home@/lib/jbi_rt.jar" ${ESBADMIN_SYSTEM_PROPERTIES} com.sun.jbi.ui.admin.cli.JMXCLI --definitionfile @com.sun.jbi.home@/config/esbcli.xml --defaultport @com.sun.jbi.management.JmxRmiPort@ --defaulthost @com.sun.aas.hostName@ "$@"
exit $?
