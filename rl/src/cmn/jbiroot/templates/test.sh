#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)test.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#test targets in update_jbi_domain.ant from shell
props=$AS8BASE/domains/domain1/jbi/config/jbienv.sh

if [ -r $props ]; then
    . $props
else
    echo "$0:  cannot open $props - has domain1 been started at least once?"
    echo "$0:  SETTING default properties for domain1"
    JBI_HOME=$AS8BASE/jbi
    JBI_DOMAIN_ROOT=$AS8BASE/domains/domain1
    echo JBI_HOME=$JBI_HOME
    echo JBI_DOMAIN_ROOT=$JBI_DOMAIN_ROOT
fi

echo "$0:  CREATING some .DELETE_ME directories"
#create some test dirs:
tstdir=$AS8BASE/domains/domain1/jbi/tmp
mkdir -p $tstdir/dira/dir1/dirx
mkdir -p $tstdir/dira/dir1/diry
mkdir -p $tstdir/dira/dir2
mkdir -p $tstdir/dirb/dirc
touch -f $tstdir/dira/dir1/diry/.DELETE_ME
touch -f $tstdir/dira/dir1/diry/aa
touch -f $tstdir/dira/dir1/diry/bb
touch -f $tstdir/dirb/.DELETE_ME
touch -f $tstdir/dirb/bb
touch -f $tstdir/dirb/bb
touch -f $tstdir/dirb/dirc/cc

#Note:  can't run update_domain task unless we provide all the appserver props.  RT 6/17/05
asant -Dcom.sun.jbi.home=$JBI_HOME -Dcom.sun.jbi.domain.root=$JBI_DOMAIN_ROOT -f update_jbi_domain.ant garbage_collection cleanup
exit $?
