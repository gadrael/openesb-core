#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)commitd.pl
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
{
#
#fsm - programmable finite state machine object
#

use strict;

package fsm;
my $pkgname = __PACKAGE__;

#imports:

#package variables:

sub newbase
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($fsmimpl, $fsmname ,$isDebug, $isVerbose) = @_;
    $fsmname = "BOGUS_FSM" unless defined($fsmname);
    $isDebug = 0 unless defined($isDebug);
    $isVerbose = 0 unless defined($isVerbose);

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        #the object holding implementation routines for this FSM:
        'mFsmImpl' => $fsmimpl,
        #the name of this FSM:
        'mFsmName' => $fsmname,
        #states:
        'mHaltStateName' => 'HaltState',
        'mInitStateName' => 'InitialState',
        'mResetStateName' => 'ResetState',
        #these are symbolic:
        'mAnyStateName' => '*',
        'mNullStateName' => 'NullState',
        #events:
        'mInitEventName' => 'INIT',
        'mHaltEventName' => 'HALT',
        'mResetEventName' => 'RESET',
        'mStartEventName' => 'START',
        'mNullEventName' => 'NULL',
        #following attributes initialized below:
        #current state:
        'mCurrentState' => undef,
        #valid states table:
        'mStates' => {},
        #valid events table:
        'mEvents' => {},
        #transitions table:
        'mTransitions' => {},
        'mEventQueue' => [],
        #feedback:
        'mDebug' => $isDebug,
        'mVerbose' => $isVerbose,
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):
    #initialize current state to null:
    $self->{'mCurrentState'} = $self->nullStateName();
    #seed the "valid states" table:
    $self->{'mStates'} = {
            $self->initStateName() => 1,
            $self->haltStateName() => 1,
            $self->resetStateName() => 1,
        };

    #seed the "valid events" table:
    $self->{'mEvents'} = {
            $self->initEventName() => 1,
            $self->haltEventName() => 1,
            $self->resetEventName() => 1,
        };

    return $self;
}

################################### PACKAGE ####################################
sub isValidEvent
{
    my ($self, $event) = @_;
    my $eref = $self->{'mEvents'};

    return ( defined($$eref{$event}) ? 1 : 0 );
}

sub getValidEventList
{
    my ($self) = @_;
    my $eref = $self->{'mEvents'};

    return ( sort keys %$eref );
}

sub getValidNextEvents
#return the list of valid next events for the current state.
{
    my ($self) = @_;
    my $tref = $self->{'mTransitions'};
    my $curr = $self->currentState();
    my $any = $self->anyStateName();

    #loop thru transitions, match currentState or any state:
    my @events = ();
    my $kk = "";
    for $kk (sort keys %$tref ) {
        my ($kcurr, $kevent) = split($;, $kk);
        push(@events, $kevent) if ($kcurr eq $curr || $kcurr eq $any);
    }

    return ( @events);
}


sub addTransition
{
    my ($self, $curr, $event, $next, $objref, $fname) = @_;
    my $eref = $self->{'mEvents'};
    my $sref = $self->{'mStates'};
    my $tref = $self->{'mTransitions'};
    my $DEBUG = $self->{'mDebug'};

    printf STDERR "addTransition args=(%s)\n", join(',', @_) if ($DEBUG);

    #new events?
    $$eref{$event} = 1 unless defined($$eref{$event});

    #new states ?
    $$sref{$curr} = 1 unless defined($$sref{$curr});
    $$sref{$next} = 1 unless defined($$sref{$next});

    #emit warning if this transition is already defined:
    if (defined($$tref{$curr, $event})) {
        printf STDERR "%s:  WARNING:  overwriting existing transition: ('%s', '%s') -> '%s'\n",
            $self->fsmName(), $curr, $event, $next;
    }


    $$tref{$curr, $event} = {
        'currentState' => $curr,
        'event' => $event,
        'nextState' => $next,
        'stateMethod' => \&{ref($objref) . '::' . $fname},
        'stateObject' => $objref,
    };

    #emit warning if the method ref is not what we expect:
    my $teref = $$tref{$curr, $event};    #teref => transition entry
    if ( ref($$teref{'stateMethod'} ne "CODE") ) {
        printf STDERR "%s:  WARNING:  ref(%s) is '%s' instead of 'CODE' for transition: ('%s', '%s') -> '%s'\n",
            $self->fsmName(), $fname, ref($$teref{'stateMethod'}), $curr, $event, $next;
    }

    #allow chaining:
    return $self;
}

sub processEvents
#returns 0 unless error
{
    my ($self) = @_;
    my $qref = $self->{'mEventQueue'};

#printf STDERR "processEvents q=(%s) #=%d\n", join(',', @$qref), $#$qref;
    return 0 if ($#$qref < 0);

    my $tref = $self->{'mTransitions'};
    my $curr = $self->currentState();
    my $event = undef;
    my $teref = undef;
    my $objref = undef;
    my $fref = undef;

    while ($event = (pop @$qref)) {
#printf STDERR "processEvents LOOP event='%s', q=(%s) #=%d\n", $event, join(',', @$qref), $#$qref;
        $teref = $$tref{$curr, $event};
        $teref = $$tref{$self->anyStateName(), $event} unless ( defined($teref) );
        if ( defined($teref) ) {
            $self->{'mCurrentState'} = $$teref{'nextState'};
            $fref = $$teref{'stateMethod'};
            $objref = $$teref{'stateObject'};

            #call the next state:
            &{$fref}($objref);
        } else {
            printf STDERR "%s:  WARNING:  no transition: ('%s', '%s') -> 'undef'\n", $self->fsmName(), $curr, $event;
        }
last;
    }

    return 0;
}

sub enqueueEvent
{
    my ($self, $event) = @_;
    my $qref = $self->{'mEventQueue'};

    #fifo:
    unshift @$qref, $event;
#printf STDERR "enqueue '%s', q=(%s) #=%d\n", $event, join(',', @$qref), $#$qref;
    #allow chaining:
    return $self;
}

#########
#accessor methods for fsm object attributes:
#########

sub fsmImpl
#return value of mFsmImpl
{
    my ($self) = @_;
    return $self->{'mFsmImpl'};
}

sub fsmName
#return value of mFsmName
{
    my ($self) = @_;
    return $self->{'mFsmName'};
}

sub currentState
#return value of mCurrentState
{
    my ($self) = @_;
    return $self->{'mCurrentState'};
}

sub nullStateName
#return value of mNullStateName
{
    my ($self) = @_;
    return $self->{'mNullStateName'};
}

sub anyStateName
#return value of mAnyStateName
{
    my ($self) = @_;
    return $self->{'mAnyStateName'};
}

sub initStateName
#return value of mInitStateName
{
    my ($self) = @_;
    return $self->{'mInitStateName'};
}

sub haltStateName
#return value of mHaltStateName
{
    my ($self) = @_;
    return $self->{'mHaltStateName'};
}

sub resetStateName
#return value of mResetStateName
{
    my ($self) = @_;
    return $self->{'mResetStateName'};
}

sub nullEventName
#return value of mNullEventName
{
    my ($self) = @_;
    return $self->{'mNullEventName'};
}

sub initEventName
#return value of mInitEventName
{
    my ($self) = @_;
    return $self->{'mInitEventName'};
}

sub haltEventName
#return value of mHaltEventName
{
    my ($self) = @_;
    return $self->{'mHaltEventName'};
}

sub resetEventName
#return value of mResetEventName
{
    my ($self) = @_;
    return $self->{'mResetEventName'};
}

sub startEventName
#return value of mStartEventName
{
    my ($self) = @_;
    return $self->{'mStartEventName'};
}


1;
} #end of fsm
{
#
#cvscommand - object for setting up and executing cvs commands.
#

use strict;

package cvscommand;
my $pkgname = __PACKAGE__;

#imports:
require "txtm.pl";

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($cvsrepo, $cmd, $cmdopts, $stdoutfn, $stderrfn, @cvsdirs) = @_;

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mCvsRepos' => $cvsrepo,            #this is the repository object
        'mCvsRoot' => $cvsrepo->cvsRoot(),  #this is the full CVSROOT
        'mCvsOpts' => $cvsrepo->cvsOpts(),  #global options to pass to cvs command
        'mCvsCommand' => $cmd,              #the cvs command name
        'mCvsCommandOpts' => $cmdopts,      #options to pass to the cvs command
        'mStdOutFn' => $stdoutfn,           #command output stdout
        'mStdErrFn' => $stderrfn,           #command output stderr
        'mCvsDirs' => \@cvsdirs,            #repository-relative list of directories to pass to the cvs command
        #these attributes are set locally:
        'mErrorText' => "",                 #contents of $! for command
        'mStatus' => -1,                    #status returned by command.
        'mStartTime' => -1,                 #system time when last command started or -1
        'mEndTime' => -1,                   #system time when last command completed or -1
        'mElapsedTime' => -1,               #system time when last command completed or -1
        'mIsRunning' => 0,                  #true if command is currently running
        'mTransactionTime' => "NULL",       #transaction time of last successful command.
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    return $self;
}

################################### PACKAGE ####################################
sub exec
#execute the cvs command
#return 0 if successful.
{
    my ($self) = @_;
    my @args = (
            "-q",
            $self->getCvsOpts(),
            "-d", $self->getCvsRoot(),
            $self->getCvsCommand(),
            $self->getCvsCommandOpts(),
            $self->getCvsDirs(),
    );

    $self->{'mStartTime'} = time();
    $self->{'mTransactionTime'} = "NULL";
    $self->{'mEndTime'} = -1;
    $self->{'mElapsedTime'} = 0;
    $self->{'mIsRunning'} = 1;
    my $cmd = sprintf("sh -c 'cvs %s >%s 2>%s'", join(" ", @args), $self->getStdOutFn(), $self->getStdErrFn());
printf STDERR "%s[exec]: cmd=>%s<\n", $pkgname, $cmd;
    system($cmd);
    $self->{'mStatus'} = $?;
    $self->{'mErrorText'} = $!;
    $self->{'mEndTime'} = time();
    $self->{'mIsRunning'} = 0;

    #calculate elapsedtime:
    $self->{'mElapsedTime'} = ($self->endTime() - $self->startTime());

    #if command was successful, set transaction time to be the endtime:
    if ($self->status() == 0) {
        $self->{'mTransactionTime'} = &txtm::to_gmtxtm($self->endTime());
    }

printf STDERR "%s[exec]: status=%s mTransactionTime=%s elapsed=%d seconds\n",
    $pkgname, $self->status(), $self->transactionTime(), $self->elapsedTime();

    return $self->status();
}

sub getCvsRoot
#return value of CvsRoot
{
    my ($self) = @_;
    return $self->{'mCvsRoot'};
}

sub setCvsRoot
#set value of CvsRoot and return value.
{
    my ($self, $value) = @_;
    $self->{'mCvsRoot'} = $value;
    return $self->{'mCvsRoot'};
}

sub getCvsOpts
#return value of CvsOpts
{
    my ($self) = @_;
    return $self->{'mCvsOpts'};
}

sub setCvsOpts
#set value of CvsOpts and return value.
{
    my ($self, $value) = @_;
    $self->{'mCvsOpts'} = $value;
    return $self->{'mCvsOpts'};
}

sub getCvsCommand
#return value of CvsCommand
{
    my ($self) = @_;
    return $self->{'mCvsCommand'};
}

sub setCvsCommand
#set value of CvsCommand and return value.
{
    my ($self, $value) = @_;
    $self->{'mCvsCommand'} = $value;
    return $self->{'mCvsCommand'};
}

sub getCvsCommandOpts
#return value of CvsCommandOpts
{
    my ($self) = @_;
    return $self->{'mCvsCommandOpts'};
}

sub setCvsCommandOpts
#set value of CvsCommandOpts and return value.
{
    my ($self, $value) = @_;
    $self->{'mCvsCommandOpts'} = $value;
    return $self->{'mCvsCommandOpts'};
}

sub getStdOutFn
#return value of StdOutFn
{
    my ($self) = @_;
    return $self->{'mStdOutFn'};
}

sub setStdOutFn
#set value of StdOutFn and return value.
{
    my ($self, $value) = @_;
    $self->{'mStdOutFn'} = $value;
    return $self->{'mStdOutFn'};
}

sub getStdErrFn
#return value of StdErrFn
{
    my ($self) = @_;
    return $self->{'mStdErrFn'};
}

sub setStdErrFn
#set value of StdErrFn and return value.
{
    my ($self, $value) = @_;
    $self->{'mStdErrFn'} = $value;
    return $self->{'mStdErrFn'};
}

sub getCvsDirs
#return value of @CvsDirs
{
    my ($self) = @_;
    return @{$self->{'mCvsDirs'}};
}

sub setCvsDirs
#set value of CvsDirs and return value.
{
    my ($self, @value) = @_;
    $self->{'mCvsDirs'} = \@value;
    return @{$self->{'mCvsDirs'}};
}

sub errorText
#return value of mErrorText
{
    my ($self) = @_;
    return $self->{'mErrorText'};
}

sub status
#return value of mStatus
{
    my ($self) = @_;
    return $self->{'mStatus'};
}

sub startTime
#return value of mStartTime
{
    my ($self) = @_;
    return $self->{'mStartTime'};
}

sub endTime
#return value of mEndTime
{
    my ($self) = @_;
    return $self->{'mEndTime'};
}

sub elapsedTime
#return value of mElapsedTime
{
    my ($self) = @_;
    return $self->{'mElapsedTime'};
}

sub isRunning
#return value of mIsRunning
{
    my ($self) = @_;
    return $self->{'mIsRunning'};
}

sub transactionTime
#return value of mTransactionTime
{
    my ($self) = @_;
    return $self->{'mTransactionTime'};
}


1;
} #end of cvscommand
{
#
#cvsrepo - simple object holding CVSROOT parameters
#

use strict;

package cvsrepo;
my $pkgname = __PACKAGE__;

#imports:

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($cvsroot, $cvsopts, $module) = @_;

    #parse the full CVSROOT setting into individual fields:
    #example:  :pserver:russt@ashland.sfbay:/proj
    my ($dummy, $server, $user_host, $root) = split(':', $cvsroot);
    my ($login, $host) = split('@', $user_host);

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mCvsRoot' => $cvsroot,        #full CVSROOT string.
        'mCvsRootDir'      => "",      #the root directory of our repository (as found in CVS/Root file)
        'mServerMethod' => $server,
        'mLogin' => $login,
        'mHost' => $host,
        'mRoot' => $root,
        'mCvsOpts' => $cvsopts,
        'mTopModule' => $module,
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    #derive mCvsRootDir from mCvsRoot:
    if ($self->cvsRoot() =~ m|(/.*)$|) {
        $self->{'mCvsRootDir'} = $1;
    } else {
        printf STDERR "%s: ERROR: failed to set mCvsRootDir attributes, CVSROOT='%s'\n", $pkgname, $self->cvsRoot();
    }


    return $self;
}

################################### PACKAGE ####################################
sub topModule
#return value of mTopModule
{
    my ($self) = @_;
    return $self->{'mTopModule'};
}

sub cvsOpts
#return value of mCvsOpts
{
    my ($self) = @_;
    return $self->{'mCvsOpts'};
}

sub cvsRoot
#return value of mCvsRoot
{
    my ($self) = @_;
    return $self->{'mCvsRoot'};
}

sub cvsRootDir
#return value of mCvsRootDir
{
    my ($self) = @_;
    return $self->{'mCvsRootDir'};
}


1;
} #end of cvsrepo
{
#
#depositentry - a single revision deposit
#

use strict;

package depositentry;
my $pkgname = __PACKAGE__;

#imports:

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($depositdat, $rlogentry) = @_;

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mRlogEntry' => $rlogentry,                             #the rlog entry this deposit belongs to
        'mAuthor'    => $$depositdat{'deposit_author'},         #YY
        'mBranches'  => $$depositdat{'deposit_branches'},       #YY
        'mDate'      => $$depositdat{'deposit_date'},           #YY
        'mDiffCount' => $$depositdat{'deposit_diffcount'},      #YY
        'mMessage'   => $$depositdat{'deposit_message'},        #YY
        'mRevision'  => $$depositdat{'deposit_rev'},            #YY
        'mLocker'    => $$depositdat{'deposit_locker'},         #YY
        'mState'     => $$depositdat{'deposit_state'},          #YY
        #extra fields generated from above base fields:
        'mCvsRootDir' => "",                                    #the root directory of the CVSROOT
        'mSubDir' => "",                                        #the repository sub-directory for this file
        'mFileName' => "",                                      #the base file name
        'mTimeStamp' => "",                                     #rcs date converted to yyyymmddhhmmss format
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):
    my $cvsroot = $self->rlogEntry()->cvsRootDir();

    if ($self->rlogEntry()->rcsFileName() =~ m|^$cvsroot/(.*)/([^/]+),v$|) {
        $self->{'mSubDir'}   = $1;
        $self->{'mFileName'} = $2;
    } else {
        printf STDERR "%s: ERROR: failed to set mSubDir and mFileName attributes\n", $pkgname;
        printf STDERR "\tcvsroot='%s' rcsfile='%s'\n", $cvsroot, $self->rlogEntry()->rcsFileName();
    }

    #transform CVS date string into tranasaction date form:
    my $yyyymmddhhmmss = $self->date();
    #example:  2007/01/30 15:32:35 -> 20070130153235
    $yyyymmddhhmmss =~ s|[/:\s]||g;
    $self->{'mTimeStamp'} = $yyyymmddhhmmss;

    #inherit cvs root dir from rlog entry:
    $self->{'mCvsRootDir'} = $self->rlogEntry()->cvsRootDir();

    return $self;
}

################################### PACKAGE ####################################
sub calcPreviousRevision
{
    my($currRev) = @_;

    my @rev = split(/\./, $currRev);

    #minimum allowed is x.x, i.e., 2 fields:
    return undef if ($#rev < 1);

    if ($rev[$#rev] > 1) {
        --$rev[$#rev];
#printf STDERR "calcPreviousRevision T2 currRev='%s', rev=(%s) cnt=%s\n", $currRev, join('.', @rev), $#rev;
        return join('.', @rev) ;
    }
    
    #otherwise, if we are on a branch, then return root of branch:
    if ($#rev >= 3) {
#printf STDERR "calcPreviousRevision T3 currRev='%s', rev=(%s) cnt=%s\n", $currRev, join('.', @rev[0..$#rev-2]), $#rev;
        return join('.', @rev[0..$#rev-2]);
    }
    
    #otherwise, previous revision is undefined:
    return undef;
}

sub isNewFile
#true if this deposit represents a new file in the repository
{
    my ($self) = @_;

    #for now, if previous revsion is not defined, then this is a new file:
    return !defined(&calcPreviousRevision($self->revision()));

    #Q:  if it is rev x.1 on branch, and previous rev is 1.1, is it new?
}

sub isDeleteDeposit
#true if this is a deleted revision deposit
{
    my ($self) = @_;
    return ($self->state() eq "dead");
}

sub previousDeposit
#lookup previous deposit for this entry or undef.
{
    my ($self) = @_;

    return( $self->rlogEntry()->depositByRev(&calcPreviousRevision($self->revision())) );
}

sub rlogEntry
#return value of mRlogEntry
{
    my ($self) = @_;
    return $self->{'mRlogEntry'};
}

sub author
#return value of mAuthor
{
    my ($self) = @_;
    return $self->{'mAuthor'};
}

sub branches
#return value of mBranches
{
    my ($self) = @_;
    return $self->{'mBranches'};
}

sub date
#return value of mDate
{
    my ($self) = @_;
    return $self->{'mDate'};
}

sub diffCount
#return value of mDiffCount
{
    my ($self) = @_;
    return $self->{'mDiffCount'};
}

sub message
#return value of mMessage
{
    my ($self) = @_;
    return $self->{'mMessage'};
}

sub revision
#return value of mRevision
{
    my ($self) = @_;
    return $self->{'mRevision'};
}

sub locker
#return value of mLocker
{
    my ($self) = @_;
    return $self->{'mLocker'};
}

sub state
#return value of mState
{
    my ($self) = @_;
    return $self->{'mState'};
}

sub cvsRootDir
#return value of mCvsRootDir
{
    my ($self) = @_;
    return $self->{'mCvsRootDir'};
}

sub subDir
#return value of mSubDir
{
    my ($self) = @_;
    return $self->{'mSubDir'};
}

sub fileName
#return value of mFileName
{
    my ($self) = @_;
    return $self->{'mFileName'};
}

sub timeStamp
#return value of mTimeStamp
{
    my ($self) = @_;
    return $self->{'mTimeStamp'};
}


1;
} #end of depositentry
{
#
#depositlist - manage list of revision deposits for a single rcsfile
#

use strict;

package depositlist;
my $pkgname = __PACKAGE__;

#imports:

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($dplistref, $rlogentry) = @_;

    my (@dplist) = ();

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mRlogEntry' => $rlogentry,              #the rlog entry this deposit list belongs to
        'mDepositListEntries' => \@dplist,       #list of depositentry objects
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):
    #foreach element in input list...
    for my $dph (@$dplistref) {
        #add a deposit entry object;
        push @dplist, new depositentry($dph, $rlogentry);
    }

    return $self;
}

################################### PACKAGE ####################################

sub depositListEntries
#return mDepositListEntries list
{
    my ($self) = @_;
    return @{$self->{'mDepositListEntries'}};
}


1;
} #end of depositlist
{
#
#rlogentry - manage rlog data for a single rcs file
#

use strict;

package rlogentry;
my $pkgname = __PACKAGE__;

#imports:

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($rlogdat, $cvsrootdir) = @_;

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mCvsRootDir'    => $cvsrootdir,
        'mRcsFileName'   => $$rlogdat{'rcsfile'},         #YY
        'mHeadRev'       => $$rlogdat{'headrev'},         #YY
        'mBranchRev'     => $$rlogdat{'branchrev'},       #YY
        'mDescription'   => $$rlogdat{'description'},     #YY
        'mKeywordSubst'  => $$rlogdat{'keywordsubst'},    #YY
        'mSymbolicNames' => $$rlogdat{'symbolic_names'},  #YY
        'mSelectedRevs'  => $$rlogdat{'selectedrevs'},    #YY
        'mTotalRevs'     => $$rlogdat{'totalrevs'},       #YY
        'mLocks'         => $$rlogdat{'locks'},           #YY
        'mAccessList'    => $$rlogdat{'access_list'},     #YY
        'mDepositList'   => undef,                        #deposit list - user must set after initial construction.
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    return $self;
}

################################### PACKAGE ####################################

sub depositByRev
#return the deposit entry object keyed by <rev> or undef.
{
    my ($self, $rev) = @_;

    return undef unless defined($rev);

    #not defined unless deposit list has been initialized:
    my $dplist = $self->getDepositList();

    #foreach deposit entry in list...
    my $theEntry = undef;
    for my $dph ($dplist->depositListEntries()) {
        if ($rev eq $dph->revision()) {
            $theEntry = $dph;
            last;
        }
    }

    return $theEntry;
}

sub getDepositList
#return value of DepositList
{
    my ($self) = @_;
    return $self->{'mDepositList'};
}

sub setDepositList
#set value of DepositList and return value.
{
    my ($self, $value) = @_;
    $self->{'mDepositList'} = $value;
    return $self->{'mDepositList'};
}

sub cvsRootDir
#return value of mCvsRootDir
{
    my ($self) = @_;
    return $self->{'mCvsRootDir'};
}

sub rcsFileName
#return value of mRcsFileName
{
    my ($self) = @_;
    return $self->{'mRcsFileName'};
}

sub headRev
#return value of mHeadRev
{
    my ($self) = @_;
    return $self->{'mHeadRev'};
}

sub branchRev
#return value of mBranchRev
{
    my ($self) = @_;
    return $self->{'mBranchRev'};
}

sub description
#return value of mDescription
{
    my ($self) = @_;
    return $self->{'mDescription'};
}

sub keywordSubst
#return value of mKeywordSubst
{
    my ($self) = @_;
    return $self->{'mKeywordSubst'};
}

sub symbolicNames
#return value of mSymbolicNames
{
    my ($self) = @_;
    return $self->{'mSymbolicNames'};
}

sub selectedRevs
#return value of mSelectedRevs
{
    my ($self) = @_;
    return $self->{'mSelectedRevs'};
}

sub totalRevs
#return value of mTotalRevs
{
    my ($self) = @_;
    return $self->{'mTotalRevs'};
}

sub locks
#return value of mLocks
{
    my ($self) = @_;
    return $self->{'mLocks'};
}

sub accessList
#return value of mAccessList
{
    my ($self) = @_;
    return $self->{'mAccessList'};
}


1;
} #end of rlogentry
{
#
#spoolfile - manage spool files for the commit spooler
#

use strict;

package spoolfile;
my $pkgname = __PACKAGE__;

#imports:

#package variables:
#we are a sub-class of the spooler package:
no strict; @ISA = ("spooler"); use strict;

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($spooler) = @_;    #get a reference to our spool directory object

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mSpooler'     => $spooler,                  #the spooler object we belong to
        'mFileName'    => $spooler->nextSpoolFn(),   #the spool filename
        'mMessageText' => "",                        #the message text for this spool file
        'mLastErrorStr'=> "",                        #error message from last spool file operation
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    return $self;
}

################################### PACKAGE ####################################
sub writeSpoolFile
#create the spool file from the current message text
#return 0 if successful
{
    my ($self) = @_;

    #write string to next file to be processed:
    my $spoolfn = $self->fileName();
    if (!open(OUTFILE, ">$spoolfn")) {
        $self->{'mLastErrorStr'} = sprintf("cannot open spool file '%s' for writing, '%s'", $spoolfn, $!);
        return 1;  #FAILURE
    }

    print OUTFILE $self->getMessageText();
    #TODO - disk full error?
    close OUTFILE;

    return 0;
}


sub getMessageText
#return value of MessageText
{
    my ($self) = @_;
    return $self->{'mMessageText'};
}

sub setMessageText
#set value of MessageText and return value.
{
    my ($self, $value) = @_;
    $self->{'mMessageText'} = $value;
    return $self->{'mMessageText'};
}

sub fileName
#return value of mFileName
{
    my ($self) = @_;
    return $self->{'mFileName'};
}

sub lastErrorStr
#return value of mLastErrorStr
{
    my ($self) = @_;
    return $self->{'mLastErrorStr'};
}

sub spooler
#return value of mSpooler
{
    my ($self) = @_;
    return $self->{'mSpooler'};
}


1;
} #end of spoolfile
{
#
#spooler - manage a spool directory
#

use strict;

package spooler;
my $pkgname = __PACKAGE__;

#imports:

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($cmdfmtStr, $spooldir, $prefix) = @_;
    $prefix = "spool_data" unless (defined($prefix));

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mCommandStr'            => $cmdfmtStr,       #the sprintf spool format string, with %s for spool file name
        'mSpoolDir'              => $spooldir,        #the spool directory
        'mSpoolDoneDir'          => "$spooldir/done", #the processed directory
        'mSpoolFilePrefix'       => $prefix,
        'mNextSpoolFileNumberFn' => "$spooldir/.next_spoolnum",
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):
    #make sure that spool dir exists:
    system sprintf("mkdir -p %s", $self->spoolDoneDir());

    return $self;
}

################################### PACKAGE ####################################

sub nextSpoolFn
#return the full pathname of the next spool file name.
{
    my ($self) = @_;

    #read in the next spoolfile number:
    my $nextNum = "NULL";
    if (open(NUMFILE, $self->nextSpoolFileNumberFn())) {
        $nextNum = <NUMFILE>;
        close NUMFILE;
        chomp $nextNum;
        if (open(NUMFILE, ">" . $self->nextSpoolFileNumberFn())) {
            printf NUMFILE "%05d\n", $nextNum + 1;
            close NUMFILE;
        } else {
            printf STDERR "%s::nextSpoolFn: ERROR(A):  write to '%s' FAILED, '%s'\n", $pkgname, $self->nextSpoolFileNumberFn(), $!;
            return undef;
        }
    } else {
        #assume this is the first spool file
        $nextNum = 1;
        if (open(NUMFILE, ">" . $self->nextSpoolFileNumberFn())) {
            printf NUMFILE "%05d\n", $nextNum + 1;
            close NUMFILE;
        } else {
            printf STDERR "%s::nextSpoolFn: ERROR(B):  write to '%s' FAILED, '%s'\n", $pkgname, $self->nextSpoolFileNumberFn(), $!;
            return undef;
        }
    }

    return sprintf("%s/%s_%05d", $self->spoolDir(), $self->spoolFilePrefix(), $nextNum);
}

sub run
#send the queued transactions to the commitServlet.
{
    my ($self) = @_;

    #foreach file in spool dir...
    my @filelist = $self->lsSpoolDir();
    my $cnt = 0;
    for my $fn (@filelist) {
        ++$cnt;
        #formulate command string:
        my $outfn  = sprintf("%s/%s.out", $self->spoolDir(), $fn);
        my $errfn  = sprintf("%s/%s.err", $self->spoolDir(), $fn);
        my $fullfn = sprintf("%s/%s", $self->spoolDir(), $fn);

        my $cmd   = sprintf($self->commandStr(), $fullfn);
        $cmd = sprintf("sh -c '%s >%s 2>%s'", $cmd, $outfn, $errfn);

        #run the command:
        printf STDERR "%s::run: %s\n", $pkgname, $cmd;
        my $status = 0;
        system($cmd);

        $status = $?;
        my $errstr = "$!";
        my $out = `grep '^SUCCESS' $outfn`;

        #if successful, then move file to "processed" subdir
        if ($status == 0 && $out =~ /^SUCCESS/) {
            rename $outfn,  sprintf("%s/%s.out", $self->spoolDoneDir(), $fn);
            rename $errfn,  sprintf("%s/%s.err", $self->spoolDoneDir(), $fn);
            rename $fullfn, sprintf("%s/%s", $self->spoolDoneDir(), $fn);
        } else {
            printf STDERR "%s::run: command FAILED with status %d for file '%s', '%s'\n",
                $pkgname, $status, $fullfn, $errstr;
        }

        #last if ($cnt >= 5);
    }

    return 0;
}

sub lsSpoolDir
#send the queued transactions to the commitServlet.
{
    my ($self) = @_;

    my (@lsout) = &ls($self->spoolDir());
#printf STDERR "lsSpoolDir A lsout=(%s)\n", join("\n", @lsout);
    my $pat = sprintf("^%s_\\d+\$", $self->spoolFilePrefix());

    @lsout = grep(/$pat/, sort @lsout);
#printf STDERR "lsSpoolDir B pat='%s' lsout=(%s)\n", $pat, join("\n", @lsout);
    return @lsout;
}

sub ls
{
    my ($dir) = @_;
    my @lsout = ();

    if (!opendir(DIR, $dir)) {
        print STDERR "ls:  ERROR opening directory '$dir'\n";
        return undef;
    }

    #skip '.' & '..' on unix
    @lsout = grep(!/^\.\.?$/, readdir(DIR));

    closedir(DIR);
    return @lsout;
}

sub commandStr
#return value of mCommandStr
{
    my ($self) = @_;
    return $self->{'mCommandStr'};
}

sub nextSpoolFileNumberFn
#return value of mNextSpoolFileNumberFn
{
    my ($self) = @_;
    return $self->{'mNextSpoolFileNumberFn'};
}

sub spoolDir
#return value of mSpoolDir
{
    my ($self) = @_;
    return $self->{'mSpoolDir'};
}

sub spoolFilePrefix
#return value of mSpoolFilePrefix
{
    my ($self) = @_;
    return $self->{'mSpoolFilePrefix'};
}

sub spoolDoneDir
#return value of mSpoolDoneDir
{
    my ($self) = @_;
    return $self->{'mSpoolDoneDir'};
}


1;
} #end of spooler
{
#
#inftx - create pagosa transactions from rlog data
#

use strict;

package inftx;
my $pkgname = __PACKAGE__;

#imports:

#package variables:
my $DEBUG_INFTX = 0;

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($commitspooler, $scfg) = @_;

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mCommitSpooler'  => $commitspooler,   #commit spooler for this transaction list
        'mScmRoot'        => $scfg->getInfRepositoryDir(),
                                               #this is the source root configured in the INF repository field
        'mMinNoticeId'    => $scfg->getMinNoticeId(),
                                               #do not generate transactions for IN's lower than this.
        'mSpoolErrStr'    => "",               #errors during last spoolTransactions() op.
        'mInitialRevName' => "NONE",           #previous revision of a new file
        'mOpDirType'      => "d",              #transaction on cvs directory
        'mOpFileType'     => "f",              #transaction on cvs file
        'mOpDelType'      => "del",            #cvs delete file operation
        'mOpModType'      => "add",            #cvs modify file operation
        'mOpNewType'      => "add",            #cvs new file operation
        'mTransactionQ'   => [],               #our transaction queue
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    return $self;
}

################################### PACKAGE ####################################
sub getNoticeId
#return the integration notice number from <text>, or undef.
{
    my ($text) = @_;

    if ($text =~ /IN\s*=\s*(\d+)/) {
        return $1;
    }

    return undef;
}

sub generateTransactions
#true if we successfully create the inf transaction records
{
    my ($self, $rlogCurr, $rlogPrev) = @_;

    #get list of current revision deposit key strings:
    my @curr_deposits = $rlogCurr->sortedDepositKeyList();

    #get list of previous revision deposit key strings:
    my @old_deposits =  $rlogPrev->sortedDepositKeyList();

    my @new_deposit_keys = &MINUS(\@curr_deposits, \@old_deposits);

#printf STDERR "generateTransactions: #curr_deposits=%d #old_deposits=%d #new_deposit_keys=%s\n", $#curr_deposits, $#old_deposits, $#new_deposit_keys;

    my $skipped = 0;
    my $skipped_noticeId_toolow = 0;
    my $total   = $#new_deposit_keys+1;

    for my $dpkey (@new_deposit_keys) {

        my $dp = $rlogCurr->depositObjbyKey($dpkey);

        my $noticeId = &getNoticeId($dp->message());
        if (!defined($noticeId)) {
            #skip deposits that do not have IN= in commit message:
            ++$skipped;
            next;
        }

        #skip notices up to min specified:
        if ($noticeId < $self->minNoticeId()) {
            ++$skipped;
            ++$skipped_noticeId_toolow;
            next;
        }


        if ($dp->isNewFile()) {
            #there is no old rev for new transactions:
            $self->generateNewTransaction($dp, $noticeId);
        } else {
            #get previous revision deposited from current:
            my $old = $dp->previousDeposit();
            if (!defined($old)) {
                printf STDERR "%s: ERROR: previousDeposit() failed for %s, rev %s\n",
                    $pkgname, $dp->rlogEntry()->rcsFileName(), $dp->revision();
                return 0;
            }

            if ($dp->isDeleteDeposit()) {
                $self->generateDeleteTransaction($dp, $noticeId, $old->revision());
            } else {
                $self->generateModTransaction($dp, $noticeId, $old->revision());
            }
        }
    }

printf STDERR "%s::generateTransactions: skipped %d/%d transactions, %d notices < %d\n",
    $pkgname, $skipped, $total, $skipped_noticeId_toolow, $self->minNoticeId();

    return 1;    #success
}

sub generateDeleteTransaction
#return 1 if transaction is generated, otherwise 0.
{
    my ($self, $dp, $noticeId, $oldrev) = @_;

    $self->createCommitMessage(
        $noticeId,
        $dp->timeStamp(),
        $dp->author(),
        $self->scmRoot(),     #repository root dir
        $dp->revision(),
        $dp->subDir(),        #repository relative dir
        $dp->fileName(),
        $self->opDelType(),   # "del"
        $self->opFileType(),  # "f"
        $oldrev,
    );

    return 1;    #wrote transaction
}

sub generateNewTransaction
{
    my ($self, $dp, $noticeId) = @_;

    $self->createCommitMessage(
        $noticeId,
        $dp->timeStamp(),
        $dp->author(),
        $self->scmRoot(),     #repository root dir
        $dp->revision(),
        $dp->subDir(),        #repository relative dir
        $dp->fileName(),
        $self->opNewType(),         # "add"
        $self->opFileType(),        # "f"
        $self->initialRevName(),    # "NONE"
    );

    return 1;    #wrote transaction
}

sub generateModTransaction
{
    my ($self, $dp, $noticeId, $oldrev) = @_;

    $self->createCommitMessage(
        $noticeId,
        $dp->timeStamp(),
        $dp->author(),
        $self->scmRoot(),     #repository root dir
        $dp->revision(),
        $dp->subDir(),        #repository relative dir
        $dp->fileName(),
        $self->opModType(),         # "add"
        $self->opFileType(),        # "f"
        $oldrev,
    );

    return 1;    #wrote transaction
}

sub createCommitMessage
#generate the xml for this transaction and store it in the transaction queue.
{
    my ($self, $noticeId, $timeStamp, $author, $scmroot, $newrev, $subDir, $fileName, $cvsop, $filetype, $oldrev) = @_;

    my $spoolfn = '$spoolfn';   #delay evaluation of spoolfn until we know it.
    my $userId = "NULL";

    my $commitMsg = << "!";
<?xml version="1.0" encoding="UTF-8"?>
<commit
    xmlns='http://xml.netbeans.org/examples/targetNS'
    xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
    xsi:noNamespaceSchemaLocation="commit.xsd"
    in_num="$noticeId"
    timestamp="$timeStamp"
    login="$author"
    userid="$userId"
    filename="$spoolfn"
    repositoryName="$scmroot"
>
    <items>
        <item optType="$cvsop" oldRev="$oldrev" newRev="$newrev" cvsType="$filetype" dir="$subDir" file="$fileName"/>
    </items>    
</commit>
!

    printf STDERR "%s", $commitMsg if ($DEBUG_INFTX);

    return $self->enqueueTransaction($commitMsg);
}

sub dequeueTransaction
#remove a transaction from the queue
{
    my ($self) = @_;
    return undef unless ($#{$self->{'mTransactionQ'}} >= 0);
    return (shift @{$self->{'mTransactionQ'}});
}

sub enqueueTransaction
#add a transaction to the queue
{
    my ($self, $msg) = @_;
    push @{$self->{'mTransactionQ'}}, $msg;
}

sub showTransactions
#dump the INF transaction records
{
    my ($self) = @_;

    my @txq =  @{$self->{'mTransactionQ'}};

    printf STDERR "%s::showTransactions: there are %d elements on the queue\n", $pkgname, $#txq+1;

    for my $qe (@txq) {
        printf STDERR "%s\n%s\n", "-"x80, $qe;
    }

    return 1;    #success
}

sub spoolTransactions
#create the transactions and send to the commit spooler. returns 0 if
#operation is fully successful, or the number of records that were not spooled.
{
    my ($self) = @_;

    #clear any errors from previous operation:
    $self->{'mSpoolErrStr'} = "";

    my $errcnt = 0;

    my $msg;
    while (defined($msg = $self->dequeueTransaction())) {
        #get a spooler file object:
        my $spooler = new spoolfile($self->commitSpooler());
        my $spoolfn = $spooler->fileName();

        #substitute the name of the spool file into message:
        my $msg_orig = $msg;
        $msg =~ s/\$spoolfn/$spoolfn/;

        #set the text:
        $spooler->setMessageText($msg);

        #write the message to the spool file:
        if ($spooler->writeSpoolFile() != 0) {
            ++$errcnt;
            $self->{'mSpoolErrStr'} .= sprintf("%s\n", $spooler->lastErrorStr());
            #re-queue message:
            $self->enqueueTransaction($msg_orig);
        }
    }

    return $errcnt;    #success if no errors
}

sub spoolErrStr
#return value of mSpoolErrStr
{
    my ($self) = @_;
    return $self->{'mSpoolErrStr'};
}

sub commitSpooler
#return value of mCommitSpooler
{
    my ($self) = @_;
    return $self->{'mCommitSpooler'};
}

sub scmRoot
#return value of mScmRoot
{
    my ($self) = @_;
    return $self->{'mScmRoot'};
}

sub minNoticeId
#return value of mMinNoticeId
{
    my ($self) = @_;
    return $self->{'mMinNoticeId'};
}

sub initialRevName
#return value of mInitialRevName
{
    my ($self) = @_;
    return $self->{'mInitialRevName'};
}

sub opDirType
#return value of mOpDirType
{
    my ($self) = @_;
    return $self->{'mOpDirType'};
}

sub opFileType
#return value of mOpFileType
{
    my ($self) = @_;
    return $self->{'mOpFileType'};
}

sub opDelType
#return value of mOpDelType
{
    my ($self) = @_;
    return $self->{'mOpDelType'};
}

sub opModType
#return value of mOpModType
{
    my ($self) = @_;
    return $self->{'mOpModType'};
}

sub opNewType
#return value of mOpNewType
{
    my ($self) = @_;
    return $self->{'mOpNewType'};
}

sub MINUS
#returns A - B
#usage:  my (@a, @b); ... ; @difference = &MINUS(\@a, \@b);
{
    my($A, $B) = @_;
    my(%mark);

    for (@{$B}) { $mark{$_}++;}
    return(grep(!$mark{$_}, @{$A}));
}


1;
} #end of inftx
{
#
#rlogparser - encapsulate cvs rlog data
#

use strict;

package rlogparser;
my $pkgname = __PACKAGE__;

#imports:

#package variables:
my @RLOG_RECS = ();
my $DEBUG_PARSER = 0;

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($txtime, $inputFn, $cvsrootdir) = @_;

    #globlal hashes that hold all rlog data for this instance:
    my %rlog_all_deposits = ();
    my %rlog_rcsfiles = ();

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mName'            => $txtime,              #a name for this rlog
        'mTransactionTime' => $txtime,              #transaction time associated with this rlog
        'mRlogDataFn'      => $inputFn,             #the data file containing output text of rlog command
        'mCvsRootDir'      => $cvsrootdir,          #the root directory of our repository (as found in CVS/Root file)
        'mRlogRcsFiles'    => \%rlog_rcsfiles,      #hash containing all rcsfiles, keyed by {rcsfile}
        'mRlogAllDeposits' => \%rlog_all_deposits,  #secondary hash containing all deposits, keyed by {rcsfile,rev,state}
        'mFileCrc'         => 0,                    #crc of data file (set below)
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):
    #compute crc of datafile:
    $self->{'mFileCrc'} = &CalculateFileCRC($self->rlogDataFn());


    return $self;
}

################################### PACKAGE ####################################
sub parseRlogData
#true if parse of rlog data is successful
{
    my ($self) = @_;

    #if data file is not set or not accessible, then false:
    return 0 if ($self->rlogDataFn() eq "");

    my $rlogdata = undef;
    if (&read_file2str(\$rlogdata, $self->rlogDataFn()) != 0) {
        printf STDERR "parseRlogData: errors reading data file '%s'\n", $self->rlogDataFn();
        return 0;
    }

    #split files into records.  records are split by lines of 77 "=" chars:
    my $sep = "\n" . "="x77 . "\n";
    @RLOG_RECS = split($sep, $rlogdata);
$DEBUG_PARSER=1;
printf STDERR "READ %d records into RLOG_RECS\n", $#RLOG_RECS +1 if ($DEBUG_PARSER);
$DEBUG_PARSER=0;

    my $cnt = 0;
    my $errcnt = 0;

    for (@RLOG_RECS) {
        my @arec = split(/\n\n*/, $_);
        @arec = grep($_ !~ /^\s*$/, @arec);

        if (!$self->rlog_entry(\@arec, $self->cvsRootDir())) {
            ++$errcnt;
            printf STDERR "parseRlogData: RLOG_RECS[%d] was NOT RECOGNIZED!\n", $cnt;
        } else {
            printf STDERR "parseRlogData: RLOG_RECS[%d] was RECOGNIZED!\n", $cnt if ($DEBUG_PARSER);
        }

        ++$cnt;
    }

    printf STDERR "parseRlogData: %d/%d records recognized as valid rlog data\n", $cnt-$errcnt, $cnt;;

    return (($errcnt == 0)? 1: 0);
}

sub sortedDepositKeyList
#return sorted list of keys to RLOG_ALL_DEPOSITS
{
    my ($self) = @_;

    return sort keys %{$self->{'mRlogAllDeposits'}};
}

sub depositObjbyKey
#return reference to a deposit entry keyed by <alldepkey>.
{
    my ($self, $alldepkey) = @_;

    #this returns a revision deposit object:
    return $self->{'mRlogAllDeposits'}{$alldepkey};
}

sub showRlogData
#dump the rlog data structures
{
    my ($self) = @_;

    for (@RLOG_RECS) {
        printf STDERR "\n";
        my @arec = split(/\n\n*/, $_);
        @arec = grep($_ !~ /^\s*$/, @arec);
        for (my $ii = 0; $ii <= $#arec; $ii++) {
            printf STDERR "arec(%d)='%s'\n", $ii, $arec[$ii];
        }
    }
}

sub equals
#true if this rlog is equal <other>
{
    my ($self, $other) = @_;

    return (($self->fileCrc() == $other->fileCrc())? 1 : 0);
}

sub cvsRootDir
#return value of mCvsRootDir
{
    my ($self) = @_;
    return $self->{'mCvsRootDir'};
}

sub fileCrc
#return value of mFileCrc
{
    my ($self) = @_;
    return $self->{'mFileCrc'};
}

sub name
#return value of mName
{
    my ($self) = @_;
    return $self->{'mName'};
}

sub rlogDataFn
#return value of mRlogDataFn
{
    my ($self) = @_;
    return $self->{'mRlogDataFn'};
}

sub transactionTime
#return value of mTransactionTime
{
    my ($self) = @_;
    return $self->{'mTransactionTime'};
}

sub rlog_entry
#returns true if we recognize an rlog_entry
#expect a list of the input text for a full rlog entry as argument
# rcsfile workingfile? headrev branchrev locks access_list symbolic_names?  keywordsubst revinfo description deposits
{
    my ($self, $linesref, $cvsrootdir) = @_;

    my %rlogdat = ();

#print STDERR "rlog_entry:  lines=(%s)\n", join(",", @{$linesref});
    return 0 unless &rcsfile($linesref, \%rlogdat);

    #we don't need to distinguish Attic files:
    $rlogdat{'rcsfile'} =~ s|/Attic||;

    #optional:
    &workingfile($linesref, \%rlogdat);

    return 0 unless &headrev($linesref, \%rlogdat);
    return 0 unless &branchrev($linesref, \%rlogdat);
    return 0 unless &locks($linesref, \%rlogdat);
    return 0 unless &access_list($linesref, \%rlogdat);

    #optional:
    &symbolic_names($linesref, \%rlogdat);

    return 0 unless &keywordsubst($linesref, \%rlogdat);
    return 0 unless &revinfo($linesref, \%rlogdat);
    return 0 unless &description($linesref, \%rlogdat);

    my $dplistref = undef;
    return 0 unless &deposits($linesref, \$dplistref);

printf STDERR "\t      found non-terminal rlog_entry #lines left=%d\n", $#{$linesref} if ($DEBUG_PARSER);
&dumphash(\%rlogdat, "rlog_entry") if ($DEBUG_PARSER);
&dump_deposits($dplistref, \%rlogdat) if ($DEBUG_PARSER);

    #create a new rlog_enrty object:
    my $rlogentry = new rlogentry(\%rlogdat, $cvsrootdir);

    #create a new deposit list object that points back to rlog entry::
    my $dplist = new depositlist($dplistref, $rlogentry);

    #now init the depositList entry in the rlog entry:
    $rlogentry->setDepositList($dplist);

    #add this rlog entry to collection of all entries for this repository (unique by rcs file name):
    ${$self->{'mRlogRcsFiles'}}{$rlogdat{'rcsfile'}} = $rlogentry;

    #foreach deposit, add unique entry in alldeposits: (KEY:{rcsfile, revision, state}, rlogentry_object)
    for my $dph ($dplist->depositListEntries()) {
        ${$self->{'mRlogAllDeposits'}}{$rlogentry->rcsFileName(), $dph->revision(), $dph->state()} = $dph;
    }

    return 1;
}

sub access_list
#return 1 if we recognize access_list, otherwise 0
#   access_list -> 'access list:' ('<text>')?
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "access list:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #Example:
    # access list:
    # \trusst

    #now pick up any following lines containing access logins (optional):
    my @tokens = ();
    while ($tok = shift @lines) {
        if ($tok !~ /^\t/) {
            #put the token back on the input:
            unshift @lines, $tok;
            last;
        }

        #otherwise, we found a tag:
        $tok =~ s/^\s*//;
        push @tokens, $tok;
    }

printf STDERR "\t      found '%s', tokens=(%s)\n", $lookfor, join(",", @tokens) if ($DEBUG_PARSER);

    $$outref{'access_list'} = join(";", @tokens);
    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub symbolic_names
#return 1 if we recognize symbolic_names, otherwise 0
#   symbolic_names -> 'symbolic names:' ('<tab>' '<label>' ':' '<decimal_rev>')*
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "symbolic names:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #Example:
    # symbolic names:
    # \tKenai_Milestone15: 1.1

    #now pick up any following lines containing tags (optional):
    my @tokens = ();
    while ($tok = shift @lines) {
        if ($tok !~ /^\t/) {
            #put the token back on the input:
            unshift @lines, $tok;
            last;
        }

        #otherwise, we found a tag:
        $tok =~ s/^\s*//;
        $tok =~ s/:\s+/=/;   #"blah: 1.1" -> "blah=1.1"
        push @tokens, $tok;
    }

printf STDERR "\t      found '%s', tag list=(%s)\n", $lookfor, join(",", @tokens) if ($DEBUG_PARSER);

    $$outref{'symbolic_names'} = join(";", @tokens);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub locks
#return 1 if we recognize locks, otherwise 0
#   locks -> 'locks:' ('<spaces>' 'strict')? ('<tab>' '<login>' ':' '<decimal_rev>')*
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "locks:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #example (this is for RCS only):
    # locks: strict
    # \trusst: 1.3

    #now pick up any following lines containing lockers (optional; RCS only):
    my @tokens = ();
    while ($tok = shift @lines) {
        if ($tok !~ /^\t/) {
            #put the token back on the input:
            unshift @lines, $tok;
            last;
        }

        #otherwise, we found a locker:
        $tok =~ s/^\s*//;
        $tok =~ s/:\s+/=/;   #"blah: 1.1" -> "blah=1.1"
        push @tokens, $tok;
    }

printf STDERR "\t      found '%s', lockers=(%s)\n", $lookfor, join(",", @tokens) if ($DEBUG_PARSER);

    $$outref{'locks'} = join(";", @tokens);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub revinfo
#return 1 if we recognize revinfo, otherwise 0
#   revinfo -> totalrevs ';' selectedrevs
{
    my ($linesref, $outref) = @_;

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};
    my $tok = shift @lines;

    #split line on ';'
    my @tokens = split(/;/, $tok);

    return 0 unless &totalrevs(\@tokens, $outref);
    return 0 unless &selectedrevs(\@tokens, $outref);

printf STDERR "\t      found non-terminal revinfo\n" if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub description
#return 1 if we recognize description, otherwise 0
#   description -> 'description:' '<line>'* '<dashes>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "description:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    my $text = "";
    my $found = 0;
    while ($tok = shift @lines) {
        if ($tok eq "----------------------------") {
            $found = 1;
            last;
        }
        if ($text eq "") {
            $text = $tok;
        } else {
            $text = $text . "\n" . $tok;
        }
    }

    if (!$found) {
printf STDERR "\tdid not find '%s', tok='%s'\n", "description terminator!", $tok if ($DEBUG_PARSER);
        return 0;
    }
    
printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $text if ($DEBUG_PARSER);

    $$outref{'description'} = $text;

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub deposits
#return 1 if we recognize deposits, otherwise 0
#   deposits -> deposit+
{
    my ($linesref, $dplistref) = @_;

    my @deposits = ();        #gather deposits in this list
    $$dplistref = \@deposits;          #result parameter

    my $depositrefaddr = undef;

    if (!&deposit($linesref, \$depositrefaddr)) {
printf STDERR "\tdid not find non-terminal deposits #lines left=%d\n", $#{$linesref} if ($DEBUG_PARSER);
        return 0;
    }

    push @deposits, $depositrefaddr;    #this should be a reference to a hash.

    if ($#{$linesref} < 0) {
printf STDERR "\t      found non-terminal deposits #lines left=%d\n", $#{$linesref} if ($DEBUG_PARSER);
        return 1 ;
    }

    while (&deposit($linesref, \$depositrefaddr)) {
        push @deposits, $depositrefaddr;    #add new deposit hash ref to our list
        last if ($#{$linesref} < 0);
    }

printf STDERR "\t      found non-terminal deposits #lines left=%d\n", $#{$linesref} if ($DEBUG_PARSER);
    return 1;
}

sub deposit
#return 1 if we recognize deposit, otherwise 0
#   deposit -> deposit_rev deposit_info deposit_message
{
    my ($linesref, $outref) = @_;

    my %deposit = ();        #allocate a hash to save results
    $$outref = \%deposit;

    if (&deposit_rev($linesref, \%deposit) && &deposit_info($linesref, \%deposit)) {
        ;
    } else {
printf STDERR "\tdid not find non-terminal deposit #lines left=%d\n", $#{$linesref} if ($DEBUG_PARSER);
        $$outref = undef;          #no results.
        return 0;
    }

    #this is optional:
    &deposit_branches($linesref, \%deposit);

    if (!&deposit_message($linesref, \%deposit)) {
printf STDERR "\tdid not find non-terminal deposit #lines left=%d\n", $#{$linesref} if ($DEBUG_PARSER);
        #note that we return partial results in this case.
        return 0;
    }

printf STDERR "\t      found non-terminal deposit #lines left=%d\n", $#{$linesref} if ($DEBUG_PARSER);

    return 1;
}

sub deposit_rev
#return 1 if we recognize deposit_rev, otherwise 0
#   deposit_rev -> 'revision' '<decimal_rev>' ('locked by:' '<login>')?
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "revision";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #split $tok to get remaining fields:
    my @tokens = split(/\s+/, $tok);

    my $decimal_rev = shift @tokens;
    $$outref{'deposit_rev'} = $decimal_rev;

    my $locker = "";
    if ($#tokens >= 2 && $tokens[0] eq 'locked' && $tokens[1] eq 'by:') {
        $locker = $tokens[2];
        $$outref{'deposit_locker'} = $locker;
    }

printf STDERR "\t      found '%s', decimal_rev='%s' locker='%s'\n", $lookfor, $decimal_rev, $locker if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub deposit_info
#return 1 if we recognize deposit_info, otherwise 0
#   deposit_info -> deposit_date ';' deposit_author ';' deposit_state ';' deposit_diffcount?
{
    my ($linesref, $outref) = @_;

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};
    my $tok = shift @lines;

    #split line on ';'
    my @tokens = split(/;/, $tok);

    if ( &deposit_date(\@tokens, $outref) && &deposit_author(\@tokens, $outref) && &deposit_state(\@tokens, $outref)) {
        ;
    } else {
printf STDERR "\tdid not find non-terminal deposit_info #tokens left=%d\n", $#tokens if ($DEBUG_PARSER);
        return 0;
    }

    #optional:
    &deposit_diffcount(\@tokens, $outref);

printf STDERR "\t      found non-terminal deposit_info #tokens left=%d\n", $#tokens if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub deposit_message
#return 1 if we recognize deposit_message, otherwise 0
#   deposit_message -> '<line>'* ('<dashes>' | '<EOR>')
{
    my ($linesref, $outref) = @_;

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

    my $text = "";
    my $found = 0;
    my $tok = "";
    while ($tok = shift @lines) {
        if ($tok eq "----------------------------") {
            $found = 1;
            last;
        }
        if ($text eq "") {
            $text = $tok;
        } else {
            $text = $text . "\n" . $tok;
        }
    }

    #if we are not at the end of input and did not find '<dashes>' ... 
    if ($#lines >= 0 && !$found) {
printf STDERR "\tdid not find '%s', tok='%s'\n", "deposit_message", $tok if ($DEBUG_PARSER);
        return 0;
    }
    
printf STDERR "\t      found '%s', text='%s' #lines=%d\n", "deposit_message", $text, $#lines if ($DEBUG_PARSER);
    $$outref{'deposit_message'} = $text;

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub deposit_branches
#return 1 if we recognize deposit_branches, otherwise 0
#   deposit_branches -> 'branches:' '<decimal_rev>' ';' ('<decimal_rev>' ';')*
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "branches:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #split line on ';'
    #example:  branches:  1.4.2;  1.4.4;
    my @tokens = split(/;\s*/, $tok);

printf STDERR "\t      found '%s', branch list=(%s)\n", $lookfor, join(",",@tokens) if ($DEBUG_PARSER);
    $$outref{'deposit_branches'} = join(";", @tokens);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;

    return 1;
}

sub dump_deposits
{
    my ($dplistref) = @_;
printf STDERR "DEPOSIT LIST has %d elements\n", $#{$dplistref}+1 if ($DEBUG_PARSER);
    my $cnt = 0;
    for my $dph (@$dplistref) {
        for my $kk (keys %$dph) {
            printf STDERR "deposit(%d){'%s'}='%s'\n", $cnt, $kk, $$dph{$kk};
        }
        ++$cnt;
    }
}

sub dumphash
{
    my ($thehash, $tracename) = @_;

    if (ref($thehash ) ne "HASH") {
        printf STDERR "ERROR:  dumphash for %s: arg of type '%s' is not a HASH\n", $tracename, ref($thehash);
        return;
    }

    for my $kk (keys %$thehash) {
        printf STDERR "%s{'%s'}='%s'\n", $tracename, $kk, $$thehash{$kk};
    }
}

sub rcsfile
#return 1 if we recognize rcsfile, otherwise 0
#   rcsfile -> 'RCS file:' '<text>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "RCS file:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'rcsfile'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub workingfile
#return 1 if we recognize workingfile, otherwise 0
#   workingfile -> 'Working file:' '<text>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "Working file:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'workingfile'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub headrev
#return 1 if we recognize headrev, otherwise 0
#   headrev -> 'head:' ('<decimal_rev>')?
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "head:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'headrev'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub branchrev
#return 1 if we recognize branchrev, otherwise 0
#   branchrev -> 'branch:' ('<text>')?
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "branch:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'branchrev'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub keywordsubst
#return 1 if we recognize keywordsubst, otherwise 0
#   keywordsubst -> 'keyword substitution:' ('<text>')?
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "keyword substitution:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'keywordsubst'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub totalrevs
#return 1 if we recognize totalrevs, otherwise 0
#   totalrevs -> 'total revisions:' '<int>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "total revisions:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'totalrevs'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub selectedrevs
#return 1 if we recognize selectedrevs, otherwise 0
#   selectedrevs -> 'selected revisions:' '<int>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "selected revisions:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'selectedrevs'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub deposit_date
#return 1 if we recognize deposit_date, otherwise 0
#   deposit_date -> 'date:' '<datestr>' '<timestr>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "date:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'deposit_date'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub deposit_author
#return 1 if we recognize deposit_author, otherwise 0
#   deposit_author -> 'author:' '<login>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "author:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'deposit_author'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub deposit_state
#return 1 if we recognize deposit_state, otherwise 0
#   deposit_state -> 'state:' '<state_value>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "state:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'deposit_state'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub deposit_diffcount
#return 1 if we recognize deposit_diffcount, otherwise 0
#   deposit_diffcount -> 'lines:' '<text>'
{
    my ($linesref, $outref) = @_;
    my ($lookfor) = "lines:";

    #make a local copy of the input list and get the first element:
    my @lines = @{$linesref};

printf STDERR "\tlooking for '%s'...\n", $lookfor if ($DEBUG_PARSER);

    if ($#lines < 0) {
printf STDERR "\tdid not find '%s', at '<EOR>'\n", $lookfor if ($DEBUG_PARSER);
        return 0 ;
    }

    my $tok = shift @lines;

    #trim leading spaces:
    $tok =~ s/^\s*//;

    if ($tok !~ /^$lookfor/) {
printf STDERR "\tdid not find '%s', tok='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);
        return 0;
    }
    
    #eliminate lookfor pattern:
    $tok =~ s/^$lookfor\s*//;

    #save value we found in outref hash:
    $$outref{'deposit_diffcount'} = $tok;

printf STDERR "\t      found '%s', text='%s'\n", $lookfor, $tok if ($DEBUG_PARSER);

    #value-result function:  copy result back to input:
    @{$linesref} = @lines;
    return 1;
}

sub read_file2str
# read a file into a string
# note that the string is passed via reference
# input: (string_to_write_into, infile name)
# output: 0 = okay, 1 = unable to open infile, 3 = read returned error
{
    my ($bufref, $infile) = @_;

    if (!open(INFILE, $infile)) {
        printf STDERR "read_file2str: open for read failed on '%s': '%s'", $infile, $!;
        return(1);
    }

    my(@rec) = stat INFILE;
    my($filesize) = $rec[7];

    $$bufref = "";
    my ($nbytes) = (0,0,"");

    $nbytes = read(INFILE, $$bufref, $filesize);

#printf STDERR "read_file2str filesize=%ld nbytes=%d\n", $filesize, $nbytes;

    if (!defined($nbytes)) {
        #read returns undef on error
        printf STDERR "error reading input file '%s': '%s'", $infile, $!;
        close INFILE;
        return(2);
    }

    close INFILE;

    return(0);
}

my ($TM_SECOND, $TM_MINUTE, $TM_HOUR, $TM_MDAY, $TM_MONTH, $TM_YEAR, $TM_WDAY, $TM_YDAY, $TM_ISDST) =(0..8);
my $TXTM_LENGTH = length("yyyymmddhhmmss");

sub to_gmtxtm
#
#convert unix time to transaction date/time format
#Usage:
#   $theTime = time;
#   $yyyymmddhhmmss = &txtm::to_txtm($theTime);
#
{
    my($gtime) = @_;

    my(@trec) = gmtime($gtime);

    &txtm_init();

    return sprintf("%04d%02d%02d%02d%02d%02d",
        $trec[$TM_YEAR] + 1900,
        $trec[$TM_MONTH] +1,
        $trec[$TM_MDAY],
        $trec[$TM_HOUR],
        $trec[$TM_MINUTE],
        $trec[$TM_SECOND]);
}

sub CalculateFileCRC
#crc binary must be in path.
#Usage:  printf "crc=0x%x\n", &CalculateFileCRC("somefile");
{
    my($filename) = @_;
    my($crc);

    my $out = `crc $filename 2> /dev/null`;
    return undef unless (defined($out));      #failed - can't exec crc

    chomp $out;
    my @out = split(/\s+/, $out);

    #expect:  fn hexcrc
    return hex($out[1]) if ($#out == 1);

    return undef;     #failed.
}


1;
} #end of rlogparser
{
#
#serverconfig - object to hold server configuration
#

use strict;

package serverconfig;
my $pkgname = __PACKAGE__;

#imports:

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    #no args for now:
    #my () = @_;

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mProgName' => undef,
        'mCvsRoot' => undef,
        'mCvsOpts' => undef,
        'mCvsModule' => undef,
        'mCvsModules' => [],
        'mCvsTopModule' => undef,
        'mRlogOpts' => undef,
        'mFsmname' => undef,
        'mFsmEventHost' => undef,
        'mFsmEventPort' => undef,
        'mSvrInterval' => undef,
        'mSvrWorkDir' => undef,
        'mCommitServletUrl' => undef,
        'mCommitSpoolDir' => undef,
        'mLastRlogFn' => undef,
        'mInfRepositoryDir' => undef,
        'mLastRlogTxFn' => undef,
        'mMinNoticeId' => undef,          #ignore all INF notices < mMinNoticeId
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    return $self;
}

################################### PACKAGE ####################################

sub getCommitSpoolDir
#return value of CommitSpoolDir
{
    my ($self) = @_;
    return $self->{'mCommitSpoolDir'};
}

sub setCommitSpoolDir
#set value of CommitSpoolDir and return value.
{
    my ($self, $value) = @_;
    $self->{'mCommitSpoolDir'} = $value;
    return $self->{'mCommitSpoolDir'};
}

sub getCvsRoot
#return value of CvsRoot
{
    my ($self) = @_;
    return $self->{'mCvsRoot'};
}

sub setCvsRoot
#set value of CvsRoot and return value.
{
    my ($self, $value) = @_;
    $self->{'mCvsRoot'} = $value;
    return $self->{'mCvsRoot'};
}

sub getCvsModule
#return value of CvsModule
{
    my ($self) = @_;
    return $self->{'mCvsModule'};
}

sub setCvsModule
#set value of CvsModule and return value.
{
    my ($self, $value) = @_;
    $self->{'mCvsModule'} = $value;
    return $self->{'mCvsModule'};
}

sub getCvsOpts
#return value of CvsOpts
{
    my ($self) = @_;
    return $self->{'mCvsOpts'};
}

sub setCvsOpts
#set value of CvsOpts and return value.
{
    my ($self, $value) = @_;
    $self->{'mCvsOpts'} = $value;
    return $self->{'mCvsOpts'};
}

sub getCvsTopModule
#return value of CvsTopModule
{
    my ($self) = @_;
    return $self->{'mCvsTopModule'};
}

sub setCvsTopModule
#set value of CvsTopModule and return value.
{
    my ($self, $value) = @_;
    $self->{'mCvsTopModule'} = $value;
    return $self->{'mCvsTopModule'};
}

sub getFsmname
#return value of Fsmname
{
    my ($self) = @_;
    return $self->{'mFsmname'};
}

sub setFsmname
#set value of Fsmname and return value.
{
    my ($self, $value) = @_;
    $self->{'mFsmname'} = $value;
    return $self->{'mFsmname'};
}

sub getFsmEventHost
#return value of FsmEventHost
{
    my ($self) = @_;
    return $self->{'mFsmEventHost'};
}

sub setFsmEventHost
#set value of FsmEventHost and return value.
{
    my ($self, $value) = @_;
    $self->{'mFsmEventHost'} = $value;
    return $self->{'mFsmEventHost'};
}

sub getFsmEventPort
#return value of FsmEventPort
{
    my ($self) = @_;
    return $self->{'mFsmEventPort'};
}

sub setFsmEventPort
#set value of FsmEventPort and return value.
{
    my ($self, $value) = @_;
    $self->{'mFsmEventPort'} = $value;
    return $self->{'mFsmEventPort'};
}

sub getRlogOpts
#return value of RlogOpts
{
    my ($self) = @_;
    return $self->{'mRlogOpts'};
}

sub setRlogOpts
#set value of RlogOpts and return value.
{
    my ($self, $value) = @_;
    $self->{'mRlogOpts'} = $value;
    return $self->{'mRlogOpts'};
}

sub getSvrInterval
#return value of SvrInterval
{
    my ($self) = @_;
    return $self->{'mSvrInterval'};
}

sub setSvrInterval
#set value of SvrInterval and return value.
{
    my ($self, $value) = @_;
    $self->{'mSvrInterval'} = $value;
    return $self->{'mSvrInterval'};
}

sub getSvrWorkDir
#return value of SvrWorkDir
{
    my ($self) = @_;
    return $self->{'mSvrWorkDir'};
}

sub setSvrWorkDir
#set value of SvrWorkDir and return value.
{
    my ($self, $value) = @_;
    $self->{'mSvrWorkDir'} = $value;
    return $self->{'mSvrWorkDir'};
}

sub getProgName
#return value of ProgName
{
    my ($self) = @_;
    return $self->{'mProgName'};
}

sub setProgName
#set value of ProgName and return value.
{
    my ($self, $value) = @_;
    $self->{'mProgName'} = $value;
    return $self->{'mProgName'};
}

sub getCommitServletUrl
#return value of CommitServletUrl
{
    my ($self) = @_;
    return $self->{'mCommitServletUrl'};
}

sub setCommitServletUrl
#set value of CommitServletUrl and return value.
{
    my ($self, $value) = @_;
    $self->{'mCommitServletUrl'} = $value;
    return $self->{'mCommitServletUrl'};
}

sub getLastRlogFn
#return value of LastRlogFn
{
    my ($self) = @_;
    return $self->{'mLastRlogFn'};
}

sub setLastRlogFn
#set value of LastRlogFn and return value.
{
    my ($self, $value) = @_;
    $self->{'mLastRlogFn'} = $value;
    return $self->{'mLastRlogFn'};
}

sub getInfRepositoryDir
#return value of InfRepositoryDir
{
    my ($self) = @_;
    return $self->{'mInfRepositoryDir'};
}

sub setInfRepositoryDir
#set value of InfRepositoryDir and return value.
{
    my ($self, $value) = @_;
    $self->{'mInfRepositoryDir'} = $value;
    return $self->{'mInfRepositoryDir'};
}

sub getLastRlogTxFn
#return value of LastRlogTxFn
{
    my ($self) = @_;
    return $self->{'mLastRlogTxFn'};
}

sub setLastRlogTxFn
#set value of LastRlogTxFn and return value.
{
    my ($self, $value) = @_;
    $self->{'mLastRlogTxFn'} = $value;
    return $self->{'mLastRlogTxFn'};
}

sub getMinNoticeId
#return value of MinNoticeId
{
    my ($self) = @_;
    return $self->{'mMinNoticeId'};
}

sub setMinNoticeId
#set value of MinNoticeId and return value.
{
    my ($self, $value) = @_;
    $self->{'mMinNoticeId'} = $value;
    return $self->{'mMinNoticeId'};
}

sub getCvsModules
#return value of @CvsModules
{
    my ($self) = @_;
    return @{$self->{'mCvsModules'}};
}

sub setCvsModules
#set value of CvsModules and return value.
{
    my ($self, @value) = @_;
    $self->{'mCvsModules'} = \@value;
    return @{$self->{'mCvsModules'}};
}


1;
} #end of serverconfig
{
#
#commitdImpl - execution environment and implementation of all commitd states.
#

use strict;

package commitdImpl;
my $pkgname = __PACKAGE__;

#imports:
#use socket i/o to receive events:
use IO::Socket;

#package variables:
#options:
my $p = "UNKNOWN";

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($scfg) = @_;

    $p = $scfg->getProgName();

    #debug:
    printf STDERR "%s:  prog_name='%s' FSMNAME='%s' ", "commitdImpl", $scfg->getProgName(), $scfg->getFsmname();
    printf STDERR "CVSROOT='%s' CVS_OPTS='%s' SVR_WORK_DIR='%s'", $scfg->getCvsRoot(), $scfg->getCvsOpts(), $scfg->getSvrWorkDir();
    printf STDERR "SVR_INTERVAL='%s' CVS_TOP_MODULE='%s' ", $scfg->getSvrInterval(), $scfg->getCvsTopModule();
    printf STDERR "FSM_EVENT_HOST='%s' FSM_EVENT_PORT='%s' ", $scfg->getFsmEventHost(), $scfg->getFsmEventPort();
    printf STDERR "CVS_MODULES=(%s) ", join(",", $scfg->getCvsModules());
    printf STDERR "spooldir='%s' ", $scfg->getCommitSpoolDir();
    printf STDERR "scmRoot='%s' ", $scfg->getInfRepositoryDir();
    printf STDERR "minNoticeId='%d'\n", $scfg->getMinNoticeId();

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        mServerConfig    => $scfg,
        mFsm             => undef,    #this is our FSM object
        mFsmComm         => new fsmcomm ($scfg->getFsmEventHost(), $scfg->getFsmEventPort()),
            #this is the object used to communicate fsm events.
        mCvsRepo      => new cvsrepo($scfg->getCvsRoot(), $scfg->getCvsOpts(), $scfg->getCvsTopModule()),
            #mCvsRepo holds cvs repository attributes (CVSROOT, etc).
        mCkCommitsTimer  => new timer($scfg->getSvrInterval()),
            #timer object to generate timeout event for ckCommits state.
        mCkCommitsPid  => 0,
            #pid of background job running the ckCommits state
        mAwaitCommitsTimerPid  => 0,
            #pid of background timer for AwaitCommits state
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):
    #we can now allocate our FSM object, now that we blessed ourselves:
    $$self{'mFsm'}      = new commitdFsm ($self, $scfg->getFsmname());

    return $self;
}

################################### PACKAGE ####################################

#$SIG{CHLD} = sub {
#    my $pid = wait;
#    printf "%s::SIGCHLD: status for pid %d is $?\n", $pkgname, $pid, $?;
#};

sub discover_commits()
#this is the main server function.  We discover and process cvs commits
#in a remote cvs repository, and then create transactions for pagosa (INF).
#returns non-zero on error.
{
    my ($self) = @_;

    #set up a cvs repo object:
    my $cvsrepo = $self->cvsRepo();
    my $scfg = $self->serverConfig();

    #create a cvs command object to run the rlog (including commit messages) command:
    #usage:  ($cvsrepo, $cmd, $cmdopts, $cvsdir) = @_;
    my ($rlogFullCmd) = new cvscommand(
        $cvsrepo,
        "rlog",
        $scfg->getRlogOpts(),
        sprintf("%s/fullrlog.out", $scfg->getSvrWorkDir()),
        sprintf("%s/fullrlog.err", $scfg->getSvrWorkDir()),
        $scfg->getCvsModules()
    );

    #create a commit spooler object:
    my $timeout = int($scfg->getSvrInterval()/2);
    $timeout    = 10 unless ($timeout < 10);	#set maximum timeout to 10 seconds
    $timeout    =  2 if ($timeout < 2);       #set minimum timeout to 2 seconds

    my $cmdfmtStr = sprintf("wget -v -O - --timeout=%d --tries=3 --waitretry=2 --no-check-certificate --post-file=%s --header=\"Content-type: text/xml\" %s",
        $timeout, "%s", $scfg->getCommitServletUrl());
    my $commitSpooler = new spooler ($cmdfmtStr, $scfg->getCommitSpoolDir(), "commit_msg");

    #create a INF commit processor:
    my ($inftx) = new inftx ($commitSpooler, $scfg);

    #####
    #exec cvs rlog:
    #####
    $rlogFullCmd->exec();

    if ($rlogFullCmd->status() != 0) {
        printf STDERR "ERROR:  rlog command FAILED: with status %s: '%s'\n", $rlogFullCmd->status(), $rlogFullCmd->errorText();
        return 1;
    }

    #create a new rlog object from the output:
    my ($rlogA) = new rlogparser ($rlogFullCmd->transactionTime(), $rlogFullCmd->getStdOutFn(), $cvsrepo->cvsRootDir());

    #create previous rlog:
    my ($rlogB) = new rlogparser ($self->getLastRlogTxTime(), $scfg->getLastRlogFn(), $cvsrepo->cvsRootDir());

    #if the transaction logs are identical (this compares rlog data files, so parse is not needed yet)...
    if ($rlogA->equals($rlogB)) {
        #then we are done.
        printf STDERR "rlog '%s' and rlog '%s' are identical\n", $rlogA->transactionTime(), $rlogB->transactionTime();
        #clear spool if it has something in it:
        $commitSpooler->run();
        return 0;
    }

    #parse the current rlog data:
    if (!$rlogA->parseRlogData()) {
        printf STDERR "Rlog parse failed for rlog[%s], data file '%s'\n", $rlogA->transactionTime(), $rlogA->rlogDataFn();
        return 1;
    }
    #$rlogA->showRlogData();

    #parse the previous rlog data unless it is the first run:
    if ($rlogB->transactionTime() ne "epoch"  && !$rlogB->parseRlogData()) {
        printf STDERR "Rlog parse failed for rlog[%s], data file '%s'\n", $rlogB->transactionTime(), $rlogB->rlogDataFn();
        return 1;
    }
    #$rlogB->showRlogData();

    #create inf transactions for the pagosa commitServlet, for those new revision
    #deposits containing a string identifying an integration notice (IN=<num> or IN=dev):
    if (!$inftx->generateTransactions($rlogA, $rlogB)) {
        printf STDERR "Cannot generate commit transactions!\n";
        return 1;
    }
    #$inftx->showTransactions();

#printf STDERR "TESTING RETURN EARLY - DO NOT spoolTransactions\n";
#return 0;

    #spool commits to a directory (this creates the spool dir if missing).
    #this step should always succeed unless there is a file i/o error.
    #we do not try to send anything at this point.
    if ($inftx->spoolTransactions() != 0) {
        printf STDERR "ERROR:  cannot spool transactions, error '%s'\n", $inftx->spoolErrStr();
        return 1;
    }

    #at this point, we can use the current rlog as the new basis for the next update cycle:
    rename $rlogA->rlogDataFn(), $scfg->getLastRlogFn();

    #update the transaction time:
    $self->updateLastRlogTxTime($rlogA->transactionTime());

    #kick the spooler, which sends each transaction to the commitServlet.
    $commitSpooler->run();

    return 0;
}

sub updateLastRlogTxTime
#update the last rlog transaction time
#return 0 if successful
{
    my ($self, $txtime) = @_;
    my $scfg = $self->serverConfig();

    if (!open(RLOGTXTIME, ">" . $scfg->getLastRlogTxFn())) {
        printf STDERR "%s::lastRlogTxTime: ERROR: cannot open '%s' for write, '%s'\n", $pkgname, $scfg->getLastRlogTxFn(), $!;
        return 1;
    }

    printf RLOGTXTIME "%s\n", $txtime;
    close  RLOGTXTIME;

    return 0;
}

sub getLastRlogTxTime
#read in the time of the last rlog parse from file.
{
    my ($self) = @_;
    my $scfg = $self->serverConfig();

    if (!-r $scfg->getLastRlogTxFn()) {
        return "epoch";    #this is the first run
    }

    my $txtime = undef;
    if (&rlogparser::read_file2str(\$txtime, $scfg->getLastRlogTxFn()) != 0) {
        printf STDERR "%s::lastRlogTxTime: errors reading rlog txtime from '%s'\n", $pkgname, $scfg->getLastRlogTxFn();
        return undef;
    }

    chomp $txtime;
    return $txtime;
}

sub fsmServiceLoop
{
    my ($self) = @_;
    my $scfg = $self->serverConfig();

    my $fsm = $self->fsm();
    my $fsmcomm = $self->fsmComm();

    printf STDERR "BEFORE LOOP:  the current state of FSM %s is: '%s'\n", $fsm->fsmName(), $fsm->currentState();

    my $main_socket = IO::Socket::INET->new (
                      LocalAddr => $scfg->getFsmEventHost(),
                      LocalPort => $scfg->getFsmEventPort(),
                      Listen    => 5,
                      Proto     => 'tcp',
                      Reuse     => 1);
    die "$p: Could not create socket: $! \n" unless $main_socket;

    $fsmcomm->setEventListenSocket($main_socket);

    printf STDERR "socket created, accepting connections...";

    #process any pending events:
    $fsm->processEvents();

    ##########
    #MAIN LOOP:
    ##########

    while (my $new_sock = $main_socket->accept()) {
        print STDERR "$p:  have connection...\n";

        while ( defined (my $event = <$new_sock>) ) {
            chomp $event;

            if (!$fsm->isValidEvent($event)) {
                printf STDERR "%s:  invalid event, '%s'", $p, $event;
                #printf STDERR "\tValid events are: (%s)\n", join(",", $fsm->getValidEventList());
                printf STDERR "\tValid events for '%s' are: (%s)\n",
                    $fsm->currentState(), join(",", $fsm->getValidNextEvents());
                next;
            }

            #otherwise, we have a valid event name:
            $fsm->enqueueEvent($event);
            $fsm->processEvents();   #this can fork new processes
            #process any secondary events generated:
            $fsm->processEvents();

    printf STDERR "The current state of FSM %s is: '%s'\n", $fsm->fsmName(), $fsm->currentState();
            last if ( $fsm->currentState() eq $fsm->haltStateName() );
        }

        if ( $fsm->currentState() eq $fsm->haltStateName() ) {
            printf STDERR "\ngood bye to %s!\n", $fsm->fsmName();
            last;
        }

        #othewise, wait for another connection:
        printf STDERR "back to accept..\n";
    }

    $fsmcomm->closeEventListenSocket();

    return 0;
}

#########
#accessor methods for commitdImpl object attributes:
#########

sub fsm
#return value of mFsm
{
    my ($self) = @_;
    return $self->{'mFsm'};
}

sub fsmComm
#return value of mFsmComm
{
    my ($self) = @_;
    return $self->{'mFsmComm'};
}

sub ckCommitsTimer
#return value of mCkCommitsTimer
{
    my ($self) = @_;
    return $self->{'mCkCommitsTimer'};
}

sub cvsRepo
#return value of mCvsRepo
{
    my ($self) = @_;
    return $self->{'mCvsRepo'};
}

sub serverConfig
#return value of mServerConfig
{
    my ($self) = @_;
    return $self->{'mServerConfig'};
}

sub getCkCommitsPid
#return value of CkCommitsPid
{
    my ($self) = @_;
    return $self->{'mCkCommitsPid'};
}

sub setCkCommitsPid
#set value of CkCommitsPid and return value.
{
    my ($self, $value) = @_;
    $self->{'mCkCommitsPid'} = $value;
    return $self->{'mCkCommitsPid'};
}

sub getAwaitCommitsTimerPid
#return value of AwaitCommitsTimerPid
{
    my ($self) = @_;
    return $self->{'mAwaitCommitsTimerPid'};
}

sub setAwaitCommitsTimerPid
#set value of AwaitCommitsTimerPid and return value.
{
    my ($self, $value) = @_;
    $self->{'mAwaitCommitsTimerPid'} = $value;
    return $self->{'mAwaitCommitsTimerPid'};
}


1;
} #end of commitdImpl
{
#
#fsmcomm - handle communications to/from a fsm
#

use strict;

package fsmcomm;
my $pkgname = __PACKAGE__;

#imports:
#use socket i/o to receive events:
use IO::Socket;

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($fsmhost, $fsmport) = @_;

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mFsmPort' => $fsmport,
        'mFsmHost' => $fsmhost,
        'mEventListenSocket' => 0,
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    return $self;
}

################################### PACKAGE ####################################
sub closeEventListenSocket
#if the event socket is open (non-zero), then close it.
{
    my ($self) = @_;

    my $sock = $self->getEventListenSocket();

    close $sock unless ($sock == 0);
}

sub send_event
#open a connection to state machine, send a single event, and close connection.
{
    my ($self, $event) = @_;

    my $main_socket = IO::Socket::INET->new (
                      PeerAddr => $self->fsmHost(),
                      PeerPort => $self->fsmPort(),
                      Proto     => 'tcp'
                      );

    if (!defined($main_socket)) {
        printf STDERR "%s:  could not create socket '%s:%s': '%s'.\n",
            "fsmcomm", $self->fsmHost(), $self->fsmPort(), $!;
        return;
    }

#printf STDERR "client:  socket created, writing messages...";
#printf STDERR "%s:  Writing '%s' to server\n", $p, $event;
    print $main_socket "$event\n";
    close $main_socket;
}

sub fsmPort
#return value of mFsmPort
{
    my ($self) = @_;
    return $self->{'mFsmPort'};
}

sub fsmHost
#return value of mFsmHost
{
    my ($self) = @_;
    return $self->{'mFsmHost'};
}

sub getEventListenSocket
#return value of EventListenSocket
{
    my ($self) = @_;
    return $self->{'mEventListenSocket'};
}

sub setEventListenSocket
#set value of EventListenSocket and return value.
{
    my ($self, $value) = @_;
    $self->{'mEventListenSocket'} = $value;
    return $self->{'mEventListenSocket'};
}


1;
} #end of fsmcomm
{
#
#timer - simple timer object for commitd
#

use strict;

package timer;
my $pkgname = __PACKAGE__;

#imports:

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($interval) = @_;

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mIntervalSeconds' => $interval,
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    return $self;
}

################################### PACKAGE ####################################
sub reset
#reset the timer clock
{
    my ($self) = @_;
    return 0;
}

sub wait
#wait for the specified interval
{
    my ($self) = @_;
    sleep($self->{'mIntervalSeconds'});
    return 0;
}

sub getIntervalSeconds
#return value of IntervalSeconds
{
    my ($self) = @_;
    return $self->{'mIntervalSeconds'};
}

sub setIntervalSeconds
#set value of IntervalSeconds and return value.
{
    my ($self, $value) = @_;
    $self->{'mIntervalSeconds'} = $value;
    return $self->{'mIntervalSeconds'};
}


1;
} #end of timer
{
#
#commitdFsm - finite state machine to control commitd server
#

use strict;

package commitdFsm;
my $pkgname = __PACKAGE__;

#imports:

#package variables:
#we are a sub-class of the fsm package:
no strict; @ISA = ("fsm"); use strict;

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($fsmimpl, $fsmname ,$isDebug, $isVerbose) = @_;
    $fsmname = "BOGUS_FSM" unless defined($fsmname);
    $isDebug = 0 unless defined($isDebug);
    $isVerbose = 0 unless defined($isVerbose);

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):
    #######
    #extend base class  object attributes:
    #######

    #re-bless using base class:
    $self = $class->newbase($fsmimpl, $fsmname ,$isDebug, $isVerbose);

    #additional events:
    $$self{'mTimeoutEventName'} = 'TIMEOUT';
    $$self{'mCkCommitsStartedEventName'} = 'CKCOMMITSSTARTED';
    $$self{'mCkCommitsDoneEventName'} = 'CKCOMMITSDONE';
    $$self{'mAbortCkCommitsEventName'} = 'ABORTCKCOMMITS';
    $$self{'mSuccessEventName'} = 'SUCCESS';
    $$self{'mCkCommitsRunningEventName'} = 'CKCOMMITSRUNNING';
    $$self{'mCkCommitsIdleEventName'} = 'CKCOMMITSIDLE';
    $$self{'mStatusEventName'} = 'STATUS';

    #additional States:
    $$self{'mCkCommitsStateName'} = 'CkCommitsState';
    $$self{'mAwaitCommitsStateName'} = 'AwaitCommitsState';
    $$self{'mCkCommitsRunningStateName'} = 'CkCommitsRunningState';
    $$self{'mKillCkCommitsStateName'} = 'KillCkCommitsState';
    $$self{'mHaltCkCommitsStateName'} = 'HaltCkCommitsState';
    $$self{'mHaltKillCkCommitsStateName'} = 'HaltKillCkCommitsState';
    $$self{'mResetCkCommitsStateName'} = 'ResetCkCommitsState';
    $$self{'mResetKillCkCommitsStateName'} = 'ResetKillCkCommitsState';
    $$self{'mIgnoreTransitionStateName'} = 'IgnoreTransitionState';

    my $f = $self;
    #int fsm object:  curr                      event                   next state             objref  state name
    #                 ------------------------  --------------------    ------------------     -----  -----------
    $f->addTransition($f->nullStateName(),	$f->initEventName(),	$f->initStateName(),	$f,	"initState");
    $f->addTransition($f->initStateName(),	$f->startEventName(),	$f->ckCommitsStateName(),	$f,	"ckCommitsState");
    $f->addTransition($f->awaitCommitsStateName(),	$f->timeoutEventName(),	$f->ckCommitsStateName(),	$f,	"ckCommitsState");
    $f->addTransition($f->ckCommitsStateName(),	$f->ckCommitsStartedEventName(),	$f->ckCommitsRunningStateName(),	$f,	"ckCommitsRunningState");
    $f->addTransition($f->ckCommitsRunningStateName(),	$f->ckCommitsDoneEventName(),	$f->awaitCommitsStateName(),	$f,	"awaitCommitsState");
    $f->addTransition($f->ckCommitsRunningStateName(),	$f->abortCkCommitsEventName(),	$f->killCkCommitsStateName(),	$f,	"killCkCommitsState");
    $f->addTransition($f->ckCommitsRunningStateName(),	$f->timeoutEventName(),	$f->killCkCommitsStateName(),	$f,	"killCkCommitsState");
    $f->addTransition($f->killCkCommitsStateName(),	$f->successEventName(),	$f->awaitCommitsStateName(),	$f,	"awaitCommitsState");
    $f->addTransition($f->anyStateName(),	$f->haltEventName(),	$f->haltCkCommitsStateName(),	$f,	"haltCkCommitsState");
    $f->addTransition($f->haltCkCommitsStateName(),	$f->ckCommitsRunningEventName(),	$f->haltKillCkCommitsStateName(),	$f,	"haltKillCkCommitsState");
    $f->addTransition($f->haltCkCommitsStateName(),	$f->ckCommitsIdleEventName(),	$f->haltStateName(),	$f,	"haltState");
    $f->addTransition($f->haltKillCkCommitsStateName(),	$f->successEventName(),	$f->haltStateName(),	$f,	"haltState");
    $f->addTransition($f->anyStateName(),	$f->resetEventName(),	$f->resetCkCommitsStateName(),	$f,	"resetCkCommitsState");
    $f->addTransition($f->resetCkCommitsStateName(),	$f->ckCommitsIdleEventName(),	$f->initStateName(),	$f,	"initState");
    $f->addTransition($f->resetCkCommitsStateName(),	$f->ckCommitsRunningEventName(),	$f->resetKillCkCommitsStateName(),	$f,	"resetKillCkCommitsState");
    $f->addTransition($f->resetKillCkCommitsStateName(),	$f->successEventName(),	$f->initStateName(),	$f,	"initState");
    $f->addTransition($f->anyStateName(),	$f->statusEventName(),	$f->ignoreTransitionStateName(),	$f,	"ignoreTransitionState");
    #                 ------------------------  --------------------    ------------------     -----  -----------

    return $self;
}

################################### PACKAGE ####################################

#######
#unique states implemented by this FSM:
#######

sub ckCommitsState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->ckCommitsStateName();

    #wait for any previous children to prevent zombie processes:
    my $lastkid = $self->fsmImpl()->getCkCommitsPid();
    if ($lastkid != 0) {
        waitpid $lastkid,0;
        $self->fsmImpl()->setCkCommitsPid(0);
    }

    if ( defined(my $childpid = fork()) ) {
        if ($childpid == 0) {
            #this is the child process.
            #get a handle to fsmComm:
            my $fsmComm = $self->fsmImpl()->fsmComm();

            #close listen port:
            $fsmComm->closeEventListenSocket();

            ######
            #check for commits:
            ######

            $self->fsmImpl()->discover_commits();

            #commits done - send event via client socket:
            $fsmComm->send_event($self->ckCommitsDoneEventName());

            #####
            #exit child:
            #####
            exit 0;
        } else {
            #parent - save pid, enqueue ckCommitsRunning event:
            $self->fsmImpl()->setCkCommitsPid($childpid);
            $self->enqueueEvent($self->ckCommitsStartedEventName());

            return 0;
        }
    } else {
        #fork failed
        return 1;    #error
    }

    return 0;
}

sub awaitCommitsState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->awaitCommitsStateName();

    #wait for any previous children to prevent zombie processes:
    my $lastkid = $self->fsmImpl()->getAwaitCommitsTimerPid();
    if ($lastkid != 0) {
        waitpid $lastkid,0;
        $self->fsmImpl()->setAwaitCommitsTimerPid(0);
    }

    if ( defined(my $childpid = fork()) ) {
        if ($childpid == 0) {
            #this is the child process.

            #get a handle to fsmComm:
            my $fsmComm = $self->fsmImpl()->fsmComm();

            #close listen port:
            $fsmComm->closeEventListenSocket();

            ######
            #TIMER
            ######
            #get a handle to timer:
            my $timer = $self->fsmImpl()->ckCommitsTimer();

            $timer->wait();

            #wait done - send event via client socket:
            $fsmComm->send_event($self->timeoutEventName());

            #####
            #exit child:
            #####
            exit 0;
        } else {
            #parent - save pid:
            $self->fsmImpl()->setAwaitCommitsTimerPid($childpid);

            return 0;
        }
    } else {
        #fork failed
        return 1;    #error
    }

    return 0;
}

sub ckCommitsRunningState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->ckCommitsRunningStateName();

    return 0;
}

sub killCkCommitsState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->killCkCommitsStateName();

    return 0;
}

sub haltCkCommitsState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->haltCkCommitsStateName();

    #haltCkCommitsBody - kill currently running ckCommits:
    my $pid = 0;
    $pid = $self->fsmImpl()->getCkCommitsPid();
    printf STDERR "haltCkCommitsBody CkCommitsPid=%s\n", $pid;

    if ($pid != 0) {
        kill 'INT', $pid; kill 9, $pid; waitpid $pid,0;
        $self->fsmImpl()->setCkCommitsPid(0);
    }

    $pid = $self->fsmImpl()->getAwaitCommitsTimerPid();
    printf STDERR "haltCkCommitsBody AwaitCommitsTimerPid=%s\n", $pid;
    if ($pid != 0) {
        kill 'INT', $pid; kill 9, $pid; waitpid $pid,0;
        $self->fsmImpl()->setAwaitCommitsTimerPid(0);
    }
    #enqueue a "SUCCESS" event:
    $self->enqueueEvent($self->successEventName());

    return 0;
}

sub haltKillCkCommitsState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->haltKillCkCommitsStateName();

    return 0;
}

sub resetCkCommitsState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->resetCkCommitsStateName();

    return 0;
}

sub resetKillCkCommitsState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->resetKillCkCommitsStateName();

    return 0;
}

sub ignoreTransitionState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->ignoreTransitionStateName();

    return 0;
}

#########
#standard states implemented by all FSM's:
#########

sub haltState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->haltStateName();

    return 0;
}

sub initState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->initStateName();

    return 0;
}

sub resetState
{
    my ($self) = @_;
    printf STDERR "We are now in the %s\n", $self->resetStateName();

    return 0;
}

#########
#accessor methods for event and state names:
#########

sub timeoutEventName
#return value of mTimeoutEventName
{
    my ($self) = @_;
    return $self->{'mTimeoutEventName'};
}

sub ckCommitsStartedEventName
#return value of mCkCommitsStartedEventName
{
    my ($self) = @_;
    return $self->{'mCkCommitsStartedEventName'};
}

sub ckCommitsDoneEventName
#return value of mCkCommitsDoneEventName
{
    my ($self) = @_;
    return $self->{'mCkCommitsDoneEventName'};
}

sub abortCkCommitsEventName
#return value of mAbortCkCommitsEventName
{
    my ($self) = @_;
    return $self->{'mAbortCkCommitsEventName'};
}

sub successEventName
#return value of mSuccessEventName
{
    my ($self) = @_;
    return $self->{'mSuccessEventName'};
}

sub ckCommitsRunningEventName
#return value of mCkCommitsRunningEventName
{
    my ($self) = @_;
    return $self->{'mCkCommitsRunningEventName'};
}

sub ckCommitsIdleEventName
#return value of mCkCommitsIdleEventName
{
    my ($self) = @_;
    return $self->{'mCkCommitsIdleEventName'};
}

sub statusEventName
#return value of mStatusEventName
{
    my ($self) = @_;
    return $self->{'mStatusEventName'};
}

sub ckCommitsStateName
#return value of mCkCommitsStateName
{
    my ($self) = @_;
    return $self->{'mCkCommitsStateName'};
}

sub awaitCommitsStateName
#return value of mAwaitCommitsStateName
{
    my ($self) = @_;
    return $self->{'mAwaitCommitsStateName'};
}

sub ckCommitsRunningStateName
#return value of mCkCommitsRunningStateName
{
    my ($self) = @_;
    return $self->{'mCkCommitsRunningStateName'};
}

sub killCkCommitsStateName
#return value of mKillCkCommitsStateName
{
    my ($self) = @_;
    return $self->{'mKillCkCommitsStateName'};
}

sub haltCkCommitsStateName
#return value of mHaltCkCommitsStateName
{
    my ($self) = @_;
    return $self->{'mHaltCkCommitsStateName'};
}

sub haltKillCkCommitsStateName
#return value of mHaltKillCkCommitsStateName
{
    my ($self) = @_;
    return $self->{'mHaltKillCkCommitsStateName'};
}

sub resetCkCommitsStateName
#return value of mResetCkCommitsStateName
{
    my ($self) = @_;
    return $self->{'mResetCkCommitsStateName'};
}

sub resetKillCkCommitsStateName
#return value of mResetKillCkCommitsStateName
{
    my ($self) = @_;
    return $self->{'mResetKillCkCommitsStateName'};
}

sub ignoreTransitionStateName
#return value of mIgnoreTransitionStateName
{
    my ($self) = @_;
    return $self->{'mIgnoreTransitionStateName'};
}


1;
} #end of commitdFsm
{
#
#commitd - monitor a remote cvs server for commit transactions.
#

use strict;

package commitd;
my $pkgname = __PACKAGE__;

#imports:

#standard global options:
my $p = $main::p;
my ($VERBOSE, $HELPFLAG, $DEBUGFLAG, $DDEBUGFLAG, $QUIET) = (0,0,0,0,0);

#package global variables:
my $scfg = new serverconfig();

&init;      #init globals

##################################### MAIN #####################################

sub main
{
    local(*ARGV, *ENV) = @_;

    &init;      #init globals

    return (1) if (&parse_args(*ARGV, *ENV) != 0);
    return (0) if ($HELPFLAG);


    #create an commitd server implementation:
    my $main = new commitdImpl($scfg);

    return $main->fsmServiceLoop();

    return 0;
}

################################### PACKAGE ####################################


#################################### USAGE #####################################

sub usage
{
    my($status) = @_;

    print STDERR <<"!";
Usage:  $p [options] cvs_directory_or_module ...

SYNOPSIS
  Periodically check for commits in a remote CVS repository
  in the named cvs directories or modules.

OPTIONS
  -help             Display this help message.
  -verbose          Display additional informational messages.
  -debug            Display debug messages.
  -ddebug           Display deep debug messages.
  -quiet            Display severe errors only.

  -work <dir>       working directory for the server to store data
  -spooldir <dir>   where to spool transactions to send to commit servlet.
  -interval <secs>  the interval in seconds to check for cvs commits.  DEFAULT is 60
  -port <number>    the port number that the commitdcli client connects to. Defaults to 1936.
  -host <name>      the host name that commitdcli client connects to. Defaults to "localhost".
  -url <str>        Url of the INF Commit Servlet
  -minnotice <num>  Ignore all integrations before <num>

  -cvsroot <str>    CVSROOT setting
  -scmroot <str>    Source repository directory configured for this product in the INF.
                    Usually the same as the CVS root dir but not always.
  -cvstop  <str>    top CVS module or directory name.  Defaults to ".".
  -cvsopts <str>    options to pass to the cvs server.  Default options are:  -z3
  -cvsroot <str>    CVSROOT setting

ENVIRONMENT
  CVSROOT - the remote cvs server.  can also be specified with -cvsroot, which will override env. setting.

EXAMPLE
  $p -host "localhost" -port 1936 -url http://mojave/inf/commit -cvsroot ":pserver:guest\@cvs.dev.java.net:/shared/data/ccvs/repository" -cvsopts "-z3" -work /var/spool/$p -translog /CVSCHROOT/commit_logs/openesb_commits.log open-esb
!
    return ($status);
}

sub parse_args
#proccess command-line aguments
{
    local(*ARGV, *ENV) = @_;

    #set defaults:
    $scfg->setProgName($p);

    $scfg->setCvsRoot(":pserver:guest\@cvs.dev.java.net:/shared/data/ccvs/repository");
    $scfg->setCvsTopModule("pagosa");
    $scfg->setCvsOpts("-z3");
    $scfg->setRlogOpts("-N");
    $scfg->setSvrWorkDir(".");
    $scfg->setSvrInterval(60);
    $scfg->setCommitSpoolDir("spool/commitq");
    $scfg->setFsmname("commitdFsm");
    $scfg->setFsmEventHost("localhost");
    $scfg->setFsmEventPort(1936);
    $scfg->setCommitServletUrl("http://mojave/inf/commit");
    $scfg->setInfRepositoryDir("/sunsetcvs/java-net");
    $scfg->setMinNoticeId(0);

    #eat up flag args:
    my ($flag);
    while ($#ARGV+1 > 0 && $ARGV[0] =~ /^-/) {
        $flag = shift(@ARGV);

        if ($flag =~ '^-debug') {
            $DEBUGFLAG = 1;
        } elsif ($flag =~ '^-host') {
            #-host hostname
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setFsmEventHost(shift(@ARGV));
            } else {
                printf STDERR "%s:  -host requires a host name\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-port') {
            #-port server_port
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setFsmEventPort(shift(@ARGV));
        printf STDERR "HELLO WORLD:  port is set to '%s'\n", $scfg->getFsmEventPort();
            } else {
                printf STDERR "%s:  -port requires a port number\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-i') {
            #-interval <seconds>
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setSvrInterval(shift(@ARGV));
            } else {
                printf STDERR "%s:  -interval requires the number of seconds\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-min') {
            # -minnotice <num>  Ignore all integrations before <num>
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setMinNoticeId(shift(@ARGV));
            } else {
                printf STDERR "%s:  -minnotice requiresthe minimum INF notice number to process\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-url') {
            # -url <str>        Url of the INF Commit Servlet
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setCommitServletUrl(shift(@ARGV));
            } else {
                printf STDERR "%s:  -url requires the url of the INF Commit Servlet\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-spool') {
            #-spooldir <dir>
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setCommitSpoolDir(shift(@ARGV));
            } else {
                printf STDERR "%s:  -spooldir requires a directory name\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-scmroot') {
            #-scmroot inf_scm_dir
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setInfRepositoryDir(shift(@ARGV));
            } else {
                printf STDERR "%s:  -scmroot requires the name of the source repository dir configured in the INF.\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-cvstop') {
            #-cvstop <module_or_dir>
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setCvsTopModule(shift(@ARGV));
            } else {
                printf STDERR "%s:  -cvstop requires a top cvs directory or module name\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-cvsr') {
            #-cvsroot <CVSROOT>
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setCvsRoot(shift(@ARGV));
            } else {
                printf STDERR "%s:  -cvsroot requires a valid CVSROOT setting\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-cvso') {
            #-cvsopts <opts>
            if ($#ARGV+1 > 0) {
                $scfg->setCvsOpts(shift(@ARGV));
            } else {
                printf STDERR "%s:  -cvsopts requires cvs option flags\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-w') {
            #-work <dir>
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $scfg->setSvrWorkDir(shift(@ARGV));
            } else {
                printf STDERR "%s:  -work requires a directory name\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-dd') {
            $DDEBUGFLAG = 1;
        } elsif ($flag =~ '^-v') {
            $VERBOSE = 1;
        } elsif ($flag =~ '^-h') {
            $HELPFLAG = 1;
            return &usage(0);
        } else {
            printf STDERR "%s:  unrecognized option, '%s'\n", $p, $flag;
            return &usage(1);
        }
    }

    #eliminate empty args (this happens on some platforms):
    @ARGV = grep(!/^$/, @ARGV);

    if ($#ARGV < 0) {
        printf STDERR "%s:  you must supply at least one top cvs directory or module name.\n", $p;
        return 4;
    } else {
        $scfg->setCvsModules(@ARGV);
    }

    #set files for saving state to root of work dir:
    $scfg->setLastRlogFn(sprintf("%s/%s", $scfg->getSvrWorkDir(), ".last_rlog_data"));
    $scfg->setLastRlogTxFn(sprintf("%s/%s", $scfg->getSvrWorkDir(), ".last_rlog_time"));
    return 0;
}

################################ INITIALIZATION ################################

sub init
{
}

sub cleanup
{
}
1;
} #end of commitd
