#
# BEGIN_HEADER - DO NOT EDIT
# 
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)fortepj.rc - ver 1.1 - 01/04/2006
#
# Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
# 
# END_HEADER - DO NOT EDIT
#

#common defs for Forte VSPMS users:
#
# 04-Dec-96 (russt)
#   created
# 23-Oct-00 (russt)
#   revised for CVS
#

#g=rwx, o=r-x
umask 002

alias fortepjhelp '(setenv FORTEPJ_HELP_ONLY 1; csh -f $TOOLROOT/lib/cmn/fortepj.rc)'
if ($?FORTEPJ_HELP_ONLY) then
    setenv FORTEPJ_HELP 1
    goto FORTEPJ_HELP_ONLY
endif

if !($?FORTEPJ_HELP) then
    alias showhelpusage 'echo HELP COMMAND is fortepjhelp'
    #don't do long help:
    setenv FORTEPJ_HELP 0
else
    alias showhelpusage ""
endif

#aliases to get source dir of to a tool or auxiliary file:
alias getsrcdir dirname '`wheresrc \!* |' awk \'NR == 1 { print '$2' }\''`'

#aliases to find the source to a test
alias gettstsrvc 'basename \!^ | sed -e' "'"'s/\(..\).*/\1/'"'"
alias wheretst '(cd $SRCROOT; find `gettstsrvc \!^`/regress -name \!^)'
alias getblddate  'setenv BLDDATE "`if (-r $SRCROOT/regress/.date) cat $SRCROOT/regress/.date`"'
alias showblddate  'getblddate; if ($BLDDATE != "") echo LAST BUILD $BLDDATE'

#regular cvs working dir:
alias showsrcpath 'echo "SRCROOT:  $SRCROOT"; echo "CVSROOT:  $CVSROOT"'

############
# csh note -
#   an alias that attempts:  "if ($?FOO) echo $FOO"
#   will get "FOO undefined" since the entire line is evaluated
#   unconditionally prior to if expression.  We avoid this error by
#   adding a secondary alias that does the variable reference.
# RT 3/26/98
############
alias echolinkroot 'echo $FORTE_LINKROOT'
alias showlinkroot 'if ($?FORTE_LINKROOT) echo FORTE_LINKROOT is `echolinkroot`'
alias echotoolroot 'echo $TOOLROOT'
alias showtoolroot 'if ($?TOOLROOT) echo TOOLROOT is `echotoolroot`'

alias pathinfo  'showsrcpath; showtoolroot; showlinkroot; showblddate; showhelpusage'

if ($?VSPMS_REVISION) then
    alias srcdir   'subpj `getsrcdir \!*`'
    alias regdir   'set tmp = `wheretst \!^`; subpj `dirname $tmp`'
    alias gosrc    'subpj `echo $SBPJ | sed s./tst/./src/. | sed s./regress/./src/.`'
    alias gotst    'subpj `echo $SBPJ | sed s./src/./tst/. | sed s./regress/./tst/.`'
    alias goreg    'subpj `echo $SBPJ | sed s./src/./regress/. | sed s./tst/./regress/.`'

    alias golog    'getblddate; subpj regress/log/$BLDDATE'
    alias goreglog 'getblddate; subpj regress/$FORTE_PORT/$BLDDATE/\!*'

    ### Note that REV is a VSPMS variable, and is already exported.  RT 11/9/00
    if ($?CVS_BRANCH_NAME) then
        setenv REV "$CVS_BRANCH_NAME"
    else if ($?PATHNAME) then
        setenv REV "$PATHNAME"
    else
        setenv REV "NULL"
    endif
else
    echo WARNING - VSPMS not installed
endif

#CVS ALIASES:
alias checkOut "cvs edit"
alias checkIn "echo checkIn is not available"
alias uncheckOut "cvs unedit"
alias newFile "cvs add"
alias newDir "mkdir -p \!^; newfile \!^"
alias updatePath "cvs update -d -P"
alias integratePath "cvs commit"
alias includeService "cvs checkout"
alias deleteFile "cvs remove -f"
alias deleteService "cvs release -d"

#shortcuts.  make sure you update these when you update above.
#do not redirect to above aliases to make it easier for the
#user to get the definition.
alias pco "cvs edit"
alias pci "echo pci is not available"
alias uco "cvs unedit"
alias newfile "cvs add"
alias newdir "mkdir -p \!^; newfile \!^"
alias update "cvs update -d -P"
alias integrate "cvs commit"

alias cvsdiff "cvs diff"
alias cvslog "cvs log"

alias pull '$TOOLROOT/boot/updateDist'

######
#maven common aliases:
######
alias smaven 'maven -b -DSRCROOT=$JV_SRCROOT -Dmaven.home.local=$JV_SRCROOT/maven'
alias smavencb  'smaven -Dgoal=clean,jar:install multiproject:goal'
alias smavencbt  'smaven -Dmaven.test.skip=false -Dgoal=clean,jar:install,test multiproject:goal'
alias smaventest 'smaven -Dmaven.test.skip=false test'

#######
#maven2 common aliases:
#######
alias smvn 'mvn -Dmaven.test.skip=true -Dmaven.repo.local=$JV_SRCROOT/m2/repository'
alias smvntest 'mvn -Dmaven.test.skip=false -Dmaven.repo.local=$JV_SRCROOT/m2/repository'
alias helpplugin "smvn -Dplugin=\!^ -Dfull=true help:describe"
alias helppom smvn help:effective-pom
alias helpsettings smvn help:effective-settings
alias helpprofiles smvn help:active-profiles

#####
#test result common aliases (jregress/junit):
#####
alias jres "egrep 'Passed|FAILED|TIMED OUT'"
#include junit results as well
alias tres "egrep 'Tests run:|Passed|FAILED|TIMED OUT'"

set opt=`optpath ` >& /dev/null
if ($status == 0 && "$opt" != "") then
    setenv PATH $opt
else
    echo WARNING:  \$PATH optimizization failed (optpath command).
endif

#####
#NOTE - the aliases below are used by the help command:
#####

FORTEPJ_HELP_ONLY:

alias onecol perl -n -a -F"'"'/\s+|:|;/'"'" -e "'"'{for (@F) {print "$_\n";}}'"'"
alias addplus perl -n -e "'"'{printf "+%d %s", $ii++, $_;}'"'"
alias pdirs 'dirs|onecol|addplus'
alias showpath 'echo $PATH|onecol'

#####
#NOTE - if you define a "setprompt" alias, then this will execute it:
#####
set xx="`alias setprompt`"
if ("$xx" != "") then
    setprompt
endif
unset xx

##################
#PERL USAGE NOTES:
#   -n opens each arg and adds  while (<>) loop.
#   -a auto-splits each line into @F as specified by -F pattern (or defaults to /\s+/
#   -e adds in the specified script - executed once per input line.
##################

if ($FORTEPJ_HELP != "0") then
    more << EOF

=============================================================================
====================== STANDARD FORTE PROJECT SETTINGS ======================
=============================================================================

Standard command aliases:

### CVS aliases for old FORTE path tools:
checkOut,pco        - cvs edit [files]
uncheckOut,uco      - cvs unedit [files]
checkIn,pci     - not available
integratePath,integrate - cvs commit [directories]
updatePath,update   - cvs update -d -P [directories]
newDir,newdir       - mkdir; cvs add [directory]
newFile,newfile     - cvs add [files]
includeService      - cvs checkout [modules]
deleteService       - cvs release -d [directories]

### CVS shortcuts:
cvsdiff - cvs diff
cvslog  - cvs log

### update your tools:
pull      - runs \$TOOLROOT/boot/updateDist (updates tools)

Informational:

pathinfo - show usePath & other important environment settings.
pdirs    - output from "dirs" in a single column (useful with pushd/popd).
showpath - displays \$PATH settings in single column.

More info... (pathinfo does all of these):

showsrcpath   - brief usePath info.
showblddate   - displays \$SRCROOT/regress/.date if defined.
showlinkroot  - show FORTE_LINKROOT setting if defined.
showtoolroot  - show TOOLROOT setting.
showhelpusage - show help command usage.  See FORTEPJ_HELP below.

Search:

getsrcdir - get the directory where a source file is located (uses wheresrc).
wheretst  - find the source location for a .scr or .ref file.

Search & GO (These require VSPMS.  See ~russt/bin/installpj.):

srcdir - look up a tools/release file and go to the source dir.
regdir - find the source dir for a .scr or .ref file and go there.
gosrc  - if you are in a tst or regress dir, go to corresponding src dir.
gotst  - if you are in a src or regress dir, go to corresponding tst dir.
goreg  - if you are in a tst or src dir, go to corresponding regress dir.

The current COMMAND PATH setting is:

`showpath`

=============================================================================
EOF
endif

unsetenv FORTEPJ_HELP_ONLY
