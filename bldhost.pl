#!/bin/perl

my $prod ;
if ( !@ARGV ){
    $prod ='shasta';
}
else {
    $prod = shift(@ARGV);
}

%FULL_PRODUCT_NAME = (
    'shasta', 'Open Enterprise Service Bus',
    'lassen', 'Java Business Integration (JBI) Reference Implementation',
    'whitney', 'Java Business Integration (JBI) Technology Preview',
    'openesb', 'Open Enterprise Service Bus',
);

%SHORT_PRODUCT_NAME = (
    'shasta', 'Open_ESB',
    'lassen', 'JBI_RI',
    'whitney', 'JBI_Preview',
    'openesb', 'Open_ESB',
);

%MAJOR_VERSION = (
    'shasta', '2',
    'lassen', '1',
    'whitney', '1',
    'openesb', '2',
);

%MINOR_VERSION = (
    'shasta', '3.0',
    'lassen', '1.0',
    'whitney', '1.0',
    'openesb', '3.0',
);

#this is the open-esb rollout tag for now.  RT 2/20/05
%MILESTONE_VERSION = (
    'shasta', 'SM05',
    'lassen', 'SM05',
    'whitney', 'SM05',
    'openesb', 'SM05',
);

#this is the milestone date this release is based on.
#i.e., for open-esb, it is the trunk milestone date we are releasing.  RT 2/20/05
%MILESTONE_DATE = (
    'shasta', '01/04/2006',
    'lassen', '08/17/2005',
    'whitney', '08/17/2005',
    'openesb', '01/19/2007',
);

versionproperties($prod);

sub versionproperties
{
    local ($product) = @_;
    my ($errs) = 0;
    my ($tmp) = "";
    my ($minor) = "";
    my ($major) = "";

    if (!defined($product)) {
        print STDERR "Usage:  versionproperties(product)\n";
        return 0;
    }

    $release=$ENV{'RELEASE_BUILD'};

    if ( $release) {
        if ( $release == 1 )
        {
            $tmp2 = `cat $ENV{KITROOT}/seebeyond/alaska/latest/version.txt`; $tmp2 =~ s/:/\\:/g;
            if ( $tmp2 ) {
                printf "ALASKA_RELEASE_NAME=%s\n", $tmp2;
            } else {
                printf "ALASKA_RELEASE_NAME=%s\n", "UNDEFINED";
            } 
        } else {
            printf "ALASKA_RELEASE_NAME=%s\n", "UNDEFINED";
        }
    } else {
        printf "ALASKA_RELEASE_NAME=%s\n", "UNDEFINED";
    }

    if (defined($FULL_PRODUCT_NAME{$product})) {
        $tmp = $FULL_PRODUCT_NAME{$product}; $tmp =~ s/:/\\:/g;
        printf "FULL_PRODUCT_NAME=%s\n", $tmp;
    } else {
        printf STDERR "ERROR:  FULL_PRODUCT_NAME for '%s' is undefined\n", $product;
        printf "FULL_PRODUCT_NAME=%s\n", "NULL";
        $errs++;
    }

    if (defined($SHORT_PRODUCT_NAME{$product})) {
        $tmp = $SHORT_PRODUCT_NAME{$product}; $tmp =~ s/:/\\:/g;
        printf"SHORT_PRODUCT_NAME=%s\n", $tmp;
    } else {
        printf STDERR "ERROR:  SHORT_PRODUCT_NAME for '%s' is undefined\n", $product;
        printf "SHORT_PRODUCT_NAME=%s\n", "NULL";
        $errs++;
    }

    if (defined($MAJOR_VERSION{$product})) {
        $tmp = $MAJOR_VERSION{$product}; $tmp =~ s/:/\\:/g;
        $major = $tmp; 
        printf"MAJOR_VERSION=%s\n", $tmp;
    } else {
        printf STDERR "ERROR:  MAJOR_VERSION for '%s' is undefined\n", $product;
        printf "MAJOR_VERSION=%s\n", "NULL";
        $errs++;
    }

    if (defined($MINOR_VERSION{$product})) {
        $tmp = $MINOR_VERSION{$product}; $tmp =~ s/:/\\:/g;
        $minor = $tmp;
        printf"MINOR_VERSION=%s\n", $tmp;
    } else {
        printf STDERR "ERROR:  MINOR_VERSION for '%s' is undefined\n", $product;
        printf "MINOR_VERSION=%s\n", "NULL";
        $errs++;
    }

    $FULL_VERSION_UL = "$major.$minor";
    $FULL_VERSION_UL =~ s/\./_/g;
    printf "FULL_VERSION_UL=%s\n", $FULL_VERSION_UL;
    $FULL_VERSION = "$major.$minor";
    printf"FULL_VERSION=%s\n", $FULL_VERSION;

    if (defined($MILESTONE_VERSION{$product})) {
        $tmp = $MILESTONE_VERSION{$product}; $tmp =~ s/:/\\:/g;
        printf"MILESTONE_VERSION=%s\n", $tmp;
    } else {
        printf STDERR "ERROR:  MILESTONE_VERSION for '%s' is undefined\n", $product;
        printf "MILESTONE_VERSION=%s\n", "NULL";
        $errs++;
    }

    if (defined($MILESTONE_DATE{$product})) {
        $tmp = $MILESTONE_DATE{$product}; $tmp =~ s/:/\\:/g;
        printf"MILESTONE_DATE=%s\n", $tmp;
    } else {
        printf STDERR "ERROR:  MILESTONE_DATE for '%s' is undefined\n", $product;
        printf "MILESTONE_DATE=%s\n", "NULL";
        $errs++;
    }

    return ($errs == 0);  #true if no errors
}

