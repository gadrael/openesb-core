#!/bin/sh
###################################### Instructions to build Open ESB   ###################################################
# Modify the path according to Your environment ##################
# Run dos2unix ./setup_cyg.sh to convert file to unix format  and run following command ##################
# Run ' source ./setup_cyg.sh ' from cygwin on open-esb root directory in windows. ######
# Run smvn clean , to clean up and Run smvn to build entire open-esb.
#
############################################################################
# Set SRCROOT to the path which contains the code that you checked out in step b above 
export SRCROOT="C:/Source/open-esb"
# Set AS8BASE to the path which contains your installation of Sun's glassfish App Server
export AS8BASE="C:/CAPS/JavaCAPS62/appserver"
# Set CVSROOT to your java.net CVSROOT , set your user name
export CVSROOT=":pserver:${USERNAME}@cvsserver.stc.com:/cvs/sierra"
# Set JAVA_HOME to the path which contains your installation of Java SDK 1.5.0
export JAVA_HOME=C:/Java/jdk1.5.0_13
# Set PERL5_HOME to the path which contains your installation of Perl 5. You can download strawberry perl if you don't have any perl tool
export PERL5_HOME="C:/strawberry/perl"
# Set M2_HOME to the path which contains your maven installation
export M2_HOME="C:/apache-maven-2.0.9"
# If you already have ANT version 1.6.2 or greater you can comment out this setting of ANT_HOME
#export ANT_HOME="C:/apache-ant-1.6.0"
export ANT_HOME="${AS8BASE}/lib/ant"
# Local Maven Repository Path
export LOCALREPO="${SRCROOT}/m2/repository"
export TOOLROOT="${SRCROOT}/tools"
export PATH="${M2_HOME}/bin:${ANT_HOME}/bin:${PATH}:${SRCROOT}/tools/bin/cmn:./tools/bin/cmn"
source ${TOOLROOT}/boot/buildenv.sh
export MAKEDRV_MAKE_COMMAND="gmake"
alias smvn="${M2_HOME}/bin/mvn -Dmaven.repo.local='${LOCALREPO}' -Dmaven.test.skip=true"
