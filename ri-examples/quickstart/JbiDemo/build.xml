<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)build.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->

<project name="JbiDemo" default="default" basedir="." xmlns:jbi="http://www.sun.com/ns/jbi/jbi-ant/1" >

    <target name="default" depends="clean, build, install-demo-components, deploy-demo, run-test" description="Build, Deploy and Test the JbiDemo."/>
    
    <target name="-init-project" >
        <echo level="debug" message="loading build.properties" />
        <property file="build.properties"/>
        <property file="jbi-demo-private.properties"/>
        <!-- default values for the properties -->
        <property name="as.home" location="../.." />
        <property name="as.domain" value="domain1" />
        <property name="as.host" value="localhost" />
        <property name="as.admin.port" value="4848" />
        <property name="as.admin.username" value="admin" />
        <property name="as.admin.password" value="adminadmin" />
        <property name="as.http.port" value="8080" />
        <property name="jbi.home" location="${as.home}/jbi" />
        
        <property name="as.password.file" location="${jbi.demo.test.dir}/.as-password-file" />     
           
        <condition property="ant.executable" value="${as.home}/bin/asant.bat">
            <os family="windows"/>
        </condition>
        
        <condition property="ant.executable" value="${as.home}/bin/asant">
            <os family="unix"/>
        </condition>
        <echo message="asant executable ${ant.executable}"/>
        
        <condition property="as.admin.executable" value="${as.home}/bin/asadmin.bat">
            <os family="windows"/>
        </condition>
        
        <condition property="as.admin.executable" value="${as.home}/bin/asadmin">
            <os family="unix"/>
        </condition>
        
        <echo message="as admin executable ${as.admin.executable}"/>
                
        <!-- jbi admin ant script settings -->
        <property name="jbi.admin.xml" location="${jbi.home}/bin/jbi_admin.xml" />
                
    </target>
    
    <target name="-do-init" >
    </target>
        
    <target name="-init-check" depends="-init-project,-do-init">
    </target>
    
    <target name="-init-macrodef-jbi-admin">        
        <macrodef name="jbi-admin" uri="http://www.sun.com/ns/jbi/jbi-ant/1">
            <attribute name="target"/>
            <attribute name="host" default="${jbi.host}"/>
            <attribute name="port" default="${jbi.port}"/>
            <attribute name="username" default="${jbi.username}"/>
            <attribute name="password" default="${jbi.password}"/>            
            <attribute name="failOnError" default="true"/>
            <element name="target-options" optional="true"/>
            <sequential>
                <exec failonerror="false" executable="${ant.executable}">
                    <arg value="-emacs"/>
                    <arg value="-q"/>
                    <arg value="-f"/>
                    <arg value="${jbi.admin.xml}"/>
                    <arg value="-Djbi.host=@{host}"/>
                    <arg value="-Djbi.port=@{port}"/>
                    <arg value="-Djbi.username=@{username}"/>
                    <arg value="-Djbi.password=@{password}"/>
                    <arg value="-Djbi.task.fail.on.error=@{failOnError}"/>
                    <target-options/>
                    <arg value="@{target}"/>
                </exec>
            </sequential>
        </macrodef>        
        
    </target>
    
    <!-- Init project -->
    <target name="init" 
        depends="-init-project,-do-init,-init-check, -init-macrodef-jbi-admin"
        description="initilizes the build , dist dirs and other settings">
    </target>

    <target name="-do-runtime-check" depends="init" >
        <!-- -->
        <echo message="[Admin port] Checking if App Server is listening on ${as.host}:${as.admin.port} ..."/>
        <condition property="as.admin.alive">
            <socket server="${as.host}" port="${as.admin.port}" />
        </condition>

        <echo message="[HTTP port] Checking if App Server is listening on ${as.host}:${as.http.port} ..."/>
        <condition property="as.http.alive">
            <socket server="${as.host}" port="${as.http.port}" />
        </condition>
        <!-- -->
        <!-- -->
        <fail message="[Admin port] Failed to detect App Server Admin running on host ${as.host}:${as.admin.port}" unless="as.admin.alive"/>
        <echo message="[Admin port] App Server Admin is running on host ${as.host}:${as.admin.port}"/>

        <fail message="[HTTP port] Failed to detect App Server WebContainer running on host ${as.host}:${as.http.port}" unless="as.http.alive"/>
        <echo message="[HTTP port] App Server's Web Container is running on host ${as.host}:${as.http.port}"/>
        <!-- -->       
    </target>
    
    <target name="-show-demo-settings" >
    <echo/>
    <echo message="------------------------- Jbi Demo Settings-------------------------------------------"/>        
    <echo>
    as.home              Application Server installation directory  : ${as.home}
    as.domain            Application Server domain name used by JBI : ${as.domain}
    as.admin.port        Application Server domain admin port       : ${as.admin.port}
    as.admin.username    Application Server domain admin username   : ${as.admin.username}
    as.admin.password    Application Server domain admin password   : ${as.admin.password}
    as.http.port         Application Server domain HTTP port        : ${as.http.port}            
    jbi.port             JBI Admin JMX server port                  : ${jbi.port}
    jbi.home             JBI installation directory                 : ${jbi.home}
    </echo>
    <echo message="----------------------------------------------------------------------------------"/>
    <echo/>                
    </target>
    
    <target name="deps-build" depends="init" unless="no.deps">
        <ant target="build" inheritall="false" antfile="${project.LAFPayrollWebService}/build.xml"/>
        <ant target="build" inheritall="false" antfile="${project.DemoServiceAssembly}/build.xml"/>
        <ant target="dist" inheritall="false" antfile="${project.TestClient}/build.xml"/>
    </target>
    
    <target name="deps-clean" depends="init" unless="no.deps">
        <ant target="clean" inheritall="false" antfile="${project.LAFPayrollWebService}/build.xml"/>
        <ant target="clean" inheritall="false" antfile="${project.DemoServiceAssembly}/build.xml"/>
        <ant target="clean" inheritall="false" antfile="${project.TestClient}/build.xml"/>
    </target>
    
    <target name="build" depends="init, deps-build">
    </target>    

    <target name="clean" depends="init, deps-clean">
    </target>    
        
    <target name="run-test" depends="init, -show-demo-settings, -do-runtime-check, -do-run-ftp-test, -do-run-soap-test, show-test-output">                
    </target>
        
    <target name="show-test-output" depends="init,-do-runtime-check, -show-nb-browser, -show-cli-browser"  >        
    </target>
    
    <target name="-show-nb-browser" depends="init,-do-runtime-check" if="netbeans.user" >
        <nbbrowse url="${jbi.demo.webapp.out.dir.url}"/>
    </target>
    
    <target name="-show-cli-browser" depends="init,-do-runtime-check" unless="netbeans.user" >
        <ant target="launch-browser" inheritall="false" antfile="${project.TestClient}/build.xml" />
    </target>
    
    <target name="run-ftp-test" depends="init, -show-demo-settings, -do-runtime-check, -do-run-ftp-test, show-test-output">
    </target>
    
    <target name="-do-run-ftp-test" depends="init, -show-demo-settings, -do-runtime-check">
        
        <delete dir="${jbi.demo.fbc.timecard.ftp.dir}" quiet="true" />
        <mkdir dir="${jbi.demo.fbc.test.dir}" />
        <mkdir dir="${jbi.demo.fbc.timecard.ftp.dir}" />
        <mkdir dir="${jbi.demo.fbc.timecard.out.dir}" />
        <mkdir dir="${jbi.demo.fbc.timecard.processed.dir}" />       
        
        <echo>Copying file ${jbi.demo.ftp.input.file} to ftp location ${jbi.demo.fbc.timecard.ftp.dir} ...</echo> 
        <copy file="${jbi.demo.ftp.input.file}" todir="${jbi.demo.fbc.timecard.ftp.dir}"/>

        <echo>Waiting for input file to be processed ...</echo>
        <sleep seconds="5"/>

        <echo/>
        <echo>Output file should be in this directory at url: ${jbi.demo.webapp.out.dir.url}</echo>
        <echo/>
        
    </target>

    <target name="run-soap-test" depends="init, -show-demo-settings, -do-runtime-check, -do-run-soap-test, show-test-output">        
    </target>
    
    <target name="-do-run-soap-test" depends="init, -show-demo-settings, -do-runtime-check">
        <ant target="run-test" inheritall="false" antfile="${project.TestClient}/build.xml"/>
    </target>

    <!-- workaround for invoking the asant tasks from inside netbeans with right set of as libraries 
    ###  do not call this task directly in the netbeans -->
    <target name="deploy-webapp-sun-as" depends="init" if="sun.app.server" >
        <property name="LAFPayrollWebService.war" location="${reference.LAFPayrollWebService.war}" />
        <echo message = "Deploying ${LAFPayrollWebService.war}" />
        <echo file="${as.password.file}" append="false">AS_ADMIN_password=${as.admin.password}</echo>
        <sun-appserv-deploy
        file="${LAFPayrollWebService.war}"
        user="${as.admin.username}"
        passwordfile="${as.password.file}"
        host="${as.admin.host}"
        port="${as.admin.port}"
        asinstalldir="${as.home}" /> 
        <delete file="${as.password.file}" quiet="true" />
    </target>
    
    <target name="-deploy-webapp-sun-as" depends="init, -do-runtime-check, deps-build" if="sun.app.server" >        
        <exec failonerror="false" executable="${ant.executable}" dir="${jbi.demo.home}" >
            <arg value="deploy-webapp-sun-as"/>
        </exec>
    </target>

    <target name="-deploy-webapp-jboss-as" depends="init, -do-runtime-check, deps-build" if="jboss.app.server" >
        <fail message="JBOSS Appserver is not yet supported by this demo" />
    </target>
    <!-- workaround for invoking the asant tasks from inside netbeans with right set of as libraries 
    ###  do not call this task directly in the netbeans -->
    <target name="undeploy-webapp-sun-as" depends="init" if="sun.app.server" >
        <echo message = "Undeploying ${LAFPayrollWebService.webmodule.name}" />
        <echo file="${as.password.file}" append="false">AS_ADMIN_password=${as.admin.password}</echo>
        <sun-appserv-undeploy
        name="${LAFPayrollWebService.webmodule.name}"
        user="${as.admin.username}"
        passwordfile="${as.password.file}"
        host="${as.admin.host}"
        port="${as.admin.port}"
        asinstalldir="${as.home}" />            
        <delete file="${as.password.file}" quiet="true" />
    </target>
    
    <target name="-undeploy-webapp-sun-as" depends="init, -do-runtime-check" if="sun.app.server" >
        <exec failonerror="false" executable="${ant.executable}" dir="${jbi.demo.home}" >
            <arg value="undeploy-webapp-sun-as"/>
        </exec>
    </target>

    <target name="-undeploy-webapp-jboss-as" depends="init, -do-runtime-check" if="jboss.app.server" >
        <fail message="JBOSS Appserver is not yet supported by this demo" />
    </target>
    
    <target name="deploy-webapp" depends="init, -do-runtime-check, deps-build, -deploy-webapp-sun-as, -deploy-webapp-jboss-as">
    </target>
    
    <target name="undeploy-webapp" depends="init, -do-runtime-check, -undeploy-webapp-sun-as, -undeploy-webapp-jboss-as">
    </target>
    
    <target name="deploy-sa" depends="init, -do-runtime-check, deps-build, -do-deploy-sa, start-sa, show-sa">        
    </target>
    
    <target name="undeploy-sa" depends="init, -do-runtime-check, stop-sa, shutdown-sa, -do-undeploy-sa ">               
    </target>
    
    <target name="deploy-demo" depends="init, -do-runtime-check, deps-build, deploy-webapp, deploy-sa"></target>
    <target name="undeploy-demo" depends="init, -do-runtime-check, undeploy-webapp, undeploy-sa"></target>
        
    <target name="-do-deploy-sa" depends="init, -do-runtime-check, deps-build">
        <property name="DemoServiceAssembly.zip" location="${reference.DemoServiceAssembly.zip}" />      
        <jbi:jbi-admin target="deploy-service-assembly">
            <target-options>
                <arg value="-Djbi.deploy.file=${DemoServiceAssembly.zip}" />
            </target-options>
        </jbi:jbi-admin>
    </target>
    
    <target name="-do-undeploy-sa" depends="init, -do-runtime-check">
        <jbi:jbi-admin target="undeploy-service-assembly">
            <target-options>
                <arg value="-Djbi.service.assembly.name=${DemoServiceAssembly.name}" />
            </target-options>
        </jbi:jbi-admin>        
    </target>
    
    <target name="show-sa" depends="init, -do-runtime-check" >
        <jbi:jbi-admin target="list-service-assemblies" failOnError="false" >
            <target-options>
                <arg value="-Djbi.service.assembly.name=${DemoServiceAssembly.name}" />
            </target-options>
        </jbi:jbi-admin>
    </target>
    
    <target name="start-sa" depends="init, -do-runtime-check" >
        <jbi:jbi-admin target="start-service-assembly" >
            <target-options>
                <arg value="-Djbi.service.assembly.name=${DemoServiceAssembly.name}" />
            </target-options>
        </jbi:jbi-admin>
    </target>

    <target name="stop-sa" depends="init, -do-runtime-check" >
        <jbi:jbi-admin target="stop-service-assembly" >
            <target-options>
                <arg value="-Djbi.service.assembly.name=${DemoServiceAssembly.name}" />
            </target-options>
        </jbi:jbi-admin>
    </target>

    <target name="shutdown-sa" depends="init, -do-runtime-check" >
        <jbi:jbi-admin target="shut-down-service-assembly" >
            <target-options>
                <arg value="-Djbi.service.assembly.name=${DemoServiceAssembly.name}" />
            </target-options>
        </jbi:jbi-admin>
    </target>
    
    <target name="start-demo-components" depends="init, -do-runtime-check" >
        <jbi:jbi-admin target="start-component" failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunSequencingEngine.name}" />
            </target-options>
        </jbi:jbi-admin>
        <jbi:jbi-admin target="start-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunTransformationEngine.name}" />
            </target-options>
        </jbi:jbi-admin>
        <jbi:jbi-admin target="start-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunFileBinding.name}" />
            </target-options>
        </jbi:jbi-admin>        
        <jbi:jbi-admin target="start-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunSOAPBinding.name}" />
            </target-options>
        </jbi:jbi-admin>        
    </target>
    
    <target name="list-engines" depends="init, -do-runtime-check">
        <jbi:jbi-admin target="list-service-engines"/>
    </target>

    <target name="list-bindings" depends="init, -do-runtime-check">
        <jbi:jbi-admin target="list-binding-components"/>
    </target>

    <target name="list-slibs" depends="init, -do-runtime-check">
        <jbi:jbi-admin target="list-shared-libraries"/>
    </target>
    
    <target name="list-sa" depends="init, -do-runtime-check">
        <jbi:jbi-admin target="list-service-assemblies"/>
    </target>    
    
    <target name="list-all" depends="init, -do-runtime-check">
        <jbi:jbi-admin target="list-shared-libraries"/>
        <jbi:jbi-admin target="list-service-engines"/>
        <jbi:jbi-admin target="list-binding-components"/>
        <jbi:jbi-admin target="list-service-assemblies"/>
    </target>    
            
    <!-- help targets -->

    <target name="help" description="Description of targets for running the demo">
        <echo>
            Example :
            asant clean build deploy-demo run-test
        </echo>
    </target>
    
    <target name="install-demo-components" depends="init, -do-runtime-check, -do-install-demo-components, start-demo-components" >
    </target>
    
    <target name="stop-and-shutdown-demo-components" depends="init, -do-runtime-check, stop-demo-components, shutdown-demo-components" >
    </target>
    
    <target name="-do-install-demo-components" depends="init, -do-runtime-check" >
        <echo message="Trying to install required demo components" />
        <echo message="### You can safely ignore the install component failures if the component is already installed ###" />
        <jbi:jbi-admin target="install-shared-library" failOnError="false" >
            <target-options>
                <arg value="-Djbi.install.file=${SunWSDLSharedLibrary.zip}" />
            </target-options>
        </jbi:jbi-admin>
        
        <jbi:jbi-admin target="install-component" failOnError="false" >
            <target-options>
                <arg value="-Djbi.install.file=${SunSequencingEngine.zip}" />
            </target-options>
        </jbi:jbi-admin>
        
        <jbi:jbi-admin target="install-component" failOnError="false" >
            <target-options>
                <arg value="-Djbi.install.file=${SunTransformationEngine.zip}" />
            </target-options>
        </jbi:jbi-admin>
        
        <jbi:jbi-admin target="install-component" failOnError="false" >
            <target-options>
                <arg value="-Djbi.install.file=${SunFileBinding.zip}" />
            </target-options>
        </jbi:jbi-admin>
        
        <jbi:jbi-admin target="install-component" failOnError="false" >
            <target-options>
                <arg value="-Djbi.install.file=${SunSOAPBinding.zip}" />
            </target-options>
        </jbi:jbi-admin>
        
    </target>
        
    <target name="stop-demo-components" depends="init, -do-runtime-check" >        
        <jbi:jbi-admin target="stop-component" failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunSequencingEngine.name}" />
            </target-options>
        </jbi:jbi-admin>
        <jbi:jbi-admin target="stop-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunTransformationEngine.name}" />
            </target-options>
        </jbi:jbi-admin>
        <jbi:jbi-admin target="stop-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunFileBinding.name}" />
            </target-options>
        </jbi:jbi-admin>        
        <jbi:jbi-admin target="stop-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunSOAPBinding.name}" />
            </target-options>
        </jbi:jbi-admin>   
    </target>
    
    <target name="shutdown-demo-components" depends="init, -do-runtime-check" >
        <!-- shutdown components -->
        <jbi:jbi-admin target="shut-down-component" failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunSequencingEngine.name}" />
            </target-options>
        </jbi:jbi-admin>
        <jbi:jbi-admin target="shut-down-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunTransformationEngine.name}" />
            </target-options>
        </jbi:jbi-admin>
        <jbi:jbi-admin target="shut-down-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunFileBinding.name}" />
            </target-options>
        </jbi:jbi-admin>        
        <jbi:jbi-admin target="shut-down-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunSOAPBinding.name}" />
            </target-options>
        </jbi:jbi-admin>        
        
    </target>
        
    <target name="uninstall-demo-components" depends="init, -do-runtime-check" >
        
        <jbi:jbi-admin target="uninstall-component" failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunSequencingEngine.name}" />
            </target-options>
        </jbi:jbi-admin>
        <jbi:jbi-admin target="uninstall-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunTransformationEngine.name}" />
            </target-options>
        </jbi:jbi-admin>
        <jbi:jbi-admin target="uninstall-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunFileBinding.name}" />
            </target-options>
        </jbi:jbi-admin>
        
        <jbi:jbi-admin target="uninstall-component"  failOnError="false" >
            <target-options>
                <arg value="-Djbi.component.name=${SunFileBinding.name}" />
            </target-options>
        </jbi:jbi-admin>                
        
    </target>
    
    
</project>
