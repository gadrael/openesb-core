#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00010.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

# regress setup
. ./regress_defs.ksh

#
# test runtime and components
#

test_configuration_tasks()
{


$JBI_ANT list-runtime-configuration
$JBI_ANT -Djbi.component.name="sun-http-binding" list-component-configuration

### set and list configuration for runtime. ###############################
$JBI_ANT -Djbi.config.params.file=$JV_SVC_REGRESS/jbi-config-params.properties set-runtime-configuration 
$JBI_ANT list-runtime-configuration

### set and list configuration for component. ###############################
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.config.params.file=$JV_SVC_REGRESS/httpbc-config-params.properties set-component-configuration 
$JBI_ANT -Djbi.component.name="sun-http-binding" list-component-configuration

### set and list configuration for component for target ant_cluster. ########
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.config.params.file=$JV_SVC_REGRESS/httpbc-config-params.properties -Djbi.target=ant_cluster set-component-configuration 
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.target=ant_cluster list-component-configuration

### set and list configuration for component for target instance13. ########
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.config.params.file=$JV_SVC_REGRESS/httpbc-config-params.properties -Djbi.target=instance13 set-component-configuration 
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.target=instance13 list-component-configuration
}

reset_runtime_configuration()
{
### Reset the configuration back to the default values. ###
$JBI_ANT -Djbi.config.params.file=$JV_SVC_REGRESS/jbi-default-config-params.properties set-runtime-configuration 
$JBI_ANT list-runtime-configuration
}

reset_component_configuration()
{
### Reset the configuration back to the default values. ###
$JBI_ANT -Djbi.config.params.file=$JV_SVC_REGRESS/jbi-default-httpbc-config-params.properties -Djbi.component.name=sun-http-binding set-component-configuration 
$JBI_ANT -Djbi.component.name=sun-http-binding list-component-configuration

$JBI_ANT -Djbi.config.params.file=$JV_SVC_REGRESS/jbi-default-httpbc-config-params.properties -Djbi.component.name=sun-http-binding -Djbi.target=ant_cluster set-component-configuration 
    $JBI_ANT -Djbi.component.name=sun-http-binding -Djbi.target=ant_cluster list-component-configuration

$JBI_ANT -Djbi.config.params.file=$JV_SVC_REGRESS/jbi-default-httpbc-config-params.properties -Djbi.component.name=sun-http-binding -Djbi.target=instance13 set-component-configuration 
$JBI_ANT -Djbi.component.name=sun-http-binding -Djbi.target=instance13 list-component-configuration
}

run_test()
{
build_test_artifacts
$JBI_ANT -Djbi.component.name="sun-http-binding" start-component
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.target=ant_cluster start-component
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.target=instance13 start-component

sleep 10

test_configuration_tasks

# always reset the runtime configuration (sp the timeouts since these affect other tests )
reset_runtime_configuration
reset_component_configuration

$JBI_ANT -Djbi.component.name="sun-http-binding" stop-component
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.target=ant_cluster stop-component
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.target=instance13 stop-component

}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test

exit 0
