/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJbiListStatisticsTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import junit.framework.TestCase;
import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.ArrayType;
import java.lang.Long;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJbiListStatisticsTask extends TestCase
{
    
    /**
     * Creates a new instance of TestJbiListStatisticsTask
     * @param aTestName name
     */
    public TestJbiListStatisticsTask(String aTestName)
    {
        super(aTestName);
    }
    
    /**
     * Test the code for framework
     */
    public void testJbiListStatsForFramework()
       throws Exception
    {
        JbiListStatisticsTask theTask = new JbiListStatisticsTask();
        Class clazz = theTask.getClass();

        java.lang.reflect.Method
               mtd = clazz.getDeclaredMethod("printStatsForFramework",
                         new Class[]{ javax.management.openmbean.TabularData.class,
	                                                    java.lang.String.class,
                                                         java.io.PrintWriter.class});
        mtd.setAccessible(true);

        String [] sItemNames = new String[] {"InstanceName", "UpTime (ms)", "StartupTime (ms)" };
     

        String[] itemDesc = { "Instance Name", "Up Time", "Startup Time" };

        OpenType[] rowAttrTypes = {SimpleType.STRING, SimpleType.LONG, SimpleType.LONG};

        String[] rowIndex = new String[]{ "InstanceName" };


        CompositeType fwkStatsCompositeType = new CompositeType("Framework Stats Compiste Data",
                                                  "Name and value pair",
                                                   sItemNames,
                                                   itemDesc,
                                                   rowAttrTypes);

        TabularType fwkTabularTable = new TabularType("FrameworkStatsType",
                                               "List of framework stats",
                                                    fwkStatsCompositeType,
                                                    rowIndex);


        TabularDataSupport tData = new TabularDataSupport(fwkTabularTable);
        // Add one entry
        CompositeDataSupport cd = new CompositeDataSupport(fwkStatsCompositeType,
                                         sItemNames, new Object[] {"instance1", new Long(100000), new Long(100)});
        CompositeDataSupport cd1 = new CompositeDataSupport(fwkStatsCompositeType,
                                         sItemNames, new Object[] {"instance2", new Long(200000), new Long(200)});
        tData.put(cd);
        tData.put(cd1);

        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter msgWriter = new java.io.PrintWriter(stringWriter);

        mtd.invoke(theTask, new Object[]{tData, "target1", msgWriter});

        msgWriter.close();
        java.lang.System.out.println(stringWriter.getBuffer().toString());
    }

    /**
     * Test the code for MNR
     */
    public void testJbiListStatsForNMR()
       throws Exception
    {
        JbiListStatisticsTask theTask = new JbiListStatisticsTask();
        Class clazz = theTask.getClass();

        java.lang.reflect.Method
               mtd = clazz.getDeclaredMethod("printStatsForNMR",
                         new Class[]{ javax.management.openmbean.TabularData.class,
                                                            java.lang.String.class,
                                                         java.io.PrintWriter.class});
        mtd.setAccessible(true);

        String [] sItemNames = new String[] {"InstanceName", "ListActiveChannels", "ListActiveEndpoints" };

        String[] itemDesc = { "Instance Name", "List Active Channels", "List Active Endpoints" };

        ArrayType stringArrayType = new ArrayType(1, SimpleType.STRING);
        OpenType[] rowAttrTypes = {SimpleType.STRING, stringArrayType, stringArrayType};

        String[] rowIndex = new String[]{ "InstanceName" };


        CompositeType nmrStatsCompositeType = new CompositeType("NMR Stats Compiste Data",
                                                  "Name and value pair",
                                                   sItemNames,
                                                   itemDesc,
                                                   rowAttrTypes);

        TabularType nmrTabularTable = new TabularType("FrameworkStatsType",
                                               "List of framework stats",
                                                    nmrStatsCompositeType,
                                                    rowIndex);


        TabularDataSupport tData = new TabularDataSupport(nmrTabularTable);
        // Add one entry
        CompositeDataSupport cd = new CompositeDataSupport(nmrStatsCompositeType,
                                        sItemNames, new Object[] {"instance1",
					new String[] {"sun-http-binding", "sun-http-binding", "sun-javaee-engine"},
                                        new String[] {"http://localhost/SynchronousSample,service1,endpoint1",
                                                      "http://localhost/New,service1,endpoint2",
                                                      "http://localhost/New,service1,endpoint2"} });

        CompositeDataSupport cd1 = new CompositeDataSupport(nmrStatsCompositeType,
                                        sItemNames, new Object[] {"instance2",
					new String[] {"sun-http-binding", "sun-http-binding", "sun-javaee-engine"},
                                        new String[] {"http://localhost/SynchronousSample,service1,endpoint1",
                                                      "http://localhost/New,service1,endpoint2",
                                                      "http://localhost/New,service1,endpoint2"} });
        tData.put(cd);
        tData.put(cd1);

        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter msgWriter = new java.io.PrintWriter(stringWriter);

        mtd.invoke(theTask, new Object[]{tData, "target1", msgWriter});

        msgWriter.close();
        java.lang.System.out.println(stringWriter.getBuffer().toString());
 
    }

    /**
     * Test the code for component
     */
    public void testJbiListStatsForComponents()
       throws Exception
    {
        JbiListStatisticsTask theTask = new JbiListStatisticsTask();
        Class clazz = theTask.getClass();

        java.lang.reflect.Method
               mtd = clazz.getDeclaredMethod("printStatsForComponent",
                         new Class[]{ java.lang.String.class,
                                      javax.management.openmbean.TabularData.class,
                                      java.lang.String.class,
                                      java.io.PrintWriter.class,
                                      java.lang.String.class,
                                      java.lang.Boolean.class});
        mtd.setAccessible(true);

        String [] sItemNames = new String[] {"InstanceName",
                                             "ComponentUpTime (ms)",
                                             "NumActivatedEndpoints",
                                             "NumReceivedRequests",
                                             "NumSentRequests",
                                             "NumReceivedReplies",
                                             "NumSentReplies",
                                             "NumReceivedDONEs",
                                             "NumSentDONEs",
                                             "NumReceivedFaults",
                                             "NumSentFaults",
                                             "NumReceivedErrors",
                                             "NumSentErrors",
                                             "NumCompletedExchanges",
                                             "NumActiveExchanges",
                                             "NumErrorExchanges",
                                             "MessageExchangeResponseTime Avg (ns)",
                                             "MessageExchangeComponentTime Avg (ns)",
                                             "MessageExchangeDeliveryTime Avg (ns)",
                                             "MessageExchangeNMRTime Avg (ns)",                                           
                                             "ComponentExtensionStats"};

        String[] itemDesc = new String[] {"ComponentName",
                                             "ComponentUpTime",
                                             "NumActivatedEndpoints",
                                             "NumReceivedRequests",
                                             "NumSentRequests",
                                             "NumReceivedReplies",
                                             "NumSentReplies",
                                             "NumReceivedDONEs",
                                             "NumSentDONEs",
                                             "NumReceivedFaults",
                                             "NumSentFaults",
                                             "NumReceivedErrors",
                                             "NumSentErrors",
                                             "NumCompletedExchanges",
                                             "NumActiveExchanges",
                                             "NumErrorExchanges",
                                             "MEResponseTime",
                                             "MEComponentTime",
                                             "MEDeliveryChannelTime",
                                             "MEMessageServiceTime",
                                             "ComponentExtensionStats"};

        ArrayType stringArrayType = new ArrayType(1, SimpleType.STRING);

        String [] cpExtItemNames = new String[] {"ReceivedRequest",
                                             "ListOfConsumingEndpoints",
                                             "ListOfProvisioningEnpoints"};

        String [] cpExtItemDesc = new String[] {"ReceivedRequest",
                                             "ListOfConsumingEndpoints",
                                             "ListOfProvisioningEnpoints"};

        OpenType[] cpExtRowAttrTypes = {SimpleType.LONG,
                                              stringArrayType,
                                              stringArrayType};


        CompositeType cpExtStatsCompositeType = new CompositeType("Component Ext. Stats Compiste Data",
                                                  "Name and value pair",
                                                   cpExtItemNames,
                                                   cpExtItemDesc,
                                                   cpExtRowAttrTypes);

        OpenType[] rowAttrTypes = {SimpleType.STRING,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   cpExtStatsCompositeType};

        String[] rowIndex = new String[]{ "InstanceName" };


        CompositeType cpStatsCompositeType = new CompositeType("Component Stats Compiste Data",
                                                  "Name and value pair",
                                                   sItemNames,
                                                   itemDesc,
                                                   rowAttrTypes);

        TabularType cpTabularTable = new TabularType("ComponentStatsType",
                                               "List of component stats",
                                                    cpStatsCompositeType,
                                                    rowIndex);


        TabularDataSupport tData = new TabularDataSupport(cpTabularTable);

        CompositeDataSupport extCd = new CompositeDataSupport(cpExtStatsCompositeType,
                                              cpExtItemNames, new Object[] { new Long(10),
                                              new String[] {"http://localhost/SynchronousSample,service1,endpoint1",
                                                            "http://localhost/SynchronousSample,service1,endpoint2"},
                                              new String[] {"http://localhost/SynchronousSample,service1,endpoint1"} });
        // Add one entry
        CompositeDataSupport cd = new CompositeDataSupport(cpStatsCompositeType,
                                        sItemNames, new Object[] {"component1",
                                                                  new Long(10),
                                                                  new Long(5),
                                                                  new Long(5),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  new Long(10),
                                                                  extCd } );

        tData.put(cd);

        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter msgWriter = new java.io.PrintWriter(stringWriter);

        mtd.invoke(theTask, new Object[] {"Component1", tData, "target1", msgWriter,
                        "----------------------------------", new Boolean(true)});

        msgWriter.close();
        java.lang.System.out.println(stringWriter.getBuffer().toString());
    }

    /**
     * Test the code for service assembly 
     */
    public void testJbiListStatsForServiceAssemblies()
       throws Exception
    {
        JbiListStatisticsTask theTask = new JbiListStatisticsTask();
        Class clazz = theTask.getClass();

        java.lang.reflect.Method
               mtd = clazz.getDeclaredMethod("printStatsForServiceAssembly",
                         new Class[]{ javax.management.openmbean.TabularData.class,
                                      java.lang.String.class,
                                      java.io.PrintWriter.class,
                                      java.lang.String.class});
        mtd.setAccessible(true);

        String [] suItemNames = new String[] { "ServiceUnitName",
                                             "ServiceUnitShutdownTime Avg (ms)",
                                             "ServiceUnitStartupTime Avg (ms)",
                                             "ServiceUnitStopTime Avg (ms)"};

        String[] suItemDesc =  new String[] {  "ServiceUnitName",
                                             "ServiceUnitShutdownTime Avg (ms)",
                                             "ServiceUnitStartupTime Avg (ms)",
                                             "ServiceUnitStopTime Avg (ms)"};


        ArrayType stringArrayType = new ArrayType(1, SimpleType.STRING);

        OpenType[] suRowAttrTypes = {SimpleType.STRING,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG};

        String[] suRowIndex = new String[]{ "ServiceUnitName" };


        CompositeType suStatsCompositeType = new CompositeType("SU Stats Composite Data",
                                                  "Name and value pair",
                                                   suItemNames,
                                                   suItemDesc,
                                                   suRowAttrTypes);


        String [] saItemNames = new String[] { "ServiceAssemblyName",
                                             "LastStartupTime",
                                             "ShutdownTime Avg (ms)",
                                             "StartupTime Avg (ms)",
                                             "StopTime Avg (ms)",
                                             "ServiceUnitStatistics"};

        String[] saItemDesc =  new String[] { "ServiceAssemblyName",
                                             "LastStartupTime",
                                             "ShutdownTime Avg (ms)",
                                             "StartupTime Avg (ms)",
                                             "StopTime Avg (ms)",
                                             "ServiceUnitStatistics"};

        ArrayType suCompositeArrayType = new ArrayType(1, suStatsCompositeType);

        OpenType[] saRowAttrTypes = {SimpleType.STRING,
                                   SimpleType.DATE,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   SimpleType.LONG,
                                   suCompositeArrayType};

        String[] saRowIndex = new String[]{ "ServiceAssemblyName" };


        CompositeType saStatsCompositeType = new CompositeType("SA Stats Compiste Data",
                                                  "Name and value pair",
                                                   saItemNames,
                                                   saItemDesc,
                                                   saRowAttrTypes);

        TabularType saTabularTable = new TabularType("SAStatsType",
                                               "List of SA stats",
                                                    saStatsCompositeType,
                                                    saRowIndex);


        TabularDataSupport tData = new TabularDataSupport(saTabularTable);
        // Add one entry
        CompositeDataSupport su1 = new CompositeDataSupport(suStatsCompositeType,
                                        suItemNames, new Object[] {"CompositeApp1-SU1",
                                                                  new Long(8),
                                                                  new Long(7),
                                                                  new Long(6)} );

        CompositeDataSupport su2 = new CompositeDataSupport(suStatsCompositeType,
                                        suItemNames, new Object[] {"CompositeApp1-SU2",
                                                                  new Long(0),
                                                                  new Long(0),
                                                                  new Long(0)} );

        CompositeDataSupport[] sus = new CompositeDataSupport[] { su1, su2 };

        CompositeDataSupport sa = new CompositeDataSupport(saStatsCompositeType,
                                        saItemNames, new Object[] {"CompositeApp1",
                                                                  java.util.Calendar.getInstance().getTime(),
                                                                  new Long(1),
                                                                  new Long(47),
                                                                  new Long(39),
                                                                  new CompositeDataSupport[] { su1, su2 } } );

        tData.put(sa);

        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter msgWriter = new java.io.PrintWriter(stringWriter);

        mtd.invoke(theTask, new Object[]{tData, "target1", msgWriter,
                                         "---------------------------------------------------"});

        msgWriter.close();
        java.lang.System.out.println(stringWriter.getBuffer().toString());
    }

    /**
     * Test the code for endpoint
     */
    public void testJbiListStatsForProviderEndpoints()
       throws Exception
    {
        JbiListStatisticsTask theTask = new JbiListStatisticsTask();
        Class clazz = theTask.getClass();

        java.lang.reflect.Method
               mtd = clazz.getDeclaredMethod("printStatsForEndpoint",
                         new Class[]{ java.lang.String.class,
                                      javax.management.openmbean.TabularData.class,
                                      java.lang.String.class,
                                      java.io.PrintWriter.class,
                                      java.lang.String.class,
                                      java.lang.Boolean.class } );
        mtd.setAccessible(true);

        String [] providerEndpointItemNames = new String[] {"InstanceName",
                                                            "ActivationTime",
                                                            "EndpointUpTime (ms)",
                                                            "NumActiveExchanges",
                                                            "NumReceivedRequests",
                                                            "NumSentReplies",
                                                            "NumReceivedDONEs",
                                                            "NumSentDONEs",
                                                            "NumReceivedFaults",
                                                            "NumSentFaults",
                                                            "NumReceivedErrors",
                                                            "NumSentErrors",
                                                            "ComponentName",
                                                            "MessageExchangeResponseTime Avg (ns)",
                                                            "MessageExchangeComponentTime Avg (ns)",
                                                            "MessageExchangeDeliveryTime Avg (ns)",
                                                            "MessageExchangeNMRTime Avg (ns)",
                                                            "PerformanceMeasurements"};


        String [] providerEndpointItemDesc = new String[] {"InstanceName",
                                                            "ActivationTime",
                                                            "UpTime",
                                                            "NumActiveExchanges",
                                                            "NumReceivedRequests",
                                                            "NumSentReplies",
                                                            "NumReceivedDONEs",
                                                            "NumSentDONEs",
                                                            "NumReceivedFaults",
                                                            "NumSentFaults",
                                                            "NumReceivedErrors",
                                                            "NumSentErrors",
                                                            "ComponentName",
                                                            "ME-ResponseTime-Avg",
                                                            "ME-ComponentTime-Avg",
                                                            "ME-DeliveryChannelTime-Avg",
                                                            "ME-MessageServiceTime-Avg",
                                                            "PerformanceMeasurements"};

        ArrayType stringArrayType = new ArrayType(1, SimpleType.STRING);

        String [] perfItemNames = new String[] {"MedianTime",
                                               "FirstTime",
                                               "SubTopic"};

        String [] perfItemDesc = new String[] {"MedianTime",
                                               "FirstTime",
                                               "SubTopic"};

        OpenType[] perfRowAttrTypes = { SimpleType.LONG,
                                        SimpleType.LONG,
                                        SimpleType.STRING};

        CompositeType perfStatsCompositeType = new CompositeType("Performance Stats Compiste Data",
                                                  "Name and value pair",
                                                   perfItemNames,
                                                   perfItemDesc,
                                                   perfRowAttrTypes);

        String[] perfRowIndex = new String[]{ "SubTopic" };

        TabularType perfTabularTable = new TabularType("PerfStatsType",
                                               "List of SA stats",
                                                    perfStatsCompositeType,
                                                    perfRowIndex);

        OpenType[] endpointRowAttrTypes = { SimpleType.STRING,
                                      SimpleType.DATE,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.STRING,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      perfTabularTable};

        String[] endpointRowIndex = new String[]{ "InstanceName" };


        CompositeType endpointStatsCompositeType = new CompositeType("ProviderEndpointStats",
                                                  "Name and value pair",
                                                   providerEndpointItemNames,
                                                   providerEndpointItemDesc,
                                                   endpointRowAttrTypes);


        TabularDataSupport perfTbData = new TabularDataSupport(perfTabularTable);
        // Add one entry
        CompositeDataSupport perfData = new CompositeDataSupport(perfStatsCompositeType,
                                        perfItemNames, new Object[] { new Long(100),
                                                                      new Long(50),
                                                                      "Documentation"} );
        perfTbData.put(perfData);

        CompositeDataSupport endpointData = new CompositeDataSupport(endpointStatsCompositeType,
                                        providerEndpointItemNames, new Object[] {"http://localhost/SynchronousSample,service1,endpoint1 statistics",
                                      java.util.Calendar.getInstance().getTime(),
                                      new Long(10),
                                      new Long(10),
                                      new Long(10),
                                      new Long(5),
                                      new Long(10),
                                      new Long(10),
                                      new Long(5),
                                      new Long(10),
                                      new Long(10),
                                      new Long(10),
                                      "sun-bpel-engine",
                                      new Long(10),
                                      new Long(10),
                                      new Long(10),
                                      new Long(10),
                                      perfTbData} );

        TabularType endpointTabularTable = new TabularType("EndpointStatsType",
                                               "List of endpoint stats",
                                                    endpointStatsCompositeType,
                                                    endpointRowIndex);

        TabularDataSupport endpointTbData = new TabularDataSupport(endpointTabularTable);
        endpointTbData.put(endpointData);

        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter msgWriter = new java.io.PrintWriter(stringWriter);

        mtd.invoke(theTask, new Object[]{"http://localhost/SynchronousSample,service1,endpoint1 statistics",
                                         endpointTbData, "target1", msgWriter,
                                         "-------------------------------------------------",
                                         new Boolean(true) });

        msgWriter.close();
        java.lang.System.out.println(stringWriter.getBuffer().toString());
    }

    /**
     * Test the code for endpoint
     */
    public void testJbiListStatsForConsumerEndpoints()
       throws Exception
    {
        JbiListStatisticsTask theTask = new JbiListStatisticsTask();
        Class clazz = theTask.getClass();

        java.lang.reflect.Method
               mtd = clazz.getDeclaredMethod("printStatsForEndpoint",
                         new Class[]{ java.lang.String.class,
                                      javax.management.openmbean.TabularData.class,
                                      java.lang.String.class,
                                      java.io.PrintWriter.class,
                                      java.lang.String.class,
                                      java.lang.Boolean.class } );
        mtd.setAccessible(true);

        String [] consumerEndpointItemNames = new String[] {"InstanceName",
                                                            "NumSentRequests",
                                                            "NumReceivedReplies",
                                                            "NumReceivedDONEs",
                                                            "NumSentDONEs",
                                                            "NumReceivedFaults",
                                                            "NumSentFaults",
                                                            "NumReceivedErrors",
                                                            "NumSentErrors",
                                                            "ME-ResponseTime-Avg",
                                                            "ME-ComponentTime-Avg",
                                                            "ME-DeliveryChannelTime-Avg",
                                                            "ME-MessageServiceTime-Avg",
                                                            "PerformanceMeasurements"};


        String [] consumerEndpointItemDesc = new String[] {"InstanceName",
                                                            "NumSentRequests",
                                                            "NumReceivedReplies",
                                                            "NumReceivedDONEs",
                                                            "NumSentDONEs",
                                                            "NumReceivedFaults",
                                                            "NumSentFaults",
                                                            "NumReceivedErrors",
                                                            "NumSentErrors",
                                                            "ME-ResponseTime-Avg",
                                                            "ME-ComponentTime-Avg",
                                                            "ME-DeliveryChannelTime-Avg",
                                                            "ME-MessageServiceTime-Avg",
                                                            "PerformanceMeasurements"};

        ArrayType stringArrayType = new ArrayType(1, SimpleType.STRING);

        String [] perfItemNames = new String[] {"MedianTime",
                                               "FirstTime",
                                               "SubTopic"};

        String [] perfItemDesc = new String[] {"MedianTime",
                                               "FirstTime",
                                               "SubTopic"};

        OpenType[] perfRowAttrTypes = { SimpleType.LONG,
                                        SimpleType.LONG,
                                        SimpleType.STRING};

        CompositeType perfStatsCompositeType = new CompositeType("Performance Stats Compiste Data",
                                                  "Name and value pair",
                                                   perfItemNames,
                                                   perfItemDesc,
                                                   perfRowAttrTypes);

        String[] perfRowIndex = new String[]{ "SubTopic" };

        TabularType perfTabularTable = new TabularType("PerfStatsType",
                                               "List of SA stats",
                                                    perfStatsCompositeType,
                                                    perfRowIndex);

        OpenType[] endpointRowAttrTypes = { SimpleType.STRING,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      SimpleType.LONG,
                                      perfTabularTable};

        String[] endpointRowIndex = new String[]{ "InstanceName" };


        CompositeType endpointStatsCompositeType = new CompositeType("ConsumerEndpointStats",
                                                  "Name and value pair",
                                                   consumerEndpointItemNames,
                                                   consumerEndpointItemDesc,
                                                   endpointRowAttrTypes);


        TabularDataSupport perfTbData = new TabularDataSupport(perfTabularTable);
        // Add one entry
        CompositeDataSupport perfData = new CompositeDataSupport(perfStatsCompositeType,
                                        perfItemNames, new Object[] { new Long(100),
                                                                      new Long(50),
                                                                      "Documentation"} );
        perfTbData.put(perfData);

        CompositeDataSupport endpointData = new CompositeDataSupport(endpointStatsCompositeType,
                                        consumerEndpointItemNames, new Object[] {"http://localhost/SynchronousSample,service1,endpoint1 statistics",
                                      new Long(10),
                                      new Long(10),
                                      new Long(10),
                                      new Long(10),
                                      new Long(5),
                                      new Long(10),
                                      new Long(10),
                                      new Long(5),
                                      new Long(10),
                                      new Long(10),
                                      new Long(10),
                                      new Long(10),
                                      perfTbData} );

        TabularType endpointTabularTable = new TabularType("EndpointStatsType",
                                               "List of endpoint stats",
                                                    endpointStatsCompositeType,
                                                    endpointRowIndex);

        TabularDataSupport endpointTbData = new TabularDataSupport(endpointTabularTable);
        endpointTbData.put(endpointData);

        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter msgWriter = new java.io.PrintWriter(stringWriter);

        mtd.invoke(theTask, new Object[]{"http://localhost/SynchronousSample,service1,endpoint1 statistics",
                                         endpointTbData, "target1", msgWriter,
                                         "------------------------------------------",
                                         new Boolean(true) });

        msgWriter.close();
        java.lang.System.out.println(stringWriter.getBuffer().toString());
    }

    /**
     * main
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            TestJbiListStatisticsTask testInstance = new TestJbiListStatisticsTask("test");
            // testInstance.testJbiListStatsForFramework();
            // testInstance.testJbiListStatsForNMR();
            // testInstance.testJbiListStatsForComponents();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
