/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiDeployServiceAssemblyTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import java.io.File;
import org.apache.tools.ant.BuildException;


/** This class is an ant task for service assembly deployment.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiDeployServiceAssemblyTask extends JbiTargetTask
{
    /**
     * success msg key
     */
    private static final String PARTIAL_SUCCESS_STATUS_KEY = "jbi.ui.ant.deploy.partial.success";
    
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.deploy.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.deploy.failed";
    
    /** Holds value of property deployFile. */
    private File mDeployFile;
    
    /** Holds value of property service assembly Name. */
    private String mSAName = null;
    
    /** Getter for property service assembly Name.
     * @return Value of property service assembly Name.
     *
     */
    public String getName()
    {
        return this.mSAName;
    }
    
    /**
     * Setter for property service assembly Name.
     * @param name service assembly name.
     */
    public void setName(String name)
    {
        this.mSAName = name;
    }
    
    /** Getter for property deploy file.
     * @return Value of property deploy file.
     *
     */
    public File getFile()
    {
        return this.mDeployFile;
    }
    
    /**
     * Setter for property file.
     * @param file file object
     */
    public void setFile(File file)
    {
        this.mDeployFile = file;
    }
    
    /**
     * validates the file
     */
    protected void validateDeployFile(File deployFile) throws BuildException
    {
        if ( deployFile == null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.deploy.error.sa.archive.file.path.null");
        }
        
        if ( deployFile.getPath().trim().length() <= 0 )
        {
            throwTaskBuildException(
                "jbi.ui.ant.deploy.error.sa.archive.file.path.required");
        }
        
        if ( !deployFile.exists() )
        {
            
            throwTaskBuildException(
                "jbi.ui.ant.deploy.error.sa.archive.file.not.exist",
                deployFile.getName());
        }
        
        if ( deployFile.isDirectory() )
        {
            
            throwTaskBuildException(
                "jbi.ui.ant.deploy.error.sa.archive.file.is.directory");
        }
        
    }
    /**
     * validate compName with valid target
     * @param saName name of the service assembly in the repository
     * @param target value, can not be domain
     */
    protected void validateDeployFromDomainAttributes(String saName, String target)  throws BuildException
    {
        if ( saName.trim().length() == 0 )
        {
            throwTaskBuildException(
                "jbi.ui.ant.deploy.from.domain.error.sa.name.required");
        }
        if ( "domain".equals(target))
        {
            throwTaskBuildException(
                "jbi.ui.ant.deploy.error.sa.invalid.target.with.name.attrib");
        }
    }
    
    /** executes the deployment task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        String deployFileAbsolutePath = null;
        boolean deployFromDoamin = false;
        
        String saName = getName();
        
        File deployFile = getFile();
        
        String target = getValidTarget();
        
        if ( saName == null && deployFile == null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.deploy.error.sa.name.or.file.required");
        }
        
        if ( saName != null && deployFile != null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.deploy.error.sa.name.and.file.set");
        }
        
        if ( saName != null )
        {
            validateDeployFromDomainAttributes(saName, target);
            deployFromDoamin = true;
        }
        else
        {
            validateDeployFile(deployFile);
            deployFileAbsolutePath = deployFile.getAbsolutePath();
        }
        
        // perform deployment
        
        String result = null;
        
        try
        {
            if ( deployFromDoamin )
            {
                result = this.getJBIAdminCommands().deployServiceAssemblyFromDomain(saName, target);
            }
            else
            {
                result = this.getJBIAdminCommands().deployServiceAssembly(deployFileAbsolutePath, target);
            }            
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
        
        processTaskResult(result);
        
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    /**
     * return i18n key for the partial success
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return PARTIAL_SUCCESS_STATUS_KEY;
    }
    
}
