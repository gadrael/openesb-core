/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiListRuntimeLoggersTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for displaying the runtime configuration or component 
 * configuration.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiListRuntimeLoggersTask extends JbiTargetTask
{
    /** logger success msg key   */
    private static final String LOGGER_SUCCESS_STATUS_KEY = "jbi.ui.ant.list.logger.successful";
    /** logger failure msg key   */
    private static final String LOGGER_FAILED_STATUS_KEY = "jbi.ui.ant.list.logger.failed";

    /** INSTANCE ERROR PROPERTY */
    private static final String INSTANCE_ERROR_PROP = "com.sun.jbi.cluster.instance.error";
    
    /** Configuration Name Key */
    private static final String APP_CONFIG_NAME_KEY = "configurationName";
    
    /** Holds Param Nested elements */
    private List mLoggerList;

    /**
     * Logic for listing runtime configuration
     */

    private void executeListRuntimeLoggers()
	throws BuildException
    {
        this.logDebug("Executing List Runtime Loggers ....");
        try
        {
            String	target		= getValidTarget();
            List	tmpLoggerList	= this.getLoggerList();
            List	loggerList	= new ArrayList(tmpLoggerList);

            // Check the list, remove the entry with empty name value
            Iterator iter = tmpLoggerList.iterator();
            while (iter.hasNext())
            {
                Logger logger = (Logger) iter.next();
                if (logger != null)
                {
                    String loggerName = logger.getName();
                    if ((loggerName != null) && (loggerName.trim().compareTo("")==0))
                    {
                        loggerList.remove(logger);
                    }
                }
           }

           if (loggerList.size() == 0)
           {
               Map<String, Level> resultLevels =
                                this.getJBIAdminCommands().getRuntimeLoggerLevels(target);
               if (resultLevels == null)
               {
                   String errMsg = createFailedFormattedJbiAdminResult(
                                            "jbi.ui.ant.task.error.no.logger.resolved", null);
                   throw new BuildException(errMsg);
               }

               printRuntimeLoggerLevels(resultLevels);
           }
           else
           {
               // Go throught the logger elements
               Iterator it = loggerList.iterator();
	       TreeMap<String, Level> resultLevels = new TreeMap<String, Level> ();
               while(it.hasNext())
               {
		    Logger logger = (Logger) it.next();
                    String loggerName = logger.getName();
                    if ((loggerName != null) && (loggerName.compareTo("")!=0))
                    {
		        Level resultLevel = 
                           this.getJBIAdminCommands().getRuntimeLoggerLevel(
                               logger.getName(),
                               target);

                        if (resultLevel == null)
                        {
                            String errMsg = createFailedFormattedJbiAdminResult(
                                            "jbi.ui.ant.task.error.no.logger.resolved", null);
                            throw new BuildException(errMsg);
                        }

		        resultLevels.put(logger.getName(), resultLevel);
                    }
	        }
		printRuntimeLoggerLevels(resultLevels);
            }
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
    }
   
    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        this.logDebug("Executing List Configuration Task....");
        
        executeListRuntimeLoggers();
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
	return LOGGER_FAILED_STATUS_KEY;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
	return LOGGER_SUCCESS_STATUS_KEY;
    }
    
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getLoggerEmptyQueryResultI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.logger.info.empty" : 
            "jbi.ui.ant.print.jbi.comp.logger.info.empty";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.header.separator";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getLoggerQueryResultHeaderI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.logger.info.header" :
            "jbi.ui.ant.print.jbi.comp.logger.info.header";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultPageSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.separator" ;
    }
    
    protected void printRuntimeLoggerLevels(Map<String, Level> theMap)
    {
        this.logDebug("Printing Rimtime Logger Levels....");
        if ( theMap == null ) {
            this.logDebug("List Runtime Logger Level map returns NULL");
        } else {
            this.logDebug("Size of Runtime Logger Level map: " + theMap.size());
        }
        boolean isRuntime = true;
        String header =
            getI18NBundle().getMessage( getLoggerQueryResultHeaderI18NKey(isRuntime), getValidTarget() );
        String headerSeparator =
            getI18NBundle().getMessage( getQueryResultHeaderSeparatorI18NKey(isRuntime) );
        String pageSeparator =
            getI18NBundle().getMessage( getQueryResultPageSeparatorI18NKey(isRuntime) );

        String emptyResult =
            getI18NBundle().getMessage( getLoggerEmptyQueryResultI18NKey(isRuntime) );

        StringWriter stringWriter = new StringWriter();
        PrintWriter msgWriter = new PrintWriter(stringWriter);

        msgWriter.println(headerSeparator);
        msgWriter.println(header);
        msgWriter.println(headerSeparator);

        if ( theMap == null || theMap.size() <= 0 )
        {
            msgWriter.println(emptyResult);
            msgWriter.println(pageSeparator);
        }
        else
        {
            // sort the keys and display for deterministic output
            SortedSet keys = new TreeSet(theMap.keySet());
            for(Object key : keys) {
                String loggerName = (String)key;
                Level level = (Level) theMap.get(loggerName);
                String param = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.jbi.config.param",
                    loggerName, (level==null) ? "DEFAULT" : level.getLocalizedName());
                msgWriter.println(param);
            }
            msgWriter.println(pageSeparator);
        }

        msgWriter.close();
        printMessage(stringWriter.getBuffer().toString());

    }

    /**
     * returns nested element list of <logger>
     * @return Logger List
     */
    protected List getLoggerList()
    {
        if ( this.mLoggerList == null )
        {
            this.mLoggerList = new ArrayList();
        }
        return this.mLoggerList;
    }

    /**
     * factory method for creating the nested element <logger>
     * @return Logger Object
     */
    public Logger createLogger()
    {
        Logger logger = new Logger();
        this.getLoggerList().add(logger);
        return logger;
    }
}
