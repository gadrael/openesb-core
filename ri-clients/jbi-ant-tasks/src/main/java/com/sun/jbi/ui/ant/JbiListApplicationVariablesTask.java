/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiListApplicationVariablesTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for displaying the runtime configuration or component 
 * configuration.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiListApplicationVariablesTask extends JbiTargetTask
{
    /** application variable success msg key   */
    private static final String APPVARIABLE_SUCCESS_STATUS_KEY = "jbi.ui.ant.list.appvariable.successful";
    /** application variable failure msg key   */
    private static final String APPVARIABLE_FAILED_STATUS_KEY = "jbi.ui.ant.list.appvariable.failed";

    /** INSTANCE ERROR PROPERTY */
    private static final String INSTANCE_ERROR_PROP = "com.sun.jbi.cluster.instance.error";
    
    /** Configuration Name Key */
    private static final String APP_CONFIG_NAME_KEY = "configurationName";
    
    /** Holds value of property componentName.  */
    private String mComponentName = null;
   
    /** Holds appvariable Nested elements */
    private List mAppVariableList;

    /** Holds mShowFullLength */
    private boolean mShowFullLength = false;

    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    /**
     * Getter for property showFullLength.
     * @return Value of property showFullLength.
     */
    public boolean getShowFullLength()
    {
        return this.mShowFullLength;
    }

    /**
     * Setter for property showFullLength.
     * @param showFullLength
     */
    public void setShowFullLength(boolean showFullLength)
    {
        this.mShowFullLength = showFullLength;
    }

    /**
     * Central logic for listing component configuration, component logger,
     * application variables and application configuration
     * @param componentName component name
     */
    private void executeListApplicationVariables(String componentName)
	throws BuildException
    {
        try
        {
	    String	target		= getValidTarget();
	    boolean	showFullLength	= getShowFullLength();
            List	appVariableList	= this.getAppVariableList();

	    // Go throught the appvariable elements
            if ((componentName == null) || (componentName.compareTo("") == 0))
            {
                String errMsg = createFailedFormattedJbiAdminResult(
                                                "jbi.ui.ant.task.error.nullCompName",
                                                null);
                throw new BuildException(errMsg);
            }

            this.logDebug("Executing List component application variables ....");

            Properties resultProp =
                this.getJBIAdminCommands().getApplicationVariables(componentName, target);

            List printList = new ArrayList(appVariableList);

            // Check the list, remove the entry with empty name
            Iterator iter = appVariableList.iterator();
            while (iter.hasNext())
            {
                AppVariable appVariable = (AppVariable) iter.next();
                if (appVariable != null)
                {
                    String appVariableName = "" + appVariable.getName();
                    if (appVariableName.trim().compareTo("")==0)
                    {
                        printList.remove(appVariable);
                    }
                }
            }

            StringWriter stringWriter = new StringWriter();
            PrintWriter msgWriter = new PrintWriter(stringWriter);

            String showFullLengthStr = showFullLength ? "true" : "false";

            printApplicationVariables(componentName, printList, resultProp,
                                             target, msgWriter, showFullLength);
            msgWriter.close();
            printMessage(stringWriter.getBuffer().toString());
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }                
    }

    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        this.logDebug("Executing List Configuration Task....");
        String compName = getComponentName();
        
        executeListApplicationVariables(compName);
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
	return APPVARIABLE_FAILED_STATUS_KEY;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
	return APPVARIABLE_SUCCESS_STATUS_KEY;
    }
    
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getEmptyQueryResultI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.empty" : 
            "jbi.ui.ant.print.jbi.comp.config.info.empty";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header" :
            "jbi.ui.ant.print.jbi.comp.config.info.header";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.header.separator";
    }

    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultPageSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.separator" ;
    }
    
    protected void printApplicationVariables(String compName,
                                        List appVariableList,
					Properties returnProp,
					String target,
                                        PrintWriter msgWriter,
                                        boolean showFullLength)
    {
        String SPACE = "  ";

        String header = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.jbi.comp.appvariable.info.header",
                                                new String[] { compName, target } );
        String infoNames =
            getI18NBundle().getMessage( "jbi.ui.ant.print.jbi.comp.appvariable.info.column.names" );
        String headerSeparatorChar =
            getI18NBundle().getMessage( "jbi.ui.ant.print.jbi.comp.appvariable.info.header.separator.char" );
        String pageSeparatorChar = 
            getI18NBundle().getMessage( "jbi.ui.ant.print.jbi.comp.appvariable.info.separator.char" );
        String emptyResult =
            getI18NBundle().getMessage( "jbi.ui.ant.print.jbi.comp.appvariable.info.empty" );

        int maxNameLength = 20;
        int maxTypeLength = 10;
        int maxValueLength = 46;
        int totalLen = maxNameLength + SPACE.length() +
                                        maxTypeLength + SPACE.length() + maxValueLength;

        StringTokenizer nameTk = new StringTokenizer(infoNames, ",");
        String nameStr = nameTk.nextToken();
        String typeStr = nameTk.nextToken();
        String valueStr = nameTk.nextToken();

        String headerSeparator = "";
        String pageSeparator = "";

        for(int i=0; i < totalLen; ++i)
        {
            headerSeparator = headerSeparator + headerSeparatorChar;
            pageSeparator = pageSeparator + pageSeparatorChar;
        }

        String formatStr = "%1$-" + maxNameLength + "s  %2$-" +
                            maxTypeLength + "s  %3$-" + maxValueLength + "s\n"; 
        
        if (returnProp == null || returnProp.size() <= 0)
        {
            msgWriter.println(headerSeparator);
            msgWriter.println(header);
            msgWriter.println(headerSeparator);
            msgWriter.format(formatStr, nameStr, typeStr, valueStr);
            msgWriter.println(pageSeparator);

            msgWriter.println(emptyResult);
            msgWriter.println(pageSeparator);

        }
        else
        {
            Vector entries = new Vector();
          
            SortedSet keys = null;
            if (appVariableList == null || appVariableList.size() <= 0)
            {
                keys = new TreeSet(returnProp.keySet());
            }
            else
            {
                keys = new TreeSet(appVariableList);
            }

            for(Object key : keys)
            {
                String name = null;
                if ( (key != null) && (key instanceof AppVariable ))
                {
                    name = "" + ((AppVariable) key).getName();
                }
                else
                {
                    name = "" + (String)key;
                }

                String tmpStr = returnProp.getProperty(name);
		String valueType = null;
		String value = null;

		if (tmpStr == null)
                {
                    continue;
                }
		else if ((tmpStr.length() > 0) && (tmpStr.charAt(0) == '['))
		{
		    int closingBrkt = tmpStr.indexOf(']');
		    if (closingBrkt == -1)
		    {
			String errMsg =
			    getI18NBundle().getMessage(
                                "jbi.ui.ant.list.appvariable.wrong.format");
			throw new BuildException(errMsg);
		    }
		    valueType = tmpStr.substring(1, closingBrkt);
		    value = tmpStr.substring(closingBrkt+1);
		}
		else
		{
		    valueType = "STRING";
		    value = tmpStr;
		}

                // Recalculate the lengthes if need to show all
                if (showFullLength == true)
                {
                    maxNameLength = maxNameLength < (key+"").length() ?
                                       (name+"").length() : maxNameLength;
                    maxTypeLength = maxTypeLength < (valueType+"").length() ?
                                       (valueType+"").length() : maxTypeLength;
                    maxValueLength = maxValueLength < (value+"").length() ?
                                       (value+"").length() : maxValueLength;
                }

                AppVariable entry = new AppVariable(name, valueType, value);
                entries.add(entry);
            }

            if (showFullLength == true)
            {
                // Recalculate the lengthes if need to show all
                totalLen = maxNameLength + SPACE.length() +
                                        maxTypeLength + SPACE.length() + maxValueLength;

                headerSeparator = "";
                pageSeparator = "";

                for(int i=0; i < totalLen; ++i)
                {
                    headerSeparator = headerSeparator + headerSeparatorChar;
                    pageSeparator = pageSeparator + pageSeparatorChar;
                }
            }

            formatStr = "%1$-" + maxNameLength + "s  %2$-" +
                            maxTypeLength + "s  %3$-" + maxValueLength + "s\n";

            msgWriter.println(headerSeparator);
            msgWriter.println(header);
            msgWriter.println(headerSeparator);
            msgWriter.format(formatStr, nameStr, typeStr, valueStr);
            msgWriter.println(pageSeparator);

            Enumeration enu = entries.elements();
            while (enu.hasMoreElements())
            {
                AppVariable entry =  (AppVariable) enu.nextElement();
                String printNameStr = entry.getName();;
                String printTypeStr = entry.getType();
                String printValueStr = entry.getValue();

                if (showFullLength != true)
                {
                    printNameStr = ((printNameStr+"").length() > maxNameLength) ?
                       (printNameStr+"").substring(0,(maxNameLength-3)) + "..." :
                                                                    printNameStr;
                    printTypeStr =  ((printTypeStr+"").length() > maxTypeLength) ?
                        (printTypeStr+"").substring(0,(maxTypeLength-3)) + "..." :
                                                                     printTypeStr;
                    printValueStr =  ((printValueStr+"").length() > maxValueLength) ?
                        (printValueStr+"").substring(0,(maxValueLength-3)) + "..." :
                                                                      printValueStr;
                }

                msgWriter.format(formatStr, printNameStr, printTypeStr, printValueStr);
            }

            msgWriter.println(pageSeparator);
        }
    }

    /**
     * returns nested element list of &lt;appvariable>
     * @return appvariable List
     */
    protected List getAppVariableList()
    {
        if ( this.mAppVariableList == null )
        {
            this.mAppVariableList = new ArrayList();
        }
        return this.mAppVariableList;
    }

    /**
     * factory method for creating the nested element &lt;param>
     * @return Logger Object
     */
    public AppVariable createAppVariable()
    {
        AppVariable appVariable = new AppVariable();
        this.getAppVariableList().add(appVariable);
        return appVariable;
    }
}
