/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiListComponentConfigurationTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for displaying the runtime configuration or component 
 * configuration.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiListComponentConfigurationTask extends JbiTargetTask
{
    /** success msg key   */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.list.configuration.successful";
    /** failure msg key   */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.list.configuration.failed";
    /** logger success msg key   */

    /** INSTANCE ERROR PROPERTY */
    private static final String INSTANCE_ERROR_PROP = "com.sun.jbi.cluster.instance.error";
    
    /** Holds value of property componentName.  */
    private String mComponentName = null;
   
    /** Holds Param Nested elements */
    private List mParamList;

    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    /**
     * Central logic for listing component configuration, component logger,
     * application variables and application configuration
     * @param componentName component name
     */
    private void executeListComponentConfiguration(String componentName)
	throws BuildException
    {
        try
        {
	    String	target		= getValidTarget();
            List        tmpParamList   = this.getParamList();
            List        paramList      = new ArrayList(tmpParamList);

            // Check the list, remove the entry with empty name value
            Iterator iter = tmpParamList.iterator();
            while (iter.hasNext())
            {
                Param param = (Param) iter.next();
                if (param != null)
                {
                    String paramName = param.getName();
                    if ((paramName != null) && (paramName.trim().compareTo("")==0))
                    {
                        paramList.remove(param);
                    }
                }
            }

	    // Print the component configuration
            this.logDebug("Executing List Component Configuration ....");

            Properties paramsMap = this.getJBIAdminCommands().getComponentConfiguration(componentName, target);
            printComponentConfiguration(componentName, paramList, paramsMap, target);
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }                
    }


    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        this.logDebug("Executing List Component Configuration Task....");
        String compName = getComponentName();
        
        executeListComponentConfiguration(compName);
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getEmptyQueryResultI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.empty" : 
            "jbi.ui.ant.print.jbi.comp.config.info.empty";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header" :
            "jbi.ui.ant.print.jbi.comp.config.info.header";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.header.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.header.separator";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultPageSeparatorI18NKey(boolean isRuntime)
    {
        return isRuntime ? "jbi.ui.ant.print.jbi.config.info.separator" :
            "jbi.ui.ant.print.jbi.comp.config.info.separator" ;
    }
    
    /**
     * Printing component application variables
     * @param compName component name
     * @param params the paramters to be printed
     * @param target target name
     */
    protected void printComponentConfiguration(String compName, List paramList, Properties returnProps, String target )
    {
        this.logDebug("Printing Component Configuration .... for component=" + compName + " on target=" + target);

        boolean isRuntime = false;
        String header =
            getI18NBundle().getMessage( getQueryResultHeaderI18NKey(isRuntime), new String[] { compName, target } );
        String headerSeparator =
            getI18NBundle().getMessage( getQueryResultHeaderSeparatorI18NKey(isRuntime) );
        String pageSeparator =
            getI18NBundle().getMessage( getQueryResultPageSeparatorI18NKey(isRuntime) );
        String emptyResult =
            getI18NBundle().getMessage( getEmptyQueryResultI18NKey(isRuntime) );
        
        StringWriter stringWriter = new StringWriter();
        PrintWriter msgWriter = new PrintWriter(stringWriter);
        
        msgWriter.println(headerSeparator);
        msgWriter.println(header);
        msgWriter.println(headerSeparator);
       
        if ( returnProps == null || returnProps.size() <= 0 )
        {
            msgWriter.println(emptyResult);
            msgWriter.println(pageSeparator);
        }
        else if ( paramList == null || paramList.size() <= 0 )
        {
	    SortedSet keys = new TreeSet(returnProps.keySet());
			
	    for(Object key : keys)
            {
		String name = (String)key;
		String value = returnProps.getProperty(name, "");
		String param = getI18NBundle().getMessage(
			"jbi.ui.ant.print.jbi.comp.config.param",
			name, value);
		msgWriter.println(param);
	    }
	    msgWriter.println(pageSeparator);
	}
        else
        {
            Iterator itr = paramList.iterator();
            while( itr.hasNext() )
            {
                String name = "" + ((Param) itr.next()).getName();
                String value = returnProps.getProperty(name);
                if (value != null)
                {
                    String param = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.jbi.comp.config.param",
                        name, value);
                    msgWriter.println(param);
                }
                else
                {
                     String msg = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.jbi.comp.config.param.not.retrieved",
                        name);
                    msgWriter.println(msg);
                }
            }
	    msgWriter.println(pageSeparator);
        }

        msgWriter.close();
        printMessage(stringWriter.getBuffer().toString());
    }

    /**
     * returns param element list
     * @return param List
     */
    protected List getParamList()
    {
        if ( this.mParamList == null )
        {
            this.mParamList = new ArrayList();
        }
        return this.mParamList;
    }

    /**
     * factory method for creating the nested element &lt;param>
     * @return Param object
     */
    public Param createParam()
    {
        Param param = new Param();
        this.getParamList().add(param);
        return param;
    }
}
