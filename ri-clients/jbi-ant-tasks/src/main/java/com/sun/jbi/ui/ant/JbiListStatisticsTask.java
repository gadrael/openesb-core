/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiListStatisticsTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import org.apache.tools.ant.BuildException;
import java.io.File;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.ArrayType;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Boolean;
import java.lang.IllegalArgumentException;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.JBIStatisticsItemNames;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.ant.util.TimeUtil;

enum StatisticsType
{
    FRAMEWORK	("framework"),
    COMPONENT	("component"),
    SERVICEASSEMBLY	("serviceassembly"),
    ENDPOINT	("endpoint"),
    NMR		("nmr"),
    ALL		("all");

    /** The String alue */
    private String mString;

    StatisticsType(String strValue)
    {
	mString = strValue;
    }

    public String toString()
    {
	return mString.toUpperCase();
    }

    public static StatisticsType valueOfString(String valStr)
    {
	return StatisticsType.valueOf(valStr.toUpperCase());
    }
};

/** This class is an ant task for stopping the service
 *  engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiListStatisticsTask extends JbiTargetTask
{
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY =
                           "jbi.ui.ant.list.statistics.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY =
                           "jbi.ui.ant.list.statistics.failed";
    /**
     * component state
     */
    private static final String  COMPONENT_STARTED_STATE = "Started";

    /**
     * keys for providing endpoints
     */
    private static final String  PROVIDER_ENDPOINT_TYPE_NAME = "ProviderEndpointStats";
     
    /**
     * keys for consuming endpoints 
     */
    private static final String  COMSUMER_ENDPOINT_TYPE_NAME = "ConsumerEndpointStats";
    
    /** Holds value of property statistics type. */
    private String  mStatisticsType;

    /** Holds value of property mComponentName. */
    private List mComponentList;

    /** Holds value of property mServiceAssemblyList. */
    private List mServiceAssemblyList;

    /** Holds value of property mEndpointList. */
    private List mEndpointList;

    /** Getter for property statistics type.
     * @return value of property statistics type.
     *
     */
    public String getType()
    {
        return this.mStatisticsType;
    }

    /**
     * Setter for property statistics type.
     * @param statisticsType the type of statistics data.
     */
    public void setType(String statisticsType)
    {
        this.mStatisticsType = statisticsType;
    }

    /**
     * returns component element list
     * @return component List
     */
    public List getComponentList()
    {
        if ( this.mComponentList == null )
        {
            this.mComponentList = new ArrayList();
        }
        return this.mComponentList;
    }

    /**
     * factory method for creating the nested element &lt;component>
     * @return Component Object
     */
    public Component createComponent()
    {
        Component component = new Component();
        this.getComponentList().add(component);
        return component;
    }

    /**
     * returns serviceassembly element list
     * @return List
     */
    public List getServiceAssemblyList()
    {
        if ( this.mServiceAssemblyList == null )
        {
            this.mServiceAssemblyList = new ArrayList();
        }
        return this.mServiceAssemblyList;
    }

    /**
     * factory method for creating the nested element &lt;serviceassembly>
     * @return ServiceAssembly Object
     */
    public ServiceAssembly createServiceAssembly()
    {
        ServiceAssembly sa = new ServiceAssembly();
        this.getServiceAssemblyList().add(sa);
        return sa;
    }

    /**
     * returns endpoint element list
     * @return component List
     */
    public List getEndpointList()
    {
        if ( this.mEndpointList == null )
        {
            this.mEndpointList = new ArrayList();
        }
        return this.mEndpointList;
    }

    /**
     * factory method for creating the nested element &lt;endpoint>
     * @return Endpoint Object
     */
    public Endpoint createEndpoint()
    {
        Endpoint endpoint = new Endpoint();
        this.getEndpointList().add(endpoint);
        return endpoint;
    }

    /** executes the stop componentnt task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {

        String target = getValidTarget();
        String theTypeStr = getType();        
        try
        {
            StringWriter stringWriter = new StringWriter();
            PrintWriter msgWriter = new PrintWriter(stringWriter);

	    StatisticsType theType = null;
            try
            {
	        theType = StatisticsType.valueOfString(theTypeStr);
            }
            catch (IllegalArgumentException exp)
            {
                String errMsg = null;
                if ((theTypeStr == null) || (theTypeStr.compareTo("") == 0))
                {
                    errMsg = createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.list.statistics.error.empty.type",
                                       null );
                }
                else
                {
                     errMsg = createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.list.statistics.error.wrong.type",
                                       new String [] { theTypeStr } );
                } 

                throw new Exception(errMsg);
            }
            catch (NullPointerException exp)
            {
                String errMsg =  createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.list.statistics.error.empty.type",
                                       new String [] { theTypeStr } );
                throw new Exception(errMsg);
            }

            TabularData 	result = null;

	    switch (theType)
	    {
		case FRAMEWORK:
                    logDebug("Statistics for framework, the target is " + target);
		    result = this.getJBIAdminCommands().getFrameworkStats(target);
                    logDebug("Statistics for framework, the target is " + target);
                    printStatsForFramework(result, target, msgWriter);
		    break;
		case COMPONENT:
                    logDebug("Statistics for components, the target is " + target);
		    getStatsForComponentsAndPrintStats(getComponentList(), target,
                                                          msgWriter, stringWriter);
		    break;
		case SERVICEASSEMBLY:
                    logDebug("Statistics for service assemblies, the target is " + target);
		    getStatsForServiceAssemblies(getServiceAssemblyList(), target,
                                                          msgWriter, stringWriter);
		    break;
		case ENDPOINT:
                    logDebug("Statistics for endpoints, the target is " + target);
		    getStatsForEndpointsAndPrintStats(getEndpointList(), target,
                                                          msgWriter, stringWriter);
		    break;
		case NMR:
                    logDebug("Statistics for NMR, the target is " + target);
		    result = this.getJBIAdminCommands().getNMRStats(target);
                    logDebug("Statistics for NMR, the result is " + result);
                    printStatsForNMR(result, target, msgWriter);
		    break;
                case ALL:
                    logDebug("Statistics for NMR, the target is " + target);
                    result = this.getJBIAdminCommands().getNMRStats(target);
                    logDebug("Statistics for NMR, the result is " + result);
                    printStatsForNMR(result, target, msgWriter);

                    logDebug("Statistics for framework, the target is " + target);
                    result = this.getJBIAdminCommands().getFrameworkStats(target);
                    logDebug("Statistics for framework, the target is " + target);
                    printStatsForFramework(result, target, msgWriter);
                    stringWriter.flush();

                    logDebug("Statistics for components, the target is " + target);
                    getStatsForComponentsAndPrintStats(getComponentList(), target,
                                                          msgWriter, stringWriter);

                    logDebug("Statistics for service assemblies, the target is " + target);
                    getStatsForServiceAssemblies(getServiceAssemblyList(), target,
                                                          msgWriter, stringWriter);

                    logDebug("Statistics for endpoints, the target is " + target);
                    getStatsForEndpointsAndPrintStats(getEndpointList(), target,
                                                          msgWriter, stringWriter);
                    stringWriter.flush();
                    break;
                default:
                    String errMsg = null;
                    if ((theTypeStr == null) || (theTypeStr.compareTo("") == 0))
                    {
                        errMsg = createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.list.statistics.error.empty.type",
                                       new String [] { theTypeStr } );
                    }
                    else
                    {
                         errMsg = createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.list.statistics.error.wrong.type",
                                       null );
                    }

                    throw new Exception(errMsg);
	    }

            msgWriter.close();
            printMessage(stringWriter.getBuffer().toString());
            stringWriter.close();

        }
        catch (Exception ex )
        {
            throw new BuildException(ex);
        }
    }

    /**
     * Print statistics data for framework
     * @param result the Tabular data containing stats data
     * @param target
     * @param msgWriter the PrintWriter
     */
    private void printStatsForFramework(TabularData result, String target,
                                                       PrintWriter msgWriter)
    {
        String header = getI18NBundle().getMessage(
                            "jbi.ui.ant.print.jbi.stats.framework.info.header",
                                                       new String[] { target } );
        String headerSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.header.separator");
        String pageSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.separator");

        String emptyResult = getI18NBundle().getMessage(
                                      "jbi.ui.ant.print.jbi.stats.framework.info.empty");

        msgWriter.println(headerSeparator);
        msgWriter.println(header);
        msgWriter.println(headerSeparator);

        if ((result == null) || (result.size() == 0))
        {
            msgWriter.println(emptyResult);
            msgWriter.println(pageSeparator);
            return;
        }

        for (Iterator dataIter=result.values().iterator(); dataIter.hasNext(); ) {
            CompositeData compData = (CompositeData) dataIter.next();
            String instanceName = (String) compData.get(JBIStatisticsItemNames.INSTANCE_NAME);
            Long startupTime = (Long) compData.get(JBIStatisticsItemNames.FRAMEWORK_STARTUP_TIME);
            Long upTime = (Long) compData.get(JBIStatisticsItemNames.FRAMEWORK_UPTIME);

            startupTime = (startupTime!=null)? startupTime : ((long) 0);
            upTime = (upTime!=null)? upTime : ((long) 0);

            String instanceNameLine = getI18NBundle().getMessage(
                                      "jbi.ui.ant.print.jbi.stats.framework.instance.name",
                                                  new String[] { instanceName });
            String formatString = getI18NBundle().getMessage(
                                           "jbi.ui.ant.print.jbi.stats.framework.up.time");
            TimeUtil timeUtil = new TimeUtil(upTime);
            String upTimeLine = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.framework.up.time",
                                                  new String[] { upTime.toString() });
            String startupTimeLine = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.framework.startup.time",
                                                  new String[] { startupTime.toString() });

            msgWriter.println(instanceNameLine);
            msgWriter.println(pageSeparator);
            msgWriter.format( formatString+"\n", timeUtil.getDays(),
                                                 timeUtil.getHours(),
                                                 timeUtil.getMins(),
                                                 timeUtil.getSecs());
            msgWriter.println(startupTimeLine);

            msgWriter.println("");
        }
    }

    /**
     * Print statistics data for components
     * @param compName
     * @param result the vector data containing tabular stats data
     * @param target
     * @param msgWriter the PrintWriter
     * @param pageSeparator
     * @param jUnitFlag
     */
    private void printStatsForComponent(String compName, TabularData result, String target,
                                PrintWriter msgWriter, String pageSeparator, Boolean jUnitFlag)
    {
        if (compName != null)
        {
            msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.component.name",
                                     new String[] { compName }) );
            msgWriter.println(pageSeparator);
        }
        else
        {
            return;
        }

        if ((result == null) ||
            (result.values() == null) ||
            (result.values().size() == 0))
        {
            String emptyResult = getI18NBundle().getMessage(
                                      "jbi.ui.ant.print.jbi.stats.component.info.nostats");

            msgWriter.println(emptyResult);
            msgWriter.println("");
            msgWriter.println("");
           
            return;
        }

        Collection collect = result.values();
        Iterator dataIter= collect.iterator();
        for (; dataIter.hasNext(); )
        {
            CompositeData compData = (CompositeData) dataIter.next();
            CompositeType compType = compData.getCompositeType();
            Set compItemSet = compType.keySet();
            String instanceName = "";

            if (compItemSet.contains(JBIStatisticsItemNames.COMPONENT_UPTIME))
            {
                Long componentUpTime = (Long) compData.get(JBIStatisticsItemNames.COMPONENT_UPTIME);
                String formatString = getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.component.up.time");
                TimeUtil timeUtil = new TimeUtil(componentUpTime);
                msgWriter.format( formatString+"\n", timeUtil.getDays(),
                                                      timeUtil.getHours(),
                                                      timeUtil.getMins(),
                                                      timeUtil.getSecs());
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_ACTIVATED_ENDPOINTS))
            {
                Long numActivatedEndpoints = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_ACTIVATED_ENDPOINTS);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.activated.endpoints",
                                             new String[] { (numActivatedEndpoints != null)?
                                                             numActivatedEndpoints.toString() : "" }) );
            }

            if  (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REQUESTS))
            {
                Long numReceivedRequests = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REQUESTS);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.received.requests",
                                             new String[] { (numReceivedRequests != null)?
                                                             numReceivedRequests.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_REQUESTS))
            {
                Long numSentRequests = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_REQUESTS);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.sent.requests",
                                             new String[] { (numSentRequests != null)?
                                                             numSentRequests.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REPLIES))
            {
                Long numReceivedReplies = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REPLIES);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.received.replies",
                                             new String[] { (numReceivedReplies != null)?
                                                             numReceivedReplies.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_REPLIES))
            {
                Long numSentReplies = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_REPLIES);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.sent.replies",
                                             new String[] { (numSentReplies != null)?
                                                             numSentReplies.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES))
            {
                Long numReceivedDONEs = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.received.dones",
                                             new String[] { (numReceivedDONEs != null)?
                                                             numReceivedDONEs.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES))
            {
                Long numSentDONEs = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.sent.dones",
                                             new String[] { (numSentDONEs != null)?
                                                             numSentDONEs.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS))
            {
                    Long numReceivedFaults = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS);
                    msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.received.faults",
                                             new String[] { (numReceivedFaults != null)?
                                                             numReceivedFaults.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS))
            {
                Long numSentFaults = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.sent.faults",
                                             new String[] { (numSentFaults != null)?
                                                             numSentFaults.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS))
            {
                Long numReceivedErrors= (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.received.errors",
                                             new String[] { (numReceivedErrors != null)?
                                                             numReceivedErrors.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS))
            {
                Long numSentErrors= (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.sent.errors",
                                             new String[] { (numSentErrors != null)?
                                                             numSentErrors.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_COMPLETED_EXCHANGES))
            {
                Long numCompletedExchanges = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_COMPLETED_EXCHANGES);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.completed.exchanges",
                                             new String[] { (numCompletedExchanges != null)?
                                                             numCompletedExchanges.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES))
            {
                Long numActiveExchanges = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.active.exchanges",
                                             new String[] { (numActiveExchanges != null)?
                                                             numActiveExchanges.toString() : "" }) );
            }

            if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_ERROR_EXCHANGES))
            {
                Long numErrorExchanges = (Long) compData.get(JBIStatisticsItemNames.NUMBER_OF_ERROR_EXCHANGES);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.num.error.exchanges",
                                             new String[] { (numErrorExchanges != null)?
                                                             numErrorExchanges.toString() : "" }) );
            }

            msgWriter.println( "\n" +
                               getI18NBundle().getMessage(
                                   "jbi.ui.ant.print.jbi.stats.common.message.exchanges.statistics", null) +
                               "\n");

            String valueNotAvailable = getI18NBundle().getMessage(
                                       "jbi.ui.ant.print.jbi.stats.common.message.value.not.available", null);

            if (!compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME) &&
                 jUnitFlag != true)
            {
                if (getMsgSvcTimingStatisticsEnabledFlag(target) == false)
                {
                    String msgString = getI18NBundle().getMessage(
                                          "jbi.ui.ant.print.jbi.stats.timing.disabled.msg");
                    msgWriter.println("  " + msgString);
                }
                else
                {
                    String msgString = getI18NBundle().getMessage(
                                          "jbi.ui.ant.print.jbi.stats.timing.data.not.available");
                    msgWriter.println("  " + msgString);
                }
            }
            else
            {
                if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME))
                {
                    Long mepResponseTime = (Long) compData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME);
                    msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.mep.response.time",
                                             new String[] { mepResponseTime == null ?
                                                             valueNotAvailable : mepResponseTime.toString() }) );
                }

                if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME))
                {
                    Long mepComponentTime = (Long) compData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME);
                    msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.mep.component.time",
                                             new String[] { instanceName,
                                                             mepComponentTime == null ?
                                                             valueNotAvailable : mepComponentTime.toString() }) );
                }

                if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME))
                {
                    Long mepDeliveryChannelTime = (Long) compData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME);
                    msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.mep.delivery.channel.time",
                                             new String[] { mepDeliveryChannelTime == null ?
                                                             valueNotAvailable : mepDeliveryChannelTime.toString() } ));
                }

                if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME))
                {
                    Long mepMessageServiceTime = (Long) compData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME);
                    msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.mep.message.service.time",
                                             new String[] { mepMessageServiceTime == null ?
                                                             valueNotAvailable : mepMessageServiceTime.toString() }) );
                }

                if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_STATUS_TIME))
                {
                    Long mepStatusTime = (Long) compData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_STATUS_TIME);
                    msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.mep.status.time",
                                             new String[] { mepStatusTime == null ?
                                                             valueNotAvailable : mepStatusTime.toString() }) );
                }
            }

            if (compItemSet.contains(JBIStatisticsItemNames.COMPONENT_EXTENSION_STATS))
            {
                msgWriter.println( "\n" + getI18NBundle().getMessage(
                                   "jbi.ui.ant.print.jbi.stats.common.provided.statistics", null) +
                                   "\n");
                CompositeData  componentExtensionStats = (CompositeData) compData.get(
                                                            JBIStatisticsItemNames.COMPONENT_EXTENSION_STATS);
                if (componentExtensionStats != null)
                {
                    printCompositeDataRecursively(componentExtensionStats, msgWriter, "\t");
                }
            }
            msgWriter.println("");
        }

        msgWriter.println("");
    }

    /** 
     * Print statistics data for service assemblies
     * @param results the vector data containing tabular stats data
     * @param target
     * @param msgWriter the PrintWriter
     */
    private void printStatsForServiceAssembly(TabularData result, String target,
                                              PrintWriter msgWriter, String pageSeparator)
    {
        String valueNotAvailable = getI18NBundle().getMessage(
                                       "jbi.ui.ant.print.jbi.stats.common.message.value.not.available", null);

        Collection collect = result.values();
        Iterator dataIter= collect.iterator();
        for (; dataIter.hasNext(); )
        {
            CompositeData saCompData = (CompositeData) dataIter.next();
            CompositeType saCompType = saCompData.getCompositeType();
            Set saCompItemSet = saCompType.keySet();

            if (saCompItemSet.contains(JBIStatisticsItemNames.SERVICE_ASSEMBLY_NAME))
            {
                String saName = (String) saCompData.get(JBIStatisticsItemNames.SERVICE_ASSEMBLY_NAME);
                msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.sa.name",
                                     new String[] { saName }) );
                msgWriter.println(pageSeparator);
            }

            if (saCompItemSet.contains(JBIStatisticsItemNames.SERVICE_ASSEMBLY_LAST_STARTUP_TIME))
            {
                Date saLastStartupTime = (Date) saCompData.get(JBIStatisticsItemNames.SERVICE_ASSEMBLY_LAST_STARTUP_TIME);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.sa.last.startup.time",
                                             new String[] { (saLastStartupTime != null)?
                                                             saLastStartupTime.toString() : "" }) );
            }

            if (saCompItemSet.contains(JBIStatisticsItemNames.SERVICE_ASSEMBLY_SHUTDOWN_TIME))
            {
                Long saShutdownTime = (Long) saCompData.get(JBIStatisticsItemNames.SERVICE_ASSEMBLY_SHUTDOWN_TIME);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.sa.shutdown.time",
                                             new String[] { saShutdownTime == null ?
                                                             valueNotAvailable : saShutdownTime.toString() }) );
            }

            if (saCompItemSet.contains(JBIStatisticsItemNames.SERVICE_ASSEMBLY_STARTUP_TIME))
            {
                Long saStartupTime = (Long) saCompData.get(JBIStatisticsItemNames.SERVICE_ASSEMBLY_STARTUP_TIME);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.sa.startup.time",
                                             new String[] { saStartupTime == null ?
                                                             valueNotAvailable : saStartupTime.toString() }) );
            }

            if (saCompItemSet.contains(JBIStatisticsItemNames.SERVICE_ASSEMBLY_STOP_TIME))
            {
                Long saStopTime = (Long) saCompData.get(JBIStatisticsItemNames.SERVICE_ASSEMBLY_STOP_TIME);
                msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.sa.stop.time",
                                             new String[] { saStopTime == null ?
                                                             valueNotAvailable : saStopTime.toString() }) );
            }

            msgWriter.println( "" );
            String tab = "\t";

            if (saCompItemSet.contains(JBIStatisticsItemNames.SERVICE_ASSEMBLY_SU_STATISTICS))
            {
                CompositeData[] suStatistics = (CompositeData[]) saCompData.get(JBIStatisticsItemNames.SERVICE_ASSEMBLY_SU_STATISTICS);

                for(int i=0; i < suStatistics.length; ++i)
                {
                    CompositeData suCompData = suStatistics[i];
                    CompositeType suCompType = suCompData.getCompositeType();
                    Set suCompItemSet = suCompType.keySet();

                    if (suCompItemSet.contains(JBIStatisticsItemNames.SERVICE_UNIT_NAME))
                    {
                        String suName = (String) suCompData.get(JBIStatisticsItemNames.SERVICE_UNIT_NAME);
                        msgWriter.println( getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.su.name",
                                             new String[] { suName }) );
                    }

                    if (suCompItemSet.contains(JBIStatisticsItemNames.SERVICE_UNIT_SHUTDOWN_TIME))
                    {
                        Long suShutdownTime = (Long) suCompData.get(JBIStatisticsItemNames.SERVICE_UNIT_SHUTDOWN_TIME);
                        msgWriter.println( tab + getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.su.shutdown.time",
                                             new String[] { suShutdownTime == null ?
                                                             valueNotAvailable : suShutdownTime.toString() }) );
                    }

                    if (suCompItemSet.contains(JBIStatisticsItemNames.SERVICE_UNIT_STARTUP_TIME))
                    {
                        Long suStartupTime = (Long) suCompData.get(JBIStatisticsItemNames.SERVICE_UNIT_STARTUP_TIME);
                        msgWriter.println( tab + getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.su.startup.time",
                                             new String[] { suStartupTime == null ?
                                                             valueNotAvailable : suStartupTime.toString() }) );
                    }


                    if (suCompItemSet.contains(JBIStatisticsItemNames.SERVICE_UNIT_STOP_TIME))
                    {
                        Long suStopTime = (Long) suCompData.get(JBIStatisticsItemNames.SERVICE_UNIT_STOP_TIME);
                        msgWriter.println( tab + getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.su.stop.time",
                                             new String[] { suStopTime == null ?
                                                             valueNotAvailable : suStopTime.toString() }) );
                    }

                    msgWriter.println("");
                }

                msgWriter.println("");
            }
            msgWriter.println("");
        }
    }

    /** 
     * Print statistics data for endpoints
     * @param endpointName
     * @param result the vector data containing tabular stats data
     * @param target
     * @param msgWriter the PrintWriter
     * @param pageSeparator
     * @param jUnitFlag
     */
    private void printStatsForEndpoint(String endpointName, TabularData result, String target,
                                  PrintWriter msgWriter, String pageSeparator, Boolean jUnitFlag)
    {
        for (Iterator dataIter=result.values().iterator(); dataIter.hasNext(); )
        {
            CompositeData endpointCompData = (CompositeData) dataIter.next();
            CompositeType compType = endpointCompData.getCompositeType();
            Set compItemSet = compType.keySet();
            String compTypeName = endpointCompData.getCompositeType().getTypeName();
            if ((""+compTypeName).compareTo(PROVIDER_ENDPOINT_TYPE_NAME) == 0)
            {
                msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.endpoint.provider.name",
                                     new String[] { endpointName }) );
                msgWriter.println(pageSeparator);

                if (compItemSet.contains(JBIStatisticsItemNames.PROVIDER_ENDPOINT_ACTIVATION_TIME))
                {
                    Date activationTime = (Date) endpointCompData.get(JBIStatisticsItemNames.PROVIDER_ENDPOINT_ACTIVATION_TIME);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.endpoint.activation.time",
                                     new String[] { activationTime.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.PROVIDER_ENDPOINT_UPTIME))
                {
                    Long upTime = (Long) endpointCompData.get(JBIStatisticsItemNames.PROVIDER_ENDPOINT_UPTIME);
                    String formatString = getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.endpoint.up.time");
                    TimeUtil timeUtil = new TimeUtil(upTime);
                    msgWriter.format( formatString+"\n", timeUtil.getDays(),
                                                      timeUtil.getHours(),
                                                      timeUtil.getMins(),
                                                      timeUtil.getSecs());
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES))
                {
                    Long numActiveExchanges = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.active.exchanges",
                                     new String[] { numActiveExchanges.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REQUESTS))
                {
                    Long numReceivedRequests = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REQUESTS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.received.requests",
                                     new String[] { numReceivedRequests.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_REPLIES))
                {
                    Long numSentReplies = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_REPLIES);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.sent.replies",
                                     new String[] { numSentReplies.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES))
                {
                    Long numReceivedDONEs = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.received.dones",
                                     new String[] { numReceivedDONEs.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES))
                {
                    Long numSentDONEs = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.sent.dones",
                                     new String[] { numSentDONEs.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS))
                {
                    Long numReceivedFaults = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.received.faults",
                                     new String[] { numReceivedFaults.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS))
                {
                    Long numSentFaults = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.sent.faults",
                                     new String[] { numSentFaults.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS))
                {
                    Long numReceivedErrors = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.received.errors",
                                     new String[] { numReceivedErrors.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS))
                {
                    Long numSentErrors = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.sent.errors",
                                     new String[] { numSentErrors.toString() }) );
                }

                String valueNotAvailable = getI18NBundle().getMessage(
                                       "jbi.ui.ant.print.jbi.stats.common.message.value.not.available", null);

                msgWriter.println( "\n" +
                                   getI18NBundle().getMessage(
                                       "jbi.ui.ant.print.jbi.stats.common.message.exchanges.statistics", null) +
                                   "\n");

                if (!compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME) &&
                     jUnitFlag != true)
                {
                    if (getMsgSvcTimingStatisticsEnabledFlag(target) == false)
                    {
                        String msgString = getI18NBundle().getMessage(
                                          "jbi.ui.ant.print.jbi.stats.timing.disabled.msg");
                        msgWriter.println("  " + msgString);
                    }
                    else
                    {
                        String msgString = getI18NBundle().getMessage(
                                          "jbi.ui.ant.print.jbi.stats.timing.data.not.available");
                        msgWriter.println("  " + msgString);
                    }
                }
                else
                {
                    if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME))
                    {
                        Long mepResponseTime = (Long) endpointCompData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME);
                        msgWriter.println( getI18NBundle().getMessage(
                              "jbi.ui.ant.print.jbi.stats.common.mep.response.time",
                              new String[] { mepResponseTime == null ?
                                          valueNotAvailable : mepResponseTime.toString() }) );
                    }
                    if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME))
                    {
                        String componentName = (String) endpointCompData.get(JBIStatisticsItemNames.ENDPOINT_COMPONENT_NAME);
                        Long mepComponentTime = (Long) endpointCompData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME);
                        msgWriter.println( getI18NBundle().getMessage(
                              "jbi.ui.ant.print.jbi.stats.common.mep.component.time",
                              new String[] { componentName+"",
                                         mepComponentTime == null ?
                                          valueNotAvailable : mepComponentTime.toString() }) );
                    }
                    if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME))
                    {
                        Long mepDeliveryChannelTime = (Long) endpointCompData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME);
                        msgWriter.println( getI18NBundle().getMessage(
                              "jbi.ui.ant.print.jbi.stats.common.mep.delivery.channel.time",
                              new String[] { mepDeliveryChannelTime == null ?
                                          valueNotAvailable : mepDeliveryChannelTime.toString() }) );
                    }
                    if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME))
                    {
                        Long mepMessageServiceTime = (Long) endpointCompData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME);
                        msgWriter.println( getI18NBundle().getMessage(
                              "jbi.ui.ant.print.jbi.stats.common.mep.message.service.time",
                              new String[] { mepMessageServiceTime == null ?
                                          valueNotAvailable : mepMessageServiceTime.toString() }) );
                    }
                }

                if (compItemSet.contains(JBIStatisticsItemNames.OJC_PERFORMANCE_STATS))
                {
                    msgWriter.println( "\n" + getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.perf.instr.dataline", null) +
                                   "\n");

                    TabularData pfTbData = (TabularData) endpointCompData.get(JBIStatisticsItemNames.OJC_PERFORMANCE_STATS);
                    for (Iterator pfTbIter=pfTbData.values().iterator(); pfTbIter.hasNext(); )
                    {
                        CompositeData pfCompData = (CompositeData) pfTbIter.next();
                        printCompositeDataRecursively(pfCompData, msgWriter, "");
                        msgWriter.println("");
                    }
                }
            }
            else if ((""+compTypeName).compareTo(COMSUMER_ENDPOINT_TYPE_NAME) == 0)
            {
                msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.endpoint.consumer.name",
                                     new String[] { endpointName }) );
                msgWriter.println(pageSeparator);

                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_REQUESTS))
                {
                    Long numSentRequests = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_REQUESTS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.sent.requests",
                                     new String[] { numSentRequests.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REPLIES))
                {
                    Long numReceivedRequests = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REPLIES);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.received.replies",
                                     new String[] { numReceivedRequests.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES))
                {
                    Long numReceivedDONEs = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.received.dones",
                                     new String[] { numReceivedDONEs.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES))
                {
                    Long numSentDONEs = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.sent.dones",
                                     new String[] { numSentDONEs.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS))
                {
                    Long numReceivedFaults = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.received.faults",
                                     new String[] { numReceivedFaults.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS))
                {
                    Long numSentFaults = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.sent.faults",
                                     new String[] { numSentFaults.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS))
                {
                    Long numReceivedErrors = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.received.errors",
                                     new String[] { numReceivedErrors.toString() }) );
                }
                if (compItemSet.contains(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS))
                {
                    Long numSentErrors = (Long) endpointCompData.get(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS);
                    msgWriter.println( getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.common.num.sent.errors",
                                     new String[] { numSentErrors.toString() }) );
                }


                String valueNotAvailable = getI18NBundle().getMessage(
                                       "jbi.ui.ant.print.jbi.stats.common.message.value.not.available", null);

                msgWriter.println( "\n" +
                                   getI18NBundle().getMessage(
                                       "jbi.ui.ant.print.jbi.stats.common.message.exchanges.statistics", null) +
                                   "\n");

                if (!compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME) &&
                     jUnitFlag != true)
                {
                    if (getMsgSvcTimingStatisticsEnabledFlag(target) == false)
                    {
                        String msgString = getI18NBundle().getMessage(
                                          "jbi.ui.ant.print.jbi.stats.timing.disabled.msg");
                        msgWriter.println("  " + msgString);
                    }
                    else
                    {
                        String msgString = getI18NBundle().getMessage(
                                          "jbi.ui.ant.print.jbi.stats.timing.data.not.available");
                        msgWriter.println("  " + msgString);
                    }
                }
                else
                {
                    if (compItemSet.contains(JBIStatisticsItemNames.CONSUMING_ENDPOINT_STATUS_TIME))
                    {
                        Long mepStatusTime = (Long) endpointCompData.get(JBIStatisticsItemNames.CONSUMING_ENDPOINT_STATUS_TIME);
                        msgWriter.println( getI18NBundle().getMessage(
                              "jbi.ui.ant.print.jbi.stats.common.mep.status.time",
                              new String[] { mepStatusTime == null ?
                                          valueNotAvailable : mepStatusTime.toString() }) );
                    }
                    if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME))
                    {
                        Long mepComponentTime = (Long) endpointCompData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME);
                        msgWriter.println( getI18NBundle().getMessage(
                              "jbi.ui.ant.print.jbi.stats.common.mep.component.time.noname",
                              new String[] { mepComponentTime == null ?
                                          valueNotAvailable : mepComponentTime.toString() }) );
                    }
                    if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME))
                    {
                        Long mepDeliveryChannelTime = (Long) endpointCompData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME);
                        msgWriter.println( getI18NBundle().getMessage(
                              "jbi.ui.ant.print.jbi.stats.common.mep.delivery.channel.time",
                              new String[] { mepDeliveryChannelTime == null ?
                                          valueNotAvailable : mepDeliveryChannelTime.toString() }) );
                    }
                    if (compItemSet.contains(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME))
                    {
                        Long mepNMRTime = (Long) endpointCompData.get(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME);
                        msgWriter.println( getI18NBundle().getMessage(
                          "jbi.ui.ant.print.jbi.stats.common.mep.message.service.time",
                          new String[] { mepNMRTime == null ?
                                          valueNotAvailable : mepNMRTime.toString() }) );
                    }
                }

                msgWriter.println( "\n" + getI18NBundle().getMessage(
                                             "jbi.ui.ant.print.jbi.stats.common.perf.instr.dataline", null) +
                                   "\n");

                if (compItemSet.contains(JBIStatisticsItemNames.OJC_PERFORMANCE_STATS))
                {
                    TabularData pfTbData = (TabularData) endpointCompData.get(JBIStatisticsItemNames.OJC_PERFORMANCE_STATS);
                    for (Iterator pfTbIter=pfTbData.values().iterator(); pfTbIter.hasNext(); )
                    {
                        CompositeData pfCompData = (CompositeData) pfTbIter.next();
                        printCompositeDataRecursively(pfCompData, msgWriter, "");
                        msgWriter.println("");
                    }
                }
            }
            msgWriter.println("");
        }
        msgWriter.println("");
    }

    /** 
     * Print statistics data for components
     * @param result the tabular data containing NMR stats data
     * @param target
     * @param msgWriter the PrintWriter
     */
    private void printStatsForNMR(TabularData result, String target,
                                                              PrintWriter msgWriter)
    {
        String header = getI18NBundle().getMessage(
                            "jbi.ui.ant.print.jbi.stats.nmr.info.header",
                                                       new String[] { target } );
        String headerSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.header.separator");
        String pageSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.separator");

        String emptyResult = getI18NBundle().getMessage(
                                      "jbi.ui.ant.print.jbi.stats.nmr.info.empty");

        msgWriter.println(headerSeparator);
        msgWriter.println(header);
        msgWriter.println(headerSeparator);

        if ((result == null) || (result.size() == 0))
        {
            msgWriter.println(emptyResult);
            msgWriter.println(pageSeparator);
            return;
        }

        for (Iterator dataIter=result.values().iterator(); dataIter.hasNext(); ) {
            CompositeData compData = (CompositeData) dataIter.next();
            String instanceName = (String) compData.get(JBIStatisticsItemNames.INSTANCE_NAME);
            String[] listActiveChannels = (String []) compData.get(
                    JBIStatisticsItemNames.NMR_STATS_ACTIVE_CHANNELS);
            String[] listActiveEndpoints = (String []) compData.get(
                    JBIStatisticsItemNames.NMR_STATS_ACTIVE_ENDPOINTS);

            String instanceNameLine = getI18NBundle().getMessage(
                                      "jbi.ui.ant.print.jbi.stats.nmr.instance.name",
                                                  new String[] { instanceName });

            msgWriter.println(instanceNameLine);
            msgWriter.println(pageSeparator);

            if (listActiveChannels != null)
            {
                String activeChannelNumberLine = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.nmr.active.channel.number",
                                          new String[] { Integer.toString(listActiveChannels.length) });

                msgWriter.println(activeChannelNumberLine);
                for(int i=0; i < listActiveChannels.length; ++i)
                {
                   msgWriter.println(listActiveChannels[i]);
                }

            }

            msgWriter.println("");

            if (listActiveEndpoints != null)
            {
                String activeEndpointNumberLine = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.nmr.active.endpoint.number",
                                     new String[] { Integer.toString(listActiveEndpoints.length) });
                msgWriter.println(activeEndpointNumberLine);
                for(int i=0; i < listActiveEndpoints.length; ++i)
                {
                   msgWriter.println(listActiveEndpoints[i]);
                }
            }

            msgWriter.println("");
            msgWriter.println("");
        }
    }

    /**
     * Query the stats data for components
     * @param theList the list which contains component element
     * @param target
     * @return the vector which contains comoonent stats data in tabular objects
     */
    private void getStatsForComponentsAndPrintStats(List theList, String target,
                                   PrintWriter msgWriter, StringWriter stringWriter)
    {

        ArrayList componentList = new ArrayList(theList);

        // Check the list, remove the entry with empty name value
        Iterator compIter = theList.iterator();
        while (compIter.hasNext())
        {
             Component comp = (Component) compIter.next();
             if (comp != null)
             {
                 String compName = comp.getName();
                 if ((compName != null) && (compName.trim().compareTo("")==0))
                 {
                     componentList.remove(comp);
                 }
             }
        }

        try
        {
            if (componentList.size() == 0)
            {
                 compIter = null;
                 JBIComponentInfo compInfo = null;

                 List bcList = JBIComponentInfo.readFromXmlText(
                                 this.getJBIAdminCommands().listBindingComponents(COMPONENT_STARTED_STATE,
                                                                                  null,
                                                                                  null,
                                                                                  target));
                 compIter = bcList.iterator();
                 while (compIter.hasNext())
                 {
                     compInfo = (JBIComponentInfo) compIter.next();
                     Component component = new Component();
                     component.setName(compInfo.getName());
                     componentList.add(component);
                 }

                 List seList = JBIComponentInfo.readFromXmlText(
                                 this.getJBIAdminCommands().listServiceEngines(COMPONENT_STARTED_STATE,
                                                                               null,
                                                                               null,
                                                                               target));
                                   compIter = bcList.iterator();
                 compIter = seList.iterator();
                 while (compIter.hasNext())
                 {
                     compInfo = (JBIComponentInfo) compIter.next(); 
                     Component component = new Component();
                     component.setName(compInfo.getName());
                     componentList.add(component);
                 }
            }


            // Start to print ...
            String header = getI18NBundle().getMessage(
                            "jbi.ui.ant.print.jbi.stats.component.info.header",
                                                       new String[] { target } );
            String headerSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.header.separator");
            String pageSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.separator");

            String emptyResult = getI18NBundle().getMessage(
                                      "jbi.ui.ant.print.jbi.stats.component.info.empty");

            msgWriter.println(headerSeparator);
            msgWriter.println(header);
            msgWriter.println(headerSeparator);

            int results = 0;
            Iterator it = componentList.iterator();
            while (it.hasNext())
            {
                String compName = (String) ((Component) it.next()).getName();
                logDebug("The component name is: " + compName);
                TabularData result = this.getJBIAdminCommands().getComponentStats(
                                                 compName,
                                                 target);
                printStatsForComponent(compName, result, target,
                                         msgWriter, pageSeparator, false);
                stringWriter.flush();

                ++results;
            }

            if (results == 0)
            {
                msgWriter.println(emptyResult);
                msgWriter.println(pageSeparator);
            }
        }
        catch (Exception ex)
        {
            processTaskException(ex);
        }
    }

    /**
     * Query the stats data for service assemblies
     * @param theList the list which contains service assembly elements
     * @param target
     * @return the vector which contains service assembly stats data in tabular objects
     */
    private void getStatsForServiceAssemblies(List theList, String target,
                                   PrintWriter msgWriter, StringWriter stringWriter)
    {
        ArrayList saNameList = new ArrayList(theList);

        // Check the list, remove the entry with empty name value
        Iterator saNameIter = theList.iterator();
        while (saNameIter.hasNext())
        {
            ServiceAssembly sa = (ServiceAssembly) saNameIter.next();
            if (sa != null)
            {
                String saName = sa.getName();
                if ((saName != null) && (saName.trim().compareTo("")==0))
                {
                    saNameList.remove(sa);
                }
            }
        }

        try
        {
            if (saNameList.size() == 0)
            {
                List saList = ServiceAssemblyInfo.readFromXmlTextWithProlog(
                                 this.getJBIAdminCommands().listServiceAssemblies(target));

                ServiceAssemblyInfo saInfo = null;
                Iterator saIter = saList.iterator();
                logDebug("Start to list sa names");
                while (saIter.hasNext())
                {
                    saInfo = (ServiceAssemblyInfo) saIter.next();
                    ServiceAssembly sa = new ServiceAssembly();
                    sa.setName(saInfo.getName());
                    saNameList.add(sa);
                    logDebug("The sa name is: " + saInfo.getName());
                }
            }
 
            // Start to print ...
            String header = getI18NBundle().getMessage(
                            "jbi.ui.ant.print.jbi.stats.sa.info.header",
                                                       new String[] { target } );
            String headerSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.header.separator");
            String pageSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.separator");

            String emptyResult = getI18NBundle().getMessage(
                                      "jbi.ui.ant.print.jbi.stats.sa.info.empty");

            msgWriter.println(headerSeparator);
            msgWriter.println(header);
            msgWriter.println(headerSeparator);

            int results = 0;
            Iterator it = saNameList.iterator();
            while (it.hasNext())
            {
                String saName = ((ServiceAssembly) it.next()).getName();
                TabularData result = (TabularData)
                         this.getJBIAdminCommands().getServiceAssemblyStats(saName, target);
                if (result != null)
                {
                    printStatsForServiceAssembly(result, target,
                                             msgWriter, pageSeparator);
                    stringWriter.flush();
                    ++results;
                }
            }

            if (results == 0)
            {
                msgWriter.println(emptyResult);
                msgWriter.println(pageSeparator);
            }
        }
        catch (Exception ex)
        {
            processTaskException(ex);
        }
    }

    /**
     * Query the stats data for endpoints 
     * @param theList the list which contains endpoint element
     * @param target
     * @return the vector which contains comoonent stats data in tabular objects
     */
    private void getStatsForEndpointsAndPrintStats(List theList, String target,
                                   PrintWriter msgWriter, StringWriter stringWriter)
    {
        ArrayList endpointNameList = new ArrayList(theList);

        // Check the list, remove the entry with empty name value
        Iterator endpointNameIter = theList.iterator();
        while (endpointNameIter.hasNext())
        {
            Endpoint endpoint = (Endpoint) endpointNameIter.next();
            if (endpoint != null)
            {
                String endpointName = endpoint.getName();
                if ((endpointName != null) &&
                                   (endpointName.trim().compareTo("")==0))
                {
                    endpointNameList.remove(endpoint);
                }
            }
        }

        try
        {
            if (endpointNameList.size() ==0)
            {
                TabularData tbData = this.getJBIAdminCommands().getNMRStats(target);

                for (Iterator dataIter=tbData.values().iterator(); dataIter.hasNext(); )
                {
                    CompositeData compData = (CompositeData) dataIter.next();
                    String[] listActiveEndpoints =
                                      (String []) compData.get(
                                      JBIStatisticsItemNames.NMR_STATS_ACTIVE_ENDPOINTS);

                    for(int i=0; i < listActiveEndpoints.length; ++i)
                    {
                        Endpoint endpoint = new Endpoint();
                        endpoint.setName((String) listActiveEndpoints[i]);
                        endpointNameList.add(endpoint);
                    }
                }
            }

            // Start to print ...
            String header = getI18NBundle().getMessage(
                            "jbi.ui.ant.print.jbi.stats.endpoint.info.header",
                                                       new String[] { target } );
            String headerSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.header.separator");
            String pageSeparator = getI18NBundle().getMessage(
                                     "jbi.ui.ant.print.jbi.stats.info.separator");

            String emptyResult = getI18NBundle().getMessage(
                                      "jbi.ui.ant.print.jbi.stats.endpoint.info.empty");

            msgWriter.println(headerSeparator);
            msgWriter.println(header);
            msgWriter.println(headerSeparator);

            int results = 0;

            Iterator it = endpointNameList.iterator();
            while (it.hasNext())
            {
                String endpointName = ((Endpoint) it.next()).getName();
                logDebug("Statistics for endpoint, the endpoint is " + endpointName);
                logDebug("Statistics for endpoint, the target is " + target);
                TabularData result =
                   (TabularData) this.getJBIAdminCommands().getEndpointStats(endpointName,
                                                                                    target);
                if (result != null)
                {
                    printStatsForEndpoint(endpointName, result, target,
                                         msgWriter, pageSeparator, false);
                    stringWriter.flush();
                    ++results;
                }
            }

            if (results == 0)
            {
                msgWriter.println(emptyResult);
                msgWriter.println(pageSeparator);
            }
      
        }
        catch (Exception ex)
        {
            processTaskException(ex);
        }
    }

    /**
     * Query the boolean value of runtime configuration "msgSvcTimingStatisticsEnabled"
     * @param target
     * @return boolean which indicates this functionality is enabled or not
     */
    private boolean getMsgSvcTimingStatisticsEnabledFlag(String target)
        throws BuildException
    {
        try
        {
            Properties returnProps =
                this.getJBIAdminCommands().getRuntimeConfiguration(target);
            if (returnProps == null)
            {
                return false;
            }
            else
            {
                String theBooleanPropStr =
                    returnProps.getProperty("msgSvcTimingStatisticsEnabled");
                if (theBooleanPropStr == null)
                {
                    return false;
                }
                else
                {
                    return Boolean.valueOf(theBooleanPropStr); 
                }
            }
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }

        return false;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }

    /**
     * Ant helper class
     */ 
    public class Component
    {
        protected String mName;

        public Component()
        {
            mName = "";
        }

        public String getName()
        {
            return mName;
        }

        public void setName(String theName)
        {
            mName = theName;
        }
    }

    /**
     * Ant helper class
     */ 
    public class ServiceAssembly
    {
        protected String mName;

        public ServiceAssembly()
        {
            mName = "";
        }

        public String getName()
        {
            return mName;
        }

        public void setName(String theName)
        {
            mName = theName;
        }
    }

    /**
     * Ant helper class
     */ 
    public class Endpoint
    { 
        protected String mName;

        public Endpoint()
        {
            mName = "";
        }

        public String getName()
        {
            return mName;
        }

        public void setName(String theName)
        {
            mName = theName;
        }
    }
}
