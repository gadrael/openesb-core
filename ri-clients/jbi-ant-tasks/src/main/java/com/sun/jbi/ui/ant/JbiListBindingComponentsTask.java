/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiListBindingComponentsTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIComponentInfo;
import java.util.ArrayList;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for displaying binding components information.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiListBindingComponentsTask extends JbiQueryTask.JbiComponentQueryTask
{
    
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY =
        "jbi.ui.ant.list.bindings.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.list.bindings.failed";
    
    /**
     * Holds value of property binding component name.
     */
    private String mBindingComponentName;
    
    /**
     * Holds value of property descriptor (flag) which indicates whether showing descriptor or nor.
     */
    private boolean mDescriptor;

    /**
     * contructor
     */
    public JbiListBindingComponentsTask()
    {
        super();
    }
    
    /**
     * Getter for property binding component name.
     * @return Value of property binding component name.
     */
    public String getBindingComponentName()
    {
        return this.mBindingComponentName;
    }
    
    /**
     * Setter for property binding component name.
     * @param bindingComponentName name of the binding component
     */
    public void setBindingComponentName(String bindingComponentName)
    {
        this.mBindingComponentName = bindingComponentName;
    }
    
    /**
     * Getter for property descriptor (flag) which indicates showing descriptor or not.
     * @return Value of property descriptor.
     */
    public boolean getDescriptor()
    {
        return this.mDescriptor;
    }

    /**
     * Setter for property descriptor  (flag) which indicates showing descriptor or not..
     * @param descriptor New value of property descriptor.
     */
    public void setDescriptor(boolean descriptor)
    {
        this.mDescriptor = descriptor;
    }

    /** executes the list binding component task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        // set a empty component info list if xml output set
        initXmlOutput(JBIComponentInfo.writeAsXmlText(new ArrayList())); 
        
        validateTaskPropertyForStateValue(getState());
        String target = getValidTarget();
        
        try
        {
            String result;
            String descriptor = null;
            
            String name = this.getBindingComponentName();
            
            if ( null != name && name.length() > 0 )
            {
                // if binding component name present, execute show to display single object info.
                result = this.getJBIAdminCommands().showBindingComponent(
                    name,
                    toJbiComponentInfoState(getState()),
                    getSharedLibraryName(),
                    getServiceAssemblyName(),
                    target
                    );
                this.setSingleQueryResultType();

                if (mDescriptor == true)
                {
                    descriptor = this.getJBIAdminCommands().getComponentInstallationDescriptor(name);
                    if (descriptor == null)
                    {
                        String errMsg =  createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.show.component.descriptor.not.found",
                                       new String [] { name } );
                        throw new Exception(errMsg);
                    }
                }
            }
            else
            {
                if (mDescriptor == true)
                {
                    String errMsg =  createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.show.component.descriptor.single.only",
                                       null );
                    throw new Exception(errMsg);
                }

                result = this.getJBIAdminCommands().listBindingComponents(
                    toJbiComponentInfoState(getState()),
                    getSharedLibraryName(),
                    getServiceAssemblyName(),
                    target
                    );
            }
            
            printComponentQueryResults(result);

            if (descriptor != null)
            {
                printMessage("");
                printMessage(getI18NBundle().getMessage("jbi.ui.ant.print.comp.descriptor"));
                printMessage(descriptor);
                printMessage("");
            }
        }
        catch (Exception ex )
        {
            // throwTaskBuildException(ex);
            processTaskException(ex);
        }
        
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getEmptyQueryResultI18NKey()
    {
        return "jbi.ui.ant.print.no.bindings";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderI18NKey()
    {
        return "jbi.ui.ant.print.bindings.header";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderSeparatorI18NKey()
    {
        return "jbi.ui.ant.print.comp.info.header.separator";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultPageSeparatorI18NKey()
    {
        return "jbi.ui.ant.print.comp.info.separator";
    }
    
    /**
     * return the i18n message for empty results
     * @return string
     */
    protected String getEmptyQueryResultI18NMessage()
    {
        if ( this.getQueryResultType() == this.SINGLE_QUERY_RESULT_TYPE )
        {
            return getI18NBundle().getMessage(
                "jbi.ui.ant.print.no.binding.with.name" ,
                this.getBindingComponentName());
        }
        else
        {
            return getI18NBundle().getMessage( getEmptyQueryResultI18NKey() );
        }
    }
    
}
