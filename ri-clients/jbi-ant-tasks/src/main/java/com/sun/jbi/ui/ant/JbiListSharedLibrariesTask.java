/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiListSharedLibrariesTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIComponentInfo;
import java.util.ArrayList;
import org.apache.tools.ant.BuildException;



/** This class is an ant task for displaying shared libraries information.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiListSharedLibrariesTask extends JbiQueryTask
{
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY =
        "jbi.ui.ant.list.shared.lib.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.list.shared.lib.failed";
    
    /**
     * Holds value of property sharedLibraryName.
     */
    private String mSharedLibraryName;
    
    /**
     * Holds value of property componentName.
     */
    private String mComponentName;
    
    /**
     * Holds value of property descriptor (flag) which indicates whether showing descriptor or nor.
     */
    private boolean mDescriptor;

    /**
     * Getter for property sharedLibraryName.
     * @return Value of property sharedLibraryName.
     */
    public String getSharedLibraryName()
    {
        return this.mSharedLibraryName;
    }
    
    /**
     * Setter for property sharedLibraryName.
     * @param sharedLibraryName New value of property sharedLibraryName.
     */
    public void setSharedLibraryName(String sharedLibraryName)
    {
        this.mSharedLibraryName = sharedLibraryName;
    }
    
    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    /**
     * Getter for property descriptor (flag) which indicates showing descriptor or not.
     * @return Value of property descriptor.
     */
    public boolean getDescriptor()
    {
        return this.mDescriptor;
    }

    /**
     * Setter for property descriptor  (flag) which indicates showing descriptor or not..
     * @param descriptor New value of property descriptor.
     */
    public void setDescriptor(boolean descriptor)
    {
        this.mDescriptor = descriptor;
    }

    /** executes the list shared libraries task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        // set a empty component info list if xml output set
        initXmlOutput(JBIComponentInfo.writeAsXmlText(new ArrayList())); 

        String descriptor = null;
        String target = getValidTarget();
        try
        {
            String result;
            String name = this.getSharedLibraryName();
            String compName = this.getComponentName();
            
            if ( null != name && name.length() > 0 )
            {
                // if shared library name present, execute show to display single object info.
                result = this.getJBIAdminCommands().showSharedLibrary(
                    name, compName, target);
                
                this.setSingleQueryResultType();

                if (mDescriptor == true)
                {
                    descriptor = this.getJBIAdminCommands().getSharedLibraryInstallationDescriptor(name);
                    if (descriptor == null)
                    {
                        String errMsg =  createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.show.shared.library.descriptor.not.found",
                                       new String [] { name } );
                        throw new Exception(errMsg);
                    }
                }
            }
            else
            {
                if (mDescriptor == true)
                {
                    String errMsg =  createFailedFormattedJbiAdminResult(
                                      "jbi.ui.ant.show.shared.library.descriptor.single.only",
                                       null );
                    throw new Exception(errMsg);
                }

                result = this.getJBIAdminCommands().listSharedLibraries(compName, target);
            }
            
            printComponentQueryResults(result);
            if (descriptor != null)
            {
                printMessage("");
                printMessage(getI18NBundle().getMessage("jbi.ui.ant.print.shared.lib.descriptor"));
                printMessage(descriptor);
                printMessage("");
            }
        }
        catch (Exception ex )
        {
            // throwTaskBuildException(ex);
            processTaskException(ex);
        }
        
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getEmptyQueryResultI18NKey()
    {
        return "jbi.ui.ant.print.no.slib";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderI18NKey()
    {
        return "jbi.ui.ant.print.slib.header";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultHeaderSeparatorI18NKey()
    {
        return "jbi.ui.ant.print.comp.info.header.separator";
    }
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected String getQueryResultPageSeparatorI18NKey()
    {
        return "jbi.ui.ant.print.comp.info.separator";
    }
    
    /**
     * message text for empty query results
     * @return message text
     */
    protected String getEmptyQueryResultI18NMessage()
    {
        if ( this.getQueryResultType() == this.SINGLE_QUERY_RESULT_TYPE )
        {
            return getI18NBundle().getMessage(
                "jbi.ui.ant.print.no.slib.with.name" ,
                this.getSharedLibraryName());
        }
        else
        {
            return getI18NBundle().getMessage( getEmptyQueryResultI18NKey() );
        }
    }
}
