<!--
#
# The contents of this file are subject to the terms 
# of the Common Development and Distribution License 
# (the License).  You may not use this file except in
# compliance with the License.
# 
# You can obtain a copy of the license at 
# https://glassfish.dev.java.net/public/CDDLv1.0.html or
# glassfish/bootstrap/legal/CDDLv1.0.txt.
# See the License for the specific language governing 
# permissions and limitations under the License.
# 
# When distributing Covered Code, include this CDDL 
# Header Notice in each file and include the License file 
# at glassfish/bootstrap/legal/CDDLv1.0.txt.  
# If applicable, add the following below the CDDL Header, 
# with the fields enclosed by brackets [] replaced by
# you own identifying information: 
# "Portions Copyrighted [year] [name of copyright owner]"
# 
# Copyright 2006 Sun Microsystems, Inc. All rights reserved.
#
-->
<!-- jbi/pe/rootLogging.jsf -->

<sun:page>

    <!beforeCreate 
    
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2"  bundle="com.sun.enterprise.tools.admingui.resources.Strings")
setResourceBundle(key="help"  bundle="com.sun.enterprise.tools.admingui.resources.Helplinks");
   
//------------------------------------------------------------------------------------
//  If not set, initialize the dropDown to domain (Happens when page is first displayed)
//------------------------------------------------------------------------------------
if (!$session{jbiRuntimeLoggerInstanceValue}){
    setSessionAttribute(key="jbiRuntimeLoggerInstanceValue", value="server" );
}

//------------------------------------------------------------------------------------
//  This will be true when the "Save" button is clicked.  When this happens, we need
//  to update the log levels for all the selected entries in the update listbox.
//------------------------------------------------------------------------------------
if ($attribute{hasResults}) {
    jbiSaveJBIRuntimeLogLevels (propertySheetParentId     = "$session{propertySheetId}"
                                propertySheetId           = "jbiSetJBIRuntimeLogLevelsPropertySheet"
                                propertySheetSectionIdTag = "jbiSetJBIRuntimeLogLevelPropertySheetSection"
                                propertyIdTag             = "jbiSetJBIRuntimeLogLevelProperty"
                                dropDownIdTag             = "jbiDropDown"
                                hiddenFieldIdTag          = "jbiHiddenField"
                                targetName                = "#{sessionScope.jbiRuntimeLoggerInstanceValue}"
                                instanceName              = "#{sessionScope.jbiRuntimeLoggerInstanceValue}"
                                isAlertNeeded => $session{isJbiAlertNeeded}
                                alertSummary  => $session{jbiAlertSummary}
                                alertDetails  => $session{jbiAlertDetails});
    
    jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}');
    setAttribute(key="hasResults" value="" );
}

if (!$session{loadDefaultLevel}) {
    setSessionAttribute(key="jbiDropDownDefaultLevel", value="");
}

if ($session{loadDefaultLevel}) {
    setSessionAttribute(key="jbiDropDownDefaultLevel", value="DEFAULT");
    setSessionAttribute(key="loadDefaultLevel", value="" );
}

    />

    <sun:html>
        <sun:head id="jbiRootLoggingHead" />
        <sun:body>
        
             <sun:form id="rootTabsForm">
             
#include treeBreadcrumbs.inc
#include "jbi/scripts/jbiscripts.js"
#include "jbi/pe/inc/rootTabs.inc"

                 <sun:hidden id="helpKey" value="$resource{help.jbi.pe.rootLogging}" />
             </sun:form>
             
#include "jbi/pe/inc/alertBox.inc"

             <sun:form id="loggersSelectionForm"> 

                 <sun:title id="title1"
                     style = "text-indent: 8pt"
                     title = "$resource{i18n.jbi.runtime.loggers.page.title}"
                     helpText="$resource{i18n.jbi.runtime.loggers.page.help}">
                     >    
                     <!facet pageButtonsTop>
                         <sun:panelGroup id="topButtons">
                             <sun:button id="saveButton" 
                                 text="$resource{i18n2.button.Save}" 
                                 disabled="#{LoggingBean.saveButtonDisabled}" >
                                 onClick="javascript: 
                                     if ( guiValidate('#{reqMsg}','#{reqInt}','#{reqPort}'))
                                        submitAndDisable(this, '$resource{i18n2.button.Processing}');
                                     return false; "
                                 >
                                 <!command
                                     setAttribute(key="hasResults" value="true");
                                     navigate(page="jbi/pe/rootLogging.jsf");
                                 />
                             </sun:button>
                         </sun:panelGroup>
                     </facet>
                 </sun:title>
                
                 "<br />
                 <sun:button id="loadDefaults" 
                     immediate="#{true}"
                     primary="#{false}" 
                     style="margin-left: 8pt"  
                     text="$resource{i18n2.button.LoadDefaults}" 
                     >
                     <!command
                         setSessionAttribute(key="loadDefaultLevel", value="true" );
                         navigate(page="jbi/pe/rootLogging.jsf");
                     />
                 </sun:button>
                            
                 <dynamicPropertySheet id="jbiDynamicProperySheet"
                     propertySheetId           = "jbiSetJBIRuntimeLogLevelsPropertySheet"
                     propertySheetSectionIdTag = "jbiSetJBIRuntimeLogLevelPropertySheetSection"
                     propertyIdTag             = "jbiSetJBIRuntimeLogLevelProperty"
                     staticTextIdTag           = "jbiStaticText"
                     dropDownIdTag             = "jbiDropDown"
                     dropDownDefaultLevel      = "#{sessionScope.jbiDropDownDefaultLevel}"
                     hiddenFieldIdTag          = "jbiHiddenField"
                     componentName             = "#{sessionScope.sharedShowName}"
                     targetName                = "#{sessionScope.jbiRuntimeLoggerInstanceValue}"
                     instanceName              = "#{sessionScope.jbiRuntimeLoggerInstanceValue}"
		     configFile                = "/com/sun/jbi/config/LoggerConfig.xml"                  
                     propertySheetAdaptorClass = "com.sun.jbi.jsf.util.JBIRuntimeLogLevelsPropertySheetAdaptor"
                     >
                     <!beforeCreate
                         getClientId(component="$this{component}" clientId=>$session{propertySheetId});
                     />
                 </dynamicPropertySheet>

                 <sun:title id="title2">    
                     <!facet pageButtonsBottom>
                         <sun:panelGroup id="bottomButtons">
                             <sun:button id="saveButton2" 
                                 text="$resource{i18n2.button.Save}" 
                                 disabled="#{LoggingBean.saveButtonDisabled}" >
                                 onClick="javascript: 
                                     if ( guiValidate('#{reqMsg}','#{reqInt}','#{reqPort}'))
                                        submitAndDisable(this, '$resource{i18n2.button.Processing}');
                                     return false; "
                                 >
                                 <!command
                                     setAttribute(key="hasResults" value="true");
                                     navigate(page="jbi/pe/rootLogging.jsf");
                                 />
                             </sun:button>
                         </sun:panelGroup>
                     </facet>
                 </sun:title>

             </sun:form>           
         </sun:body> 
     </sun:html>  

 </sun:page>


