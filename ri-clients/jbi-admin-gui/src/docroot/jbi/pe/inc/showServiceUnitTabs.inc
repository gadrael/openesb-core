<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/pe/inc/showServiceUnitTabs.inc -->

<sun:tabSet id="showServiceUnitTabs" immediate="$boolean{true}" selected="#{sessionScope.showServiceUnitTabs}">
     
    <sun:tab id="general" 
             immediate="$boolean{true}" 
             text="$resource{i18n.jbi.show.service.unit.GeneralTab}" >
        <!command
setSessionAttribute(key="showServiceUnitTabs" value="general");
redirect(page="showServiceUnit.jsf?saname=#{show_serviceAssemblyName}&compName=#{show_targetComponent}&compType=#{show_componentType}&qs=#{show_qs}&type=#{show_type}&name=#{show_serviceUnitName}");
        />       
    </sun:tab>
    
    <sun:tab id="descriptor" 
             immediate="$boolean{true}" 
             text="$resource{i18n.jbi.show.service.unit.DescriptorTab}" >
	<!command
setSessionAttribute(key="showServiceUnitTabs" value="descriptor");
redirect(page="showServiceUnitMetadata.jsf?saname=#{show_serviceAssemblyName}&compName=#{show_targetComponent}&compType=#{show_componentType}&qs=#{show_qs}&type=#{show_type}&name=#{show_serviceUnitName}");
        />
    </sun:tab>

    <sun:tab id="monitor" 
             immediate="$boolean{true}" 
             text="$resource{i18n.jbi.pe.show.MonitorTab}" >
	<!command
setSessionAttribute(key="showServiceUnitTabs" value="monitor");
redirect(page="monitorServiceUnit.jsf?saname=#{show_serviceAssemblyName}&compName=#{show_targetComponent}&compType=#{show_componentType}&qs=#{show_qs}&type=#{show_type}&name=#{show_serviceUnitName}");
        />
    </sun:tab>

</sun:tabSet>
