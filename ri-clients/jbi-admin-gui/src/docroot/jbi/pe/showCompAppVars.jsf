<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/pe/showCompAppVars.jsf -->

<sun:page>

    <!beforeCreate 
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks")

setSessionAttribute(key="jbiShowTabs", value="application")
setSessionAttribute(key="showCompAppTabs", value="variables")

setSessionAttribute(key="showCompAppVarsTitleSuffixText", value='#{"binding-component"==sessionScope.sharedRequestType ? "$resource{i18n.jbi.show.comp.app.vars.page.title.binding.suffix.text}"  : "$resource{i18n.jbi.show.comp.app.vars.page.title.engine.suffix.text}" }') 


if (!$session{sharedShowName}) {
    setSessionAttribute(key="sharedShowName" value="$requestParameter{name}")
}
if (!$session{sharedShowType}) {
    setSessionAttribute(key="sharedShowType" value="$requestParameter{type}")
}
if (!$session{jbiSelectedInstanceValue}){
    setSessionAttribute(key="jbiSelectedInstanceValue", value="server" );
}

setSessionAttribute(key="showNameLinkUrl", value="showCompAppVars.jsf");
setJBIComponentId(JBIComponentName="$requestParameter{name}",JBIComponentType="$requestParameter{type}");     
 

jbiGetCompVarsTableList(
  compName="#{sessionScope.sharedShowName}",
  compType="#{sessionScope.sharedShowType}",
  instanceName="#{sessionScope.jbiSelectedInstanceValue}"
  alertDetails=>$session{jbiAlertDetails}
  alertSummary=>$session{jbiAlertSummary}
  isAlertNeeded=>$session{isJbiAlertNeeded}
  isCreateOrEditAllowed=>$page{isJbiAppVarsCreateOrEditAllowed}
  TableList=>$attribute{compVarsTableList}
  showStartButton=>$session{showStartButton}
  showSaveButton=>$session{showSaveButton}
  installedOnZeroTargets=>$session{installedOnZeroTargets}
 );

 />

    <sun:html>
	 <sun:head id="showCompAppVarsHead" />

	 <sun:body>

	     <sun:form id="showCompAppVarsBreadcrumbsForm" > 
                <sun:hidden id="helpKey" value="$resource{help.jbi.pe.showCompAppVars}" />

#include treeBreadcrumbs.inc

             </sun:form>

            <sun:form id="tabsForm">
#include "jbi/pe/inc/showTabs.inc"
            </sun:form>

#include "jbi/pe/inc/alertBox.inc"

      <sun:form id="showCompAppVarsTableForm">

                <sun:title id="showCompAppVarsPageTitle"
                    title="#{sessionScope.sharedShowName} - #{sessionScope.showCompAppVarsTitleSuffixText}"
                    helpText="$resource{i18n.jbi.show.comp.app.vars.page.help.inline.text}"
                    >
                    <!-- Buttons  -->
                    <!facet pageButtonsTop>
                        <sun:panelGroup id="topButtons">
                            <sun:button id="saveButton" text="$resource{i18n2.button.Save}"
                                rendered="$session{showSaveButton}">
                                <!command
getUIComponent(clientId="showCompAppVarsTableForm:showCompAppVarsTable:compAppVarTableRowGroupId", component=>$attribute{compVarsTableRowGroup});
jbiSaveCompAppVarsChanges(
 compName="#{sessionScope.sharedShowName}"
 instanceName="#{sessionScope.jbiSelectedInstanceValue}"
 tableRowGroup="$attribute{compVarsTableRowGroup}"
 isAlertNeeded=>$session{isJbiAlertNeeded}
 alertSummary=>$session{jbiAlertSummary}
 alertDetails=>$session{jbiAlertDetails}
);
redirect(page="showCompAppVars.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}&tname=#{jbiSelectedInstanceValue}");
                                />
                            </sun:button>
                        </sun:panelGroup>
                    </facet>
                </sun:title>

#include "jbi/pe/inc/startComponent.inc"
"<br />
               
             <sun:table id="showCompAppVarsTable" 
                    deselectMultipleButton="$boolean{true}"
                    deselectMultipleButtonOnClick="setTimeout('changeOneTableButton()', 0)"
                    lite="#{false}" 
                    paginateButton="#{false}"
                    paginationControls="#{false}"
                    rendered="#{isJbiAppVarsCreateOrEditAllowed}"
                    selectMultipleButton="$boolean{true}"
                    selectMultipleButtonOnClick="setTimeout('changeOneTableButton()', 0)"
                    title="$resource{i18n.jbi.show.comp.app.vars.table.title}" 
                    >  
        <!afterCreate
            getClientId(component="$this{component}" clientId=>$page{tableId});
        />
                    
      <!facet actionsTop>
                    <sun:panelGroup id="topActionsGroup1">
                        <!afterCreate
                                getClientId(component="$this{component}" clientId=>$page{topActionGroup});
                            />
                        <sun:button 
                            disabled="#{false}" 
                            id="addCompAppVarButton" 
                            rendered="$pageSession{isJbiAppVarsCreateOrEditAllowed}"
                            text="$resource{i18n.jbi.button.AddCompAppVar}" 
                            >   
                            <!command
redirect(page="addCompAppVar.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}&tname=#{jbiSelectedInstanceValue}");
       
                            />                                 
                        </sun:button>
                        <sun:button 
                            id="button1" 
                            disabled="#{true}"
                            onClick="
                                if (! getConfirm(this, '$resource{i18n.jbi.show.comp.app.vars.delete.confirmation}')) 
                                    return false;
                                return submitAndDisable(this, '$resource{i18n2.button.Processing}');"  
                            rendered="$pageSession{isJbiAppVarsCreateOrEditAllowed}"
                            text="$resource{i18n.jbi.button.DeleteCompAppVar}" 
                            primary="#{false}"
                            >

                            <!command
getUIComponent(clientId="showCompAppVarsTableForm:showCompAppVarsTable:compAppVarTableRowGroupId", component=>$attribute{compVarsTableRowGroup});
jbiGetSelectedTableRowKeys(tableRowGroup="${compVarsTableRowGroup}" rowKeys=>$attribute{rowKeys});
jbiDeleteCompAppVarsTableRows(compName="#{sessionScope.sharedShowName}",instanceName="#{sessionScope.jbiSelectedInstanceValue}",tableRowGroup="${compVarsTableRowGroup}" rowKeys="${rowKeys}");
jbiCommitTableRowGroup(tableRowGroup="${compVarsTableRowGroup}");  
                            />                                
                        </sun:button>
                    </sun:panelGroup>
                    </facet>       
                  
                    <sun:tableRowGroup id="compAppVarTableRowGroupId"
                         aboveColumnHeader="#{true}"
                         data={"$attribute{compVarsTableList}"}
		         selected="#{componentConfig.value.selected}"
                         sourceVar="componentConfig" 
                         >
				         <!afterCreate
				           getClientId(component="$this{component}" clientId=>$page{tableRowGroupId3});
				         />
                        <sun:tableColumn 
                            id="selectColumn"
                            rowHeader="$boolean{false}" 
                            selectId="select" 
                            >
                            <sun:checkbox 
				selectedValue="$boolean{true}"
			        selected="#{componentConfig.value.selected}"
                                id="select" 
                                onClick="setTimeout('initAllRows(); changeOneTableButton()', 0)" 
                            />
                        </sun:tableColumn>                    

                        <sun:tableColumn 
                            headerText="$resource{i18n.jbi.show.comp.app.vars.PropertyName}" 
                            id="nameColumn"
                            rowHeader="$boolean{false}" 
                            sort="name" 
                            >
                            <sun:staticText 
                                columns="$int{30}" 
                                id="nameText"   
                                value="#{componentConfig.value.name}" 
                                />
                        </sun:tableColumn>

                        <sun:tableColumn 
                            headerText="$resource{i18n.jbi.show.comp.app.vars.PropertyType}" 
                            id="typeColumn"
                            rowHeader="$boolean{false}" 
                            sort="name" 
                            >
                            <sun:staticText 
                                columns="$int{30}" 
                                id="typeText"   
                                value="#{componentConfig.value.displayType}" 
                                />
	
                               <sun:dropDown id="compVarTypeDropDown"
                                   forgetValue = "#{false}"
                                   labels      = {"$resource{i18n.jbi.show.comp.app.vars.typeIsString}" "$resource{i18n.jbi.show.comp.app.vars.typeIsNumber}" "$resource{i18n.jbi.show.comp.app.vars.typeIsBoolean}" "$resource{i18n.jbi.show.comp.app.vars.typeIsPassword}"}
	                           rendered    = "#{false}"
                                   submitForm  = "#{true}"
                                   value       = "#{componentConfig.value.type}"
                                   values      = {"[STRING]" "[NUMBER]" "[BOOLEAN]" "[PASSWORD]"}
                               >
                               </sun:dropDown>
                        </sun:tableColumn>

                        <sun:tableColumn 
                             headerText="$resource{i18n.jbi.show.comp.app.vars.PropertyValue}" 
                             id="valueColumn"
                             rowHeader="$boolean{false}" 
                             sort="value"
                             >

                             <sun:dropDown id="valueDropDown"
                                 disabled    = "#{false}" 
                                 forgetValue = "#{false}"
                                 labels      = {"$resource{i18n.jbi.show.comp.app.vars.valueIsFalse}" "$resource{i18n.jbi.show.comp.app.vars.valueIsTrue}" }
                                 rendered="#{'[BOOLEAN]'==componentConfig.value.type ? true : false}"
                                 submitForm  = "#{false}"
                                 selected    = "#{componentConfig.value.value}"
                                 values      = {"FALSE" "TRUE" } 
                                 > <!-- note: labels are I18n, values are not -->
                             </sun:dropDown>

                            <sun:textField 
                                columns="$int{55}" 
                                id="numberValueTextField" 
                                rendered="#{'[NUMBER]'==componentConfig.value.type ? true : false}"
                                text="#{componentConfig.value.value}" 
                                />

                            <sun:passwordField 
                                columns="$int{55}" 
                                id="passwordValueTextField" 
                                password="#{componentConfig.value.value}" 
                                rendered="#{'[PASSWORD]'==componentConfig.value.type ? true : false}"
                                />

                            <sun:textField 
                                columns="$int{55}" 
                                id="stringValueTextField" 
                                rendered="#{'[STRING]'==componentConfig.value.type ? true : false}"
                                text="#{componentConfig.value.value}" 
                                />

                        </sun:tableColumn>

                    </sun:tableRowGroup>

                </sun:table>

             </sun:form>

	 </sun:body> 
     
#include "changeButtonsJS.inc"           

     </sun:html>  
 </sun:page>
