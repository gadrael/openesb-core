<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/cluster/rootMonitoring.jsf -->

<sun:page>

    <!beforeCreate 
    
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks");

setSessionAttribute(key="showRuntimeMonitoringSubtabs", value="framework")

if (!$session{jbiSelectedRuntimeInstanceValue}){
    setSessionAttribute(key="jbiSelectedRuntimeInstanceValue", value="server" );
}

//------------------------------------------------------------------------------------
//  Retrieve the list of instances that will be displayed in the dropdown
//------------------------------------------------------------------------------------
jbiGetRuntimeInstances(
  addDomainFlag="$boolean{true}"
  instancesList=>$page{instanceList}
);

jbiSetFrameworkStatsForInstance(instanceName="#{sessionScope.jbiSelectedRuntimeInstanceValue}")

    <!-- display an alert if the selected target is not running -->
jbiAlertIfTargetNotRunning(
 targetName="#{sessionScope.jbiSelectedRuntimeInstanceValue}"
 isAlertNeeded=>$session{isJbiAlertNeeded}
 alertSummary=>$session{jbiAlertSummary}
 alertDetails=>$session{jbiAlertDetails}
)

    />

    <sun:html>
	 <sun:head id="jbiRootMonitoringHead" />
	 <sun:body>
             <sun:form id="rootTabsForm">
                 
#include treeBreadcrumbs.inc

#include "jbi/cluster/inc/rootTabs.inc"
		<sun:hidden id="helpKey" value="$resource{help.jbi.cluster.rootMonitoring}" />
             </sun:form>
             
#include "jbi/cluster/inc/alertBox.inc"

     <sun:form id="jbiRootMonitoringForm"> 

                <sun:title 
                    id="jbiRootMonitoringPageTitle" 
                    title="$resource{i18n.jbi.root.monitoring.framework.page.title.text}"
                    helpText="$resource{i18n.jbi.root.monitoring.framework.page.help.inline.text}"
                    > 
                    
                "<br />
                <sun:image id="indentDropDown"
                    align  = "top"
                    height = "$int{10}"
                    url    = "/resource/images/dot.gif"
                    width  = "$int{8}"
                />
                <sun:dropDown id="FromInstance"
                    immediate  = "#{true}"
                    items      = "$pageSession{instanceList}"
                    label      = "$resource{i18n.jbi.root.monitoring.instance.dropdown.label}"
                    selected   = "#{sessionScope.jbiSelectedRuntimeInstanceValue}"
                    submitForm = "#{true}"
                    >
                    <!command
                        setAttribute(key="click" value="$this{component}");
                        setAttribute(key="fromValue" value="#{click.selected}")
                        setSessionAttribute(key="jbiSelectedRuntimeInstanceValue", value="$attribute{fromValue}" );
                        navigate(page="jbi/cluster/rootMonitoring.jsf");
                    />
                </sun:dropDown>
                "<br />

                </sun:title>

                <sun:propertySheet 
                    id="runtimeMonitoringFWStatsPS" 
                    jumpLinks="true" 
                    style="text-align:left;" 
                    binding="#{RuntimeStatsBean.frameworkPropertySheet}"> 
                </sun:propertySheet>           	      		 

	     </sun:form>           
	 </sun:body> 
     </sun:html>  

 </sun:page>


