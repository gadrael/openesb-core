<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/cluster/configureLoggers.jsf -->

<sun:page>

    <!beforeCreate
    
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks");

if (!$session{sharedShowName}) {
    //setSessionAttribute(key="sharedShowName" value="$requestParameter{name}")
    getRequestValue(key="name", value=>$attribute{name});
    setSessionAttribute(key="sharedShowName" value="$attribute{name}")
}
if (!$session{sharedShowType}) {
    //setSessionAttribute(key="sharedShowType" value="$requestParameter{type}")
    getRequestValue(key="type", value=>$attribute{type});
    setSessionAttribute(key="sharedShowType" value="$attribute{type}")
}

<!-- removed code that assumed target="server" always works (it doesn't) -->

    <!-- Retrieve instances list for view instance dropDown   -->
jbiGetInstances(
 type="#{sessionScope.sharedRequestType}"
 compOrSaName="#{sessionScope.sharedShowName}"
 prevSelectedInstanceIfAny="#{jbiSelectedInstanceValue}"
 instancesList=>$page{instancesList}
 firstInstance=>$session{jbiSelectedInstanceValue}
 isAlertNeeded=>$session{isJbiAlertNeeded}
 alertSummary=>$session{jbiAlertSummary}
 alertDetails=>$session{jbiAlertDetails}
 installedOnZeroTargets=>$session{installedOnZeroTargets}
);

<!-- set the dropDown to the first instance -->
if (!$attribute{fromVal}){
  setAttribute(key="fromVal" value="#{sessionScope.jbiSelectedInstanceValue}");
}


    <!-- This will be true when the "Save" button is clicked.  When this happens, we need -->
    <!-- to update the log levels for all the selected entries in the update listbox.     -->
if ($attribute{hasResults}) {
    jbiSaveInstanceLogLevels(propertySheetParentId="$session{propertySheetId}"
            propertySheetId="jbiSetLogLevelsPropertySheet"
            propertySheetSectionIdTag="jbiSetLogLevelPropertySheetSection"
            propertyIdTag="jbiSetLogLevelProperty"
            dropDownIdTag="jbiDropDown"
            hiddenFieldIdTag="jbiHiddenField"
            componentName="#{sessionScope.sharedShowName}"
	    targetName="#{sessionScope.jbiSelectedInstanceValue}"
	    instanceName="#{sessionScope.jbiSelectedInstanceValue}"
            isAlertNeeded=>$session{isJbiAlertNeeded}
            alertSummary=>$session{jbiAlertSummary}
            alertDetails=>$session{jbiAlertDetails});
    jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}')
    setAttribute(key="hasResults" value="" );
}

if (!$session{loadDefaultLevel}) {
    setSessionAttribute(key="jbiDropDownDefaultLevel", value="");
}

if ($session{loadDefaultLevel}) {
    setSessionAttribute(key="jbiDropDownDefaultLevel", value="DEFAULT");
    setSessionAttribute(key="loadDefaultLevel", value="" );
}

    <!-- display an alert if the selected target is not running -->
jbiAlertIfTargetNotRunning(
 targetName="#{sessionScope.jbiSelectedInstanceValue}"
 isAlertNeeded=>$session{isJbiAlertNeeded}
 alertSummary=>$session{jbiAlertSummary}
 alertDetails=>$session{jbiAlertDetails}
)

    />

    <sun:html>
        <sun:head id="jbiConfigureLoggersHead" >
            "<script language="JavaScript" src="../../js/adminjsf.js"></script>
        </sun:head>

        <sun:body onLoad="javascript: initLastSelected('loggersSelectionForm:ToInstances_list');" >
    
            <sun:form id="jbiConfigureLoggersBreadcrumbsForm">
                <sun:hidden id="helpKey" value="$resource{help.jbi.cluster.ConfigureLoggers}" />
#include treeBreadcrumbs.inc
            </sun:form>

            <sun:form id="tabsForm">
#include "jbi/cluster/inc/showTabs.inc"
            </sun:form>

#include "jbi/cluster/inc/alertBox.inc"

            <sun:form id="loggersSelectionForm">

                <!-- <sun:legend id="legend2" text="$resource{i18n2.required.field.legend}" style="padding-right: 8pt;"/> -->
                "<br />
                <sun:image id="indentDropDown"
                    align  = "top"
                    height = "$int{10}"
                    url    = "/resource/images/dot.gif"
                    width  = "$int{8}"
                />
                <sun:dropDown id="FromInstance"
                    immediate  = "#{true}"
                    items      = "$pageSession{instancesList}"
                    label      = "$resource{i18n.jbi.configure.component.page.from.instance.dropdown.label}"
		            selected   = "#{sessionScope.jbiSelectedInstanceValue}"
                    submitForm = "#{true}"
                    rendered   = "#{sessionScope.installedOnZeroTargets == 'false'}"
                    >
                    <!command
                        setAttribute(key="click" value="$this{component}");
                        setAttribute(key="fromVal" value="#{click.selected}")
                        setSessionAttribute(key="jbiSelectedInstanceValue", value="$attribute{fromVal}" );
                        setAttribute(key="dropDownChange" value="true")
                        navigate(page="jbi/cluster/configureLoggers.jsf");
                    />
                </sun:dropDown>
                <sun:title id="title1"
                    style = "text-indent: 8pt"
                    title = '#{sessionScope.sharedShowName} - #{"binding-component"==sessionScope.sharedRequestType ? "$resource{i18n.jbi.configure.loggers.page.title.binding.suffix.text}"  : "$resource{i18n.jbi.configure.loggers.page.title.engine.suffix.text}" }'
                    helpText="$resource{i18n.jbi.configure.loggers.page.help}">
                    >    
                    <!facet pageButtonsTop>
                        <sun:panelGroup id="topButtons">
                            <sun:button id="saveButton" text="$resource{i18n2.button.Save}"
                                        rendered="#{sessionScope.installedOnZeroTargets == 'false'}" 
                                        disabled="#{LoggingBean.saveButtonDisabled}" >
                                <!command
                                    setAttribute(key="hasResults" value="true" );
                                    navigate(page="jbi/cluster/configureLoggers.jsf");
                                />
                            </sun:button>
                        </sun:panelGroup>
                    </facet>
                </sun:title>
<!--
                <sun:helpInline id="ConfigureLoggersHelpInline"
                    style = "text-indent: 8pt"
                    text  = '#{"binding-component"==sessionScope.sharedRequestType ? "$resource{i18n.jbi.configure.loggers.page.bc.help.inline.text}"  :  "$resource{i18n.jbi.configure.loggers.page.se.help.inline.text}" }' 
                    type  = "page"
                />
-->
                <sun:button id="loadDefaults" style="margin-left: 8pt"  primary="#{false}" text="$resource{i18n2.button.LoadDefaults}" disabled="#{LoggingBean.saveButtonDisabled}" >
                    <!command
                        setSessionAttribute(key="loadDefaultLevel", value="true" );
                        navigate(page="jbi/cluster/configureLoggers.jsf");
                    />
                </sun:button>

                <dynamicPropertySheet id="jbiDynamicProperySheet"
                    propertySheetId           = "jbiSetLogLevelsPropertySheet"
                    propertySheetSectionIdTag = "jbiSetLogLevelPropertySheetSection"
                    propertyIdTag             = "jbiSetLogLevelProperty"
                    staticTextIdTag           = "jbiStaticText"
                    dropDownIdTag             = "jbiDropDown"
                    dropDownDefaultLevel      = "#{sessionScope.jbiDropDownDefaultLevel}"
                    hiddenFieldIdTag          = "jbiHiddenField"
                    componentName             = "#{sessionScope.sharedShowName}"
                    targetName                = "#{sessionScope.jbiSelectedInstanceValue}"
                    instanceName              = "#{sessionScope.jbiSelectedInstanceValue}"
		    configFile                = "/com/sun/jbi/config/LoggerConfig.xml"                  
                    propertySheetAdaptorClass = "com.sun.jbi.jsf.util.JBILogLevelsPropertySheetAdaptor"
                    >
                    <!beforeCreate
getClientId(component="$this{component}" clientId=>$session{propertySheetId});
                    />
                </dynamicPropertySheet>

                <sun:title id="title2">    
                    <!facet pageButtonsBottom>
                        <sun:panelGroup id="bottomButtons">
                            <sun:button id="saveButton2" text="$resource{i18n2.button.Save}" 
                                        rendered="#{sessionScope.installedOnZeroTargets == 'false'}"
                                        disabled="#{LoggingBean.saveButtonDisabled}" >
                                <!command
                                    setAttribute(key="hasResults" value="true" );
                                    navigate(page="jbi/cluster/configureLoggers.jsf");
                                />
                            </sun:button>
                        </sun:panelGroup>
                    </facet>
                </sun:title>

            </sun:form>

        </sun:body>
     
#include "changeButtonsJS.inc"

    </sun:html>
</sun:page>

