#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
  build_test_application_artifacts
  #test_cleanup
  
  #--------------------------------------------------------------------------------
  # Make sure cli-config-binding componet is installed and started
  #--------------------------------------------------------------------------------
  install_component server ${JV_SVC_BLD}/regress/dist/cli-config-binding.jar
  start_component server cli-config-binding
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Configurations for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-configurations --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-configurations --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Application Configuration defaultConfig for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-application-configuration --component=cli-config-binding defaultConfig"
  $AS8BASE/bin/asadmin show-jbi-application-configuration --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS defaultConfig
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Create another Application Configuration for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-configuration --component=cli-config-binding --configname=testConfig testConfigData.properties"
  $AS8BASE/bin/asadmin create-jbi-application-configuration --component=cli-config-binding --configname=testConfig --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS testConfigData.properties

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Application Configuration testConfig for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-application-configuration --component=cli-config-binding testConfig"
  $AS8BASE/bin/asadmin show-jbi-application-configuration --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS testConfig

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Update the Application Configuration defaultConfig"
  echo "-------------------------------------------------------------------"
  echo "update-jbi-application-configuration --component=cli-config-binding --configname=defaultConfig username=steve"
  $AS8BASE/bin/asadmin update-jbi-application-configuration --component=cli-config-binding --configname=defaultConfig --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS username=steve

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Update the Application Configuration testConfig"
  echo "-------------------------------------------------------------------"
  echo "update-jbi-application-configuration --component=cli-config-binding --configname=testConfig username=steve"
  $AS8BASE/bin/asadmin update-jbi-application-configuration --component=cli-config-binding --configname=testConfig --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS username=bob
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Configurations for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-configurations --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-configurations --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Delete the Application Configuration testConfig"
  echo "-------------------------------------------------------------------"
  echo "delete-jbi-application-configuration --component=cli-config-binding testConfig"
  $AS8BASE/bin/asadmin delete-jbi-application-configuration --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS testConfig
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Configurations for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-configurations --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-configurations --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
         
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00011"
TEST_DESCRIPTION="Test Application Configuration Commands"
. ./regress_defs.ksh
run_test

exit 0


