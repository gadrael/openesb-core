
#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00023.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
 
  # Clean up the (Remove uninstall everything so this regression test will work
  #test_cleanup
   
  #--------------------------------------------------------------------------------
  # Install the shared library with any output going to the TEMP directory
  #--------------------------------------------------------------------------------
 # install_shared_library server $UI_REGRESS_DIST_DIR/cli-test-sns1.jar

  #--------------------------------------------------------------------------------
  # Install the binding component with any output going to the TEMP directory
  #--------------------------------------------------------------------------------
 # install_component server $UI_REGRESS_DIST_DIR/cli-test-binding1.jar

  #--------------------------------------------------------------------------------
  # Make sure the component is shut down, output going to the TEMP directory
  #--------------------------------------------------------------------------------
 # shut_down_component server cli_test_binding1

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library cli-test-sns1.jar"
  $AS8BASE/bin/asadmin install-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-sns1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install binding component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-binding1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-binding1.jar
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1

  echo ""
  echo "-------------------------------------------------------------------"
  echo "Removing bootstrap jar file from install_root"
  echo "-------------------------------------------------------------------"
  echo "rm $JV_JBI_DOMAIN_ROOT/jbi/components/cli_test_binding1/install_root/cli-test-binding1-boot.jar"
  rm $JV_JBI_DOMAIN_ROOT/jbi/components/cli_test_binding1/install_root/cli-test-binding1-boot.jar

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the binding component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component  cli_test_binding1"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the binding component cli_test_binding1 with force"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component --force cli_test_binding1"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force=true cli_test_binding1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-shared-library cli_test_sns1"
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1 

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the binding component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-shared-library cli_test_sns1"
  $AS8BASE/bin/asadmin show-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1
  
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00023"
TEST_DESCRIPTION="Tests uninstall force with corrupted install_root"
. ./regress_defs.ksh
run_test

exit 0
