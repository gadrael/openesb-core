#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
 
  # Clean up the (Remove uninstall everything so this regression test will work
  test_cleanup
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library cli-test-sns1.jar"
  $AS8BASE/bin/asadmin install-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-sns1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install shared library cli_test_sns2"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library cli-test-sns2.jar"
  $AS8BASE/bin/asadmin install-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-sns2.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install binding component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-binding1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-binding1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install binding component cli_test_binding2"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-binding2.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-binding2.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install service engine cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-engine1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-engine1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install service engine cli_test_engine2"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-engine2.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-engine2.jar
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the binding component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the binding component cli_test_binding2"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_binding2"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding2
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the binding component cli_test_binding2"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_binding2"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding2
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the service engine cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the service engine cli_test_engine2"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component cli_test_engine2"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine2

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Deploy Service Assembly: cli_test_assembly_unit_1"
  echo "-------------------------------------------------------------------"
  echo "deploy-jbi-service-assembly cli-test-au1.zip"
  $AS8BASE/bin/asadmin deploy-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-au1.zip 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Engines Filtering on the lifecyclestate"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-engines --lifecyclestate=started | grep cli_test_engine1"
  $AS8BASE/bin/asadmin list-jbi-service-engines --lifecyclestate=started --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_engine1
  echo ""
  echo "list-jbi-service-engines --lifecyclestate=shutdown | grep cli_test_engine2"
  $AS8BASE/bin/asadmin list-jbi-service-engines --lifecyclestate=shutdown --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_engine2
  echo ""
  echo "list-jbi-service-engines --lifecyclestate=stopped | grep cli_test_engine1"
  $AS8BASE/bin/asadmin list-jbi-service-engines --lifecyclestate=stopped --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_engine1
  echo ""
  echo "list-jbi-service-engines --lifecyclestate=stopped | grep cli_test_engine2"
  $AS8BASE/bin/asadmin list-jbi-service-engines --lifecyclestate=stopped --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_engine2
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Binding Components Filtering on the lifecyclestate"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-binding-components --lifecyclestate=started | grep cli_test_binding1"
  $AS8BASE/bin/asadmin list-jbi-binding-components --lifecyclestate=started --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_binding1
  echo ""
  echo "list-jbi-binding-components --lifecyclestate=stopped | grep cli_test_binding2"
  $AS8BASE/bin/asadmin list-jbi-binding-components --lifecyclestate=stopped --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_binding2
  echo ""
  echo "list-jbi-binding-components --lifecyclestate=shutdown | grep cli_test_binding1"
  $AS8BASE/bin/asadmin list-jbi-binding-components --lifecyclestate=shutdown --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_binding1
  echo ""
  echo "list-jbi-binding-components --lifecyclestate=shutdown | grep cli_test_binding2"
  $AS8BASE/bin/asadmin list-jbi-binding-components --lifecyclestate=shutdown --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_binding2
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Assemblies Filtering on the lifecyclestate"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-assemblies --lifecyclestate=started | grep cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --lifecyclestate=started --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_assembly_unit_1
  echo ""
  echo "list-jbi-service-assemblies --lifecyclestate=stopped | grep cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --lifecyclestate=stopped --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_assembly_unit_1
  echo ""
  echo "list-jbi-service-assemblies --lifecyclestate=shutdown | grep cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --lifecyclestate=shutdown --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  | grep cli_test_assembly_unit_1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Shared Libraries Filtering on the componentname cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-shared-libraries --componentname=cli_test_binding1"
  $AS8BASE/bin/asadmin list-jbi-shared-libraries --componentname=cli_test_binding1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Shared Libraries Filtering on the componentname cli_test_binding2"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-shared-libraries --componentname=cli_test_binding2"
  $AS8BASE/bin/asadmin list-jbi-shared-libraries --componentname=cli_test_binding2 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Assemblies Filtering on the componentname cli_test_binding2"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-assemblies --componentname=cli_test_binding2"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --componentname=cli_test_binding2 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS

  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Engines Filtering on the libraryname"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-engines --libraryname=cli_test_sns1"
  $AS8BASE/bin/asadmin list-jbi-service-engines --libraryname=cli_test_sns1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Binding Components Filtering on the libraryname"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-binding-components --libraryname=cli_test_sns1"
  $AS8BASE/bin/asadmin list-jbi-binding-components --libraryname=cli_test_sns1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Engines Filtering on the assemblyname"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-engines --assemblyname=cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-engines --assemblyname=cli_test_assembly_unit_1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Binding Components Filtering on the assemblyname"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-binding-components --assemblyname=cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-binding-components --assemblyname=cli_test_assembly_unit_1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Undeploy Service Assembly cli_test_assembly_unit_1"
  echo "-------------------------------------------------------------------"
  echo "undeploy-jbi-service-assembly cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the Components"
  echo "-------------------------------------------------------------------"
  echo "stop-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "stop-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the Components"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "shut-down-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  echo "shut-down-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding2
  echo "shut-down-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine2
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the Components"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "uninstall-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  echo "uninstall-jbi-component cli_test_binding2"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding2
  echo "uninstall-jbi-component cli_test_engine2"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine2
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the Shared Libraries"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-shared-library cli_test_sns1"
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1
  echo "uninstall-jbi-shared-library cli_test_sns1"
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns2
  
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00003"
TEST_DESCRIPTION="Test Lyfecycle and List Commands"
. ./regress_defs.ksh
run_test

exit 0


