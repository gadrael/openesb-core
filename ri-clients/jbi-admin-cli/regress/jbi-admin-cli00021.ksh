#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
  #test_cleanup
        
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Upgrade Test - Bogus name for upgrade file"
  echo "-------------------------------------------------------------------"
  echo "upgrade-jbi-component --upgradefile=fred sun-http-binding"
  $AS8BASE/bin/asadmin upgrade-jbi-component --upgradefile=fred --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Upgrade Test - Archive file does not match specified component name"
  echo "-------------------------------------------------------------------"
  echo "upgrade-jbi-component --upgradefile=cli-test-binding1.jar sun-http-binding"
  $AS8BASE/bin/asadmin upgrade-jbi-component --upgradefile=$UI_REGRESS_DIST_DIR/cli-test-binding1.jar --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Runtime Logger Test - Specify an invalid logger type"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger com.sun.jbi.management=fred"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS com.sun.jbi.management=fred
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Runtime Logger Test - Specify an invalid logger name"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger com.sun.jbi.management.fred=WARNING"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS com.sun.jbi.management.fred=FINE

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Runtime Logger Test - Specify a property file the does not exist"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger fred.property"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/tmp/fred.property
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Positive Runtime Logger Test"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger positiveRuntimeLogger1.property"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/testdata/property-files/positiveRuntimeLogger1.property
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Runtime Logger Test - Property file with invaled logger level"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger negitiveRuntimeLogger1.property"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/testdata/property-files/negitiveRuntimeLogger1.property
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Runtime Logger Test - Property file with invalid logger name"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger negitiveRuntimeLogger2.property"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/testdata/property-files/negitiveRuntimeLogger2.property
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Set Runtime Configuration - Invalid configuration name"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration fred=6000"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred=6000
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Set Runtime Configuration - Invalid configuration value"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration heartBeatInterval=fred"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS heartBeatInterval=fred
            
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00021"
TEST_DESCRIPTION="Negative Runtime logger and configuration tests"
. ./regress_defs.ksh
run_test

exit 0


