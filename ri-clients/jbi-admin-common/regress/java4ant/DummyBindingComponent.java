/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import javax.management.StandardMBean;
import javax.management.ObjectName;

/**
 * Dummy binding component used to test deployment.
 *
 * @author Sun Microsystems, Inc.
 */
public class DummyBindingComponent 
        extends BindingComponent
{ 
    /**
     * Register custom MBeans
     */
    protected void registerCustomMBeans()
        throws javax.jbi.JBIException
    {
        try
        {
           
            StandardMBean compMBean;

            DummyConfigurationMBean mbean = new DummyConfiguration();
            compMBean = new StandardMBean(mbean, 
                    DummyConfigurationMBean.class);
            

            ObjectName compMBeanName = 
                mContext.getMBeanNames().createCustomComponentMBeanName("Configuration");

            mContext.getMBeanServer().registerMBean(compMBean, compMBeanName);
        }
        catch ( Exception exp )
        {
            throw new javax.jbi.JBIException(exp.getMessage());
        }
    }
    
    /**
     * Unregister custom MBeans
     */
    protected void unregisterCustomMBeans()
        throws javax.jbi.JBIException
    {
        try
        {
            ObjectName compMBeanName = 
                mContext.getMBeanNames().createCustomComponentMBeanName("Configuration");

            mContext.getMBeanServer().unregisterMBean(compMBeanName);
        }
        catch ( Exception exp )
        {
            throw new javax.jbi.JBIException(exp.getMessage());
        }
    }
}
