/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.management.openmbean.*;
import javax.management.remote.*;
import javax.management.*;

/**
 */
public class TestComponentConfiguration
{
     
    private MBeanServerConnection mbns;
    private static final String RESULT_PREFIX = "##### Result of ";
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String COMPONENT_NAME = "component.name";
    
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    public ObjectName getComponentExtensionName(String target)
        throws Exception
    {
        String info = "com.sun.jbi:Target=" + target + ",ComponentName=" +
                      System.getProperty(COMPONENT_NAME) + ",ServiceType=Extension";
        return new ObjectName(info);
    }
    
    public ObjectName getJbiAdminCommandsUI()
        throws Exception
    {
        String admin = "com.sun.jbi:" +
            "ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return new ObjectName(admin);
    }
    
    public void getComponentConfigurationFacadeMBeanName(String target)
        throws Exception
    {
        ObjectName cfgMBean = (ObjectName) mbns.invoke(getComponentExtensionName(target), "getComponentConfigurationFacadeMBeanName", 
            new Object[] { target }, new String[] { "java.lang.String" } );
        
        System.out.println(RESULT_PREFIX + " getComponentConfigurationFacadeMBeanName = " + cfgMBean);
    }

    public void getComponentConfiguration(boolean expectingException, String target)
        throws Exception
    {
    
        Object[] params = new Object[2];
        params[0] = System.getProperty(COMPONENT_NAME);
        params[1] = target;
    
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        Properties props = null;
    
        try
        {
            props = (Properties) mbns.invoke(getJbiAdminCommandsUI(),
                                     "getComponentConfiguration", params, signature);
        
            System.out.println(RESULT_PREFIX + " getComponentConfiguration(" +
                               target + "):" + props);
            if (expectingException)
            {
                System.out.println("*** Was expecting exception, but didn't get one! ***");
            }
        }
        catch (Exception e)
        {
            if (expectingException)
            {
                System.out.println(RESULT_PREFIX + " getComponentConfiguration(" +
                                   target + "): caught expected exception.");
            }
            else
            {
                System.out.println(RESULT_PREFIX + " getComponentConfiguration(" +
                                   target + "): Exception: " + e.getMessage());
            }
        }
    }

    public void setComponentConfiguration(boolean expectingException, String target, Properties props)
        throws Exception
    {
    
        Object[] params = new Object[3];
        params[0] = System.getProperty(COMPONENT_NAME);
        params[1] = target;
        params[2] = props;
    
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.util.Properties";
        
        try
        {
            String resultString = null;
    
            resultString = (String) mbns.invoke(getJbiAdminCommandsUI(),
                                     "setComponentConfiguration", params, signature);
            resultString = resultString.replace("en_US", "en");
        
            System.out.println(RESULT_PREFIX + " setComponentConfiguration(" +
                               target + "):" + resultString);
            if (expectingException)
            {
                System.out.println("*** Was expecting exception, but didn't get one! ***");
            }
        }
        catch (Exception e)
        {
            if (expectingException)
            {
                System.out.println(RESULT_PREFIX + " setComponentConfiguration(" +
                                   target + "): caught expected exception.");
            }
            else
            {
                System.out.println(RESULT_PREFIX + " setComponentConfiguration(" +
                                   target + "): Exception: " + e.getMessage());
            }
        }
    }

    public static void main (String[] params)
        throws Exception 
    {
        TestComponentConfiguration test = new TestComponentConfiguration();
        boolean expectingDown = ("down".equals(params[0]));
        System.out.println("TestComponentConfiguration: expecting servers to be "
                           + params[0] + ".");
        
        test.initMBeanServerConnection();
        
        Properties props = new Properties();
        props.put("A", "foo");
        props.put("B", "bar");
        props.put("C", "baz");
        
        test.getComponentConfigurationFacadeMBeanName("server");

        test.getComponentConfiguration(false, "server");
        test.setComponentConfiguration(false, "server", props);
        test.getComponentConfiguration(false, "server");

        test.getComponentConfiguration(false, "ccfg-cluster");
        test.setComponentConfiguration(false, "ccfg-cluster", props);
        test.getComponentConfiguration(false, "ccfg-cluster");

        props.put("A", "clusterInstanceFoo");
        props.put("B", "clusterInstanceBar");
        props.put("C", "clusterInstanceBaz");
        
        test.getComponentConfiguration(expectingDown, "ccfg-cluster-instance-1");
        test.setComponentConfiguration(expectingDown, "ccfg-cluster-instance-1", props);
        test.getComponentConfiguration(expectingDown, "ccfg-cluster-instance-1");
    }
}
