/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJBIComponentInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJBIComponentInfo extends TestCase
{
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestJBIComponentInfo(String aTestName)
    {
        super(aTestName);
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testJBIComponentInfoLoadSave() throws Exception
    {
        List list = getTestData();
        
        String xmlText = JBIComponentInfo.writeAsXmlText(list);
        System.out.println("Serialized JBI Component Info ");
        System.out.println(xmlText);
        
        List testList = JBIComponentInfo.readFromXmlText(xmlText);
        System.out.println("DeSerialized JBI Component Info ");
        for ( Iterator itr = testList.iterator(); itr.hasNext();)
        {
            System.out.println(itr.next());
        }
        
        System.out.println("From File : JBI Component Info ");
        
        InputStream res = this.getClass().getResourceAsStream("test_component_info_list.xml");
        InputStreamReader xmlReader = new InputStreamReader(res);
        testList = JBIComponentInfo.readFromXmlText(xmlReader); 
        
        for ( Iterator itr = testList.iterator(); itr.hasNext();)
        {
            System.out.println(itr.next());
        }
        
    }
    
    /** test data
     * @return test list objects
     */
    public static List getTestData()
    {
        ArrayList list = new ArrayList();
        
        JBIComponentInfo info1 =
        new JBIComponentInfo("service-engine","started","comp1","comp1engine");
        
        JBIComponentInfo info2 =
        new JBIComponentInfo("binding-component","stopped","comp2","comp2binding");
        
        JBIComponentInfo info3 =
        new JBIComponentInfo("shared-library",
        "installed","comp3","comp3namespace");
        
        list.add(info1);
        list.add(info2);
        list.add(info3);
        
        return list;
    }
    
    
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestJBIComponentInfo("test").testJBIComponentInfoLoadSave();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
