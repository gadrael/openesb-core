/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestClusterMgmtMessage.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestClusterMgmtMessage extends TestCase
{
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestClusterMgmtMessage(String aTestName )
    {
        super(aTestName);
    }
    
    private void printJbiMgmtMessage(String title, String msgXml) {
        if ( title == null ) {
            title = "########## Formatted JBI Results ###########";
        }
        System.out.println(title + "[");
        
        JBIManagementMessage msg =
        JBIManagementMessage.createJBIManagementMessage(msgXml);
        System.out.println(msg.getMessage());
                
        System.out.println("]");

//        System.out.println("******* DEBUG Message **********");
//        System.out.println(msg.getDebugMessage());
//        
//        System.out.println("******* StackTrace **********");
//        msg.printStackTrace(System.out);
    }
    
    private void printESBResults(String title, String msgXml) {
        if ( title == null ) {
            title = "########## Formatted ESB Results ###########";
        }
        System.out.println(title + "[");
        ESBResultFormatter formatter = new ESBResultFormatter();
        String msg = formatter.getFormattedESBResult(msgXml);
        System.out.println(msg);
        System.out.println("]");
        
    }
    
////    /**
////     * test sucess msg
////     * @throws Exception on error
////     */
////    public void testJbiResultNoMessages() throws Exception
////    {
////        String xmlText = this.readMsgFromResource("jbi-result-no-messages.xml");
////        printJbiMgmtMessage("####### JBI Result - NO Messages", xmlText);
////    }
////
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultAllStatusMessages() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-all-status-msgs.xml");
        printESBResults("####### ESB Result - All Status Messages", xmlText);
    }

    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultAllExpMessages() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-all-ex-msgs.xml");
        printESBResults("####### ESB Result - All Exception Messages", xmlText);
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultAllMessages() throws Exception
    {
        String esbResultXml = this.readMsgFromResource("jbi-result-cluster-all-messages.xml");
        printESBResults("####### ESB Result - All Messages", esbResultXml);
//        String jbiResultXml = this.readMsgFromResource("jbi-result-all-messages.xml");
//        printJbiMgmtMessage(null, jbiResultXml);
    }
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultFwMsgOnly() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-fw-msg.xml");
         printESBResults("####### ESB Result - Framework Message only", xmlText);
    }    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultFwExpOnly() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-fw-exp.xml");
         printESBResults("####### ESB Result - Framework Exception only", xmlText);
    }    
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultFwMsgAndExp() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-fw-msg-and-exp.xml");
         printESBResults("####### ESB Result - Framework Message and Exception", xmlText);
    }    
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultCompMsgOnly() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-comp-msg.xml");
        printESBResults("####### ESB Result - Component Message only", xmlText);
    }    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultCompExpOnly() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-comp-exp.xml");
        printESBResults("####### ESB Result - Component Exception only", xmlText);
    }    
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultCompMsgAndExp() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-comp-msg-and-exp.xml");
        printJbiMgmtMessage("####### ESB Result - Component Message and Exception", xmlText);
    }    
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testClusterResultBadXml() throws Exception
    {
        String xmlText = this.readMsgFromResource("jbi-result-cluster-bad-xml.xml");
        printJbiMgmtMessage("####### ESB Result - Bad xml", xmlText);
        
        JBIManagementMessage msg =
        JBIManagementMessage.createJBIManagementMessage(xmlText);
        System.out.println("Old mgmt msg [");
        System.out.println(msg.getMessageDeprecated("verbose"));
        System.out.println("]");
    }    
    
    
////    /**
////     * test sucess msg
////     * @throws Exception on error
////     */
////    public void testSuccessMsg() throws Exception
////    {
////        String xmlText = this.readMsgFromResource("TestMgmtSuccessMessage.xml");
////        
////        JBIManagementMessage msg =
////        JBIManagementMessage.createJBIManagementMessage(xmlText);
////        System.out.println();
////        PrintWriter writer = new PrintWriter(System.out, true);
////        writer.println("TestMgmtSuccessMessage.xml");
////        writer.println();
////        msg.printMessage(writer);
////        writer.println();
////        writer.println();
////        msg.printStackTrace(writer);
////        this.assertTrue("Expected Success Message",  msg.isSuccessMsg());
////    }
////    
////    /**
////     * test sucess msg
////     * @throws Exception on error
////     */
////    public void testSuccessMsg2() throws Exception
////    {
////        String xmlText = this.readMsgFromResource("TestMgmtSuccessMessage2.xml");
////        
////        JBIManagementMessage msg =
////        JBIManagementMessage.createJBIManagementMessage(xmlText);
////        System.out.println();
////        PrintWriter writer = new PrintWriter(System.out, true);
////        writer.println("TestMgmtSuccessMessage2.xml");
////        writer.println();
////        msg.printMessage(writer);
////        writer.println();
////        writer.println();
////        msg.printStackTrace(writer);
////        this.assertTrue("Expected Success Message",  msg.isSuccessMsg());
////    }
////    
////    
////    /**
////     * test failure msg
////     * @throws Exception on error
////     */
////    public void testFailureMsg() throws Exception
////    {
////        String xmlText = this.readMsgFromResource("TestMgmtFailureMessage.xml");
////        
////        JBIManagementMessage msg =
////        JBIManagementMessage.createJBIManagementMessage(xmlText);
////        System.out.println();
////        PrintWriter writer = new PrintWriter(System.out, true);
////        writer.println("TestMgmtFailureMessage.xml");
////        writer.println();
////        msg.printMessage(writer);
////        writer.println();
////        writer.println();
////        msg.printStackTrace(writer);
////        this.assertTrue("Expected Failed Message",  msg.isFailedMsg());
////        
////    }
////    
////    /**
////     * test exception msg
////     * @throws Exception on error
////     */
////    public void testExceptionMsg() throws Exception
////    {
////        String xmlText = this.readMsgFromResource("TestMgmtExceptionMessage.xml");
////        
////        JBIManagementMessage msg =
////        JBIManagementMessage.createJBIManagementMessage(xmlText);
////        System.out.println();
////        PrintWriter writer = new PrintWriter(System.out, true);
////        writer.println("TestMgmtExceptionMessage.xml");
////        writer.println();
////        msg.printMessage(writer);
////        writer.println();
////        writer.println();
////        msg.printStackTrace(writer);
////        this.assertTrue("Expected Failed Message",  msg.isFailedMsg());
////        
////    }
////    
////    /**
////     * test exception msg
////     * @throws Exception on error
////     */
////    public void testExceptionMsg2() throws Exception
////    {
////        String xmlText = this.readMsgFromResource("TestMgmtExceptionMessage2.xml");
////        
////        JBIManagementMessage msg =
////        JBIManagementMessage.createJBIManagementMessage(xmlText);
////        System.out.println();
////        PrintWriter writer = new PrintWriter(System.out, true);
////        writer.println("TestMgmtExceptionMessage2.xml");
////        writer.println();
////        msg.printMessage(writer);
////        writer.println();
////        writer.println();
////        msg.printStackTrace(writer);
////        this.assertTrue("Expected Failed Message",  msg.isFailedMsg());
////        
////    }
////    

    /**
     * reads the msg from resources
     * @return xml text
     * @param resName resource file name
     */
    private String readMsgFromResource(String resName )
    {
        try
        {
            InputStream res = this.getClass().getResourceAsStream(resName);
            StringWriter strWriter = new StringWriter();
            PrintWriter writer = new PrintWriter(strWriter);
            BufferedReader reader = new BufferedReader(new InputStreamReader(res));
            String ln = null;
            while ( (ln = reader.readLine()) != null )
            {
                writer.println(ln);
            }
            writer.close();
            reader.close();
            
            return strWriter.getBuffer().toString();
            
        } catch (Exception ex )
        {
            ex.printStackTrace();
            return null;
        }
    }
    
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
    }
    
}
