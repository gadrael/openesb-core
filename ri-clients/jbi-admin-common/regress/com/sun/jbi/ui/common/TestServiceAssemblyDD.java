/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceAssemblyDD.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestServiceAssemblyDD extends TestCase
{
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestServiceAssemblyDD(String aTestName)
    {
        super(aTestName);
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testServiceAssemblyDD() throws Exception
    {
        InputStream res = this.getClass().getResourceAsStream("test_jbi.xml");
        InputStreamReader reader = new InputStreamReader(res);
        ServiceAssemblyDD dd = (ServiceAssemblyDD)JBIDescriptor.createJBIDescriptor(reader);
        System.out.println(dd.toString());
        for ( Iterator itr = dd.getServiceUnitDDList().iterator(); itr.hasNext();)
        {
            System.out.println(itr.next());
        }
    }
    
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestServiceAssemblyDD("test").testServiceAssemblyDD();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
} 
