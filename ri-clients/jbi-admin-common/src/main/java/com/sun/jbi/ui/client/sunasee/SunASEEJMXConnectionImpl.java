/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SunASEEJMXConnectionImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client.sunasee;

import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.JMXConnectionException;
import com.sun.jbi.ui.common.ToolsLogManager;
import com.sun.jbi.ui.common.Util;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import com.sun.jbi.ui.client.JMXConnectionImpl;

import com.sun.jbi.ui.client.JMXConnectionProperties;
import java.util.logging.Level;

/** This class implements the JMXConnection interface for connecting to
 * jmx connector server remotely using jmx remote apis
 *
 * @author Sun Microsystems, Inc.
 */

public class SunASEEJMXConnectionImpl extends JMXConnectionImpl
{
    /** username */
    private String mUsername = null;
    /** password */
    private String mPassword = null;
    /** host */
    private String mHost = null;
    /** port */
    private String mPort = null;
    
    /**
     * jmx url
     */
    private String mUrl = null;
    
    /** jmx url */
    private JMXServiceURL mJmxUrl = null;
    /** jmx connector */
    private JMXConnector mJmxConnector = null;
    
    /**
     * constructor
     * @throws JMXConnectionException on error
     */
    public SunASEEJMXConnectionImpl() throws JMXConnectionException
    {
        this(new Properties());
    }

    /**
     * constructor
     * @param connProps connection props
     * @throws JMXConnectionException on error
     */
    public SunASEEJMXConnectionImpl(Properties connProps)
    throws JMXConnectionException
    {
        super();
        initConnectionProperties(connProps);
    }
    
    /**
     * init
     * @param connProps props
     * @throws JMXConnectionException error
     */    
    protected void initConnectionProperties(Properties connProps) 
    throws JMXConnectionException {
        String url = connProps.getProperty(JMXConnectionProperties.URL_PROP);
        String host = connProps.getProperty(JMXConnectionProperties.HOST_PROP);
        String port = connProps.getProperty(JMXConnectionProperties.PORT_PROP);
        String username = connProps.getProperty(JMXConnectionProperties.USERNAME_PROP);
        String password = connProps.getProperty(JMXConnectionProperties.PASSWORD_PROP);
        this.setUrl(url);
        this.setHost(host);
        this.setPort(port);
        this.setUsername(username);
        this.setPassword(password);
        validatePortValue(port);
    }
    
    /**
     * validate
     * @param port port
     * @throws JMXConnectionException on error
     */    
    protected void validatePortValue(String port ) throws JMXConnectionException
    {
        if ( port == null ) return;
        int portValue = -1;
        try
        {
            portValue = Integer.parseInt(port);
        } catch (NumberFormatException ex)
        {
            throw new JMXConnectionException(
                JBIResultXmlBuilder.createJbiResultXml(
                getI18NBundle(),"jbi.ui.jmx.open.error.invalid.port", new Object[] {port}, ex),
                null);
            
        }
    }
    
    /** Getter for property username.
     * @return Value of property username.
     *
     */
    protected String getUrl()
    {
        return this.mUrl;
    }
    
    /**
     * Setter for property username.
     * @param aUrl url
     */
    protected void setUrl(String aUrl)
    {
        
        try
        {
            closeConnection();  // if previously opened
        } catch (Exception ex)
        {
            Util.logDebug(ToolsLogManager.getClientLogger(),ex);
            // ignore this as a secondary exception.
        }
        this.mUrl = aUrl;
    }
    
    /** Getter for property username.
     * @return Value of property username.
     *
     */
    protected String getUsername()
    {
        return this.mUsername;
    }
    
    /**
     * Setter for property username.
     * @param aUsername username
     */
    protected void setUsername(String aUsername)
    {
        
        try
        {
            closeConnection();  // if previously opened
        } catch (Exception ex)
        {
            Util.logDebug(ToolsLogManager.getClientLogger(),ex);
            // ignore this as a secondary exception.
        }
        this.mUsername = aUsername;
    }
    
    /** Getter for property password.
     * @return Value of property password.
     *
     */
    protected String getPassword()
    {
        return this.mPassword;
    }
    
    /**
     * Setter for property password.
     * @param aPassword password
     */
    protected void setPassword(String aPassword)
    {
        
        try
        {
            closeConnection();  // if previously opened
        } catch (Exception ex)
        {
            Util.logDebug(ToolsLogManager.getClientLogger(),ex);
            // ignore this as a secondary exception.
        }
        this.mPassword = aPassword;
    }
    
    
    /** Getter for property host.
     * @return Value of property host.
     *
     */
    protected String getHost()
    {
        return this.mHost;
    }
    
    /** Setter for property host.
     * @param aHost New value of property host.
     *
     */
    protected void setHost(String aHost)
    {
        
        try
        {
            closeConnection();  // if previously opened
        } catch (Exception ex)
        {
            Util.logDebug(ToolsLogManager.getClientLogger(),ex);
            // ignore this as a secondary exception.
        }
        this.mHost = aHost;
    }
    
    /** Getter for property port.
     * @return Value of property port.
     *
     */
    protected String getPort()
    {
        return this.mPort;
    }
    
    /** Setter for property port.
     * @param aPort New value of property port.
     */
    protected void setPort(String aPort)
    {
        // close the previously opened connection
        try
        {
            closeConnection();
        } catch (Exception ex)
        {
            Util.logDebug(ToolsLogManager.getClientLogger(),ex);
            // ignore this as a secondary exception.
        }
        this.mPort = aPort;
    }
    /**
     * returns the creadentials maps that can be passed as env
     * @return credential map
     */
    protected Map getCredentialsMap()
    {
        Map env = new HashMap();
        String username = this.getUsername();
        String password = this.getPassword();
        
        // TODO : using the default from the Connection.properties file is temp.
        // throw the exception if the username/password combination is invalie.
        // e.g. username specified, but null password and vice versa.
        if ( username == null )
        {
            username = JMXConnectionProperties.getInstance().getDefaultUserName();
        }
        
        if ( password == null )
        {
            password = JMXConnectionProperties.getInstance().getDefaultPassword();
        }
        
        ToolsLogManager.getClientLogger().log(Level.FINE, "username,password  : {0} : {1}", new Object[]{username, password});
        
        String[] credentials = new String[]
        { username , password };
        env.put("jmx.remote.credentials", credentials);
        return env;
    }
    /**
     * obtains the the jmx connection if it is not already exists
     * @throws JMXConnectionException on error
     */
    public void openConnection() throws JMXConnectionException
    {
        
        if ( this.mMBeanServerConnection != null )
        {
            return; // already connected.
        }
        
        String url = null;
        try
        {
            
            url = JMXConnectionProperties.getInstance()
            .getConnectionURL(getUrl(), getHost(), getPort());
            ToolsLogManager.getClientLogger().log(Level.FINE, "JMX URL : {0}", url);
            // ToolsLogManager.getClientLogger().severe("JMX URL : " + url);
            this.mJmxUrl = new JMXServiceURL(url);
        } catch (Exception ex )
        {
            throw new JMXConnectionException(
                JBIResultXmlBuilder.createJbiResultXml(
                getI18NBundle(),"jbi.ui.jmx.open.error", new Object[] {url}, ex),
                null);
            
        }
        
        // connect to the remote connector server
        try
        {
            Map env = this.getCredentialsMap();
            this.mJmxConnector =
            JMXConnectorFactory.connect( this.mJmxUrl, env );
        } catch (IOException ioEx)
        {
            // connection communication is wrong port or host wrong.
            // System.out.println("Expcetion occured in JMX Connect" + ex);
            this.mJmxUrl = null;
            throw new JMXConnectionException(
                JBIResultXmlBuilder.createJbiResultXml(
                getI18NBundle(),"jbi.ui.jmx.open.io.error", new Object[] {url}, ioEx),
                null);
            
            
        } catch ( SecurityException secEx ) {
            // security exception wrong username or password or other security problems.
            this.mJmxUrl = null;
            throw new JMXConnectionException(
                JBIResultXmlBuilder.createJbiResultXml(
                getI18NBundle(),"jbi.ui.jmx.open.security.error", new Object[] {url}, secEx),
                null);
            
        } catch (Exception ex) {
            // any other exception 
            this.mJmxUrl = null;
            throw new JMXConnectionException(
                JBIResultXmlBuilder.createJbiResultXml(
                getI18NBundle(),"jbi.ui.jmx.open.error", new Object[] {url}, ex),
                null);
            
        }
        
        // Get an MBeanServerConnection interface
        try
        {
            this.mMBeanServerConnection =
            this.mJmxConnector.getMBeanServerConnection();
        } catch (Exception ex)
        {
            try
            {
                closeConnection();
            } catch (Exception closeEx )
            {
                // ignore close exception
                Util.logDebug(ToolsLogManager.getClientLogger(), closeEx); 
                this.mMBeanServerConnection = null;
            }
            throw new JMXConnectionException(
                JBIResultXmlBuilder.createJbiResultXml(
                getI18NBundle(),"jbi.ui.jmx.open.error", new Object[] {url}, ex),
                null);
            
        }
    }
    
    /**
     * closes the jmx connection
     * @throws JMXConnectionException on error
     */
    public void closeConnection() throws JMXConnectionException
    {
        
        this.mMBeanServerConnection = null;
        this.mJmxUrl = null;
        
        if ( this.mJmxConnector != null )
        {
            try
            {
                this.mJmxConnector.close();
                this.mJmxConnector = null;
            } catch (Exception ex)
            {
                throw new JMXConnectionException(
                    JBIResultXmlBuilder.createJbiResultXml(
                    getI18NBundle(),"jbi.ui.jmx.close.error", null, ex),
                    null);
                
            } finally
            {
                this.mJmxConnector = null;
            }
        }
    }
    
}
