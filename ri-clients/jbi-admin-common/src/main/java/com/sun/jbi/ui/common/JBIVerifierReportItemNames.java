/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIStatisticsItemNames.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

/**
 * This class provides the JBIVerifierReport Item Names
 * These names are used as keys in the CompositeData used
 * to represent the various items in VerifierReport
 * 
 * @author Sun Microsystems, Inc.
 */
public final class JBIVerifierReportItemNames {

    
//--------------------------BEGIN - Common Statistics Keys------------------------------
    
    /** 
     * keys with this prefix and their values are not i18ned
     */
    public static String NON_I18N_KEY_PREFIX = "_";
    
   /**  
    * key denoting the status for an item in the JavaEEVerifierReport
    */        
    public static String JAVAEE_VERIFIER_ITEM_MESSAGE_KEY = "_message";          

   /**  
    * key denoting the status for an item in the JavaEEVerifierReport
    */        
    public static String JAVAEE_VERIFIER_ITEM_STATUS_KEY = "_status";          
    
    /**
     * value denoting okay
     */
    public static String JAVAEE_VERIFIER_ITEM_STATUS_OKAY = "0";

   /**
     * value denoting warning
     */
    public static String JAVAEE_VERIFIER_ITEM_STATUS_WARNING = "1";

    /**
     * value denoting error
     */
    public static String JAVAEE_VERIFIER_ITEM_STATUS_ERROR = "2";


}
