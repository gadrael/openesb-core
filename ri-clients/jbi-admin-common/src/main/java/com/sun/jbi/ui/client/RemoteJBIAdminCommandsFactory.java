/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RemoteJBIAdminCommandsFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client;

import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.JMXConnectionException;
import java.util.Properties;

/** This class is a factory class to create the RemoteJBIAdminCommands implemenation.
 *
 * @author Sun Microsystems, Inc.
 */

public abstract class RemoteJBIAdminCommandsFactory
{
    /** property name for class to load */
    public static final String FACTORY_IMPL_CLASS_PROP =
        "com.sun.jbi.ui.client.RemoteJBIAdminCommandsFactory";
    /** if no property set, try load this impl class  */
    public static final String FACTORY_IMPL_CLASS =
        "com.sun.jbi.internal.ui.client.RemoteJBIAdminCommandsFactoryImpl";
    
    /**
     * constructor
     */
    protected RemoteJBIAdminCommandsFactory()
    {
        // check schemaorg_apache_xmlbeans.system properties and initailize the builders.
    }
    
    /**
     * Creates a Factory class depending on the schemaorg_apache_xmlbeans.system settings. The order it looks for
     * creating the factory implementation is look for the schemaorg_apache_xmlbeans.system property
     * com.sun.jbi.ui.ant.client.RemoteJBIAdminCommandsFactory for the class name to
     * load and then to the standard com.sun.jbi.internal.ui.client.RemoteJBIAdminCommandsFactoryImpl
     * or a default implementation com.sun.jbi.ui.ant.client.RemoteJBIAdminCommandsFactory.Impl
     *
     * @return The instance of the JBIAdminCommandsFactory
     */
    public static RemoteJBIAdminCommandsFactory newInstance()
    {
        String factoryImplClass = System.getProperty(FACTORY_IMPL_CLASS_PROP,
            FACTORY_IMPL_CLASS);
        
        RemoteJBIAdminCommandsFactory impl = null;
        try
        {
            impl = (RemoteJBIAdminCommandsFactory) Class.forName(factoryImplClass).newInstance();
        }
        catch (Exception ex)
        {
            // any exception, just load the default class
            impl = new DefaultFactoryImpl();
        }
        return impl;
    }
    
    /** get jbi client interface
     * @param connProps Connection Properties
     * @throws JMXConnectionException on error
     * @return result
     */
    public abstract RemoteJBIAdminCommands createRemoteJBIAdminCommands(Properties connProps)
    throws JMXConnectionException ;
    
    /**
     * This class is a default factory implementation.
     */
    private static class DefaultFactoryImpl extends RemoteJBIAdminCommandsFactory
    {
        /**
         * constructor.
         */
        public DefaultFactoryImpl()
        {
            super();
        }
        /** get jbi client interface
         * @param connProps Connection Properties
         * @throws JMXConnectionException on error
         * @return result
         */
        public RemoteJBIAdminCommands createRemoteJBIAdminCommands(Properties connProps)
        throws JMXConnectionException
        {
            return new DefaultRemoteJBIAdminCommandsImpl(connProps);
        }
        
        /**
         * string value of the object
         * @return string value of the object
         */
        public String toString()
        {
            return "Default RemoteJBIAdminCommandsFactory Implementation";
        }
    }
    
    /**
     * Default implementation of the RemoteJBIAdminCommands
     */
    public static class DefaultRemoteJBIAdminCommandsImpl
        extends AbstractJMXClient
        implements RemoteJBIAdminCommands
    {
        /** host for jmx connection */
        private String mHost = null;
        
        /**
         * constructor
         * @param connProps Properties object
         */
        public DefaultRemoteJBIAdminCommandsImpl(Properties connProps)
        {
            super();
            this.mHost = connProps.getProperty(JMXConnectionProperties.HOST_PROP);
            
        }
        /**
         * remotely deploys service assembly by uploading the file to server side.
         * Not supported in RI. Using this interface throws unsupported exception in RI.
         * @return result as a management message xml text
         * @param zipFilePath fie path
         * @throws JBIRemoteException on error
         */
        public String remoteDeployServiceAssembly(String zipFilePath) throws JBIRemoteException
        {
            String msg = JBIResultXmlBuilder.createFailedJbiResultXml(getI18NBundle(),
                "jbi.ui.client.remote.deploy.not.supported", new Object[] {
                this.mHost});
                throw new JBIRemoteException(new UnsupportedOperationException(msg));
        }
        /**
         * remotely install service engine or binding component by uploading the file server side
         * Not supported in RI. Using this interface throws unsupported exception in RI.
         * @return result as a management message xml text
         * @param paramProps Properties object contains name,value pair for parameters.
         * @param zipFilePath fie path
         * @throws JBIRemoteException on error
         */
        public String remoteInstallComponent(String zipFilePath, Properties params) throws JBIRemoteException
        {
            String msg = null;
            if ( params != null && params.size() > 0  )
            {
                msg = JBIResultXmlBuilder.createFailedJbiResultXml(getI18NBundle(),
                    "jbi.ui.client.remote.install.component.with.params.not.supported", new Object[] {
                    this.mHost});
                    
            }
            else
            {
                msg = JBIResultXmlBuilder.createFailedJbiResultXml(getI18NBundle(),
                    "jbi.ui.client.remote.install.component.not.supported", new Object[] {
                    this.mHost});
            }
            
            throw new JBIRemoteException(new UnsupportedOperationException(msg));
        }
        /**
         * remotely install service engine or binding component by uploading the file server side
         * Not supported in RI. Using this interface throws unsupported exception in RI.
         * @return result as a management message xml text
         * @param zipFilePath fie path
         * @throws JBIRemoteException on error
         */
        public String remoteInstallComponent(String zipFilePath) throws JBIRemoteException
        {
            String msg = JBIResultXmlBuilder.createFailedJbiResultXml(getI18NBundle(),
                "jbi.ui.client.remote.install.component.not.supported", new Object[] {
                this.mHost});
                
                throw new JBIRemoteException(new UnsupportedOperationException(msg));
        }
        /**
         * remotely install shared library by uploading the file to server side
         * Not supported in RI. Using this interface throws unsupported exception in RI.
         * @return result as a management message xml text
         * @param zipFilePath fie path
         * @throws JBIRemoteException on error
         */
        public String remoteInstallSharedLibrary(String zipFilePath) throws JBIRemoteException
        {
            String msg = JBIResultXmlBuilder.createFailedJbiResultXml(getI18NBundle(),
                "jbi.ui.client.remote.install.slib.not.supported", new Object[] {
                this.mHost});
                
                throw new JBIRemoteException(new UnsupportedOperationException(msg));
        }
        
    }
    
}
