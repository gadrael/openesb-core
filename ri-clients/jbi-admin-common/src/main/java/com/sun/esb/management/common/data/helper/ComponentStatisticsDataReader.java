/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStatisticsDataReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sun.esb.management.common.data.ComponentStatisticsData;

/**
 * @author graj
 * 
 */
public class ComponentStatisticsDataReader extends DefaultHandler implements
        ComponentStatisticsDataXMLConstants, Serializable {
    
    static final long                                                serialVersionUID = -1L;
    
    // Private members needed to parse the XML document
    
    // keep track of parsing
    private boolean                                                  parsingInProgress;
    
    // keep track of QName
    private Stack<String>                                            qNameStack       = new Stack<String>();
    
    private String                                                   componentStatisticsDataListVersion;
    
    private ComponentStatisticsData                                  data;
    
    private Map<String /* instanceName */, ComponentStatisticsData> dataMap;
    
    /** Constructor - creates a new instance of ComponentStatisticsDataReader */
    public ComponentStatisticsDataReader() {
    }
    
    /**
     * @return the dataMap
     */
    public Map<String /* instanceName */, ComponentStatisticsData> getComponentStatisticsDataMap() {
        return this.dataMap;
    }
    
    /**
     * Start of document processing.
     * 
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void startDocument() throws SAXException {
        parsingInProgress = true;
        qNameStack.removeAllElements();
    }
    
    /**
     * End of document processing.
     * 
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void endDocument() throws SAXException {
        parsingInProgress = false;
        // We have encountered the end of the document. Do any processing that
        // is desired, for example dump all collected element2 values.
        
    }
    
    /**
     * Process the new element.
     * 
     * @param uri
     *            is the Namespace URI, or the empty string if the element has
     *            no Namespace URI or if Namespace processing is not being
     *            performed.
     * @param localName
     *            is the The local name (without prefix), or the empty string if
     *            Namespace processing is not being performed.
     * @param qName
     *            is the qualified name (with prefix), or the empty string if
     *            qualified names are not available.
     * @param attributes
     *            is the attributes attached to the element. If there are no
     *            attributes, it shall be an empty Attributes object.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        if (qName != null) {
            if (qName.endsWith(COMPONENT_STATISTICS_DATA_LIST_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if ((attributes != null) && (attributes.getLength() > 0)) {
                    String namespace = attributes.getValue(NAMESPACE_KEY);
                    // //////////////////////////////////////////////////////
                    // Read performanceDataListVersion attribute and ensure you
                    // store the right
                    // performanceDataListVersion of the report map list
                    // //////////////////////////////////////////////////////
                    this.componentStatisticsDataListVersion = attributes
                            .getValue(VERSION_KEY);
                    if ((componentStatisticsDataListVersion != null)
                            && (VERSION_VALUE
                                    .equals(componentStatisticsDataListVersion))) {
                        this.dataMap = new HashMap<String /* instanceName */, ComponentStatisticsData>();
                    } else {
                        // Invalid componentStatisticsDataListVersion.
                        // Not storing it
                    }
                }
            } else if (qName.endsWith(COMPONENT_STATISTICS_DATA_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if (this.dataMap != null) {
                    this.data = new ComponentStatisticsData();
                }
            }
            // Keep track of QNames
            qNameStack.push(qName);
        }
    }
    
    /**
     * Process the character report for current tag.
     * 
     * @param ch
     *            are the element's characters.
     * @param start
     *            is the start position in the character array.
     * @param length
     *            is the number of characters to use from the character array.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        String qName;
        String chars = new String(ch, start, length);
        // Get current QName
        qName = (String) qNameStack.peek();
        if (qName.endsWith(INSTANCE_NAME_KEY)) {
            if (this.data != null) {
                this.data.setInstanceName(chars);
            }
        } else if (qName.endsWith(COMPONENT_UPTIME_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setComponentUpTime(value);
            }
        } else if (qName.endsWith(NUMBER_OF_ACTIVATED_ENDPOINTS_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfActivatedEndpoints(value);
            }
        } else if (qName.endsWith(NUMBER_OF_RECEIVED_REQUESTS_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfReceivedRequests(value);
            }
        } else if (qName.endsWith(NUMBER_OF_SENT_REQUESTS_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfSentRequests(value);
            }
        } else if (qName.endsWith(NUMBER_OF_RECEIVED_REPLIES_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfReceivedReplies(value);
            }
        } else if (qName.endsWith(NUMBER_OF_SENT_REPLIES_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfSentReplies(value);
            }
        } else if (qName.endsWith(NUMBER_OF_RECEIVED_DONES_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfReceivedDones(value);
            }
        } else if (qName.endsWith(NUMBER_OF_SENT_DONES_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfSentDones(value);
            }
        } else if (qName.endsWith(NUMBER_OF_RECEIVED_FAULTS_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfReceivedFaults(value);
            }
        } else if (qName.endsWith(NUMBER_OF_SENT_FAULTS_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfSentFaults(value);
            }
        } else if (qName.endsWith(NUMBER_OF_RECEIVED_ERRORS_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfReceivedErrors(value);
            }
        } else if (qName.endsWith(NUMBER_OF_SENT_ERRORS_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfSentErrors(value);
            }
        } else if (qName.endsWith(NUMBER_OF_COMPLETED_EXCHANGES_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfCompletedExchanges(value);
            }
        } else if (qName.endsWith(NUMBER_OF_ACTIVE_EXCHANGES_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfActiveExchanges(value);
            }
        } else if (qName.endsWith(NUMBER_OF_ERROR_EXCHANGES_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setNumberOfErrorExchanges(value);
            }
        } else if (qName.endsWith(EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY)) {
            if (this.data != null) {
                boolean value = Boolean.valueOf(chars);
                this.data.setExtendedTimingStatisticsFlagEnabled(value);
            }
        } else if (qName.endsWith(MESSAGE_EXCHANGE_RESPONSE_TIME_AVERAGE_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setMessageExchangeResponseTimeAverage(value);
            }
        } else if (qName.endsWith(MESSAGE_EXCHANGE_COMPONENT_TIME_AVERAGE_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setMessageExchangeComponentTimeAverage(value);
            }
        } else if (qName.endsWith(MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME_AVERAGE_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setMessageExchangeDeliveryChannelTimeAverage(value);
            }
        } else if (qName.endsWith(MESSAGE_EXCHANGE_MESSAGE_SERVICE_TIME_AVERAGE_KEY)) {
            if (this.data != null) {
                long value = Long.valueOf(chars);
                this.data.setMessageExchangeMessageServiceTimeAverage(value);
            }
        } else if (qName.endsWith(COMPONENT_EXTENSION_STATUS_KEY)) {
            if (this.data != null) {
                this.data.setComponentExtensionStatusAsString(chars);
            }
        }
    }
    
    /**
     * Process the end element tag.
     * 
     * @param uri
     *            is the Namespace URI, or the empty string if the element has
     *            no Namespace URI or if Namespace processing is not being
     *            performed.
     * @param localName
     *            is the The local name (without prefix), or the empty string if
     *            Namespace processing is not being performed.
     * @param qName
     *            is the qualified name (with prefix), or the empty string if
     *            qualified names are not available.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        // Pop QName, since we are done with it
        qNameStack.pop();
        if (qName != null) {
            if (qName.endsWith(COMPONENT_STATISTICS_DATA_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) && (this.data != null)) {
                    this.dataMap.put(this.data.getInstanceName(), this.data);
                    this.data = null;
                }
            }
        }
    }
    
    /**
     * 
     * @param rawXMLData
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ComponentStatisticsData> parseFromXMLData(
            String rawXMLData) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        // System.out.println("Parsing file: "+uriString);
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the XML Document InputStream
        Reader reader = new StringReader(rawXMLData);
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(reader);
        
        // Parse the aspectInput XML document stream, using my event handler
        ComponentStatisticsDataReader parser = new ComponentStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getComponentStatisticsDataMap();
        
    }
    
    /**
     * 
     * @param fileName
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ComponentStatisticsData> parseFromFile(
            String fileName) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        File file = new File(fileName);
        return parseFromFile(file);
    }
    
    /**
     * 
     * @param fileName
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ComponentStatisticsData> parseFromFile(
            File file) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the URI and XML Document InputStream
        InputStream inputStream = new FileInputStream(file);
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(inputStream);
        
        // Parse the aspectInput XML document stream, using my event handler
        ComponentStatisticsDataReader parser = new ComponentStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getComponentStatisticsDataMap();
    }
    
    /**
     * 
     * @param uriString
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ComponentStatisticsData> parseFromURI(
            String uriString) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        URI uri = new URI(uriString);
        return parseFromURI(uri);
    }
    
    /**
     * 
     * @param uri
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ComponentStatisticsData> parseFromURI(
            URI uri) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the URI and XML Document InputStream
        InputStream inputStream = uri.toURL().openStream();
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(inputStream);
        
        // Parse the aspectInput XML document stream, using my event handler
        ComponentStatisticsDataReader parser = new ComponentStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getComponentStatisticsDataMap();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/componentstatistics/ComponentStatisticsData.xml";
        try {
            Map<String /* instanceName */, ComponentStatisticsData> map = null;
            map = ComponentStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println(map.get(instanceName).getDisplayString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
