/* 
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IEndpointStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.Map;

/**
 * Provides Endpoint Statistics Interface
 * 
 * @author graj
 */
public interface IEndpointStatisticsData extends Serializable {
    /**
     * @return the isProvisioningEndpoint
     */
    public boolean isProvisioningEndpoint();
    
    /**
     * @param isProvisioningEndpoint
     *            the isProvisioningEndpoint to set
     */
    public void setProvisioningEndpoint(boolean isProvisioningEndpoint);
    
    
    /**
     * @return the instanceName
     */
    public String getInstanceName();
    
    /**
     * @param instanceName
     *            the instanceName to set
     */
    public void setInstanceName(String instanceName);
    
    /**
     * @return the componentName
     */
    public String getComponentName();

    /**
     * @param componentName the componentName to set
     */
    public void setComponentName(String componentName);
    
    /**
     * @return the numberOfActiveExchanges
     */
    public long getNumberOfActiveExchanges();
    
    /**
     * @param numberOfActiveExchanges
     *            the numberOfActiveExchanges to set
     */
    public void setNumberOfActiveExchanges(long numberOfActiveExchanges);
    
    /**
     * @return the numberOfReceivedDones
     */
    public long getNumberOfReceivedDones();
    
    /**
     * @param numberOfReceivedDones
     *            the numberOfReceivedDones to set
     */
    public void setNumberOfReceivedDones(long numberOfReceivedDones);
    
    /**
     * @return the numberOfSentDones
     */
    public long getNumberOfSentDones();
    
    /**
     * @param numberOfSentDones
     *            the numberOfSentDones to set
     */
    public void setNumberOfSentDones(long numberOfSentDones);
    
    /**
     * @return the numberOfReceivedFaults
     */
    public long getNumberOfReceivedFaults();
    
    /**
     * @param numberOfReceivedFaults
     *            the numberOfReceivedFaults to set
     */
    public void setNumberOfReceivedFaults(long numberOfReceivedFaults);
    
    /**
     * @return the numberOfReceivedErrors
     */
    public long getNumberOfReceivedErrors();
    
    /**
     * @param numberOfReceivedErrors
     *            the numberOfReceivedErrors to set
     */
    public void setNumberOfReceivedErrors(long numberOfReceivedErrors);
    
    /**
     * @return the numberOfSentFaults
     */
    public long getNumberOfSentFaults();
    
    /**
     * @param numberOfSentFaults
     *            the numberOfSentFaults to set
     */
    public void setNumberOfSentFaults(long numberOfSentFaults);
    
    /**
     * @return the numberOfSentErrors
     */
    public long getNumberOfSentErrors();
    
    /**
     * @param numberOfSentErrors
     *            the numberOfSentErrors to set
     */
    public void setNumberOfSentErrors(long numberOfSentErrors);
    
    /**
     * @return the extendedTimingStatisticsFlagEnabled
     */
    public boolean isExtendedTimingStatisticsFlagEnabled();

    /**
     * @param extendedTimingStatisticsFlagEnabled the extendedTimingStatisticsFlagEnabled to set
     */
    public void setExtendedTimingStatisticsFlagEnabled(
            boolean extendedTimingStatisticsFlagEnabled);
    
    /**
     * @return the messageExchangeComponentTimeAverage
     */
    public long getMessageExchangeComponentTimeAverage();
    
    /**
     * @param messageExchangeComponentTimeAverage
     *            the messageExchangeComponentTimeAverage to set
     */
    public void setMessageExchangeComponentTimeAverage(long messageExchangeComponentTimeAverage);
    
    /**
     * @return the messageExchangeDeliveryChannelTimeAverage
     */
    public long getMessageExchangeDeliveryChannelTimeAverage();
    
    /**
     * @param messageExchangeDeliveryChannelTimeAverage
     *            the messageExchangeDeliveryChannelTimeAverage to set
     */
    public void setMessageExchangeDeliveryChannelTimeAverage(long messageExchangeDeliveryChannelTimeAverage);
    
    /**
     * @return the messageExchangeServiceTimeAverage
     */
    public long getMessageExchangeServiceTimeAverage();
    
    /**
     * @param messageExchangeServiceTimeAverage
     *            the messageExchangeServiceTimeAverage to set
     */
    public void setMessageExchangeServiceTimeAverage(long messageExchangeServiceTimeAverage);
    
    /**
     * @return the categoryToPerformanceDataMap
     */
    public Map<String, PerformanceData> getCategoryToPerformanceDataMap();
    
    /**
     * @param categoryToPerformanceDataMap
     *            the categoryToPerformanceDataMap to set
     */
    public void setCategoryToPerformanceDataMap(Map<String, PerformanceData> categoryToPerformanceDataMap);
    
    /**
     * Return a displayable string of values
     * 
     * @return
     */
    public String getDisplayString();
}
