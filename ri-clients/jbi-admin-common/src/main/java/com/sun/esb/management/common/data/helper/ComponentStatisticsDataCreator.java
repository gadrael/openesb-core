/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStatisticsDataCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.util.Map;
import java.util.Set;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.ComponentStatisticsData;

/**
 * @author graj
 *
 */
public class ComponentStatisticsDataCreator {
    /**  table index   */
    protected static String[] STATS_TABLE_INDEX = new String[] { "InstanceName" };
    
    protected static String[] itemNames         = new String[] {
            "InstanceName", "ComponentUpTime", "NumActivatedEndpoints",
            "NumReceivedRequests", "NumSentRequests", "NumReceivedReplies",
            "NumSentReplies", "NumReceivedDONEs", "NumSentDONEs",
            "NumReceivedFaults", "NumSentFaults", "NumReceivedErrors",
            "NumSentErrors", "NumCompletedExchanges", "NumActiveExchanges",
            "NumErrorExchanges", "ME-ResponseTime-Avg", "ME-ComponentTime-Avg",
            "ME-DeliveryChannelTime-Avg", "ME-MessageServiceTime-Avg",
            "ComponentExtensionStats"          };
    
    protected static String[] itemDescriptions  = new String[] {
            "Instance Name", "Component Uptime",
            "Number of activated endpoints", "Number of received requests",
            "Number of sent requests", "Number of received replies",
            "Number of sent replies", "Number of received DONEs",
            "Number of sent DONEs", "Number of received faults",
            "Number of sent faults", "Number of received errors",
            "Number of sent errors", "Number of completed exchanges",
            "Number of active exchanges", "Number of error exchanges",
            "Avg. response time for message exchange",
            "Avg. time taken in component by message exchange",
            "Avg. time taken in delivery channel by message exchange",
            "Avg. time taken in message service by message exchange",
            "Statistics reported by component statistics MBeans" };
    
    protected static String[] itemNamesNoExt         = new String[] {
            "InstanceName", "ComponentUpTime", "NumActivatedEndpoints",
            "NumReceivedRequests", "NumSentRequests", "NumReceivedReplies",
            "NumSentReplies", "NumReceivedDONEs", "NumSentDONEs",
            "NumReceivedFaults", "NumSentFaults", "NumReceivedErrors",
            "NumSentErrors", "NumCompletedExchanges", "NumActiveExchanges",
            "NumErrorExchanges", "ME-ResponseTime-Avg", "ME-ComponentTime-Avg",
            "ME-DeliveryChannelTime-Avg", "ME-MessageServiceTime-Avg"};
   
    protected static String[] itemDescriptionsNoExt  = new String[] {
            "Instance Name", "Component Uptime",
            "Number of activated endpoints", "Number of received requests",
            "Number of sent requests", "Number of received replies",
            "Number of sent replies", "Number of received DONEs",
            "Number of sent DONEs", "Number of received faults",
            "Number of sent faults", "Number of received errors",
            "Number of sent errors", "Number of completed exchanges",
            "Number of active exchanges", "Number of error exchanges",
            "Avg. response time for message exchange",
            "Avg. time taken in component by message exchange",
            "Avg. time taken in delivery channel by message exchange",
            "Avg. time taken in message service by message exchange"};

    /**
     * 
     */
    public ComponentStatisticsDataCreator() {
    }
    
    /**
     * 
     * @param data
     * @return
     * @throws ManagementRemoteException
     */
    public static TabularData createTabularData(
            Map<String /* instanceName */, ComponentStatisticsData> map)
            throws ManagementRemoteException {
        TabularData componentStatisticsTable = null;
        TabularType componentStatisticsTableType = null;
        
        Set<String> instanceNames = map.keySet();
        CompositeData[] componentStatistics = new CompositeData[instanceNames
                .size()];
        int index = 0;
        for (String instanceName : instanceNames) {
            ComponentStatisticsData data = map.get(instanceName);
            if(data != null) {
                componentStatistics[index++] = composeComponentStatistics(data);
            }
        }
        // The tabular type can be constructed only after we have the component 
        // statistics composite type. We need at least one entry
        try {
            if (index > 0 && componentStatistics[0] != null) {
                componentStatisticsTableType = new TabularType(
                        "ComponentStats", "Component Statistic Information",
                        componentStatistics[0].getCompositeType(),
                        STATS_TABLE_INDEX);
                componentStatisticsTable = new TabularDataSupport(
                        componentStatisticsTableType);
                
                for (int innerIndex = 0; innerIndex < index; innerIndex++) {
                    componentStatisticsTable
                            .put(componentStatistics[innerIndex]);
                }
            }
        } catch (OpenDataException e) {
            throw new ManagementRemoteException(e);
        }
        
        return componentStatisticsTable;
    }

    /**
     * This method is used compose component statistics composite data
     * @param data
     * @return
     * @throws ManagementRemoteException
     */
    protected static CompositeData composeComponentStatistics(
            ComponentStatisticsData data) throws ManagementRemoteException {
        try {
            CompositeType type = null;
            if(data.getComponentExtensionStatus() != null) {
                type = data.getComponentExtensionStatus().getCompositeType();
            }

            OpenType[] itemTypes = null;
            Object[] itemValues = null;
            if(type != null)
            {
                itemTypes = new OpenType[] { SimpleType.STRING,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG,
                    type };
            
                itemValues = new Object[] { data.getInstanceName(),
                    data.getComponentUpTime(),
                    data.getNumberOfReceivedRequests(),
                    data.getNumberOfSentRequests(),
                    data.getNumberOfReceivedReplies(),
                    data.getNumberOfSentReplies(),
                    data.getNumberOfReceivedDones(),
                    data.getNumberOfSentDones(),
                    data.getNumberOfReceivedFaults(),
                    data.getNumberOfSentFaults(),
                    data.getNumberOfReceivedErrors(),
                    data.getNumberOfSentErrors(),
                    data.getNumberOfCompletedExchanges(),
                    data.getNumberOfActiveExchanges(),
                    data.getNumberOfActiveExchanges(),
                    data.getNumberOfErrorExchanges(),
                    data.getMessageExchangeResponseTimeAverage(),
                    data.getMessageExchangeComponentTimeAverage(),
                    data.getMessageExchangeDeliveryChannelTimeAverage(),
                    data.getMessageExchangeMessageServiceTimeAverage(),
                    data.getComponentExtensionStatus() };

                return new CompositeDataSupport(new CompositeType("ComponentStats",
                    "Component Statistics", itemNames, itemDescriptions,
                    itemTypes), itemNames, itemValues);
            }
            else
            {
                itemTypes = new OpenType[] { SimpleType.STRING,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG,
                    };
            
                itemValues = new Object[] { data.getInstanceName(),
                    data.getComponentUpTime(),
                    data.getNumberOfReceivedRequests(),
                    data.getNumberOfSentRequests(),
                    data.getNumberOfReceivedReplies(),
                    data.getNumberOfSentReplies(),
                    data.getNumberOfReceivedDones(),
                    data.getNumberOfSentDones(),
                    data.getNumberOfReceivedFaults(),
                    data.getNumberOfSentFaults(),
                    data.getNumberOfReceivedErrors(),
                    data.getNumberOfSentErrors(),
                    data.getNumberOfCompletedExchanges(),
                    data.getNumberOfActiveExchanges(),
                    data.getNumberOfActiveExchanges(),
                    data.getNumberOfErrorExchanges(),
                    data.getMessageExchangeResponseTimeAverage(),
                    data.getMessageExchangeComponentTimeAverage(),
                    data.getMessageExchangeDeliveryChannelTimeAverage(),
                    data.getMessageExchangeMessageServiceTimeAverage() };

                return new CompositeDataSupport(new CompositeType("ComponentStats",
                    "Component Statistics", itemNamesNoExt, itemDescriptionsNoExt,
                    itemTypes), itemNamesNoExt, itemValues);
            }
 
        } catch (OpenDataException ex) {
            throw new ManagementRemoteException(ex);
        }
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
