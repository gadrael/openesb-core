/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationService.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.api.configuration;

import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularData;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.ui.common.JBIRemoteException;

/**
 * Defines operations for common configuration services across the JBI Runtime,
 * component containers, configuring logger levels, etc.
 * 
 * @author graj
 */
public interface ConfigurationService {
    /**
     * Retrieve component configuration
     * 
     * @param componentName
     * @param targetName
     * @return the targetName as key and the name/value pairs as properties
     * @throws ManagementRemoteException
     *             on error
     */
    public Properties getComponentConfiguration(String componentName,
            String targetName) throws ManagementRemoteException;
    
    /**
     * Gets the extension MBean object names
     * 
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetInstanceName */, ObjectName[]> getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName)
            throws ManagementRemoteException;
    
    /**
     * Gets the extension MBean object names
     * 
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return an array of ObjectName(s)
     * @throws ManagementRemoteException
     *             on error
     */
    public ObjectName[] getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName,
            String targetInstanceName) throws ManagementRemoteException;
    
    /**
     * Gets the component custom loggers and their levels
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* loggerCustomName */, Level /* logLevel */> getComponentLoggerLevels(
            String componentName, String targetName, String targetInstanceName)
            throws ManagementRemoteException;
    
    /**
     * Gets the component custom loggers and their display names.
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their display names
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* loggerCustomName */, String /* displayName */> getComponentLoggerDisplayNames(
            String componentName, String targetName, String targetInstanceName)
            throws ManagementRemoteException;
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Properties that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Properties getDefaultRuntimeConfiguration()
            throws ManagementRemoteException;
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Properties that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Properties getRuntimeConfiguration(String targetName)
            throws ManagementRemoteException;
    
    /**
     * This method returns the runtime configuration metadata associated with
     * the specified property. The metadata contain name-value pairs like:
     * default, descriptionID, descriptorType, displayName, displayNameId,
     * isStatic, name, resourceBundleName, tooltip, tooltipId, etc.
     * 
     * @param propertyKeyName
     * @return Properties that represent runtime configuration metadata
     * @throws ManagementRemoteException
     */
    Properties getRuntimeConfigurationMetaData(String propertyKeyName)
            throws ManagementRemoteException;
    
    /**
     * Lookup the level of one runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger (e.g. com.sun.jbi.framework
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     * @deprecated
     */
    public Level getRuntimeLoggerLevel(String runtimeLoggerName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException;
    
    /**
     * Gets all the runtime loggers and their display names
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of display names to their logger names
     * @throws ManagementRemoteException
     *             on error
     * @deprecated
     */
    public Map<String /* display name */, String /* logger name */> getRuntimeLoggerNames(
            String targetName, String targetInstanceName)    
            throws ManagementRemoteException;
    
    /**
     * Gets all the runtime loggers and their levels
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     * @deprecated
     */
    public Map<String /* runtimeLoggerName */, Level /* logLevel */> getRuntimeLoggerLevels(
            String targetName, String targetInstanceName)
            throws ManagementRemoteException;
    
        
    /**
     * Lookup the level of one runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger (e.g. com.sun.jbi.framework
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     */
    public Level getRuntimeLoggerLevel(String runtimeLoggerName,
            String targetName) throws ManagementRemoteException;
    
    /**
     * Gets all the runtime loggers and their display names
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of display names to their logger names
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* display name */, String /* logger name */> getRuntimeLoggerNames(
            String targetName) throws ManagementRemoteException;
    
    /**
     * Gets all the runtime loggers and their levels
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* runtimeLoggerName */, Level /* logLevel */> getRuntimeLoggerLevels(
            String targetName) throws ManagementRemoteException;
    
    /**
     * checks if the server need to be restarted to apply the changes made to
     * some of the configuration parameters.
     * 
     * @return true if server needs to be restarted for updated configuration to
     *         take effect. false if no server restart is needed.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public boolean isServerRestartRequired() throws ManagementRemoteException;
    
    /**
     * Will return jbi mgmt message with success, failure, or partial success
     * per instance. The entry per instance will have value as part of the
     * management message (XML) String.
     * 
     * @param componentName
     * @param configurationValue
     * @param targetName
     * @return value as part of the management message (XML) String.
     * @throws ManagementRemoteException
     *             on error
     */
    public String setComponentConfiguration(String componentName,
            Properties configurationValues, String targetName)
            throws ManagementRemoteException;
    
    /**
     * Sets the component log level for a given custom logger
     * 
     * @param componentName
     *            name of the component
     * @param loggerCustomName
     *            the custom logger name
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @throws ManagementRemoteException
     *             on error
     */
    public void setComponentLoggerLevel(String componentName,
            String loggerCustomName, Level logLevel, String targetName,
            String targetInstanceName) throws ManagementRemoteException;
    
    /**
     * This method sets one or more configuration parameters on the runtime with
     * a list of name/value pairs passed as a properties object. The property
     * name in the properties object should be an existing configuration
     * parameter name. If user try to set the parameter that is not in the
     * configuration parameters list, this method will throw an exception.
     * 
     * The value of the property can be any object. If the value is non string
     * object, its string value (Object.toString()) will be used as a value that
     * will be set on the configuration.
     * 
     * This method first validates whether all the paramters passed in
     * properties object exist in the runtime configuration or not. If any one
     * the parameters passed is not existing, it will return an error without
     * settings the parameters that are passed in the properties including a
     * valid parameters.
     * 
     * If there is an error in setting a paramter, this method throws an
     * exception with the list of parameters that were not set.
     * 
     * @param params
     *            Properties object that contains name/value pairs corresponding
     *            to the configuration parameters to be set on the runtime.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return true if server restart is required, false if not
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error or a invalid parameter is passed in
     *             the params properties object. In case of an error setting the
     *             a particular parameter, the error message should list the
     *             invalid parameters.
     */
    public boolean setRuntimeConfiguration(Properties parameters,
            String targetName) throws ManagementRemoteException;
    
    /**
     * Sets the log level for a given runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @throws ManagementRemoteException
     *             on error
     * @deprecated
     */
    public void setRuntimeLoggerLevel(String runtimeLoggerName, Level logLevel,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException;
    
        
    /**
     * Sets the log level for a given runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @throws ManagementRemoteException
     *             on error
     */
    public void setRuntimeLoggerLevel(String runtimeLoggerName, Level logLevel,
            String targetName) throws ManagementRemoteException;
    
    /**
     * Return the display name for a runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the logger (e.g. com.sun.jbi.framework)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return the display name for the given logger
     * @throws JBIRemoteException
     *             on error
     * @deprecated
     */
    public String getRuntimeLoggerDisplayName(String runtimeLoggerName,
            String targetName, String targetInstanceName)    
    throws ManagementRemoteException;
    
        
    /**
     * Return the display name for a runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the logger (e.g. com.sun.jbi.framework)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return the display name for the given logger
     * @throws JBIRemoteException
     *             on error
     */
    public String getRuntimeLoggerDisplayName(String runtimeLoggerName,
            String targetName) throws ManagementRemoteException;

    ////////////////////////////////////////////////////
    // Application Configuration & Application Variables
    ////////////////////////////////////////////////////

    /**
     * This method is used to verify if the application variables and 
     * application configuration objects used in the given 
     * application are available in JBI runtime in the specified target. 
     * Also this method verifies if all necessary  components are installed.
     * If generateTemplates is true templates for missing application variables 
     * and application configurations are generated. A command script that uses
     * the template files to set configuration objects is generated.
     *
     * @param applicationURL the URL for the application zip file
     * @param generateTemplates true if templates have to be generated
     * @param templateDir the dir to store the generated templates
     * @param includeDeployCommand true if the generated script should include
     * deploy command 
     * @param targetName the target on which the application has to be verified
     * @param clientSAFilePath is used to pass on the file path to the SA in the client side
     *  - used in remote server use cases.
     *
     * @returns CompositeData the verification report
     * 
     * CompositeType of verification report
     *  String          - "ServiceAssemblyName",
     *  String          - "ServiceAssemblyDescription",
     *  Integer         - "NumServiceUnits",
     *  Boolean         - "AllComponentsInstalled",
     *  String[]        - "MissingComponentsList",
     *  CompositeData[] - "EndpointInfo",
     *  String          - "TemplateZIPID"
     * 
     * CompositeType of each EndpointInfo
     *  String    - "EndpointName",
     *  String    - "ServiceUnitName",
     *  String    - "ComponentName",
     *  String    - "Status"
     * 
     * @throws ManagementRemoteException if the application could not be verified
     * 
     * Note: param templateDir is used between ant/cli and common client client
     * TemplateZIPID is used between common client server and common client client
     */
    public String verifyApplication(String applicationURL,
            boolean generateTemplates, String templateDir,
            boolean includeDeployCommand, String targetName,
            String clientSAFilePath)
     throws ManagementRemoteException;    
     
    /**
     * This method is used to export the application variables and 
     * application configuration objects used by the given 
     * application in the specified target. 
     *
     * @param applicationName the name of the application
     * @param targetName the target whose configuration has to be exported
     * @param configDir the dir to store the configurations
     * @returns String the id for the zip file with exported configurations
     *
     * @throws ManagementRemoteException if the application configuration could not be exported
     *
     * Note: param configDir is used between ant/cli and common client client.
     * The return value is used between common client server and common client client.
     */
    public String exportApplicationConfiguration(String applicationName,
             String targetName, String configDir)
     throws ManagementRemoteException;      
    
    
    /**
     * Detect the components support for component configuration. This method
     * returns true if the component has a configuration MBean with configurable
     * attributes
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean has configuration 
     *              attributes
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isComponentConfigSupported(String componentName, String targetName)  
        throws ManagementRemoteException;
             
    
    /*----------------------------------------------------------------------------------*\
     *              Operations for Application Variable Management                      *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application configuration. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application configuration management.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean implements all the
     *         operations for application configuration.
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppConfigSupported(String componentName, String targetName)  
        throws ManagementRemoteException;
     
    /**
     * Detect the components support for application variables. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application variable management.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean implements all the
     *         operations for application variables.
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppVarsSupported(String componentName, String targetName)  
        throws ManagementRemoteException;
     
    /**
     * Add application variables to a component installed on a given target. If even a
     * variable from the set is already set on the component, this operation fails.
     * 
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @param  appVariables - set of application variables to add. The values of the
     *                     application variables have the application variable type and
     *                     value and the format is "[type]value"
     * @return
     *             a JBI Management message indicating the status of the operation.
     *             In case a variable is not added the management message has a 
     *             ERROR task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String addApplicationVariables(String componentName,
            String targetName, Properties appVariables)
            throws ManagementRemoteException;
    
    /**
     * Set application variables on a component installed on a given target. If even a
     * variable from the set has not been added to the component, this operation fails.
     * 
     * @param componentName 
     *             component identification
     * @param  appVariables - set of application variables to update. The values of the
     *                     application variables have the application variable type and
     *                     value and the format is "[type]value"
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *             cluster or clustered instance.
     * @return
     *             a JBI Management message indicating the status of the operation.
     *             In case a variable is not set the management message has a 
     *             ERROR task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String setApplicationVariables (String componentName, 
            Properties appVariables, String targetName)
            throws ManagementRemoteException;
    
    /**
     * Delete application variables from a component installed on a given target. If even a
     * variable from the set has not been added to the component, this operation fails.
     * 
     * @param componentName
     *             component identification
     * @param targetName
     *            identification of the target. Can be a standalone server, 
     *            cluster or clustered instance.
     * @param  appVariableNames - names  of application variables to delete.
     * @return
     *             a JBI Management message indicating the status of the operation.
     *             In case a variable is not deleted the management message has a 
     *             ERROR task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String deleteApplicationVariables(String componentName,
            String targetName, String[] appVariableNames)
            throws ManagementRemoteException;
     
    /**
     * Get all the application variables set on a component.
     *
     * @return all the application variables et on the component. The return proerties
     *         set has the name="[type]value" pairs for the application variables.
     * @return
     *             a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Properties getApplicationVariables(String componentName,
            String targetName) throws ManagementRemoteException;
     
    /*----------------------------------------------------------------------------------*\
     *              Operations for Application Configuration Management                 *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Get the CompositeType definition for the components application configuration 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return the CompositeType for the components application configuration.
     */
    public javax.management.openmbean.CompositeType queryApplicationConfigurationType(
            String componentName, String targetName)
             throws ManagementRemoteException;
     
    /**
     * Add a named application configuration to a component installed on a given target. 
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @param name 
     *             application configuration name
     * @param config 
     *             application configuration represented as a set of properties.
     * @return
     *             a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String     addApplicationConfiguration (String componentName, 
        String targetName, String name, Properties config)
            throws ManagementRemoteException;
    
    /**
     * Update a named application configuration in a component installed on a given target. 
     *
     * @param componentName 
     *            component identification
     * @param name 
     *             application configuration name
     * @param config 
     *             application configuration represented as a set of properties.
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return
     *             a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String     setApplicationConfiguration (String componentName, 
        String name, Properties config, String targetName)
            throws ManagementRemoteException;
    
    /**
     * Delete a named application configuration in a component installed on a given target. 
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @param name 
     *             name of application configuration to be deleted
     * @return
     *             a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String  deleteApplicationConfiguration (String componentName, 
            String targetName, String name) throws ManagementRemoteException;
    
    /**
     * List all the application configurations in a component.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return
     *              an array of names of all the application configurations.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String[] listApplicationConfigurationNames(String componentName,
            String targetName) throws ManagementRemoteException;
    
    /**
     * Get a specific named configuration. If the named configuration does not exist
     * in the component the returned properties is an empty set.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return the application configuration represented as a set of properties.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Properties getApplicationConfiguration(String componentName,
            String name, String targetName) throws ManagementRemoteException;
    
        /**
     * Get all the application configurations set on a component.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return 
     *             a map of all the application configurations keyed by the 
     *             configuration name.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Map<String, Properties> getApplicationConfigurations(
            String componentName, String targetName)
            throws ManagementRemoteException;
    
    
    
    /** 
     * Retrieves the component specific configuration schema.
     *
     * @param componentName
     *        component identification
     * @param targetName
     *        identification of the target. Can be a standalone server,
     *        cluster or clustered instance.
     * @return a String containing the configuration schema.
     * @throws ManagementRemoteException if the config schema cannot be retrieved.
     */
    public String retrieveConfigurationDisplaySchema(String componentName,
            String targetName) throws ManagementRemoteException;;
    
    /** 
     * Retrieves the component configuration metadata.
     * The XML data conforms to the component 
     * configuration schema.
     *
     * @param componentName
     *        component identification
     * @param targetName
     *        identification of the target. Can be a standalone server,
     *        cluster or clustered instance.
     * @return a String containing the configuration metadata.
     * @throws ManagementRemoteException if the config data cannot be retrieved.
     */
    public String retrieveConfigurationDisplayData(String componentName,
            String targetName) throws ManagementRemoteException;

    /**
     * Add an application configuration. The configuration name is a part of the CompositeData.
     * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
     *
     * @param componentName
     * @param targetName
     * @param name - configuration name, must match the value of the field "name" in the namedConfig
     * @param appConfig - application configuration composite 
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     * @throws ManagementRemoteException if the application configuration cannot be added.
     */
    String addApplicationConfiguration(String componentName, String targetName,
            String name, CompositeData appConfig)
        throws ManagementRemoteException;    
    
    /**
     * Update a application configuration. The configuration name is a part of the CompositeData.
     * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
     *
     * @param componentName
     * @param targetName
     * @param name - configuration name, must match the value of the field "configurationName" in the appConfig
     * @param appConfig - application configuration composite
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     * @throws ManagementRemoteException if there are errors encountered when updating the configuration.
     */
    String setApplicationConfiguration(String componentName, String targetName,
            String name, CompositeData appConfig)
            throws ManagementRemoteException;

    /**
     * Get a Map of all application configurations for the component.
     *
     * @param componentName
     * @param targetName
     * @return a TabularData of all the application configurations for a 
     *         component keyed by the configuration name. 
     * @throws ManagementRemoteException if there are errors encountered when updating the configuration.
     */
    TabularData getApplicationConfigurationsAsTabularData(String componentName,
            String targetName) throws ManagementRemoteException;


    /**
     * Get the Application Variable set for a component.
     *
     * @param componentName
     * @param targetName
     * @return  a TabularData which has all the application variables set on the component. 
     * @throws ManagementRemoteException
     */
    TabularData getApplicationVariablesAsTabularData(String componentName,
            String targetName) throws ManagementRemoteException;
    
    /**
     * This operation adds a new application variable. If a variable already exists with 
     * the same name as that specified then the operation fails.
     * 
     * @param componentName
     * @param targetName
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     * @throws ManagementRemoteException if an error occurs in adding the application variables to the 
     *         component. 
     */
    String addApplicationVariable(String componentName, String targetName,
            String name, CompositeData appVar) throws ManagementRemoteException;
    
    /**
     * This operation sets an application variable. If a variable does not exist with 
     * the same name, its an error.
     * 
     * @param componentName
     * @param targetName
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste to be updated.
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     * @throws ManagementRemoteException if one or more application variables cannot be deleted
     */
    String setApplicationVariable(String componentName, String targetName,
            String name, CompositeData appVar) throws ManagementRemoteException;
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Map that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Map<String /*attributeName*/, Object /*attributeValue*/> getDefaultRuntimeConfigurationAsMap()
            throws ManagementRemoteException;
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Map that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Map<String /*attributeName*/, Object /*attributeValue*/> getRuntimeConfigurationAsMap(String targetName)
            throws ManagementRemoteException;
            
   /**
     * This method sets one or more configuration parameters on the runtime with
     * a list of name/value pairs passed as a properties object. The property
     * name in the properties object should be an existing configuration
     * parameter name. If user try to set the parameter that is not in the
     * configuration parameters list, this method will throw an exception.
     * 
     * The value of the property can be any object. If the value is non string
     * object, its string value (Object.toString()) will be used as a value that
     * will be set on the configuration.
     * 
     * This method first validates whether all the paramters passed in
     * properties object exist in the runtime configuration or not. If any one
     * the parameters passed is not existing, it will return an error without
     * settings the parameters that are passed in the properties including a
     * valid parameters.
     * 
     * If there is an error in setting a paramter, this method throws an
     * exception with the list of parameters that were not set.
     * 
     * @param params
     *            Map object that contains name/value pairs corresponding
     *            to the configuration parameters to be set on the runtime.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return true if server restart is required, false if not
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error or a invalid parameter is passed in
     *             the params properties object. In case of an error setting the
     *             a particular parameter, the error message should list the
     *             invalid parameters.
     */
    public boolean setRuntimeConfiguration(Map<String /*attributeName*/, Object /*attributeValue*/> parameters,
            String targetName) throws ManagementRemoteException;    
            
            
    /**
     * Retrieve component configuration
     * 
     * @param componentName
     * @param targetName
     * @return the targetName as key and the name/value pairs as Map
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /*attributeName*/, Object /*attributeValue*/> getComponentConfigurationAsMap(String componentName,
            String targetName) throws ManagementRemoteException;            
            
    /**
     * Will return jbi mgmt message with success, failure, or partial success
     * per instance. The entry per instance will have value as part of the
     * management message (XML) String.
     * 
     * @param componentName
     * @param configurationValues
     * @param targetName
     * @return value as part of the management message (XML) String.
     * @throws ManagementRemoteException
     *             on error
     */
    public String setComponentConfiguration(String componentName,
            Map<String /*attributeName*/, Object /*attributeValue*/> configurationValues, String targetName)
            throws ManagementRemoteException;                
    
    /**
     * Invokes an operation on an Extension MBean.
     * @param componentName
     * @param extensionName the name of the extension (e.g., Configuration, Logger, etc.)
     * @param operationName The name of the operation to be invoked.
     * @param parameters An array containing the parameters to be set when the operation is invoked
     * @param signature An array containing the signature of the operation. The class objects will be loaded using the same class loader as the one used for loading the MBean on which the operation was invoked.
     * @param targetName name of the target (e.g., server, Cluster1, StandloneServer2, etc.)
     * @param targetInstanceName name of the target instance (e.g., Cluster1-Instance1, Cluster2-Instance10, etc.)
     * @return The object returned by the operation, which represents the result of invoking the operation on the Extension MBean specified.
     * @throws ManagementRemoteException
     */
    public Object invokeExtensionMBeanOperation(String componentName, String extensionName, String operationName, Object[] parameters, String[] signature, String targetName, String targetInstanceName) throws ManagementRemoteException;
    
}
