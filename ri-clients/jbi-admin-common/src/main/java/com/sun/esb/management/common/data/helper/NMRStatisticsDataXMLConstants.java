/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRStatisticsDataXMLConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.Serializable;

/**
 * @author graj
 * 
 */
public interface NMRStatisticsDataXMLConstants extends Serializable {
    // XML TAGS
    public static final String VERSION_KEY                  = "version";
    
    public static final String VERSION_VALUE                = "1.0";
    
    public static final String NAMESPACE_KEY                = "xmlns";
    
    public static final String NAMESPACE_VALUE              = "http://java.sun.com/xml/ns/esb/management/NMRStatisticsData";
    
    public static final String NMR_STATISTICS_DATA_LIST_KEY = "NMRStatisticsDataList";
    
    public static final String NMR_STATISTICS_DATA_KEY      = "NMRStatisticsData";
    
    public static final String INSTANCE_NAME_KEY            = "InstanceName";
    
    public static final String ACTIVE_CHANNELS_LIST_KEY     = "ActiveChannelsList";
    
    public static final String ACTIVE_CHANNEL_KEY           = "ActiveChannel";
    
    public static final String ACTIVE_ENDPOINTS_LIST_KEY    = "ActiveEndpointsList";
    
    public static final String ACTIVE_ENDPOINT_KEY          = "ActiveEndpoint";
    
}
