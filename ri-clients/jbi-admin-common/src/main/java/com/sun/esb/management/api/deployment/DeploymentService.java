/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.api.deployment;

import java.util.Map;

import com.sun.esb.management.common.ManagementRemoteException;

/**
 * Defines operations for common deployment/undeployment services.
 * 
 * @author graj
 */
public interface DeploymentService {
    /**
     * deploys service assembly
     * 
     * @return result as a management message xml text
     * @param zipFilePath
     *            file path
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     */
    public String deployServiceAssembly(String zipFilePath, String targetName)
            throws ManagementRemoteException;
    
    /**
     * deploys service assembly from domain target
     * 
     * @param assemblyName
     *            service assembly name
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     */
    public String deployServiceAssemblyFromDomain(String assemblyName,
            String targetName) throws ManagementRemoteException;
    
    /**
     * forcibly undeploys service assembly with option to retain it in domain
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            boolean forceDelete, boolean retainInDomain, String targetName)
            throws ManagementRemoteException;
    
    /**
     * forcibly undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            boolean forceDelete, String targetName)
            throws ManagementRemoteException;
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException;
    
    // ///////////////////////////////////////////
    // Start of Cumulative Operation Definitions
    // ///////////////////////////////////////////
    /**
     * deploys service assembly
     * 
     * @param zipFilePath
     *            fie path
     * @param targetNames
     * @return result as a management message xml text [targetName,xmlString]
     *         map
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String, String> deployServiceAssembly(String zipFilePath,
            String[] targetNames) throws ManagementRemoteException;
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @return result as a management message xml text as [targetName, string]
     *         map
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> undeployServiceAssembly(
            String serviceAssemblyName, String[] targetNames)
            throws ManagementRemoteException;
    
    /**
     * deploys service assembly
     * 
     * @param assemblyName
     *            name of the service assembly
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and management message xml text strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> deployServiceAssemblyFromDomain(
            String assemblyName, String[] targetNames)
            throws ManagementRemoteException;
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> undeployServiceAssembly(
            String serviceAssemblyName, boolean forceDelete,
            String[] targetNames) throws ManagementRemoteException;
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> undeployServiceAssembly(
            String serviceAssemblyName, boolean forceDelete,
            boolean retainInDomain, String[] targetNames)
            throws ManagementRemoteException;
    
    // ///////////////////////////////////////////
    // End of Cumulative Operation Definitions
    // ///////////////////////////////////////////
    
}
