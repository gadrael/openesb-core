/* 
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ProvisioningEndpointStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.Date;

/**
 * Provides Provisioning Endpoint Statistics
 * 
 * @author graj
 */
public class ProvisioningEndpointStatisticsData extends EndpointStatisticsData
        implements IEndpointStatisticsData, Serializable {
    static final long          serialVersionUID          = -1L;
    
    public static final String ACTIVATION_TIME_KEY       = "ActivationTime";
    
    public static final String UPTIME_KEY                =  "EndpointUpTime (ms)";    
    
    public static final String NUM_RECEIVED_REQUESTS_KEY = "NumReceivedRequests";
    
    public static final String NUM_SENT_REPLIES_KEY      = "NumSentReplies";
    
    public static final String ME_RESPONSE_TIME_AVG_KEY  = "MessageExchangeResponseTime Avg (ns)";
    
    protected Date             activationTime;
    
    protected long             uptime;
    
    protected long             numberOfReceivedRequests;
    
    protected long             numberOfSentReplies;
    
    protected long           messageExchangeResponseTimeAverage;
    
    /** Constructor - creates a ProvisioningEndpointStatisticsData object */
    public ProvisioningEndpointStatisticsData() {
        this.isProvisioningEndpoint = true;
    }
    
    /**
     * Constructor - creates a ProvisioningEndpointStatisticsData object
     * 
     * @param endpointData
     */
    public ProvisioningEndpointStatisticsData(
            EndpointStatisticsData endpointData) {
        super(endpointData);
        this.isProvisioningEndpoint = true;
        ProvisioningEndpointStatisticsData data = (ProvisioningEndpointStatisticsData) endpointData;
        this.setActivationTime(data.getActivationTime());
        this.setUptime(data.getUptime());
        this.setNumberOfReceivedRequests(data.getNumberOfReceivedRequests());
        this.setNumberOfSentReplies(data.getNumberOfSentReplies());
        this.setMessageExchangeResponseTimeAverage(data
                .getMessageExchangeResponseTimeAverage());
    }
    
    /**
     * @return the activationTime
     */
    public Date getActivationTime() {
        return this.activationTime;
    }
    
    /**
     * @param activationTime
     *            the activationTime to set
     */
    public void setActivationTime(Date activationTime) {
        this.activationTime = activationTime;
    }
    
    /**
     * @return the uptime
     */
    public long getUptime() {
        return this.uptime;
    }
    
    /**
     * @param uptime
     *            the uptime to set
     */
    public void setUptime(long uptime) {
        this.uptime = uptime;
    }
    
    /**
     * @return the numberOfReceivedRequests
     */
    public long getNumberOfReceivedRequests() {
        return this.numberOfReceivedRequests;
    }
    
    /**
     * @param numberOfReceivedRequests
     *            the numberOfReceivedRequests to set
     */
    public void setNumberOfReceivedRequests(long numberOfReceivedRequests) {
        this.numberOfReceivedRequests = numberOfReceivedRequests;
    }
    
    /**
     * @return the numberOfSentReplies
     */
    public long getNumberOfSentReplies() {
        return this.numberOfSentReplies;
    }
    
    /**
     * @param numberOfSentReplies
     *            the numberOfSentReplies to set
     */
    public void setNumberOfSentReplies(long numberOfSentReplies) {
        this.numberOfSentReplies = numberOfSentReplies;
    }
    
    /**
     * @return the messageExchangeResponseTimeAverage
     */
    public long getMessageExchangeResponseTimeAverage() {
        return this.messageExchangeResponseTimeAverage;
    }
    
    /**
     * @param messageExchangeResponseTimeAverage
     *            the messageExchangeResponseTimeAverage to set
     */
    public void setMessageExchangeResponseTimeAverage(
            long messageExchangeResponseTimeAverage) {
        this.messageExchangeResponseTimeAverage = messageExchangeResponseTimeAverage;
    }
    
    /**
     * Return a displayable string of values
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n    Activation Time" + "=" + this.getActivationTime());
        buffer.append("\n    Uptime" + "=" + this.getUptime());
        buffer.append("\n    Number Of Received Requests" + "="
                + this.getNumberOfReceivedRequests());
        buffer.append("\n    Number Of Sent Replies" + "="
                + this.getNumberOfSentReplies());
        buffer.append("\n    Message Exchange Response Time Average" + "="
                + this.getMessageExchangeResponseTimeAverage());
        buffer.append(super.getDisplayString());
        buffer.append("\n");
        return buffer.toString();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
