/* 
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provides Service Unit Statistics
 * 
 * @author graj
 * 
 */
public class ServiceUnitStatisticsData implements Serializable {
    static final long                          serialVersionUID      = -1L;
    
    public static final String                 NAME_KEY              = "ServiceUnitName";
    
    public static final String                 STARTUP_TIME_AVG_KEY  = "ServiceUnitStartupTime Avg (ms)";
    
    public static final String                 STOP_TIME_AVG_KEY     = "ServiceUnitStopTime Avg (ms)";
    
    public static final String                 SHUTDOWN_TIME_AVG_KEY = "ServiceUnitShutdownTime Avg (ms)";
    
    public static final String                 ENDPOINTS_KEY         = "Endpoints";
    
    protected String                           name;
    
    protected long                           startupTimeAverage;
    
    protected long                           stopTimeAverage;
    
    protected long                           shutdownTimeAverage;
    
    protected List<String /* endpointName */> endpointNameList      = new ArrayList<String /* endpointName */>();
    
    /** Constructor - creates a ServiceUnitStatisticsData object */
    public ServiceUnitStatisticsData() {
    }
    
    /**
     * @return the serviceUnitName
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * @param serviceUnitName
     *            the serviceUnitName to set
     */
    public void setName(String serviceUnitName) {
        this.name = serviceUnitName;
    }
    
    /**
     * @return the startupTimeAverage
     */
    public long getStartupTimeAverage() {
        return this.startupTimeAverage;
    }
    
    /**
     * @param startupTimeAverage
     *            the startupTimeAverage to set
     */
    public void setStartupTimeAverage(long serviceUnitStartTime) {
        this.startupTimeAverage = serviceUnitStartTime;
    }
    
    /**
     * @return the stopTimeAverage
     */
    public long getStopTimeAverage() {
        return this.stopTimeAverage;
    }
    
    /**
     * @param stopTimeAverage
     *            the stopTimeAverage to set
     */
    public void setStopTimeAverage(long serviceUnitStopTime) {
        this.stopTimeAverage = serviceUnitStopTime;
    }
    
    /**
     * @return the shutdownTimeAverage
     */
    public long getShutdownTimeAverage() {
        return this.shutdownTimeAverage;
    }
    
    /**
     * @param shutdownTimeAverage
     *            the shutdownTimeAverage to set
     */
    public void setShutdownTimeAverage(long serviceUnitShutdownTime) {
        this.shutdownTimeAverage = serviceUnitShutdownTime;
    }
    
    /**
     * @return the endpointNameList
     */
    public List<String> getEndpointNameList() {
        return this.endpointNameList;
    }
    
    /**
     * @return the endpointNameArray
     */
    public String[] getEndpointNameArray() {
        String[] result = toArray(this.endpointNameList, String.class);
        return result;
    }
    
    /**
     * @param endpointNameList
     *            the endpointNameList to set
     */
    public void setEndpointNameList(List<String> endpointNameList) {
        this.endpointNameList = endpointNameList;
    }
    
    /**
     * @param endpointNameArray
     *            the endpointNameArray to set
     */
    public void setEndpointNameList(String[] endpointNameArray) {
        for (String endpointName : endpointNameArray) {
            this.endpointNameList.add(endpointName);
        }
    }
    
    /**
     * Convert a collection to an array
     * 
     * @param collection
     * @param componentType
     * @return
     */
    @SuppressWarnings("unchecked")
    static protected <Type> Type[] toArray(Collection<Type> collection,
            Class<Type> componentType) {
        // unchecked cast
        Type[] array = (Type[]) java.lang.reflect.Array.newInstance(
                componentType, collection.size());
        int index = 0;
        for (Type value : collection) {
            array[index++] = value;
        }
        return array;
    }
    
    /**
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n    Service Unit Name" + "=" + this.getName());
        buffer.append("\n    Service Unit Shutdown Time" + "="
                + this.getShutdownTimeAverage());
        buffer.append("\n    Service Unit Start Time" + "="
                + this.getStartupTimeAverage());
        buffer.append("\n    Service Unit Stop Time" + "="
                + this.getStopTimeAverage());
        if ((this.getEndpointNameList() != null)
                && (this.getEndpointNameList().size() > 0)) {
            buffer.append("\n    Endpoints List:");
            for (String endpoint : this.getEndpointNameList()) {
                buffer.append("\n      Endpoint Name" + "=" + endpoint);
            }
        }
        buffer.append("\n\n");
        return buffer.toString();
        
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
