/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointInformation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;

/**
 * Provides Endpoint Information
 * 
 * @author graj
 */
public class EndpointInformation implements Serializable {
    static final long serialVersionUID = -1L;
    
    String            endpointName;
    
    String            serviceUnitName;
    
    String            componentName;
    
    String            status;
    
    /** missing application variables */
    String[]              missingAppVars;
    
    /** missing application configurations */
    String[]              missingAppConfigs;
    
    /**
     * 
     */
    public EndpointInformation() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * @return the endpointName
     */
    public String getEndpointName() {
        return this.endpointName;
    }
    
    /**
     * @param endpointName
     *            the endpointName to set
     */
    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }
    
    /**
     * @return the serviceUnitName
     */
    public String getServiceUnitName() {
        return this.serviceUnitName;
    }
    
    /**
     * @param serviceUnitName
     *            the serviceUnitName to set
     */
    public void setServiceUnitName(String serviceUnitName) {
        this.serviceUnitName = serviceUnitName;
    }
    
    /**
     * @return the componentName
     */
    public String getComponentName() {
        return this.componentName;
    }
    
    /**
     * @param componentName
     *            the componentName to set
     */
    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }
    
    /**
     * @return the status
     */
    public String getStatus() {
        return this.status;
    }
    
    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    /**
     * @return the missing application variables list
     */
    public String[] getMissingApplicationVariables() {
        return this.missingAppVars;
    }
    
    /**
     * Sets missing application variables
     * @param the list of missing application variables
     */
    public void setMissingApplicationVariables(String[] appvars) {
        this.missingAppVars = appvars;
    }
    
    /**
     * @return the missing application configuration list
     */
    public String[] getMissingApplicationConfigurations() {
        return this.missingAppConfigs;
    }
    
    /**
     * Sets missing application configuration list
     * @param appConfigs missing application configurations
     */
    public void setMissingApplicationConfigurations(String[] appConfigs) {
        this.missingAppConfigs = appConfigs;
    }
    
    
    /**
     * Return a displayable string of values
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n    Endpoint Name" + "=" + this.endpointName);
        buffer.append("\n    Service Unit Name" + "=" + this.getServiceUnitName());
        buffer.append("\n    Component Name" + "=" + this.getComponentName());
        buffer.append("\n    Status" + "=" + this.getStatus());

        if (this.getMissingApplicationVariables() != null && 
            this.getMissingApplicationVariables().length > 0)
        {
            String[] missingVars = this.getMissingApplicationVariables();
            buffer.append("\n    Missing Application Variables:\n");            
            for (String missingVar : missingVars) {
                buffer.append("\n\t " + missingVar);
            }
        }
        
        if (this.getMissingApplicationConfigurations() != null && 
            this.getMissingApplicationConfigurations().length > 0)
        {
            String[] missingConfigs = this.getMissingApplicationConfigurations();
            buffer.append("\n    Missing Application Configurations :\n");            
            for (String missingConfig : missingConfigs) {
                buffer.append("\n\t " + missingConfig);
            }
        }
        
        buffer.append("\n");
        return buffer.toString();
    }
    
    
}
