package net.openesb.security;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface AuthenticationToken {
    
    /**
     * Returns the account identity submitted during the authentication 
     * process.
     * 
     * @return the account identity submitted during the authentication process.
     */
    Object getPrincipal();
    
    /**
     * Returns the credentials submitted by the user during the authentication 
     * process that verifies the submitted {@link #getPrincipal() account 
     * identity}.
     * 
     * @return the credential submitted by the user during the authentication 
     * process.
     */
    Object getCredentials();
}
