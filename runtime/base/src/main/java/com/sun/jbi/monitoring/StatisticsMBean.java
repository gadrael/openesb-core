/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StatisticsMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.monitoring;

/**
 * This interface provides the common base for all of the statistics monitoring
 * MBean interfaces. The methods here are common to all statistics MBeans.
 *
 * @author Sun Microsystems, Inc.
 */
public interface StatisticsMBean
{
    /**
     * Test whether or not statistics collection is enabled.
     * @return true if statistics collection is enabled, false if not.
     */
    boolean isEnabled();

    /**
     * Disable statistics collection. This method causes collection for this
     * object and all its child objects to be disabled.
     */
    void setDisabled();

    /**
     * Enable statistics collection. This method causes collection for this
     * object and all its child objects to be enabled.
     */
    void setEnabled();
}
