/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventNotifierMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import javax.management.NotificationEmitter;
import javax.management.ObjectName;

/**
 * This interface defines the DAS event notifier MBean, which collects event
 * notifications from all instances within the domain, and re-emits them. This
 * allows a client to register as a notification listener with this MBean and
 * automatically receive notifications from all instances within the domain.
 * Event notifications are generated for all events affecting the state of the
 * runtime, such as startup and shutdown of the runtime, installation or
 * uninstallation of a component or shared library, deployment or undeployment
 * of a service assembly, and life cycle operations on components and service
 * assemblies.
 *
 * @author Mark S White
 */
public interface EventNotifierMBean
    extends NotificationEmitter
{
    /**
     * This method is called to disable event notifications.
     *
     * @return true if notifications were previously enabled, or false if
     * they were already disabled.
     */
    boolean disableNotifications();

    /**
     * This method is called to enable event notifications.
     *
     * @return true if notifications were previously disabled, or false if
     * they were already enabled.
     */
    boolean enableNotifications();

    /**
     * This method is called to inform the DAS that an instance has started
     * and it needs to register itself as a notification listener for the
     * event notifier MBean on that instance.
     *
     * @param instanceName the name of the instance that has started.
     * @param objectName the JMX object name of the event notifier MBean for
     * the instance.
     * @return true if the registration was successful, or false if not.
     */
    boolean instanceStarted(String instanceName, ObjectName objectName);

    /**
     * This method is called to inform the DAS that an instance has stopped
     * and it needs to unregister itself as a notification listener for the
     * event notifier MBean on that instance.
     *
     * @param instanceName the name of the instance that has stopped.
     * @param objectName the JMX object name of the event notifier MBean for
     * the instance.
     * @return true if the unregistration was successful, or false if not.
     */
    boolean instanceStopped(String instanceName, ObjectName objectName);
}
