/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventNotifierBase.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

/**
 * This interface defines enumerators and constants used for constructing
 * event notifications that are emitted by the runtime.
 *
 * @author Mark S White
 */
public interface EventNotifierBase
{
    /**
     * Events that generate notifications.
     */
    enum EventType {
        Installed,
        Upgraded,
        Uninstalled,
        Deployed,
        Undeployed,
        Ready,
        Started,
        Stopped,
        ShutDown
    }

    /**
     * Sources of notifications.
     */
    enum SourceType {
        JBIRuntime,
        BindingComponent,
        ServiceEngine,
        SharedLibrary, 
        ServiceUnit,
        ServiceAssembly
    }

    /**
     * Common prefix for all notification messages.
     */
    static final String JBI_PREFIX = "com.sun.jbi.";

    /**
     * Item name key for event type.
     */
    static final String EVENT_TYPE_KEY = "EventType";

    /**
     * Item name key for source type.
     */
    static final String SOURCE_TYPE_KEY = "SourceType";

    /**
     * Item name key for target name.
     */
    static final String TARGET_NAME_KEY = "TargetName";

    /**
     * Item name key for source type.
     */
    static final String SOURCE_NAME_KEY = "SourceName";

    /**
     * Item name key for component name.
     */
    static final String COMPONENT_NAME_KEY = "ComponentName";

    /**
     * Item name key for service assembly name.
.
     */
    static final String SERVICE_ASSEMBLY_NAME_KEY = "ServiceAssemblyName";

    /**
     * Item name key for service unit name.
     */
    static final String SERVICE_UNIT_NAME_KEY = "ServiceUnitName";

    /**
     * Notification types emitted by the runtime.
     */
    static final String[] NOTIFICATION_TYPES = {
        JBI_PREFIX + EventType.Ready + "." + SourceType.JBIRuntime,
        JBI_PREFIX + EventType.Started + "." + SourceType.JBIRuntime,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.JBIRuntime,
        JBI_PREFIX + EventType.Installed + "." + SourceType.SharedLibrary,
        JBI_PREFIX + EventType.Uninstalled + "." + SourceType.SharedLibrary,
        JBI_PREFIX + EventType.Installed + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.Started + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.ShutDown + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.Uninstalled + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.Installed + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.Started + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.ShutDown + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.Uninstalled + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.Deployed + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.Started + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.ShutDown + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.Undeployed + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.Deployed + "." + SourceType.ServiceUnit,
        JBI_PREFIX + EventType.Started + "." + SourceType.ServiceUnit,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.ServiceUnit,
        JBI_PREFIX + EventType.ShutDown + "." + SourceType.ServiceUnit,
        JBI_PREFIX + EventType.Undeployed + "." + SourceType.ServiceUnit
    };

    /**
     * Class name for notifications emitted by the runtime.
     */
    static final String NOTIFICATION_CLASS_NAME =
        "javax.management.Notification";

    /**
     * Desription for notifications emitted by the runtime.
     */
    static final String NOTIFICATION_DESCRIPTION =
        "JBI runtime event";
}
