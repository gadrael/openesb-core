/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import java.util.Properties;

/**
 * This ComponentInfo extends the base com.sun.jbi.ComponentInfo to 
 * provide access to the persisted component configuration, variables and named 
 * configuration
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentInfo
    extends com.sun.jbi.ComponentInfo
{
    /**
     * Application Variables.
     */
    public class Variable
    {
        /** name, value and type */
        private String mName;
        private String mValue;
        private String mType;
        
        public Variable(String name, String value, String type)
        {
            mName  = name;
            mType  = type;
            mValue = value;
        }
        
        /**
         * @return the name
         */
        public String getName()
        {
            return mName;
        }
        
        /**
         * @return the value
         */
        public String getValue()
        {
            return mValue;
        }
        
        /**
         * @return the type
         */
        public String getType()
        {
            return mType;
        }
        
        /**
         * @return truw if this variable is equal to the one being compared with.
         */
        public boolean equals(Variable var)
        {
            return ( var.getName().equals(mName)   &&
                     var.getValue().equals(mValue) &&
                     var.getType().equals(mType));
        }
        
    }
    
    /**
     * Get the Application Variables
     *
     * @return an array of Variables set on the component. If there are zero
     *         environment variables et on the component an empty array is returned.
     */ 
    public Variable[] getVariables();
    
    /**
     * Get the static component configuration.
     * 
     * @return the components constant configuration.
     */
    public Properties getConfiguration();
    
    /**
     * Get a specific application configuration.
     *
     * @return the application configuration whose name = configName. If one does not exist
     *         an empty properties object is returned.
     */
    public Properties getApplicationConfiguration(String name);
    
    /**
     * Get the names of all application configurations set on the component.
     *
     * @return a list of names of all application configurations. If there are zero
     *         application configurations set on the component an empty array is returned.
     */
    public String[] getApplicationConfigurationNames();

}
