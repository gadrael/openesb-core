/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingMessageReference.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * API for  WSDL 2.0 Binding message reference component.
 *
 * @author ApiGen AX.00
 */
public interface BindingMessageReference extends ExtensibleDocumentedComponent
{
    /**
     * Get message exchange pattern role identifier.
     *
     * @return Message exchange pattern role identifier
     */
    String getMessageLabel();

    /**
     * Set message exchange pattern role identifier.
     *
     * @param theMessageLabel Message exchange pattern role identifier
     */
    void setMessageLabel(String theMessageLabel);

    /**
     * Get direction of this message in the exchange.
     *
     * @return Direction of this message in the exchange
     */
    Direction getDirection();

    /**
     * Set direction of this message in the exchange.
     *
     * @param theDirection Direction of this message in the exchange
     */
    void setDirection(Direction theDirection);

}

// End-of-file: BindingMessageReference.java
