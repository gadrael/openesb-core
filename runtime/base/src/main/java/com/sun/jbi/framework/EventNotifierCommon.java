/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventNotifierCommon.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.EventNotifierBase;
import javax.management.Notification;

/**
 * This interface defines the methods that are used to emit notifications for
 * events affecting the state of JBI runtime. Methods are provided for emitting
 * notifications for all applicable events in the runtime, such as startup and
 * shutdown of the runtime, installation or uninstallation of a component or
 * shared library, deployment or undeployment of a service assembly or service
 * unit, and all life cycle operations on components, service assemblies, and
 * service units.
 *
 * This interface is implemented by the EventNotifier MBean in the runtime.
 *
 * @author Mark S White
 */
public interface EventNotifierCommon extends EventNotifierBase
{
    /**
     * This method is called to emit a notification for an event on the JBI
     * runtime. The caller provides information that determines the content of
     * the notification. The event type for a JBI runtime notification can be
     * Ready, Started, or Stopped. The source type is always JBIRuntime.
     * 
     * @param eventType the type of event that occurred.
     * @param msg a message associated with the event.
     * @return the actual Notification sent.
     */
    Notification emitRuntimeNotification(EventType eventType, String msg);

    /**
     * This method is called to emit a notification for an event on either a
     * binding component or a service engine. The caller provides information
     * that determines the content of the notification. The event type for a
     * component notification can be Installed, Uninstalled, Started, Stopped,
     * or ShutDown. The source type is either BindingComponent or ServiceEngine.
     * 
     * @param eventType the type of event that occurred.
     * @param sourceType the type of component on which the event occurred.
     * @param componentName the name of the component on which the event
     * occurred.
     * @param msg a message associated with the event.
     * @return the actual Notification sent.
     */
    Notification emitComponentNotification(EventType eventType,
        SourceType sourceType, String componentName, String msg);

    /**
     * This method is called to emit a notification for an event on a shared
     * library. The caller provides information that determines the content of
     * the notification. The event type for a shared library notification can be
     * either Installed or Uninstalled. The source type is always SharedLibrary.
     * 
     * @param eventType the type of event that occurred.
     * @param sharedLibraryName the name of the shared library on which the
     * event occurred.
     * @param msg a message associated with the event.
     * @return the actual Notification sent.
     */
    Notification emitSharedLibraryNotification(EventType eventType,
        String sharedLibraryName, String msg);

    /**
     * This method is called to emit a notification for an event on a service
     * assembly. The caller provides information that determines the content
     * of the notification. The event type for a service assembly notification
     * can be Deployed, Undeployed, Started, Stopped, or ShutDown. The source
     * type is always ServiceAssembly.
     * 
     * @param eventType the type of event that occurred.
     * @param serviceAssemblyName the name of the service assembly on which the
     * event occurred.
     * @param msg a message associated with the event.
     * @return the actual Notification sent.
     */
    Notification emitServiceAssemblyNotification(EventType eventType,
        String serviceAssemblyName, String msg);

    /**
     * This method is called to emit a notification for an event on a service
     * unit. The caller provides information that determines the content of the
     * notification. The event type for a service unit notification can be
     * Deployed, Undeployed, Started, Stopped, or ShutDown. The source type is
     * always ServiceUnit.
     * 
     * @param eventType the type of event that occurred.
     * @param serviceUnitName the name of the service unit on which the event
     * occurred.
     * @param serviceAssemblyName the name of the service assembly to which the
     * service unit belongs.
     * @param componentName the name of the component to which the service unit
     * is deployed.
     * @param msg a message associated with the event.
     * @return the actual Notification sent.
     */
    Notification emitServiceUnitNotification(EventType eventType,
        String serviceUnitName, String serviceAssemblyName,
        String componentName, String msg);
}
