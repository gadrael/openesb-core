/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import java.util.Map;
import java.util.Properties;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.management.MBeanException;

/**
 * The ComponentConfiguration is a dynamic facade MBean for component configuration.
 * This MBean provides a facade for managing component static, variable and
 * named configuration.
 *
 * This ComponentExtension MBean is registered for each installation of the component
 * on a target i.e it is target specific. 
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentConfiguration
    extends javax.management.DynamicMBean
{
    /**
     * Detect the components support for component configuration. This method
     * returns true if the component has a configuration MBean with
     * configuration attributes.
     *
     * @return true if the components configuration MBean has configuration
     *         attributes.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
    public boolean isComponentConfigSupported()  
        throws MBeanException;
    
    /*---------------------------------------------------------------------------------*\
     *          Operations for Application Variables Management                        *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application variables. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application variable management.
     *
     * @return true if the components configuration MBean implements all the
     *         operations for application variables.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppVarsSupported()  
        throws MBeanException;
     
    /**
     * This operation adds a new application variable. If a variable already exists with 
     * the same name as that specified then the operation fails.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste
     * @throws MBeanException if an error occurs in adding the application variables to the 
     *         component. 
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    String addApplicationVariable(String name, CompositeData appVar) throws MBeanException;
     
    /**
     * This operation sets an application variable. If a variable does not exist with 
     * the same name, its an error.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste to be updated.
     * @throws MBeanException if one or more application variables cannot be deleted
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    String setApplicationVariable(String name, CompositeData appVar) throws MBeanException; 
     
    /**
     * This operation deletes an application variable, if a variable with the specified name does
     * not exist, it's an error.
     *
     * @param name - name of the application variable
     * @throws MBeanException on errors.
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    String deleteApplicationVariable(String name) throws MBeanException;
     
     /**
      * Get the Application Variable set for a component.
      *
      * @return  a TabularData which has all the applicationvariables set on the component. 
      */
     TabularData getApplicationVariables();

    /*---------------------------------------------------------------------------------*\
     *            Operations for Application Configuration Management                  *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application configuration. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application configuration management.
     *
     * @return true if the components configuration MBean implements all the
     *         operations for application configuration.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppConfigSupported()  
        throws MBeanException;
     
     /**
      * Get the CompositeType definition for the components application configuration 
      *
      * @return the CompositeType for the components application configuration.
      */
     CompositeType queryApplicationConfigurationType();
     
      /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appCfgProps - application configuration properties
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     String addApplicationConfiguration(String name, Properties appCfgProps) throws MBeanException;
     
     /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appConfig - application configuration composite 
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     String addApplicationConfiguration(String name, CompositeData appConfig) throws MBeanException;
    
     /**
      * Delete an application configuration. 
      *
      * @param name - identification of the application configuration to be deleted
      * @throws MBeanException if the configuration cannot be deleted.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     String deleteApplicationConfiguration(String name) throws MBeanException;
     

     /**
      * Update a application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "configurationName" in the appConfig
      * @param appCfgProps - application configuration properties.
      * @throws MBeanException if there are errors encountered when updating the configuration.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     String setApplicationConfiguration(String name, Properties appCfgProps) throws MBeanException;
     
     /**
      * Update a application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "configurationName" in the appConfig
      * @param appConfig - application configuration composite
      * @throws MBeanException if there are errors encountered when updating the configuration.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     String setApplicationConfiguration(String name, CompositeData appConfig) throws MBeanException;
    
    /**
     * Get a Map of all application configurations for the component.
     *
     * @return a TabularData of all the application configurations for a 
     *         component keyed by the configuration name. 
     */
    TabularData getApplicationConfigurations(); 
    
    /*---------------------------------------------------------------------------------*\
     *            Operations Component Configuration meta-data Management              *
    \*---------------------------------------------------------------------------------*/
    
    /** 
     * Retrieves the component specific configuration schema.
     *
     * @return a String containing the configuration schema.
     * @throws MBeanException on errors.
     */
    public String retrieveConfigurationDisplaySchema() throws MBeanException;
    
    /** 
     * Retrieves the component configuration metadata.
     * The XML data conforms to the component 
     * configuration schema.
     *
     * @return a String containing the configuration metadata.
     * @throws MBeanException on errors
     */
    public String retrieveConfigurationDisplayData() throws MBeanException;
}
