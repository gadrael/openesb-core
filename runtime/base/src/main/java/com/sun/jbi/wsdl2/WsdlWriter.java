/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WsdlWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import org.w3c.dom.Document;

/**
 * This interface describes a collection of methods that allow a WSDL 2.0 
 * Description component to be written to a writer in an XML format that 
 * follows the WSDL 2.0 schema.
 *
 * Based on javax.wsdl.WSDLWriter, Matthew J. Duftler, IBM.
 *
 * @author Sun Microsystems, Inc.
 */
public interface WsdlWriter 
{
    /**
     * Return a document generated from the specified WSDL model.
     * 
     * @param model The WSDL Description component to be written
     * @return A DOM document that reflects the contents of the given model
     *
     * @exception WsdlException if a WSDL 2.0 validity error is encountered.
     */
    Document getDocument(Description model) throws WsdlException;

    /**
     * @deprecated use Document getDocument(Description model)
     */
    Document getDocument(Definitions model) throws WsdlException;

    /**
     * Write the specified WSDL Description to the specified Writer.
     *
     * @param model The WSDL Description component to be written
     * @param sink  The Writer to write the xml to
     *
     * @exception WsdlException if a WSDL 2.0 validity error is encountered.
     * @exception IOException   if an I/O errors while writing to the given
     *                          writer.
     */
    void writeDescription(Description model, Writer sink)
        throws WsdlException, IOException;

    /**
     * @deprecated use writeDescription
     */
    void writeWsdl(Definitions model, Writer sink)
        throws WsdlException, IOException;

    /**
     * Write the specified WSDL Description to the specified OutputStream.
     *
     * @param model The WSDL Description component to be written
     * @param sink  The OutputStream to write the XML to
     *
     * @exception WsdlException if a WSDL 2.0 validity error is encountered.
     * @exception IOException   if an I/O errors while writing to the given
     *                          output stream.
     */
    void writeDescription(Description model, OutputStream sink)
        throws WsdlException, IOException;

    /** 
     * @deprecated use writeDescription
     */
    void writeWsdl(Definitions model, OutputStream sink)
        throws WsdlException, IOException;
}
