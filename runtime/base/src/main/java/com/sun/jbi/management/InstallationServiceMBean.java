/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallationServiceMBean.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import javax.management.ObjectName;

/**
 * This InstallationServiceMBean extends the public InstallationService interface. 
 * This has additional operations to :
 * <br/>
 * <ul>
 * <li> Install a component from the repository </li>
 * <li> Install a shared library from the repository </li>
 * <li> Allow forceful uninstall of shared libraries </li>
 * </ul>
 * @author Sun Microsystems, Inc.
 */
public interface InstallationServiceMBean
    extends javax.jbi.management.InstallationServiceMBean
{
    /**
     * Load the InstallerMBean for a previously installed component.
     * <p>
     * The "component name" refers to the
     * <code>&lt;identification>&lt;name></code> element value from the
     * component's installation package (see {@link #loadNewInstaller(String)}).
     *
     * @param componentName the component name identifying the installer to
     *        load; must be non-null and non-empty
     * @param force set to <code>true</code> when this is being called as part
     *        of a forced uninstall, causing a failure to load the component's
     *        bootstrap class to be ignored.
     * @return the JMX ObjectName of the InstallerMBean loaded from an existing
     *         installation context; <code>null</code> if the installer MBean
     *         doesn't exist
     */
    ObjectName loadInstaller(String componentName, boolean force);
   
    /**
     * Load the installer for a component from the Repository. The component archive from 
     * the repository is uploaded to the target instance(s), loadNewInstaller() in
     * invoked on the remote instance(s). The object name of the Installer MBean 
     * is returned.
     * <br/>
     * If the component is not there in the DAS repository a exception will
     * be thrown. 
     * 
     * @param componentName - name of the registered component.
     * @return the ObjectName of Installer MBean for the registered component
     * @exception javax.jbi.JBIException if the component is not registered.
     */
     ObjectName loadInstallerFromRepository(String componentName)
        throws javax.jbi.JBIException;
     
    /**
     * Install a shared library from the Repository. The library archive from the 
     * repository is uploaded to the target instance(s) and installSharedLibrary() is 
     * invoked on the remote instance InstallationService with the uploaded file name.
     * <br/>
     * If the shared library is not there in the DAS repository an exception will
     * be thrown. 
     * 
     * @param sharedLibraryName - name of the registered shared library.
     * @return the shared library name
     * @exception javax.jbi.JBIException if the shared library is not registered or
     * uninstall fails.
     */
     String installSharedLibraryFromRepository(String sharedLibraryName)
        throws javax.jbi.JBIException;
     
     
    /**
     * Uninstall a previously installed shared library.
     *
     * @param slName the name of the shared name space to uninstall; must be
     *        non-null and non-empty
     * @param keep if true the shared libray is not deleted from the domain. 
     *        If false, the shared library is deleted from the repository 
     *        if after this uninstall it is not installed on any targets.
     * @return true if the uninstall was successful
     */
    boolean uninstallSharedLibrary(String slName, boolean keep);
    
   
   /**
    * Upgrade a component. This is used to perform an update of the runtime
    * files of a component without requiring undeployment of Service Assemblies
    * with Service Units deployed to the component.
    * @param componentName The name of the component.
    * @param installZipURL The URL to the component archive.
    * @return a status management message that contains component upgrade result 
    * @throws JBIException if there is a problem with the upgrade.
    */
    String upgradeComponent(String componentName, String installZipURL)
        throws javax.jbi.JBIException;

}
