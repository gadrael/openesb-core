/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageReference.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * API for  WSDL 2.0 Message Reference Component.
 *
 * @author ApiGen AX.00
 */
public interface MessageReference extends ExtensibleDocumentedComponent
{
    /**
     * Get message exchange pattern role label.
     *
     * @return Message exchange pattern role label
     */
    String getMessageLabel();

    /**
     * Set message exchange pattern role label.
     *
     * @param theMessageLabel Message exchange pattern role label
     */
    void setMessageLabel(String theMessageLabel);

    /**
     * Get content (String) or payload element type declaration (QName).
     *
     * @return Content (String) or payload element type declaration (QName)
     */
    Object getElement();

    /**
     * Set content (String) or payload element type declaration (QName).
     *
     * @param theElement Content (String) or payload element type declaration
     * (QName)
     */
    void setElement(Object theElement);

    /**
     * Get direction of this message in the exchange.
     *
     * @return Direction of this message in the exchange
     */
    Direction getDirection();

    /**
     * Set direction of this message in the exchange.
     *
     * @param theDirection Direction of this message in the exchange
     */
    void setDirection(Direction theDirection);

    /**
     * Get content model type: #ANY, #NONE, or #ELEMENT.
     *
     * @return Content model type: #ANY, #NONE, or #ELEMENT
     */
    MessageContentModel getMessageContentModel();

}

// End-of-file: MessageReference.java
