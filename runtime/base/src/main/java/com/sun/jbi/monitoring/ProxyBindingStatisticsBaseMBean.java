/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ProxyBindingStatisticsBaseMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.monitoring;
import java.util.Date;

/**
 * This interface defines the MBean for collection of global statistics for 
 * the Proxy Binding in a single JBI instance. 
 * All statistics are since the last JBI startup; they are all reset 
 * when JBI is restarted.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ProxyBindingStatisticsBaseMBean
{
    /**
     * Get the number of ME's sent by the PB for all JBI components
     * as consumers.
     * @return The current number of sent exchanges.
     */
    long getSentExchanges();


    /**
     * Get the number of ME's received by the PB for all JBI components
     * as providers.
     * @return The current number of received exchanges.
     */
    long getReceivedExchanges();

    /**
     * Get the total bytes sent by the PB for all JBI components
     * as consumers.
     * @return The current bytes sent by the PB.
     */
    long getSentBytes();


    /**
     * Get the total bytes received by the PB for all JBI components
     * as providers.
     * @return The current bytes received by the PB.
     */
    long getReceivedBytes();

    /**
     * Get the total messages sent by the PB for all JBI components
     * as consumers.
     * @return The current messages sent by the PB.
     */
    long getSentMessages();


    /**
     * Get the total messages received by the PB for all JBI components
     * as providers.
     * @return The current messages received by the PB.
     */
    long getReceivedMessages();

    /**
     * Get the total number of Message Faults created since the
     * last NMR startup.
     * @return The number of message faults.
     */
    long getMessageFaults();

    /**
     * Get the total number of Message Errors created since the
     * last NMR startup.
     * @return The number of message errors.
     */
    long getMessageErrors();

    /**
     * Get the total number of completed Message Exchanges created since the
     * last NMR startup.
     * @return The number of completed MEs.
     */
    long getCompletedExchanges();
}
