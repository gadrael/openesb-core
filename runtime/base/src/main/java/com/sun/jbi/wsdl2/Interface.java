/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Interface.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;
import java.net.URI;

/**
 * API for  WSDL 2.0 Interface Component.
 *
 * @author ApiGen AX.00
 */
public interface Interface extends ExtensibleDocumentedComponent
{
    /**
     * Get target namespace of this binding.
     *
     * @return Target namespace of this binding
     */
    String getTargetNamespace();

    /**
     * Get local name of this interface component.
     *
     * @return Local name of this interface component
     */
    String getName();

    /**
     * Set local name of this interface component.
     *
     * @param theName Local name of this interface component
     */
    void setName(String theName);

    /**
     * Get qualified name of this component.
     *
     * @return Qualified name of this component
     */
    QName getQName();

    /**
     * Get the number of Interface items in interfaces.
     *
     * @return The number of Interface items in interfaces
     */
    int getInterfacesLength();

    /**
     * Get interfaces this interface extends, if any by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Interfaces this interface extends, if any at given
     * <code>index</code> position.
     */
    Interface getInterface(int index);

    /**
     * Set interfaces this interface extends, if any by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theInterface Item to add at position <code>index</code>.
     */
    void setInterface(int index, Interface theInterface);

    /**
     * Append an item to interfaces this interface extends, if any.
     *
     * @param theInterface Item to append to interfaces
     */
    void appendInterface(Interface theInterface);

    /**
     * Remove interfaces this interface extends, if any by index position.
     *
     * @param index The index position of the interface to remove
     * @return The Interface removed, if any.
     */
    Interface removeInterface(int index);

    /**
     * Get the number of InterfaceFault items in faults.
     *
     * @return The number of InterfaceFault items in faults
     */
    int getFaultsLength();

    /**
     * Get faults that may occur executing operations of this interface by
     * indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Faults that may occur executing operations of this interface
     * at given <code>index</code> position.
     */
    InterfaceFault getFault(int index);

    /**
     * Set faults that may occur executing operations of this interface by
     * indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theFault Item to add at position <code>index</code>.
     */
    void setFault(int index, InterfaceFault theFault);

    /**
     * Append an item to faults that may occur executing operations of this
     * interface.
     *
     * @param theFault Item to append to faults
     */
    void appendFault(InterfaceFault theFault);

    /**
     * Remove faults that may occur executing operations of this interface by
     * index position.
     *
     * @param index The index position of the fault to remove
     * @return The InterfaceFault removed, if any.
     */
    InterfaceFault removeFault(int index);

    /**
     * Get the default styles used to construct the {element} properties of
     * {message references} of all operations contained in this interface.
     *
     * @return The default styles used to construct the {element} properties
     * of {message references} of all operations contained in this interface
     */
    URI[] getStyleDefault();

    /**
     * Set the default styles used to construct the {element} properties of
     * {message references} of all operations contained in this interface.
     *
     * @param theStyleDefault The default styles used to construct the
     * {element} properties of {message references} of all operations
     * contained in this interface
     */
    void setStyleDefault(URI[] theStyleDefault);

    /**
     * Get the number of InterfaceOperation items in operations.
     *
     * @return The number of InterfaceOperation items in operations
     */
    int getOperationsLength();

    /**
     * Get operations defined by this interface only by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return operations defined by this interface only at given
     * <code>index</code> position.
     */
    InterfaceOperation getOperation(int index);

    /**
     * Set operations defined by this interface only by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theOperation Item to add at position <code>index</code>.
     */
    void setOperation(int index, InterfaceOperation theOperation);

    /**
     * Append an item to operations defined by this interface only.
     *
     * @param theOperation Item to append to operations
     */
    void appendOperation(InterfaceOperation theOperation);

    /**
     * Remove operations defined by this interface only by index position.
     *
     * @param index The index position of the operation to remove
     * @return The InterfaceOperation removed, if any.
     */
    InterfaceOperation removeOperation(int index);

    /**
     * Get the interfaces extended by this interface, including those
     * extended by this interface's super-interfaces.
     *
     * @return Extended interfaces, including those extended by this
     * interface's super-interfaces
     */
    Interface[] getExtendedInterfaces();

    /**
     * Get the operations defined by this interface, and all its
     * super-interfaces.
     *
     * @return Operations defined by this interface, and all super-interfaces
     */
    InterfaceOperation[] getExtendedOperations();

    /**
     * Create a new operation, appending it to this interface's operations
     * list.
     *
     * @return Newly created operation, appended to the operations list.
     */
    InterfaceOperation addNewOperation();

    /**
     * Create a new fault, and append it to this interface's faults list.
     *
     * @param name Name of the new fault
     * @return Newly created interface, appended to the faults list.
     */
    InterfaceFault addNewFault(
        String name);
}

// End-of-file: Interface.java
