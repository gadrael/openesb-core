/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 8, 2004, 6:28 PM
 */

package com.sun.jbi.binding.security;

/**
 * The SecurityHandler interface will be used a Binding Component to
 * secure outgoing Messages and validate incoming messages. There is a 1:1 association 
 * between a Binding Component and a Security Handler.
 *
 * @author Sun Microsystems, Inc.
 */
public interface SecurityHandler 
{   
    /**
     * Get an Instance of the Deployment Listener.
     *
     * @return an Instance of the Deployment Listener
     */
    DeploymentListener getDeploymentListener();

    /**
     * Get an Instance of the Interceptor.
     *
     * @return an Instance of the Deployment Listener
     */
    Interceptor getInterceptor();

    /**
     *
     * Get the HttpSecurityHandler.
     *
     * @return a new Instance of the HttpSecurityHandler
     */
    HttpSecurityHandler getHttpSecurityHandler();

    /** 
     * Get the Name of the Component associated with the handler.
     *
     * @return the String Component Name.
     */
    String getComponentName();
}
