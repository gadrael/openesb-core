package com.sun.jbi.messaging;

import javax.xml.namespace.QName;

/**
 * @author DavidD
 * Used on start and shutdown of the service assembly 
 * to activate and deactivate the new Interface system.
 */
public interface Interface2Manager {
    
    /**
     * Register a new NonLinkedConsumer. 
     * Also create the route type entry if necessary
     * @param service  -> the service of the endpoint to register
     * @param endpointName  -> the name of the endpoint to register
     * @param consumedInterface  -> the interface for the service consumed by the consumer
     * @return true if a new entry has been created, false if the endpoint already exists
     */
    public boolean registerNonLinkedConsumer(QName service, String endpointName, QName consumedInterface);
    
    /**
     * Unregister a NonLinkedConsumer :
     * remove the endpoint from the maps and also remove the interface consumed
     * remove the route type if still set
     * @param service  -> the service name of the endpoint to remove
     * @param endpointName  -> the name of the endpoint to remove
     * @return true if a consumer has been unregistered
     */
    public boolean unregisterNonLinkedConsumer(QName service, String endpointName);
    
    /**
     * Set the route type for a consumer. ALL consumers must have a route type.  
     * (does not check if the consumer exists) 
     * @param service -> the service of the consumer to set.
     * @param endpointName  -> the name of the consumer to set.
     * @param routeType -> the route type to set. 'direct', 'indirect', 'redirect' are supported.
     * @return false if the endpoint has already a route type set
     * @throws IllegalArgumentException if the route type is not supported.
     */
    public boolean setRouteType(QName service, String endpointName, String routeType);
    
    /**
     * Set the way that the provider is chosen among all of those implementing the consumed interface
     * Overrides the existing rule if any.
     * @param service   -> the service of the consumer 
     * @param endpointName  -> the name of the consumer
     * @param routingRule   -> the applied rule. 'default','random' are supported ('load-balancing' is random)
     * @return false if a rule has been overridden, true if not.
     */
    public boolean setRoutingRule(QName service, String endpointName, String routingRule);
    
    /**
     * Specifies if the consumer can consume a provider outside the JVM or not.
     * Overrides the existing mode if any.
     * @param service   -> the service of the consumer 
     * @param endpointName  -> the name of the consumer
     * @param mode   -> the applied rule. 'local','distributed' are supported
     * @return false if a mode has been overridden, true if not.
     */
    public boolean setMode(QName service, String endpointName, String mode);
    
    /**
     * Unset the route type of a consumer (for deactivation).
     * Also remove the routing rule and the mode if set.
     * @param service   -> the service of the consumer
     * @param endpointName  -> the name of the consumer
     * @return true if a route type has been removed, false if not.
     */
    public boolean removeRouteType(QName service, String endpointName);
    
    /**
     * Specifies that the given provider is implementing the given interface.
     * @param providerName  -> the name of the provider
     * @param providerService   -> the service of the provider
     * @param implementedInterface  -> the interface implemented by the provider
     * @return false if the provider already implements the given interface, true if not.
     */
    public boolean addImplementedInterface(String providerName, QName providerService, QName implementedInterface);
    
    /**
     * Specifies that the given provider is no longer implementing the given interface (for deactivation).
     * @param providerName  -> the name of the provider
     * @param providerService   -> the service of the provider
     * @param implementedInterface  -> the interface implemented by the provider
     * @return true if the implemented interface by the provider has been removed, false if not.
     */
    public boolean removeImplementedInterface(String providerName, QName providerService, QName implementedInterface);
    
    /**
     * Specifies the interface of the service consumed by a consumer. 
     * Should be called on for redirect linked endpoint.
     * (does not check if the consumer exists)
     * @param consumerService   -> the service of the consumer
     * @param consumerName  -> the name of the consumer
     * @param consumedInterface -> the interface of the service consumed by the consumer
     * @return true if a new entry has been created, 
     * false if the entry has overridden an already existing interface for this consumer
     */
    public boolean addConsumedInterface(QName consumerService, String consumerName, QName consumedInterface);
    
    /**
     * Specifies that the given consumer no longer consumes its interface-defined service.
     * @param consumerService   -> the service of the consumer
     * @param consumerName  -> the name of the consumer
     * @return true if an entry has been remove, false if the given consumer had no consumed interface.
     */
    public boolean removeConsumedInterface(QName consumerService, String consumerName);
    
}
