/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

import java.util.List;
import java.util.Map;

/**
 * This interface provides information on components (Binding Components,
 * Service Engines, and Shared Libraries.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentInfo
{
    // Public methods

    /**
     * Determine if the supplied Object is equal to this one.
     * @param object - the Object to be compared with this one.
     * @return true if the supplied Object is equal to this one.
     */
    boolean equals(Object object);

    /**
     * Get the class path elements. For BCs and SEs this is the list of elements
     * that comprise the runtime class path; for Shared Libraries it is the list
     * of elements that comprise the shared class path. Each element in the list
     * is a String representing a single jar file or directory.
     * @return List of jar files and directories that comprise the class path
     * for the component or shared library.
     */ 
    List<String> getClassPathElements();

    /**
     * Get the component class name. For BCs and SEs this is the name of
     * the class that implements the
     * <CODE>javax.jbi.component.Component</CODE>
     * interface; for Shared Libraries it is null.
     * @return the name of the Component class.
     */
    String getComponentClassName();

    /**
     * Get the component type.
     * @return the component type: ComponentInfo.BINDING, ComponentInfo.ENGINE,
     * or ComponentInfo.SHARED_LIBRARY.
     */
    ComponentType getComponentType();

    /**
     * Get the description of the component.
     * @return the String describing the component.
     */
    String getDescription();

    /**
     * Get the directory into which this component is installed.
     * @return the directory into which this component is installed.
     */
    String getInstallRoot();
    
    /**
     * Get the workspace for the component
     * @return this components workspace directory.
     */
    String getWorkspaceRoot();

    /**
     * Get the name of the component.
     * @return the name of the component.
     */
    String getName();

    /**
     * Get the list of ServiceUnitInfo objects representing all the SUs that
     * are currently deployed to this component; for Shared Libraries this is
     * null.
     * @return List of ServiceUnitInfo objects.
     */
    List<ServiceUnitInfo> getServiceUnitList();

    /**
     * Get the list of Shared Libraries required by this component. For BCs
     * and SEs, this is the list of Shared Library names in the order that they
     * appear in the class loader chain; for Shared Libraries this is null.
     * @return List of Strings containing Shared Library names.
     */
    List<String> getSharedLibraryNames();

    /**
     * Get the Status of the Component. For Shared Libraries this is always
     * INSTALLED.
     * @return should be of the values: LOADED, INSTALLED, STOPPED and STARTED
     */
    ComponentState getStatus();

    /**
     * Get a hash code for this ComponentInfo.
     * @return the hash code for the object.
     */
    int hashCode();
    
    /**
     * Get the installation specific properties for the component.
     *
     * @return a Map of the properties
     */
    Map<String, String> getProperties();
    
    /**
     * @return true/false depending on the value of the bootstrapClassLoaderSelfFirst 
     * attribute. This method returns false for shared libraries.
     */
    boolean isBootstrapClassLoaderSelfFirst();
    
    /**
     * @return true/false depending on the value of the {Component}ClassLoaderSelfFirst 
     * attribute.
     */
    boolean isClassLoaderSelfFirst();

   /**
    * Get the class path elements that this BC or SE needs in its bootstrap
    * runtime environment.
    * @return A list of the elements of the bootstrap class path as strings.
    */
    List<String> getBootstrapClassPathElements();
    
   /**
    * Get the class name of the bootstrap implementation for this BC or SE.
    * @return The bootstrap class name.
    */
    String getBootstrapClassName();
    
    /**
     * Get the Installation Descriptor ( jbi.xml ) for the Component / Shared Library.
     *
     * @return the jbi.xml as a String
     */
    String getInstallationDescriptor();
}
