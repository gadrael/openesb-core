/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)VersionInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

/**
 * VersionInfo supplies build-specific product version information.
 *
 * @author Sun Microsystems, Inc.
 */
public interface VersionInfo
{
    /**
     * @return the full product name.
     */
    String fullProductName();

    /**
     * @return the short product name.
     */
    String shortProductName();

    /**
     * @return the major version number.
     */
    String majorVersion();

    /**
     * @return the minor version number.
     */
    String minorVersion();

    /**
     * @return the build number for this version.
     */
    String buildNumber();

    /**
     * @return the Copyright for this version.
     */
    String copyright();
}
