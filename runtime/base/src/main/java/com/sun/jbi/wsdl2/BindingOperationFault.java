/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingOperationFault.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;

/**
 * API for  WSDL 2.0 Fault Message Reference Component.
 *
 * @author ApiGen AX.00
 */
public interface BindingOperationFault extends ExtensibleDocumentedComponent
{
    /**
     * Get fault component describing the fault referred to by the component.
     *
     * @return Fault component describing the fault referred to by the
     * component
     */
    QName getRef();

    /**
     * Set fault component describing the fault referred to by the component.
     *
     * @param theRef Fault component describing the fault referred to by the
     * component
     */
    void setRef(QName theRef);

    /**
     * Get message exchange pattern role label.
     *
     * @return Message exchange pattern role label
     */
    String getMessageLabel();

    /**
     * Set message exchange pattern role label.
     *
     * @param theMessageLabel Message exchange pattern role label
     */
    void setMessageLabel(String theMessageLabel);
}

// End-of-file: BindingOperationFault.java
