/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

import java.util.List;

/**
 * This interface provides information on Service Assemblys.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ServiceAssemblyInfo
{

    /**
     * Determine if the supplied Object is equal to this one.
     * @param object - the Object to be compared with this one.
     * @return true if the supplied Object is equal to this one.
     */
    boolean equals(Object object);
    
    /**
     * Get a hash code for this ServiceAssemblyInfo.
     * @return the hash code for the object.
     */
    int hashCode();

    /**
     * Get the name of the Service Assembly.
     * @return the name of the Service Assembly.
     */
    String getName();
    
    /**
     * Get a List<ServiceUnitInfo> of all service units in this service assembly 
     * @return the Service Unit Info List.
     */
    List<ServiceUnitInfo> getServiceUnitList();

    /**
     * Get the state of the Service Assembly.
     * @return current state, one of {SHUTDOWN, STOPPED, STARTED, UNKNOWN}
     */
    ServiceAssemblyState getStatus();
}
