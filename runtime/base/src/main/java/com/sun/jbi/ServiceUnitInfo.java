/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

/**
 * This interface provides information on Service Units.
 * @author Sun Microsystems, Inc.
 */
public interface ServiceUnitInfo
{

    /**
     * Determine if the supplied Object is equal to this one.
     * @param object - the Object to be compared with this one.
     * @return true if the supplied Object is equal to this one.
     */
    boolean equals(Object object);

    /**
     * Get the name of the Service Unit.
     * @return the name of the Service Unit.
     */
    String getName();

    /**
     * Get the name of the Service Assembly containing this Service Unit.
     * @return the name of the Service Assembly.
     */
    String getServiceAssemblyName();

    /**
     * Get the state of the Service Unit.
     * @return current state
     */
    ServiceUnitState getState();

    /**
     * Get the state of the Service Unit as a string.
     * @return current state, as one of the values: "shutdown", "stopped",
     * "started".
     */
    String getStateAsString();
    
    /**
     * Get the Service Units target component
     *
     * @return the target component name
     */
    String getTargetComponent();
    
    /**
     * Get the path to the Service Unit archive.
     * @return the file path.
     */
    public String getFilePath();
}
