/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TimeOutException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * TimeOutException is a subclass of JBIException that is used for exceptions
 * that indicate a call to a method in a Component has timed out.
 *
 * @author Sun Microsystems
 */

public class TimeOutException extends javax.jbi.JBIException
{
    /**
     * Creates a new instance of TimeOutException with an exception
     * message.
     * @param aMessage String describing this exception.
     */
    public TimeOutException(String aMessage)
    {
        super(aMessage);
    }

    /**
     * Creates a new instance of TimeOutException with the specified
     * message and cause.
     * @param aMessage String describing this exception.
     * @param aCause Throwable which represents an underlying problem
     * (or null).
     */
    public TimeOutException(String aMessage, Throwable aCause)
    {
        super(aMessage, aCause);
    }

   /**
    * Creates a new instance of TimeOutException with the specified
    * cause.
    * @param aCause Throwable which represents an underlying problem
    * (or null).
    */
    public TimeOutException(Throwable aCause)
    {
        super(aCause);
    }
}
