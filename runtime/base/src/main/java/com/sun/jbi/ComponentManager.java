/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentManager.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

import java.util.List;

import javax.management.ObjectName;

/**
 * This is the public interface to the Component Framework, which provides
 * installation and management of Binding Components, Service Engines, and
 * and Shared Libraries within the JBI environment.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentManager
{
   /**
    * Get the javax.jbi.component.Component instance for the component with
    * the specified name. If no instance exists, returns null.
    * @param componentName The unique name of the component.
    * @return The instance of javax.jbi.component.Component or null if none
    * is found.
    */
    javax.jbi.component.Component getComponentInstance(String componentName);

   /**
    * Get the com.sun.jbi.framework.DeployerMBean instance for the component
    * with the specified name. If no instance exists, returns null.
    * @param componentName The unique name of the component.
    * @return The instance of com.sun.jbi.framework.DeployerMBean or null if
    * none is found.
    */
    com.sun.jbi.framework.DeployerMBean getDeployerInstance(String componentName);

   /**
    * Install a Shared Library into the JBI framework.
    * @param name The unique name of the Shared Library.
    * @param description The description of the Shared Library.
    * @param componentRoot The root directory for this Shared Library.
    * @param isSelfFirst Set to true to force the class loader for this Shared
    * Library to use a self-first hierarchy, or false for parent-first.
    * @param elements The list of jar files and class directories included in
    * this Shared Library, as a List of String objects in the order in which
    * they should appear in the class path. Each list element contains the full
    * path to either a jar file or a directory containing class files.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    void installSharedLibrary(
        String name,
        String description,
        String componentRoot,
        boolean isSelfFirst,
        List elements)
        throws javax.jbi.JBIException;

   /**
    * Load a component's bootstrap into the JBI framework. This is the first
    * step in the installation and uninstallation processes. It creates and
    * registers the InstallerMBean that is used to complete the installation
    * or uninstallation.
    * @param installContext The installation context.
    * @param bootClassName The name of the bootstrap class for the component.
    * @param bootClassPathElements A list of elements comprising the class
    * path for loading the bootstrap class. Each element in the list is a
    * String containing the full path to either a jar file or a directory
    * containing class files.
    * @param sharedLibraryList A list of String objects specifing the names of
    * the Shared Libraries required by the component, in the order in which
    * they should appear in the class path.
    * @param force Set to <code>true</code> if this is for a forced uninstall.
    * @return The JMX object name of the installation configuration MBean
    * provided by the component, if any is provided, or null otherwise.
    * @throws javax.jbi.JBIException if the bootstrap cannot be loaded.
    */
    ObjectName loadBootstrap(
        com.sun.jbi.component.InstallationContext installContext,
        String bootClassName,
        List bootClassPathElements,
        List sharedLibraryList,
        boolean force)
        throws javax.jbi.JBIException;

   /**
    * Uninstall a Shared Library from the JBI framework. A Shared Library
    * cannot be uninstalled until all dependent components (BCs and SEs) have
    * been shut down. If any active dependent components are found, the
    * uninstall of the Shared Library is aborted.
    * @param id The component ID of the Shared Library.
    * @throws javax.jbi.JBIException if the uninstall fails.
    */
    void uninstallSharedLibrary(
        String id)
        throws javax.jbi.JBIException;

   /**
    * Unload a component's bootstrap from the JBI framework. This is the last
    * step in the installation and uninstallation processes. It unregisters
    * the InstallerMBean that was used to complete the installation or
    * uninstallation, and calls the bootstrap cleanUp() method if it has not
    * yet been called. Note that in the case where the this method is called
    * after loadBootstrap() for a new component, without an intervening call to
    * installComponent(), the Component instance is removed here.
    * @param componentName The unique name of the component.
    * @throws javax.jbi.JBIException if the bootstrap cannot be unloaded.
    */
    void unloadBootstrap(
        String componentName)
        throws javax.jbi.JBIException;

    /**
     * Cancel a pending component upgrade. This is called when some failure
     * has occurred after <code>validateComponentForUpgrade()</code> has already
     * been called but before <code>upgradeComponent()</code> is called. All
     * this method does is return the component to the <code>SHUTDOWN</code>
     * state and remove the busy indicator from the component entry in the
     * runtime registry cache.
     * @param componentName The name of the component.
     * @throws javax.jbi.JBIException if the component does not exist.
     */
    void cancelComponentUpgrade(
        String componentName)
        throws javax.jbi.JBIException;

   /**
    * Upgrade an installed component. This is used to upgrade a component to a
    * newer version without requiring the component to be uninstalled (which
    * requires undeployment of all Service Assemblies that have Service Units
    * deployed to the component). There are two ways a component can be updated.
    * If the component does not provide an <code>upgrade()</code> method in its
    * bootstrap class, then the runtime jar files are updated and any changes
    * to the component's installation descriptor in jbi.xml are propagated. If
    * the component provides an <code>upgrade()</code> method, that method is
    * called to give the component the opportunity to upgrade its workspace
    * and all SUs deployed to it to a new version. In this case, it is the
    * responsibility of the component to perform all version verification, and
    * to provide any recovery processing required in the event of a failed
    * upgrade. 
    * 
    * @param installContext The installation context.
    * @param bootClassName The name of the component's bootstrap class.
    * @param bootClassPathElements A list of elements comprising the class
    * path for loading the bootstrap class. Each element in the list is a
    * <code>String</code> containing the full path to either a jar file or a
    * directory containing class files.
    * @param sharedLibraryList A list of <code>String</code> objects specifing
    * the names of the Shared Libraries required by the component, in the order
    * in which they should appear in the class path.
    * @throws javax.jbi.JBIException if the update fails for some reason.
    */
    void upgradeComponent(
        com.sun.jbi.component.InstallationContext installContext,
        String bootClassName,
        List<String> bootClassPathElements,
        List<String> sharedLibraryList)
        throws javax.jbi.JBIException;

   /**
    * Validate a component for upgrade. This validates that the runtime jar
    * files provided for the upgrade contain valid implementations of the
    * required interfaces and the component classes will load correctly. It
    * also verifies that all Shared Library dependencies are met. Finally, it
    * determines whether or not the component has provided an implementation
    * of the <code>upgrade()</code> method and returns an indicator of that to
    * the caller.
    *
    * @param installContext The installation context.
    * @param bootClassName The name of the bootstrap class for the component.
    * @param bootClassPathElements A list of elements comprising the class
    * path for loading the bootstrap class. Each element in the list is a
    * <code>String</code> containing the full path to either a jar file or a
    * directory containing class files.
    * @param sharedLibraryList A list of <code>String</code> objects specifing
    * the names of the Shared Libraries required by the component, in the order
    * in which they should appear in the class path.
    * @return <code>true</code> if the component provided an <code>upgrade()
    * </code> method, <code>false</code> if not.
    * @throws javax.jbi.JBIException if there is a problem with the component
    * implementation classes or with Shared Library dependencies.
    */
    boolean validateComponentForUpgrade(
        com.sun.jbi.component.InstallationContext installContext,
        String bootClassName,
        List<String> bootClassPathElements,
        List<String> sharedLibraryList)
        throws javax.jbi.JBIException;
}
