/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyQuery.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

import java.util.List;

/**
 * This interface provides services for query of information about Binding
 * Components, Service Engines, and Shared Libraries.
 * 
 * @author Sun Microsystems, Inc.
 */
public interface ServiceAssemblyQuery
{
   /**
    * Get a list of all deployed service assembly names.
    *
    * @return A List<String> of all deployed service assemblies.
    */
    List<String> getServiceAssemblies();
 
   /**
    * Get the ServiceAssemblyInfo for a particular Service Assembly.
    * @param serviceAssemblyName The unique name of the service assembly being retrieved.
    * @return The ServiceAssemblyInfo for the requested service assembly or null if the
    * service assembly is not registered.
    */
    ServiceAssemblyInfo getServiceAssemblyInfo(String serviceAssemblyName);

   /**
    * Get the current status of a service assembly.
    *
    * @param serviceAssemblyName The unique service assembly name.
    * @return The current status of the service assembly: ServiceAssemblyState 
    *         {SHUTDOWN, STOPPED, STARTED, UNKNOWN}
    * @throws javax.jbi.JBIException if the service assembly is not registered.
    */
    ServiceAssemblyState getStatus(String serviceAssemblyName)
        throws javax.jbi.JBIException;
}
