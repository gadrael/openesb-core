/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Description.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;

/**
 * API for  WSDL 2.0 Description container.
 *
 * @author ApiGen AX.00
 */
public interface Description extends ExtensibleDocumentedComponent
{
    /**
     * Get the number of Binding items in bindings.
     *
     * @return The number of Binding items in bindings
     */
    int getBindingsLength();

    /**
     * Get bindings for this component by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Bindings for this component at given <code>index</code>
     * position.
     */
    Binding getBinding(int index);

    /**
     * Set bindings for this component by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theBinding Item to add at position <code>index</code>.
     */
    void setBinding(int index, Binding theBinding);

    /**
     * Append an item to bindings for this component.
     *
     * @param theBinding Item to append to bindings
     */
    void appendBinding(Binding theBinding);

    /**
     * Remove bindings for this component by index position.
     *
     * @param index The index position of the binding to remove
     * @return The Binding removed, if any.
     */
    Binding removeBinding(int index);

    /**
     * Get base URI for the document for this wsdl:definitions component.
     *
     * @return Base URI for the document for this wsdl:definitions component
     */
    String getDocumentBaseUri();

    /**
     * Set base URI for the document for this wsdl:definitions component.
     *
     * @param theDocumentBaseUri Base URI for the document for this
     * wsdl:definitions component
     */
    void setDocumentBaseUri(String theDocumentBaseUri);

    /**
     * Get the number of Import items in imports.
     *
     * @return The number of Import items in imports
     */
    int getImportsLength();

    /**
     * Get definitions imported into this container by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Description imported into this container at given
     * <code>index</code> position.
     */
    Import getImport(int index);

    /**
     * Set definitions imported into this container by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theImport Item to add at position <code>index</code>.
     */
    void setImport(int index, Import theImport);

    /**
     * Append an item to definitions imported into this container.
     *
     * @param theImport Item to append to imports
     */
    void appendImport(Import theImport);

    /**
     * Remove definitions imported into this container by index position.
     *
     * @param index The index position of the import to remove
     * @return The Import removed, if any.
     */
    Import removeImport(int index);

    /**
     * Get the number of Include items in includes.
     *
     * @return The number of Include items in includes
     */
    int getIncludesLength();

    /**
     * Get definitions included in this container by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Description included in this container at given
     * <code>index</code> position.
     */
    Include getInclude(int index);

    /**
     * Set definitions included in this container by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theInclude Item to add at position <code>index</code>.
     */
    void setInclude(int index, Include theInclude);

    /**
     * Append an item to definitions included in this container.
     *
     * @param theInclude Item to append to includes
     */
    void appendInclude(Include theInclude);

    /**
     * Remove definitions included in this container by index position.
     *
     * @param index The index position of the include to remove
     * @return The Include removed, if any.
     */
    Include removeInclude(int index);

    /**
     * Get the number of Interface items in interfaces.
     *
     * @return The number of Interface items in interfaces
     */
    int getInterfacesLength();

    /**
     * Get interfaces for this component by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Interfaces for this component at given <code>index</code>
     * position.
     */
    Interface getInterface(int index);

    /**
     * Set interfaces for this component by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theInterface Item to add at position <code>index</code>.
     */
    void setInterface(int index, Interface theInterface);

    /**
     * Append an item to interfaces for this component.
     *
     * @param theInterface Item to append to interfaces
     */
    void appendInterface(Interface theInterface);

    /**
     * Remove interfaces for this component by index position.
     *
     * @param index The index position of the interface to remove
     * @return The Interface removed, if any.
     */
    Interface removeInterface(int index);

    /**
     * Get the number of Service items in services.
     *
     * @return The number of Service items in services
     */
    int getServicesLength();

    /**
     * Get services for this component by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Services for this component at given <code>index</code>
     * position.
     */
    Service getService(int index);

    /**
     * Set services for this component by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theService Item to add at position <code>index</code>.
     */
    void setService(int index, Service theService);

    /**
     * Append an item to services for this component.
     *
     * @param theService Item to append to services
     */
    void appendService(Service theService);

    /**
     * Remove services for this component by index position.
     *
     * @param index The index position of the service to remove
     * @return The Service removed, if any.
     */
    Service removeService(int index);

    /**
     * Get namespace for components in this container.
     *
     * @return Namespace for components in this container
     */
    String getTargetNamespace();

    /**
     * Set namespace for components in this container.
     *
     * @param theTargetNamespace Namespace for components in this container
     */
    void setTargetNamespace(String theTargetNamespace);

    /**
     * Get the number of Types items in types.
     *
     * @return The number of Types items in types
     */
    int getTypesLength();

    /**
     * Get types (in schema or whatever) by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Types (in schema or whatever) at given <code>index</code>
     * position.
     */
    Types getType(int index);

    /**
     * Set types (in schema or whatever) by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theType Item to add at position <code>index</code>.
     */
    void setType(int index, Types theType);

    /**
     * Append an item to types (in schema or whatever).
     *
     * @param theType Item to append to types
     */
    void appendType(Types theType);

    /**
     * Remove types (in schema or whatever) by index position.
     *
     * @param index The index position of the type to remove
     * @return The Types removed, if any.
     */
    Types removeType(int index);

    /**
     * Find named binding in this definition or the imported/included
     * bindings.
     *
     * @param name Name of binding to find.
     * @return Named Binding; null if none found.
     */
    Binding findBinding(
        QName name);

    /**
     * Find named interface in this definition or the imported/included
     * interfaces.
     *
     * @param name Name of interface to find.
     * @return Named Interface; null if none found.
     */
    Interface findInterface(
        QName name);

    /**
     * Find named service in this definition or the imported/included
     * services.
     *
     * @param name Name of service to find.
     * @return Named Service; null if none found.
     */
    Service findService(
        QName name);

    /**
     * Create a new binding component, appended to this definition's binding
     * list.
     *
     * @param name Name of binding to create.
     * @return Newly created binding, appended to the bindings list.
     */
    Binding addNewBinding(
        String name);

    /**
     * Create a new import component, appended to this definition's import
     * list.
     *
     * @return Newly created import, appended to the imports list.
     */
    Import addNewImport();

    /**
     * Create a new include, appended to this definition's include list.
     *
     * @return Newly created include, appended to the includes list.
     */
    Include addNewInclude();

    /**
     * Create a new interface component, appended to this definition's
     * interface list.
     *
     * @param name Name of interface to create.
     * @return Newly created interface, appended to interfaces list.
     */
    Interface addNewInterface(
        String name);

    /**
     * Create a new service component, appended to this definition's service
     * list.
     *
     * @param name Name of service to create.
     * @return Newly created service, appended to the services list.
     */
    Service addNewService(
        String name);

    /**
     * Create a new types component, replacing the existing types component
     * of this definition, if necessary.
     *
     * @return Newly created Types component.
     */
    Types newTypes();

    /**
     * Return this WSDL definition as an XML string.
     *
     * @return This definition, serialized as an XML string.
     */
    String toXmlString();

    /**
     * Return this document as a DOM document. The DOM tree is a copy;
     * altering it will not affect this definitions component.
     *
     * @return This definition, as a DOM document.
     */
    org.w3c.dom.Document toXmlDocument();

}

// End-of-file: Description.java
