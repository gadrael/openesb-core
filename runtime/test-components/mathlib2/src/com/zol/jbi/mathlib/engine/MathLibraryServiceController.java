/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MathLibraryServiceController.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.zol.jbi.mathlib.engine;

import com.sun.jbi.management.support.JbiNameInfo;

/**
 * The MathLibraryServiceController class uses the functionality of the model
 * Component Installer to do all its work.
 */
public class MathLibraryServiceController extends com.sun.jbi.management.engine.ModelEngineComponent
{

    /** our immutable name: */
    private JbiNameInfo mJbiNameInfo;

    /**
     * Initializes MathLibraryServiceController.
     * @param anEnv is the JBI Framework Context provided for this component.
     */
    public void init(javax.jbi.component.ComponentContext anEnv)
        throws javax.jbi.JBIException
    {
        mJbiNameInfo = new JbiNameInfo( anEnv.getComponentName(), false );

        //we are a model component - intialize the model:
        super.initModelEngineComponent(anEnv, mJbiNameInfo);

        super.bootstrap();
    }
}
