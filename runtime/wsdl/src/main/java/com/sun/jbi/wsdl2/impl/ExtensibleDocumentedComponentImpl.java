/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtensibleDocumentedComponentImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import java.util.HashMap;

import org.apache.xmlbeans.XmlObject;

/**
 * Implementation of base for extensible, documented components.
 * 
 * @author Sun Microsystems, Inc.
 */
class ExtensibleDocumentedComponentImpl extends ExtensibleDocumentedComponent
{
    /**
     * Construct an extensible, documentation component from the given XML bean.
     * @param bean The XML bean used to construct this component.
     */
    ExtensibleDocumentedComponentImpl(XmlObject bean)
    {
        super(bean);
    }

    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = new HashMap();
        }

        return sWsdlAttributeQNames;    
    }
  
    /**
     * Get extensions for component, if any.
     *
     * @return extensions for component, if any
     */
    public com.sun.jbi.wsdl2.Extensions getExtensions()
    {
        return null;  // $$TODO    
    }

    /**
     * Set extensions for component, if any.
     *
     * @param theExtensions extensions for component, if any
     */
    public void setExtensions(com.sun.jbi.wsdl2.Extensions theExtensions)
    {
        return; // $$TODO    
    }

    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return null; // stub
    }  
}

// End-of-file: ExtensibleDocumentedComponentImpl.java
