/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Service.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.w3c.dom.DocumentFragment;
import org.w3.ns.wsdl.ServiceType;

/**
 * Abstract implementation of
 * WSDL 2.0 Service component.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class Service extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.Service
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final ServiceType getBean()
    {
        return (ServiceType) this.mXmlObject;
    }

    /**
     * Construct an abstract Service implementation base component.
     * 
     * @param bean      The XML bean for this Service component
     */
    Service(ServiceType bean)
    {
        super(bean);
    }

    /**
     * Create a new end point component, appending it to this service's
     * endpoint list.
     *
     * @param name NC name for the new endpoint.
     * @param binding Binding to which the endpoint refers.
     * @return The newly created endpoint, appended to this service's
     * endpoint list.
     */
    public abstract com.sun.jbi.wsdl2.Endpoint addNewEndpoint(
        String name,
        com.sun.jbi.wsdl2.Binding binding);

    /**
     * Return this WSDL service as an XML string.
     *
     * @return This service, serialized as an XML string.
     */
    public abstract String toXmlString();

    /**
     * Return this service as a DOM document fragment. The DOM subtree is a
     * copy; altering it will not affect this service.
     *
     * @return This service, as a DOM document fragment.
     */
    public abstract DocumentFragment toXmlDocumentFragment();

}

// End-of-file: Service.java
