/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingMessageReference.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.w3.ns.wsdl.BindingOperationMessageType;

/**
 * Abstract implementation of
 * WSDL 2.0 Binding message reference component.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class BindingMessageReference extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.BindingMessageReference
{
    /**
     * The the Xml bean for this component.
     *
     * @return Xml Bean for this component.
     */
    BindingOperationMessageType getBean()
    {
        return (BindingOperationMessageType) this.mXmlObject;
    }

    /**
     * Construct an abstract Binding operation message implementation base 
     * component.
     * 
     * @param bean The XML bean for this Binding operation message component.
     */
    BindingMessageReference(BindingOperationMessageType bean)
    {
        super(bean);
    }

}

// End-of-file: BindingMessageReference.java
