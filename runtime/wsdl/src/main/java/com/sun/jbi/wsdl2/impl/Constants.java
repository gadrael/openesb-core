/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Constants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

/**
 * Constants for the WSDL2 implementation.
 * 
 * @author Sun Microsystems, Inc.
 */
class Constants implements com.sun.jbi.wsdl2.Constants
{
    /** The root private name for this package */
    public static final String ROOT_PRIVATE_NAME    = ROOT_PUBLIC_NAME + ".impl";

    /** The "empty" namespace name */
    public static final String EMPTY_NAMESPACE_NAME = "";

    /** Indent level for XML pretty printing (ie, conversion to string) */
    public static final int XML_PRETTY_PRINT_INDENT = 4;
}
