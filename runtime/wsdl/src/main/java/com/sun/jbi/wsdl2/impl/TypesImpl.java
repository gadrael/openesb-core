/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TypesImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import java.util.Map;

import org.w3.ns.wsdl.TypesType;

/**
 * Implementation of WSDL 2.0 type definitions component.
 * 
 * @author Sun Microsystems, Inc.
 */
final class TypesImpl extends Types
{
    /** Container for this Types component */
    private DescriptionImpl mContainer;
  
    /**
     * Construct a Types component implementation object from the given XML bean.
     * 
     * @param bean The types XML bean to use to construct this component.
     * @param container The container for this Types component.
     */
    private TypesImpl(TypesType bean, DescriptionImpl container)
    {
        super(bean);

        this.mContainer = container;
    }

    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /** 
     * Worker class method for {@link #getWsdlAttributeNameMap()}.
     * 
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    static synchronized java.util.Map getAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = XmlBeansUtil.getAttributesMap(
                TypesType.type);
        }

        return sWsdlAttributeQNames;
    }
  
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        return getAttributeNameMap();
    }
  
    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return this.mContainer;
    }
  
    /**
     * A factory class for creating / finding components for given XML beans.
     * <p>
     * This factory guarantees that there will only be one component for each
     * XML bean instance.
     */
    static class Factory
    {
        /**
         * Find the WSDL types component associated with the given XML
         * bean, creating a new component if necessary.
         * <p>
         * We use a simple pattern to avoid serializing execution of this
         * entire method. This is motivated by the observation that this method
         * is more commonly executed in the case where the component exists than
         * the case where a component needs to be created.
         * 
         * @param bean The XML bean to find the component for.
         * @param defs The container for the component.
         * @return The WSDL types component for the given <code>bean</code>
         *         (null if the <code>bean</code> is null).
         */
        static TypesImpl getInstance(TypesType bean, DescriptionImpl defs)
        {
            TypesImpl   result;

            if (bean != null)
            {
                Map         map = defs.getTypesMap();
                
                result = (TypesImpl) map.get(bean);

                if (result == null)
                {
                    synchronized (map)
                    {
                        result = (TypesImpl) map.get(bean);

                        if (result == null)
                        {
                            result = new TypesImpl(bean, defs);
                            map.put(bean, result);
                        }
                    }
                }
            }
            else
            {
                result = null;
            }
      
            return result;
        }
    }
}

// End-of-file: TypesImpl.java
