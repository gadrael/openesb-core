/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterfaceFaultImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import javax.xml.namespace.QName;

import org.w3.ns.wsdl.InterfaceFaultType;

/**
 * Implementation of WSDL 2.0 Interface Fault component.
 * 
 * @author Sun Microsystems, Inc.
 */
final class InterfaceFaultImpl extends InterfaceFault 
{
    /** The definitions component this binding belongs to */
    private DescriptionImpl   mContainer;
  
    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return this.mContainer;
    }
  
    /**
     * Construct a interface fault component implementation object from the 
     * given XML bean.
     * 
     * @param bean The interface XML bean to use to construct this component.
     * @param defs The container for this interface fault component.
     */
    private InterfaceFaultImpl(InterfaceFaultType bean, DescriptionImpl defs)
    {
        super(bean);

        this.mContainer = defs;
    }
    
    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /** 
     * Worker class method for {@link #getWsdlAttributeNameMap()}.
     * 
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    static synchronized java.util.Map getAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = XmlBeansUtil.getAttributesMap(
                InterfaceFaultType.type);
        }

        return sWsdlAttributeQNames;
    }
  
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        return getAttributeNameMap();
    }

    /**
     * Get the name of this component.
     *
     * @return The name of this component
     */
    public String getName()
    {
        return getBean().getName();
    }

    /**
     * Set the name of this component.
     *
     * @param theName The name of this component
     */
    public void setName(String theName)
    {
        getBean().setName(theName);
    }

    /**
     * Get qualified name referring to the element declaration of the payload
     * of the fault.
     *
     * @return Qualified name referring to the element declaration of the
     * payload of the fault
     */
    public QName getElement()
    {
        return getBean().getElement();
    }

    /**
     * Set qualified name referring to the element declaration of the payload
     * of the fault.
     *
     * @param theElement Qualified name referring to the element declaration
     * of the payload of the fault
     */
    public void setElement(QName theElement)
    {
        if (theElement != null)
        {
            getBean().setElement(theElement);
        }
        else
        {
            getBean().unsetElement();
        }
    }

    /**
     * Get qualified name of this component.
     *
     * @return Qualified name of this component
     */
    public QName getQualifiedName()
    {
        return new QName(getContainer().getTargetNamespace(), getName());
    }

    /**
     * A factory class for creating / finding components for given XML beans.
     * <p>
     * This factory guarantees that there will only be one component for each
     * XML bean instance.
     */
    static class Factory
    {
        /**
         * Find the WSDL interface component associated with the given XML
         * bean, creating a new component if necessary.
         * <p>
         * This is thread-safe.<p>
         * 
         * @param bean The XML bean to find the component for.
         * @param defs The container for the component.
         * @return The WSDL interface fault component for the given 
         *         <code>bean</code> (null if the <code>bean</code> is null).
         */
        static InterfaceFaultImpl getInstance(
            InterfaceFaultType bean, DescriptionImpl defs)
        {
            InterfaceFaultImpl   result = null;
      
            if (bean != null)
            {
                java.util.Map     map = defs.getInterfaceFaultMap();
                
                synchronized (map)
                {
                    result = (InterfaceFaultImpl) map.get(bean);

                    if (result == null)
                    {
                        result = new InterfaceFaultImpl(bean, defs);
                        map.put(bean, result);
                    }
                }
            }

            return result;
        }
    }
}
