/*
 * An XML document type.
 * Localname: include
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.IncludeDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one include(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class IncludeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.IncludeDocument
{
    
    public IncludeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INCLUDE$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "include");
    
    
    /**
     * Gets the "include" element
     */
    public org.w3.ns.wsdl.IncludeType getInclude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.IncludeType target = null;
            target = (org.w3.ns.wsdl.IncludeType)get_store().find_element_user(INCLUDE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "include" element
     */
    public void setInclude(org.w3.ns.wsdl.IncludeType include)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.IncludeType target = null;
            target = (org.w3.ns.wsdl.IncludeType)get_store().find_element_user(INCLUDE$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.IncludeType)get_store().add_element_user(INCLUDE$0);
            }
            target.set(include);
        }
    }
    
    /**
     * Appends and returns a new empty "include" element
     */
    public org.w3.ns.wsdl.IncludeType addNewInclude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.IncludeType target = null;
            target = (org.w3.ns.wsdl.IncludeType)get_store().add_element_user(INCLUDE$0);
            return target;
        }
    }
}
