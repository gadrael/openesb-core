/*
 * XML Type:  ServiceType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.ServiceType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML ServiceType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class ServiceTypeImpl extends org.w3.ns.wsdl.impl.ExtensibleDocumentedTypeImpl implements org.w3.ns.wsdl.ServiceType
{
    
    public ServiceTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ENDPOINT$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "endpoint");
    private static final javax.xml.namespace.QName NAME$2 = 
        new javax.xml.namespace.QName("", "name");
    private static final javax.xml.namespace.QName INTERFACE$4 = 
        new javax.xml.namespace.QName("", "interface");
    
    
    /**
     * Gets array of all "endpoint" elements
     */
    public org.w3.ns.wsdl.EndpointType[] getEndpointArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ENDPOINT$0, targetList);
            org.w3.ns.wsdl.EndpointType[] result = new org.w3.ns.wsdl.EndpointType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "endpoint" element
     */
    public org.w3.ns.wsdl.EndpointType getEndpointArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.EndpointType target = null;
            target = (org.w3.ns.wsdl.EndpointType)get_store().find_element_user(ENDPOINT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "endpoint" element
     */
    public int sizeOfEndpointArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ENDPOINT$0);
        }
    }
    
    /**
     * Sets array of all "endpoint" element
     */
    public void setEndpointArray(org.w3.ns.wsdl.EndpointType[] endpointArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(endpointArray, ENDPOINT$0);
        }
    }
    
    /**
     * Sets ith "endpoint" element
     */
    public void setEndpointArray(int i, org.w3.ns.wsdl.EndpointType endpoint)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.EndpointType target = null;
            target = (org.w3.ns.wsdl.EndpointType)get_store().find_element_user(ENDPOINT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(endpoint);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "endpoint" element
     */
    public org.w3.ns.wsdl.EndpointType insertNewEndpoint(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.EndpointType target = null;
            target = (org.w3.ns.wsdl.EndpointType)get_store().insert_element_user(ENDPOINT$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "endpoint" element
     */
    public org.w3.ns.wsdl.EndpointType addNewEndpoint()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.EndpointType target = null;
            target = (org.w3.ns.wsdl.EndpointType)get_store().add_element_user(ENDPOINT$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "endpoint" element
     */
    public void removeEndpoint(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ENDPOINT$0, i);
        }
    }
    
    /**
     * Gets the "name" attribute
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" attribute
     */
    public org.apache.xmlbeans.XmlNCName xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(NAME$2);
            return target;
        }
    }
    
    /**
     * Sets the "name" attribute
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$2);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" attribute
     */
    public void xsetName(org.apache.xmlbeans.XmlNCName name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlNCName)get_store().add_attribute_user(NAME$2);
            }
            target.set(name);
        }
    }
    
    /**
     * Gets the "interface" attribute
     */
    public javax.xml.namespace.QName getInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(INTERFACE$4);
            if (target == null)
            {
                return null;
            }
            return target.getQNameValue();
        }
    }
    
    /**
     * Gets (as xml) the "interface" attribute
     */
    public org.apache.xmlbeans.XmlQName xgetInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlQName target = null;
            target = (org.apache.xmlbeans.XmlQName)get_store().find_attribute_user(INTERFACE$4);
            return target;
        }
    }
    
    /**
     * Sets the "interface" attribute
     */
    public void setInterface(javax.xml.namespace.QName xinterface)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(INTERFACE$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(INTERFACE$4);
            }
            target.setQNameValue(xinterface);
        }
    }
    
    /**
     * Sets (as xml) the "interface" attribute
     */
    public void xsetInterface(org.apache.xmlbeans.XmlQName xinterface)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlQName target = null;
            target = (org.apache.xmlbeans.XmlQName)get_store().find_attribute_user(INTERFACE$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlQName)get_store().add_attribute_user(INTERFACE$4);
            }
            target.set(xinterface);
        }
    }
}
