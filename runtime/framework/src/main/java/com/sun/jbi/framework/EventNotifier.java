/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventNotifier.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.platform.PlatformContext;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;

/**
 * This is the instance Event Notifier MBean that generates notifications for
 * the entire JBI runtime. There is one of these MBeans per runtime instance,
 * and it is called to emit notifications for all applicable events in the
 * runtime. This MBean is registered with ServiceName=Framework,
 * ControlType=Notification, and ComponentType=System.
 *
 * Notifications that are currently supported by this implementation include:
 * <ul>
 * <li>JBI Runtime:
 *   <ul>
 *   <li>Ready</li>
 *   <li>Started</li>
 *   <li>Stopped</li>
 *   </ul></li>
 * <li>Component (Binding Component / Service Engine):
 *   <ul>
 *   <li>Installed</li>
 *   <li>Started</li>
 *   <li>Stopped</li>
 *   <li>Shut down</li>
 *   <li>Uninstalled</li>
 *   </ul></li>
 * <li>Shared Library:
 *   <ul>
 *   <li>Installed</li>
 *   <li>Uninstalled</li>
 *   </ul></li>
 * <li>Service Assembly:
 *   <ul>
 *   <li>Deployed</li>
 *   <li>Started</li>
 *   <li>Stopped</li>
 *   <li>Shut down</li>
 *   <li>Undeployed</li>
 *   </ul></li>
 * <li>Service Unit:
 *   <ul>
 *   <li>Deployed</li>
 *   <li>Started</li>
 *   <li>Stopped</li>
 *   <li>Shut down</li>
 *   <li>Undeployed</li> 
 *   </ul></li>
 * </ul>
 *
 * All notifications emitted by this MBean are instances of the
 * <code>javax.management.Notification</code> class, and include following
 * information:
 *
 * <ul>
 * <li>
 * type - a dotted string describing the notification, such as
 *        "com.sun.jbi.started.binding.component"
 * </li>
 * <li>
 * msg - a message resulting from the operation, such as
 *       "Binding sun-http-binding started successfully"
 * </li>
 * <li>
 * seqNo - a sequence number which is unique for the current execution of the
 *         runtime
 * </li>
 * <li>
 * timeStamp - a timestamp indicating the time the notification was created
 * </li>
 * <li>
 * userData - a <code>javax.management.openmbean.CompositeData</code> object
 *            which contains the following:
 *   <ul> 
 *   <li>
 *   Event type: Deployed, Undeployed, Installed, Uninstalled, Started,
 *               Stopped, or ShutDown
 *   </li>
 *   <li>
 *   Source type: BindingComponent, ServiceEngine, SharedLibrary,
 *                ServiceAssembly, ServiceUnit, or JBIRuntime
 *   </li>
 *   <li>
 *   Source name: component name, shared library name, service assembly name,
 *                or service unit name
 *   </li>
 *   <li>
 *   Target name: instance name or cluster name + instance name, such as
 *                "server", "instance1", or "cluster1-instance1"
 *   </li>
 *   <li>
 *   Component name: this is present only for Service Unit notifications
 *   </li>
 *   <li>
 *   Service Assembly name: this is present only for Service Unit notifications 
 *   </li>
 *   <li>
 *   </ul>
 * </li>
 * </ul>
 *
 * Note that this class extends the NotificationBroadcasterSupport class
 * provided by the JDK, and there is no guarantee that the calls to send
 * notifications are asynchronous. Therefore, all sending of notifications
 * is done on separate threads to ensure that an ill-behaved client cannot
 * block the thread on which the call to this class was made. This is done
 * using the facilities provided by the java.util.concurrent package. In the
 * constructor for this class, a thread pool is created, and all calls to
 * the sendNotification() method are made using this thread pool. The Notify
 * class contained in this class implements the Runnable interface and is
 * used to make the sendNotification() calls.
 *
 * @author Mark S White
 */
public class EventNotifier
    extends NotificationBroadcasterSupport
    implements EventNotifierCommon, EventNotifierMBean
{
    /**
     * Global sequence number for notifications from this MBean.
     */
    private static AtomicLong sSeqNo = new AtomicLong();

    /**
     * Flag for enabling / disabling event notifications.
     */
    private boolean mNotificationsEnabled;

    /**
     * Logger for messages.
     */
    private Logger mLog;

    /**
     * Translator / formatter for messages.
     */
    private StringTranslator mTranslator;

    /**
     * Instance name where this MBean is running.
     */
    private String mInstanceName;

    /**
     * Combined cluster and instance name. If this is a clustered instance,
     * this is "clusterName-instanceName"; otherwise it's "instanceName".
     */
    private String mCombinedName;

    /**
     * JMX ObjectName of this MBean.
     */
    private ObjectName mObjName;

    /**
     * JMX ObjectName of the DAS event notifier MBean. This is required for
     * invoking operations to inform the MBean when this instance event
     * notifier MBean starts up and shuts down.
     */
    private ObjectName mDASMBeanName;

    /**
     * Platform Context for the runtime environment.
     */
    private PlatformContext mPlatformContext;

    /**
     * Executor service to send notifications on threads from a pool.
     */
    private ExecutorService mExecSvc;

    /**
     * CompositeType used to describe CompositeData representing user data
     * in notifications for runtime, component, shared library, and service
     * assembly events.
     */
    private CompositeType mUserDataType1;

    /**
     * CompositeType used to describe CompositeData representing user data
     * in notifications for service unit events.
     */
    private CompositeType mUserDataType2;

    /**
     * Source name for JBI runtime notification messages. Package-visible for
     * unit testing.
     */
    static final String JBI_FRAMEWORK = "JBIFramework";

    /**
     * List of item names for CompositeData construction for runtime, component,
     * shared library, and service assembly notifications. Package-visible for
     * unit testing.
     */
    static final String[] ITEM_NAMES_1 = {
        EVENT_TYPE_KEY,
        SOURCE_TYPE_KEY,
        TARGET_NAME_KEY,
        SOURCE_NAME_KEY
    };

    /**
     * List of item names for CompositeData construction for service unit
     * notifications. Package-visible for unit testing.
     */
    static final String[] ITEM_NAMES_2 = {
        EVENT_TYPE_KEY,
        SOURCE_TYPE_KEY,
        TARGET_NAME_KEY,
        SOURCE_NAME_KEY,
        SERVICE_ASSEMBLY_NAME_KEY,
        COMPONENT_NAME_KEY
    };

    /**
     * List of descriptions of items for CompositeData construction for
     * runtime, component, shared library, and service assembly notifications.
     * Package-visible for unit testing.
     */
    static final String[] ITEM_DESCRIPTIONS_1 = {
        "Type of event that occurred",
        "Type of entity on which the event occurred",
        "Name of the target where the event occurred",
        "Name of the entity affected by the event"
    };

    /**
     * List of descriptions of items for CompositeData construction for service
     * unit notifications. Package-visible for unit testing.
     */
    static final String[] ITEM_DESCRIPTIONS_2 = {
        "Type of event that occurred",
        "Type of entity on which the event occurred",
        "Name of the target where the event occurred",
        "Name of the service unit affected by the event",
        "Name of the service assembly to which the service unit belongs",
        "Name of the component to which the service unit is deployed"
    };

    /**
     * List of types of items for CompositeData construction for runtime,
     * component, shared library, and service assembly notifications. Package-
     * visible for unit testing.
     */
    static final OpenType[] ITEM_TYPES_1 = {
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING
    };

    /**
     * List of types of items for CompositeData construction for service unit
     * notifications. Package-visible for unit testing.
     */
    static final OpenType[] ITEM_TYPES_2 = {
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING
    };

    /**
     * Constructor to set up local constants.
     *
     * @param ctx the environment context for the JBI runtime.
     */
    EventNotifier(EnvironmentContext ctx)
        throws javax.jbi.JBIException
    {
        super();
        mLog = ctx.getLogger();
        mTranslator = (StringTranslator) ctx.getStringTranslatorFor(this);
        mPlatformContext = ctx.getPlatformContext();
        mInstanceName = mPlatformContext.getInstanceName();
        mNotificationsEnabled = true;  // Default to enabled for now

        if ( mPlatformContext.isInstanceClustered(mInstanceName) )
        {
            mCombinedName = mInstanceName + "-" + 
                mPlatformContext.getTargetName();
        }
        else
        {
            mCombinedName = mInstanceName;
        }

        // Create the thread pool to be used for sending notifications

        mExecSvc = Executors.newCachedThreadPool();

        // Initialize the composite data types used in the notifications sent
        // by this MBean.
        try
        {
            mUserDataType1 = new CompositeType(
                "UserData",
                "notification user data",
                ITEM_NAMES_1, ITEM_DESCRIPTIONS_1, ITEM_TYPES_1);
            mUserDataType2 = new CompositeType(
                "UserData",
                "notification user data",
                ITEM_NAMES_2, ITEM_DESCRIPTIONS_2, ITEM_TYPES_2);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.FN_USER_DATA_CREATE_FAILED,
                odEx.getClass().getName());
            mLog.log(Level.WARNING, msg, odEx);
            throw new javax.jbi.JBIException(msg, odEx);
        }

        // Construct the EventNotifierMBean name for this instance.
        mObjName = ctx.getMBeanNames().getSystemServiceMBeanName(
            MBeanNames.SERVICE_NAME_FRAMEWORK,
            MBeanNames.CONTROL_TYPE_NOTIFICATION);

        // Construct the DAS EventNotifierMBean name.
        mDASMBeanName = ctx.getMBeanNames().getSystemServiceMBeanName(
            MBeanNames.ServiceName.Framework,
            MBeanNames.ServiceType.Notification, "domain");
    }

//
// Overridden methods of the NotificationBroadcasterSupport class
//

    /**
     * Returns an array indicating, for each notification this MBean may send,
     * the notification type, the name of the Java class of the notification,
     * and a description of the notification.
     *
     * @return an array of <code>javax.managagement.MBeanNotificationInfo</code>
     * objects which describe the notifications.
     */
    public MBeanNotificationInfo[] getNotificationInfo()
    {
        MBeanNotificationInfo[] notifs = {
            new MBeanNotificationInfo(NOTIFICATION_TYPES,
                NOTIFICATION_CLASS_NAME, NOTIFICATION_DESCRIPTION)
        };
        return notifs;
    }

//
// Methods in the EventNotifierMBean interface
//

    /**
     * Disable event notifications from being emitted by this MBean.
     */
    public void disableNotifications()
    {
        mNotificationsEnabled = false;
        mLog.log(Level.FINE, "Event notifications disabled for instance {0}", mCombinedName);
    }

    /**
     * Enable event notifications to be emitted by this MBean.
     */
    public void enableNotifications()
    {
        mNotificationsEnabled = true;
        mLog.log(Level.FINE, "Event notifications enabled for instance {0}", mCombinedName);
    }

//
// Methods in the EventNotifierBase interface. These methods are callable from
// any service in the JBI runtime and are not exposed as MBean operations.
//

    /**
     * Emit a notification for an event on the JBI runtime. The caller provides
     * information that determines the content of of the notification. The
     * event type for a JBI runtime notification can be Ready, Started, or
     * Stopped. The source type is always JBIRuntime.
     *
     * @param eventType the type of event that occurred.
     * @param msg a message associated with the event.
     * @return the actual Notification sent (this is used primarily for unit
     * testing).
     */
    public Notification emitRuntimeNotification(EventType eventType, String msg)
    {
        if ( mNotificationsEnabled )
        {
            String type = JBI_PREFIX + eventType + "." + SourceType.JBIRuntime;
            Notification notif =
                new Notification(type, mObjName, sSeqNo.incrementAndGet(), msg);
            CompositeData userData = toCompositeData1(eventType.toString(),
                SourceType.JBIRuntime.toString(), JBI_FRAMEWORK);
            if ( null != userData )
            {
                mLog.log(Level.FINEST, "Created notification userdata: {0}", userData.toString());
                notif.setUserData(userData);
            }
            mLog.log(Level.FINER, "Sending notification: {0} sequence number {1}", new Object[]{notif.toString(), notif.getSequenceNumber()});
            mExecSvc.execute(new Notify(this, notif));
            return notif;
        }
        else
        {
            return null;
        }
    }

    /**
     * Emit a notification for an event on either a binding component or a
     * service engine. The caller provides information that determines the
     * content of the notification. The event type for a component notification
     * can be Installed, Uninstalled, Started, Stopped, or ShutDown. The source
     * type is either BindingComponent or ServiceEngine.
     *
     * @param eventType the type of event that occurred.
     * @param sourceType the type of component on which the event occurred.
     * @param componentName the name of the component on which the event
     * occurred.
     * @param msg a message associated with the event.
     * @return the actual Notification sent (this is used primarily for unit
     * testing).
     */
    public Notification emitComponentNotification(EventType eventType,
        SourceType sourceType, String componentName, String msg)
    {
        if ( mNotificationsEnabled )
        {
            String type = JBI_PREFIX + eventType + "." + sourceType;
            Notification notif =
                new Notification(type, mObjName, sSeqNo.incrementAndGet(), msg);
            CompositeData userData = toCompositeData1(eventType.toString(),
                sourceType.toString(), componentName);
            if ( null != userData )
            {
                mLog.log(Level.FINEST, "Created notification userdata: {0}", userData.toString());
                notif.setUserData(userData);
            }
            mLog.log(Level.FINER, "Sending notification: {0} sequence number {1}", new Object[]{notif.toString(), notif.getSequenceNumber()});
            mExecSvc.execute(new Notify(this, notif));
            return notif;
        }
        else
        {
            return null;
        }
    }

    /**
     * Emit a notification for an event on a shared library. The caller provides
     * information that determines the content of the notification. The event
     * type for a shared library notification can be either Installed or
     * Uninstalled. The source type is always SharedLibrary.
     *
     * @param eventType the type of event that occurred.
     * @param sharedLibraryName the name of the shared library on which the
     * event occurred.
     * @param msg a message associated with the event.
     * @return the actual Notification sent (this is used primarily for unit
     * testing).
     */
    public Notification emitSharedLibraryNotification(EventType eventType,
        String sharedLibraryName, String msg)
    {
        if ( mNotificationsEnabled )
        {
            String type = JBI_PREFIX + eventType + "." +
                SourceType.SharedLibrary;
            Notification notif =
                new Notification(type, mObjName, sSeqNo.incrementAndGet(), msg);
            CompositeData userData = toCompositeData1(eventType.toString(),
                SourceType.SharedLibrary.toString(), sharedLibraryName);
            if ( null != userData )
            {
                mLog.log(Level.FINEST, "Created notification userdata: {0}", userData.toString());
                notif.setUserData(userData);
            }
            mLog.log(Level.FINER, "Sending notification: {0} sequence number {1}", new Object[]{notif.toString(), notif.getSequenceNumber()});
            mExecSvc.execute(new Notify(this, notif));
            return notif;
        }
        else
        {
            return null;
        }
    }

    /**
     * Emit a notification for an event on a service assembly. The caller
     * provides information that determines the content of the notification.
     * The event type for a service assembly notification can be Deployed,
     * Undeployed, Started, Stopped, or ShutDown. The source type is always
     * ServiceAssembly.
     *
     * @param eventType the type of event that occurred.
     * @param serviceAssemblyName the name of the service assembly on which the
     * event occurred.
     * @param msg a message associated with the event.
     * @return the actual Notification sent (this is used primarily for unit
     * testing).
     */
    public Notification emitServiceAssemblyNotification(EventType eventType,
        String serviceAssemblyName, String msg)
    {
        if ( mNotificationsEnabled )
        {
            String type = JBI_PREFIX + eventType + "." +
                SourceType.ServiceAssembly;
            Notification notif =
                new Notification(type, mObjName, sSeqNo.incrementAndGet(), msg);
            CompositeData userData = toCompositeData1(eventType.toString(),
                SourceType.ServiceAssembly.toString(), serviceAssemblyName);
            if ( null != userData )
            {
                mLog.log(Level.FINEST, "Created notification userdata: {0}", userData.toString());
                notif.setUserData(userData);
            }
            mLog.log(Level.FINER, "Sending notification: {0} sequence number {1}", new Object[]{notif.toString(), notif.getSequenceNumber()});
            mExecSvc.execute(new Notify(this, notif));
            return notif;
        }
        else
        {
            return null;
        }
    }

    /**
     * Emit a notification for an event on a service unit. The caller provides
     * information that determines the content of the notification. The event
     * type for a service unit notification can be Deployed, Undeployed, 
     * Started, Stopped, or ShutDown. The source type is always ServiceUnit.
     *
     * @param eventType the type of event that occurred.
     * @param serviceUnitName the name of the service unit on which the event
     * occurred.
     * @param serviceAssemblyName the name of the service assembly to which the
     * service unit belongs.
     * @param componentName the name of the component to which the service unit
     * is deployed.
     * @param msg a message associated with the event.
     * @return the actual Notification sent (this is used primarily for unit
     * testing).
     */
    public Notification emitServiceUnitNotification(EventType eventType,
        String serviceUnitName, String serviceAssemblyName,
        String componentName, String msg)
    {
        if ( mNotificationsEnabled )
        {
            String type = JBI_PREFIX + eventType + "." + SourceType.ServiceUnit;
            Notification notif =
                new Notification(type, mObjName, sSeqNo.incrementAndGet(), msg);
            CompositeData userData = toCompositeData2(eventType.toString(),
                SourceType.ServiceUnit.toString(), serviceUnitName,
                serviceAssemblyName, componentName);
            if ( null != userData )
            {
                mLog.log(Level.FINEST, "Created notification userdata: {0}", userData.toString());
                notif.setUserData(userData);
            }
            mLog.log(Level.FINER, "Sending notification: {0} sequence number {1}", new Object[]{notif.toString(), notif.getSequenceNumber()});
            mExecSvc.execute(new Notify(this, notif));
            return notif;
        }
        else
        {
            return null;
        }
    }

//
// Package visible methods
//

    /**
     * Inform the DAS Event Notifier MBean that this instance is starting. This
     * causes the DAS MBean to invoke the <code>addNotificationListener()</code>
     * operation on this instance MBean to register itself as a notification
     * listener for notifications from this instance. This method is called by
     * the framework during startup.
     */
    void instanceStarting()
    {
        invokeDASMBean("instanceStarted");
    }

    /**
     * Inform the DAS Event Notifier MBean that this instance is stopping. This
     * causes the DAS MBean to invoke the <code>removeNotificationListener</code>
     * operation on this instance MBean to unregister itself as a notification
     * listener for notifications from this instance. This method is called by
     * the framework during shutdown.
     */
    void instanceStopping()
    {
        invokeDASMBean("instanceStopped");

        // Shut down the ExecutorService. This is crucial to prevent any
        // notification threads from hanging the shutdown of the runtime.
        mExecSvc.shutdownNow();
    }

//
// Private methods
//

    /**
     * Invoke an operation on the DAS Event Notifier MBean.
     *
     * @param operation the operation to be invoked, either "instanceStarted"
     * or "instanceStopped".
     */
    private void invokeDASMBean(String operation)
    {
        // Get an MBean server connection with the DAS

        MBeanServerConnection mbsConn = null;
        try
        {
            mbsConn = mPlatformContext.getMBeanServerConnection(
                mPlatformContext.getAdminServerName());
        }
        catch ( Throwable ex )
        {
            // This means that the DAS is not currently running. This can be
            // ignored, because when the DAS starts, it registers as a
            // notification listener with any instances that are already up.
            mLog.log(Level.FINE, "Unable to get MBean server connection " +
                " for admin server " + mPlatformContext.getAdminServerName() +
                ". Exception follows.", ex);
        }

        // If we have a connection with the DAS, invoke the MBean operation.

        if ( null != mbsConn )
        {
            // Set up parameters for calling the MBean
            String[] signature;
            Object[] params;
            signature = new String[]{"java.lang.String",
                "javax.management.ObjectName"};
            params = new Object[]{mInstanceName, mObjName};

            // Invoke the specified operation on the DAS event notifier MBean
            try
            {
                mLog.log(Level.FINEST, "Invoking {0} on MBean {1}", new Object[]{operation, mDASMBeanName});
                mbsConn.invoke(mDASMBeanName, operation, params, signature);
            }
            catch ( javax.management.InstanceNotFoundException infEx )
            {
                mLog.log(Level.FINE, "Invoke of {0} on DAS MBean {1} failed, MBean is not registered", new Object[]{operation, mDASMBeanName});
                   
            }
            catch ( Throwable ex )
            {
                Throwable actualEx = stripJmxException(ex);
                mLog.log(Level.FINE, "Failure invoking MBean operation " +
                    operation + ". Exception follows.", actualEx);
            }
        }
    }

    /**
     * Strip JMX Exceptions from the exception chain and return the actual
     * embedded Exception thrown by the remote instance.
     *
     * @param ex the JMX exception caught.
     * @return the actual exception thrown by the remote instance.
     */
    private Throwable stripJmxException(Throwable ex)
    {
        Throwable currEx = ex;

        while ( null != currEx.getCause() )
        {
            if ( !(currEx instanceof javax.management.JMException)
                && !(currEx instanceof javax.management.JMRuntimeException) )
            {
                return currEx;
            }
            currEx = currEx.getCause();
        }
        return currEx;
    }

    /**
     * Convert notification information to a CompositeData type. This version
     * is for runtime, component, shared library, and service assembly events.
     *
     * @param eventType The type of event that occurred.
     * @param sourceType The type entity on which the event occurred.
     * @param sourceName The name of the entity on which the event occurred.
     * @return The CompositeData constructed from the input parameters.
     */
    private CompositeData toCompositeData1(String eventType,
        String sourceType, String sourceName)
    {
        Object values[] = {
            eventType,
            sourceType,
            mCombinedName,
            sourceName
        };

        CompositeData cd = null;
        try
        {
            cd = new CompositeDataSupport(mUserDataType1, ITEM_NAMES_1, values);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            mLog.log(Level.WARNING, mTranslator.getString(
                LocalStringKeys.FN_USER_DATA_CREATE_FAILED,
                odEx.getClass().getName()), odEx);
        }
        return cd;
    }

    /**
     * Convert notification information to a CompositeData type. This version
     * is for service unit events.
     *
     * @param eventType The type of event that occurred.
     * @param sourceType The type entity on which the event occurred.
     * @param serviceUnitName The name of the service unit on which the event
     * occurred.
     * @param serviceAssemblyName The name of the service assembly to which the
     * service unit belongs.
     * @param componentName The name of the component to which the service unit
     * is deployed.
     * @return The CompositeData constructed from the input parameters.
     */
    private CompositeData toCompositeData2(String eventType,
        String sourceType, String serviceUnitName,
        String serviceAssemblyName, String componentName)
    {
        Object values[] = {
            eventType,
            sourceType,
            mCombinedName,
            serviceUnitName,
            serviceAssemblyName,
            componentName
        };

        CompositeData cd = null;
        try
        {
            cd = new CompositeDataSupport(mUserDataType2, ITEM_NAMES_2, values);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            mLog.log(Level.WARNING, mTranslator.getString(
                LocalStringKeys.FN_USER_DATA_CREATE_FAILED,
                odEx.getClass().getName()), odEx);
        }
        return cd;
    }

//
// Inner class to provide a Runnable implementation that can run notifications
// on threads from a pool.
//

    /**
     * Runnable class to send a notification on a separate thread.
     */
    class Notify implements Runnable
    {
        /**
         * EventNotifier instance reference.
         */
        EventNotifier mEventNotifier;

        /**
         * Notification instance to be sent.
         */
        Notification mNotification;

        /**
         * Constructor.
         *
         * @param en the instance of EventNotifier that called this constructor.
         * @param notif the JMX Notification to be sent.
         */
        Notify(EventNotifier en, Notification notif)
        {
            mEventNotifier = en;
            mNotification = notif;
        }

        /**
         * Run the actual sending of the notification. This runs on a thread
         * from the pool created by Executors.newCachedThreadPool().
         */
        public void run()
        {
            mEventNotifier.sendNotification(mNotification);
        }
    }

}
