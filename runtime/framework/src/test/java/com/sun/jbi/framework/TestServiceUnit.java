/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceUnit.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.framework.ScaffoldPlatformContext;
import java.io.ByteArrayInputStream;
import java.util.Properties;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.ServiceUnitState;

/**
 * Tests the ServiceUnit class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServiceUnit
    extends junit.framework.TestCase
{
    /**
     * Local instance of the EnvironmentContext
     */
    private EnvironmentContext mContext;

    /**
     * Local instance of the ServiceUnit class
     */
    private ServiceUnit mSU;

    /**
     * Constant for bad state.
     */
    static final int BAD_STATE = 409;
 	 
    /**
     * Constant for Component name
     */
    static final String COMP_NAME =
        "ElroyJetson";

    /**
     * Constant for Service Assembly name
     */
    static final String SA_NAME =
        "GeorgeJetson";

    /**
     * Constant for Service Unit name
     */
    static final String SU_NAME =
        "SpacelySprockets";

    /**
     * Constant for Service Unit file path
     */
    static final String SU_PATH =
        "/as8/domains/JBIdomain/jbi/schemaorg_apache_xmlbeans.system/deployment/SpacelySprockets";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestServiceUnit(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the ServiceUnit instances
     * and other objects needed for the tests. It also serves to test
     * the constructor for the class.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mContext = new EnvironmentContext(new ScaffoldPlatformContext(), 
                new JBIFramework(), new Properties());

        // Create a ServiceUnit with a single-element class path

        mSU = new ServiceUnit(SA_NAME, SU_NAME, SU_PATH);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Tests getEquals with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEquals()
        throws Exception
    {
        ServiceUnit su;

        // Null object:
        assertFalse("Got wrong result with null: ",
                     mSU.equals(null));

        // Different object types:
        assertFalse("Got wrong result different object type: ",
                     mSU.equals(this));

        // Different SA name values:
        su = new ServiceUnit("dork", SU_NAME, SU_PATH);
        assertFalse("Got wrong result with different SA names: ",
                     mSU.equals(su));

        // Different name values:
        su = new ServiceUnit(SA_NAME, "nerd", SU_PATH);
        assertFalse("Got wrong result with different names: ",
                     mSU.equals(su));

        // Different file path values:
        su = new ServiceUnit(SA_NAME, SU_NAME, "geek");
        assertFalse("Got wrong result with different file paths: ",
                     mSU.equals(su));

        // Equal:
        su = new ServiceUnit(SA_NAME, SU_NAME, SU_PATH);
        assertTrue("Got wrong result with identical objects: ",
                   mSU.equals(su));

        // Equal with different state values:
        su = new ServiceUnit(SA_NAME, SU_NAME, SU_PATH);
        su.setState(ServiceUnitState.STARTED);
        assertTrue("Got wrong result with different states: ",
                   mSU.equals(su));

        // Equal with different desired state values:
        su = new ServiceUnit(SA_NAME, SU_NAME, SU_PATH);
        su.setDesiredState(ServiceUnitState.STARTED);
        assertTrue("Got wrong result with different states at shutdown: ",
                   mSU.equals(su));
    }

    /**
     * Tests getName with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetName()
        throws Exception
    {
        String name = mSU.getName();

        assertEquals("Got incorrect name: ",
                     SU_NAME, name);
    }

    /**
     * Tests getFilePath with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetFilePath()
        throws Exception
    {
        String path = mSU.getFilePath();

        assertEquals("Got incorrect file path: ",
                     SU_PATH, path);
    }

    /**
     * Tests getServiceAssemblyName with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetServiceAssemblyName()
        throws Exception
    {
        String name = mSU.getServiceAssemblyName();

        assertEquals("Got incorrect name: ",
                     SA_NAME, name);
    }

    /**
     * Tests getState and setState with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetState()
        throws Exception
    {
        mSU.setState(ServiceUnitState.SHUTDOWN);
        assertEquals("Got wrong result with SHUTDOWN state: ",
                     ServiceUnitState.SHUTDOWN, mSU.getState());

        mSU.setState(ServiceUnitState.STOPPED);
        assertEquals("Got wrong result with STOPPED state: ",
                     ServiceUnitState.STOPPED, mSU.getState());

        mSU.setState(ServiceUnitState.STARTED);
        assertEquals("Got wrong result with STARTED state: ",
                     ServiceUnitState.STARTED, mSU.getState());
    }

    /**
     * Tests getStateAsString.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStateAsString()
        throws Exception
    {
        mSU.setState(ServiceUnitState.SHUTDOWN);
        assertEquals("Got wrong result with SHUTDOWN state: ",
                     ServiceUnitState.SHUTDOWN.toString(), mSU.getStateAsString());
 
        mSU.setState(ServiceUnitState.STOPPED);
        assertEquals("Got wrong result with STOPPED state: ",
                     ServiceUnitState.STOPPED.toString(), mSU.getStateAsString());

        mSU.setState(ServiceUnitState.STARTED);
        assertEquals("Got wrong result with STARTED state: ",
                     ServiceUnitState.STARTED.toString(), mSU.getStateAsString());

        mSU.setState(ServiceUnitState.UNKNOWN);
        assertEquals("Got wrong result with invalid state: ",
                     ServiceUnitState.UNKNOWN.toString(), mSU.getStateAsString());
    }

    /**
     * Tests getDesiredState and setDesiredState with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetDesiredState()
        throws Exception
    {
        mSU.setDesiredState(ServiceUnitState.SHUTDOWN);
        assertEquals("Got wrong result with SHUTDOWN state: ",
                     ServiceUnitState.SHUTDOWN, mSU.getDesiredState());

        mSU.setDesiredState(ServiceUnitState.STOPPED);
        assertEquals("Got wrong result with STOPPED state: ",
                     ServiceUnitState.STOPPED, mSU.getDesiredState());

        mSU.setDesiredState(ServiceUnitState.STARTED);
        assertEquals("Got wrong result with STARTED state: ",
                     ServiceUnitState.STARTED, mSU.getDesiredState());
    }

    /**
     * Tests get/setTargetComponent with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetTargetComponent()
        throws Exception
    {
        mSU.setTargetComponent(COMP_NAME);
        assertEquals("Got incorrect Target Component: ",
                     COMP_NAME, mSU.getTargetComponent());
    }

    /**
     * Tests hashCode() with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testHashCode()
        throws Exception
    {
        ServiceUnit su =
            new ServiceUnit("This one is", "different", "/from/the/other/one");

        assertEquals("Got wrong result with same objects: ",
                     mSU.hashCode(), mSU.hashCode());
        assertFalse("Got wrong result with different objects: " +
                    "\nmSU.hashCode() returned " + mSU.hashCode() +
                    "\nsu.hashCode() returned " + su.hashCode(),
                    (mSU.hashCode() == su.hashCode()));
    }

    /**
     * Tests isShutdown() with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsShutdown()
        throws Exception
    {
        mSU.setState(ServiceUnitState.SHUTDOWN);
        assertTrue("Got wrong result with SHUTDOWN state: ",
                   mSU.isShutdown());
        mSU.setState(ServiceUnitState.STOPPED);
        assertFalse("Got wrong result with STOPPED state: ",
                    mSU.isShutdown());
        mSU.setState(ServiceUnitState.STARTED);
        assertFalse("Got wrong result with STARTED state: ",
                    mSU.isShutdown());
    }

    /**
     * Tests isStarted() with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsStarted()
        throws Exception
    {
        mSU.setState(ServiceUnitState.SHUTDOWN);
        assertFalse("Got wrong result with SHUTDOWN state: ",
                    mSU.isStarted());
        mSU.setState(ServiceUnitState.STOPPED);
        assertFalse("Got wrong result with STOPPED state: ",
                    mSU.isStarted());
        mSU.setState(ServiceUnitState.STARTED);
        assertTrue("Got wrong result with STARTED state: ",
                   mSU.isStarted());
    }

    /**
     * Tests isStopped() with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsStopped()
        throws Exception
    {
        mSU.setState(ServiceUnitState.SHUTDOWN);
        assertFalse("Got wrong result with SHUTDOWN state: ",
                    mSU.isStopped());
        mSU.setState(ServiceUnitState.STOPPED);
        assertTrue("Got wrong result with STOPPED state: ",
                   mSU.isStopped());
        mSU.setState(ServiceUnitState.STARTED);
        assertFalse("Got wrong result with STARTED state: ",
                    mSU.isStopped());
    }

    /**
     * Tests setShutdown with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetShutdown()
        throws Exception
    {
        // Set STOPPED service unit to SHUTDOWN. This is allowed.
        mSU.setState(ServiceUnitState.STOPPED);
        mSU.setShutdown();
        assertEquals("Failure in setShutdown from STOPPED state: ",
                     ServiceUnitState.SHUTDOWN, mSU.getState());

        // Set STARTED service unit to SHUTDOWN. This is not allowed.
        try
        {
            mSU.setState(ServiceUnitState.STARTED);
            mSU.setShutdown();
            fail("Expected exception not received on setShutdown() from " +
                 "STARTED state");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state cannot change")));
        }
    }

    /**
     * Tests setStarted with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetStarted()
        throws Exception
    {
        // Set STOPPED service unit to STARTED. This is allowed.
        mSU.setState(ServiceUnitState.STOPPED);
        mSU.setStarted();
        assertEquals("Failure in setStarted from STOPPED state: ",
                     ServiceUnitState.STARTED, mSU.getState());

        // Set SHUTDOWN service unit to STARTED. This is not allowed.
        try
        {
            mSU.setState(ServiceUnitState.SHUTDOWN);
            mSU.setStarted();
            fail("Expected exception not received on setStarted() from " +
                 "SHUTDOWN state");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("state cannot change")));
        }
    }

    /**
     * Tests setStopped with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetStopped()
        throws Exception
    {
        // Set SHUTDOWN service unit to STOPPED. This is allowed.
        mSU.setState(ServiceUnitState.SHUTDOWN);
        mSU.setStopped();
        assertEquals("Failure in setStopped from SHUTDOWN state: ",
                     ServiceUnitState.STOPPED, mSU.getState());

        // Set STARTED service unit to STOPPED. This is allowed.
        mSU.setState(ServiceUnitState.STARTED);
        mSU.setStopped();
        assertEquals("Failure in setStopped from STARTED state: ",
                     ServiceUnitState.STOPPED, mSU.getState());
    }

    /**
     * Tests toString with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testToString()
        throws Exception
    {
        // Identical instances should produce identical output:
        ServiceUnit su = new ServiceUnit(SA_NAME, SU_NAME, SU_PATH);
        assertEquals("Failure in toString with equal objects: ",
                     mSU.toString(), su.toString());

        // Different instances should produce different output:
        su.setState(ServiceUnitState.STARTED);
        assertFalse("Failure in toString with different objects: " +
                    "\nmSU.toString() returned " + mSU.toString() +
                    "\nsu.toString() returned " + su.toString(),
                    mSU.toString().equals(su.toString()));
    }
}
