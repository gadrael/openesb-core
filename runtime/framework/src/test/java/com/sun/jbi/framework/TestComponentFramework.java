/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentFramework.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentState;
import com.sun.jbi.management.ComponentInstallationContext;

import javax.management.ObjectName;
import java.io.File;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests for the ComponentFramework.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentFramework
{
    /**
     * Helper class to setup environment for testing.
     */
    private EnvironmentSetup mSetup;

    /**
     * Instance of the EnvironmentContext class
     */
    private EnvironmentContext mContext;

    /**
     * Local instance of the ComponentInstallationContext class
     */
    private ComponentInstallationContext mBindingContext;

    /**
     * Local instance of the ComponentInstallationContext class
     */
    private ComponentInstallationContext mEngineContext;

    /**
     * Instance of the ComponentFramework class
     */
    private ComponentFramework mCompFW;

    /**
     * Local instance of the ComponentRegistry class
     */
    private ComponentRegistry mCompReg;

    /**
     * Local instance of the binding bootstrap class path array
     */
    private ArrayList mBindingBootClassPath;

    /**
     * Local instance of the binding lifecycle class path array
     */
    private ArrayList mBindingLifeClassPath;

    /**
     * Local instance of the engine bootstrap class path array
     */
    private ArrayList mEngineBootClassPath;

    /**
     * Local instance of the engine lifecycle class path array
     */
    private ArrayList mEngineLifeClassPath;

    /**
     * Installation root directory for components.
     */
    private String mInstallRoot;

    /**
     * Workspace root directory for components.
     */
    private String mWorkspaceRoot;

    /**
     * Local instance of the SharedLibrary element list
     */
    private ArrayList mSl1Elements;

    /**
     * Local instance of the SharedLibrary element list
     */
    private ArrayList mSl2Elements;

    /**
     * Local handle to StringTranslator
     */
    private StringTranslator mTranslator;
    
    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    @Before
    public void setUp()
        throws Exception
    {
        String srcroot = Utils.getSourceRoot() + "/";
        srcroot = srcroot.replace('\\', '/');

        // Set up directories for component tests. Note that the current
        // working directory is where these are set up. The test always
        // runs in the $SRCROOT/runtime/framework directory.

        mInstallRoot = "target";
        mWorkspaceRoot = "target/workspace";
        File f = new File(mWorkspaceRoot);
        if ( f.exists() )
        {
            File[] fl = f.listFiles();
            if ( null != fl )
            { 
                for (File fl1 : fl) {
                    fl1.delete();
                }   
            }
        }
        else
        {
            f.mkdir();
        }

        // Create and initialize the EnvironmentContext. Create, initialize,
        // and start up the Component Registry and Framework.

        mSetup = new EnvironmentSetup();
        mContext = mSetup.getEnvironmentContext();
        mSetup.startup(true, true);
        mCompReg = mContext.getComponentRegistry();
        mCompFW = mContext.getComponentFramework();
        mTranslator = (StringTranslator) mContext.getStringTranslatorFor(mCompFW);
        
        // Create binding class path / class name lists

        mBindingBootClassPath = new ArrayList();
        mBindingBootClassPath.add(srcroot + Constants.BC_BOOTSTRAP_CLASS_PATH);
        mBindingLifeClassPath = new ArrayList();
        mBindingLifeClassPath.add(Constants.BC_LIFECYCLE_CLASS_PATH);

        // Create engine class path lists

        mEngineBootClassPath = new ArrayList();
        mEngineBootClassPath.add(srcroot + Constants.SE_BOOTSTRAP_CLASS_PATH);
        mEngineLifeClassPath = new ArrayList();
        mEngineLifeClassPath.add(Constants.SE_LIFECYCLE_CLASS_PATH);

        // Create two SharedLibrary instances

        mSl1Elements = new ArrayList();
        mSl1Elements.add(srcroot + "antbld/lib/jbi.jar");
        mSl1Elements.add(srcroot + "framework/bld/src/com/sun/jbi/framework");
 
        mSl2Elements = new ArrayList();
        mSl2Elements.add(srcroot + "antbld/lib/jbi_ri_rt.jar");

        // Create installation contexts for the Binding and Engine

        mBindingContext =
            new ComponentInstallationContext(
                Constants.BC_NAME,
                ComponentInstallationContext.BINDING,
                Constants.BC_LIFECYCLE_CLASS_NAME,
                mBindingLifeClassPath,
                null);
        mBindingContext.setInstallRoot(mInstallRoot);
        mBindingContext.setWorkspaceRoot(mWorkspaceRoot);
        mBindingContext.setDescription(Constants.BC_DESCRIPTION);

        mEngineContext =
            new ComponentInstallationContext(
                Constants.SE_NAME,
                ComponentInstallationContext.ENGINE,
                Constants.SE_LIFECYCLE_CLASS_NAME,
                mEngineLifeClassPath,
                null);
        mEngineContext.setInstallRoot(mInstallRoot);
        mEngineContext.setWorkspaceRoot(mWorkspaceRoot);
        mEngineContext.setDescription(Constants.SE_DESCRIPTION);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    @After
    public void tearDown()
        throws Exception
    {
        mSetup.shutdown(true, true);
    }

// =============================  test methods ================================

    /**
     * Tests loadBootstrap with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingGood()
        throws Exception
    {
        ObjectName mbn;

        // First test loading the bootstrap for an install.

        mBindingContext.setIsInstall(true);
        mbn = mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);

        // The component should now be in LOADED state, and should have an
        // InstallerMBean registered.

        assertTrue("Failure loading BC",
            mCompReg.getComponent(Constants.BC_NAME).isLoaded());
        assertTrue("Failure registering InstallerMBean",
            mContext.getMBeanServer().isRegistered(mbn));       

        // Now test loading the bootstrap for an uninstall.

        mCompReg.getComponent(Constants.BC_NAME).setShutdown();
        mBindingContext.setIsInstall(false);
        mbn = mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,   
            null, false);
        assertNotNull("Failure loading for uninstall",
            mCompReg.getComponent(Constants.BC_NAME).getBootstrapInstance(false));
        assertTrue("Failure registering InstallerMBean",
            mContext.getMBeanServer().isRegistered(mbn));       
    }

    /**
     * Tests loadBootstrap with a null ComponentInstallationContext parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingBadNullInstallationContext()
        throws Exception
    {
        try
        {
            mCompFW.loadBootstrap(
                null,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("installContext")));
        }
    }
    
    /**
     * Tests loadBootstrap with a null bootstrap class name parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingBadNullBootstrapClassName()
        throws Exception
    {
        try
        {
            mCompFW.loadBootstrap(
                mBindingContext,
                null,
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassName")));
        }
    }

    /**
     * Tests loadBootstrap with an empty bootstrap class name parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingBadEmptyBootstrapClassName()
        throws Exception
    {
        try
        {
            mCompFW.loadBootstrap(
                mBindingContext,
                "",
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassName")));
        }
    }

    /**
     * Tests loadBootstrap with a null bootstrap class path parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingBadNullBootstrapClassPath()
        throws Exception
    {
        try
        {
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                null,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassPathElements")));
        }
    }

    /**
     * Tests loadBootstrap with an empty bootstrap class path parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingBadEmptyBootstrapClassPath()
        throws Exception
    {
        ArrayList emptyList = new ArrayList();
        try
        {
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                emptyList,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassPathElements")));
        }
    }

    /**
     * Tests loadBootstrap with a duplicate component name specified. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingBadDuplicate()
        throws Exception
    {
        try
        {
            // Pre-install the component so that the second load will fail.
            mBindingContext.setIsInstall(true);
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath,
                null, false);
            mCompFW.installComponent(mBindingContext);
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("already registered")));
        }
    }

    /**
     * Tests loadBootstrap with a required Shared Library missing. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingBadMissingSharedLibrary()
        throws Exception
    {
        // Create a list of Shared Library IDs that are not installed.
        ArrayList slList = new ArrayList();
        slList.add(Constants.SL_1_NAME);
        slList.add(Constants.SL_2_NAME);
        try
        {
            mBindingContext.setIsInstall(true);
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath,
                slList, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("must be installed")));
        }
    }

    /**
     * Tests loadBootstrap with a bad bootstrap class name. The bootstrap
     * class will not be found when an attempt is made to load it. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingBadBootstrapNotFound()
        throws Exception
    {
        try
        {
            mBindingContext.setIsInstall(true);
            mCompFW.loadBootstrap(
                mBindingContext,
                "bad.bootstrap.class.name",
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getCause().getMessage().indexOf("could not be loaded")));
        }
    }

    /**
     * Tests loadBootstrap with a binding that throws an exception from its
     * bootstrap init() method.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadBootstrapBindingBadBootstrapInitException()
        throws Exception
    {
        mBindingContext.setComponentName(Constants.BC_NAME_BAD_BOOTSTRAP_INIT);
        mBindingContext.setIsInstall(true);
        try
        {
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap init failed")));
        }
    }

    /**
     * Tests loadBootstrap with a binding that throws an exception from its
     * bootstrap init() method and from its bootstrap cleanUp() method.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadBootstrapBindingBadBootstrapInitCleanUpException()
        throws Exception
    {
        mBindingContext.setComponentName(
            Constants.BC_NAME_BAD_BOOTSTRAP_INIT_CLEANUP);
        mBindingContext.setIsInstall(true);
        try
        {
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap init failed")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap cleanup failed")));
        }
    }

    /**
     * Tests loadBootstrap for an uninstall with a non-existent component.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadBootstrapBindingBadNotInstalled()
        throws Exception
    {
        try
        {
            mBindingContext.setIsInstall(false);
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                ( (-1 < ex.getMessage().indexOf("failed"))
                && (-1 < ex.getMessage().indexOf("not installed"))));
        }
    }

    /**
     * Tests loadBootstrap for an uninstall with a loaded component.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadBootstrapBindingBadLoaded()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mBindingContext.setIsInstall(false);
            mCompFW.loadBootstrap(
                mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                ( (-1 < ex.getMessage().indexOf("failed"))
                && (-1 < ex.getMessage().indexOf("not installed"))));
        }
    }

    /**
     * Tests loadBootstrap for an uninstall force with a bad bootstrap class
     * name. The bootstrap class will not be found when an attempt is made to
     * load it, but the installer MBean should still be registered.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapBindingForceBootstrapNotFound()
        throws Exception
    {
        ObjectName mbn;

        // First load the bootstrap for an install to create the required
        // objects.

        mBindingContext.setIsInstall(true);
        mbn = mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);

        // The component should now be in LOADED state. Unregister the installer
        // MBean so that the test verification works.

        assertTrue("Failure loading BC",
            mCompReg.getComponent(Constants.BC_NAME).isLoaded());
        mContext.getMBeanServer().unregisterMBean(mbn);

        // Now test loading the bootstrap for an uninstall force.

        mCompReg.getComponent(Constants.BC_NAME).setShutdown();

        mBindingContext.setIsInstall(false);
        mbn = mCompFW.loadBootstrap(
            mBindingContext,
            "bad.bootstrap.class.name",
            mBindingBootClassPath,
            null, true);

        assertTrue("Failure registering InstallerMBean",
            mContext.getMBeanServer().isRegistered(mbn));       
    }

    /**
     * Tests loadBootstrap with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineGood()
        throws Exception
    {
        // Test loading for an install

        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        assertTrue("Failure loading for install",
            mCompReg.getComponent(Constants.SE_NAME).isLoaded());

        // Test loading for an uninstall

        mCompReg.getComponent(Constants.SE_NAME).setShutdown();
        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        assertNotNull("Failure loading for uninstall",
            mCompReg.getComponent(Constants.SE_NAME).getBootstrapInstance(false));
    }

    /**
     * Tests loadBootstrap with a null ComponentInstallationContext parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineBadNullInstallationContext()
        throws Exception
    {
        try
        {
            mCompFW.loadBootstrap(
                null,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("installContext")));
        }
    }

    /**
     * Tests loadBootstrap with a null bootstrap class name parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineBadNullBootstrapClassName()
        throws Exception
    {
        try
        {
            mCompFW.loadBootstrap(
                mEngineContext,
                null,
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassName")));
        }
    }

    /**
     * Tests loadBootstrap with an empty bootstrap class name parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineBadEmptyBootstrapClassName()
        throws Exception
    {
        try
        {
            mCompFW.loadBootstrap(
                mEngineContext,
                "",
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassName")));
        }
    }

    /**
     * Tests loadBootstrap with a null bootstrap class path parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineBadNullBootstrapClassPath()
        throws Exception
    {
        try
        {
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                null,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassPathElements")));
        }
    }

    /**
     * Tests loadBootstrap with an empty bootstrap class path parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineBadEmptyBootstrapClassPath()
        throws Exception
    {
        ArrayList emptyList = new ArrayList();
        try
        {
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                emptyList,
                null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassPathElements")));
        }
    }

    /**
     * Tests loadBootstrap with a duplicate component name specified. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineBadDuplicate()
        throws Exception
    {
        try
        {
            // Pre-install the component so that the second load will fail.
            mEngineContext.setIsInstall(true);
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                mEngineBootClassPath,
                null, false);
            mCompFW.installComponent(mEngineContext);
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("already registered")));
        }
    }

    /**
     * Tests loadBootstrap with a required Shared Library missing. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineBadMissingSharedLibrary()
        throws Exception
    {
        // Create a list of Shared Library IDs that are not installed.
        ArrayList slList = new ArrayList();
        slList.add(Constants.SL_1_NAME);
        slList.add(Constants.SL_2_NAME);
        try
        {
            mEngineContext.setIsInstall(true);
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                mEngineBootClassPath,
                slList, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("must be installed")));
        }
    }

    /**
     * Tests loadBootstrap with a bad bootstrap class name. The bootstrap
     * class will not be found when an attempt is made to load it. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineBadBootstrapNotFound()
        throws Exception
    {
        try
        {
            mEngineContext.setIsInstall(true);
            mCompFW.loadBootstrap(
                mEngineContext,
                "bad.bootstrap.class.name",
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getCause().getMessage().indexOf(
                    "could not be loaded")));
        }
    }

    /**
     * Tests loadBootstrap with an engine that throws an exception from its
     * bootstrap init() method.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadBootstrapEngineBadBootstrapInitException()
        throws Exception
    {
        mEngineContext.setComponentName(Constants.SE_NAME_BAD_BOOTSTRAP_INIT);
        mEngineContext.setIsInstall(true);
        try
        {
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap init failed")));
        }
    }

    /**
     * Tests loadBootstrap with an engine that throws an exception from its
     * bootstrap init() method and from its bootstrap cleanUp() method.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadBootstrapEngineBadBootstrapInitCleanUpException()
        throws Exception
    {
        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_INIT_CLEANUP);
        mEngineContext.setIsInstall(true);
        try
        {
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap init failed")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap cleanup failed")));
        }
    }

    /**
     * Tests loadBootstrap with for an uninstall with a non-existent component.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadBootstrapEngineBadNotInstalled()
        throws Exception
    {
        try
        {
            mEngineContext.setIsInstall(false);
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                ( (-1 < ex.getMessage().indexOf("failed"))
                && (-1 < ex.getMessage().indexOf("not installed"))));
        }
    }

    /**
     * Tests loadBootstrap with for an uninstall with a loaded component.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadBootstrapEngineBadLoaded()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mEngineContext.setIsInstall(false);
            mCompFW.loadBootstrap(
                mEngineContext,
                Constants.SE_BOOTSTRAP_CLASS_NAME,
                mEngineBootClassPath,
                null, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                ( (-1 < ex.getMessage().indexOf("failed"))
                && (-1 < ex.getMessage().indexOf("not installed"))));
        }
    }

    /**
     * Tests loadBootstrap for an uninstall force with a bad bootstrap class
     * name. The bootstrap class will not be found when an attempt is made to
     * load it, but the installer MBean should still be registered.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void loadBootstrapEngineForceBootstrapNotFound()
        throws Exception
    {
        ObjectName mbn;

        // First load the bootstrap for an install to create the required
        // objects.

        mEngineContext.setIsInstall(true);
        mbn = mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);

        // The component should now be in LOADED state. Unregister the installer
        // MBean so that the test verification works.

        assertTrue("Failure loading SE",
            mCompReg.getComponent(Constants.SE_NAME).isLoaded());
        mContext.getMBeanServer().unregisterMBean(mbn);

        // Now test loading the bootstrap for an uninstall force.

        mCompReg.getComponent(Constants.SE_NAME).setShutdown();

        mEngineContext.setIsInstall(false);
        mbn = mCompFW.loadBootstrap(
            mEngineContext,
            "bad.bootstrap.class.name",
            mEngineBootClassPath,
            null, true);

        assertTrue("Failure registering InstallerMBean",
            mContext.getMBeanServer().isRegistered(mbn));       
    }

    /**
     * Tests getInstallerConfigurationMBeanName for a BC with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void getInstallerConfigurationMBeanNameGoodBinding()
        throws Exception
    {
        ObjectName objectName;

        // Load the bootstrap for install.
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);

        // Get the installer configuration MBean object name
        objectName = mCompFW.getInstallerConfigurationMBeanName(Constants.BC_NAME);
        assertNotNull("Failure getting InstallerConfigurationMBean name",
            objectName);

        // Now load the bootstrap for uninstall.
        mCompReg.getComponent(Constants.BC_NAME).setShutdown();
        mBindingContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);

        // Get the installer configuration MBean object name
        objectName = mCompFW.getInstallerConfigurationMBeanName(Constants.BC_NAME);
        assertNotNull("Failure getting InstallerConfigurationMBean name",
            objectName);
    }

    /**
     * Tests getInstallerConfigurationMBeanName for a BC with a bad result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void getInstallerConfigurationMBeanNameBadBinding()
        throws Exception
    {
        ObjectName objectName;

        // Try to get the installer configuration MBean object name for
        // a BC that is not in the registry.
        try
        {
            objectName = mCompFW.getInstallerConfigurationMBeanName(Constants.BC_NAME);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("not found")));
        }
    }

    /**
     * Tests getInstallerConfigurationMBeanName for an SE with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void getInstallerConfigurationMBeanNameGoodEngine()
        throws Exception
    {
        ObjectName objectName;

        // Load the bootstrap for install.
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);

        // Get the installer configuration MBean object name
        objectName = mCompFW.getInstallerConfigurationMBeanName(Constants.SE_NAME);
        assertNotNull("Failure getting InstallerConfigurationMBean name",
            objectName);

        // Now load the bootstrap for uninstall.
        mCompReg.getComponent(Constants.SE_NAME).setShutdown();
        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);

        // Get the installer configuration MBean object name
        objectName = mCompFW.getInstallerConfigurationMBeanName(Constants.SE_NAME);
        assertNotNull("Failure getting InstallerConfigurationMBean name",
            objectName);
    }

    /**
     * Tests getInstallerConfigurationMBeanName for an SE with a bad result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void getInstallerConfigurationMBeanNameBadEngine()
        throws Exception
    {
        ObjectName objectName;

        // Try to get the installer configuration MBean object name for
        // an SE that is not in the registry.
        try
        {
            objectName = mCompFW.getInstallerConfigurationMBeanName(Constants.SE_NAME);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("not found")));
        }
    }

    /**
     * Tests installComponent for a BC with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingGood()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        assertTrue("Got wrong status for binding. Expected installed, got " +
            comp.getStatusAsString(), comp.isInstalled());

        // Verify that all the MBeans were created and registered
        assertTrue("LifeCycleMBean not created for binding",
            mContext.getMBeanServer().isRegistered(comp.getLifeCycleMBeanName()));
        assertTrue("DeployerMBean not created for binding",
            mContext.getMBeanServer().isRegistered(comp.getDeployerMBeanName()));
        assertTrue("StatisticsMBean not created for binding",
            mContext.getMBeanServer().isRegistered(comp.getStatisticsMBeanName()));
        assertTrue("LoggerMBean not created for binding",
            mContext.getMBeanServer().isRegistered(comp.getLoggerMBeanName()));
        assertTrue("ConfigurationMBean not created for binding",
            mContext.getMBeanServer().isRegistered(comp.getConfigurationMBeanName()));
    }

    /**
     * Tests installComponent with a modified class path and a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingGoodModifiedClassPath()
        throws Exception
    {
        mBindingContext.setComponentName(
            Constants.BC_NAME_BOOTSTRAP_MODIFY_CLASS_PATH);
        mBindingContext.setComponentClassName(
                Constants.BC_LIFECYCLE_CLASS_NAME_ALTERNATE);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp =
            mCompReg.getComponent(Constants.BC_NAME_BOOTSTRAP_MODIFY_CLASS_PATH);
        assertTrue("Got wrong status for binding. Expected installed, got " +
            comp.getStatusAsString(),
            comp.isInstalled());
        Object instance = comp.getLifeCycleInstance(true);
        assertTrue("Got wrong instance type of life cycle class: " +
            "Expected " + Constants.BC_LIFECYCLE_CLASS_NAME_ALTERNATE + ", got " +
            instance.getClass().getName(),
            instance instanceof testbinding.Binding);
    }

    /**
     * Tests installComponent with a null ComponentInstallationContext.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadNullInstallationContext()
        throws Exception
    {
        try
        {
            mCompFW.installComponent(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("installContext")));
        }
    }

    /**
     * Tests installComponent with a component name that was never loaded. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadNotLoaded()
        throws Exception
    {
        try
        {
            mCompFW.installComponent(mBindingContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("not yet been loaded")));
        }
    }

    /**
     * Tests installComponent with a component whose bootstrap class is no longer
     * loaded. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadBootstrapNotLoaded()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompReg.getComponent(Constants.BC_NAME).clearBootstrapInstance();
        try
        {
            mCompFW.installComponent(mBindingContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("has not been loaded")));
        }
    }

    /**
     * Tests installComponent with a binding whose bootstrap onInstall() method
     * throws an exception. 
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadOnInstallException()
        throws Exception
    {
        mBindingContext.setComponentName(
            Constants.BC_NAME_BAD_BOOTSTRAP_ONINSTALL);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mBindingContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap onInstall()")));
        }
    }

    /**
     * Tests installComponent with a binding whose bootstrap cleanUp() method
     * throws an exception. 
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadCleanUpException()
        throws Exception
    {
        mBindingContext.setComponentName(
            Constants.BC_NAME_BAD_BOOTSTRAP_INSTALL_CLEANUP);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mBindingContext);
            fail("Expected exception not received");
        }
        catch (com.sun.jbi.framework.FrameworkWarningException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap cleanUp()")));
        }
    }

    /**
     * Tests installComponent with a binding whose bootstrap onInstall() and
     * cleanUp() methods both throw exceptions. The client should see the
     * onInstall() exception, not the cleanUp() exception. The cleanUp()
     * exception should be logged.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadOnInstallCleanupExceptions()
        throws Exception
    {
        mBindingContext.setComponentName(
            Constants.BC_NAME_BAD_BOOTSTRAP_ONINSTALL_CLEANUP);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mBindingContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap onInstall()")));
        }
    }

    /**
     * Tests installComponent with a bad life cycle class name. The life cycle
     * class will not be found when an attempt is made to load it. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadLifeCycleNotFound()
        throws Exception
    {
        mBindingContext.setComponentClassName("bad.life.cycle.class.name");
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mBindingContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getCause().getMessage().indexOf(
                    "could not be loaded")));
        }
    }

    /**
     * Tests installComponent with a binding that returns null from its
     * getLifeCycle() method.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadGetLifeCycleNull()
        throws Exception
    {
        mBindingContext.setComponentClassName(
            Constants.BC_LIFECYCLE_CLASS_NAME_NULL_GET_LIFECYCLE);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mBindingContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getCause().getMessage().indexOf("JBIFW2010")));
        }
    }

    /**
     * Tests installComponent with a binding that throws an exception from its
     * getLifeCycle() method.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installBindingBadGetLifeCycleException()
        throws Exception
    {
        mBindingContext.setComponentClassName(
            Constants.BC_LIFECYCLE_CLASS_NAME_BAD_GET_LIFECYCLE);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mBindingContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getCause().getMessage().indexOf("JBIFW2009")));
        }
    }

    /**
     * Tests installComponent with a good result. 
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineGood()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(Constants.SE_NAME);
        assertTrue("Got wrong status for engine. Expected installed, got " +
            comp.getStatusAsString(), comp.isInstalled());

        // Verify that all the MBeans were created and registered
        assertTrue("LifeCycleMBean not created for engine",
            mContext.getMBeanServer().isRegistered(comp.getLifeCycleMBeanName()));
        assertTrue("DeployerMBean not created for engine",
            mContext.getMBeanServer().isRegistered(comp.getDeployerMBeanName()));
        assertTrue("StatisticsMBean not created for engine",
            mContext.getMBeanServer().isRegistered(comp.getStatisticsMBeanName()));
        assertTrue("LoggerMBean not created for engine",
            mContext.getMBeanServer().isRegistered(comp.getLoggerMBeanName()));
        
        assertTrue("ConfigurationMBean not created for engine",
            mContext.getMBeanServer().isRegistered(comp.getConfigurationMBeanName()));
    }

    /**
     * Tests installComponent with a modified class path and a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineGoodModifiedClassPath()
        throws Exception
    {
        mEngineContext.setComponentName(
            Constants.SE_NAME_BOOTSTRAP_MODIFY_CLASS_PATH);
        mEngineContext.setComponentClassName(
            Constants.SE_LIFECYCLE_CLASS_NAME_ALTERNATE);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp =
            mCompReg.getComponent(Constants.SE_NAME_BOOTSTRAP_MODIFY_CLASS_PATH);
        assertTrue("Got wrong status for engine. Expected installed, got " +
            comp.getStatusAsString(),
            comp.isInstalled());
        Object instance = comp.getLifeCycleInstance(true);
        assertTrue("Got wrong instance type of life cycle class. " +
            "Expected " + Constants.SE_LIFECYCLE_CLASS_NAME_ALTERNATE + ", got " +
            instance.getClass().getName(),
            instance instanceof testengine.Engine);
    }

    /**
     * Tests installComponent with a null ComponentInstallationContext. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadNullInstallationContext()
        throws Exception
    {
        try
        {
            mCompFW.installComponent(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("installContext")));
        }
    }

    /**
     * Tests installComponent with a component name that was never loaded. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadNotLoaded()
        throws Exception
    {
        try
        {
            mCompFW.installComponent(mEngineContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("not yet been loaded")));
        }
    }

    /**
     * Tests installComponent with a component whose bootstrap class is no longer
     * loaded. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadBootstrapNotLoaded()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompReg.getComponent(Constants.SE_NAME).clearBootstrapInstance();
        try
        {
            mCompFW.installComponent(mEngineContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("has not been loaded")));
        }
    }

    /**
     * Tests installComponent with an engine whose bootstrap onInstall() method
     * throws an exception. 
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadOnInstallException()
        throws Exception
    {
        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_ONINSTALL);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mEngineContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap onInstall()")));
        }
    }

    /**
     * Tests installComponent with an engine whose bootstrap cleanUp() method
     * throws an exception. 
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadCleanUpException()
        throws Exception
    {
        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_INSTALL_CLEANUP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mEngineContext);
            fail("Expected exception not received");
        }
        catch (com.sun.jbi.framework.FrameworkWarningException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap cleanUp()")));
        }
    }

    /**
     * Tests installComponent with an engine whose bootstrap onInstall() and
     * cleanUp() methods both throw exceptions. The client should see the
     * onInstall() exception, not the cleanUp() exception. The cleanUp()
     * exception should be logged.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadOnInstallCleanUpExceptions()
        throws Exception
    {
        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_ONINSTALL_CLEANUP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mEngineContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap onInstall()")));
        }
    }

    /**
     * Tests installComponent with a bad life cycle class name. The life cycle
     * class will not be found when an attempt is made to load it. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadLifeCycleNotFound()
        throws Exception
    {
        mEngineContext.setComponentClassName("bad.life.cycle.class.name");
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mEngineContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getCause().getMessage().indexOf(
                    "could not be loaded")));
        }
    }

    /**
     * Tests installComponent with an engine that returns null from its
     * getLifeCycle() method.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadGetLifeCycleNull()
        throws Exception
    {
        mEngineContext.setComponentClassName(
            Constants.SE_LIFECYCLE_CLASS_NAME_NULL_GET_LIFECYCLE);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mEngineContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getCause().getMessage().indexOf("JBIFW2010")));
        }
    }

    /**
     * Tests installComponent with an engine that throws an exception from its
     * getLifeCycle() method.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installEngineBadGetLifeCycleException()
        throws Exception
    {
        mEngineContext.setComponentClassName(
            Constants.SE_LIFECYCLE_CLASS_NAME_BAD_GET_LIFECYCLE);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.installComponent(mEngineContext);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getCause().getMessage().indexOf("JBIFW2009")));
        }
    }

    /**
     * Tests installSharedLibrary with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installSharedLibraryGood()
        throws Exception
    {
        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);
        assertNotNull("Failure installing Shared Library",
            mCompReg.getSharedLibrary(Constants.SL_1_NAME));
        

    }

    /**
     * Tests installSharedLibrary with a null name.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installSharedLibraryBadNullName()
        throws Exception
    {
        try
        {
            mCompFW.installSharedLibrary(null,
                                         Constants.SL_1_DESC,
                                         Constants.SL_1_ROOT,
                                         false,
                                         mSl1Elements);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests installSharedLibrary with an empty name.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installSharedLibraryBadEmptyName()
        throws Exception
    {
        try
        {
            mCompFW.installSharedLibrary("",
                                         Constants.SL_1_DESC,
                                         Constants.SL_1_ROOT,
                                         false,
                                         mSl1Elements);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Empty string argument")));
        }
    }

    /**
     * Tests installSharedLibrary with a null component root.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installSharedLibraryBadNullComponentRoot()
        throws Exception
    {
        try
        {
            mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                         Constants.SL_1_DESC,
                                         null,
                                         false,
                                         mSl1Elements);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests installSharedLibrary with an empty component root.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installSharedLibraryBadEmptyComponentRoot()
        throws Exception
    {
        try
        {
            mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                         Constants.SL_1_DESC,
                                         "",
                                         false,
                                         mSl1Elements);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Empty string argument")));
        }
    }

    /**
     * Tests installSharedLibrary with a null class path.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installSharedLibraryBadNullClassPath()
        throws Exception
    {
        try
        {
            mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                         Constants.SL_1_DESC,
                                         Constants.SL_1_ROOT,
                                         false,
                                         null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests installSharedLibrary with an empty class path.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installSharedLibraryBadEmptyClassPath()
        throws Exception
    {
        try
        {
            mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                         Constants.SL_1_DESC,
                                         Constants.SL_1_ROOT,
                                         false,
                                         new ArrayList());
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Empty list argument")));
        }
    }

    /**
     * Tests installSharedLibrary with a duplicate component name.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void installSharedLibraryBadDuplicate()
        throws Exception
    {
        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);
        try
        {
            mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                         Constants.SL_1_DESC,
                                         Constants.SL_1_ROOT,
                                         false,
                                         mSl1Elements);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("already registered")));
        }
    }

    /**
     * Tests uninstallComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingGood()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        mBindingContext.setIsInstall(false);
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        ObjectName lcmb = comp.getLifeCycleMBeanName();
        ObjectName dmb = comp.getDeployerMBeanName();
        ObjectName smb = comp.getStatisticsMBeanName();
        ObjectName lmb = comp.getLoggerMBeanName();
        ObjectName ccmb = comp.getConfigurationMBeanName();
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.uninstallComponent(mBindingContext, false);
        assertTrue("Binding " + Constants.BC_NAME + " is still registered",
            comp.isLoaded());

        // Verify that all the MBeans were destroyed and unregistered
        assertFalse("LifeCycleMBean not removed for binding",
            mContext.getMBeanServer().isRegistered(lcmb));
        assertFalse("DeployerMBean not removed for binding",
            mContext.getMBeanServer().isRegistered(dmb));
        assertFalse("StatisticsMBean not removed for binding",
            mContext.getMBeanServer().isRegistered(smb));
        assertFalse("LoggerMBean not removed for binding",
            mContext.getMBeanServer().isRegistered(lmb));
        assertFalse("ConfigurationMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(ccmb));
    }

    /**
     * Tests uninstallComponent with a null ComponentInstallationContext
     * parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingBadNullInstallationContext()
        throws Exception
    {
        try
        {
            mCompFW.uninstallComponent(null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests uninstallComponent with an nonexistent binding. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingBadNotFound()
        throws Exception
    {
        try
        {
            mCompFW.uninstallComponent(mBindingContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("no Binding installed")));
        }
    }

    /**
     * Tests uninstallComponent with binding whose bootstrap onUninstall() method
     * throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingBadOnUninstallException()
        throws Exception
    {
        mBindingContext.setComponentName(
            Constants.BC_NAME_BAD_BOOTSTRAP_ONUNINSTALL);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        mBindingContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mBindingContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap onUninstall()")));
        }
    }

    /**
     * Tests uninstallComponent with binding whose bootstrap cleanUp() method
     * throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingBadCleanUpException()
        throws Exception
    {
        mBindingContext.setComponentName(
            Constants.BC_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        mBindingContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mBindingContext, false);
            fail("Expected exception not received");
        }
        catch (com.sun.jbi.framework.FrameworkWarningException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap cleanUp()")));
        }
    }

    /**
     * Tests uninstallComponent with binding whose bootstrap onUninstall() and
     * cleanUp() methods both throw exceptions. The client should see the
     * onUninstall() exception, not the cleanUp() exception. The cleanUp()
     * exception is only logged.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingBadOnUninstallCleanUpExceptions()
        throws Exception
    {
        mBindingContext.setComponentName(
            Constants.BC_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        mBindingContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mBindingContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap onUninstall()")));
        }
    }

    /**
     * Tests uninstallComponent with the binding started. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingBadStarted()
        throws Exception
    {
        // Load, install, and start the binding.

        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(comp);
        mBindingContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mBindingContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("is still running")));
        }
    }

    /**
     * Tests uninstallComponent with the binding stopped. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingBadStopped()
        throws Exception
    {
        // Load, install, start, and stop the binding.

        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(comp);
        mCompFW.stopComponent(comp);
        mBindingContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mBindingContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("is stopped")));
        }
    }

    /**
     * Tests uninstallComponent with a binding that still has SUs deployed to it.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallBindingBadDeployed()
        throws Exception
    {
        // Install the binding.

        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);

        // Now fake up a deployment.

        ServiceUnit su1 = new ServiceUnit(
            "ServiceAssembly1", "ServiceUnit1", "sus/ServiceUnit1");
        mCompReg.getComponent(Constants.BC_NAME).addServiceUnit(su1);

        // Try to uninstall. This should fail.

        mBindingContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mBindingContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1118")));
        }
    }


    /**
     * Tests uninstallComponent with good results.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineGood()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        mEngineContext.setIsInstall(false);
        Component comp = mCompReg.getComponent(Constants.SE_NAME);
        ObjectName lcmb = comp.getLifeCycleMBeanName();
        ObjectName dmb = comp.getDeployerMBeanName();
        ObjectName smb = comp.getStatisticsMBeanName();
        ObjectName lmb = comp.getLoggerMBeanName();
        ObjectName ccmb = comp.getConfigurationMBeanName();
        
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.uninstallComponent(mEngineContext, false);
        assertTrue("Engine " + Constants.SE_NAME + " is still registered",
            comp.isLoaded());

        // Verify that all the MBeans were destroyed and unregistered
        assertFalse("LifeCycleMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(lcmb));
        assertFalse("DeployerMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(dmb));
        assertFalse("StatisticsMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(smb));
        assertFalse("LoggerMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(lmb));
        assertFalse("ConfigurationMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(ccmb));
    }

    /**
     * Tests uninstallComponent with a null ComponentInstallationContext
     * parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineBadNullInstallationContext()
        throws Exception
    {
        try
        {
            mCompFW.uninstallComponent(null, false);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests uninstallComponent with an nonexistent engine. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineBadNotFound()
        throws Exception
    {
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("no Engine")));
        }
    }

    /**
     * Tests uninstallComponent with an engine whose bootstrap onUninstall()
     * method throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineBadOnUninstallException()
        throws Exception
    {
        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap onUninstall()")));
        }
    }

    /**
     * Tests uninstallComponent with an engine whose bootstrap cleanUp()
     * method throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineBadCleanUpException()
        throws Exception
    {
        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (com.sun.jbi.framework.FrameworkWarningException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap cleanUp()")));
        }
    }

    /**
     * Tests uninstallComponent with an engine whose bootstrap onUninstall() and
     * cleanUp() methods both throw exceptions. The client should see the
     * onUninstall() exception, not the cleanUp() exception. The cleanUp()
     * exception is only logged.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineBadOnUninstallCleanupExceptions()
        throws Exception
    {
        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootstrap onUninstall()")));
        }
    }

    /**
     * Tests uninstallComponent with the engine started. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineBadStarted()
        throws Exception
    {
        // Load, install, and start the engine.

        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(comp);
        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("is still running")));
        }
    }

    /**
     * Tests uninstallComponent with the engine stopped. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineBadStopped()
        throws Exception
    {
        // Load, install, start, and stop the engine.

        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(comp);
        mCompFW.stopComponent(comp);
        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("is stopped")));
        }
    }

    /**
     * Tests uninstallComponent with an engine that still has SUs deployed.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallEngineBadDeployed()
        throws Exception
    {
        // Install the engine.

        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);

        // Now fake up a deployment.

        ServiceUnit su1 = new ServiceUnit(
            "ServiceAssembly1", "ServiceUnit1", "sus/ServiceUnit1");
        mCompReg.getComponent(Constants.SE_NAME).addServiceUnit(su1);

        // Try to uninstall. This should fail.

        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1118")));
        }
    }

    /**
     * Tests uninstallComponent with the force option with a component that
     * throws an exception in its onUninstall() method.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallForceGoodOnUninstallException()
        throws Exception
    {
        // Install the engine.

        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(
            Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL);
        ObjectName lcmb = comp.getLifeCycleMBeanName();
        ObjectName dmb = comp.getDeployerMBeanName();
        ObjectName smb = comp.getStatisticsMBeanName();
        ObjectName lmb = comp.getLoggerMBeanName();

        // Try to uninstall. This should fail.

        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1117")));
        }

        // Try to uninstall with force set. This should succeed.

        mCompFW.uninstallComponent(mEngineContext, true);
        assertTrue("Engine " + Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL +
            " is still registered",
            comp.isLoaded());

        // Verify that all the MBeans were destroyed and unregistered
        assertFalse("LifeCycleMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(lcmb));
        assertFalse("DeployerMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(dmb));
        assertFalse("StatisticsMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(smb));
        assertFalse("LoggerMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(lmb));
    }

    /**
     * Tests uninstallComponent with the force option with a component that
     * throws an exception in its cleanUp() method.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallForceGoodCleanUpException()
        throws Exception
    {
        // Install the engine.

        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(
            Constants.SE_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP);
        ObjectName lcmb = comp.getLifeCycleMBeanName();
        ObjectName dmb = comp.getDeployerMBeanName();
        ObjectName smb = comp.getStatisticsMBeanName();
        ObjectName lmb = comp.getLoggerMBeanName();

        // Try to uninstall. This should fail.

        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, true);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1121")));
        }

        // Try to uninstall with force set. This should succeed.

        mCompFW.uninstallComponent(mEngineContext, true);
        assertTrue("Engine " +
            Constants.SE_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP +
            " is still registered",
            comp.isLoaded());

        // Verify that all the MBeans were destroyed and unregistered
        assertFalse("LifeCycleMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(lcmb));
        assertFalse("DeployerMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(dmb));
        assertFalse("StatisticsMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(smb));
        assertFalse("LoggerMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(lmb));
    }

    /**
     * Tests uninstallComponent with the force option with a component that
     * throws an exception in both its onUninstall() and its cleanUp() methods.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallForceGoodOnUninstallCleanUpException()
        throws Exception
    {
        // Install the engine.

        mEngineContext.setComponentName(
            Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(
            Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP);
        ObjectName lcmb = comp.getLifeCycleMBeanName();
        ObjectName dmb = comp.getDeployerMBeanName();
        ObjectName smb = comp.getStatisticsMBeanName();
        ObjectName lmb = comp.getLoggerMBeanName();

        // Try to uninstall. This should fail.

        mEngineContext.setIsInstall(false);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, true);
        try
        {
            mCompFW.uninstallComponent(mEngineContext, false);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1117")));
        }

        // Try to uninstall with force set. This should succeed.

        mCompFW.uninstallComponent(mEngineContext, true);
        assertTrue("Engine " +
            Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP +
            " is still registered",
            comp.isLoaded());

        // Verify that all the MBeans were destroyed and unregistered
        assertFalse("LifeCycleMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(lcmb));
        assertFalse("DeployerMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(dmb));
        assertFalse("StatisticsMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(smb));
        assertFalse("LoggerMBean not removed for engine",
            mContext.getMBeanServer().isRegistered(lmb));
    }

    /**
     * Tests uninstallSharedLibrary with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallSharedLibraryGood()
        throws Exception
    {
        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);
        mCompFW.uninstallSharedLibrary(Constants.SL_1_NAME);
        assertFalse("Failure uninstalling Shared Library",
            mCompReg.isComponentRegistered(Constants.SL_1_NAME));
    }

    /**
     * Tests uninstallSharedLibrary with dependent components that are
     * already shutdown.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallSharedLibraryGoodDependents()
        throws Exception
    {
        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);

        mCompFW.installSharedLibrary(Constants.SL_2_NAME,
                                     Constants.SL_2_DESC,
                                     Constants.SL_2_ROOT,
                                     false,
                                     mSl2Elements);

        ArrayList slList1 = new ArrayList();
        slList1.add(Constants.SL_1_NAME);

        ArrayList slList2 = new ArrayList();
        slList2.add(Constants.SL_1_NAME);
        slList2.add(Constants.SL_2_NAME);

        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            slList1, false);
        mCompFW.installComponent(mBindingContext);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            slList2, false);
        mCompFW.installComponent(mEngineContext);

        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_1_NAME);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertFalse("Failure uninstalling Shared Library",
                mCompReg.isComponentRegistered(Constants.SL_1_NAME));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("reinstalled")));
        }

        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_2_NAME);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertFalse("Failure uninstalling Shared Library",
                mCompReg.isComponentRegistered(Constants.SL_2_NAME));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("reinstalled")));
        }
    }

    /**
     * Tests uninstallSharedLibrary with a nonexistent Shared Library.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallSharedLibraryBadNotFound()
        throws Exception
    {
        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_1_NAME);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("No Shared Library")));
        }
    }

    /**
     * Tests uninstallSharedLibrary with dependent components that are
     * either running or stopped. This will fail because all dependent
     * components must be shutdown before the uninstall will proceed.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void uninstallSharedLibraryBadDependents()
        throws Exception
    {
        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);

        mCompFW.installSharedLibrary(Constants.SL_2_NAME,
                                     Constants.SL_2_DESC,
                                     Constants.SL_2_ROOT,
                                     false,
                                     mSl2Elements);

        ArrayList slList1 = new ArrayList();
        slList1.add(Constants.SL_1_NAME);

        ArrayList slList2 = new ArrayList();
        slList2.add(Constants.SL_1_NAME);
        slList2.add(Constants.SL_2_NAME);

        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            slList1, false);
        mCompFW.installComponent(mBindingContext);
        Component bc = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(bc);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            slList2, false);
        mCompFW.installComponent(mEngineContext);
        Component se = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(se);
        mCompFW.stopComponent(se);
        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_1_NAME);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Unable to uninstall")));
        }
        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_2_NAME);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Unable to uninstall")));
        }
    }

    /**
     * Tests unloadBootstrap with a null component name. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void unloadBootstrapBadNullComponentName()
        throws Exception
    {
        try
        {
            mCompFW.unloadBootstrap(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("componentName")));
        }
    }

    /**
     * Tests unloadBootstrap with an empty component name. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void unloadBootstrapBadEmptyComponentName()
        throws Exception
    {
        try
        {
            mCompFW.unloadBootstrap("");
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("componentName")));
        }
    }

    /**
     * Tests unloadBootstrap for a binding with good results.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void unloadBootstrapBindingGood()
        throws Exception
    {
        ObjectName mbn;

        // First test with no install action. In this case, the component
        // should not be registered.

        mBindingContext.setIsInstall(true);
        mbn = mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.unloadBootstrap(Constants.BC_NAME);
        assertFalse("Binding " + Constants.BC_NAME + " should not be registered",
            mCompReg.isComponentRegistered(Constants.BC_NAME));
        assertFalse("InstallerMBean should no longer be registered",
            mContext.getMBeanServer().isRegistered(mbn));

        // Now test with an install action. In this case, the component
        // should be registered and in SHUTDOWN state.

        mBindingContext.setIsInstall(true);
        mbn = mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        mCompFW.unloadBootstrap(Constants.BC_NAME);
        assertTrue("Binding " + Constants.BC_NAME + " should be registered",
            mCompReg.isComponentRegistered(Constants.BC_NAME));
        assertTrue("Binding " + Constants.BC_NAME + " should be in SHUTDOWN state",
            mCompReg.getComponent(Constants.BC_NAME).isInstalled());
        assertFalse("InstallerMBean should no longer be registered",
            mContext.getMBeanServer().isRegistered(mbn));

        // Now test with an uninstall action. In this case, the component
        // should not be registered.

        mBindingContext.setIsInstall(false);
        mbn = mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.uninstallComponent(mBindingContext, false);
        mCompFW.unloadBootstrap(Constants.BC_NAME);
        assertFalse("Binding " + Constants.BC_NAME + " is still registered",
            mCompReg.isComponentRegistered(Constants.BC_NAME));
        assertFalse("InstallerMBean should no longer be registered",
            mContext.getMBeanServer().isRegistered(mbn));
    }

    /**
     * Tests unloadBootstrap for an engine with good results.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void unloadBootstrapEngineGood()
        throws Exception
    {
        ObjectName mbn;

        // First test with no install action. In this case, the component
        // should not be registered.

        mEngineContext.setIsInstall(true);
        mbn = mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.unloadBootstrap(Constants.SE_NAME);
        assertFalse("Engine " + Constants.SE_NAME + " should not be registered",
            mCompReg.isComponentRegistered(Constants.SE_NAME));
        assertFalse("InstallerMBean should no longer be registered",
            mContext.getMBeanServer().isRegistered(mbn));

        // Now test with an install action. In this case, the component
        // should be registered and in SHUTDOWN state.

        mEngineContext.setIsInstall(true);
        mbn = mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        mCompFW.unloadBootstrap(Constants.SE_NAME);
        assertTrue("Engine " + Constants.SE_NAME + " should be registered",
            mCompReg.isComponentRegistered(Constants.SE_NAME));
        assertTrue("Engine " + Constants.SE_NAME + " should be in SHUTDOWN state",
            mCompReg.getComponent(Constants.SE_NAME).isInstalled());
        assertFalse("InstallerMBean should no longer be registered",
            mContext.getMBeanServer().isRegistered(mbn));

        // Now test with an uninstall action. In this case, the component
        // should not be registered.

        mEngineContext.setIsInstall(false);
        mbn = mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.uninstallComponent(mEngineContext, false);
        mCompFW.unloadBootstrap(Constants.SE_NAME);
        assertFalse("Engine " + Constants.SE_NAME + " is still registered",
            mCompReg.isComponentRegistered(Constants.SE_NAME));
        assertFalse("InstallerMBean should no longer be registered",
            mContext.getMBeanServer().isRegistered(mbn));
    }

    /**
     * Tests startComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startBindingGood()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(binding);
        assertTrue("Failure starting BC",
            binding.isStarted());
    }

    /**
     * Tests startComponent with a binding that is already started.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startBindingAlreadyStarted()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(comp);
        
        // repeated life cycle operations should be a NOP
        mCompFW.startComponent(comp);
    }

    /**
     * Tests startComponent with a required Shared Library missing. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startBindingBadMissingSharedLibrary()
        throws Exception
    {
        // Install the Shared Libraries required by the binding.

        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);
        mCompFW.installSharedLibrary(Constants.SL_2_NAME,
                                     Constants.SL_2_DESC,
                                     Constants.SL_2_ROOT,
                                     false,
                                     mSl2Elements);

        // Create the list of Shared Library IDs required by the binding.

        ArrayList slList = new ArrayList();
        slList.add(Constants.SL_1_NAME);
        slList.add(Constants.SL_2_NAME);

        // Load, install, start, and shutdown the binding.

        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            slList, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(comp);
        mCompFW.shutdownComponent(comp);

        // Uninstall one of the Shared Libraries.

        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_1_NAME);
        }
        catch ( javax.jbi.JBIException ex )
        {
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("reinstalled")));
        }

        // Attempt to start the binding. This should fail.
        try
        {
            mCompFW.startComponent(comp);
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("must be installed first")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_1_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 == ex.getMessage().indexOf(Constants.SL_2_NAME)));
        }

        // Uninstall the other Shared Library.

        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_2_NAME);
        }
        catch ( javax.jbi.JBIException ex )
        {
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("reinstalled")));
        }

        // Attempt to start the binding. This should fail.
        try
        {
            mCompFW.startComponent(comp);
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("must be installed first")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_1_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_2_NAME)));
        }

    }

    /**
     * Tests startComponent with a binding whose init() method always throws
     * an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startBindingBadInitException()
        throws Exception
    {
        mBindingContext.setComponentName(Constants.BC_NAME_BAD_INIT);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME_BAD_INIT);

        // The init() method of this BC always throws an exception.
        // Test first in internal mode, then in client mode.

        try
        {
            mCompFW.startComponent(comp);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1135")));
        }

        try
        {
            mCompFW.startComponent(comp, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1136")));
        }
    }

    /**
     * Tests startComponent with a binding whose start() method always throws
     * an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startBindingBadStartException()
        throws Exception
    {
        mBindingContext.setComponentName(Constants.BC_NAME_BAD_START);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME_BAD_START);

        // The start() method of this BC always throws an exception.
        // Test first in internal mode, then in client mode.

        try
        {
            mCompFW.startComponent(comp);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1147")));
        }

        try
        {
            mCompFW.startComponent(comp, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1149")));
        }
    }

    /**
     * Tests startComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startEngineGood()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(engine);
        assertTrue("Failure starting SE",
            engine.isStarted());
    }

    /**
     * Tests startComponent with a engine that is already started.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startEngineAlreadyStarted()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(comp);
        
        // repeated life cycle operations should be a NOP
        mCompFW.startComponent(comp);
    }

    /**
     * Tests startComponent with a required Shared Library missing. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startEngineBadMissingSharedLibrary()
        throws Exception
    {
        // Install the Shared Libraries required by the engine.

        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);
        mCompFW.installSharedLibrary(Constants.SL_2_NAME,
                                     Constants.SL_2_DESC,
                                     Constants.SL_2_ROOT,
                                     false,
                                     mSl2Elements);

        // Create the list of Shared Library IDs required by the engine.

        ArrayList slList = new ArrayList();
        slList.add(Constants.SL_1_NAME);
        slList.add(Constants.SL_2_NAME);

        // Load, install, start, and shutdown the engine.

        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            slList, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(comp);
        mCompFW.shutdownComponent(comp);

        // Uninstall one of the Shared Libraries.

        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_1_NAME);
        }
        catch ( javax.jbi.JBIException ex )
        {
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("reinstalled")));
        }

        // Attempt to start the engine. This should fail.
        try
        {
            mCompFW.startComponent(comp);
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("must be installed first")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_1_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 == ex.getMessage().indexOf(Constants.SL_2_NAME)));
        }

        // Uninstall the other Shared Library.

        try
        {
            mCompFW.uninstallSharedLibrary(Constants.SL_2_NAME);
        }
        catch ( javax.jbi.JBIException ex )
        {
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("reinstalled")));
        }

        // Attempt to start the engine. This should fail.
        try
        {
            mCompFW.startComponent(comp);
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("must be installed first")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_1_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_2_NAME)));
        }

    }

    /**
     * Tests startComponent with an engine whose init() method always throws
     * an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startEngineBadInitException()
        throws Exception
    {
        mEngineContext.setComponentName(Constants.SE_NAME_BAD_INIT);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(Constants.SE_NAME_BAD_INIT);

        // The init() method of this SE always throws an exception.
        // Test first in internal mode, then in client mode.

        try
        {
            mCompFW.startComponent(comp);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1135")));
        }

        try
        {
            mCompFW.startComponent(comp, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1136")));
        }
    }

    /**
     * Tests startComponent with an engine whose start() method always throws
     * an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startEngineBadStartException()
        throws Exception
    {
        mEngineContext.setComponentName(Constants.SE_NAME_BAD_START);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component comp = mCompReg.getComponent(Constants.SE_NAME_BAD_START);

        // The start() method of this SE always throws an exception.
        // Test first in internal mode, then in client mode.

        try
        {
            mCompFW.startComponent(comp);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1147")));
        }

        try
        {
            mCompFW.startComponent(comp, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1149")));
        }
    }

    /**
     * Tests stopComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopBindingGood()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(binding);
        mCompFW.stopComponent(binding);
        assertTrue("Failure stopping Binding Component",
            binding.isInitialized());
    }

    /**
     * Tests stopComponent with a binding that is not active. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopBindingBadNotActive()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME);
        try
        {
            mCompFW.stopComponent(binding);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("not active")));
        }
    }

    /**
     * Tests stopComponent with a binding that is already stopped.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopBindingAlreadyStopped()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(binding);
        mCompFW.stopComponent(binding);
        
        // repeated life cycle operations should be a NOP
        mCompFW.stopComponent(binding);
    }

    /**
     * Tests stopComponent with a binding whose stop() method always throws
     * an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopBindingBadStopException()
        throws Exception
    {
        mBindingContext.setComponentName(Constants.BC_NAME_BAD_STOP);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME_BAD_STOP);
        mCompFW.startComponent(binding);

        // The stop() method of this BC always throws an exception.

        try
        {
            mCompFW.stopComponent(binding);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1159")));
        }

    }

    /**
     * Tests stopComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopEngineGood()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(engine);
        mCompFW.stopComponent(engine);
        assertTrue("Failure stopping Service Engine",
            engine.isInitialized());
    }

    /**
     * Tests stopComponent with an engine that is not active. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopEngineBadNotActive()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME);
        try
        {
            mCompFW.stopComponent(engine);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("not active")));
        }
    }

    /**
     * Tests stopComponent with an engine that is already stopped.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopEngineAlreadyStopped()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(engine);
        mCompFW.stopComponent(engine);
        
        // repeated life cycle operations should be a NOP
        mCompFW.stopComponent(engine);
    }

    /**
     * Tests stopComponent with an engine whose stop() method always throws
     * an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopEngineBadStopException()
        throws Exception
    {
        mEngineContext.setComponentName(Constants.SE_NAME_BAD_STOP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME_BAD_STOP);
        mCompFW.startComponent(engine);

        // The stop() method of this SE always throws an exception.

        try
        {
            mCompFW.stopComponent(engine);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1159")));
        }
    }

    /**
     * Tests shutdownComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownBindingGood()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(binding);
        mCompFW.stopComponent(binding);
        mCompFW.shutdownComponent(binding);
        assertFalse("Failure shutting down Binding Component",
            binding.isInitialized());
    }

    /**
     * Tests shutdownComponent with a binding that is not active.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownBindingNotActive()
        throws Exception
    {
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(binding);
        mCompFW.shutdownComponent(binding);
        
        // repeated life cycle operations should be a NOP
        mCompFW.shutdownComponent(binding);
    }

    /**
     * Tests shutdownComponent with a binding whose stop() method always
     * throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownBindingBadStopException()
        throws Exception
    {
        mBindingContext.setComponentName(Constants.BC_NAME_BAD_STOP);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME_BAD_STOP);
        mCompFW.startComponent(binding);

        // The stop() method of this BC always throws an exception.
        // Test first in internal mode, then in client mode.

        try
        {
            mCompFW.shutdownComponent(binding);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1157")));
        }

        try
        {
            mCompFW.shutdownComponent(binding, false, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1159")));
        }
    }

    /**
     * Tests shutdownComponent with a binding whose shutDown() method always
     * throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownBindingBadShutdownException()
        throws Exception
    {
        mBindingContext.setComponentName(Constants.BC_NAME_BAD_SHUTDOWN);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME_BAD_SHUTDOWN);
        mCompFW.startComponent(binding);

        // The shutDown() method of this BC always throws an exception.
        // Test first in internal mode, then in client mode.

        try
        {
            mCompFW.shutdownComponent(binding);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1167")));
        }

        try
        {
            mCompFW.shutdownComponent(binding, false, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1169")));
        }
    }

    /**
     * Tests shutdownComponent force=true with a binding whose stop() method
     * always throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownBindingForceStopException()
        throws Exception
    {
        mBindingContext.setComponentName(Constants.BC_NAME_BAD_STOP);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME_BAD_STOP);
        mCompFW.startComponent(binding);

        // The stop() method of this BC always throws an exception.

        mCompFW.shutdownComponent(binding, true, false);
        assertFalse("Failure on forced shutdown of Binding Component",
            binding.isInitialized());
    }

    /**
     * Tests shutdownComponent force=true with a binding whose shutDown()
     * method always throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownBindingForceShutdownException()
        throws Exception
    {
        mBindingContext.setComponentName(Constants.BC_NAME_BAD_SHUTDOWN);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME_BAD_SHUTDOWN);
        mCompFW.startComponent(binding);

        // The shutDown() method of this BC always throws an exception.

        mCompFW.shutdownComponent(binding, true, false);
        assertFalse("Failure on forced shutdown of Binding Component",
            binding.isInitialized());
    }

    /**
     * Tests shutdownComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownEngineGood()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(engine);
        mCompFW.stopComponent(engine);
        mCompFW.shutdownComponent(engine);
        assertFalse("Failure shutting down Service Engine",
            engine.isInitialized());
    }

    /**
     * Tests shutdownComponent with an engine that is not active.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownEngineAlreadyShutdown()
        throws Exception
    {
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(engine);
        mCompFW.shutdownComponent(engine);
        
        // repeated life cycle operations should be a NOP
        mCompFW.shutdownComponent(engine);
    }

    /**
     * Tests shutdownComponent with an engine whose stop() method always throws
     * an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownEngineBadStopException()
        throws Exception
    {
        mEngineContext.setComponentName(Constants.SE_NAME_BAD_STOP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME_BAD_STOP);
        mCompFW.startComponent(engine);

        // The stop() method of this SE always throws an exception.
        // Test first in internal mode, then in client mode.

        try
        {
            mCompFW.shutdownComponent(engine);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1157")));
        }

        try
        {
            mCompFW.shutdownComponent(engine, false, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1159")));
        }
    }

    /**
     * Tests shutdownComponent with an engine whose shutDown() method always
     * throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownEngineBadShutdownException()
        throws Exception
    {
        mEngineContext.setComponentName(Constants.SE_NAME_BAD_SHUTDOWN);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME_BAD_SHUTDOWN);
        mCompFW.startComponent(engine);

        // The shutDown() method of this SE always throws an exception.
        // Test first in internal mode, then in client mode.

        try
        {
            mCompFW.shutdownComponent(engine);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1167")));
        }

        try
        {
            mCompFW.shutdownComponent(engine, false, true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1169")));
        }
    }

    /**
     * Tests shutdownComponent force=true with an engine whose stop() method
     * always throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownEngineForceStopException()
        throws Exception
    {
        mEngineContext.setComponentName(Constants.SE_NAME_BAD_STOP);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME_BAD_STOP);
        mCompFW.startComponent(engine);

        // The stop() method of this SE always throws an exception.

        mCompFW.shutdownComponent(engine, true, false);
        assertFalse("Failure on forced shutdown of Service Engine",
            engine.isInitialized());
    }

    /**
     * Tests shutdownComponent force=true with an engine whose shutDown()
     * method always throws an exception.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void shutdownEngineForceShutdownException()
        throws Exception
    {
        mEngineContext.setComponentName(Constants.SE_NAME_BAD_SHUTDOWN);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME_BAD_SHUTDOWN);
        mCompFW.startComponent(engine);

        // The shutDown() method of this SE always throws an exception.

        mCompFW.shutdownComponent(engine, true, false);
        assertFalse("Failure on forced shutdown of Service Engine",
            engine.isInitialized());
    }

    /**
     * Tests startService with a BC that times out in its init() method and
     * an SE that times out on its start() method.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void startServiceTimeout()
        throws Exception
    {
        // Install a BC that times out in its init() method

        mBindingContext =
            new ComponentInstallationContext(
                Constants.BC_NAME_TIMEOUT_INIT,
                ComponentInstallationContext.BINDING,
                Constants.BC_LIFECYCLE_CLASS_NAME,
                mBindingLifeClassPath,
                null);
        mBindingContext.setInstallRoot(mInstallRoot);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME_TIMEOUT_INIT);

        // Install a SE that times out in its start() method

        mEngineContext =
            new ComponentInstallationContext(
                Constants.SE_NAME_TIMEOUT_START,
                ComponentInstallationContext.ENGINE,
                Constants.SE_LIFECYCLE_CLASS_NAME,
                mEngineLifeClassPath,
                null);
        mEngineContext.setInstallRoot(mInstallRoot);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME_TIMEOUT_START);

        // Stop the ComponentFramework and verify that the components are
        // both shut down

        mCompFW.stopService();
        assertTrue("Failed to shut down Binding Component, state is " +
            binding.getStatusAsString() + ", should be SHUTDOWN",
            binding.isShutDown());
        assertTrue("Failed to shut down Service Engine, state is " +
            engine.getStatusAsString() + ", should be SHUTDOWN",
            engine.isShutDown());

        // Set the timeout value so that the components will time out, and
        // set the desired states of both components to STARTED.

        mContext.setComponentTimeout(1000);
        binding.setDesiredState(ComponentState.STARTED);
        engine.setDesiredState(ComponentState.STARTED);

        // Now start the ComponentFramework and verify that the BC and SE both
        // timed out

        mCompFW.startService();
        mCompFW.ready(true);
        assertTrue("Failed to timeout on init of Binding Component, state is " +
            binding.getStatusAsString() + ", should be SHUTDOWN",
            binding.isShutDown());
        assertTrue("Failed to timeout on start of Service Engine, state is " +
            engine.getStatusAsString() + ", should be STOPPED",
            engine.isStopped());

        // Reset the timeout value

        mContext.setComponentTimeout(0);
    }

    /**
     * Tests stopService with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopServiceGood()
        throws Exception
    {
        // Install and start a BC and an SE

        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(binding);
        assertTrue("Failure starting Binding Component",
            binding.isStarted());

        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(engine);
        assertTrue("Failure starting Service Engine",
            engine.isStarted());

        // Now stop the ComponentFramework and verify that the BC and SE
        // were both shutdown

        mCompFW.stopService();
        assertTrue("Failed to shutdown Binding Component",
            binding.isInstalled());
        assertTrue("Failed to shutdown Service Engine",
            engine.isInstalled());
    }

    /**
     * Tests stopService with a BC that times out in its stop() method and
     * an SE that times out on its shutDown().
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopServiceTimeout()
        throws Exception
    {
        // Install and start a BC and an SE

        mBindingContext.setComponentName(Constants.BC_NAME_TIMEOUT_STOP);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME_TIMEOUT_STOP);
        mCompFW.startComponent(binding);
        assertTrue("Failure starting Binding Component",
            binding.isStarted());

        mEngineContext.setComponentName(Constants.SE_NAME_TIMEOUT_SHUTDOWN);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME_TIMEOUT_SHUTDOWN);
        mCompFW.startComponent(engine);
        assertTrue("Failure starting Service Engine",
            engine.isStarted());

        // Now stop the ComponentFramework and verify that the BC and SE
        // both timed out

        mCompFW.stopService();
        assertTrue("Failed to timeout Binding Component, state is " +
            binding.getStatusAsString() + ", should be STARTED",
            binding.isStarted());
        assertTrue("Failed to timeout Service Engine, state is " +
            engine.getStatusAsString() + ", should be STOPPED",
            engine.isStopped());
    }

    /**
     * Tests stopService with a BC that times out in its shutDown() method,
     * an SE that shuts down normally.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void stopServiceMixed()
        throws Exception
    {
        // Install and start a BC and an SE

        mBindingContext.setComponentName(Constants.BC_NAME_TIMEOUT_SHUTDOWN);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        Component binding = mCompReg.getComponent(Constants.BC_NAME_TIMEOUT_SHUTDOWN);
        mCompFW.startComponent(binding);
        assertTrue("Failure starting Binding Component",
            binding.isStarted());

        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        Component engine = mCompReg.getComponent(Constants.SE_NAME);
        mCompFW.startComponent(engine);
        assertTrue("Failure starting Service Engine",
            engine.isStarted());

        // Now stop the ComponentFramework and verify that the BC timed out
        // and the SE shutdown successfully.

        mCompFW.stopService();
        assertTrue("Failed to timeout Binding Component, state is " +
            binding.getStatusAsString() + ", should be STOPPED",
            binding.isStopped());
        assertTrue("Failed to shutdown Service Engine, state is " +
            engine.getStatusAsString() + ", should be installed",
            engine.isInstalled());
    }
    
    /**
     * Tests getComponentInstance with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void getComponentInstanceGood()
        throws Exception
    {
        javax.jbi.component.Component instance = null;

        // Test that a null is returned when the component does not exist
        instance = mCompFW.getComponentInstance(Constants.BC_NAME);
        assertNull("Failure getting component instance: " +
                   "expected a null return value , got " + instance,
                   instance);
        
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        
        // Test that a null is returned when the component does not exist
        instance = mCompFW.getComponentInstance(Constants.BC_NAME);
        assertNull("Failure getting component instance: " +
                   "expected a null return value , got " + instance,
                   instance);
        
        // Start it so that the component instance is created
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(comp);
        
        // Test that a valid instance returned when the component exists
        instance = mCompFW.getComponentInstance(Constants.BC_NAME);
        assertNotNull("Failure getting component instance: " +
                   "expected a non null return value , got " + instance,
                   instance);

        // Shut it down so that the component instance is removed
        comp = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.shutdownComponent(comp);
        
        // Test that a null is returned when the component does not exist
        instance = mCompFW.getComponentInstance(Constants.BC_NAME);
        assertNull("Failure getting component instance: " +
                   "expected a null return value , got " + instance,
                   instance);
    }
    
    /**
     * Tests getDeployerInstance with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void getDeployerInstanceGood()
        throws Exception
    {
        com.sun.jbi.framework.DeployerMBean instance = null;

        // Test that a null is returned when the component does not exist
        instance = mCompFW.getDeployerInstance(Constants.BC_NAME);
        assertNull("Failure getting deployer instance: " +
                   "expected a null return value , got " + instance,
                   instance);
        
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        
        // Start it so that the deployer instance is created
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(comp);
        
        // Test that a valid instance returned when the component exists
        instance = mCompFW.getDeployerInstance(Constants.BC_NAME);
        assertNotNull("Failure getting deployer instance: " +
                   "expected a non null return value , got " + instance,
                   instance);
    }

    /**
     * Tests validateComponentForUpgrade with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeGood()
        throws Exception
    {
        boolean supported;

        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        supported = mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Verify that upgrade support was detected
        assertTrue("Upgrade method should have been found: ", supported);

        // Install a good engine that does not support upgrade
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME, mEngineBootClassPath, null, false);
        mCompFW.installComponent(mEngineContext);

        // Now validate for upgrade using the same install context
        supported = mCompFW.validateComponentForUpgrade(mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME, mEngineBootClassPath, null);

        // Verify that upgrade support was not detected
        assertFalse("Upgrade method should not have been found: ", supported);
    }

    /**
     * Tests validateComponentForUpgrade with additional Shared Libraries with
     * a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeGoodSharedLibraries()
        throws Exception
    {
        boolean supported;

        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now install the required shared libraries
        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);
        mCompFW.installSharedLibrary(Constants.SL_2_NAME,
                                     Constants.SL_2_DESC,
                                     Constants.SL_2_ROOT,
                                     false,
                                     mSl2Elements);

        // Create a list of Shared Library IDs to add
        ArrayList slList = new ArrayList();
        slList.add(Constants.SL_1_NAME);
        slList.add(Constants.SL_2_NAME);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        // and with new Shared Libraries required
        supported = mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, slList);

        // Verify that upgrade support was detected
        assertTrue("Upgrade method should have been found: ", supported);
    }

    /**
     * Tests validateComponentForUpgrade with a null ComponentInstallationContext
     * parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadNullInstallationContext()
        throws Exception
    {
        try
        {
            mCompFW.validateComponentForUpgrade(null,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("installContext")));
        }
    }
    
    /**
     * Tests validateComponentForUpgrade with a null bootstrap class name
     * parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadNullBootClassName()
        throws Exception
    {
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                null, mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassName")));
        }
    }
    
    /**
     * Tests validateComponentForUpgrade with an empty bootstrap class name
     * parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadEmptyBootClassName()
        throws Exception
    {
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                "", mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassName")));
        }
    }
    
    /**
     * Tests validateComponentForUpgrade with a null bootstrap class path element
     * list parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadNullBootClassPathElementList()
        throws Exception
    {
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE, null, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassPathElements")));
        }
    }
    
    /**
     * Tests validateComponentForUpgrade with an empty bootstrap class path
     * element list parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadEmptyBootClassPathElementList()
        throws Exception
    {
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                new ArrayList(), null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassPathElements")));
        }
    }

    /**
     * Tests validateComponentForUpgrade with a non-existent component.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadNotFound()
        throws Exception
    {
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1185")));
        }
    }

    /**
     * Tests validateComponentForUpgrade with a running component.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadNotShutDown()
        throws Exception
    {
        // Install a good binding and start it
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(comp);

        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1186")));
        }
    }
    
    /**
     * Tests validateComponentForUpgrade with a busy component. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadBusy()
        throws Exception
    {
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);
        // Set the component to a busy state
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        comp.setBusy();

        // Now validate for update using the same install context
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2011")));
        }
    }
    
    /**
     * Tests validateComponentForUpgrade with a missing shared library. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadMissingSharedLibrary()
        throws Exception
    {
        // Create a list of Shared Library IDs that are not installed.
        ArrayList slList = new ArrayList();
        slList.add(Constants.SL_1_NAME);
        slList.add(Constants.SL_2_NAME);

        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now validate for update using the same install context. This should
        // fail due to missing shared libraries.
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                mBindingBootClassPath, slList);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1195")));
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_1_NAME)));
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_2_NAME)));
        }
    }

    /**
     * Tests validateComponentForUpgrade with a bootstrap class loading failure.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadBootstrapClassNotFound()
        throws Exception
    {
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now validate for update. This should fail with a bootstrap class load
        // error
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                "bad.bootstrap.class", mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2004")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bad.bootstrap.class")));
        }
    }

    /**
     * Tests validateComponentForUpgrade with a component class loading failure.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void validateComponentForUpgradeBadComponentClassNotFound()
        throws Exception
    {
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Change the component class name in the installation context to
        // force a class loading failure
        mBindingContext.setComponentClassName("bad.component.class");
            
        // Now validate for update. This should fail with a component class load
        // error
        try
        {
            mCompFW.validateComponentForUpgrade(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2004")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bad.component.class")));
        }
    }

    /**
     * Tests cancelComponentUpgrade with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void cancelComponentUpgradeGood()
        throws Exception
    {
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Now cancel the upgrade
        mCompFW.cancelComponentUpgrade(Constants.BC_NAME);

        // Verification
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        assertFalse("cancelComponentUpgrade failed to reset busy flag",
            comp.isBusy());
        assertFalse("cancelComponentUpgrade failed to reset updating flag",
            comp.isUpdating());
    }

    /**
     * Tests cancelComponentUpgrade with a null component name parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void cancelComponentUpgradeBadNullComponentName()
        throws Exception
    {
        try
        {
            mCompFW.cancelComponentUpgrade(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("componentName")));
        }
    }

    /**
     * Tests cancelComponentUpgrade with an empty component name parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void cancelComponentUpgradeBadEmptyComponentName()
        throws Exception
    {
        try
        {
            mCompFW.cancelComponentUpgrade("");
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("componentName")));
        }
    }
    
    /**
     * Tests cancelComponentUpgrade with a non-existent component. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void cancelComponentUpgradeBadNotFound()
        throws Exception
    {
        try
        {
            mCompFW.cancelComponentUpgrade(Constants.BC_NAME);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1189")));
        }
    }

    /**
     * Tests upgradeComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentGood()
        throws Exception
    {
        File f;

        // Install a good binding that supports upgrade
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);
        Component comp = mCompReg.getComponent(Constants.BC_NAME);

        // Add an SU to this component (this is a shortcut to deployment).
        String suPath = mInstallRoot + "/" + Constants.SU_NAME;
        f = new File(suPath);
        if ( f.exists() )
        {
            File[] fl = f.listFiles();
            if ( null != fl )
            { 
                for (File fl1 : fl) {
                    fl1.delete();
                }   
            }
        }
        else
        {
            f.mkdir();
        }
        ServiceUnit su = new ServiceUnit(Constants.SA_NAME,
            Constants.SU_NAME, suPath, Constants.BC_NAME);
        comp.addServiceUnit(su);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Now upgrade using the same install context
        System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
            mBindingContext.getComponentName());
        mCompFW.upgradeComponent(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Verification
        comp = mCompReg.getComponent(Constants.BC_NAME);
        assertEquals("Component instance not updated: ",
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            comp.getBootstrapClassName());

        f = new File(mWorkspaceRoot + "/" + Constants.UPGRADED_FILE_NAME);
        assertTrue("Component's upgrade method failed to write file " +
            f.getAbsolutePath(),  f.exists());

        f = new File(suPath + "/" + Constants.UPGRADED_FILE_NAME);
        assertTrue("Component's upgrade method failed to write file " +
            f.getAbsolutePath(), f.exists());

        // Make sure the component will start, stop, and shut down
        try
        {
            mCompFW.startComponent(comp);
        }
        catch ( javax.jbi.JBIException ex )
        {
            fail(ex.getMessage());
        }
        try
        {
            mCompFW.stopComponent(comp);
        }
        catch ( javax.jbi.JBIException ex )
        {
            fail(ex.getMessage());
        }
        try
        {
            mCompFW.shutdownComponent(comp);
        }
        catch ( javax.jbi.JBIException ex )
        {
            fail(ex.getMessage());
        }
    }

    /**
     * Tests upgradeComponent with a good result with a new shared library.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentGoodSharedLibrary()
        throws Exception
    {
        // Install a good binding that supports upgrade
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now install the required shared libraries
        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);

        // Create the list of Shared Library IDs to add
        ArrayList slList = new ArrayList();
        slList.add(Constants.SL_1_NAME);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        // and with the new shared library required
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, slList);

        // Now upgrade using the same install context
        System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
            mBindingContext.getComponentName());
        mCompFW.upgradeComponent(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, slList);

        // Verification
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        assertEquals("Component instance bootstrap class name not updated: ",
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            comp.getBootstrapClassName());
        assertEquals("Component instance shared library count not same: ",
            slList.size(), comp.getSharedLibraryNames().size());
        assertTrue("Component instance shared library list not same: ",
            comp.getSharedLibraryNames().containsAll(slList));
    }

    /**
     * Tests upgradeComponent with a good result with a changed bootstrap
     * class path.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentGoodBootstrapClassPath()
        throws Exception
    {
        // Install a good binding that supports upgrade
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Add an element to the bootstrap class path list
        mBindingBootClassPath.add(mInstallRoot);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        // and with the new bootstrap class path
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Now upgrade using the same install context
        System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
            mBindingContext.getComponentName());
        mCompFW.upgradeComponent(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Verification
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        assertEquals("Component instance bootstrap class name not updated: ",
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            comp.getBootstrapClassName());
        assertEquals("Component instance bootstrap class path count not same: ",
            mBindingBootClassPath.size(),
            comp.getBootstrapClassPathElements().size());
        int i = 0;
        for ( String cp : comp.getBootstrapClassPathElements() )
        {
            assertEquals("Component instance bootstrap class path not same: ",
                cp, (String) mBindingBootClassPath.get(i++));
        }
    }

    /**
     * Tests upgradeComponent with a good result with a changed component
     * class path.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentGoodComponentClassPath()
        throws Exception
    {
        // Install a good binding that supports upgrade
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Add an element to the component class path list. We have to replace
        // forward slashes with File.separatorChar to comply with the JSR208
        // defined interface for setClassPathElements().
        mBindingLifeClassPath = new ArrayList();
        mBindingLifeClassPath.add(Constants.BC_LIFECYCLE_CLASS_PATH.replace(
            '/', File.separatorChar));
        mBindingLifeClassPath.add(Constants.BC_LIFECYCLE_CLASS_PATH_NEW.replace(
            '/', File.separatorChar));
        mBindingContext.setClassPathElements(mBindingLifeClassPath);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        // and with the new bootstrap class path
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Now upgrade using the same install context
        System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
            mBindingContext.getComponentName());
        mCompFW.upgradeComponent(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Verification
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        assertEquals("Component instance bootstrap class name not updated: ",
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            comp.getBootstrapClassName());
        assertEquals("Component instance component class path count not same: ",
            mBindingLifeClassPath.size(),
            comp.getComponentClassPathElements().size());

        // Verifying the class path is a little more tricky. What's stored in
        // the Component instance is an absolute path, while what's passed in
        // to ComponentInstallationContext.setClassPathElements() is a relative
        // path. So, check to make sure the relative path is contained in the
        // corresponding absolute path.
        int i = 0;
        for ( String cp : comp.getComponentClassPathElements() )
        {
            assertTrue("Component instance component class path not same: " +
                cp + " and " + mBindingLifeClassPath.get(i),
                (-1 < cp.indexOf((String) mBindingLifeClassPath.get(i))));
            i++;
        }
    }

    /**
     * Tests upgradeComponent with a good result with a changed component
     * description.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentGoodDescription()
        throws Exception
    {
        // Install a good binding that supports upgrade
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Change the component description
        String newDesc = "This is an upgraded binding component";
        mBindingContext.setDescription(newDesc);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Now upgrade using the same install context
        System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
            mBindingContext.getComponentName());
        mCompFW.upgradeComponent(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Verification
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        assertEquals("Component instance bootstrap class name not updated: ",
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            comp.getBootstrapClassName());
        assertEquals("Component description not same: ",
            newDesc, comp.getDescription());
    }

    /**
     * Tests upgradeComponent with a good result with a changed bootstrap
     * class loader self-first setting.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentGoodBootstrapSelfFirst()
        throws Exception
    {
        // Install a good binding that supports upgrade
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Change the bootstrap class loading to self-first
        mBindingContext.setBootstrapClassLoaderSelfFirst();

        // Now validate for upgrade with a bootstrap class that supports upgrade
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Now upgrade using the same install context
        System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
            mBindingContext.getComponentName());
        mCompFW.upgradeComponent(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Verification
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        assertEquals("Component instance bootstrap class name not updated: ",
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            comp.getBootstrapClassName());
        assertTrue("Bootstrap class loader not self-first",
            comp.isBootstrapClassLoaderSelfFirst());
    }

    /**
     * Tests upgradeComponent with a good result with a changed component
     * class loader self-first setting.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentGoodComponentSelfFirst()
        throws Exception
    {
        // Install a good binding that supports upgrade
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Change the component class loading to self-first
        mBindingContext.setComponentClassLoaderSelfFirst();

        // Now validate for upgrade with a bootstrap class that supports upgrade
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Now upgrade using the same install context
        System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
            mBindingContext.getComponentName());
        mCompFW.upgradeComponent(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Verification
        Component comp = mCompReg.getComponent(Constants.BC_NAME);
        assertEquals("Component instance bootstrap class name not updated: ",
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            comp.getBootstrapClassName());
        assertTrue("Component class loader not self-first",
            comp.isComponentClassLoaderSelfFirst());
    }

    /**
     * Tests upgradeComponent with a null ComponentInstallationContext
     * parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadNullInstallationContext()
        throws Exception
    {
        try
        {
            mCompFW.upgradeComponent(null, Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("installContext")));
        }
    }
    
    /**
     * Tests upgradeComponent with a null bootstrap class name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadNullBootClassName()
        throws Exception
    {
        try
        {
            mCompFW.upgradeComponent(mBindingContext, null,
                 mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassName")));
        }
    }

    /**
     * Tests upgradeComponent with an empty bootstrap class name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadEmptyBootClassName()
        throws Exception
    {
        try
        {
            mCompFW.upgradeComponent(mBindingContext, "",
                 mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassName")));
        }
    }

    /**
     * Tests upgradeComponent with a null bootstrap class path element
     * list parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadNullBootClassPathElementList()
        throws Exception
    {
        try
        {
            mCompFW.upgradeComponent(mBindingContext,
                 Constants.BC_BOOTSTRAP_CLASS_NAME, null, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassPathElements")));
        }
    }

    /**
     * Tests upgradeComponent with an empty bootstrap class path element list
     * parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadEmptyBootClassPathElementList()
        throws Exception
    {
        try
        {
            mCompFW.upgradeComponent(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME, new ArrayList(), null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("bootClassPathElements")));
        }
    }

    /**
     * Tests upgradeComponent with a non-existent component.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadNotFound()
        throws Exception
    {
        try
        {
            mCompFW.upgradeComponent(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1185")));
        }
    }
    
    /**
     * Tests upgradeComponent without first calling
     * validateComponentForUpgrade(). An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadNoValidate()
        throws Exception
    {
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now upgrade without calling validateComponentForUpgrade() first.
        try
        {
            mCompFW.upgradeComponent(mBindingContext,
                 Constants.BC_BOOTSTRAP_CLASS_NAME,
                 mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1188")));
        }
    }

    /**
     * Tests upgradeComponent with a missing shared library. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadMissingSharedLibrary()
        throws Exception
    {
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now install the newly required shared libraries
        mCompFW.installSharedLibrary(Constants.SL_1_NAME,
                                     Constants.SL_1_DESC,
                                     Constants.SL_1_ROOT,
                                     false,
                                     mSl1Elements);
        mCompFW.installSharedLibrary(Constants.SL_2_NAME,
                                     Constants.SL_2_DESC,
                                     Constants.SL_2_ROOT,
                                     false,
                                     mSl2Elements);

        // Create a list of Shared Library IDs to add
        ArrayList slList = new ArrayList();
        slList.add(Constants.SL_1_NAME);
        slList.add(Constants.SL_2_NAME);

        // Now validate for upgrade with a bootstrap class that supports upgrade
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, slList);

        // Now uninstall one of the shared libraries to force the upgrade to
        // fail
        mCompFW.uninstallSharedLibrary(Constants.SL_1_NAME);

        // Now upgrade using the same install context. This should fail because
        // one of the shared libraries is now missing.
        try
        {
            System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
                mBindingContext.getComponentName());
            mCompFW.upgradeComponent(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                mBindingBootClassPath, slList);
               
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1195")));
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SL_1_NAME)));
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 == ex.getMessage().indexOf(Constants.SL_2_NAME)));

            Component comp = mCompReg.getComponent(Constants.BC_NAME);
            assertEquals("Component instance not restored: ",
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                comp.getBootstrapClassName());
        }
    }

    /**
     * Tests upgradeComponent with a failure loading the bootstrap class. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadBootstrapLoadFailed()
        throws Exception
    {
        // Install a good binding
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now validate for upgrade using the same install context
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null);

        // Now upgrade using the same install context but a different bootstrap
        // class name. This should fail because the bootstrap class cannot be
        // loaded.
        try
        {
            System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
                mBindingContext.getComponentName());
            mCompFW.upgradeComponent(mBindingContext,
                "bad.bootstrap.class", mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1192")));
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2002")));

            Component comp = mCompReg.getComponent(Constants.BC_NAME);
            assertEquals("Component instance not restored: ",
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                comp.getBootstrapClassName());
        }
    }
    
    /**
     * Tests upgradeComponent with a failure in the component's upgrade method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    @Test
    public void upgradeComponentBadUpgradeMethodFailed()
        throws Exception
    {
        // Install a binding with a bad upgrade method
        mBindingContext.setComponentName(
                Constants.BC_NAME_BAD_BOOTSTRAP_UPGRADE);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME, mBindingBootClassPath, null, false);
        mCompFW.installComponent(mBindingContext);

        // Now validate for upgrade
        mCompFW.validateComponentForUpgrade(mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
            mBindingBootClassPath, null);

        // Now upgrade using the same install context. This should fail because
        // the component's upgrade method will throw an exception.
        try
        {
            System.setProperty(Constants.PROPERTY_COMPONENT_NAME,
                mBindingContext.getComponentName());
            mCompFW.upgradeComponent(mBindingContext,
                Constants.BC_BOOTSTRAP_CLASS_NAME_UPGRADE,
                mBindingBootClassPath, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception message received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW1193")));

            Component comp = mCompReg.getComponent(
                Constants.BC_NAME_BAD_BOOTSTRAP_UPGRADE);
            assertEquals("Component instance not restored: ",
                Constants.BC_BOOTSTRAP_CLASS_NAME,
                comp.getBootstrapClassName());
        }
    }
}
