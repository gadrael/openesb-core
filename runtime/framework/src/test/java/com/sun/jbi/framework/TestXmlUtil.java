/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestXmlUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework;

import java.io.ByteArrayInputStream;

import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Tests for the Component class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestXmlUtil
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * EnvironmentContext
     */
    private EnvironmentContext mEnvironmentContext;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestXmlUtil(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);
        mEnvironmentContext = new EnvironmentContext(
                new ScaffoldPlatformContext(),
                new JBIFramework(), new Properties());
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        System.err.println("***** END of test " + mTestName);
        super.tearDown();
    }

// =============================  test methods ================================

    //
    // First test the getElements() method, as it is called by other methods
    // in the class.
    //

    /**
     * Test the getElements() method for a required tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementsRequiredPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        NodeList nodes = XmlUtil.getElements(top, "element1", true);

        // Verify that the correct Node was returned.

        assertEquals("Incorrect number of nodes returned: ",
                     1, nodes.getLength());
        assertEquals("Incorrect node returned: ",
                     "element1", nodes.item(0).getNodeName());
    }

    /**
     * Test the getElements() method for a required tag that is repeated.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementsRequiredRepeated()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "<element2>value2</element2>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        NodeList nodes = XmlUtil.getElements(top, "element1", true);

        // Verify that the correct Nodes were returned.

        assertEquals("Incorrect number of nodes returned: ",
                     2, nodes.getLength());
        assertEquals("Incorrect node returned: ",
                     "element1", nodes.item(0).getNodeName());
        assertEquals("Incorrect node returned: ",
                     "element1", nodes.item(1).getNodeName());
    }

    /**
     * Test the getElements() method for a required tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementsRequiredMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        try
        {
            NodeList nodes = XmlUtil.getElements(top, "element1", true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2600")));
        }
    }

    /**
     * Test the getElements() method for an optional tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementsOptionalPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        NodeList nodes = XmlUtil.getElements(top, "element1", false);

        // Verify that the correct Node was returned.

        assertEquals("Incorrect number of nodes returned: ",
                     1, nodes.getLength());
        assertEquals("Incorrect node returned: ",
                     "element1", nodes.item(0).getNodeName());
    }

    /**
     * Test the getElements() method for an optional tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementsOptionalMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        NodeList nodes = XmlUtil.getElements(top, "element2", false);

        // Verify that an empty NodeList was returned.
        assertEquals("Nonempty NodeList returned: ",
                     0, nodes.getLength());
    }

    //
    // Tests for the getElement() method, which calls getElements()
    //

    /**
     * Test the getElement() method for a required tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementRequiredPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        Node node = XmlUtil.getElement(top, "element1", true);

        // Verify that the correct Node was returned.

        assertEquals("Incorrect node returned: ",
                     "element1", node.getNodeName());
    }

    /**
     * Test the getElement() method for a required tag that is repeated.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementRequiredRepeated()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "<element2>value2</element2>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        try
        {
            Node node = XmlUtil.getElement(top, "element1", true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2601")));
        }
    }

    /**
     * Test the getElement() method for a required tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementRequiredMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        try
        {
            Node node = XmlUtil.getElement(top, "element1", true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2600")));
        }
    }

    /**
     * Test the getElement() method for an optional tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementOptionalPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        Node node = XmlUtil.getElement(top, "element1", false);

        // Verify that the correct Node was returned.

        assertEquals("Incorrect node returned: ",
                     "element1", node.getNodeName());
    }

    /**
     * Test the getElement() method for an optional tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetElementOptionalMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        Node node = XmlUtil.getElement(top, "element2", false);

        // Verify that a null value was returned.

        assertNull("Missing optional element returned a non-null value when " +
                   "a null was expected.",
                   node);
    }

    //
    // Tests for the getAttribute() method
    //

    /**
     * Test the getAttribute() method with a null node parameter.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetAttributeBadNullNode()
        throws Exception
    {
        try
        {
            String value = XmlUtil.getAttribute((Node) null, "attribute0");
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW0063")));
        }
    }

    /**
     * Test the getAttribute() method with a null attrName parameter.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetAttributeBadNullAttrName()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode attr0=\"value0\">" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        try
        {
            String value = XmlUtil.getAttribute(top, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW0063")));
        }
    }

    /**
     * Test the getAttribute() method with a missing attribute.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetAttributeGoodNotFound()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node doc = createDocument(xmlString);

        String value = XmlUtil.getAttribute(doc, "attribute0");

        // Verification
        assertEquals("Unexpected value received: " + value,
            0, value.length());
    }

    /**
     * Test the getAttribute() method with a specified attribute.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetAttributeGoodFound()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode attribute0=\"attr0\">" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node doc = createDocument(xmlString);

        String value = XmlUtil.getAttribute(doc, "attribute0");

        // Verification
        assertEquals("Expected value not received: " + value,
            "attr0", value);
    }

    /**
     * Test the getAttribute() method with a specified attribute.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetAttributeGoodFoundChild()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0 attribute0=\"attr0\">value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node doc = createDocument(xmlString);
        Node elem = XmlUtil.getElement(doc, "element0", true);

        String value = XmlUtil.getAttribute(elem, "attribute0");

        // Verification
        assertEquals("Expected value not received: " + value,
            "attr0", value);
    }

    //
    // Tests for the getBooleanValue() method
    //

    /**
     * Test the getBooleanValue() method for a required tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetBooleanValueRequiredPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>true</element1>" +
                           "<element2>false</element2>" +
                           "<element3>nerbunge</element3>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        Boolean value1 = XmlUtil.getBooleanValue(top, "element1", true);
        Boolean value2 = XmlUtil.getBooleanValue(top, "element2", true);
        Boolean value3 = XmlUtil.getBooleanValue(top, "element3", true);

        // Verify that the correct values were returned.

        assertTrue("Incorrect value returned for element1: ", value1);
        assertFalse("Incorrect value returned for element2: ", value2);
        assertFalse("Incorrect value returned for element3: ", value3);
    }

    /**
     * Test the getBooleanValue() method for a required tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetBooleanValueRequiredMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        try
        {
            Boolean value = XmlUtil.getBooleanValue(top, "element1", true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2600")));
        }
    }

    /**
     * Test the getBooleanValue() method for an optional tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetBooleanValueOptionalPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>true</element1>" +
                           "<element2>false</element2>" +
                           "<element3>nerbunge</element3>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        Boolean value1 = XmlUtil.getBooleanValue(top, "element1", false);
        Boolean value2 = XmlUtil.getBooleanValue(top, "element2", false);
        Boolean value3 = XmlUtil.getBooleanValue(top, "element3", false);

        // Verify that the correct values were returned.

        assertTrue("Incorrect value returned for element1: ", value1);
        assertFalse("Incorrect value returned for element2: ", value2);
        assertFalse("Incorrect value returned for element3: ", value3);
    }

    /**
     * Test the getBooleanValue() method for an optional tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetBooleanValueOptionalMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        Boolean value1 = XmlUtil.getBooleanValue(top, "element1", false);

        // Verification
        assertNull("Incorrect value returned for element1: ", value1);
    }

    //
    // Tests for the getStringValue(Node) method
    //

    /**
     * Test the getStringValue(Node) method with a null input parameter.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueNodeBadNullInput()
        throws Exception
    {
        try
        {
            String value = XmlUtil.getStringValue((Node) null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW0063")));
        }
    }

    /**
     * Test the getStringValue(Node) method with an empty input parameter.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueNodeBadEmptyInput()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode></topnode>";
        Node doc = createDocument(xmlString);
        try
        {
            String value = XmlUtil.getStringValue(doc);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2602")));
        }
    }

    /**
     * Test the getStringValue(Node) method with a missing text node.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueNodeBadMissingTextNode()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0></element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        Node node = XmlUtil.getElement(top, "element0", true);
        try
        {
            String value = XmlUtil.getStringValue(node);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2602")));
        }
    }

    /**
     * Test the getStringValue(Node) method with a text node present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueNodeGood()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        // Execute test

        Node node = XmlUtil.getElement(top, "element0", true);
        String value = XmlUtil.getStringValue(node);

        // Verify that the correct value was returned.

        assertEquals("Incorrect value returned for element0: ",
            "value0", value);

        // Execute test

        node = XmlUtil.getElement(top, "element1", true);
        value = XmlUtil.getStringValue(node);

        // Verify that the correct value was returned.

        assertEquals("Incorrect value returned for element1: ",
            "value1", value);
    }

    //
    // Tests for the getStringValue(NodeList) methods
    //

    /**
     * Test the getStringValue() method with a null input parameter.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueBadNullInput()
        throws Exception
    {
        try
        {
            String value = XmlUtil.getStringValue((NodeList) null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW0063")));
        }
    }

    /**
     * Test the getStringValue() method with an empty input parameter.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueBadEmptyInput()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        NodeList nodes = XmlUtil.getElements(top, "element0", false);
        try
        {
            String value = XmlUtil.getStringValue(nodes);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW0060")));
        }
    }

    /**
     * Test the getStringValue() method with a missing text node.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueBadMissingTextNode()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0></element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        NodeList nodes = XmlUtil.getElements(top, "element0", false);
        try
        {
            String value = XmlUtil.getStringValue(nodes);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2602")));
        }
    }

    /**
     * Test the getStringValue() method with an incorrect node.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueBadWrongType()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>" +
                           "<value0/>" +
                           "</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        NodeList nodes = XmlUtil.getElements(top, "element0", true);
        try
        {
            String value = XmlUtil.getStringValue(nodes);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2603")));
        }
    }

    /**
     * Test the getStringValue() method with a tag that is repeated.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueBadRepeated()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "<element2>value2</element2>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        NodeList nodes = XmlUtil.getElements(top, "element1", true);
        try
        {
            String value = XmlUtil.getStringValue(nodes);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2601")));
        }
    }

    /**
     * Test the getStringValue() method for a required tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueRequiredPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        String value1 = XmlUtil.getStringValue(top, "element1", true);

        // Verify that the correct value was returned.

        assertEquals("Incorrect value returned for element1: ",
            "value1", value1);
    }

    /**
     * Test the getStringValue() method for a required tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueRequiredMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        try
        {
            String value = XmlUtil.getStringValue(top, "element1", true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2600")));
        }
    }

    /**
     * Test the getStringValue() method for an optional tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueOptionalPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        String value1 = XmlUtil.getStringValue(top, "element1", false);

        // Verify that the correct value was returned.

        assertEquals("Incorrect value returned for element1: ",
            "value1", value1);
    }

    /**
     * Test the getStringValue() method for an optional tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueOptionalMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        String value1 = XmlUtil.getStringValue(top, "element1", false);

        // Verification
        assertNull("Incorrect value returned for element1: ", value1);
    }

    /**
     * Test the getStringValueArray() method for a required tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueArrayRequiredPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1_1</element1>" +
                           "<element2>value2</element2>" +
                           "<element1>value1_2</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        List<String> values = XmlUtil.getStringValueArray(top, "element1", true);

        // Verify that the correct values were returned.

        assertEquals("Incorrect number of values for element1: ",
            2, values.size());
        assertEquals("Incorrect value returned for element1: ",
            "value1_1", values.get(0));
        assertEquals("Incorrect value returned for element1: ",
            "value1_2", values.get(1));
    }

    /**
     * Test the getStringValueArray() method for a required tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueArrayRequiredMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        try
        {
            List<String> values = XmlUtil.getStringValueArray(top, "element1", true);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2600")));
        }
    }

    /**
     * Test the getStringValueArray() method for an optional tag that is present.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueArrayOptionalPresent()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "<element1>value1_1</element1>" +
                           "<element2>value2</element2>" +
                           "<element1>value1_2</element1>" +
                           "</topnode>";
        Node top = createDocument(xmlString);
        List<String> values = XmlUtil.getStringValueArray(top, "element1", false);

        // Verify that the correct values were returned.

        assertEquals("Incorrect number of values for element1: ",
            2, values.size());
        assertEquals("Incorrect value returned for element1: ",
            "value1_1", values.get(0));
        assertEquals("Incorrect value returned for element1: ",
            "value1_2", values.get(1));
    }

    /**
     * Test the getStringValueArray() method for an optional tag that is missing.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringValueArrayOptionalMissing()
        throws Exception
    {
        // Create an XML document for the test.

        String xmlString = "<topnode>" +
                           "<element0>value0</element0>" +
                           "</topnode>";
        Node top = createDocument(xmlString);

        List<String> values = XmlUtil.getStringValueArray(top, "element1", false);

        // Verification

        assertEquals("Incorrect value list size returned for element1: ",
            0, values.size());
    }

    //
    // Private methods used for setting up test documents
    //

    /**
     * Utility method to create a document from an XML string provided by
     * the caller, and then return the Node representing the top-level node.
     * @param xmlString an XML document in a String.
     * @return the top-level node as a DOM Node.
     * @throws Exception if an unexpected error occurs.
     */
    private Node createDocument(String xmlString)
        throws Exception
    {
        Document doc = null;

        // Add a standard header to the XML string.

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" + xmlString;

        // Create a DOM Document from the XML string.

        ByteArrayInputStream xmlStream =
            new ByteArrayInputStream(xml.getBytes());
        try
        {
            DocumentBuilder docBuilder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            docBuilder.setErrorHandler(new SAXErrorHandler());
            doc = docBuilder.parse(xmlStream);
        }
        catch (javax.xml.parsers.ParserConfigurationException pcEx)
        {
            fail("Unexpected exception received: " + pcEx.toString());
        }
        catch (org.xml.sax.SAXParseException spEx)
        {
            fail("Unexpected exception received: " + spEx.toString());
        }
        catch (org.xml.sax.SAXException saxEx)
        {
            fail("Unexpected exception received: " + saxEx.toString());
        }

        NodeList nl = doc.getChildNodes();
        return nl.item(0);
    }

    /**
     * This class provides the error handler for the SAX parser.
     * @author Sun Microsystems, Inc.
     */
    class SAXErrorHandler
        implements org.xml.sax.ErrorHandler
    {
        /**
         * This is a callback from the XML parser used to handle warnings.
         * @param aSaxException SAXParseException is the warning.
         * @throws org.xml.sax.SAXException when finished logging.
         */
        public void warning(org.xml.sax.SAXParseException aSaxException)
            throws org.xml.sax.SAXException
        {
            throw aSaxException;
        }

        /**
         * This is a callback from the XML parser used to handle errors.
         * @param aSaxException SAXParseException is the error.
         * @throws org.xml.sax.SAXException when finished logging.
         */
        public void error(org.xml.sax.SAXParseException aSaxException)
            throws org.xml.sax.SAXException
        {
            throw aSaxException;
        }

        /**
         * This is a callback from the XML parser used to handle fatal errors.
         * @param aSaxException SAXParseException is the error.
         * @throws org.xml.sax.SAXException when finished logging.
         */
        public void fatalError(org.xml.sax.SAXParseException aSaxException)
            throws org.xml.sax.SAXException
        {
            throw aSaxException;
        }
    }

}
