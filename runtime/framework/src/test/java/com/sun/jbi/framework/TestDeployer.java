/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDeployer.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.management.ComponentInstallationContext;

import java.util.ArrayList;

import javax.jbi.management.DeploymentException;
import com.sun.jbi.ServiceUnitState;

/**
 * Tests for the Deployer.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestDeployer
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Current SRCROOT path.
     */
    private String mSrcroot;

    /**
     * Helper class to setup environment for testing.
     */
    private EnvironmentSetup mSetup;

    /**
     * Instance of the EnvironmentContext class.
     */
    private EnvironmentContext mContext;

    /**
     * Instance of Component.
     */
    private Component mComponent;

    /**
     * Local instance of the ComponentInstallationContext class.
     */
    private ComponentInstallationContext mInstallContext;

    /**
     * Instance of the ComponentFramework class.
     */
    private ComponentFramework mCompFW;

    /**
     * Local instance of the ComponentRegistry class.
     */
    private ComponentRegistry mCompReg;

    /**
     * Local instance of the Deployer class.
     */
    private Deployer mDeployer;

    /**
     * Service Unit root file path.
     */
    private String mServiceUnitRoot;

    /**
     * Constant for Service Assembly zip path.
     */
    public static final String SA_ZIP_PATH =
        "framework/regress/data/junit/";

    /**
     * Constant for Service Assembly zip name.
     */
    public static final String SA_ZIP_NAME =
        "Test_SA.zip";

    /**
     * Constant for Service Unit zip name.
     */
    public static final String SU_ZIP_NAME =
        "Test_SA_SU1.zip";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestDeployer(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);
        mSrcroot = Utils.getSourceRoot() + "/";

        // Create and initialize the EnvironmentContext. Create, initialize,
        // and start up the Component Registry and Framework.

        mSetup = new EnvironmentSetup();
        mContext = mSetup.getEnvironmentContext();
        mSetup.startup(true, true);
        mCompReg = mContext.getComponentRegistry();
        mCompFW = mContext.getComponentFramework();

        // Create component class path lists

        ArrayList bootstrapClassPath = new ArrayList();
        bootstrapClassPath.add(mSrcroot + Constants.BC_BOOTSTRAP_CLASS_PATH);
        ArrayList componentClassPath = new ArrayList();
        componentClassPath.add(Constants.BC_LIFECYCLE_CLASS_PATH);

        // Create installation context.

        mInstallContext =
            new ComponentInstallationContext(
                Constants.BC_NAME,
                ComponentInstallationContext.BINDING,
                Constants.BC_LIFECYCLE_CLASS_NAME,
                componentClassPath,
                null);
        mInstallContext.setInstallRoot(mSrcroot);
        mInstallContext.setIsInstall(true);

        // Install the component.

        mCompFW.loadBootstrap(
            mInstallContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            bootstrapClassPath,
            null, false);
        mCompFW.installComponent(mInstallContext);

        // Get the Component instance.

        mComponent = mCompReg.getComponent(Constants.BC_NAME);

        // Create a Deployer instance

        mDeployer = new Deployer(mCompReg.getComponent(Constants.BC_NAME));
        mComponent.setDeployerInstance(mDeployer);

        // Start the component

        mCompFW.startComponent(mComponent);

        // Set the service unit root path

        mServiceUnitRoot = mSrcroot + Constants.SU_ROOT;
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        mSetup.shutdown(true, true);
        System.err.println("***** END of test " + mTestName);
    }

// =============================  test methods ================================

    /**
     * Tests deploy with a good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testDeployGood()
        throws Exception
    {
        String msg = mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        assertNotNull("Failure deploying Service Unit",
            msg);
        assertTrue("Failure deploying Service Unit",
            (-1 < msg.indexOf(Constants.SU_NAME)));
    }

    /**
     * Tests deploy with a null Service Unit name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeployBadNullServiceUnitName()
        throws Exception
    {
        try
        {
            mDeployer.deploy(null, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitName")));
        }
    }

    /**
     * Tests deploy with a null Service Unit root path parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeployBadNullServiceUnitRootPath()
        throws Exception
    {
        try
        {
            mDeployer.deploy(Constants.SU_NAME, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitRootPath")));
        }
    }

    /**
     * Tests deploy with a Service Unit that has already been deployed.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeployBadAlreadyDeployed()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        // Try to deploy again

        try
        {
            mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2451")));
        }
    }

    /**
     * Tests deploy with component that is not started.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeployBadComponentNotStarted()
        throws Exception
    {
        // Stop the component

        mCompFW.stopComponent(mComponent);

        // Attempt to deploy the SU. This should fail.
        try
        {
            mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2462")));
        }
    }

    /**
     * Tests deploy with an exception from the deploy method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeployBadException()
        throws Exception
    {
        try
        {
            mDeployer.deploy(Constants.SU_NAME_DEPLOY_EXCEPTION,
                Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("failed to deploy")));
        }
    }

    /**
     * Tests deploy with a missing ServiceUnitManager. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeployBadNoSuMgr()
        throws Exception
    {
        mDeployer.setServiceUnitManager(null);
        try
        {
            mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2463")));
        }
    }

    /**
     * Tests deploy with a timeout on the deploy method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDeployBadTimeout()
        throws Exception
    {
        try
        {
            mDeployer.deploy(Constants.SU_NAME_DEPLOY_TIMEOUT,
                Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2461")));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME_DEPLOY_TIMEOUT)));
        }
    }

    /**
     * Tests getDeployments with a good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testGetDeploymentsGood()
        throws Exception
    {
        // First test with no deployments

        String[] deployments = mDeployer.getDeployments();
        assertEquals("Failure on getDeployments: ",
            0, deployments.length);

        // Now test with one deployment

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        deployments = mDeployer.getDeployments();
        assertEquals("Failure on getDeployments: ",
            1, deployments.length);
        assertEquals("Failure on getDeployments: ",
            Constants.SU_NAME, deployments[0]);
    }

    /**
     * Tests getServiceUnitState with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetServiceUnitStateGood()
        throws Exception
    {
        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        ServiceUnitState state = mDeployer.getServiceUnitState(Constants.SU_NAME);
        assertEquals("Failure on getServiceUnitState: ",
            su.getState(), state);
    }

    /**
     * Tests getServiceUnitState with a null Service Unit name.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetServiceUnitStateBadNullServiceUnitName()
        throws Exception
    {
        try
        {
            ServiceUnitState state = mDeployer.getServiceUnitState(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitName")));
        }
    }

    /**
     * Tests getServiceUnitState with a non-existent SU.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetServiceUnitStateBadServiceUnitNotDeployed()
        throws Exception
    {
        ServiceUnitState state = mDeployer.getServiceUnitState(Constants.SU_NAME);
        assertEquals("Failure on getServiceUnitState: ",
            ServiceUnitState.UNKNOWN, state);
    }

    /**
     * Tests isDeployed with a good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testIsDeployedGood()
        throws Exception
    {
        // First test with SU not deployed

        assertFalse("Failure on isDeployed: ",
            mDeployer.isDeployed(Constants.SU_NAME));

        // Now test with SU deployed

        String msg = mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        assertTrue("Failure on isDeployed: ",
            mDeployer.isDeployed(Constants.SU_NAME));
    }

    /**
     * Tests isDeployed with a null Service Unit name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsDeployedBadNullServiceUnitName()
        throws Exception
    {
        try
        {
            mDeployer.isDeployed(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitName")));
        }
    }

    /**
     * Tests init with a good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testInitGood()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Test the init

        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
        assertEquals("Failure on init: ",
            ServiceUnitState.STOPPED, su.getState());
        assertEquals("Failure on init: ",
            ServiceUnitState.STOPPED, su.getDesiredState());

        // Test the init again, this one's a no-op

        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
        assertEquals("Failure on init: ",
            ServiceUnitState.STOPPED, su.getState());
        assertEquals("Failure on init: ",
            ServiceUnitState.STOPPED, su.getDesiredState());
    }

    /**
     * Tests init with a null Service Unit name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitBadNullServiceUnitName()
        throws Exception
    {
        try
        {
            mDeployer.init(null, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitName")));
        }
    }

    /**
     * Tests init with a null Service Unit root path parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitBadNullServiceUnitRoot()
        throws Exception
    {
        try
        {
            mDeployer.init(Constants.SU_NAME, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitRootPath")));
        }
    }

    /**
     * Tests init with a Service Unit that is not deployed.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitBadServiceUnitNotDeployed()
        throws Exception
    {
        try
        {
            mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2450")));
        }
    }

    /**
     * Tests init with a Service Unit that is started.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitBadServiceUnitStarted()
        throws Exception
    {
        // Deploy the SU, initialize it, and start it

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);
        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
        mDeployer.start(Constants.SU_NAME);

        // This init will fail

        try
        {
            mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2452")));
            assertEquals("Failure on init: ",
                ServiceUnitState.STARTED, su.getDesiredState());
        }
    }

    /**
     * Tests init with a component that is not started.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitBadComponentNotStarted()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Stop the component

        mCompFW.stopComponent(mComponent);

        // Attempt to init the SU. This should fail.
        try
        {
            mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2462")));
            assertEquals("Failure on init: ",
                ServiceUnitState.SHUTDOWN, su.getDesiredState());
        }
    }

    /**
     * Tests init with an exception from the init method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitBadException()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME_INIT_EXCEPTION, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_INIT_EXCEPTION, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_INIT_EXCEPTION);

        // This init will fail

        try
        {
            mDeployer.init(Constants.SU_NAME_INIT_EXCEPTION, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("failed to initialize")));
            assertEquals("Failure on init: ",
                ServiceUnitState.STOPPED, su.getDesiredState());
        }
    }

    /**
     * Tests init with a timeout on the init method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitBadTimeout()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME_INIT_TIMEOUT, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_INIT_TIMEOUT, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_INIT_TIMEOUT);

        // This init will time out

        try
        {
            mDeployer.init(Constants.SU_NAME_INIT_TIMEOUT, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME_INIT_TIMEOUT)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2461")));
            assertEquals("Failure on init: ",
                ServiceUnitState.STOPPED, su.getDesiredState());
        }
    }

    /**
     * Tests start with a good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testStartGood()
        throws Exception
    {
        // Deploy the SU and initialize it

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);
        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);

        // Test the start

        mDeployer.start(Constants.SU_NAME);
        assertEquals("Failure on start: ",
            ServiceUnitState.STARTED, su.getState());

        // Test the start again, this is a no-op

        mDeployer.start(Constants.SU_NAME);
        assertEquals("Failure on start: ",
            ServiceUnitState.STARTED, su.getState());
        assertEquals("Failure on start: ",
            ServiceUnitState.STARTED, su.getDesiredState());
    }

    /**
     * Tests start with a null Service Unit name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartBadNullServiceUnitName()
        throws Exception
    {
        try
        {
            mDeployer.start(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitName")));
        }
    }

    /**
     * Tests start with a Service Unit that is not deployed.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartBadServiceUnitNotDeployed()
        throws Exception
    {
        try
        {
            mDeployer.start(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2450")));
        }
    }

    /**
     * Tests start with a Service Unit that is not initialized.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartBadServiceUnitNotInitialized()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // This start will fail

        try
        {
            mDeployer.start(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2453")));
            assertEquals("Failure on start: ",
                ServiceUnitState.SHUTDOWN, su.getDesiredState());
        }
    }

    /**
     * Tests start with a component that is not started.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartBadComponentNotStarted()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Stop the component

        mCompFW.stopComponent(mComponent);

        // Attempt to start the SU. This should fail.
        try
        {
            mDeployer.start(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2462")));
            assertEquals("Failure on start: ",
                ServiceUnitState.SHUTDOWN, su.getDesiredState());
        }
    }

    /**
     * Tests start with an exception from the start method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartBadException()
        throws Exception
    {
        // Deploy the SU and initialize it

        mDeployer.deploy(Constants.SU_NAME_START_EXCEPTION, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_START_EXCEPTION, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_START_EXCEPTION);
        mDeployer.init(Constants.SU_NAME_START_EXCEPTION, Constants.SU_ROOT);

        // Test the start, this will fail

        try
        {
            mDeployer.start(Constants.SU_NAME_START_EXCEPTION);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("failed to start")));
            assertEquals("Failure on start: ",
                ServiceUnitState.STARTED, su.getDesiredState());
        }
    }

    /**
     * Tests start with a timeout on the start method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartBadTimeout()
        throws Exception
    {
        // Deploy the SU and initialize it

        mDeployer.deploy(Constants.SU_NAME_START_TIMEOUT, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_START_TIMEOUT, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_START_TIMEOUT);
        mDeployer.init(Constants.SU_NAME_START_TIMEOUT, Constants.SU_ROOT);

        // Test the start, this will time out

        try
        {
            mDeployer.start(Constants.SU_NAME_START_TIMEOUT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME_START_TIMEOUT)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2461")));
            assertEquals("Failure on start: ",
                ServiceUnitState.STARTED, su.getDesiredState());
        }
    }

    /**
     * Tests stop with a good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testStopGood()
        throws Exception
    {
        // Deploy the SU and initialize and start it

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);
        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
        mDeployer.start(Constants.SU_NAME);

        // Test the stop

        mDeployer.stop(Constants.SU_NAME);
        assertEquals("Failure on stop: ",
            ServiceUnitState.STOPPED, su.getState());
        assertEquals("Failure on stop: ",
            ServiceUnitState.STOPPED, su.getDesiredState());

        // Test the stop again, this is a no-op

        mDeployer.stop(Constants.SU_NAME);
        assertEquals("Failure on stop: ",
            ServiceUnitState.STOPPED, su.getState());
        assertEquals("Failure on stop: ",
            ServiceUnitState.STOPPED, su.getDesiredState());
    }

    /**
     * Tests stop with a null Service Unit name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopBadNullServiceUnitName()
        throws Exception
    {
        try
        {
            mDeployer.stop(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitName")));
        }
    }

    /**
     * Tests stop with a Service Unit that is not deployed.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopBadServiceUnitNotDeployed()
        throws Exception
    {
        try
        {
            mDeployer.stop(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2450")));
        }
    }

    /**
     * Tests stop with a Service Unit that is not initialized.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopBadServiceUnitNotInitialized()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // This stop will fail

        try
        {
            mDeployer.stop(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2454")));
            assertEquals("Failure on stop: ",
                ServiceUnitState.SHUTDOWN, su.getDesiredState());
        }
    }

    /**
     * Tests stop with a component that is not started.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopBadComponentNotStarted()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Stop the component

        mCompFW.stopComponent(mComponent);

        // Attempt to stop the SU. This should fail.
        try
        {
            mDeployer.stop(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2462")));
            assertEquals("Failure on stop: ",
                ServiceUnitState.SHUTDOWN, su.getDesiredState());
        }
    }

    /**
     * Tests stop with an exception from the stop method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopBadException()
        throws Exception
    {
        // Deploy the SU and initialize and start it

        mDeployer.deploy(Constants.SU_NAME_STOP_EXCEPTION, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_STOP_EXCEPTION, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_STOP_EXCEPTION);
        mDeployer.init(Constants.SU_NAME_STOP_EXCEPTION, Constants.SU_ROOT);
        mDeployer.start(Constants.SU_NAME_STOP_EXCEPTION);

        // Test the stop, this will fail

        try
        {
            mDeployer.stop(Constants.SU_NAME_STOP_EXCEPTION);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("failed to stop")));
            assertEquals("Failure on stop: ",
                ServiceUnitState.STOPPED, su.getDesiredState());
        }
    }

    /**
     * Tests stop with a timeout on the stop method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopBadTimeout()
        throws Exception
    {
        // Deploy the SU and initialize and start it

        mDeployer.deploy(Constants.SU_NAME_STOP_TIMEOUT, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_STOP_TIMEOUT, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_STOP_TIMEOUT);
        mDeployer.init(Constants.SU_NAME_STOP_TIMEOUT, Constants.SU_ROOT);
        mDeployer.start(Constants.SU_NAME_STOP_TIMEOUT);

        // Test the stop, this will time out

        try
        {
            mDeployer.stop(Constants.SU_NAME_STOP_TIMEOUT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME_STOP_TIMEOUT)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2461")));
            assertEquals("Failure on stop: ",
                ServiceUnitState.STOPPED, su.getDesiredState());
        }
    }

    /**
     * Tests shutdown with a good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testShutDownGood()
        throws Exception
    {
        // Deploy the SU and initialize, start, and stop it

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);
        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
        mDeployer.start(Constants.SU_NAME);
        mDeployer.stop(Constants.SU_NAME);

        // Test the shutdown

        mDeployer.shutDown(Constants.SU_NAME);
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getState());
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getDesiredState());

        // Test the shutdown again, this is a no-op

        mDeployer.shutDown(Constants.SU_NAME);
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getState());
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getDesiredState());
    }

    /**
     * Tests shutDown with a null Service Unit name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownBadNullServiceUnitName()
        throws Exception
    {
        try
        {
            mDeployer.shutDown(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitName")));
        }
    }

    /**
     * Tests shutDown with a Service Unit that is not deployed.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownBadServiceUnitNotDeployed()
        throws Exception
    {
        try
        {
            mDeployer.shutDown(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2450")));
        }
    }

    /**
     * Tests shutdown with a Service Unit that is not stopped.
     * @throws Exception if an unexpected error occurs.
     */

    public void testShutDownBadNotStopped()
        throws Exception
    {
        // Deploy the SU and initialize and start it

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);
        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
        mDeployer.start(Constants.SU_NAME);

        // Test the shutdown

        try
        {
            mDeployer.shutDown(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2455")));
            assertEquals("Failure on shutDown: ",
                ServiceUnitState.STARTED, su.getDesiredState());
        }
    }

    /**
     * Tests shutdown with a component that is not started.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownBadComponentNotStarted()
        throws Exception
    {
        // Deploy the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Stop the component

        mCompFW.stopComponent(mComponent);

        // Attempt to shut down the SU. This should fail.
        try
        {
            mDeployer.shutDown(Constants.SU_NAME);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2462")));
            assertEquals("Failure on shutDown: ",
                ServiceUnitState.SHUTDOWN, su.getDesiredState());
        }
    }

    /**
     * Tests shutDown with an exception from the shutDown method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownBadException()
        throws Exception
    {

        // Deploy the SU and initialize it

        mDeployer.deploy(Constants.SU_NAME_SHUTDOWN_EXCEPTION, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_SHUTDOWN_EXCEPTION, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_SHUTDOWN_EXCEPTION);
        mDeployer.init(Constants.SU_NAME_SHUTDOWN_EXCEPTION, Constants.SU_ROOT);

        // Test the shutdown, this will fail

        try
        {
            mDeployer.shutDown(Constants.SU_NAME_SHUTDOWN_EXCEPTION);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("failed to shut down")));
            assertEquals("Failure on shutDown: ",
                ServiceUnitState.SHUTDOWN, su.getDesiredState());
        }
    }

    /**
     * Tests shutDown with a timeout on the shutDown method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownBadTimeout()
        throws Exception
    {

        // Deploy the SU and initialize it.

        mDeployer.deploy(Constants.SU_NAME_SHUTDOWN_TIMEOUT, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_SHUTDOWN_TIMEOUT, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_SHUTDOWN_TIMEOUT);
        mDeployer.init(Constants.SU_NAME_SHUTDOWN_TIMEOUT, Constants.SU_ROOT);

        // Test the shutdown, this will time out

        try
        {
            mDeployer.shutDown(Constants.SU_NAME_SHUTDOWN_TIMEOUT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME_SHUTDOWN_TIMEOUT)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2461")));
            assertEquals("Failure on shutDown: ",
                ServiceUnitState.SHUTDOWN, su.getDesiredState());
        }
    }

    /**
     * Tests forced shutdown with a Service Unit that is not stopped.
     * @throws Exception if an unexpected error occurs.
     */

    public void testShutDownForceGoodNotStopped()
        throws Exception
    {
        // Deploy the SU and initialize and start it

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);
        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);
        mDeployer.start(Constants.SU_NAME);

        // Test the shutdown

        mDeployer.shutDown(Constants.SU_NAME, true);
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getState());
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getDesiredState());
    }

    /**
     * Tests forced shutDown with an exception from the shutDown method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownForceGoodException()
        throws Exception
    {

        // Deploy the SU and initialize it

        mDeployer.deploy(Constants.SU_NAME_SHUTDOWN_EXCEPTION, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_SHUTDOWN_EXCEPTION, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_SHUTDOWN_EXCEPTION);
        mDeployer.init(Constants.SU_NAME_SHUTDOWN_EXCEPTION, Constants.SU_ROOT);

        // Test the shutdown

        mDeployer.shutDown(Constants.SU_NAME_SHUTDOWN_EXCEPTION, true);
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getState());
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getDesiredState());
    }

    /**
     * Tests forced shutDown with a timeout on the shutDown method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownForceGoodTimeout()
        throws Exception
    {

        // Deploy the SU and initialize it.

        mDeployer.deploy(Constants.SU_NAME_SHUTDOWN_TIMEOUT, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_SHUTDOWN_TIMEOUT, mServiceUnitRoot);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME_SHUTDOWN_TIMEOUT);
        mDeployer.init(Constants.SU_NAME_SHUTDOWN_TIMEOUT, Constants.SU_ROOT);

        // Test the shutdown

        mDeployer.shutDown(Constants.SU_NAME_SHUTDOWN_TIMEOUT, true);
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getState());
        assertEquals("Failure on shutDown: ",
            ServiceUnitState.SHUTDOWN, su.getDesiredState());
    }

    /**
     * Tests undeploy with a good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testUndeployGood()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        // Now undeploy the SU

        String msg = mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT);
        assertNotNull("Failure undeploying Service Unit",
            msg);
        assertTrue("Failure undeploying Service Unit",
            (-1 < msg.indexOf(Constants.SU_NAME)));
    }

    /**
     * Tests undeploy with a null Service Unit name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeployBadNullServiceUnitName()
        throws Exception
    {
        try
        {
            mDeployer.undeploy(null, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitName")));
        }
    }

    /**
     * Tests undeploy with a null Service Unit root path parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeployBadNullServiceUnitRootPath()
        throws Exception
    {
        try
        {
            mDeployer.undeploy(Constants.SU_NAME, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("serviceUnitRootPath")));
        }
    }

    /**
     * Tests undeploy with a Service Unit that is not deployed.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeployBadNotDeployed()
        throws Exception
    {
        try
        {
            mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2450")));
        }
    }

    /**
     * Tests undeploy with a Service Unit that is not shut down.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeployBadNotShutDown()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        // Now initialize the SU

        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);

        // Now undeploy the SU. This should fail.

        try
        {
            mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2456")));
        }

        // Now start the SU

        mDeployer.start(Constants.SU_NAME);

        // Now undeploy the SU. This should fail.

        try
        {
            mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2456")));
        }
    }

    /**
     * Tests undeploy with component that is not started.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeployBadComponentNotStarted()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        // Stop the component

        mCompFW.stopComponent(mComponent);

        // Attempt to undeploy the SU. This should fail.
        try
        {
            mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2462")));
        }
    }

    /**
     * Tests undeploy with an exception from the undeploy method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeployBadException()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME_UNDEPLOY_EXCEPTION,
            Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_UNDEPLOY_EXCEPTION,
            mServiceUnitRoot);

        // Now undeploy the SU

        try
        {
            mDeployer.undeploy(Constants.SU_NAME_UNDEPLOY_EXCEPTION,
                Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("failed to undeploy")));
        }
    }

    /**
     * Tests undeploy with a timeout on the undeploy method.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeployBadTimeout()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME_UNDEPLOY_TIMEOUT,
            Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_UNDEPLOY_TIMEOUT,
            mServiceUnitRoot);

        // Now undeploy the SU

        try
        {
            mDeployer.undeploy(Constants.SU_NAME_UNDEPLOY_TIMEOUT,
                Constants.SU_ROOT);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME_UNDEPLOY_TIMEOUT)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2461")));
        }
    }

    /**
     * Tests forced undeploy with good results.
     * @throws Exception if an unexpected error occurs.
     */

    public void testUndeployForceGood()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        // Now undeploy the SU

        String msg = mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT,
            true);
        assertNotNull("Failure undeploying Service Unit",
            msg);
        assertTrue("Failure undeploying Service Unit",
            (-1 < msg.indexOf("SUCCESS")));
    }

    /**
     * Tests forced undeploy with component that is not started.
     * @throws Exception if an unexpected error occurs.
     */

    public void testUndeployForceGoodComponentNotStarted()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        // Stop the component

        mCompFW.stopComponent(mComponent);

        // Now undeploy the SU

        String msg = mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT,
            true);
        assertNotNull("Failure undeploying Service Unit",
            msg);
        assertTrue("Failure undeploying Service Unit",
            (-1 < msg.indexOf("SUCCESS")));
    }

    /**
     * Tests forced undeploy with an exception in the SU manager.
     * @throws Exception if an unexpected error occurs.
     */

    public void testUndeployForceGoodException()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME_UNDEPLOY_EXCEPTION, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_UNDEPLOY_EXCEPTION, mServiceUnitRoot);

        // Now undeploy the SU

        String msg = mDeployer.undeploy(Constants.SU_NAME_UNDEPLOY_EXCEPTION,
            Constants.SU_ROOT, true);
        assertNotNull("Failure undeploying Service Unit",
            msg);
        assertTrue("Failure undeploying Service Unit",
            (-1 < msg.indexOf("SUCCESS")));
    }

    /**
     * Tests forced undeploy with a timeout in the SU manager.
     * @throws Exception if an unexpected error occurs.
     */

    public void testUndeployForceGoodTimeout()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME_UNDEPLOY_TIMEOUT, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_UNDEPLOY_TIMEOUT, mServiceUnitRoot);

        // Now undeploy the SU

        String msg = mDeployer.undeploy(Constants.SU_NAME_UNDEPLOY_TIMEOUT,
            Constants.SU_ROOT, true);
        assertNotNull("Failure undeploying Service Unit",
            msg);
        assertTrue("Failure undeploying Service Unit",
            (-1 < msg.indexOf("SUCCESS")));
    }

    /**
     * Tests forced undeploy with a Service Unit that is not shut down.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUndeployForceBadNotShutDown()
        throws Exception
    {
        // First deploy and register the SU

        mDeployer.deploy(Constants.SU_NAME, Constants.SU_ROOT);
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, mServiceUnitRoot);

        // Now initialize the SU

        mDeployer.init(Constants.SU_NAME, Constants.SU_ROOT);

        // Now undeploy the SU. This should fail.

        try
        {
            mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT, true);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2456")));
        }

        // Now start the SU

        mDeployer.start(Constants.SU_NAME);

        // Now undeploy the SU. This should fail.

        try
        {
            mDeployer.undeploy(Constants.SU_NAME, Constants.SU_ROOT, true);
            fail("Expected exception not received");
        }
        catch (DeploymentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf(Constants.SU_NAME)));
            assertTrue("Incorrect exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("JBIFW2456")));
        }
    }

}
