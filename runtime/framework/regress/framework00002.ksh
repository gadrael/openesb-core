#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)framework00002.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

echo testing private library invocation for engines/bindings

# package components
ant -emacs -q -f $JV_FRAMEWORK_REGRESS_DIR/scripts/build-framework00002-components.ant build-components

# install engine 
$JBI_ANT -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/PrivateLibraryTestEngine.jar install-component

# start engine (this invokes the private lib API)
$JBI_ANT -Djbi.component.name="PrivateLibraryTestEngine" start-component

# shutdown engine
$JBI_ANT -Djbi.component.name="PrivateLibraryTestEngine" shut-down-component

# uninstall engine
$JBI_ANT -Djbi.component.name="PrivateLibraryTestEngine" uninstall-component

echo Private Library Invocation
grep "SUN'S STOCK VALUE = 25.0" $JBI_DOMAIN_ROOT/logs/classloaderregresstests.privatelibtest.engine.rt.log | wc -l
