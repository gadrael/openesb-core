/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.StringTranslator;

import java.io.File;
import java.util.logging.Logger;

import javax.jbi.management.MBeanNames;
import javax.jbi.messaging.DeliveryChannel;
import javax.management.MBeanServer;
import javax.naming.InitialContext;

/**
 * Tests for the ComponentContext class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentContext
    extends junit.framework.TestCase
{
    /**
     * EnvironmentContext
     */
    private EnvironmentContext mEnvironmentContext;

    /**
     * EnvironmentSetup
     */
    private EnvironmentSetup mEnvironmentSetup;

    /**
     * Component
     */
    private Component mComponent;

    /**
     * ComponentContext
     */
    private ComponentContext mComponentContext;

    /**
     * Component install root directory
     */
    private String mInstallRoot;

    /**
     * Component work root directory
     */
    private String mWorkspaceRoot;

    /**
     * JBI root directory
     */
    private String mJbiRoot;

    /**
     * Current test name
     */
    private String mTestName;

    /**
     * Constant for component name
     */
    private static final String COMPONENT_NAME = "TestComponent";

    /**
     * Constant for package name
     */
    private static final String PACKAGE_NAME = "com.sun.jbi.framework";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentContext(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test. This creates the ComponentContext instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);

        // Create an EnvironmentContext
        mEnvironmentSetup = new EnvironmentSetup();
        mEnvironmentContext = mEnvironmentSetup.getEnvironmentContext();

        // Set JBI and component roots
        mJbiRoot = mEnvironmentContext.getJbiInstanceRoot();
        mInstallRoot = mJbiRoot + "/" + COMPONENT_NAME;
        mWorkspaceRoot = mJbiRoot + "/" + COMPONENT_NAME + "/" + "workspace";

        // Create a Component
        Binding instance = new Binding();
        mComponent = new Component();
        mComponent.setName(COMPONENT_NAME);
        mComponent.setComponentInstance(instance);
        mComponent.setInstallRoot(mInstallRoot);
        mComponent.setWorkspaceRoot(mWorkspaceRoot);
        mComponent.setStatisticsInstance(new ComponentStatistics(COMPONENT_NAME));

        // Create a ComponentContext
        mComponentContext =
            new ComponentContext(mComponent, mEnvironmentContext);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        System.err.println("***** END of test " + mTestName);
    }

// =============================  test methods ================================

    /**
     * Test the method for creating a custom component MBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testCreateCustomComponentMBeanName()
        throws Exception
    {
        javax.management.ObjectName mbn;

        mComponent.setComponentTypeBinding();
        mbn = mComponentContext.createCustomComponentMBeanName(
            "InstallerConfigurationMBean");
        assertNotNull("Failure on createCustomComponentMBeanName(): " +
            "expected a non-null value, got a null", mbn);
        System.out.println("MBean name is " + mbn);
        mComponent.setComponentTypeEngine();
        mbn = mComponentContext.createCustomComponentMBeanName(
            "InstallerConfigurationMBean");
        assertNotNull("Failure on createCustomComponentMBeanName(): " +
            "expected a non-null value, got a null", mbn);
        System.out.println("MBean name is " + mbn);
    }

    /**
     * Test the get method for the delivery channel.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetDeliveryChannel()
        throws Exception
    {
        DeliveryChannel dc1 = mComponentContext.getDeliveryChannel(false);
        DeliveryChannel dc2 = mComponentContext.getDeliveryChannel(true);
        DeliveryChannel dc3 = mComponentContext.getDeliveryChannel(false);
        DeliveryChannel dc4 = mComponentContext.getDeliveryChannel();
        assertNull("Failure on getDeliveryChannel(false): " +
                   "expected a null, got a non-null value",
                   dc1);
        assertNotNull("Failure on getDeliveryChannel(true): " +
                      "expected a non-null value, got a null",
                      dc2);
        assertSame("Failure on getDeliveryChannel(false): ",
                   dc3, dc2);
        assertSame("Failure on getDeliveryChannel(): ",
                   dc4, dc2);
    }

    /**
     * Test the get method for the component name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentName()
        throws Exception
    {
        String name = mComponentContext.getComponentName();
        assertEquals("Failure on getComponentName(): ",
                     name, COMPONENT_NAME);
    }

    /**
     * Test the get method for the component install root directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetInstallRoot()
        throws Exception
    {
        String ir = mComponentContext.getInstallRoot();
        assertEquals("Failure on getInstallRoot(): ",
                     mInstallRoot.replace('/', File.separatorChar), ir);
    }

    /**
     * Test the get method for the JMX domain name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetJmxDomainName()
        throws Exception
    {
        String dn = mComponentContext.getJmxDomainName();
        assertEquals("Failure on getJmxDomainName(): ",
                     dn, mEnvironmentContext.getMBeanNames().getJmxDomainName());
    }

    /**
     * Test the get method for component loggers.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLogger()
        throws Exception
    {
        // Get a main logger with no name suffix and no resource bundle.

        Logger l1 = mComponentContext.getLogger("", null);
        assertTrue("Main logger has wrong name: " + l1.getName(),
                   l1.getName().equals(COMPONENT_NAME));

        // Get a secondary logger with the suffix "secondary" and no resource
        // bundle.

        String suffix = "secondary";
        Logger l2 = mComponentContext.getLogger(suffix, null);
        assertTrue("Secondary logger has wrong name: " + l2.getName(),
                   l2.getName().startsWith(COMPONENT_NAME));
        assertTrue("Secondary logger has wrong name: " + l2.getName(),
                   l2.getName().endsWith(suffix));

        // Make sure that a repeated call for the same logger returns the
        // same Logger instance.

        Logger l3 = mComponentContext.getLogger(suffix, null);
        assertSame("Duplicate logger created: " + l3.getName(),
                   l2, l3);

        // Get a secondary logger with suffix "com.sun.jbi.TestBinding"
        // and no resource bundle.

        suffix = "com.sun.jbi.TestBinding";
        Logger l4 = mComponentContext.getLogger(suffix, null);
        assertTrue("Secondary logger has wrong name: " + l4.getName(),
                   l4.getName().startsWith(COMPONENT_NAME));
        assertTrue("Secondary logger has wrong name: " + l4.getName(),
                   l4.getName().endsWith(suffix));
    }

    /**
     * Test the get method for component loggers with a default main logger.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefaultMain()
        throws Exception
    {
        // Get a secondary logger with the suffix "secondary" and no resource
        // bundle. This will also create the main logger by default.

        String suffix = "secondary";
        Logger l1 = mComponentContext.getLogger(suffix, null);
        assertTrue("Secondary logger has wrong name: " + l1.getName(),
                   l1.getName().startsWith(COMPONENT_NAME));
        assertTrue("Secondary logger has wrong name: " + l1.getName(),
                   l1.getName().endsWith(suffix));

        Logger l2 = l1.getParent();
        assertNotNull("Main logger not created automatically: ",
                      l2);
        assertTrue("Main logger has wrong name: ",
                   l2.getName().equals(COMPONENT_NAME));

        // Get the main logger and verify that it is the one created by the
        // first call to getLogger().

        Logger l3 = mComponentContext.getLogger("", null);
        assertSame("Duplicate main logger created: " + l3.getName(),
                   l2, l3);
    }

    /**
     * Test the get method for component loggers and make sure the parents
     * are set properly.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerCheckParent()
        throws Exception
    {
        Logger jbi = Logger.getLogger("com.sun.jbi");

        // Get a main logger with no name suffix and no resource bundle and
        // make sure its parent is set to the top-level JBI logger.

        Logger l1 = mComponentContext.getLogger("", null);
        assertSame("Main logger has wrong parent: ",
                   l1.getParent(), jbi);

        // Get a sub-logger with no resource bundle and make sure its parent
        // is set to the main logger.

        Logger l2 = mComponentContext.getLogger("another", null);
        assertSame("Sub-logger has wrong parent: ",
                   l2.getParent(), l1);
    }

    /**
     * Test the get method for component loggers with resource bundles.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerWithResourceBundle()
        throws Exception
    {
        // Create resource bundle for loggers.
        String rbName = this.getClass().getPackage().getName() + ".TestBundle1";

        // Get a main logger with no name suffix and a resource bundle.

        Logger l1 = mComponentContext.getLogger("", rbName);
        assertTrue("Failure on getLogger(null, rbName): ",
                   l1.getName().startsWith(COMPONENT_NAME));
        assertEquals("Failure on getLogger(null, rbName): ",
                     l1.getResourceBundleName(), rbName);

        // Get a secondary logger with the suffix "secondary" and a resource
        // bundle.

        String suffix = "secondary";
        Logger l2 = mComponentContext.getLogger(suffix, rbName);
        assertTrue("Failure on getLogger(suffix, rbName): ",
                   l2.getName().startsWith(COMPONENT_NAME));
        assertTrue("Failure on getLogger(suffix, rbName): ",
                   l2.getName().endsWith(suffix));
        assertEquals("Failure on getLogger(suffix, rbName): ",
                     l2.getResourceBundleName(), rbName);
    }

    /**
     * Test the getLogger method with a failure due to an invalid "suffix"
     * argument.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerBadNullSuffix()
        throws Exception
    {
        try
        {
            Logger l1 = mComponentContext.getLogger(null, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (ex.getMessage().startsWith("JBIFW1356")));
        }
    }

    /**
     * Test the getLogger method with a failure due to a missing resource
     * bundle. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerBadMissingResource()
        throws Exception
    {
        // Resource bundle name for loggers.
        String rbName = this.getClass().getPackage().getName() + ".BadBundle1";
 
        // Note: due to the behavior of the Logger class, this test has to
        // create its logger with a different name from the previous test
        // to avoid an IllegalArgumentException.

        String suffix = "missingresource";
        try
        {
            Logger l1 = mComponentContext.getLogger(suffix, rbName);
            fail("Expected exception not received");
        }
        catch (java.util.MissingResourceException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf(rbName)));
        }
    }

    /**
     * Test the getLogger method with a failure due to an existing logger
     * using a different resource bundle. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerBadDifferentResource()
        throws Exception
    {
        // Resource bundle names for loggers.
        String rbName1 = this.getClass().getPackage().getName() + ".TestBundle1";
        String rbName2 = this.getClass().getPackage().getName() + ".TestBundle2";
 
        // Note: due to the behavior of the Logger class, this test has to
        // create its logger with a different name from the previous test
        // to avoid an IllegalArgumentException.

        String suffix = "differentresource";
        Logger l1 = mComponentContext.getLogger("", rbName1);
        try
        {
            Logger l2 = mComponentContext.getLogger("", rbName2);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       ( (-1 < ex.getMessage().indexOf(rbName1))
                       && (-1 < ex.getMessage().indexOf(rbName2)) ));
        }
    }

    /**
     * Test the get method for the MBeanNames service.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetMBeanNames()
        throws Exception
    {
        MBeanNames mbn = mComponentContext.getMBeanNames();
        assertNotNull("Failure on getMBeanNames(): " +
                      "expected a non-null value, got a null",
                      mbn);
        assertTrue("Failure on getMBeanNames(): " +
                   "expected a javax.jbi.management.MBeanNames " +
                   "instance, got a " + mbn.getClass().getName(),
                   mbn instanceof javax.jbi.management.MBeanNames);
    }

    /**
     * Test the get method for the MBean server.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetMBeanServer()
        throws Exception
    {
        MBeanServer mbs = mComponentContext.getMBeanServer();
        assertNotNull("Failure on getMBeanServer(): " +
                      "expected a non-null value, got a null",
                      mbs);
    }

    /**
     * Test the get method for the naming context.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetNamingContext()
        throws Exception
    {
        InitialContext nc = mComponentContext.getNamingContext();
        assertNotNull("Failure on getNamingContext(): " +
                      "expected a non-null value, got a null",
                      nc);
    }

    /**
     * Test the get method for the StringTranslator for a package name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringTranslator()
        throws Exception
    {
        StringTranslator st1 = mComponentContext.getStringTranslator(PACKAGE_NAME);
        StringTranslator st2 = mComponentContext.getStringTranslator(PACKAGE_NAME);
        assertNotNull("Failure on getStringTranslator(PACKAGE_NAME): " +
                      "expected a non-null value, got a null",
                      st1);
        assertSame("Received different instances of StringTranslator: ",
                   st1, st2);
    }

    /**
     * Test the get method for the StringTranslator for an object.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringTranslatorFor()
        throws Exception
    {
        StringTranslator st1 = mComponentContext.getStringTranslatorFor(this);
        StringTranslator st2 = mComponentContext.getStringTranslatorFor(this);
        assertNotNull("Failure on getStringTranslatorFor(this): " +
                      "expected a non-null value, got a null",
                      st1);
        assertSame("Received different instances of StringTranslator: ",
                   st1, st2);
    }

    /**
     * Test the get method for the component work root directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetWorkspaceRoot()
        throws Exception
    {
        String wr = mComponentContext.getWorkspaceRoot();
        assertEquals("Failure on getWorkspaceRoot(): ",
                     mWorkspaceRoot.replace('/', File.separatorChar), wr);
    }

}
