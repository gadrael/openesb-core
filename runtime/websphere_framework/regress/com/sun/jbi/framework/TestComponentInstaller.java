/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentInstaller.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.management.ComponentInstallationContext;

import java.io.File;

import javax.management.ObjectName;

/**
 * Tests for the ComponentInstaller class. Note that these test are minimal,
 * because the class does nothing more than delegate to the ComponentFramework.
 * The tests here are merely to make sure the ComponentFramework methods get
 * invoked.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentInstaller
    extends junit.framework.TestCase
{
    /**
     * EnvironmentContext
     */
    private EnvironmentContext mEnvironmentContext;

    /**
     * EnvironmentSetup
     */
    private EnvironmentSetup mEnvironmentSetup;

    /**
     * ComponentInstallationContext
     */
    private ComponentInstallationContext mInstallationContext;

    /**
     * ComponentInstaller
     */
    private ComponentInstaller mComponentInstaller;

    /**
     * Constant for install root
     */
    private static final String COMP_INSTALL_ROOT =
        "/as8/domains/domain1/jbi/TestComponent";

    /**
     * Constant for lifecycle class name
     */
    private static final String COMP_CLASS_NAME =
        "com.sun.jbi.framework.TestComponent";

    /**
     * Constant for component name
     */
    private static final String COMP_NAME =
        "TestComponent";

    /**
     * Constant for workspace root
     */
    private static final String COMP_WORKSPACE_ROOT =
        "/as8/domains/domain1/jbi/work/TestComponent";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentInstaller(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the ComponentInstaller instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

        // Create an EnvironmentContext
        mEnvironmentSetup = new EnvironmentSetup();
        mEnvironmentContext = mEnvironmentSetup.getEnvironmentContext();

        // Start up the Component Registry and Component Framework
        mEnvironmentSetup.startup(true, true);

        // Create a ComponentInstallationContext
        mInstallationContext =
            new ComponentInstallationContext(COMP_NAME,
                                             ComponentInstallationContext.BINDING,
                                             COMP_CLASS_NAME,
                                             null,
                                             null);
        mInstallationContext.setInstallRoot(COMP_INSTALL_ROOT);
        mInstallationContext.setWorkspaceRoot(COMP_WORKSPACE_ROOT);

        // Create a ComponentInstaller
        mComponentInstaller =
            new ComponentInstaller(
                    mInstallationContext,
                    mEnvironmentContext.getComponentFramework(),
                    mEnvironmentContext.getComponentRegistry());
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        mEnvironmentSetup.shutdown(true, true);
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Test the get method for the installer configuration MBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetInstallerConfigurationMBean()
        throws Exception
    {
        // The test is for a fresh install, and will fail because the
        // component's installer has not been loaded.
        try
        {
            ObjectName mbn =
                mComponentInstaller.getInstallerConfigurationMBean();
            fail("Expected exception not received");
        }
        catch ( javax.jbi.JBIException jbiEx )
        {
            // Verification
            assertTrue("Incorrect exception received from install(): " +
                       jbiEx.getMessage(),
                       (-1 < jbiEx.getMessage().indexOf("not found")));
        }
    }

    /**
     * Test the get method for the component install root directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetInstallRoot()
        throws Exception
    {
        String installRoot = mComponentInstaller.getInstallRoot();
        assertEquals("Failure on getInstallRoot(): ",
                     COMP_INSTALL_ROOT.replace('/', File.separatorChar),
                     installRoot);
    }

    /**
     * Test the install method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInstall()
        throws Exception
    {
        // The test is for a fresh install, and will fail because the
        // component's installer has not been loaded.
        try
        {
            ObjectName on = mComponentInstaller.install();
            fail("Expected exception not received");
        }
        catch ( javax.jbi.JBIException jbiEx )
        {
            // Verification
            assertTrue("Incorrect exception received from install(): " +
                       jbiEx.getMessage(),
                       (-1 < jbiEx.getMessage().indexOf("not yet been loaded")));
        }
    }

    /**
     * Test the method for checking whether the component is installed.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsInstalled()
        throws Exception
    {
        boolean installed = mComponentInstaller.isInstalled();
        assertFalse("Failure on isInstalled(): returned true, expected false",
                    installed);
    }

    /**
     * Test the uninstall method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUninstall()
        throws Exception
    {
        try
        {
            mComponentInstaller.uninstall();
            fail("Expected exception not received");
        }
        catch ( javax.jbi.JBIException jbiEx )
        {
            // Verification
            assertTrue("Incorrect exception received from uninstall(): " +
                       jbiEx.getMessage(),
                       (-1 < jbiEx.getMessage().indexOf("no Binding installed")));
        }
    }

    /**
     * Test the uninstall method with the force flag.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUninstallForce()
        throws Exception
    {
        try
        {
            mComponentInstaller.uninstall(true);
            fail("Expected exception not received");
        }
        catch ( javax.jbi.JBIException jbiEx )
        {
            // Verification
            assertTrue("Incorrect exception received from uninstall(): " +
                       jbiEx.getMessage(),
                       (-1 < jbiEx.getMessage().indexOf("no Binding installed")));
        }
    }
}
