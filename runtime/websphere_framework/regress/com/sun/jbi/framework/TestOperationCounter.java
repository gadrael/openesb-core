/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestOperationCounter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.Date;

/**
 * Tests for the OperationCounter class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestOperationCounter
    extends junit.framework.TestCase
{
    /**
     * OperationCounter
     */
    private OperationCounter mCounter;

    /**
     * Constant for 5 second interval in milliseconds.
     */
    private static final long FIVE_SECONDS = 5000;

    /**
     * Constant for count of 5.
     */
    private static final int FIVE = 5;

    /**
     * Constant for count of 4.
     */
    private static final int FOUR = 4;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestOperationCounter(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the OperationCounter instance.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mCounter = new OperationCounter();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Test the increment and decrement methods for the counter.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIncrementDecrement()
        throws Exception
    {
        for ( int i = 1; i <= FIVE; i++ )
        {
            assertEquals("Failure incrementing counter: ",
                         i, mCounter.increment());
        }
        for ( int i = FOUR; i >= 0; i-- )
        {
            assertEquals("Failure decrementing counter: ",
                         i, mCounter.decrement());
        }
    }

    /**
     * Test the notification capability of the decrement() method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testDecrementNotify()
        throws Exception
    {
        for ( int i = 0; i < FIVE; i++ )
        {
            mCounter.increment();
        }
        CounterThread ct = new CounterThread();
        Date start;
        Date stop;
        synchronized (mCounter)
        {
            new Thread(ct).start();
            start = new Date();
            mCounter.wait(FIVE_SECONDS);
            stop = new Date();
        }
        long startTime = start.getTime();
        long stopTime = stop.getTime();
        assertTrue("Notify() failed, wait() timed out",
                   FIVE_SECONDS > (stopTime - startTime));
    }

    /**
     * Class to run on a separate thread to decrement counters.
     */
    class CounterThread implements Runnable
    {
        /**
         * Separate thread to do the counter decrements to test the notify()
         * capability.
         */
        public final void run()
        {
            while ( 0 < mCounter.decrement() )
            {
                ;
            }
        }
    }

}
