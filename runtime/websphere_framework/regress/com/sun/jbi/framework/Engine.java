/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Engine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;

/**
 * This is an implementation of a Service Engine that is purely for
 * unit testing. It does nothing other than log messages when its methods
 * are called.
 *
 * @author Sun Microsystems, Inc.
 */
public class Engine
    extends AbstractComponent
{
    /**
     * Special flag for causing failure on second call to init().
     */
    private boolean mFirst = true;

    /**
     * Constant for timeout tests - 1 minute sleep interval (in milliseconds).
     */
    private static final long TIMEOUT_INTERVAL = 60000;

    /**
     * Public Constructor.
     */
    public Engine()
    {
        mLog = Logger.getLogger("com.sun.jbi.framework.test.Engine");
        mComponentType = "Engine";
    }

    //
    // Overridden ComponentLifeCycle methods
    //

    /**
     * Initialize the Service Engine.
     * @param context the JBI component context created by the JBI framework.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void init(ComponentContext context)
        throws javax.jbi.JBIException
    {
        if ( null != context )
        {
            mContext = context;
            mComponentName = context.getComponentName();
            if ( mComponentName.equals(Constants.SE_NAME_BAD_INIT) )
            {
                throw new javax.jbi.JBIException("EngineBadInit");
            }
            else
            {
                mLog.info("Engine " + mComponentName + " initialized");
            }
        }
        else
        {
            throw new javax.jbi.JBIException("Null argument received for " +
                                             "ComponentContext");
        }

        // This code is here to allow a special test case to run, where the
        // engine is being started after having been shut down, and the
        // init() method throws an exception. The second time init() is
        // called, it throws an exception.

        if ( mFirst )
        {
            mFirst = false;
        }
        else
        {
            throw new javax.jbi.JBIException("Engine initialization failed");
        }
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this SE. This
     * implementation always returns null.
     * @return javax.management.ObjectName is always null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Start the Service Engine.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        if ( mComponentName.equals(Constants.SE_NAME_BAD_START) )
        {
            throw new javax.jbi.JBIException("EngineBadStart");
        }
        else if ( mComponentName.equals(Constants.SE_NAME_TIMEOUT_START) )
        {
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL); /* 5 minutes */
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("EngineInterrupted");
            }
        }
        else
        {
            mLog.info("Engine " + mComponentName + " started");
        }
    }

    /**
     * Stop the Service Engine.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        if ( mComponentName.equals(Constants.SE_NAME_BAD_STOP) )
        {
            throw new javax.jbi.JBIException("EngineBadStop");
        }
        else if ( mComponentName.equals(Constants.SE_NAME_TIMEOUT_STOP) )
        {
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL); /* 5 minutes */
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("EngineInterrupted");
            }
        }
        else
        {
            mLog.info("Engine " + mComponentName + " stopped");
        }
    }

    /**
     * Shut down the Service Engine.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        if ( mComponentName.equals(Constants.SE_NAME_BAD_SHUTDOWN) )
        {
            throw new javax.jbi.JBIException("EngineBadShutdown");
        }
        else if ( mComponentName.equals(Constants.SE_NAME_BAD_SHUTDOWN_BOOTSTRAP_ONUNINSTALL) )
        {
            throw new javax.jbi.JBIException("EngineBadShutdown");
        }
        else if ( mComponentName.equals(Constants.SE_NAME_TIMEOUT_SHUTDOWN) )
        {
            try
            {
                Thread.sleep(TIMEOUT_INTERVAL); /* 5 minutes */
            }
            catch ( java.lang.InterruptedException iEx )
            {
                throw new javax.jbi.JBIException("EngineInterrupted");
            }
        }
        else
        {
            mLog.info("Engine " + mComponentName + " shut down");
        }
    }

    //
    // ServiceUnitManager methods
    //

    /**
     * Deploy a Service Unit.
     * @param serviceUnitName the name of the Service Unit being deployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return a deployment status message.
     * @throws javax.jbi.management.DeploymentException if the deployment
     * operation is unsuccessful.
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info("Engine " + mComponentName + " deployed Service Unit " +
            serviceUnitName);
        return null;
    }

    /**
     * Initialize the deployment.
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state.
     */
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info("Engine " + mComponentName + " initialized Service Unit " +
            serviceUnitName);
    }

    /**
     * Shut down the deployment.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void shutDown(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info("Engine " + mComponentName + " shut down Service Unit " +
            serviceUnitName);
    }

    /**
     * Start the deployment.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info("Engine " + mComponentName + " started Service Unit " +
            serviceUnitName);
    }

    /**
     * Stop the deployment.
     * @param serviceUnitName the name of the Service Unit being stopped.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void stop(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info("Engine " + mComponentName + " stopped Service Unit " +
            serviceUnitName);
    }

    /**
     * Undeploy a Service Unit from the component.
     * @param serviceUnitName the name of the Service Unit being undeployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return an undeployment status message.
     * @throws javax.jbi.management.DeploymentException if the undeployment
     * operation is unsuccessful.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info("Engine " + mComponentName + " undeployed Service Unit " +
            serviceUnitName);
        return null;
    }
}
