/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingBootstrapUpgrade.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.io.File;
import java.util.List;

/**
 * This is an implementation of the bootstrap class for a Binding Component
 * that is purely for unit testing. It does nothing but log messages when
 * its methods are called.
 *
 * @author Sun Microsystems, Inc.
 */
public class BindingBootstrapUpgrade extends BindingBootstrap
{
    /**
     * Perform an upgrade of the component.
     * @param workspaceRoot the workspace root for the new version of the
     * component that will replace the currently installed version. This is
     * populated with the contents of the original workspace root and the
     * component must update it to match the new version of the component.
     * @param serviceUnitRoots a list of directory paths to all of the Service
     * Units currently deployed to the component. The component must update all
     * of these to match the new version of the component.
     * @throws javax.jbi.JBIException when there is an error requiring that the
     * upgrade be terminated.
     */
    public void upgrade(String workspaceRoot, List<String> serviceUnitRoots)
        throws javax.jbi.JBIException
    {
        String name = System.getProperty(Constants.PROPERTY_COMPONENT_NAME);
        mLog.info(name + " upgrade started");
        if ( name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_UPGRADE) )
        {
            throw new javax.jbi.JBIException("unable to complete upgrade");
        }

        // Write a file in the workspace root so that the junit test can
        // verify that the upgrade succeeded.

        writeFile(workspaceRoot, Constants.UPGRADED_FILE_NAME);
        mLog.info("workspace root " + workspaceRoot + " upgraded");

        // Write a file in each SU root so that the junit test can verify
        // that the upgrade succeeded.

        for ( String suRoot : serviceUnitRoots )
        {
            writeFile(suRoot, Constants.UPGRADED_FILE_NAME);   
            mLog.info("SU root " + suRoot + " upgraded");
        }

        mLog.info(name + " upgrade complete");
    }

    /**
     * Utility method to write a file, overwriting it if it already exists.
     *
     * @param path the full path to the directory where the file is to be
     * written.
     * @param name the name of the file to be created.
     */
    private void writeFile(String path, String name)
    {
        File f = new File(path + "/" + name);
        if ( f.exists() )
        {
            f.delete();
        }
        try
        {
            if ( !f.createNewFile() )
            {
                mLog.warning("Unable to create file " + f.getAbsolutePath());
            }
        }
        catch ( java.io.IOException ioEx )
        {
            mLog.warning("Unable to create file " + f.getAbsolutePath() +
                " due to exception: " + ioEx.getMessage());
        }
    }
}
