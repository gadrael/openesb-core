/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EnvironmentSetup.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.framework.ScaffoldPlatformContext;

import java.io.File;
import java.util.logging.Level;
import javax.management.MBeanServerFactory;
import javax.naming.InitialContext;

/**
 * Utility class to provide the setup for unit tests that require a running
 * environment.
 *
 * @author Sun Microsystems, Inc.
 */
public class EnvironmentSetup
{
    /**
     * Value of the $SRCROOT environment variable
     */
    private String mSrcroot;

    /**
     * Value of the $AS8BASE environment variable
     */
    private String mAS8base;

    /**
     * JBI install root directory
     */
    private String mJbiroot;

    /**
     * JBI instance root directory
     */
    private String mJbiInstanceRoot;

    /**
     * Local instance of the EnvironmentContext class
     */
    private EnvironmentContext mContext;

    /**
     * The appserver domain root directory.
     */
    private static final String
        DOMAIN_ROOT = "domains/domain1";

    /**
     * The JBI installation root directory.
     */
    private static final String
        JBI_INSTALL_ROOT = "jbi";

    /**
     * One second (in milliseconds) for timeout properties.
     */
    private static final String
        TIMEOUT_STRING = "1000";

    /**
     * One second (in milliseconds) for timeout values.
     */
    private static final int
        TIMEOUT = 1000;

    /**
     * Constructor.
     * @throws Exception when set up fails for any reason.
     */
    public EnvironmentSetup()
        throws Exception
    {
        mSrcroot = System.getProperty("junit.srcroot");
        mSrcroot = mSrcroot.replace('\\', '/');
        mAS8base = System.getProperty("junit.as8base") + "/";
        mJbiroot = mAS8base + JBI_INSTALL_ROOT;
        mJbiInstanceRoot = mAS8base + DOMAIN_ROOT + "/" + JBI_INSTALL_ROOT;

        System.err.println("srcroot = " + mSrcroot);
        System.err.println("AS8Base = " + mAS8base);
        System.err.println("JBI install root = " + mJbiroot);
        System.err.println("JBI instance root = " + mJbiInstanceRoot);

        // Create and initialize the EnvironmentContext.

        java.util.Properties props = new java.util.Properties();
        props.setProperty("com.sun.jbi.defaultLogLevel",
            Level.FINEST.toString());
        
        // Create a new framework instance with state = ready
        JBIFramework framework = new JBIFramework();
        framework.setFrameworkReady();
        
        ScaffoldPlatformContext platform = new ScaffoldPlatformContext();
        platform.setInstallRoot(mAS8base);
        platform.setInstanceRoot(mAS8base + DOMAIN_ROOT);
        mContext = new ScaffoldEnvironmentContext(platform, framework, props);
        mContext.setComponentTimeout(TIMEOUT);
        mContext.setDeploymentTimeout(TIMEOUT);
        mContext.setInstallationTimeout(TIMEOUT);
        mContext.setServiceUnitTimeout(TIMEOUT);
        mContext.setJbiInstallRoot(mJbiroot);
        mContext.setJbiInstanceRoot(mJbiInstanceRoot);
        mContext.setNamingContext(new InitialContext());
        mContext.setRegistry(new ScaffoldRegistry(mContext));
    }

    /**
     * Get the EnvironmentContext.
     * @return The EnvironmentContext created by the constructor.
     */
    public EnvironmentContext getEnvironmentContext()
    {
        return mContext;
    }
    
    /**
     * Get the scaffolded registry.
     * @return The ScaffoldRegistry created by the constructor.
     */
    public ScaffoldRegistry getScaffoldRegistry()
    {
        return (ScaffoldRegistry)mContext.getRegistry();
    }
    
    /**
     * Startup the test environment.
     * @param startComponentRegistry - set to true to start the Component
     * Registry.
     * @param startComponentFramework - set to true to start the Component
     * Framework.
     * @throws Exception when startup fails for any reason.
     */
    public void startup(
        boolean startComponentRegistry,
        boolean startComponentFramework)
        throws Exception
    {
        if ( startComponentRegistry )
        {
            com.sun.jbi.ServiceLifecycle cr =
                (com.sun.jbi.ServiceLifecycle) mContext.getComponentRegistry();
            cr.initService(mContext);
            cr.startService();
        }
        if ( startComponentFramework )
        {
            com.sun.jbi.ServiceLifecycle cf =
                (com.sun.jbi.ServiceLifecycle) mContext.getComponentFramework();
            cf.initService(mContext);
            cf.startService();
        }
    }

    /**
     * Shutdown the test environment.
     * @param stopComponentRegistry - set to true to stop the Component
     * Registry.
     * @param stopComponentFramework - set to true to stop the Component
     * Framework.
     * @throws Exception when shutdown fails for any reason.
     */
    public void shutdown(
        boolean stopComponentRegistry,
        boolean stopComponentFramework)
        throws Exception
    {
        if ( stopComponentFramework )
        {
            mContext.getComponentFramework().stopService();
        }
        if ( stopComponentRegistry )
        {
            mContext.getComponentRegistry().stopService();
        }
    }
}
