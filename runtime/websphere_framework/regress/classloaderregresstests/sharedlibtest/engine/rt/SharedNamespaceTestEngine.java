/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SharedNamespaceTestEngine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package classloaderregresstests.sharedlibtest.engine.rt;

import javax.jbi.component.ServiceUnitManager;
import javax.jbi.component.ComponentLifeCycle;
import java.util.logging.*;
import javax.management.ObjectName;
import javax.jbi.JBIException;
import java.io.IOException;
import classloaderregresstests.util.*;
import library1.QuoteEngine;


/**
 * This is an implementation of a Business Process Engine used to 
 * test the various use cases of the classloader design
 * In particular it tests if a private library specified in the "componentClassPath"
 * element of jbi.xml
 *
 * @author Sun Microsystems, Inc.
 */
public class SharedNamespaceTestEngine implements javax.jbi.component.ComponentLifeCycle, javax.jbi.component.Component
{            
    /**
     * Local copy of the component ID
     */
    private String mComponentName;

    /**
     * Local handle to the EnvironmentContext
     */
    private javax.jbi.component.ComponentContext mContext;

    /**
     * Logger instance
     */
    private String logStr = "classloaderregresstests.sharedlibtest.engine.rt";
    private Logger mLog = Logger.getLogger(logStr);
    private int msgCount = 0;


    /**
     * Initialize the Business Process Engine.
     * @param context the JBI environment context created
     * by the JBI framework
     * @throws JBIException if an error occurs
     */
    public void init(javax.jbi.component.ComponentContext context)
        throws javax.jbi.JBIException
    {
        if ( null != context )
        {
            mComponentName = context.getComponentName();

            // setup the logger
	    try
	    {
	       Utils.setUpLogger(mLog , new FileHandler( Utils.getLogLocation(logStr) )
	                          , new SimpleFormatter() , Level.INFO);
	       mLog.info("SNS User Engine " + mComponentName + " initialized");
	    }
	    catch (IOException ioe)
	    {
	       System.out.println ("Could not setup logger") ;
	    }
        }
        else
        {
            throw new javax.jbi.JBIException("Null argument received for " +
                                             "EnvironmentContext");
        }
    }

    /**
     * Get the JMX ObjectName for the DeploymentMBean for this BPE. If there
     * is none, return null.
     * @return ObjectName the JMX object name of the DeploymentMBean or null
     * if there is no DeploymentMBean.
     */
    public ObjectName getDeploymentMBeanName()
    {
        return null;
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this BPE. If there
     * is none, return null.
     * @return ObjectName the JMX object name of the additional MBean or null
     * if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Start the Business Process Engine.
     * @throws JBIException if an error occurs
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.info("SNS User Engine " + mComponentName + " started");
	try
	{
	    mLog.info ("MSFT'S STOCK VALUE = " + QuoteEngine.getQuote("MSFT"));
	}
	catch (Exception cnfe)
	{
            mLog.info ("Unable to find QuoteEngine reference :" + cnfe.toString());
	    throw new JBIException ( "could not load QuoteEngine:" , cnfe);
	}
    }

    /**
     * Stop the Business Process Engine.
     * @throws JBIException if an error occurs
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLog.info("SNS User Engine " + mComponentName + " stopped");
    }

    /**
     * Shut down the Business Process Engine.
     * @throws JBIException if an error occurs
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLog.info("SNS User Engine " + mComponentName + " shut down");
    }


    // javax.jbi.component.Component interface
    
    /**
     * Get the ComponentLifeCycle implementation instance for this Binding
     * Component
     * @return the lifecycle impl instance
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return this;
    }

    /**
     * Get the ServiceUnitManager implementation instance for this Binding
     * Component.
     * @return the Service Unit manager implementation instance.
     */
     public ServiceUnitManager getServiceUnitManager()
     {
         mLog.info("Engine " + mComponentName + " getServiceUnitManager called");
         return null;
     }

    /**
     * Resolve descriptor details for the specified reference, which is for a
     * service provided by this component.
     * @param ref the endpoint reference to be resolved.
     * @return the description for the specified reference.
     */
     public org.w3c.dom.Document getServiceDescription(
		         javax.jbi.servicedesc.ServiceEndpoint ref)
     {
         mLog.info("Engine " + mComponentName + " getServiceDescription called");
			             return null;
     }
     
     /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public javax.jbi.servicedesc.ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment epr)
    {
        return null;
    }
}
