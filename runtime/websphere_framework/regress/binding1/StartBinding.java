/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StartBinding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package binding1;

import com.sun.jbi.framework.AbstractComponent;
import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;

/**
 * This is an implementation of a Binding life cycle which starts a 
 * thread.  The thread is the main implementation of the binding.
 *
 * @author Sun Microsystems, Inc.
 */
public class StartBinding extends AbstractComponent
{
    /**
     *  Actual binding implementation
     */
    private BindingImpl mBinding;

    /**
     * Public constructor.
     */
    public StartBinding()
    {
        mLog = Logger.getLogger("com.sun.jbi.framework.test");
        mComponentType = "Binding";
    }

    //
    // Overridden ComponentLifeCycle methods
    //

    /**
     * Initialize the Binding Component.
     * @param context the JBI component context created
     * by the JBI framework
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void init(ComponentContext context)
        throws javax.jbi.JBIException
    {
        if ( null != context )
        {
            mComponentName = context.getComponentName();
            mLog.info("Binding " + mComponentName + " initialized");
        }
        else
        {
            throw new javax.jbi.JBIException(
                "Null argument received for ComponentContext");
        }

        mBinding = new BindingImpl(context, context.getDeliveryChannel(), mLog);
        
        try
        {
            mBinding.init();
        }
        catch (Exception ex)
        {
            throw new javax.jbi.JBIException(ex);
        }
    }

    /**
     * Start the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.info("Binding " + mComponentName + " started");
        mBinding.start();
    }

}
