/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package binding1;

import java.util.Vector;
import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * This test exercises one way MEPs in both directions (InOnly, OutOnly).  The 
 * binding begins by initiating an InOnly exchnage with the engine.  Once that 
 * completes the binding initiates 100 exchanges with the engine in a serial 
 * fashion.  This process reverses from the engine perspective (with OutOnly) 
 * after the InOnly portion completes. 
 *
 * @author Sun Microsystems, Inc.
 */

public class BindingImpl extends Thread
{
    /**
     * Local copy of the component name
     */
    private String mComponentName;

    /**
     * Local copy of the component context
     */
    private ComponentContext mContext;

    /**
     *  Service name for the Binding service.
     */
    private static final QName  BINDING_SERVICE  = new QName("binding_service");

    /**
     *  Endpoint name for the Binding service.
     */
    private static final String BINDING_ENDPOINT = "binding_endpoint";

    /**
     *  Service name for the Engine service.
     */
    private static final QName ENGINE_SERVICE = new QName("engine_service");

    /**
     *  DeliveryChannel for this binding.
     */
    private DeliveryChannel mChannel;
    
    /**
     *  Factory for creating message exchanges.
     */
    private MessageExchangeFactory mFactory;

    /**
     * XML document builder.
     */
    private DocumentBuilder mDomBuilder;

    /**
     * Binding-provided endpoint reference.
     */
    private ServiceEndpoint mBindingEndpoint;

    /**
     * InOnly exchanges.
     */
    private Vector mExchanges;

    /**
     * Logger instance
     */
    private Logger mLog;

    /**
     * Constructor for the Binding Implementation.
     * @param context - the component context for this binding.
     * @param channel - the delivery channel provided by the NMR.
     * @param logger - the logger for this binding.
     */
    public BindingImpl(ComponentContext context,
                       DeliveryChannel channel,
                       Logger logger)
    {
        mComponentName = context.getComponentName();
        mContext = context;
        mChannel = channel;
        mFactory = mChannel.createExchangeFactory();
        mLog = logger;
        mExchanges = new Vector();
    }
    
    /**
     * Initialize.
     * @throws Exception on any error.
     */
    public void init()
        throws Exception
    {        
        mLog.info("BindingImpl activating binding endpoint");
        mBindingEndpoint = mContext.activateEndpoint(BINDING_SERVICE,
            BINDING_ENDPOINT);

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        mDomBuilder = dbf.newDocumentBuilder();
    }

    /**
     *  The thread run method which starts the message exchange.
     */
    public void run()
    {
        try
        {
            /**
             * NOTE: This example provides a serial test of exchanging messages
             * between a binding and an engine.  A single InOnly and OutOnly
             * object is reused over and over again to keep code bloat down.
             * Also, the order of the exchanges is lock-step; any change to the
             * order of sending and receiving may cause this test to fail.
             */            
            
            /*############################################
             *##        PING Binding -> Engine          ##
             *###########################################*/
            
            // Initiate 'ping' exchange with engine
            InOnly ping1 = mFactory.createInOnlyExchange();
            ping1.setEndpoint(mContext.getEndpointsForService(ENGINE_SERVICE)[0]);
            ping1.setOperation(new QName("EnginePing"));
            ping1.setInMessage(createMessage(ping1,
                "Ping message to InOnly service"));
            
            mLog.info("Sending ping exchange to engine " + ENGINE_SERVICE);
            mChannel.send(ping1);

            ping1 = (InOnly) mChannel.accept();
            if (ping1.getStatus() != ExchangeStatus.DONE)
            {
                throw new javax.jbi.messaging.MessagingException(
                    "Engine ping failed");
            }
         
            mLog.info("Engine ping exchange successful");
            
            /*############################################
             *##        PING Engine -> Binding          ##
             *###########################################*/
            
            // Accept 'ping' exchange from engine
            mLog.info("Accepting ping exchange from engine");
            InOnly ping2 = (InOnly) mChannel.accept();
            mLog.info("Exchange received, sending done status");
            ping2.setStatus(ExchangeStatus.DONE);
            mChannel.send(ping2);
            
            /*############################################
             *##    100 Exchanges  Binding -> Engine    ##
             *###########################################*/
            
            // Initiate 100 InOnly exchanges with Engine
            mLog.info("Initiating 100 InOnly Exchanges");
            
            InOnly inOnly;
            
            for (int j = 0; j < 100; ++j)
            {
                // address the exchange, add the in message, and send it
                inOnly = mFactory.createInOnlyExchange();
                inOnly.setEndpoint(mContext.getEndpointsForService(ENGINE_SERVICE)[0]);
                inOnly.setOperation(new QName("InOnlyTest"));
                inOnly.setInMessage(createMessage(inOnly,
                    "Message " + j + " to InOnly Service"));
                mChannel.send(inOnly);
                
                // track the exchange id
                mExchanges.add(inOnly.getExchangeId());
            }
            
            // Now perform 100 accepts to get the status on initiated exchanges
            for (int j = 0; j < 100; ++j)
            {
                inOnly = (InOnly) mChannel.accept();
                
                if (inOnly.getStatus() != ExchangeStatus.DONE)
                {
                    throw new javax.jbi.messaging.MessagingException(
                        "exchange received with incomplete/error status");
                }
                
                // remove the exchange id from our list of pending exchanges
                mExchanges.remove(inOnly.getExchangeId());
            }
            
            // Verify that the pending exchange list is empty
            if (mExchanges.isEmpty())
            {            
                mLog.info("Binding " + mComponentName +
                    " successfully initiated 100 InOnly exchanges to engine");
            }
            else
            {
                mLog.warning("TEST FAILED: " + mExchanges.size() 
                    + " entries remain in the binding's exchange pending list");
            }
            
            /*############################################
             *##    100 Exchanges  Engine -> Binding    ##
             *###########################################*/
            
            // Accept 100 message test from binding            
            mLog.info("BindingImpl -- Accepting 100 InOnly Exchanges");
            for (int j = 0; j < 100; ++j)
            {
                inOnly = (InOnly) mChannel.accept();
                inOnly.setStatus(ExchangeStatus.DONE);
                mChannel.send(inOnly);
            }
            
            mLog.info("BindingImpl -- Accepted 100 Exchanges Successfully");
            
        }
        catch (javax.jbi.JBIException e1)
        {
            mLog.warning("Binding Exception: " + e1.getClass().getName() +
                ", " + e1);
            e1.printStackTrace();
        }

    }

    /**
     * Utility function to create a Normalized message.
     * @param exchange - the message exchange for which a message is needed.
     * @param content - the content to be added to the message.
     * @return the normalized message.
     */
    private NormalizedMessage createMessage(MessageExchange exchange,
        String content)
    {
        NormalizedMessage  msg =  null;
        
        try
        {
            Document doc;
            Element  ele;

            msg = exchange.createMessage();
            doc = mDomBuilder.newDocument();
            ele = doc.createElement("message");

            // fill in some content
            ele.appendChild(doc.createTextNode(content));
            doc.appendChild(ele);
            
            msg.setContent(new DOMSource(doc));
        }
        catch (javax.jbi.messaging.MessagingException e1)
        {
            mLog.warning("MessagingException " + e1);
        }
        return msg;
    }
}
