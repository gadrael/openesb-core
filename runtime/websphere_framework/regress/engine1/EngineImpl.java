/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EngineImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package engine1;

import java.util.Vector;
import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * This test exercises one way MEPs in both directions (InOnly, OutOnly).  The 
 * binding begins by initiating an InOnly exchnage with the engine.  Once that 
 * completes the binding initiates 100 exchanges with the engine in a serial 
 * fashion.  This process reverses from the engine perspective (with OutOnly) 
 * after the InOnly portion completes. 
 *
 * @author Sun Microsystems, Inc.
 */

public class EngineImpl extends Thread
{
    /**
     * Local copy of the component name
     */
    private String mComponentName;

    /**
     * Local copy of the component context
     */
    private ComponentContext mContext;

    /**
     * Service name for the Engine service.
     */
    private static final QName  ENGINE_SERVICE = new QName("engine_service");

    /**
     * Endpoint name for the Engine service.
     */
    private static final String ENGINE_ENDPOINT = "engine_endpoint";

    /**
     * Service name for the Binding service.
     */
    private static final QName BINDING_SERVICE = new QName("binding_service");

    /**
     * Delivery Channel.
     */
    private DeliveryChannel mChannel;
    
    /**
     *  Factory for creating message exchanges.
     */
    private MessageExchangeFactory mFactory;

    /**
     * XML Document builder.
     */
    private DocumentBuilder mDomBuilder;

    /**
     * ServiceEndpoint for binding-provided service.
     */
    private ServiceEndpoint mBindingEndpoint;

    /**
     * ServiceEndpoint for engine-provided service.
     */
    private ServiceEndpoint mEngineEndpoint;

    /**
     * OutOnly exchanges.
     */
    private Vector mExchanges;

    /**
     * Logger instance.
     */
    private Logger mLog;

    /**
     * Constructor for the Engine Implementation.
     * @param context - the component context for this engine.
     * @param channel - the DeliveryChannel from the NMR.
     * @param logger - the logger instance for this engine.
     */
    public EngineImpl(ComponentContext context, DeliveryChannel channel, Logger logger)
    {
        mContext = context;
        mComponentName = context.getComponentName();
        mChannel = channel;
        mFactory = mChannel.createExchangeFactory();
        mLog = logger;
        mExchanges = new Vector();
    }
    
    /**
     * Activate endpoints at init() time.
     * @throws Exception on any error.
     */
    public void init()
        throws Exception
    {
        mLog.info("EngineImpl activating engine endpoint");
        mEngineEndpoint = mContext.activateEndpoint(ENGINE_SERVICE, ENGINE_ENDPOINT);

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        mDomBuilder = dbf.newDocumentBuilder();       
    }

    /**
     *  The thread run method which starts the message exchange.
     */
    public void run()
    {
        try
        {
            /**
             * NOTE : This example provides a serial test of exchanging messages
             * between an engine and a binding.  A single InOnly and OutOnly
             * object is reused over and over again to keep code bloat down.
             * Also, the order of the exchanges is lock-step; any change to the
             * order of sending and receiving may cause this test to fail.
             */
            
            /*############################################
             *##        PING Binding -> Engine          ##
             *###########################################*/
            
            // Accept 'ping' exchange from binding
            mLog.info("Accepting ping exchange from binding");
            InOnly ping1 = (InOnly) mChannel.accept();
            mLog.info("Exchange received, sending done status");
            ping1.setStatus(ExchangeStatus.DONE);
            mChannel.send(ping1);
            
            /*############################################
             *##        PING Engine -> Binding          ##
             *###########################################*/
            
            // Initiate 'ping' exchange with binding
            InOnly ping2 = mFactory.createInOnlyExchange();
            ping2.setEndpoint(mContext.getEndpointsForService(BINDING_SERVICE)[0]);
            ping2.setOperation(new QName("BindingPing"));
            ping2.setInMessage(createMessage(ping2, "Ping message to binding"));
            
            mLog.info("Sending ping exchange to binding " + BINDING_SERVICE);
            mChannel.send(ping2);

            ping2 = (InOnly) mChannel.accept();
            if (ping2.getStatus() != ExchangeStatus.DONE)
            {
                throw new javax.jbi.messaging.MessagingException(
                    "Binding ping failed");
            }
         
            mLog.info("Binding ping exchange successful");
            
            /*############################################
             *##    100 Exchanges  Binding -> Engine    ##
             *###########################################*/
            
            // Accept 100 message test from binding            
            mLog.info("EngineImpl -- Accepting 100 InOnly Exchanges");
            for (int j = 0; j < 100; ++j)
            {
                InOnly inOnly = (InOnly) mChannel.accept();
                inOnly.setStatus(ExchangeStatus.DONE);
                mChannel.send(inOnly);
            }
            
            mLog.info("EngineImpl -- Accepted 100 Exchanges Successfully");
            
            /*############################################
             *##    100 Exchanges  Engine -> Binding    ##
             *###########################################*/
            
            InOnly inOnly;
            
            // Initiate 100 OutOnly exchanges with Binding
            mLog.info("Initiating 100 InOnly Exchanges");
            for (int j = 0; j < 100; ++j)
            {
                // address the exchange, add the in message, and send it
                inOnly = mFactory.createInOnlyExchange();
                inOnly.setEndpoint(mContext.getEndpointsForService(BINDING_SERVICE)[0]);
                inOnly.setOperation(new QName("InOnlyTest"));
                inOnly.setInMessage(createMessage(inOnly,
                    "Message " + j + " to Binding"));
                mChannel.send(inOnly);
                
                // track the exchange id
                mExchanges.add(inOnly.getExchangeId());
            }
            
            // Now perform 100 accepts to get the status on initiated exchanges
            for (int j = 0; j < 100; ++j)
            {
                inOnly = (InOnly) mChannel.accept();
                
                if (inOnly.getStatus() != ExchangeStatus.DONE)
                {
                    throw new javax.jbi.messaging.MessagingException(
                        "exchange received with incomplete/error status");
                }
                
                // remove the exchange id from our list of pending exchanges
                mExchanges.remove(inOnly.getExchangeId());
            }
            
            // Verify that the pending exchange list is empty
            if (mExchanges.isEmpty())
            {            
                mLog.info("Engine " + mComponentName +
                    " successfully initiated 100 InOnly exchanges to binding");
            }
            else
            {
                mLog.warning("TEST FAILED: " + mExchanges.size() 
                    + " entries remain in the engine's exchange pending list");
            }            
            
        }
        catch (javax.jbi.JBIException e1)
        {
            mLog.warning("Engine Exception: " + e1);
        }
    }
    
    /**
     * Utility function to create a Normalized message.
     * @param exchange - the MessageExchange for which a message is needed.
     * @param content - the content to be added to the message.
     * @return a normalized message.
     */
    private NormalizedMessage createMessage(MessageExchange exchange, String content)
    {
        NormalizedMessage  msg =  null;
        
        try
        {
            Document doc;
            Element  ele;

            msg = exchange.createMessage();
            doc = mDomBuilder.newDocument();
            ele = doc.createElement("message");

            // fill in some content
            ele.appendChild(doc.createTextNode(content));
            doc.appendChild(ele);
            
            msg.setContent(new DOMSource(doc));
        }
        catch (javax.jbi.messaging.MessagingException e1)
        {
            mLog.warning("MessagingException " + e1);
        }
        return msg;
    }
}
