#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)framework00007.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

# Tests JBI runtime in stand-alone mode using Java SE.

#regress setup
. ./regress_defs.ksh

# package components
ant -emacs -q -f framework00006.xml package

# start the framework
start_jbise &
startInstanceDelay

# install components
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/slow-engine.jar install-component
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/fast-binding.jar install-component
installComponentDelay

# start components
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-slow-engine" start-component
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-fast-binding" start-component
startComponentDelay

# deploy service assemblies
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.deploy.file=$JV_FRAMEWORK_BLD_DIR/dist/restart-sa-1.jar deploy-service-assembly
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.deploy.file=$JV_FRAMEWORK_BLD_DIR/dist/restart-sa-2.jar deploy-service-assembly
deploySaDelay

# start service assemblies
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-1" start-service-assembly
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-2" start-service-assembly
startSaDelay

# Query the state of our components and service assemblies
$JBISE_ANT -Djbi.binding.component.name="test-fast-binding" list-binding-components
$JBISE_ANT -Djbi.service.engine.name="test-slow-engine" list-service-engines
$JBISE_ANT -Djbi.service.assembly.name="restart-sa-1" list-service-assemblies
$JBISE_ANT -Djbi.service.assembly.name="restart-sa-2" list-service-assemblies

# shut down service assemblies
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-1" shut-down-service-assembly
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-2" shut-down-service-assembly
stopSaDelay

# undeploy service assemblies
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-1" undeploy-service-assembly
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-2" undeploy-service-assembly
undeploySaDelay

# shutdown components
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-slow-engine" shut-down-component
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-fast-binding" shut-down-component
stopComponentDelay

# uninstall components
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-slow-engine" uninstall-component
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-fast-binding" uninstall-component
uninstallComponentDelay

# stop the JBI framework
shutdown_jbise
stopInstanceDelay

# ### END
