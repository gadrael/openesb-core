#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for regression tests.

#set this prop to the test domain. default is JBITest
#my_test_domain=domain1

#use global regress setup:
. $SRCROOT/antbld/regress/common_defs.ksh

REGRESS_CLASSPATH="$JBI_DOMAIN_ROOT/lib/jbi.jar\
${JBI_PS}$AS_INSTALL/lib/jmxremote.jar\
${JBI_PS}$AS_INSTALL/lib/jmxremote_optional.jar\
${JBI_PS}$AS_INSTALL/lib/appserv-rt.jar\
${JBI_PS}$AS_INSTALL/lib/j2ee.jar\
"

#override the variable or define other common scripts here
# the following variables are setup by regress scritps
#JBI_DOMAIN_NAME=JBITest
#JBI_ADMIN_HOST=localhost
#JBI_ADMIN_PORT=8687
#JBI_ADMIN_XML=$SRCROOT/ui/src/scripts/jbi_admin.xml
#JBI_DOMAIN_ROOT=$AS8BASE/domains/$JBI_DOMAIN_NAME

export FRAMEWORK_SRC_DIR FRAMEWORK_REGRESS_DIR FRAMEWORK_BLD_DIR
export JV_FRAMEWORK_SRC_DIR JV_FRAMEWORK_REGRESS_DIR JV_FRAMEWORK_BLD_DIR

if [ -d $SRCROOT/runtime/framework ]; then
    FRAMEWORK_SRC_DIR=$SRCROOT/runtime/framework
    FRAMEWORK_BLD_DIR=$FRAMEWORK_SRC_DIR/bld/test-classes
    JV_FRAMEWORK_SRC_DIR=$JV_SRCROOT/runtime/framework
    JV_FRAMEWORK_BLD_DIR=$JV_FRAMEWORK_SRC_DIR/bld/test-classes
else
    FRAMEWORK_SRC_DIR=$SRCROOT/framework
    FRAMEWORK_BLD_DIR=$FRAMEWORK_SRC_DIR/bld/regress
    JV_FRAMEWORK_SRC_DIR=$JV_SRCROOT/framework
    JV_FRAMEWORK_BLD_DIR=$JV_FRAMEWORK_SRC_DIR/bld/regress
fi
FRAMEWORK_REGRESS_DIR=$FRAMEWORK_SRC_DIR/regress
JV_FRAMEWORK_REGRESS_DIR=$JV_FRAMEWORK_SRC_DIR/regress
