/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Sequencer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;


/**
 *
 * @author Sun Microsystems, Inc.
 */
public class Sequencer 
{
    private int       sequence = 0;
    private int       stallSequence = 0;
    private boolean   stalled = false;
    
    /** Creates a new instance of Sequencer */
    public Sequencer() 
    {
    }
 
    synchronized public void reached(int reached)
    {
        sequence = reached;
        if (stalled && reached >= stallSequence)
        {
            stalled = false;
            this.notify();
        }
    }
    
    synchronized public void stallUntil(int stall)
    {
        stallSequence = stall;
        if (stall > sequence)
        {
            stalled = true;
            do
            {
                try
                {
                    this.wait();
                }
                catch (java.lang.InterruptedException iEx)
                {
                    break;
                }
            } while (stalled);
        }
    }
}
