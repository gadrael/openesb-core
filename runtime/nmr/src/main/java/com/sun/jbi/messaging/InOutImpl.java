/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InOutImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.net.URI;

import javax.jbi.messaging.InOut;
import javax.jbi.messaging.NormalizedMessage;

/** Implementation of In Out Message Exchange Pattern.
 * @author Sun Microsystems, Inc.
 */
public class InOutImpl 
        extends MessageExchangeProxy 
        implements InOut
{    
    /** Simple state machine for the SOURCE side of the  pattern. */
    private static final int[][]  SOURCE  =
    {
        { SET_IN | ADDRESS | SET_TRANSACTION |
            SET_PROPERTY | DO_SEND | DO_SENDSYNCH | MARK_ACTIVE | SUSPEND_TX,
          1, -1, -1, -1
        },
        { DO_ACCEPT | CHECK_STATUS_OR_FAULT | RESUME_TX,
          -1, 2, 5, 2
        },
        { SET_ERROR | SET_DONE | SET_PROPERTY,
          -1, -1, 3, -1
        },
        { SET_ERROR | SET_DONE | DO_SEND | SET_PROPERTY,
          4, -1, -1, -1
        },
        { MARK_DONE,
          -1, -1, -1, -1
        },
        { MARK_DONE | COMPLETE,
          -1, -1, -1, -1
        },
    };

    /** Simple state machine for the TARGET side of the pattern */
    private static final int[][]  TARGET  =
    {
        { DO_ACCEPT | REQUEST | RESUME_TX,
          -1, 1, -1, -1
        },
        { SET_OUT | SET_ERROR | SET_FAULT | CREATE_FAULT |
            DO_SEND | DO_SENDSYNCH | SUSPEND_TX | SET_PROPERTY,
          4, -1, 2, 3 
        },
        { SET_ERROR | DO_SEND | SUSPEND_TX | SET_PROPERTY,
          6, -1, 2, -1
        },
        {
          DO_SEND | DO_SENDSYNCH | SET_FAULT | CREATE_FAULT | SUSPEND_TX | SET_PROPERTY,
          4, -1, -1, 3
        },
        { DO_ACCEPT | STATUS,
          -1, 5, -1, -1
        },
        { MARK_DONE | COMPLETE,
          -1, -1, -1, -1
        },
        { MARK_DONE,
          -1, -1, -1, -1
        },
    };

    /**
     * Default constructor.
     */
    InOutImpl()
    {
        this(SOURCE);
    }
    
    /** Create a new InOutImpl in the forward or reverse direction.
     */
    InOutImpl(int[][] state)
    {
        super(state);
    }
    
    /**
     * Return a new instance of ourselves in the target role.
     */
    MessageExchangeProxy newTwin()
    {
        return (new InOutImpl(TARGET));
    }

    /** Get the pattern.
     * @return the message exchange pattern.
     */
    public URI getPattern()
    {
        return (ExchangePattern.IN_OUT.getURI());
    }

    /** Retrieve the message with reference id "in" from this exchange.
     * @return the out message, or null if it is not present in the exchange
     */    
    public NormalizedMessage getInMessage()
    {
        return getMessage(IN_MSG);
    }
    
    /** Retrieve the message with reference id "out" from this exchange.
     * @return the out message, or null if it is not present in the exchange
     */  
    public NormalizedMessage getOutMessage()
    {
        return getMessage(OUT_MSG);        
    }
    
    /** Specifies the "in" message reference for this exchange.
     *  @param msg in message
     *  @throws javax.jbi.messaging.MessagingException invalid message or the 
     *  current state of the exchange does not permit this operation.
     */
    public void setInMessage(NormalizedMessage msg)
        throws javax.jbi.messaging.MessagingException
    {
        setMessage(msg, IN_MSG);
    }
    
    /** Specifies the "out" message reference for this exchange.
     *  @param msg out message
     *  @throws javax.jbi.messaging.MessagingException invalid message or the 
     *  current state of the exchange does not permit this operation.
     */   
    public void setOutMessage(NormalizedMessage msg)
        throws javax.jbi.messaging.MessagingException
    {
        setMessage(msg, OUT_MSG);
    }

}
