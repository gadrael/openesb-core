/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.messaging;

import com.sun.jbi.messaging.util.RouteType;
import javax.xml.namespace.QName;

/**
 * @author DavidD
 */
public class NonLinkedConsumerInternalEndpoint extends InternalEndpoint {
    
    /**
     * a NonLinkedConsumerInternalEndpoint is not a real activate endpoint 
     * but used to route via interface.
     * We set a default fake ownerID since it is not supposed to be used
     * in the routing process
     */
    public static final String OWNER_ID="myFakeOwnerID";
    
    public NonLinkedConsumerInternalEndpoint(QName serviceName, String endpointName){
        super(serviceName,endpointName,OWNER_ID);
    }
    
}
