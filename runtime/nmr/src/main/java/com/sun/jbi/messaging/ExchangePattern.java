/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExchangePattern.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.net.URI;

import org.w3c.dom.Document;

/** 
 * 
 * @author Sun Microsystems, Inc.
 */
public enum ExchangePattern
{    
    IN_ONLY ("http://www.w3.org", "2004/08", "in-only"),
    IN_OUT ("http://www.w3.org", "2004/08", "in-out"),
    ROBUST_IN_ONLY ("http://www.w3.org", "2004/08", "robust-in-only"),
    IN_OPTIONAL_OUT ("http://www.w3.org", "2004/08", "in-opt-out"),
    UNKNOWN ("", "", "");
    
    private String mPatternStr;
    private URI mPatternUri;
    
    ExchangePattern(String uriRoot, String version, String mep)
    {
        mPatternStr = uriRoot + "/" + version + "/wsdl/" + mep;
        
        try
        {
            mPatternUri = new URI(mPatternStr);
        }
        catch (java.net.URISyntaxException uriEx)
        {
            throw new IllegalArgumentException(mPatternStr, uriEx);
        }
    }
    
    public String toString()
    {
        return mPatternStr;
    }
    
    public URI getURI()
    {
        return mPatternUri;
    }
    
    public static ExchangePattern valueOf(URI pattern)
    {
        String mep = pattern.toString();
        
        // At this point, we only recognize MEPs defined by the W3C WSDL WG
        if (!mep.startsWith("http://www.w3.org/"))
        {
            return UNKNOWN;
        }
        
        // Match based on the actual MEP name
        if (mep.endsWith("/in-only"))
        {
            return IN_ONLY;
        }
        else if (mep.endsWith("/in-out"))
        {
            return IN_OUT;
        }
        else if (mep.endsWith("/robust-in-only"))
        {
            return ROBUST_IN_ONLY;
        }
        else if (mep.endsWith("/in-opt-out"))
        {
            return IN_OPTIONAL_OUT;
        }
        else
        {
            return UNKNOWN;
        }
    }

}
