/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.messaging;

import com.sun.jbi.ComponentManager;
import java.util.ArrayList;

/**
 * @author DavidD
 */
public interface ProviderSelection {
    
    public RegisteredEndpoint selectProvider(MessageExchangeProxy exchange,
                        ArrayList<RegisteredEndpoint> providers,
                        String channelID,
                        ComponentManager componentManager);
    
}
