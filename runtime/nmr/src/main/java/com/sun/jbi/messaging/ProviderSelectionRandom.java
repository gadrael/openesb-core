package com.sun.jbi.messaging;

import com.sun.jbi.ComponentManager;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author DavidD
 */
public class ProviderSelectionRandom extends ProviderSelectionImpl implements ProviderSelection {
    
    public ProviderSelectionRandom(){
        
    }
    
    @Override
    public RegisteredEndpoint selectProvider(MessageExchangeProxy exchange,
                        ArrayList<RegisteredEndpoint> providers,
                        String channelID,
                        ComponentManager componentManager){
        
        /*             
         *JBI 1.0 mentions that we must ask the consumer and the provider
         *if they are okay with this exchange before processing it
         *However, the main components always return true for these two methods
         *except the BPEL engine, which returns false when it should not.
         *So we bypass this verification
         * (Uncomment to restore the verification)
         */
        //providers = removeNotMatchingProvider(exchange, providers, channelID, componentManager);
        
        if(providers.size()>0){
            int rand = ThreadLocalRandom.current().nextInt(0, providers.size());
            return providers.get(rand);
        } 
        
        //no match
        return null;
    }
            
}
