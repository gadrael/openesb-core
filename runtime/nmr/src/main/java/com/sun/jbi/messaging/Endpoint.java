/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.messaging;

import javax.xml.namespace.QName;

/**
 *
 * @author David
 */
public class Endpoint {
    private QName   mServiceName;
    private String  mEndpointName;

    Endpoint(QName serviceName, String endpointName)
    {
        mServiceName    = serviceName;
        mEndpointName   = endpointName;
    }

    public QName getServiceName() {
        return mServiceName;
    }

    public String getEndpointName() {
        return mEndpointName;
    }

    public boolean equals(Object obj)
    {
        boolean isEqual = false;

        if (obj instanceof Endpoint &&
            ((Endpoint)obj).mServiceName.equals(mServiceName) &&
            ((Endpoint)obj).mEndpointName.equals(mEndpointName))
        {
            isEqual = true;
        }

        return isEqual;
    }

    public String toString()
    {
        StringBuilder   sb = new StringBuilder();

        sb.append("        ServiceName: " + mServiceName);
        sb.append("\n        EndpointName: " + mEndpointName);
        sb.append("\n");
        return (sb.toString());

    }
    public int hashCode()
    {
       return (mServiceName.hashCode() ^ mEndpointName.hashCode());
    }
}
