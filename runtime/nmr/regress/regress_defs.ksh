#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for nmr regression tests.

####
# Source the esb definitions
####
. $SRCROOT/antbld/regress/esb_common_defs.ksh

export REGRESS_CLASSPATH
REGRESS_CLASSPATH="$JV_SVC_CLASSES\
${JBI_PS}$JV_SVC_TEST_CLASSES\
${JBI_PS}$JV_JBI_HOME/lib/jbi_rt.jar\
${JBI_PS}$AS_INSTALL/lib/SUNWjdmk/5.1/lib/jdmkrt.jar\
${JBI_PS}$AS_INSTALL/lib/jmxremote_optional.jar\
${JBI_PS}$AS_INSTALL/lib/appserv-admin.jar\
${JBI_PS}$AS_INSTALL/lib/appserv-rt.jar\
${JBI_PS}$AS_INSTALL/lib/javaee.jar\
"

# Calls ant with jbi.task.fail.on.error=false so that we can track negative
# test case output in the .out file
export JBI_ANT_NEG
JBI_ANT_NEG="$JBI_ANT -Djbi.task.fail.on.error=false"

#
# The target could be passed to this script. 
# jbiadmin00116 runs this test with target=cluster1
#
if [ "$1" != "" ]; then
    JBI_ANT_TARGET="$JBI_ANT -Djbi.target=$1" 
    JBI_ANT_NEG_TARGET="$JBI_ANT -Djbi.target=$1 -Djbi.task.fail.on.error=false"
else
    JBI_ANT_TARGET="$JBI_ANT" 
    JBI_ANT_NEG_TARGET="$JBI_ANT_NEG"
fi

