#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)config.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "config.ksh- Configure JBITest for NMR Test Binding/Engine"

. ./regress_defs.ksh

#$AS8BASE/imq/bin/imqcmd -b localhost:$JMS_ADMIN_PORT -u admin -passfile imq.password create dst -t t -n SunJbiProxyBindingConnection
#$AS8BASE/imq/bin/imqcmd -b localhost:$JMS_ADMIN_PORT -u admin -passfile imq.password create dst -t q -n JBITest_at_LIFEBOOK_minus_98440_dot_0 
#$AS8BASE/imq/bin/imqcmd -b localhost:$JMS_ADMIN_PORT -u admin -passfile imq.password create dst -t q -n ESBTest_at_LIFEBOOK_minus_98440_dot_0 


asadmin create-jms-resource -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --host localhost --port $ASADMIN_PORT --restype javax.jms.ConnectionFactory --enabled=true --property AddressList=\"localhost\:$JMS_ADMIN_PORT\" jms/NMRTestQueueFactory 

asadmin create-jms-resource -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --host localhost --port $ASADMIN_PORT --restype javax.jms.Queue --enabled=true --property Name=QUEUE_NMR_TEST_BINDING2 jms/QUEUE_NMR_TEST_BINDING2

asadmin create-jms-resource -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --host localhost --port $ASADMIN_PORT --restype javax.jms.Queue --enabled=true --property Name=QUEUE_NMR_TEST_ENGINE2 jms/QUEUE_NMR_TEST_ENGINE2


echo Completed Configure for JMS
