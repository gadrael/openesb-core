#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)nms00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "nms00001 : NMS performance test"

####
# This tests component update in a cluster
####
. ./regress_defs.ksh

echo Install the TestBinding component
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SRCROOT/runtime/nmr/test-components/binding/install/bld/testbinding-1.1.jar

echo Install tbe TestEngine component
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SRCROOT/runtime/nmr/test-components/engine/install/bld/testengine-1.1.jar

echo Install tbe Observer component
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SRCROOT/runtime/nmr/test-components/observer/install/bld/observer-1.1.jar

echo Start the TestEngine component
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestEngine

echo Deploy SA to TestEngine component
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SRCROOT/runtime/nmr/test-components/engine/config/sa.jar

echo Start SA in TestEngine component
asadmin start-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestEngine_SA

echo Start the TestBinding component
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestBinding

echo Test running...
sleep 5

echo Start the Observer component
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Observer

echo Observer running...
sleep 5

echo Stop the Observer component
asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Observer

sleep 5

cat $JV_AS8BASE/domains/CAS/jbi/components/Observer/install_root/workspace/observer.stats

sleep 30

echo Stop the TestBinding
asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestBinding

echo Stop SA from TestEngine component
asadmin shut-down-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestEngine_SA

echo Undeploy SA from TestEngine component
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestEngine_SA

echo Stop the TestEngine
asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestEngine

sleep 5

cat $JV_AS8BASE/domains/CAS/jbi/components/TestBinding/install_root/workspace/binding.stats
cat $JV_AS8BASE/domains/CAS/jbi/components/TestEngine/install_root/workspace/engine.stats

echo Shutdown the Observer 
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Observer

echo Shutdown the TestEngine 
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestEngine

echo Shutdown the TestBinding 
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestBinding

echo Uninstall the Observer
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Observer

echo Uninstall the TestEngine
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestEngine

echo Uninstall the TestBinding
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT TestBinding
