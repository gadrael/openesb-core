#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)security00003.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
. ./regress_defs.ksh

OUTPUT_FILE=${SEC_REGRESS_DIR}/../bld/security0003.response.out

###############################################################################
#                                  Test Run                                   #
###############################################################################
# Test Endpoint1
ant -emacs -q -Dhost="${HOSTNAME}" -Dport="${JBITest_HTTP_LISTENER_2}" -DtestOutputFile=${OUTPUT_FILE} -Dendpoint="endpoint2" -f ${SEC_REGRESS_DIR}/scripts/run-client.ant run_test

# Get the Response in the output file
cat ${OUTPUT_FILE}

# Delete the response
ant -emacs -q -DfileToDelete=${OUTPUT_FILE} -f  ${SEC_REGRESS_DIR}/scripts/util.ant delete
