#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)security00099.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
. ./regress_defs.ksh

###############################################################################
#                                  Test Tear Down                             #
###############################################################################

# Delete the War
ant -emacs -q -DfileToDelete=${SEC_BLD_DIR}/${TEST_SVLT_WAR} -f ${SEC_REGRESS_DIR}/scripts/util.ant delete

# Delete the Exported Certificate files
ant -emacs -q -DfileToDelete=${SEC_BLD_REGRESS_DIR}/s1as.cert -f  ${SEC_REGRESS_DIR}/scripts/util.ant delete
ant -emacs -q -DfileToDelete=${SEC_BLD_REGRESS_DIR}/cacert.cert -f  ${SEC_REGRESS_DIR}/scripts/util.ant delete

# Delete the bld/config folder
ant -emacs -q -f ${SEC_REGRESS_DIR}/scripts/copy-config-data.ant delete_config_folder

# Delete imported certificate from the client truststore
keytool -delete -keystore $SEC_BLD_REGRESS_DIR/keystores/client-truststore.jks -storepass changeit -alias s1as2

# Delete imported certificate from the application server truststore
if [ ${IS_EE_INSTALL} = "1" ]; then

    certutil -D -n certificate-authority -d $JV_JBI_DOMAIN_ROOT/config/ 

else

    keytool -delete -keystore $JV_JBI_DOMAIN_ROOT/config/cacerts.jks -storepass changeit -alias certificate-authority
fi


# Undeploy the Servlet
echo Undeploying the TestSecurityServlet.
asadmin undeploy -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port ${ASADMIN_PORT} TestSecurityServlet
if [ $? -eq 0 ]; then
    echo Successfully undeployed the TestSecurityServlet.
else
    echo Failed to undeploy TestSecurityServlet
fi


# Restore the domains server.policy
ant -emacs -q -DJBI_DOMAIN_ROOT=${JV_JBI_DOMAIN_ROOT} -DSERVER_POLICY_APPEND=${SERVER_POLICY_APPEND} -DSERVER_POLICY_BAK=${SERVER_POLICY_BAK} -f ${SEC_REGRESS_DIR}/scripts/update-appserver.ant restore_server_policy


# Restore the domains login.conf
ant -emacs -q -DJBI_DOMAIN_ROOT=${JV_JBI_DOMAIN_ROOT} -DLOGIN_CONF_APPEND=${LOGIN_CONF_APPEND} -DLOGIN_CONF_BAK=${LOGIN_CONF_BAK} -f ${SEC_REGRESS_DIR}/scripts/update-login-conf.ant restore_login_conf

# Delete the server.policy backup file 
ant -emacs -DfileToDelete=${SEC_BLD_DIR}/${SERVER_POLICY_APPEND} -q -f ${SEC_REGRESS_DIR}/scripts/util.ant delete

# Delete the server.policy append file
ant -emacs -DfileToDelete=${SEC_BLD_DIR}/${SERVER_POLICY_BAK} -q -f ${SEC_REGRESS_DIR}/scripts/util.ant delete

# Stop the application server domain
asadmin stop-domain --domaindir "$JV_JBI_DOMAIN_DIR" "$JBI_DOMAIN_NAME"

# Remove the User data file
rm -f ${JBI_DOMAIN_ROOT}/config/${USER_FILE}

# Reset to a known state by removing previous installations and deployments:
rm -rf $JBI_DOMAIN_ROOT/jbi/engines
rm -rf $JBI_DOMAIN_ROOT/jbi/bindings
rm -rf $JBI_DOMAIN_ROOT/jbi/sharedlibraries
rm -rf $JBI_DOMAIN_ROOT/jbi/schemaorg_apache_xmlbeans.system
mkdir $JBI_DOMAIN_ROOT/jbi/engines
mkdir $JBI_DOMAIN_ROOT/jbi/bindings
mkdir $JBI_DOMAIN_ROOT/jbi/sharedlibraries
mkdir $JBI_DOMAIN_ROOT/jbi/schemaorg_apache_xmlbeans.system

# Remove persisted component meta-data:
rm -f $JBI_DOMAIN_ROOT/jbi/repository/ComponentData.xml
