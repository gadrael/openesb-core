/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  EndpointImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 16, 2004, 3:21 PM
 */

package com.sun.jbi.internal.security.https.jregress.server;

import javax.xml.namespace.QName;
/**
 *
 * @author Sun Microsystems, Inc.
 */
public class EndpointImpl
    implements com.sun.jbi.binding.security.Endpoint
{
    
    /**
     * Holds value of property mEndpointName.
     */
    private String mEndpointName;
    
    /**
     * Holds value of property mSecConfigFileName.
     */
    private String mSecConfigFileName;
    
    /**
     * Service QName.
     */
    private QName mSvcQName;
    
    /**
     * Component Id.
     */
    private String mComponentId;

    
    
    
    /** Creates a new instance of EndpointImpl */
    public EndpointImpl (String epName, QName svcQname, 
        String componentId, String secConfigFile   )
    {
        mEndpointName = epName;
        mSvcQName = svcQname;
        mComponentId = componentId;
        mSecConfigFileName = secConfigFile;
        
        
    }
    
    public String getComponentId ()
    {
        return mComponentId;
    }
    
    public String getEndpointName ()
    {
        return mEndpointName;
    }
    
    public java.util.Iterator getOperationNames ()
    {
        // -- return an Iterator over an empty list
        return ( new java.util.Vector().iterator());
    }
    
    public String getSecurityConfigFileName ()
    {
        return mSecConfigFileName;
    }
    
    public QName getServiceName ()
    {
        return mSvcQName;
    }
    
    public javax.jbi.messaging.MessageExchange.Role getRole()
    {
        return javax.jbi.messaging.MessageExchange.Role.CONSUMER;
    }
    
}
