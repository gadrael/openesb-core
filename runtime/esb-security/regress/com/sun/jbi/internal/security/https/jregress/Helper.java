/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Helper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Helper.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 16, 2004, 3:23 PM
 */

package com.sun.jbi.internal.security.https.jregress;

import java.net.HttpURLConnection;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class Helper
{
    
    /**
     * Get the response and print it.
     */
    public static void getResponse(HttpURLConnection connection, Writer writer)
        throws Exception
    {
       
        InputStream istr = null;
        Reader reader = null;
        try
        {
            istr = connection.getInputStream();
            reader = new InputStreamReader( istr );
        }
        catch ( java.io.IOException ioex)
        {
            StringBuffer buf = new StringBuffer();
            buf.append("HTTP Error Code: " + connection.getResponseCode () + "\n");
            buf.append("HTTP Error Message: " + connection.getResponseMessage () +"\n" );
            reader = new StringReader(buf.toString().trim());
        }
        
        if (reader != null)
        {
            final int len = 256;
            char cbuf[] = new char[len]; 

            int bytesRead = reader.read(cbuf, 0, len);

            BufferedWriter bufWriter = new BufferedWriter(writer);
            while (bytesRead != -1 ) 
            {
                bufWriter.write(cbuf, 0, bytesRead);

                bytesRead = reader.read(cbuf , 0, len);

            };
            bufWriter.flush();
            bufWriter.close();

            reader.close();
        } 
    }
    
}
