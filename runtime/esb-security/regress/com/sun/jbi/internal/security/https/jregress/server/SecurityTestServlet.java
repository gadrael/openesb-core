/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityTestServlet.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.https.jregress.server;

import com.sun.jbi.binding.security.Endpoint;
import com.sun.jbi.binding.security.DeploymentListener;
import com.sun.jbi.binding.security.HttpSecurityHandler;
import com.sun.jbi.binding.security.HttpErrorResponseException;
import com.sun.jbi.binding.security.SecurityHandler;
import com.sun.jbi.internal.security.config.SecurityConfiguration;
import com.sun.jbi.internal.security.ExtendedSecurityService;
import com.sun.jbi.internal.security.SampleBindingContext;
import com.sun.jbi.internal.security.https.jregress.SecurityConfigImpl;
import com.sun.jbi.internal.security.https.jregress.Helper;
import com.sun.jbi.internal.security.Util;

import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;



/**
 *
 * @author Sun Microsystems, Inc.
 * @version
 */
public class SecurityTestServlet extends HttpServlet
{
    /** Some Constants */
    private static String EP_PREFIX = "endpoint";
    private static String SVC_NAME  = "testService";
    private static String SVC_TNS   = "http://www.sun.com";
    
    /**
     * Binding Id
     */
    private String mBindingId;
    
    /**
     * Binding Name.
     */
    private String mBindingName;
    
    /**
     * Security Handler 
     */
    private SecurityHandler mSecHandler;
    
    /**
     * The HttpSecurityHandler
     */
    private HttpSecurityHandler mHttpSecHandler;
    
    /**
     * The Security Service.
     */
    private ExtendedSecurityService mSecSvc;
    
    /**
     * Dummy Deployment Registry
     */
    private HashMap mEndpoints;
    
    /** Initializes the servlet.
     */
    public void init (ServletConfig config) throws ServletException
    {
        super.init (config);
        System.out.println("TestSecurity Servlet initialized.");
        mEndpoints = new HashMap();
        try
        {
            // -- Create Security Handler
            System.out.println("Creating the Security Handler.");
            createSecurityHandler(config);
            mHttpSecHandler = mSecHandler.getHttpSecurityHandler();
            System.out.println("Security Handler created.");
            
            // -- Simulate the deployments
            addDeployments(config);
        } 
        catch ( Exception ex )
        {
            // -- ex.printStackTrace();
            throw new ServletException(ex);
        }
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy ()
    {
        // -- Remove the deployments
        removeDeployments();
        
        // -- Remove Security Handler
        System.out.println("Removing the Security Handler.");
        removeSecurityHandler();
        System.out.println("TestSecurity Servlet destroyed.");
        
    }
    
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * The request URL will be of the form http(s)://host:port/security/endpointX[/testX.]
     * endpointX identifies the endpoint to whom the request is directed.
     * 
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest (HttpServletRequest request, 
        HttpServletResponse response)
    throws ServletException, IOException
    {
        try{
            // -- Dispatch the requests to various request handlers.
            Endpoint ep = getEndpointFromRequest(request);
            try
            {
                mHttpSecHandler.authenticateSenderRequest (request, ep, null);
                process(request, response, ep);
                sendSuccessResponse(ep, response);
                
            }
            catch (HttpErrorResponseException httpEx)
            {
                response.sendError(httpEx.getErrorCode(), httpEx.getErrorMessage());
            }
        } catch (Exception ex)
        {
            throw new ServletException(ex);
        }
    }
    
    /**
     *
     * @param ep is the Endpoint to send the response to.
     * @param response is the HttpServletResponse
     * @throws IOException on IO errors.
     */
    private void sendSuccessResponse(Endpoint ep, HttpServletResponse response)
        throws IOException
    {
        response.setContentType ("text/html");
        PrintWriter out = response.getWriter ();
        /* TODO output your page here*/
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Test for endpoint " + ep.getEndpointName ()+
            " Succeeded</title>");
        out.println("</head>");
        out.println("<body>");

        out.println("</body>");
        out.println("</html>");

        out.close ();  
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        processRequest (request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        processRequest (request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo ()
    {
        return "Test Servlet to test TLS";
    }
    
    /**
     * Create a Security Handler
     *
     * @throws Exception
     */
    private void createSecurityHandler(ServletConfig config)
        throws Exception
    {
        if ( config.getInitParameter("userfile") == null )
            throw new Exception("Init parameter userfile missing from ServletConfig");
         if ( config.getInitParameter("userfile2") == null )
            throw new Exception("Init parameter userfile missing from ServletConfig");
        if ( config.getInitParameter("keystorebase") == null )
            throw new Exception("Init parameter keystorebase missing from ServletConfig");
        
        
        
        SecurityConfiguration installConfig = 
            SecurityConfigImpl.getTestSecurityConfiguration(
                config.getInitParameter("userfile"), 
                config.getInitParameter("userfile2"), 
                config.getInitParameter("keystorebase"),
                Util.getStringTranslator("com.sun.jbi.internal.security"));
        
        mBindingId = 
            ( config.getInitParameter("bindingid") == null 
                ? "def-123" : config.getInitParameter("bindingid") );
                
        mBindingName =
            ( config.getInitParameter("bindingname") == null 
                ? "def-binding" : config.getInitParameter("bindingname") );
        
        
        // -- Create the Installtime Setup
        mSecSvc = new ExtendedSecurityService(installConfig);
        // -- Initialize the Service here, pass the ctx to it.
        com.sun.jbi.component.ComponentContext bndCtx = new SampleBindingContext(
            mBindingId, mSecSvc);
        mSecHandler = bndCtx.getSecurityHandler();
    }
    
    /**
     * Remove the Security Handler
     *
     * @throws Exception
     */
     private void removeSecurityHandler()
     {
         mSecHandler = null;
         mSecSvc.removeSecurityHandler(mBindingId);
     }
     
     /**
      * Add deployments, making this as dynamic as possible.
      *
      * @param config is the ServletConfig from where one could get the endpoint info
      */
     private void addDeployments(ServletConfig config)
        throws Exception
     { 
         createTestEndpoints(config);
         DeploymentListener listener = mSecHandler.getDeploymentListener();
         
         Iterator itr = mEndpoints.keySet().iterator();    
         while ( itr.hasNext() )
         {
             listener.addDeployment( (Endpoint) mEndpoints.get( (String) itr.next()));
         }
     }
     
     
     /**
      * Create Test Endpoints from the ServletConfig
      *
      * @param config is the ServletConfig from where one could get the endpoint info
      */
     private void createTestEndpoints(ServletConfig config)
        throws Exception
     {
         // -- Check the init parameters for all endpoint* parameters, create Endpoints
         // -- from these.
         
         Enumeration params = config.getInitParameterNames();
         
         while ( params.hasMoreElements() )
         {
             String param = (String) params.nextElement();
             if ( param.startsWith(EP_PREFIX))
             {
                 // -- This is an endpoint
                 String secFileName = config.getInitParameter(param);
                 if ( !(new File(secFileName).exists()) )
                 {
                     throw new Exception( "The file : " + secFileName 
                        + " does not exist.");
                 }
                 System.out.println("Creating Endpoint " + param +
                    " Svc " + (new javax.xml.namespace.QName(SVC_TNS, SVC_NAME)).toString()
                    + " id " + mBindingId + " secFileName " + secFileName);
                 mEndpoints.put( param, new EndpointImpl(                   
                     param,
                     new javax.xml.namespace.QName(SVC_TNS, SVC_NAME),
                     mBindingId,
                     secFileName
                     ) );
             }
         }
     }
     
     /**
      * Remove all the Endpoint deployments
      *
      */
     private void removeDeployments()
     {
         DeploymentListener listener = mSecHandler.getDeploymentListener();
         Iterator itr = mEndpoints.keySet().iterator();    
         while ( itr.hasNext() )
         {
             listener.removeDeployment( (Endpoint) mEndpoints.get( (String) itr.next()));
         }
     }
    
     /**
      * Get the Endpoint from the Request
      *
      */
     private Endpoint getEndpointFromRequest(HttpServletRequest request)
        throws Exception
     {
         String serviceURL = request.getRequestURL().toString();
         System.out.println("Request URL : " + serviceURL );
         
         int startIndex = serviceURL.indexOf(EP_PREFIX);
         int endIndex   = serviceURL.indexOf("/", startIndex);
         
         if ( startIndex != -1 )
         {
            
            String endpoint = ( endIndex == -1 ) 
                ? serviceURL.substring(startIndex) 
                : serviceURL.substring(startIndex, endIndex);
            System.out.println("Request is for endpoint : " + endpoint );
            return (Endpoint) mEndpoints.get(endpoint);
         }
         else
         {
             throw new Exception("Could not identify the Endpoint the request is for.");
         }
     }
     
     /**
      * This is hook to test the outbound tls functionality.
      *
      * @param request is the HttpServletRequest
      * @param ep is the Endpoint
      */
     private void process(HttpServletRequest request, HttpServletResponse response,
        Endpoint ep)
        throws Exception
     {
         if ( ep.getEndpointName().equals("endpoint4"))
         {
             // -- Simulate a outbound connection.
             String serviceURL = request.getRequestURL().toString();
             System.out.println("Request URL : " + serviceURL );
         
             int insertPos = serviceURL.indexOf(EP_PREFIX);
             
             if ( mEndpoints.get("endpoint3") != null )
             {
                String connectURL = 
                    serviceURL.substring(0, insertPos).concat("endpoint3");
                // -- Connect to a secure endpoint i.e. endpoint3
                System.out.println("Connecting to : " + connectURL);
                HttpURLConnection conxn = (HttpURLConnection) 
                    mHttpSecHandler.createSecureClientConnection(
                        new java.net.URL(connectURL), 
                        ep);
                Helper.getResponse(conxn, response.getWriter());

             }
             
             
         }
     }
}
