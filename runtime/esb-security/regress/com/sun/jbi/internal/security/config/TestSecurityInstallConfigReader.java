/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSecurityInstallConfigReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  TestSecurityInstallConfigReader.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 17, 2004, 2:35 PM
 */

package com.sun.jbi.internal.security.config;

import com.sun.jbi.internal.security.Util;
import java.io.File;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class TestSecurityInstallConfigReader
    extends junit.framework.TestCase
{
    private String mConfig;
    private String mConfigSchemaDir;
        
    /** Creates a new instance of TestSecurityInstallConfigReader */
    public TestSecurityInstallConfigReader(String testname)
    {
        super(testname);
        String srcroot = System.getProperty("junit.srcroot");
        String security = "/runtime/esb-security";        // open-esb build

        java.io.File f = new java.io.File(srcroot + security);
        if (! f.exists())
        {
            security = "/shasta/security";       // mainline/whitney build
        }

        mConfig = srcroot + security + 
            "/regress/installconfig/InstallConfig1.xml";
        mConfigSchemaDir = srcroot + security + "/schema";
    }
    
    /**
     *
     */
    public void testSecurityInstallConfigReader()
        throws Exception
    {
        SecurityInstallConfigReader reader = 
            new SecurityInstallConfigReader(Util.getStringTranslator(
                "com.sun.jbi.internal.security"), mConfigSchemaDir);
        reader.setValidating(true);
        SecurityInstallConfig secCfg = (SecurityInstallConfig) 
            reader.read(new File(mConfig));
      
        assertTrue("File".equals(secCfg.getDefaultUserDomainName()));
        assertTrue("SimpleManager".equals(secCfg.getDefaultKeyStoreManagerName()));

                    
        // -- Check TLS parameters
        assertTrue(secCfg.getSSLServerRequireClientAuth());
        assertFalse(secCfg.getSSLClientUseDefault());
        assertEquals("SSLv3", secCfg.getSSLClientProtocol());
        assertEquals("xws-security-client", secCfg.getSSLClientAlias());
        
        assertNotNull(secCfg.getKeyStoreContexts());
        assertNotNull(secCfg.getTransportSecurityContext());
        assertNotNull(secCfg.getUserDomainContexts());
        
        SecurityInstallConfigWriter.write(secCfg, System.out);
        
    }
    
    
    
}
