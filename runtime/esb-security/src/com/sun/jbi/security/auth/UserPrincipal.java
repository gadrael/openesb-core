/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UserPrincipal.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.security.auth; 

import java.security.Principal; 
/** 
 * A Simple User Principal.
 *
 * @author Sun Microsystems, Inc.
 */
public class UserPrincipal 
    implements Principal, java.io.Serializable 
{ 
    /** 
     * The Principal name.
     */ 
    private String mName;
    
    /** 
     * Create a UserPrincipal with the user name.
     * 
     * @param name the username for this user. 
     */ 
    public UserPrincipal (String name) 
    { 
        mName = name; 
    } 
    
    /** 
     * 
     * @return the username of this Principal 
     */ 
    public String getName() 
    { 
        return mName; 
    } 
    
    /** 
     * 
     * @return a string representation of this UserPrincipal. 
     */ 
    public String toString() 
    { 
        return (this.getClass().getName() + ":" + mName); 
    } 
    
    /** 
     * Compares the specified Object with this UserPrincipal 
     * for equality. Returns true if the given object is also a 
     * UserPrincipal instance and the two UserPrincipals have 
     * the same username. 
     * @param obj Object to be compared for equality with this 
     * UserPrincipal. 
     * @return true if the specified Object is equal equal to this 
     * UserPrincipal. 
     */
    public boolean equals(Object obj) 
    { 
        if (obj == null) 
        {
            return false;
        }
        if ( !(obj instanceof UserPrincipal) ) 
        {
            return false; 
        }
        
        return ( getName().equals(( (UserPrincipal) obj).getName()));
    } 
    
    /** 
     * 
     * @return a hash code for this UserPrincipal. 
     */ 
    public int hashCode() 
    { 
        return mName.hashCode(); 
    } 
} 
