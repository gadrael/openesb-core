/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileLoginModule.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.security.auth.module; 

import com.sun.jbi.internal.security.util.Loader;
import com.sun.jbi.security.auth.UserPrincipal;

import java.io.File;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;

import java.security.Principal;

import javax.security.auth.Subject; 
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException; 
import javax.security.auth.spi.LoginModule; 

/** 
 * This is a simple login module which autheticates
 * using a File. The file containing the username and password 
 * is obtained from the options configured in the configuration file.
 * 
 * This Login module understands the following options: mDebug - 
 * if set to true mDebug messages are printed. file - is the pathname 
 * to the file which contains the user data.
 *
 * @author Sun Microsystems, Inc.
 */ 
public class FileLoginModule implements LoginModule 
{ 
    /**
     * Debug option.
     */
    private static final String DEBUG = "debug";
    
    /**
     * File Name option.
     */
    private static final String FILE  = "file";
    
    /**
     * Attribute: password.
     */    
    public static final String ATTR_PASSWORD = "password";
    
    /**
     * Attribute: name.
     */    
    public static final String ATTR_NAME = "name";
    
    /**
     * User Element.
     */    
    public static final String ELEMENT_USER = "User";
    
    
    
    /**
     * Subject to be updated.
     */
    private Subject mSubject; 
    
    /**
     * Principal to be added to Subject.
     */
    private Principal mUserPrincipal;
    
    /**
     * Handler to get username / passwd info. 
     */
    private CallbackHandler mCallbackHandler; 
    
    /**
     * Options passed from Config file.
     */
    private Map mOptions; 
    
    /**
     * Indicates that Initialization was successful.
     */
    private boolean mInitSucceeded;
    
    /**
     * Debug instance var.
     */
    private boolean mDebug = false; 
    
    /**
     * Auth status.
     */
    private boolean mLoginSucceeded = false; 
    
    /**
     * Commit Status.
     */
    private boolean mCommitSucceeded = false; 
    
   /**
    * Authenticated User.
    */
    private String mUsername; 
    
    /**
     * Authenticated Users Passwd.
     */
    private char[] mPassword; 
    
    /**
     * String Translator.
     */
    private StringTranslator mTranslator;
    
    /**
     * The Logger.
     */
    private Logger mLogger;
    
    /**
     * The User Map.
     */
    private HashMap mUsers;
    
    /** 
     * Initialize this LoginModule. 
     *
     * @param subject the Subject to be authenticated. 
     * @param callbackHandler a CallbackHandler for getting the username and password 
     * information.
     * @param sharedState shared LoginModule state, this module does not get or set 
     * information in the Shared State.
     * @param options options specified in the login Configuration for this 
     * particular LoginModule. 
     */ 
    public void initialize(Subject subject, CallbackHandler callbackHandler, 
        Map sharedState, Map options) 
    { 
        
        mTranslator = new StringTranslator(this.getClass().getPackage().getName(),
            this.getClass().getClassLoader());
        mLogger = Logger.getLogger(this.getClass().getPackage().getName());
        
        // -- Save these for later
        mSubject = ( (subject == null) ? new Subject() : subject );
        mCallbackHandler = callbackHandler; 
        mOptions = options; 
        
        // -- initialize any configured options 
        mDebug = "true".equalsIgnoreCase( (String) options.get(DEBUG)); 
        String filename  = (String) options.get(FILE);
        
        if ( filename == null )
        {
            mLogger.severe( mTranslator.getString(
                LocalStringConstants.AUTH_MISSING_CONFIG_OPTION, FILE));
            mInitSucceeded = false;
        }
        
        File file = new File(filename);
        
        if ( !file.exists() || !file.canRead() )
        {
            mLogger.severe( mTranslator.getString(
                LocalStringConstants.AUTH_MISSING_INVALID_FILE, filename));
            mInitSucceeded = false;
        } 
        
        try
        {
            loadUsers(file);
        }
        catch (java.io.IOException ioex)
        {
            mInitSucceeded = false;
        }
            
        mInitSucceeded = true;
    } 
    
    /** 
     * Authenticate the user by getting  a user name and password.  
     * @return true if authentication succeeded, false otherwise.  
     * @exception LoginException if this LoginModule is unable to perform the 
     * authentication. 
     */ 
    public boolean login() 
        throws LoginException 
    { 
        checkState();
        authenticate();
        mLoginSucceeded = true; 
        return true; 
    } 
    
    /** 
     * This is phase two of the two phase authentication and this is called
     * if the LoginContext's overall authentication succeeded.
     * 
     * If this LoginModules authentication was successful, update the Subject
     * else clear all state.
     * 
     * 
     * @exception LoginException if the commit fails. 
     * 
     * @return true if this LoginModule's own login and commit 
     * attempts succeeded, or false otherwise. 
     */ 
    public boolean commit() 
        throws LoginException 
    { 
        if (!mLoginSucceeded) 
        { 
            cleanState();
            return false; 
        } 
        else 
        { 
            // -- Update the Subject
            mUserPrincipal = new UserPrincipal(mUsername);
            if (!mSubject.getPrincipals().contains(mUserPrincipal)) 
            {
                mSubject.getPrincipals().add(mUserPrincipal); 

                if (mDebug) 
                { 
                    mLogger.info(mTranslator.getString(
                        LocalStringConstants.AUTH_LM_USER_AUTH_SUCCESS,
                        mUsername, this.getClass().getName())); 
                } 
            }

            cleanState();
            mCommitSucceeded = true; 
            return true; 
        } 
    } 
 
    /** 
     * This is phase two of the two phase authentication and this is called
     * if the LoginContext's overall authentication failed. 
     * 
     * @exception LoginException if the abort fails. 
     * 
     * @return false if this LoginModule's own login and/or commit attempts 
     * failed, and true otherwise. 
     */ 
    public boolean abort() throws LoginException 
    { 
        if (!mLoginSucceeded) 
        { 
            if (mDebug) 
            { 
                mLogger.info(mTranslator.getString(
                    LocalStringConstants.AUTH_LM_USER_AUTH_FAILURE,
                        mUsername, this.getClass().getName())); 
            } 
            cleanState();
            return false; 
        } 
        // 
        else if ( mLoginSucceeded && !mCommitSucceeded )
        { 
            // -- 2nd phase of authentication failed just clean state
            mLoginSucceeded = false; 
            cleanState();
            mUserPrincipal = null; 
            return false;
        } 
        else 
        { 
            // -- Our authentication succeeded but overall authentication failed
            logout();
        } 
        return true; 
    }
 
    /** 
     * Logout the user, this method removes the Principal that was 
     * added by the commit method. 
     *
     * @throws LoginException if the logout fails. 
     * 
     * @return true in all cases since this LoginModule 
     * should not be ignored. 
     */ 
    public boolean logout() throws LoginException 
    { 
        mSubject.getPrincipals().remove(mUserPrincipal); 
        mUserPrincipal = null;
        mLoginSucceeded = false;
        mCommitSucceeded = false; 
        cleanState();
        return true; 
    } 
    
    /**
     * Loads the User information from the configured file and
     * updates the local state. 
     *
     * @param file - the  name of the file to read the information from
     * @throws java.io.IOException on io errors
     */
    private void loadUsers(File file)
        throws java.io.IOException
    {
        org.w3c.dom.Document dom = null;
        java.io.FileInputStream fistr  = null;
        mUsers = new HashMap();
        try
        {
            Loader loader = new Loader();
            fistr = new java.io.FileInputStream(file);
            dom = loader.load(fistr, false, null, null);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mLogger.severe(mTranslator.getString(
                    LocalStringConstants.AUTH_ERR_LOAD_USERS_FAILED,
                        ex.toString()));
            if ( fistr != null )
            {
                fistr.close();
            }
            mInitSucceeded = false;
        }
        
        org.w3c.dom.NodeList list = dom.getDocumentElement().
            getElementsByTagName(ELEMENT_USER);
        
        for ( int i = 0; i < list.getLength(); i++ )
        {
            try
            {
                org.w3c.dom.Element user = (org.w3c.dom.Element) list.item(i);
                String name = user.getAttribute(ATTR_NAME).trim();
                String pwd = user.getAttribute(ATTR_PASSWORD).trim();
                mUsers.put(name, pwd); 
            }
            catch (Exception ex)
            {
                mLogger.severe(mTranslator.getString(
                        LocalStringConstants.AUTH_ERR_LOAD_USERS_FAILED,
                            ex.toString()));
            }
        }
        fistr.close();
    }
    
        /**
     *
     * @throws LoginException if the state of the LoginModule is not valid.
     */
    private void checkState()
        throws LoginException
    {
        if ( !mInitSucceeded )
        {
            throw new LoginException(mTranslator.getString(
                LocalStringConstants.AUTH_LM_INIT_FAILED, this.getClass().getName()));
        }
        
        if (mCallbackHandler == null) 
        {
            throw new LoginException(mTranslator.getString(
                LocalStringConstants.AUTH_LM_MISSING_CBHNDLR, this.getClass().getName()));
        }
    }
    
    /**
     * Get the User information and authenticate the user.
     * 
     * @throws LoginException if authentication fails
     */
    private void authenticate()
        throws LoginException
    {
        Callback[] callbacks = new Callback[]{
            new NameCallback(mTranslator.getString(
                LocalStringConstants.AUTH_USER_PROMPT)), 
            new PasswordCallback(mTranslator.getString(
                LocalStringConstants.AUTH_PASSWD_PROMPT), false), }; 
            
        try 
        { 
            mCallbackHandler.handle(callbacks); 
            mUsername = ( (NameCallback) callbacks[0]).getName(); 
            char[] tmpPwd = ( (PasswordCallback) callbacks[1]).getPassword(); 
            
            if (tmpPwd == null) 
            { 
                // -- If no password was specified, use an empty password
                tmpPwd = new char[0]; 
            } 
            
            mPassword = new char[tmpPwd.length];
            System.arraycopy(tmpPwd, 0, mPassword, 0, tmpPwd.length);
            //((PasswordCallback)callbacks[1]).clearPassword(); 
            
            if ( !mUsers.containsKey(mUsername))
            {
                throw new LoginException(mTranslator.getString(
                    LocalStringConstants.AUTH_USER_AUTH_FAILED, mUsername));
            }
            
            String password = (String) mUsers.get(mUsername);
            if ( !(new String(mPassword).equals(password)) )
            {
                if (mDebug) 
                { 
                    mLogger.info(mTranslator.getString(
                        LocalStringConstants.AUTH_LM_USER_AUTH_FAILURE,
                            mUsername, this.getClass().getName())); 
                } 
                throw new LoginException(mTranslator.getString(
                    LocalStringConstants.AUTH_USER_AUTH_FAILED, mUsername));
            } 
        } 
        catch (java.io.IOException ioe) 
        { 
            throw new LoginException(ioe.toString()); 
        } 
        catch (UnsupportedCallbackException uce) 
        { 
            throw new LoginException(mTranslator.getString(
                LocalStringConstants.AUTH_LM_UNSUP_CB, 
                    uce.getCallback().toString()));                
        } 
        
    }
    
    /**
     *
     * Clean the state.
     */
    private void cleanState()
    {
        mUsername = null; 
        if ( mPassword != null )
        {
            for (int i = 0; i < mPassword.length; i++) 
            {
                mPassword[i] = ' '; 
            }
            mPassword = null; 
        }
    }
} 
