/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageSecPolicy.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  MessageSecPolicy.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on March 18, 2005, 12:05 PM
 */

package com.sun.jbi.internal.security.config;

import com.sun.enterprise.security.jauth.AuthPolicy;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public interface MessageSecPolicy
{
    /**
     * Get the RequestPolicy. The Request Policy is of the Type AuthPolicy.
     * This class is currently defined in the com.sun.enterprise.security.jauth 
     * package, this would be standardized in JSR 196.
     *
     * If the Endpoint is a Consumer (Server) Endpoint the Request Policy
     * applies to incoming requests, if its a Provider (Client) Endpoint 
     * the policy applies to the outgoing request.
     *
     * @return the request AuthPolicy
     */
    AuthPolicy getRequestPolicy();
   
    
    /**
     * Get the ResponsePolicy. The Request Policy is of the Type AuthPolicy.
     * This class is currently defined in the com.sun.enterprise.security.jauth 
     * package, this would be standardized in JSR 196.
     *
     * If the Endpoint is a Consumer (Server) Endpoint the Response Policy
     * applies to outgoing requests, if its a Provider (Client) Endpoint 
     * the policy applies to the incoming response.
     *
     * @return the ResponsePolicy.
     */
    AuthPolicy getResponsePolicy();
    
    /**
     * Get the Name of the Provider. If a Provider is not specified in the deployment
     * configuration, an empty String is returned.
     *
     * @return the name of the Message Security Provider
     */
    String getMessageProviderId();
    
    /**
     * Get the Set of Operations to which this MessageSecPolicy applies.
     *
     * @return the Set of Operations to which this MessageSecPolicy applies
     */
    java.util.Set getOperations();
    
}
