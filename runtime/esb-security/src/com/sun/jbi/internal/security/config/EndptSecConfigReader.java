/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndptSecConfigReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  EndptSecConfigReader.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 16, 2004, 2:38 PM
 */

package com.sun.jbi.internal.security.config;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.internal.security.Constants;
import com.sun.jbi.internal.security.util.Loader;

import java.io.File;
import java.io.InputStream;
import java.io.Reader;

import javax.jbi.JBIException;
import org.xml.sax.InputSource;
import org.w3c.dom.Document;



/**
 *
 * @author Sun Microsystems, Inc.
 */
public class EndptSecConfigReader
{
    /** StringTranslator. */
    private StringTranslator mTranslator;
    
    /** Schemas for validation. */
    private String[] mSchemas;
    
    /** The Error Handler for handling SAX Parsing errors */
    private ConfigErrorHandler mErrHndlr;
    
    /** Validate Flag. */
    private boolean mValidate;
    
    /** 
     * Creates a new instance of EndptSecConfigReader.
     *
     * @param translator is the String Translator to use for i18N
     * @param schemaDir is the directory where all the schema files will reside.
     * @param 
     */
    public EndptSecConfigReader (StringTranslator translator, String schemaDir)
    {
        mTranslator = translator;
        mSchemas = new String[]
        {schemaDir + File.separator + Constants.DEPLOY_CONFIG_SCHEMA};
        mErrHndlr = new ConfigErrorHandler(translator);
        mValidate = false;
    }
    
    /**
     * @return true if validation will be done.
     */
    public boolean isValidating()
    {
        return mValidate;
    }
    
    /**
     * @param validate is the validation flag.
     */
    public void setValidating(boolean validate)
    {
        mValidate = validate;
    }
    
    /**
     * Read the Security Configuration from a input file.
     *
     * @param file is the File to read the config data from.
     * @return the EndpointSecurityConfig read.
     * @throws JBIException on errors.
     */
    public EndpointSecurityConfig read(File file)
        throws JBIException
    {
        try
        {
            return this.read(
                new java.io.FileInputStream(file));
        }
        catch (Exception ex)
        {
            throw new JBIException(ex.getMessage(), ex);
        }
    }
    
    /**
     * Read the Security Configuration from a input stream.
     *
     * @param istr is the inputstream to read the data from.
     * @return the EndpointSecurityConfig read.
     * @throws JBIException on Errors
     */
    public EndpointSecurityConfig read(InputStream istr)
        throws JBIException
    {
        Loader loader = new Loader();
        try
        {
            Document dom = loader.load(new InputSource(istr), mValidate, 
                mErrHndlr, mSchemas);
            return new EndptSecConfig(dom, mTranslator);
        }
        catch (Exception ex)
        {
            throw new JBIException(ex.getMessage(), ex);
        }
    }
    
    /**
     * Read the EndptSecConfig from the input string.
     * 
     * @param reader is the Reader to the XML Source
     * @return the EndptSecConfig instance
     * @throws JBIException on errors.
     */
    public EndpointSecurityConfig read(Reader reader)
        throws JBIException
    {
        Loader loader = new Loader();
        try
        {
            Document dom = loader.load(new InputSource(reader), mValidate, 
                mErrHndlr, mSchemas);
            return new EndptSecConfig(dom, mTranslator);
        }
        catch (Exception ex)
        {
            throw new JBIException(ex.toString(), ex);
        }
    }
    
}
