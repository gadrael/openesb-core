/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UsrPwdCallbackHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  UsrPwdCallbackHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 25, 2005, 10:37 PM
 */

package com.sun.jbi.internal.security.callback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;

/**
 * This CallbackHandler handles only NameCallback and
 * PasswordCallbacks, this handler is primarily used to provide 
 * username and password information which is known apriori to
 * a LoginModule.
 *
 * @author Sun Microsystems, Inc.
 */
public class UsrPwdCallbackHandler 
    implements CallbackHandler
{
    /** The Username to provide to a NameCallback. */
    private String mUsername = null;
    
    /** The Password to provide to a PasswordCallback. */
    private char[] mPassword = null;
    
    /**
     * @param username - user name
     * @param password - password
     */
    public UsrPwdCallbackHandler(String username, String password)
    {
        mUsername = username;
        mPassword = password.toCharArray();
    }
    
    /**
     * @param username - user name
     * @param password - password char array
     */
    public UsrPwdCallbackHandler(String username, char[] password)
    {
        mUsername = username;
        mPassword = password;
    }
    
    /**
     * Handle an array of Callbacks. 
     *
     * @param callbacks an array of Callback objects which contain
     * the information requested by an underlying security
     * service to be retrieved or displayed.
     * @exception java.io.IOException if an input or output error occurs.
     * @exception javax.security.auth.callback.UnsupportedCallbackException 
     * if the implementation of this method does not support one or more of the Callbacks 
     * specified in the callbacks parameter.
     */
    public void handle (Callback[] callbacks) 
        throws java.io.IOException, 
            javax.security.auth.callback.UnsupportedCallbackException
    {
        for (int i = 0; i < callbacks.length; i++)
        {
            if (callbacks[i] instanceof PasswordCallback)
            { 
                ((PasswordCallback) callbacks[i]).setPassword(mPassword);
            }
            else if (callbacks[i] instanceof NameCallback)
            {
                ((NameCallback) callbacks[i]).setName(mUsername);
            } 
            else
            {
                throw new javax.security.auth.callback.UnsupportedCallbackException 
                    (callbacks[i]);
            }
        }
    }
}
