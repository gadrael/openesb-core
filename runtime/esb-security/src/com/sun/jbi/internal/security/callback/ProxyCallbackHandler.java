/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ProxyCallbackHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ProxyCallbackHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 23, 2005, 9:54 AM
 */

package com.sun.jbi.internal.security.callback;

import com.sun.enterprise.security.jauth.callback.*;
import com.sun.jbi.binding.security.PasswordCredential;
import com.sun.jbi.internal.security.ThreadLocalContext;


import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;




/**
 * This is a Delegating CallbackHandler, it delegates the task of handling the
 * callbacks to secondary callback handlers.
 *
 * @author Sun Microsystems, Inc.
 */
public class ProxyCallbackHandler
    implements CallbackHandler
{
    /** The Key Information Callback Handler. */
    private CallbackHandler mKeyInfoHandler;
    
    /** The User Information Callback Handler. */
    private CallbackHandler mAuthHandler;
    
    /** 
     * Creates a new instance of ProxyCallbackHandler.
     *
     * @param keyInfoHandler is the callback handler to which this handler will delegate
     * the handling of the Callbacks for Key/Certificate information.
     *
     * @param authHandler is the callback handler to which this handler will delegate
     * the handling of the Callbacks for validating user information.
     */
    public ProxyCallbackHandler (CallbackHandler keyInfoHandler, 
        CallbackHandler authHandler)
    {
        mKeyInfoHandler = keyInfoHandler;
        mAuthHandler = authHandler;
    }
    
    /**
     * The implementation on the CallbackInterface.
     *
     * @param callbacks - array of Callbacks to be handled.
     * @throws java.io.IOException - if an input or output error occurs. 
     * @throws UnsupportedCallbackException - if the implementation of this method
     * does not support one or more of the Callbacks specified in the callbacks 
     * parameter.
     */
    public void handle(Callback[] callbacks)
        throws java.io.IOException, UnsupportedCallbackException
    {
        for (int i = 0; i < callbacks.length; i++) 
        {
            CallbackHandler handler = null;
            
            if  ( (callbacks[i] instanceof CertStoreCallback)  ||
                (callbacks[i] instanceof PrivateKeyCallback)   ||
                (callbacks[i] instanceof SecretKeyCallback)    ||
                (callbacks[i] instanceof TrustStoreCallback)   )
            {
                mKeyInfoHandler.handle(new Callback[]{callbacks[i]});
            } 
            else if (callbacks[i] instanceof PasswordValidationCallback) 
            {
                mAuthHandler.handle(new Callback[]{callbacks[i]});
            } 
            // -- Don't need separate classes for this
            else if (callbacks[i] instanceof NameCallback)
            {
                NameCallback nameCB = (NameCallback) callbacks[i];
                nameCB.setName("no-name");
                if ( ThreadLocalContext.getLocalSubject() != null )
                {
                    java.util.Iterator itr =  ThreadLocalContext.getLocalSubject().
                        getPrivateCredentials(PasswordCredential.class).iterator();
                    
                    
                    if (itr.hasNext())
                    {
                        PasswordCredential pc = (PasswordCredential) itr.next();
                        nameCB.setName(pc.getUsername());
                    }
                }
            }
            else if (callbacks[i] instanceof PasswordCallback)
            {
                PasswordCallback pwdCB = (PasswordCallback) callbacks[i];
                pwdCB.setPassword(new String("no-pwd").toCharArray ());
                if ( ThreadLocalContext.getLocalSubject() != null )
                {
                    java.util.Iterator itr =  ThreadLocalContext.getLocalSubject().
                        getPrivateCredentials(PasswordCredential.class).iterator();
                    
                    
                    if (itr.hasNext())
                    {
                        PasswordCredential pc = (PasswordCredential) itr.next();
                        pwdCB.setPassword(pc.getPassword().toCharArray());
                    }
                }
            }
            else
            {
                throw new UnsupportedCallbackException(callbacks[i]);
            }
        }
    }
    
}
