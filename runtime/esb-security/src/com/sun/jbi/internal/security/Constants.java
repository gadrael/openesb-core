/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Constants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security;

/**
 * Common class to define the Constants.
 * @author Sun Microsystems, Inc.
 */
public interface Constants
{
    /**
     * The binding/common package name.
     */
    String PACKAGE = "com.sun.jbi.internal.security";
    /**
     * MessageHandlerChain Element.
     */    
    String ELEMENT_HANDLER_CHAIN = "MessageHandlerChain";
    /**
     * MessageHandler Element.
     */    
    String ELEMENT_HANDLER = "MessageHandler";
    /**
     * MessageHandler Parameter.
     */    
    String ELEMENT_HANDLER_PARAMETER = "Parameter";
    /**
     * MessageHandler Classname.
     */    
    String ELEMENT_HANDLER_CLASSNAME = "ClassName";
    /**
     * Endpoint Element.
     */    
    String ELEMENT_ENDPOINT = "Endpoint";
    /**
     * Operation Element.
     */    
    String ELEMENT_OPERATION = "Operation";
    /**
     * RequestPolicy Element.
     */    
    String ELEMENT_REQ_POLICY = "RequestPolicy";
    /**
     * ResponsePolicy Element.
     */    
    String ELEMENT_RESP_POLICY = "ResponsePolicy";
    /**
     * Inbound Element. // TBD
     */    
    String ELEMENT_INBOUND = "Inbound";
    /**
     * Outbound element. // TBD
     */    
    String ELEMENT_OUTBOUND = "Outbound";
    /**
     * User Element.
     */    
    String ELEMENT_USER = "User";
    
    // -- Security Elements in the Configuration file
    /**
     * SecurityEnvironment.
     */    
    String ELEMENT_SECENV = "SecurityEnvironment"; 
    
    /**
     * SecurityConfiguration.
     */    
    String ELEMENT_SEC_CONFIG = "SecurityConfiguration"; 
    
    
    /**
     * KeyStoreManager Element.
     */    
    String ELEMENT_KEYSTORE_MANAGER = "KeyStoreManager"; 
    
    /**
     * KeyStoreManagers Element.
     */    
    String ELEMENT_KEYSTORE_MANAGERS = "KeyStoreManagers"; 
    
    /**
     * UserDomain Element.
     */    
    String ELEMENT_USER_DOMAIN = "UserDomain"; 
    
    /**
     * UserDomains Element.
     */    
    String ELEMENT_USER_DOMAINS = "UserDomains"; 
    
    /**
     * TransportSecurity Element in the Endpoint Deployment Config.
     */
    String ELEMENT_TRANSPORT_SECURITY = "TransportSecurity";
    
    /**
     * TransportSecurity/Server Element in the Endpoint Deployment Config.
     */
    String ELEMENT_SERVER = "Server";    
    
    /**
     * TransportSecurity/Server Element in the Endpoint Deployment Config.
     */
    String ELEMENT_CLIENT = "Client";
    
    /**
     * TransportSecurity/MessageProviderId Element in the Endpoint Deployment Config.
     */
    String ELEMENT_PROVIDER_ID = "MessageProviderId";
    
    /**
     * TransportSecurity/Consumer/SSLProtocol Element in the Endpoint Deployment Config.
     */
    String ELEMENT_SSL_PROTOCOL = "SSLProtocol";
    
    /**
     * TransportSecurity/Consumer/ClientAlias Element in the Endpoint Deployment Config.
     */
    String ELEMENT_SSL_CLIENT_ALIAS = "ClientAlias";
    
     /**
     * TransportSecurity/Consumer/UseDefault Element in the Endpoint Deployment Config.
     */
    String ELEMENT_SSL_USE_DEFAULT = "UseDefault";
    
    /**
     * TransportSecurity/Provider/RequireClientAuth Element in the Endpoint 
     * Deployment Config.
     */
    String ELEMENT_SSL_REQ_CLIENT_AUTH = "RequireClientAuth";
    
    
    /**
     * Name element.
     */
    String ELEMENT_NAME = "name";
    
    /**
     * Domain element.
     */
    String ELEMENT_DOMAIN = "domain";
    
    /**
     * Manager element.
     */
    String ELEMENT_MANAGER = "manager";
    
    /**
     * Param element.
     */
    String ELEMENT_PARAM = "param";
    
    /**
     * Param Name element.
     */
    String ELEMENT_PARAM_NAME = "paramname";
    
    /**
     * Param Value element.
     */
    String ELEMENT_PARAM_VALUE = "paramvalue";
    
    /**
     * Default ELement.
     */
    String ELEMENT_DEFAULT = "default";
    
    /**
     * Attribute: name.
     */    
    String ATTR_NAME = "name";
    /**
     * Attribute: value.
     */    
    String ATTR_VALUE = "value";
    /**
     * Attribute: auth-source.
     */
    String ATTR_AUTH_SRC = "auth-source";
    /**
     * Attribute: auth-source.
     */
    String ATTR_AUTH_RECP = "auth-recipient";

    /**
     * Attribute: service.
     */    
    String ATTR_SERVICE = "service";
    /**
     * Attribute: targetNamespace.
     */    
    String ATTR_TNS = "targetNamespace";
    /**
     * Attribute: location.
     */    
    String ATTR_LOCATION = "location";

    // TBD --
    /**
     * Attribute: password.
     */    
    String ATTR_PASSWORD = "password";
    /**
     * Attribute type.
     */    
    String ATTR_TYPE = "type";
    /**
     * Attribute: alias.
     */    
    String ATTR_ALIAS = "alias";
    /**
     * Attribute: keypassword.
     */    
    String ATTR_KEYPASS = "keyPassword";
    /**
     * Attribute: Username.
     */    
    String ATTR_USERNAME = "username";
    // TBD --
    
    /**
     * Attribute: default keystore type.
     */    
    String DEFAULT_STORE_TYPE = "JKS";
    
    /**
     * X509 type.
     */
    String X509 = "X.509";

    /**
     * XWSS Namespace.
     */    
    String NAMESPACE_XWSS = "http://java.sun.com/xml/ns/xwss/config";
    /**
     * Element: SecurityConfiguration.
     */    
    String ELEMENT_SECURITY_CONFIGURATION =
        "SecurityConfiguration";
    /**
     * SecurityConfiguration.
     */    
    String SECURITY_CONFIGURATION_PARAMETER =
        "securityConfiguration";
                
    /**
     * Endpoint Deployment Configuration Schema. TBD
     */    
    String CONFIG_SCHEMA = "EndpointConfig.xsd";  
    
    /**
     * Deployment Security Configuration Schema.
     */
    String DEPLOY_CONFIG_SCHEMA = "secdeployconfig.xsd";
     
    
    /**
     * Security Configuration Schema.
     */    
    String SEC_INSTALL_SCHEMA = "secinstallconfig.xsd";
    
    /**
     * Security Configuration File Name.
     */    
    String SEC_INSTALL_CONFIG_FILE = "secinstallconfig.xml";
    
    /**
     * XWSS Security Configuraion Schema file name.
     */    
    String SECURITY_CONFIG_SCHEMA = "xwssconfig.xsd";    
    
    /**
     * Outbound Security Handler Classname.
     */    
    String OUTBOUND_SECURITY_HANDLER_CLASSNAME =
        "com.sun.jbi.internal.security.msg.soap.OutboundSecurityHandler";
    /**
     * Inbound Security Handler Classname.
     */    
    String INBOUND_SECURITY_HANDLER_CLASSNAME =
        "com.sun.jbi.internal.security.msg.soap.InboundSecurityHandler";
    /**
     * Default: Operation Name.
     */    
    String DEFAULT_OPERATION_NAME = "default";
    
    /**
     * Subject.
     */    
    String SUBJECT = "subject";
    /**
     * Http Servlet Request.
     */    
    String HTTP_SERVLET_REQUEST = "http_servlet_request";
    /**
     * Manager constant.
     */    
    String MANAGER = "manager";
    
    /**
     * Domain constant.
     */
    String DOMAIN = "domain";
        
    /**
     * Parameter: KeyStore Location.
     */    
    String PARAM_KEYSTORE_LOCATION = "keystore.url";
    /**
     * Parameter: KeyStore type.
     */    
    String PARAM_KEYSTORE_TYPE = "keystore.type";
    /**
     * Parameter: KeyStore Password.
     */    
    String PARAM_KEYSTORE_PASS = "keystore.password";

    /**
     * Parameter: TrustStore Location.
     */    
    String PARAM_TRUSTSTORE_LOCATION = "truststore.url";
    /**
     * Parameter: TrustStore Type.
     */    
    String PARAM_TRUSTSTORE_TYPE = "truststore.type";
    /**
     * Parameter: TrustStore Password.
     */    
    String PARAM_TRUSTSTORE_PASS = "truststore.password";
    
    /**
     * true value constant.
     */    
    String TRUE = "true";
    
    /**
     * Component Id constant.
     */    
    String COMPONENT_ID = "component_id";
    
    /**
     * Parameter: File name.
     */    
    String PARAM_FILE_NAME = "file";

    /**
     * JAAS Auth type.
     */
    String JAAS = "JAAS";
    
    /**
     * JBI Schema Directory.
     */
    String SCHEMA_DIR = "schemas";
    
    /**
     * JBI Schema Directory.
     */
    String JBI_DIR = "jbi";
    
    /**
     * JBI Install Root.
     */
    String JBI_INSTALL_ROOT = "com.sun.jbi.home";
    
}
