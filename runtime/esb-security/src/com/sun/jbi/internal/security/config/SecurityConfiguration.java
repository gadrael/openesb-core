/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityConfiguration.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 22, 2004, 4:23 PM
 */

package com.sun.jbi.internal.security.config;

import java.util.HashMap;
import java.util.Properties;

/**
 * The Install time security configuration.
 * @author Sun Microsystems, Inc.
 */
public interface SecurityConfiguration
{

    /**
     *  Get the Trabnsport Security Configuration.
     *
     * @return the Property Set which encapsulates the SSL configuration.
     */
    Properties getTransportSecurityContext();

    /**
     * Get the Name of the default User Domain.
     *
     * @return the name of the Default User Domain
     */
    String getDefaultUserDomainName();

    /**
     * Get a Map of User Domain Contexts by their name.
     * [ Key = Name (string) : Value = UserDomain Contexts (Properties) ]
     * @return a Map of User Domain Contexts by their name
     */
    HashMap getUserDomainContexts();

    /**
     * Get the Name of the default KeyStore Service.
     *
     * @return the name of the default KeyStore.
     */
    String getDefaultKeyStoreManagerName();

    /**
     * Get a Map of KeyStore Services keyed by their name.
     *
     * [ Key = Name (string) : Value = KeyStoreContexts (Properties) ]
     * @return a Map of KeyStore Manager keyed by their name.
     */
    HashMap getKeyStoreContexts();
    
    /**
     * As the name suggests generate a DOM from the Configuration data this object holds.
     *
     * @return a DOM based on the state of this object.
     * @throws javax.jbi.JBIException on errors.
     */
    org.w3c.dom.Document generateDocument()
        throws javax.jbi.JBIException;
     
}
