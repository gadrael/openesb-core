/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)HttpConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  HttpConstants.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 11, 2004, 1:39 PM
 */

package com.sun.jbi.internal.security.https;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public interface HttpConstants
{
    /**
     * The Package name.
     */
    String PACKAGE = "com.sun.jbi.internal.security.https";
    
    /**
     * The Package name.
     */
    String MAIN_PACKAGE = "com.sun.jbi.internal.security";
    
    /** SSL Client Certificate Required. */
    int CLIENT_CERTIFICATE_REQUIRED = 407;
    
    /** SSL Client Certificate failed Authentication. */
    int CLIENT_CERTIFICATE_INVALID = 407;
    
    /**
     * Parameter: SSL Default Settings parameter.
     */    
    String PARAM_SSL_USE_DEFAULT = "ssl.client.use.default";
    /**
     * Parameter: SSL Protocol parameter.
     */    
    String PARAM_SSL_PROTOCOL = "ssl.client.protocol";
    
    /**
     *  Parameter: Require SSL Client Auth.
     */    
    String PARAM_SSL_REQ_CLIENT_AUTH = "ssl.server.require.client.auth";
    
    /**
     *  Parameter: Require SSL Client Auth.
     */    
    String PARAM_SSL_CLIENT_ALIAS = "ssl.client.alias";
    
    /**
     *  Parameter: Require SSL Client Auth.
     */    
    String PARAM_SSL_CLIENT_ALIAS_PASSWD = "ssl.client.alias.password";
    
    
    /**
     * Default SSL Protocol value.
     */    
    String DEFAULT_SSL_PROTOCOL = "TLS";
    
    /**
     * SSLv3.
     */
    String SSLV3 = "SSLv3";
    
    /**
     * TLSv1.
     */
    String TLSV1 = "TLSv1";
    
    /**
     * TLSv1.
     */
    String TLS = "TLS";
    
    /**
     * Default KeyManager instances will use this algorithm.
     */    
    String DEFAULT_KEY_MANAGEMENT_ALGO = "SunX509";
    
    /**
     * Default value for require client auth is false.
     */
    boolean DEFAULT_SSL_REQ_CLIENT_AUTH = false;
    
    /**
     * Default value for the client alias is an empty string.
     */
    String DEFAULT_CLIENT_ALIAS = "";
    
    /**
     * Default value for the client alias password is an empty string.
     */
    String DEFAULT_CLIENT_ALIAS_PWD = "";
    
    
    /**
     * The Servlet request Certificate parameter.
     */
    String X509CERT_SERVLET_REQUEST_ATTRIB = 
        "javax.servlet.request.X509Certificate";
    
    
    
    
    
    
    
    /*--------------------------------------------------------------------------------*\
     *                          Localized String Constants                            *
    \*--------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------*\
     *                          INFORMATIONAL  Messages                              *
    \*-------------------------------------------------------------------------------*/
    
    /** INFO: TLS Created. */
    String BC_INFO_CREATE_TLS_CTX = "BC_INFO_CREATE_TLS_CTX";
    
    /** INFO: KS Mgr Used. */
    String BC_INFO_UD_USED = "BC_INFO_UD_USED";
    
     /** INFO: User Doamain Used. */
    String BC_INFO_KSMGR_USED = "BC_INFO_KSMGR_USED";
    
    /** INFO: Using the default SSL Context. */
    String BC_INFO_SSL_PROTOCOL_USED = "BC_INFO_SSL_PROTOCOL_USED";
    
    /** INFO: Using the default SSL Context. */
    String BC_INFO_USING_SYSTEM_DEFAULT_SSL_CONTEXT =
        "BC_INFO_USING_SYSTEM_DEFAULT_SSL_CONTEXT";
    
    /** INFO: On the alias being used for Client Auth. */
    String BC_INFO_CLIENT_AUTH_DETAILS = 
        "BC_INFO_CLIENT_AUTH_DETAILS";
    

    /*-------------------------------------------------------------------------------*\
     *                          ERROR Messages                                       *
    \*-------------------------------------------------------------------------------*/
    
    /** ERR: Client Certificate required. */
    String BC_ERR_CLIENT_CERT_REQUIRED = "BC_ERR_CLIENT_CERT_REQUIRED";
    
    /** ERR: Client Certificate required details. */
    String BC_ERR_CLIENT_CERT_REQUIRED_DETAIL = 
        "BC_ERR_CLIENT_CERT_REQUIRED_DETAIL";
    
    /** ERR: Client Certificate required. */
    String BC_ERR_CLIENT_CERT_AUTH_FAILED = "BC_ERR_CLIENT_CERT_AUTH_FAILED";
    
    /** ERR: Client Certificate required details. */
    String BC_ERR_CLIENT_CERT_AUTH_FAILED_DETAIL = 
        "BC_ERR_CLIENT_CERT_AUTH_FAILED_DETAIL";
    
    /** ERR: SSL Ctx creation failed. */ 
    String  BC_ERR_SSL_CTX_FAIL_USE_DEFAULT = 
        "BC_ERR_SSL_CTX_FAIL_USE_DEFAULT";
    
    /** ERR: Hostname Verification failed. */
    String BC_ERR_HOSTNAME_VERIFICATION_FAILED = 
        "BC_ERR_HOSTNAME_VERIFICATION_FAILED";
    
    /** ERR: Certificate Authentication failed. */
    String BC_ERR_CERT_AUTH_FAILED = "BC_ERR_CERT_AUTH_FAILED";
    
    /** ERR: Could not get the Certificate Chain for alias. */
    String BC_ERR_GET_CERT_CHAIN_FAILED = "BC_ERR_GET_CERT_CHAIN_FAILED";
    
    /** ERR: Could not get the private key for an alias. */
    String BC_ERR_GET_PRIVATE_KEY_FAILED = "BC_ERR_GET_PRIVATE_KEY_FAILED";
     
         
    /** ERR: SecurityContext specific SSL Context creation failed. */
    String BC_ERR_SSL_CONTEXT_CREATION_FAILED =
        "BC_ERR_SSL_CONTEXT_CREATION_FAILED";
    
    /** ERR: Key Management Algorithm not supported. */
    String BC_ERR_KEYMGMT_ALGO_NOT_SUPPORTED = 
        "BC_ERR_KEYMGMT_ALGO_NOT_SUPPORTED";
    
    /** ERR: Key Store Manager does not know of Client Alias. */
    String BC_ERR_MISSING_PRIVATE_KEY = "BC_ERR_MISSING_PRIVATE_KEY";

    
    /*-------------------------------------------------------------------------------*\
     *                          WARNING Messages                                     *
    \*-------------------------------------------------------------------------------*/
    
    /** WRN: Endpoint deployment info could not be retrieved. */
    String BC_WRN_ENDPT_DEPL_CTX_NOT_FOUND = 
        "BC_WRN_ENDPT_DEPL_CTX_NOT_FOUND";

    
}
