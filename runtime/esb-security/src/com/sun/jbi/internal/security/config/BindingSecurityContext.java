/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingSecurityContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.config;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.internal.security.LocalStringConstants;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The BindingSecurityContext interface provides access to the Security Information 
 * configured for an endpoint.
 *
 * @author Sun Microsystems, Inc.
 */
public class BindingSecurityContext
    extends com.sun.jbi.internal.security.ContextImpl
    implements SecurityContext
{
    /** KeyStore Manager. */
    private String mKeyStoreManagerName;
    
    /** UserDomain Name */
    private String mUserDomainName;
    
    /** 
     * The String Translator. 
     */
    private StringTranslator mTranslator;
    
    /**
     * The Provider Id.
     */
    private String mProviderId = null;
    
    /**
     * Constructor.
     *
     * @param translator is the StringTranslator.
     */
    public BindingSecurityContext(StringTranslator translator)
    {
        super(translator);
        mTranslator = translator;
        mKeyStoreManagerName = "";
        mProviderId = null;
        mUserDomainName = "";
    }
  
    /**
     * Get the name of the KeyStoreManager to use with this Security Configuration.
     *
     * @return the name of the User Domain, if the this parameter is missing in
     * the deployment security configuration an empty string is returned "".
     */
    public String getKeyStoreManagerName()
    {
        return mKeyStoreManagerName; 
    }
    
    /**
     * Set the name of the Keystore Manager from the deployment context.
     * @param name is the KeyStoreManager name to be set
     */
    public void setKeyStoreManagerName(String name)
    {
        mKeyStoreManagerName = name; 
    }
    
    /**
     * Get the name of the User Domain to use with this Security Configuration.
     *
     * @return the name of the User Domain, if this parameter is missing in
     * the deployment security configuration an empty string is returned "".
     */
    public String getUserDomainName()
    {
        return mUserDomainName;
    }
    
    /**
     * Set the name of the User Domain from the deployment security configuration.
     * @param name is the name of the UserDomain to be set
     */
    public void setUserDomainName(String name)
    {
        mUserDomainName = name; 
    }
    
    /**
     * Print the Contents of the Context to the Logger.
     *
     * @param logger is the java.util.Logger to use for printing out the contents.
     * @param level is the logging level
     */
    public void print (Logger logger, Level level)
    {
        super.print(logger, level);
        StringBuffer buffer = new StringBuffer();
        buffer.append(mTranslator.getString(LocalStringConstants.CONST_KM)
            +  " " + getKeyStoreManagerName() + "\n");
        buffer.append(mTranslator.getString(LocalStringConstants.CONST_UD)
            +  " " + getUserDomainName() + "\n");    
        logger.log(level, buffer.toString());
    }
    
    /**
     * Get the Provider Id.
     *
     * @return the Message Provider Id.
     */
    public String getMessageProviderId ()
    {
        return mProviderId;
    }
    
    /**
     * Set the Message Provider Id.
     *
     * @param providerId is the Message Provider Id.
     */
    public void setMessageProviderId (String providerId)
    {
        mProviderId = providerId;
    }
    
}
