/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ThreadLocalContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ThreadLocalContext.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 6, 2005, 6:57 PM
 */

package com.sun.jbi.internal.security;

import javax.security.auth.Subject;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class ThreadLocalContext
{
    
    /**
     * The static Inheritable Thread Local
     */
    private static InheritableThreadLocal sTLSubject = new InheritableThreadLocal();
    
    /** Creates a new instance of ThreadLocalContext. */
    public ThreadLocalContext ()
    {
    }
    
    /**
     * Get the Subject from the TLS. If a Subject has been set in TLS by calling
     * by calling setLocalSubject() this value should be returned, else a null value will
     * be returned.
     *
     * @return the Subject from TLS.
     */
    public static Subject getLocalSubject()
    {
        return (Subject) sTLSubject.get();
    }
    
    /** 
     * Set the Subject in the TLS.
     *
     * @param subject is the Subject to be set in the TLS.
     */
    public static void setLocalSubject(Subject subject)
    {
        if (subject == null )
        {
            return;
        }
        
        sTLSubject.set(subject);
    }
}
