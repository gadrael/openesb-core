/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UserDomain.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  UserDomain.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 24, 2004, 10:51 AM
 */

package com.sun.jbi.internal.security;

/**
 * UserDomain is the handle to getting the user information.
 * This interface will be refined further based on requirements.
 *
 * The methods in this interface throw Exception and no specific exception has been 
 * defined to keep the interface non-proprietary.
 * @author Sun Microsystems, Inc.
 */
public interface UserDomain
    extends com.sun.jbi.binding.security.Context
{
    
    /**
     * Get the User Domain Name.
     * @return the name of the User Domain
     */
    String getName();
    
    /**
     * Set the User Domain Name.
     * @param name is the name of the User Domain
     */
    void setName(String name);
    

    /**
     * Get the Authentication Type for the domain.
     *
     * @return the Authentication Type.
     */
    String getAuthType();
    
    /**
     * Set the Authentication Type for this domain.
     *
     * @param authType is the authentication type string, the only supported
     * type in Shasta 1.0 is JAAS.
     */
    void setAuthType(String authType);

}
