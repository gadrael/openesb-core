/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Authenticator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Authenticator.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 8, 2005, 12:33 PM
 */

package com.sun.jbi.internal.security.auth;

import com.sun.jbi.internal.security.UserDomain;

import javax.security.auth.callback.CallbackHandler;

/**
 * An Authenticator is used for authnetication. A Authenticator instance should be
 * created for each authentication request.
 *
 * @author Sun Microsystems, Inc.
 */
public interface Authenticator
    extends CallbackHandler
{
    
    /**
     * Initialize the authenticator with the UserDomain.
     *
     * @param domain is the UserDomain which has the Authentication Context.
     * @throws IllegalStateException if initialization fails.
     */
    void initialize(UserDomain domain)
        throws IllegalStateException;

}
