<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)pom.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <parent>
        <artifactId>runtime</artifactId>
        <groupId>net.open-esb.core</groupId>
        <version>3.1.1-SNAPSHOT</version>
    </parent>
    
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.open-esb.core</groupId>
    <artifactId>manage</artifactId>
    <name>management-services</name>
    
    <description>JBI Runtime Management components, providing installation, deployment, and other JMX interfaces for
        remote management consoles.
    </description>

    <properties>
        <esb.registry.xml.package>com.sun.jbi.management.registry.xml</esb.registry.xml.package>
        <esb.schemaorg_apache_xmlbeans.system.jbi.xml.package>com.sun.jbi.management.descriptor</esb.schemaorg_apache_xmlbeans.system.jbi.xml.package>
        <management.message.package>com.sun.jbi.management.message</management.message.package>
        <SECONDARY_ARTIFACT_ID>sun-component-extensions</SECONDARY_ARTIFACT_ID>
    </properties>
    
    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>schemas/*.xsd</include>
                </includes>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.jvnet.jaxb2.maven2</groupId>
                <artifactId>maven-jaxb2-plugin</artifactId>
                <executions>
                    <execution>
                        <id>jbi-registry</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <schemaDirectory>src/main/resources/schemas</schemaDirectory>
                            <schemaIncludes>
                                <include>jbi-registry.xsd</include>
                            </schemaIncludes>
                            <bindingExcludes>
                                <exclude>*.xjb</exclude>
                            </bindingExcludes>
                            <generatePackage>com.sun.jbi.management.registry.xml</generatePackage>
                            <generateDirectory>${project.build.directory}/generated-sources/src-jbi-registry</generateDirectory>
                            <episode>false</episode>
                        </configuration>
                    </execution>
                    <execution>
                        <id>jbi</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <schemaDirectory>src/main/resources/schemas</schemaDirectory>
                            <schemaIncludes>
                                <include>jbi.xsd</include>
                            </schemaIncludes>
                            <bindingExcludes>
                                <exclude>*.xjb</exclude>
                            </bindingExcludes>
                            <generatePackage>com.sun.jbi.management.descriptor</generatePackage>
                            <generateDirectory>${project.build.directory}/generated-sources/src-jbi</generateDirectory>
                            <episode>false</episode>
                        </configuration>
                    </execution>
                    <execution>
                        <id>managementMessage</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <schemaDirectory>src/main/resources/schemas</schemaDirectory>
                            <schemaIncludes>
                                <include>managementMessage.xsd</include>
                            </schemaIncludes>
                            <generatePackage>com.sun.jbi.management.message</generatePackage>
                            <generateDirectory>${project.build.directory}/generated-sources/src-management</generateDirectory>
                            <episode>false</episode>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <artifactId>maven-jar-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-secondary-jar</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                        <configuration>
                            <classesDirectory>${project.build.directory}/${SECONDARY_ARTIFACT_ID}</classesDirectory>
                            <classifier>${SECONDARY_ARTIFACT_ID}</classifier>
                        </configuration>
                    </execution>
                    
                    <execution>
                        <id>${project.artifactId}-resources-jar</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                        <configuration>
                            <classesDirectory>${project.basedir}/src/main/resources</classesDirectory>
                            <classifier>resources</classifier>
                            <includes>
                                <include>**/schemas/*</include>
                            </includes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <id>${project.artifactId}-add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>${project.build.directory}/jaxb_code_gen/src</source>
                            </sources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
    
    <dependencies>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>base</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>jbi</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>esb-util</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>jbi-admin-common</artifactId>
            <version>${project.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.apache.ant</groupId>
            <artifactId>ant-nodeps</artifactId>
            <version>1.8.1</version>
        </dependency>
        
        <!--
        <dependency>
            <groupId>jdmk</groupId>
            <artifactId>jdmkrt</artifactId>
        </dependency>
        -->
        
        <!-- TEST -->
        <!--
        <dependency>
            <groupId>jmx4ant</groupId>
            <artifactId>jmx4ant</artifactId>
            <version>1.2b1</version>
            <scope>test</scope>
        </dependency>
        -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>
