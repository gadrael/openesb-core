#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00321.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

testname=`echo "$0" | sed 's/^.*\///' | sed 's/\..*//'`
#echo testname is $testname

. ./regress_defs.ksh

#mark the end of the log, so that this test is repeatable:
start_line_count=`cat "$JBI_DOMAIN_ROOT/logs/server.log" | wc -l | sed -e 's/ //g'`
echo $testname - start_line_count=$start_line_count

show_installs()
{
    echo  Library installed
    tail +$start_line_count "$JBI_DOMAIN_ROOT/logs/server.log" | grep 'Shared Library .* has been installed' | wc -l
    echo  Library uninstalled
    tail +$start_line_count "$JBI_DOMAIN_ROOT/logs/server.log" | grep 'Shared Library .* has been uninstalled' | wc -l
}

echo now calling ant to install the shared library
ant -q -emacs -propertyfile "$JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml

sleep 1
show_installs

echo calling ant to reinstall the shared library
ant -q -emacs -propertyfile "$JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml

sleep 1
show_installs
