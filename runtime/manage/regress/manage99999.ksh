#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage99999.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#shutdown the jbi installations, and perform any other necessary cleanup.

. ./regress_defs.ksh

shutdown_domain
status=$?

#save our server.log for debugging:
logdst="$SVC_BLD/${JBI_DOMAIN_NAME}_server.log"
rm -f "$logdst"
cp "$JBI_DOMAIN_DIR/$JBI_DOMAIN_NAME/logs/server.log" "$logdst"

exit $status
