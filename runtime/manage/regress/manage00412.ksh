#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00412.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
##!/bin/sh
#
testname=`echo "$0" | sed 's/^.*\///' | sed 's/\..*//'`
##echo testname is $testname
#
. ./regress_defs.ksh
#

###
# Publish the File Binding SA 
###
ant -q -emacs -propertyfile "$JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml publish_filebinding_sa

###
# Deploy the Published File Binding SA 
###
ant -q -emacs -propertyfile "$JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml deploy
sleep 1
echo "has been deployed"
grep "has been deployed" $JBI_DOMAIN_ROOT/logs/server.log | wc -l

###
# Undeploy the File Binding SA 
###
ant -q -emacs -propertyfile "$JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml undeploy
sleep 1
echo "has been undeployed"
grep "has been undeployed" $JBI_DOMAIN_ROOT/logs/server.log | wc -l

###
# Unpublish the File Binding SA 
###
ant -q -emacs -propertyfile "$JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml unpublish_filebinding_sa
