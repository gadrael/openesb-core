/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ArchiveDownload.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.system.ManagementContext;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.rmi.server.UID;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 *  Downloads archive bytes from the ESB repository.
 */
public class ArchiveDownload implements ArchiveDownloadMBean
{    
    private ManagementContext   mContext;
    private StringTranslator    mStrings;
    private Logger              mLog;
    private HashMap             mSessions;
    private Repository          mRepository;
    private String              mInstallRoot;
    
    public ArchiveDownload(ManagementContext ctx)
    {
        mContext    = ctx;
        mSessions   = new HashMap();
        mLog        = ctx.getLogger();
        mRepository = ctx.getRepository();
        mStrings    = ctx.getEnvironmentContext().getStringTranslator("com.sun.jbi.management");
        mInstallRoot = new File (ctx.getEnvironmentContext().getJbiInstanceRoot()).getPath();
    }
    
    /** 
     *  Returns the archive id of a service assembly in the repository.
     *  @param saName name of the service assembly
     *  @return archive ID, or null if the service assembly does not exist
     *  in the repository.
     */
    public Object getServiceAssemblyArchiveId(String saName)
    {
        return mRepository.findArchive(ArchiveType.SERVICE_ASSEMBLY, saName);
    }
    
     /** 
     *  Returns the archive id of a component in the repository.
     *  @param componentName name of the component
     *  @return archive ID, or null if the component does not exist
     *  in the repository.
     */
    public Object getComponentArchiveId(String componentName)
    {        
        return mRepository.findArchive(ArchiveType.COMPONENT, componentName);
    }
    
    /** 
     *  Returns the archive id of a shared library in the repository.
     *  @param sharedLibrayName name of the shared library
     *  @return archive ID, or null if the shared library does not exist
     *  in the repository.
     */
    public Object getSharedLibraryArchiveId(String sharedLibrayName)
    {        
        return mRepository.findArchive(ArchiveType.SHARED_LIBRARY, sharedLibrayName);
    }
    
    /** Initiates the download of the DAS registry. This is a special purpose 
     *  method due to the need to synchronize copying the registry while others may be
     *  writing to it.
     *  @return download session id
     */
    public Object initiateRegistryDownload()
        throws java.io.IOException
    {
        String                  id;
        
        //
        //  Save the snapshot result for the download.
        //
        try
        {
            //mContext.getEnvironmentContext().isFrameworkReady(true);
            mSessions.put(id = createUID(), 
                    ((Registry)mContext.getEnvironmentContext().getRegistry()).snapshot());
            return id;
        }
        catch (Exception e)
        {
            throw new java.io.IOException(e.toString());            
        }
    }
    
    /** Initiates the download of an archive identified by 'archiveId'.  In
     *  most situations, the archiveId will simply be the CAS-local path to
     *  the archive in the ESB repository.  The path is checked to ensure that
     *  only files contains in the jbi install root can be copied. A relative path
     *  is prepended with the jbi install root. The returned Object is used as
     *  a session id for downloading the archive bytes with
     *  <code>downloadBytes()</code>.
     *  @return download session id
     */
    public Object initiateDownload(Object archiveId)
        throws java.io.IOException
    {
        String              file = (String)archiveId;
        String              id;
        File                archive;
        FileInputStream     fis;
        
        archive = new File(file);
        if (!archive.isAbsolute())
        {
            archive = new File (file = mInstallRoot + File.separator + file);
        }
        if (file.startsWith(mInstallRoot))
        {
            id      = createUID();

            if (!archive.exists())
            {
                throw new java.io.IOException(mStrings.getString(
                        LocalStringKeys.JBI_ADMIN_DOWNLOAD_ARCHIVE_DOES_NOT_EXIST,
                        archive.getPath()));
            }

            fis = new FileInputStream(archive);
            mSessions.put(id, fis);

            return id;
        }
        throw new java.io.IOException(mStrings.getString(
            LocalStringKeys.JBI_ADMIN_DOWNLOAD_ARCHIVE_DOES_NOT_EXIST,
            file));

    }
    
    /** Download <code>length</code> bytes from an existing download session.
     *  @return a byte array of the specified length.  If the remaining bytes
     *  in the session <= length, the byte array is sized appropriately.  If
     *  all bytes have been downloaded, the returned array length is 0.
     */
    public byte[] downloadBytes(Object id, int length)
        throws java.io.IOException
    {
        int     count;
        byte[]  bytes;
        
        bytes = new byte[length];
        count = getDownloadStream(id).read(bytes);
        
        // check to make sure array sizes match
        if (count <= 0)
        {
            bytes = new byte[0];
        }
        else if (count != length)
        {
            byte[] tmp = new byte[count];
            System.arraycopy(bytes, 0, tmp, 0, count);
            bytes = tmp;
        }
        
        return bytes;
    }
    
    /** Used to indicate that a download session is complete.
     */
    public void terminateDownload(Object id)
        throws java.io.IOException
    {
        getDownloadStream(id).close();
        mSessions.remove(id);
    }
    
    public void terminateAllDownloads()
    {
        Iterator ids;
        
        try
        {
            ids = mSessions.keySet().iterator();
            while (ids.hasNext())
            {
                terminateDownload(ids.next());
            }
        }
        catch (java.io.IOException ioEx)
        {
            mLog.warning(ioEx.getMessage());
        }
    }
    
    /** Retrieve the input stream for a session. */
    private InputStream getDownloadStream(Object id)
        throws java.io.IOException
    {
        InputStream is;
        
        is = (InputStream)mSessions.get(id);
        if (is == null)
        {
            throw new java.io.IOException(mStrings.getString(
                    LocalStringKeys.JBI_ADMIN_DOWNLOAD_ID_NOT_FOUND));
        }
        
        return is;
    }
    
    /** Create a unique id for the download session. */
    private String createUID()
    {
        String uid;
        
        uid = new UID().toString();
        if (uid.startsWith("-"))
        {
            uid = uid.substring(1);
        }
        
        return uid;
    }
}
