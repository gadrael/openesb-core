/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DynamicRuntimeConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.management.MBeanNames;

import java.util.Map;
import java.util.HashMap;
import java.util.Properties;

/**
 * ConfigurationBuilder is the producer of configuration data set for a 
 * particular category.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConfigurationBuilder 
{
    /** Configuration map */
    private static HashMap<MBeanNames.ServiceType, ConfigurationFactory> sConfigs;
    
    /**
     * @param defaults - default attribute values.
     * @return a Map of configuration factories, keyed by the configuration category
     */
    public static synchronized Map<MBeanNames.ServiceType, ConfigurationFactory> 
        createConfigurations(Properties defaults)
    {
        if ( sConfigs == null )
        {
            sConfigs = new HashMap();
            
            sConfigs.put(MBeanNames.ServiceType.Installation, new InstallationConfigurationFactory(defaults));
            sConfigs.put(MBeanNames.ServiceType.Deployment,   new DeploymentConfigurationFactory(defaults));
            sConfigs.put(MBeanNames.ServiceType.System,       new SystemConfigurationFactory(defaults));
            sConfigs.put(MBeanNames.ServiceType.Logger,       new LoggerConfigurationFactory(defaults));
        }
        return sConfigs;
    }
    
    /**
     * @return the control type corresponding to a service type
     */
    public static String getControlType(MBeanNames.ServiceType svcType)
    {
        String controlType = MBeanNames.CONTROL_TYPE_SYSTEM;
        
        switch(svcType)
        {
            case Installation:
                controlType = MBeanNames.CONTROL_TYPE_INSTALL_SERVICE;
                break;
            case Deployment:
                controlType = MBeanNames.CONTROL_TYPE_DEPLOY_SERVICE;
                break;
            case System:
                controlType = MBeanNames.CONTROL_TYPE_SYSTEM;
                break;
            case Logger:
                controlType = MBeanNames.CONTROL_TYPE_LOGGING_SERVICE;
                break;
        }
        return controlType;
    }
    
}
