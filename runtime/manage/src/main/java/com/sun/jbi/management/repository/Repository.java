/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Repository.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.internal.support.DirectoryUtil;
import com.sun.jbi.management.internal.support.JarFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Iterator;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.logging.Logger;

/** 
 *  A repository for ESB artifacts.  The ESB Repository stores bindings, engines,
 *  shared libraries, service assemblies, service units, and temporary uploads 
 *  from remote administration clients.
 *
 *  The ArchiveType enumeration is used to declare the type used by an archive
 *  operation.  In addition to the archive type, a name is given to identify
 *  an archive uniquely.  The combination of name and type prevents naming 
 *  conflicts between different classes of artifacts (e.g. bindings and shared 
 *  libraries).  When attempting to reference a service unit, the following
 *  naming rule is used: <br><br>
 *  
 *  <code>[sa-name]'/'[su-name]<code>
 */
public class Repository 
{
    /** Repository directory names. */
    private static final String COMPONENT_STORE_NAME        = "components";
    private static final String SHARED_LIBRARY_STORE_NAME   = "shared-libraries";
    private static final String SERVICE_ASSEMBLY_STORE_NAME = "service-assemblies";
    private static final String TEMP_STORE_NAME             = "tmp" + File.separator + "upload";
    
    /** Install Root */
    private static final String INSTALL_ROOT                = "install_root";
    
    /** AS Applications dir name */
    private static final String APPLICATIONS_DIR            = "applications";
    
    /** Name used to locate string translation file. */
    private static final String STRING_TRANSLATOR_NAME = 
            "com.sun.jbi.management";
    
    /** R/W buffer size for copying archive files. */
    private static final int BUFFER_SIZE = 8 * 1024;
    
    /** Translate messages into exciting languages. */
    private StringTranslator mStrings;
    
    private ManagementContext mContext;    
    private Logger mLog;
    
    /** The Directory suffix pattern. This is the suffix of a directory name
     *  when a directory by the actual name exists. It is ".(n)+" 
     *  example SunFileBinding.12
     */
    protected static final String DIR_SUFFIX_PATTERN          = "\\x2E\\d+";
    
    /** File references to repository stores. */
    private File mComponentStore;
    private File mSharedLibraryStore;
    private File mServiceAssemblyStore;
    private File mTempStore;
    private long mRepositoryInitTime;
    
    /** Cache containing archives which have been added or retrieved.  Archives
     *  removed from the repository must be removed from the cache.  Since the
     *  individual operations which manipulate the cache are thread-safe, there
     *  is no need to independently synchronize access to the cache.  The cache
     *  is keyed by archive's absolute path.
     */
    private HashMap<String, Archive> mArchiveCache;
    
    /** Create a new instance of the repository.
     */
    public Repository(ManagementContext context)
        throws RepositoryException
    {
        mContext        = context;
        mStrings        = context.getEnvironmentContext().getStringTranslator(STRING_TRANSLATOR_NAME);        
        mLog            = context.getLogger();
        mArchiveCache   = new HashMap<String, Archive>();
        initStores();
    }
    
    public void cleanRepository()
    {
        // clean out the temp store
        cleanDirectory(mTempStore, false);

        // Clean up archive directories which are marked for deletion
        removeDeletedArchives();
    }

    /** Set repository init time, this could be NFS time
     * @param initTime
     */
    public void setRepositoryInitTime(long initTime)
    {
        mRepositoryInitTime = initTime;
    }
    
    /** Add an archive to the repository. */
    public Archive addArchive(ArchiveType type, String path)
        throws RepositoryException
    {
        Archive archive;
        
        try
        {
            archive = new Archive(new File(path), true);
        }
        catch (java.io.IOException ioEx)
        {
            throw new RepositoryException(ioEx);
        }
        
        // verify archive type
        if (!archive.getType().equals(type))
        {
            throw new RepositoryException(mStrings.getString(
                    LocalStringKeys.JBI_ADMIN_ARCHIVE_TYPE_MISMATCH, 
                    type.toString(), 
                    archive.getType().toString()));
        }
        
        addArchive(archive);
        return archive;
    }
    
    /** Add a pre-parsed archive to the repository.
     */
    public synchronized void addArchive(Archive archive)
        throws RepositoryException
    {
        // check for duplicates
        if (archiveExists(archive.getType(), archive.getJbiName()))
        {   
            throw new RepositoryException(mStrings.getString(
                    LocalStringKeys.JBI_ADMIN_ARCHIVE_EXISTS, 
                    archive.getType().toString(),
                    archive.getJbiName()));
        }
        
        // add the archive to the repository
        addToRepository(archive);
        mArchiveCache.put(archive.getPath(), archive);
    }
    
    /** Used to determine if a particular archive is stored in the repository. */
    public boolean archiveExists(ArchiveType type, String name)
    {
        return getArchiveDirectory(type, name) != null;
    }
    
    /** Retrieve archive details for the specified id. */
    public synchronized Archive getArchive(Object archiveId)
    {
        File        file;
        Archive     archive = null;
        Calendar    uploadTime;
        
        try
        {
            file = new File((String)archiveId);
            
            if (file.exists())
            {
                // check to see if we have a copy in our cache first
                if (mArchiveCache.containsKey(archiveId))
                {
                    return mArchiveCache.get(archiveId);
                }
                
                archive = new Archive(file, false);
                uploadTime = Calendar.getInstance();
                uploadTime.setTimeInMillis(file.lastModified());
                archive.setUploadTimestamp(uploadTime);
            
                if (archive.getType().equals(ArchiveType.SERVICE_UNIT))
                {
                    // jbi.xml does not provide SU name, so cheat with the dir name
                    archive.setJbiName(file.getParentFile().getName());
                }

                // Update the cache
                mArchiveCache.put(archive.getPath(), archive);
            }
        }
        catch (Exception ex)
        {
            mLog.info(ex.getMessage());
        }

        return archive;
    }
    
    /** Retrieve archive details for the specified id. */
    public synchronized Archive getArchive(ArchiveType type, String name)
    {
        Archive archive = null;
        String  path;
        
        // Look up in the cache first
        archive = searchArchiveCache(type, name);
        
        if ( archive == null )
        {
            // Look into FS 
            path = findArchive(type, name);
            if (path != null)
            {
                archive = getArchive(path);
            }
        }

        return archive;
    }
    
    /** Returns the path to the directory where the archive is stored, or null 
     *  if archive does not exist in the repository.
     */
    public String findArchiveDirectory(ArchiveType type, String name)
    {
        String  dirPath = null;
        File    archiveDir;
        
        archiveDir = getArchiveDirectory(type, name);
        if (archiveDir != null && archiveDir.exists())
        {
            dirPath = archiveDir.getAbsolutePath();
        }
        
        return dirPath;
    }
            
    /** Returns the path to the specified archive, or null if it does not exist
     *  in the repository.
     */
    public synchronized String findArchive(ArchiveType type, String name)
    {
        File archiveDir = getArchiveDirectory(type, name);
        return findArchive(archiveDir);
    }

    
    /** Returns the path to the specified archive, or null if it does not exist
     *  in the repository.
     */
    private synchronized String findArchive(File archiveDir)
    {
        String  path = null;
        if (archiveDir != null && archiveDir.exists())
        {
            // there should only be one file in the archive directory
            File[] files = archiveDir.listFiles();
            if ( files.length >= 1 )
            {
                /**
                 * If more than one file, find the non-dir file, which should
                 * be the archive and return the path to that. This is the case
                 * when the the archive we are looking for is a Service Assembly.
                 */
                int i;
                for ( i = 0; i < files.length && files[i].isDirectory () ; i ++ );
                if ( i != files.length )
                {
                    path = files[i].getAbsolutePath();
                }
            }
        }

        return path;
    }
    
    /** Remove the specified archive.
     *  @throws RepositoryException archive not found in repository
     */
    public synchronized void removeArchive(ArchiveType type, String name)
        throws RepositoryException
    {
        String  path;
        String dirPath = null;
        File    file;
        File    dir = null;
        
        path = findArchive(type, name);
        
        if (path == null)
        {
            // The archive does not exist, just clean the parent dir 
            mLog.warning(mStrings.getString(
                    LocalStringKeys.JBI_ADMIN_ARCHIVE_NOT_EXIST, 
                    type.toString(),
                    name));
            dirPath = findArchiveDirectory(type, name);
            
            if ( dirPath != null )
            {
                dir = new File(dirPath);
            }
        }
        else
        {
            // remove the archive zip and parent directory
            file = new File(path);
            dir  = file.getParentFile();
        }
        
        if ( dir != null )
        {
            if (!DirectoryUtil.removeDir(dir.getAbsolutePath()))
            {
                // Failed to completely remove the archive directory from the 
                // repository, but it is marked for deletion on restart by
                // DirectoryUtil.  At this point, we just need to log the failure.
                mLog.fine(mStrings.getString(
                        LocalStringKeys.JBI_ADMIN_FILE_DELETE_FAILED, 
                        dir.getAbsolutePath ()) );
            }
        }
        
        if ( path != null )
        {
            mArchiveCache.remove(path);
            
            /** Fix for Issue 373 : If it is a service assembly then remove
             *  all child service units which may have been cached.
             */
            if ( ArchiveType.SERVICE_ASSEMBLY.equals(type) && dir != null )
            {
                java.util.Set<String> allKeys = mArchiveCache.keySet();
                
                String[] allPaths = new String[allKeys.size()];
                allPaths = allKeys.toArray(allPaths);

                for ( String aPath : allPaths )
                {
                    if ( aPath.startsWith(dir.getAbsolutePath()))
                    {
                        // This is a service unit in the service assembly, remove that
                        mArchiveCache.remove(aPath);
                    }
                }
            }
            
        }
    }
    
    /** Returns a file pointer to the repository temp store. */
    public File getTempStore()
    {
        return mTempStore;
    }
    
    /** Purges the repository, removing all archives. */
    public synchronized void purge()
    {
        mArchiveCache.clear();
        cleanDirectory(mComponentStore, true);
        cleanDirectory(mSharedLibraryStore, true);
        cleanDirectory(mServiceAssemblyStore,true );
        cleanDirectory(mTempStore, true);
    }
    
    /**
     *
     * @return an list of names of entities of type ArchiveType, which are 
     *         stored in the repository and are not marked for deletion.
     */
    public List<String> getArchiveEntityNames(ArchiveType type)
    {
        File dir = getStore(type);
        java.util.ArrayList markedDirs = new java.util.ArrayList();
        
        File[] subDirs = dir.listFiles();
        List<String> entityNames = new java.util.ArrayList();
        for ( File subDir : subDirs )
        {
            if ( subDir.isDirectory() )
            {
                // -- Check if it is marked for deletion
                if ( !DirectoryUtil.isMarked(subDir) )
                {
                    entityNames.add(subDir.getName());
                } else {
                    markedDirs.add(subDir.getName());
                }
            }
        }
        
        // Parse through the entity names, now if there is a marked dir matching
        // the name with an older suffix int, then strip the suffix, otherwise don't.
        java.util.ArrayList updatedEntityNames = new java.util.ArrayList();
        for (String dirName: entityNames){
            
            int dotIndex = dirName.lastIndexOf('.');
            if ( dotIndex != -1 )
            {
                String name = dirName.substring(0, dotIndex);
                String suffix = dirName.substring(dotIndex, dirName.length());

                if (isSuffix(suffix))
                {
                    int suffixedInt = Integer.parseInt(dirName.substring(dotIndex+1, dirName.length()));
                    String prevSuffix = Integer.toString(suffixedInt-1);
                    StringBuffer prevMarkedDirName = new StringBuffer(name);
                    
                    if (suffixedInt > 1 ) {
                        
                        prevMarkedDirName.append(".");
                        prevMarkedDirName.append(prevSuffix);
                    }
                    
                    if (markedDirs.contains(prevMarkedDirName.toString())) {
                        // Strip the suffix
                        dirName = name;
                    }
                }
            }
            
            updatedEntityNames.add(dirName);
        }
        return updatedEntityNames;
    }
    
    /**
     *  Clean up service assembly, shared library, and component directories
     *  which could not be deleted the last time JBI was up.  This problem
     *  is pretty much a Windows-only thing that comes up when an open file
     *  stream has not been closed by a component or a classloader.
     */
    private void removeDeletedArchives()
    {
        DirectoryUtil.removeMarkedDirs(mComponentStore.getAbsolutePath());
        DirectoryUtil.removeMarkedDirs(mSharedLibraryStore.getAbsolutePath());
        DirectoryUtil.removeMarkedDirs(mServiceAssemblyStore.getAbsolutePath());
    }
    
    /** Initialize archive store directories.  The temp directory is cleaned
     *  every time the repository is initialized.
     */
    private void initStores()
        throws RepositoryException
    {
        String jbiInstallRoot = 
                mContext.getEnvironmentContext().getJbiInstanceRoot();
        String jbiHome = 
                mContext.getEnvironmentContext().getJbiInstallRoot();

        try
        {
            mComponentStore         = loadStore(jbiInstallRoot, COMPONENT_STORE_NAME);
            mSharedLibraryStore     = loadStore(jbiInstallRoot, SHARED_LIBRARY_STORE_NAME);
            mTempStore              = loadStore(jbiInstallRoot, TEMP_STORE_NAME);
            mServiceAssemblyStore   = loadStore(jbiInstallRoot, SERVICE_ASSEMBLY_STORE_NAME);   
            
        }
        catch (java.io.IOException ioEx)
        {
            throw new RepositoryException(ioEx);
        }
    }  
    
    
    /** Handles the transfer of archive bits into the repository. */
    private void addToRepository(Archive archive)
        throws RepositoryException
    {
        File        archiveDir;
        File        archiveZip;
        File        archiveFile;
        Calendar    calendar;
        long       timestamp;
        
        // create the archive directory and file
        archiveDir = createArchiveDirectory(archive.getType(), archive.getJbiName());        
        archiveZip = new File(archiveDir, archive.getFileName());
        archiveFile = new File(archive.getPath());
        
        try
        {
            // copy the file content
            transfer(new FileInputStream(archiveFile),
                     new FileOutputStream(archiveZip));
            
            // Copy the modified timestamp.
            archiveZip.setLastModified(timestamp = archiveFile.lastModified());            
            
            // set the new path for the archive
            archive.setPath(archiveZip.getAbsolutePath());

            // perform service assembly post-processing, if necessary
            if (archive.getType().equals(ArchiveType.SERVICE_ASSEMBLY))
            {
                extractServiceUnits(archive);
            }
        }
        catch (java.io.IOException ioEx)
        {
            throw new RepositoryException(ioEx);
        }
        
        // we're done, set the upload timestamp
        calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        archive.setUploadTimestamp(calendar);
        
        // extract the component / shared library archive in the archiveDir
        if ( !archive.getType().equals(ArchiveType.SERVICE_ASSEMBLY))
        {
            File archiveInstallRoot = new File( archiveDir, INSTALL_ROOT);
            
            extractArchive(archiveInstallRoot, archiveZip);
        }
    }
    
    /**
     * Extract the archive contents. 
     *
     * @param parentDir - the parent directory to extract tp
     * @param archiveZip - the archive to extract
     */
    private void extractArchive(File parentDir, File archiveZip)
        throws RepositoryException
    {
        parentDir.mkdir();
        
        try
        {
            JarFactory jarHelper = new JarFactory(parentDir.getAbsolutePath());
            jarHelper.unJar(archiveZip);
        }
        catch (Exception ex)
        {
            throw new RepositoryException(ex);
        }
    }
    
    /** 
     * Removes all files and child directories in the specified directory. 
     * @return false if unable to delete the file
     */
    private boolean cleanDirectory(File dir, boolean all)
    {
        File[]  tmps = dir.listFiles();
        boolean cleaned = true;
        
        for (File tmp : tmps) {
            if (all || (tmp.lastModified() < mRepositoryInitTime)) {
                if (tmp.isDirectory()) {
                    if (!cleanDirectory(tmp, all)) {
                        return (false);
                    }
                }
                if (!tmp.delete()) {
                    mLog.warning(mStrings.getString(LocalStringKeys.JBI_ADMIN_FILE_DELETE_FAILED, tmp.getAbsolutePath()));
                    return false;
                }
            } else {
                cleaned = false;
            }            
        }
        return (cleaned);
     }
    
    /**
     * Reclaim memory, run object finalizers
     */
    private void finalizeDiscardedObjects()
    {
        System.gc();
        System.runFinalization();
    }
    
    
    /** Loads an archive store, creating it if necessary. */
    private File loadStore(String root, String name)
        throws java.io.IOException
    {
        File dir;
        
        dir = new File(root + File.separator + name);
        if (!dir.exists())
        {
            dir.mkdirs();
        }
        
        return dir;
    }
    
    /** Retrieve an archive store based on the archive type. 
     */
    private File getStore(ArchiveType type)
    {
        File store = null;
        
        if (type.equals(ArchiveType.COMPONENT))
        {
            store = mComponentStore;
        }
        else if (type.equals(ArchiveType.SHARED_LIBRARY))
        {
            store = mSharedLibraryStore;
        }
        else if (type.equals(ArchiveType.SERVICE_ASSEMBLY))
        {
            store = mServiceAssemblyStore;
        }        
        else if (type.equals(ArchiveType.SERVICE_UNIT))
        {
            /* This may look weird, but service units are stored in 
             * sub-directories under the service assembly.  The jbiName
             * for an SU archive contains the relative path to the sub-directory
             */
            store = mServiceAssemblyStore;
        }
        return store;
    }
    
    /** Utility method that writes all data read from the input stream to
     *  the output stream.  All streams are closed at the end of this method.
     */
    private void transfer(InputStream input, OutputStream output)
        throws java.io.IOException
    {
        byte[] buf = new byte[BUFFER_SIZE];
           
        for (int count = 0; count > -1; count = input.read(buf))
        {
            output.write(buf, 0, count);
        }

        output.flush();
        input.close();
        output.close();
    }
        
    private void extractServiceUnits(Archive archive)
        throws java.io.IOException, RepositoryException
    {
        ZipFile     zip;
        Iterator    suList;
        ZipEntry    entry;
        File        suDir;
        File        suFile;
        String      suName;
        File        suComponentDir;
        String      targetComponent = null;
        
        zip     = new ZipFile(archive.getPath());
        suList  = archive.listChildren();
        
        // extract each SU into a sub-directory of the parent service assembly
        while (suList.hasNext())
        {
            suName        = (String)suList.next();
            entry         = zip.getEntry(archive.getChildPath(suName));
            suDir   = new File(getArchiveDirectory(ArchiveType.SERVICE_ASSEMBLY, 
                        archive.getJbiName()), suName);
            
            // create the su directory and file (using original file name)
            suDir.mkdir();
            suFile  = new File(suDir, archive.getChildPath(suName));
            transfer(zip.getInputStream(entry), new FileOutputStream(suFile));
            
            // Now extract the SU archive to 
            // ${SA_REPOS_ROOT}/${SA_NAME}/${SU_NAME}/${COMPONENT_NAME}
            java.util.List<com.sun.jbi.management.descriptor.ServiceUnit> 
                sus = archive.getJbiXml(false).getServiceAssembly().getServiceUnit();
            
            for(com.sun.jbi.management.descriptor.ServiceUnit su : sus )
            {
                if ( su.getIdentification().getName().equals(suName))
                {
                    targetComponent = su.getTarget().getComponentName();
                }
            }
            
            suComponentDir = new File(suDir, targetComponent);
            extractArchive(suComponentDir, suFile);
        }
        
        zip.close();
    }
    
    /** Creates a directory in the appropriate repository store for the 
     *  specified archive.  Code calling this method must ensure that the 
     *  archive is not currently active in the repository.  In this case,
     *  active means that an archive directory with this name does not exist, 
     *  or if it does, it is marked for deletion.  In the latter case, the
     *  archive name is made unique by using the original name as a prefix and
     *  adding a ".nn", where nn is a positive integer.
     */
    private File createArchiveDirectory(ArchiveType type, String name)
    {
        File archiveDir;
        
        archiveDir = new File(getStore(type), name);
        
        if (archiveDir.exists())
        {
            // This should only happen if an attempt to remove the archive previously
            // failed and the archive directory was marked for deletion.
            for (int i = 1; archiveDir.exists(); i++)
            {
                archiveDir = new File(getStore(type), name + "." + i );
            }
        }
        
        archiveDir.mkdir();
        return archiveDir;
    }
    
    /** Returns a reference to the directory where an archive is stored, or null 
     *  if archive does not exist in the repository.  This method will 
     *  ignore archive directories which have been marked for deletion.  All
     *  repository methods that search for an archive should use this method 
     *  instead of directly creating a File reference to the archive directory
     *  using the component/SL/SA name.
     */
    private File getArchiveDirectory(ArchiveType type, String name)
    {
        File archiveDir = null;
        String dirName  = null;
        boolean cached = false;
        
        // Finding the archive is a two-stage process:
        //   Step 1: See if the standard pattern match, archive directory = 
        //           archive name, results in a hit.  If that directory exists
        //           and is not marked for deletion, we're done.
        //   Step 2: This step is potentially expensive.  We need to scan
        //           through the existing archive directories of the specified 
        //           type to determine if the archive exists with an offset.
        //           We run into this situation if an archive is added to the
        //           repository after a previous removal failed to complete (but
        //           the archive was marked for deletion).
        
            
        // If we are getting a Service Unit archive then the directory is the
        // Service Assembly Name.
        if ( type.equals(ArchiveType.SERVICE_UNIT) )
        {
            name    = name.replace('\\', '/');
            dirName = extractServiceAssemblyName(name);
        }
        else
        {
            dirName = name;
        }
        
        // First look in the cache
        Archive cachedArchive = searchArchiveCache(type, dirName);
        if ( cachedArchive != null )
        {
        	cached = true;
            archiveDir = new File( cachedArchive.getPath()).getParentFile();
        }
        else 
        {        
            File tmpDir  = new File(getStore(type), dirName);
            if (tmpDir.exists() && !DirectoryUtil.isMarked(tmpDir))
            {
                archiveDir = tmpDir;
            }
            else
            {
                // Find all files which match the name pattern and then look
                // for one that is not deleted.
                // The name pattern is : name{[0-9]*}    
                Pattern namePattern = Pattern.compile(dirName + DIR_SUFFIX_PATTERN);
                File[] dirs = DirectoryUtil.listFiles(getStore(type), namePattern);
                if (dirs != null)
                {
                    for (File file : dirs)
                    {
                        if (file.isDirectory() && !DirectoryUtil.isMarked(file))
                        {
                            archiveDir = file;
                            break;
                        }
                    }
                }
            }
        }
        
        if(!cached) {// calling getAcrchive, to add the archive to cache
        	try {
        		String archpath = findArchive(archiveDir);
        		if(archpath != null)
        			getArchive(archpath); 
        	} catch (Exception ignore) {
        		mLog.log(Level.INFO, " Could not cache archive :{0} for Unit {1}{2}", new Object[]{archiveDir, name, ignore.getMessage()});
        		//ignore.printStackTrace();
        	}
        }
        
        if ( type.equals(ArchiveType.SERVICE_UNIT) )
        {
            // If it is a service unit then the su folder
            // is relative to the service assembly folder we found
            String suName = extractServiceUnitName(name);
            archiveDir = new File(archiveDir, suName);
        }
        
        return archiveDir;
    }
    
    /**
     * Search through the archive cache for a specific archive
     * 
     * @param type
     * @param name
     * @return the Archive instance
     */
    private Archive searchArchiveCache(ArchiveType type, String name)
    {

        java.util.Set<String> keys = mArchiveCache.keySet();
        for(String key : keys)
        {
            Archive archive = mArchiveCache.get(key);
            if ( archive.getType().equals(type) && archive.getJbiName().equals(name))
            {
                return archive;
            }
        }
        return null;
    }
    
    /**
     * @param relativeName - relative name of a service unit
     *
     * @return the service unit name from the relative name. Ex. if the
     *         relative name is ServiceAssembly/ServiceUnit, this will return ServiceUnit.
     *         If the relativeName is not of the pattern <sa-name>/<su-name>, the 
     *         original string is returned.
     */
    private String extractServiceUnitName(String relativeName)
    {
        String suName = relativeName;
        int fSlash = relativeName.indexOf('/');
        if ( fSlash != -1 )
        {
            suName = relativeName.substring(fSlash + 1, relativeName.length());
        }
        return suName;
    }
    
    
    /**
     * @param relativeName - relative name of a service unit
     *
     * @return the service assembly name from the relative name. Ex. if the
     *         relative name is ServiceAssembly/ServiceUnit, this will return 
     *         ServiceAssembly.
     *
     *         If the relativeName is not of the pattern <sa-name>/<su-name>, the 
     *         original string is returned.
     */
    private String extractServiceAssemblyName(String relativeName)
    {
        String saName = relativeName;
        int fSlash = relativeName.indexOf('/');
        if ( fSlash != -1 )
        {
            saName = relativeName.substring(0, fSlash);
        }
        return saName;   
    }
    
    /**
     * If the name string passed in contains a suffix matching the DIR_SUFFIX_PATTERN
     * the suffix is removed and result is returned.
     *
     * @param dirName - possibly suffixed directory name
     * @return the name with the suffix ( if any ) removed.
     */
    private String stripSuffix(String dirName)
    {
        int dotIndex = dirName.lastIndexOf('.');
        if ( dotIndex != -1 )
        {
            String name = dirName.substring(0, dotIndex);
            String suffix = dirName.substring(dotIndex, dirName.length());

            if (isSuffix(suffix))
            {
                return name;
            }
        }
        return dirName;
    }
    
    /**
     * @return true if the suffix matches the DIR_SUFFIX_PATTERN
     */
    private boolean isSuffix(String suffix)
    {
        Pattern suffixPattern = Pattern.compile(DIR_SUFFIX_PATTERN);
        return suffixPattern.matcher(suffix).matches();
    }

}
