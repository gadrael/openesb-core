/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UpdaterImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  UpdaterImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.registry.xml;

import java.io.File;

import java.math.BigInteger;

import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.system.ManagementException;
import com.sun.jbi.management.util.LockManager;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceAssemblyInfo;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.ServiceUnitState;
import java.util.logging.Level;

import javax.xml.bind.JAXBException;


/**
 * This class encapsulates queries which are common to 
 * all targets ( domain / server / cluster ).
 *
 * @author Sun Microsystems, Inc.
 */
public class UpdaterImpl
    implements com.sun.jbi.management.registry.Updater
{
 
    private Jbi               mJbiRegistry;
    private RegistryImpl      mRegistry;
    private boolean           mValidate;
    private ManagementContext mMgtCtx;
    private Logger            mLogger;
    private StringTranslator  mTranslator;
    private GenericQueryImpl  mGenQuery;
    private ObjectFactory     mObjectFactory;
    private String            mThisTarget;
    private LockManager       mRegObjLM;
    
    public UpdaterImpl(Jbi jbi, ManagementContext mgtCtx, boolean validate, 
        RegistryImpl registry)
        throws RegistryException
    {
        mJbiRegistry = jbi;
        mRegistry = registry;
        mValidate = validate;
        mMgtCtx = mgtCtx;
        mLogger = mMgtCtx.getLogger();
        mTranslator = 
            mMgtCtx.getEnvironmentContext().getStringTranslator("com.sun.jbi.management");
        
        mGenQuery = (GenericQueryImpl) mRegistry.getGenericQuery();
        
        mObjectFactory = new ObjectFactory();
        mRegObjLM  = registry.getRegistryObjectLockManager();        
        mThisTarget = mMgtCtx.getEnvironmentContext().getPlatformContext().getTargetName();
    }

    /*--------------------------------------------------------------------------------*\
     *                          Add / Remove Entities                                 *
    \*--------------------------------------------------------------------------------*/
    
    /**
     * Add a component to the runtime target
     *
     * @param componentInfo - component instance
     * @throws RegistryException on errors
     */
    public void addComponent(ComponentInfo componentInfo) 
        throws RegistryException
    {
        addComponent(mThisTarget, componentInfo);
    }
    
    /**
     * Add a domain component
     * 
     * @param componentName - name of the component
     * @param fileName      - component file name
     */
    public void addComponent(String componentName, String fileName, Calendar timestamp)
        throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            Components compType = mJbiRegistry.getComponents();

            if ( compType == null )
            {
                compType = mObjectFactory.createComponents();
                mJbiRegistry.setComponents(compType);
            }

            DomainComponentType component = mObjectFactory.createDomainComponentType();
            component.setName(componentName);
            component.setFileName(fileName);
            component.setTimestamp(java.math.BigInteger.valueOf(timestamp.getTimeInMillis()));
            compType.getComponent().add(component);
        }
        catch (Exception jex)
        {
            mRegObjLM.releaseWriteLock();
            throw new RegistryException(jex);
        }
        commit();
    }
    
    /**
     * Add a component to a given target. If target = DOMAIN, then a simple component 
     * entry is created with the component name. If the target = SERVER / CLUSTER
     * a component-ref is added.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * not considered when targetType = DOMAIN.
     * @param componentInfo - component instance
     * @throws RegistryException on errors
     */
    public void addComponent(String targetName, ComponentInfo componentInfo) 
        throws RegistryException
    {
        boolean updated = false;
        mRegObjLM.acquireWriteLock();
        try
        {
            if ( mGenQuery.isTargetServer(targetName) )
            {
                List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

                for(InstalledComponentsListType server : servers)
                {
                    if ( server.getNameRef().equals(targetName) )
                    {
                        addComponentRef(componentInfo, server);
                        updated = true;
                        break;
                    }
                }

            }
            else if ( mGenQuery.isTargetCluster(targetName) )
            {
                List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();

                for(InstalledComponentsListType cluster : clusters)
                {
                    if ( cluster.getNameRef().equals(targetName) )
                    {
                        addComponentRef(componentInfo, cluster);
                        updated = true;
                        break;
                    }
                }
            }
            else
            {
                throw new RegistryException(
                    mTranslator.getString(LocalStringKeys.JBI_ADMIN_UNKNOWN_TARGET, targetName));
            }
        }
        catch(RegistryException re)
        {
            mRegObjLM.releaseWriteLock();
            throw re;
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        if ( updated )
        {
            commit();
        }
    }
    
    /**
     * Remove a component from the runtime's target
     *
     * @param componentName - component name
     * @throws RegistryException on errors
     */
    public void removeComponent(String componentName) 
        throws RegistryException
    {
        removeComponent(mThisTarget, componentName);
    }
    
    /**
     * Remove a component from a given target. 
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param componentName - component name
     * @throws RegistryException on errors
     */
    public void removeComponent(String targetName, String componentName) 
        throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            if ( mGenQuery.isTargetDomain(targetName))
            {
               deleteComponent(componentName);     
               mGenQuery.removeComponentFromCache(componentName);
            }
            else if ( mGenQuery.isTargetServer(targetName) )
            {
                List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

                for(InstalledComponentsListType server : servers)
                {
                    if ( server.getNameRef().equals(targetName) )
                    {
                        removeComponentRef(componentName, server);
                    }
                }
            }
            else if ( mGenQuery.isTargetCluster(targetName) )
            {
                List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();

                for(InstalledComponentsListType cluster : clusters)
                {
                    if ( cluster.getNameRef().equals(targetName) )
                    {
                        removeComponentRef(componentName, cluster);
                    }
                }
            }
            else
            {
                throw new RegistryException(
                    mTranslator.getString(LocalStringKeys.JBI_ADMIN_UNKNOWN_TARGET, targetName));
            }
        }
        catch(RegistryException re)
        {
            mRegObjLM.releaseWriteLock();
            throw re;
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        commit();
    }
    
    /** 
     * Add a shared library to he runtime's target
     *
     * @param ComponentInfo - shared library info
     * @throws RegistryException on errors
     */
    public void addSharedLibrary(ComponentInfo sharedLibraryInfo)
            throws RegistryException
    {
        addSharedLibrary(mThisTarget, sharedLibraryInfo);
    }
    
    /**
     * Add a domain shared library
     * 
     * @param slName        - name of the shared library
     * @param fileName      - component file name
     */
    public void addSharedLibrary(String slName, String fileName, Calendar timestamp)
        throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            SharedLibraries slType = mJbiRegistry.getSharedLibraries();

            if ( slType == null )
            {
                slType = mObjectFactory.createSharedLibraries();
                mJbiRegistry.setSharedLibraries(slType);
            }

            DomainSharedLibraryType sharedLibrary = mObjectFactory.createDomainSharedLibraryType();
            sharedLibrary.setName(slName);
            sharedLibrary.setFileName(fileName);
            sharedLibrary.setTimestamp(java.math.BigInteger.valueOf(timestamp.getTimeInMillis()));
            slType.getSharedLibrary().add(sharedLibrary);
        }
        catch (Exception jex)
        {
            mRegObjLM.releaseWriteLock();
            throw new RegistryException(jex);
        }
        commit();
    }
    
    /** 
     * Add a shared library to a given target. If target = DOMAIN, then a simple  
     * entry is created with the library name. If the target = SERVER / CLUSTER
     * a shared-library-ref is added.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param ComponentInfo - shared library info
     * @throws RegistryException on errors
     */
    public void addSharedLibrary(String targetName, ComponentInfo sharedLibraryInfo)
            throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            if ( mGenQuery.isTargetDomain(targetName))
            {
               addSharedLibrary(sharedLibraryInfo.getName(), "unknown", null);     
            }
            else if ( mGenQuery.isTargetServer(targetName) )
            {
                List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

                for(InstalledComponentsListType server : servers)
                {
                    if ( server.getNameRef().equals(targetName) )
                    {
                        addSharedLibraryRef(sharedLibraryInfo, server);
                    }
                }
            }
            else if ( mGenQuery.isTargetCluster(targetName) )
            {

                 List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();

                for(InstalledComponentsListType cluster : clusters)
                {
                    if ( cluster.getNameRef().equals(targetName) )
                    {
                        addSharedLibraryRef(sharedLibraryInfo, cluster);
                    }
                }
            }
            else
            {
                throw new RegistryException(
                    mTranslator.getString(LocalStringKeys.JBI_ADMIN_UNKNOWN_TARGET, targetName));
            }
        }
        catch(RegistryException re)
        {
            mRegObjLM.releaseWriteLock();
            throw re;
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        commit();
    }
    
    /** 
     * Remove a shared library from the runtimes target
     *
     * @param sharedLibraryName - shared library name
     * @throws RegistryException on errors
     */
    public void removeSharedLibrary(String sharedLibraryName)
            throws RegistryException
    {
        removeSharedLibrary(mThisTarget, sharedLibraryName);
    }
    
    /** 
     * Remove a shared library from a given target.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param sharedLibraryName - shared library name
     * @throws RegistryException on errors
     */
    public void removeSharedLibrary(String targetName, String sharedLibraryName)
            throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            if ( mGenQuery.isTargetDomain(targetName))
            {
               deleteSharedLibrary(sharedLibraryName);     
               mGenQuery.removeSharedLibraryFromCache(sharedLibraryName);
            }
            else if ( mGenQuery.isTargetServer(targetName) )
            {
                List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

                for(InstalledComponentsListType server : servers)
                {
                    if ( server.getNameRef().equals(targetName) )
                    {
                        removeSharedLibraryRef(sharedLibraryName, server);
                    }
                }
            }
            else if ( mGenQuery.isTargetCluster(targetName) )
            {
                List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();

                for(InstalledComponentsListType cluster : clusters)
                {
                    if ( cluster.getNameRef().equals(targetName) )
                    {
                        removeSharedLibraryRef(sharedLibraryName, cluster);
                    }
                }
            }
            else
            {
                throw new RegistryException(
                    mTranslator.getString(LocalStringKeys.JBI_ADMIN_UNKNOWN_TARGET, targetName));
            }
        }
        catch(RegistryException re)
        {
            mRegObjLM.releaseWriteLock();
            throw re;
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        commit();
    }
    
    /** 
     * Add a service assembly to the runtime's target
     *
     * @param saName - service assembly name 
     * @throws RegistryException on errors
     */
    public void addServiceAssembly(String saName)
            throws RegistryException
    {
        addServiceAssembly(mThisTarget, saName);
    }

    /**
     * Add a service assembly to the domain.
     *
     * @param saName - service assembly name
     * @param fileName - service assembly archive name
     * @throws RegistryException on errors
     */
    public void addServiceAssembly(String saName, String fileName, Calendar timestamp)
            throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            ServiceAssemblies saType = mJbiRegistry.getServiceAssemblies();

            if ( saType == null )
            {
                saType = mObjectFactory.createServiceAssemblies();
                mJbiRegistry.setServiceAssemblies(saType);
            }

            DomainEntityType serviceAssembly = mObjectFactory.createDomainEntityType();
            serviceAssembly.setName(saName);
            serviceAssembly.setFileName(fileName);
            serviceAssembly.setTimestamp(java.math.BigInteger.valueOf(timestamp.getTimeInMillis()));
            saType.getServiceAssembly().add(serviceAssembly);
        }
        catch (Exception jex)
        {
            mRegObjLM.releaseWriteLock();
            throw new RegistryException(jex);
        }
        commit();
    }
    
    /** 
     * Add a service assembly to a given target. If target = DOMAIN, then a simple  
     * entry is created with the assembly name. If the target = SERVER / CLUSTER
     * a service-assembly--ref is added.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param saName - service assembly name 
     * @throws RegistryException on errors
     */
    public void addServiceAssembly(String targetName, String saName)
            throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            if ( mGenQuery.isTargetDomain(targetName))
            {
               addServiceAssembly(saName, "unknown", null);     
            }
            else if ( mGenQuery.isTargetServer(targetName) )
            {
                List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

                for(InstalledComponentsListType server : servers)
                {
                    if ( server.getNameRef().equals(targetName) )
                    {
                        addServiceAssemblyRef(saName, server);   
                    }
                }
            }
            else if ( mGenQuery.isTargetCluster(targetName) )
            {

                 List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();

                for(InstalledComponentsListType cluster : clusters)
                {
                    if ( cluster.getNameRef().equals(targetName) )
                    {
                        addServiceAssemblyRef(saName, cluster);
                    }
                }
            }
            else
            {
                throw new RegistryException(
                    mTranslator.getString(LocalStringKeys.JBI_ADMIN_UNKNOWN_TARGET, targetName));
            }
        }
        catch(RegistryException re)
        {
            mRegObjLM.releaseWriteLock();
            throw re;
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        commit();
    }
    
    /** 
     * Remove a service assembly from the runtime target
     *
     * @param saName - service assembly name
     * @throws RegistryException on errors
     */
    public void removeServiceAssembly(String saName)
            throws RegistryException
    {
        removeServiceAssembly(mThisTarget, saName);
    }
    
    /** 
     * Remove a service assembly from a given target.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param saName - service assembly name
     * @throws RegistryException on errors
     */
    public void removeServiceAssembly(String targetName, String saName)
            throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            if ( mGenQuery.isTargetDomain(targetName))
            {
               deleteServiceAssembly(saName);     
               mGenQuery.removeServiceAssemblyFromCache(saName);
            }
            else if ( mGenQuery.isTargetServer(targetName) )
            {
                List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

                for(InstalledComponentsListType server : servers)
                {
                    if ( server.getNameRef().equals(targetName) )
                    {
                        removeServiceAssemblyRef(saName, server);
                    }
                }
            }
            else if ( mGenQuery.isTargetCluster(targetName) )
            {
                List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();

                for(InstalledComponentsListType cluster : clusters)
                {
                    if ( cluster.getNameRef().equals(targetName) )
                    {
                        removeServiceAssemblyRef(saName, cluster);
                    }
                }
            }
            else
            {
                throw new RegistryException(
                    mTranslator.getString(LocalStringKeys.JBI_ADMIN_UNKNOWN_TARGET, targetName));
            }
        }
        catch(RegistryException re)
        {
            mRegObjLM.releaseWriteLock();
            throw re;
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }          
        commit();
    }
    
    /**
     * @param serverNameRef - server name to be added
     */
    public void addServer(String serverNameRef) 
        throws RegistryException
    {
        try
        {
            if ( !mGenQuery.isTargetServer(serverNameRef) && !mGenQuery.isTargetCluster(serverNameRef) )
            {
                mRegObjLM.acquireWriteLock();
                try
                {
                    ServerListType servers = mJbiRegistry.getServers();

                    if ( servers == null )
                    {
                        servers = mObjectFactory.createServerListType();
                        mJbiRegistry.setServers(servers);
                    }

                    InstalledComponentsListType server = mObjectFactory.createInstalledComponentsListType();
                    server.setNameRef(serverNameRef);

                    mJbiRegistry.getServers().getServer().add(server);
                }
                catch(RuntimeException rte)
                {
                    mRegObjLM.releaseWriteLock();
                    throw rte;
                }
                commit();
            }
            else
            {
                throw new RegistryException(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_DUPLICATE_TARGET, serverNameRef ));
            }
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }
    }
    
    /**
     * @param serverNameRef - server name to be removed
     */
    public void removeServer(String serverNameRef) 
        throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            InstalledComponentsListType serverToDel = null;

            List<InstalledComponentsListType> 
                servers = mJbiRegistry.getServers().getServer();

            for (InstalledComponentsListType server : servers)
            {
                if ( server.getNameRef().equals(serverNameRef) )
                {
                    serverToDel = server;
                }
            }

            if ( serverToDel != null )
            {
                servers.remove(serverToDel);
            }
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }

        commit();
    }
    
    /**
     * @param clusterNameRef - cluster name to be added
     */
    public void addCluster(String clusterNameRef) 
        throws RegistryException
    {
        try
        {
            if ( !mGenQuery.isTargetServer(clusterNameRef) && !mGenQuery.isTargetCluster(clusterNameRef) )
            {
                mRegObjLM.acquireWriteLock();
                try
                {
                    ClusterListType clusters = mJbiRegistry.getClusters();

                    if ( clusters == null )
                    {
                        clusters = mObjectFactory.createClusterListType();
                        mJbiRegistry.setClusters(clusters);
                    }

                    InstalledComponentsListType 
                        cluster = mObjectFactory.createInstalledComponentsListType();
                    cluster.setNameRef(clusterNameRef);
                    mJbiRegistry.getClusters().getCluster().add(cluster);
                }
                catch(RuntimeException rte)
                {
                    mRegObjLM.releaseWriteLock();
                    throw rte;
                }
                commit();
            }
            else
            {
                throw new RegistryException(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_DUPLICATE_TARGET, clusterNameRef ));
            }
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }
    }
    
    /**
     * @param clusterNameRef - cluster name to be removed
     */
    public void removeCluster(String clusterNameRef) 
        throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            InstalledComponentsListType clusterToDel = null;

            List<InstalledComponentsListType> 
                clusters = mJbiRegistry.getClusters().getCluster();

            for (InstalledComponentsListType cluster : clusters)
            {
                if ( cluster.getNameRef().equals(clusterNameRef) )
                {
                    clusterToDel = cluster;
                }
            }

            if ( clusterToDel != null )
            {
                clusters.remove(clusterToDel);
            }
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }

        commit();
    }
    
    
    /*--------------------------------------------------------------------------------*\
     *                   Operations to update entries                                 *
    \*--------------------------------------------------------------------------------*/
    
    /**
     * Set the file name for the domain component.
     */
    public void setComponentFileName(String fileName, String componentName)
        throws RegistryException
    {
        
        DomainEntityType comp = mGenQuery.getComponent(componentName);
        
        if ( comp != null )
        {
            mRegObjLM.acquireWriteLock();
            try
            {
                comp.setFileName(fileName);
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
    }
    
    /**
     * Set the file name for the domain service assembly.         
     */
    public void setSharedLibraryFileName(String fileName, String slName)
        throws RegistryException    
    {
        DomainEntityType sl = mGenQuery.getSharedLibrary(slName);
        
        if ( sl != null )
        {
            mRegObjLM.acquireWriteLock();
            try
            {
                sl.setFileName(fileName);
            }
            catch (RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
    }

    
    /**
     * Set the file name for the domain service assembly
     */
    public void setServiceAssemblyFileName(String fileName, String saName)
        throws RegistryException
    {
        DomainEntityType sa = mGenQuery.getServiceAssembly(saName);
        
        if ( sa != null )
        {
            mRegObjLM.acquireWriteLock();
            try
            {
                sa.setFileName(fileName);
            }
            catch (RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
    }
    
    /**
     * Set the state of a component for the target at runtime.
     *
     * @param componentName - component name
     * @param state  - the state to set. 
     * @throws RegistryException on errors
     */
    public void setComponentState(ComponentState state, String componentName) 
        throws RegistryException
    {
        setComponentState(mThisTarget, state, componentName);
    }
    /**
     * Set the state of a component in a server/cluster.
     *
     * @param targetName - either the server-name or cluster-name
     * @param componentName - component name
     * @param state  - the state to set. 
     * @throws RegistryException on errors
     */
    public void setComponentState(String targetName,
            ComponentState state, String componentName) throws RegistryException
    {
        ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

        if ( comp != null )
        {
            mRegObjLM.acquireWriteLock();
            try
            {
                mLogger.log(Level.FINEST, "Setting component {0} to {1} for target {2}", new Object[]{componentName, state.toString(), targetName});
                comp.setState(LifeCycleStatusEnum.fromValue(state.toString()));
            }
            catch (RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
    }
    
    /**
     * Set the properties of a component in a server/cluster.
     *
     * @param targetName - either the server-name or cluster-name, this operation is 
     *        a no-op  when target = domain.
     * @param componentName - component name
     * @param props  - the properties to set. 
     * @throws RegistryException on errors
     */
    public void setComponentProperties(String targetName,
            Map<String, String> props, String componentName) throws RegistryException
    {
        try
        {
            ComponentRefType comp = mGenQuery.getComponent(componentName,
                targetName == null ? mThisTarget : targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                List<PropertyType> properties = comp.getProperty();
                int numProps = props.size();

                java.util.Iterator itr = props.keySet().iterator();

                while ( itr.hasNext() )
                {
                    PropertyType prop = mObjectFactory.createPropertyType();

                    String key = (String) itr.next();
                    prop.setName(key);
                    prop.setValue(props.get(key));
                }
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }
    }
        
    /**
     * Set the state of a ServiceUnit for the runtime target.
     *
     * @param componentName - component name
     * @param suName - service unit name
     * @param state  - the state to set. 
     * @throws RegistryException on errors
     */
    public void setServiceUnitState(
        ServiceUnitState state, String componentName, String suName) 
                throws RegistryException
    {
        setServiceUnitState(mThisTarget, state, componentName, suName); 
    }
                
    /**
     * Set the state of a ServiceUnit in a server / cluster.
     *
     * @param targetName - either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param componentName - component name
     * @param suName - service unit name
     * @param state  - the state to set. 
     * @throws RegistryException on errors
     */
    public void setServiceUnitState(String targetName,
        ServiceUnitState state, String componentName, String suName) 
                throws RegistryException
    {
        ComponentRefType compRef = mGenQuery.getComponent(componentName, targetName);
        boolean updated = false;
        
        mRegObjLM.acquireWriteLock();
        try
        {
            ServiceUnitListType suListType = compRef.getServiceUnits();

            if ( suListType != null )
            {
                List<ServiceUnitType> suList = suListType.getServiceUnit();

                for ( ServiceUnitType su : suList )
                {
                    if ( su.getName().equals(suName))
                    {
                        su.setState(LifeCycleStatusEnum.fromValue(state.toString()));
                    }
                }
                updated = true;
            }
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        
        if ( updated )
        {
            commit();
        }
        else
        {
            mRegObjLM.releaseWriteLock();
        }
    }

    /**
     * Set the state of a ServiceAssembly for the runtime target.
     *
     * @param state  - the state to set.
     * @param saName - the service assembly name
     * @throws RegistryException on errors
     */
    public void setServiceAssemblyState(
        ServiceAssemblyState state, String saName)
                throws RegistryException
    {
        setServiceAssemblyState(mThisTarget, state, saName);   
    }

    /**
     * Set the state of a ServiceAssembly in a server / cluster.
     *
     * @param targetName - either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param saName - service assembly name
     * @param state  - the state to set.
     * @throws RegistryException on errors
     */
    public void setServiceAssemblyState(String targetName,
        ServiceAssemblyState state, String saName)
                throws RegistryException 
    {
        ServiceAssemblyRefType sa = mGenQuery.getServiceAssembly(saName, targetName);

        if ( sa != null )
        {
            mRegObjLM.acquireWriteLock();
            try
            {
                mLogger.log(Level.FINEST, "Setting service assembly {0} state to {1} for target {2}", new Object[]{saName, state.toString(), targetName});
                sa.setState(LifeCycleStatusEnum.fromValue(state.toString()));
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        } 
    }  

    /**
     * Add a ServiceUnit to a Component for the runtime target
     *
     * @param suInfo - ServiceUnitInfo
     * @param componentName - component name
     * @throws RegistryException on errors
     */
    public void addServiceUnitToComponent(
        String componentName, ServiceUnitInfo suInfo) 
            throws RegistryException
    {
        addServiceUnitToComponent(mThisTarget, componentName, suInfo);
    }
    
    /**
     * Add a ServiceUnit to a Component
     *
     * @param suInfo - ServiceUnitInfo
     * @param targetName - either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param componentName - component name
     * @throws RegistryException on errors
     */
    public void addServiceUnitToComponent(String targetName,
        String componentName, ServiceUnitInfo suInfo) 
                throws RegistryException
    {
        try
        {
            ComponentRefType compRef = mGenQuery.getComponent(componentName, targetName);
            if ( compRef == null )
            {
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_COMPONENT_MISSING_IN_REGISTRY,
                    componentName);
                mLogger.warning(errMsg);
                throw new RegistryException(errMsg);
            }

            mRegObjLM.acquireWriteLock();
            try
            {
                ServiceUnitListType suListType = compRef.getServiceUnits();

                if ( suListType == null )
                {
                    suListType = mObjectFactory.createServiceUnitListType();
                    compRef.setServiceUnits(suListType);
                }
                List<ServiceUnitType> suList = suListType.getServiceUnit();

                for ( ServiceUnitType su : suList )
                {
                    if ( su.getName().equals(suInfo.getName()))
                    {
                        throw new RegistryException(mTranslator.getString(
                            LocalStringKeys.JBI_ADMIN_CANNOT_ADD_DUPLICATE_SU, suInfo.getName(),
                                componentName, targetName));
                    }
                }
                suList.add(ObjectTranslator.getJaxbServiceUnitType(suInfo));
            }
            catch(RegistryException re)
            {
                mRegObjLM.releaseWriteLock();
                throw re;
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();

        }
        catch(JAXBException jex)
        {
            throw new RegistryException(jex);
        }
    }
    
    /**
     * Remove a ServiceUnit from a Component for the runtime target
     *
     * @param suName - ServiceUnitInfo name
     * @param componentName - component name
     * @throws RegistryException on errors
     */
    public void removeServiceUnitFromComponent(
        String componentName, String suName)
            throws RegistryException
    {
        removeServiceUnitFromComponent(mThisTarget, componentName, suName);
    }
    
    /**
     * Remove a ServiceUnit from a Component
     *
     * @param suName - ServiceUnitInfo name
     * @param targetName - the server-name or cluster-name.
     * @param componentName - component name
     * @throws RegistryException on errors
     */
    public void removeServiceUnitFromComponent(String targetName,
        String componentName, String suName) 
            throws RegistryException
    {
        ComponentRefType compRef = mGenQuery.getComponent(componentName, targetName);
        boolean updated = false;
        
        mRegObjLM.acquireWriteLock();
        try
        {
            ServiceUnitListType suListType = compRef.getServiceUnits();

            if ( suListType != null )
            {
                ServiceUnitType suToDel = null;
                List<ServiceUnitType> suList = suListType.getServiceUnit();

                for ( ServiceUnitType su : suList )
                {
                    if ( su.getName().equals(suName))
                    {
                        suToDel = su;
                    }
                }
                suList.remove(suToDel);
                updated = true;
            }
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        if ( updated)
        {
            commit();
        }
        else
        {
            mRegObjLM.releaseWriteLock();
        }
    }
    /*--------------------------------------------------------------------------------*\
     *                   Set configuration attributes                                 *
    \*--------------------------------------------------------------------------------*/
     
    /**
     * Set the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setAttribute(ConfigurationCategory type, String name, String value)
        throws RegistryException
    {
        setAttribute(mThisTarget, type, name, value);
    }
    
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setAttribute(String targetName, ConfigurationCategory type, String name, String value)
        throws RegistryException
    {
        /**
         * Don't persist the com.sun.jbi log level in the jbi registry
         */
        if ( com.sun.jbi.platform.PlatformContext.JBI_LOGGER_NAME.equals(name) )
        {
            return;
        }
        
        ObjectFactory factory = new ObjectFactory();
        mRegObjLM.acquireWriteLock();
        
        try
        {
            // If attribute is already overriden then just set it,
            // If not then might need to add a new target config/category/property
            if ( mGenQuery.isAttributeOverriden(targetName, type, name) )
            {
                ConfigCategoryType category = mGenQuery.getConfigCategory(targetName, type);

                List<PropertyType> props = category.getProperty();

                for ( PropertyType prop : props )
                {
                    if ( prop.getName().equals(name) )
                    {
                        prop.setValue(value);
                    }
                }
            }
            else
            {
                // Add the attribute ( and config, config-type if need be )
               ConfigType configType = mGenQuery.getConfig(targetName);
               if ( configType == null )
               {
                   configType = factory.createConfigType();
                   configType.setName(mGenQuery.getConfigName(targetName));
                   if (mJbiRegistry.getConfigs() == null)
                   {
                       mJbiRegistry.setConfigs(factory.createConfigs());
                   }
                   mJbiRegistry.getConfigs().getConfig().add(configType);
               }

               ConfigCategoryType category = mGenQuery.getConfigCategory(targetName, type);

               if ( category == null )
               {
                   // Add the category
                   List<ConfigCategoryType> categoryList =  configType.getConfigType();
                   category = factory.createConfigCategoryType();
                   category.setCategory(ConfigTypeEnum.fromValue(type.toString()));
                   categoryList.add(category);
               }
               List<PropertyType> props = category.getProperty();
               PropertyType newProp = factory.createPropertyType();
               newProp.setName(name);
               newProp.setValue(value);
               props.add(newProp);   
            }
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        commit();
    }
    
    /**
     * Delete the named attribute from the category if it exists, for the implicit target
     * The attribute is deleted only if it is overriden for the target, if target is "domain"
     * the attribute is not deleted.
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     */
    public void deleteAttribute(ConfigurationCategory type, String name)
        throws RegistryException
    {
        deleteAttribute(mThisTarget, type, name);
    }
    
    /**
     * Delete the named attribute from the category if it exists.
     * The attribute is deleted only if it is overriden for the target, if target is "domain"
     * the attribute is not deleted.
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     */
    public void deleteAttribute(String targetName, ConfigurationCategory type, String name)
        throws RegistryException
    {
        if ( !(mGenQuery.isTargetServer(targetName) || mGenQuery.isTargetCluster(targetName)))
        {
            // nothing to do for target="domain".
            return;
        }

        ObjectFactory factory = new ObjectFactory();
        mRegObjLM.acquireWriteLock();
        
        try
        {
            // If attribute is overriden then only delete it,
            if ( mGenQuery.isAttributeOverriden(targetName, type, name) )
            {
                ConfigCategoryType category = mGenQuery.getConfigCategory(targetName, type);

                List<PropertyType> props = category.getProperty();
                PropertyType propToDelete = null;
                for ( PropertyType prop : props )
                {
                    if ( prop.getName().equals(name) )
                    {
                        propToDelete = prop;
                        break;
                    }
                }
                if ( propToDelete != null )
                {
                    props.remove(propToDelete);
                }
            }
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        commit();
    }
    
    /**
     * This method is used to set the upgrade-number attribute in the domain 
     * level entry for a component
     * @param componentName the component name
     * @param upgradeNumber the update number
     * @throws RegistryException if the update number could not be set
     */
    public void setComponentUpgradeNumber(String componentName, java.math.BigInteger upgradeNumber)
        throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            Components compType = mJbiRegistry.getComponents();

            if ( compType != null )
            {
                List<DomainComponentType> components = compType.getComponent();
                for (DomainComponentType component : components  )
                {
                    if ( component.getName().equals(componentName))
                    {
                        component.setUpgradeNumber(upgradeNumber);
                        break;
                    }
                }
            }
        }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        commit();
    }

   /**
    * This method is used to correct timestamps on schemaorg_apache_xmlbeans.system components on the DAS if they
    * are not already set. Typically this only happens the first time an installation
    * starts after install or upgrade.
    */
    public void correctTimestamps()
        throws RegistryException
    {
        mRegObjLM.acquireWriteLock();
        try
        {
            //
            //  Check components first.
            //
            Components compType = mJbiRegistry.getComponents();

            if ( compType != null )
            {
                List<DomainComponentType> components = compType.getComponent();
                for (DomainComponentType component : components  )
                {
                    if (component.getTimestamp().equals(BigInteger.valueOf(0)))
                    {
                        long        timestamp = 0;
                        String       filename;
                        File         file;
                        
                        filename = mRegistry.getRepository().getArchive(ArchiveType.COMPONENT, component.getName()).getPath();
                        file = new File(filename);
                        timestamp = file.lastModified();
                        component.setTimestamp(BigInteger.valueOf(timestamp));
                    }
                }
            }
            
            //
            //  Next check shared libraries.
            //
            SharedLibraries slType = mJbiRegistry.getSharedLibraries();
            if ( slType != null )
            {
                List<DomainSharedLibraryType> libraries = slType.getSharedLibrary();
                for (DomainSharedLibraryType library : libraries  )
                {
                    if (library.getTimestamp().equals(BigInteger.valueOf(0)))
                    {
                        long        timestamp = 0;
                        String       filename;
                        File         file;
                        
                        filename = mRegistry.getRepository().getArchive(ArchiveType.SHARED_LIBRARY, library.getName()).getPath();
                        file = new File(filename);
                        timestamp = file.lastModified();
                        library.setTimestamp(BigInteger.valueOf(timestamp));
                    }
                }
            }
       }
        catch(RuntimeException rte)
        {
            mRegObjLM.releaseWriteLock();
            throw rte;
        }
        commit();
        
    }
    
    /*--------------------------------------------------------------------------------*\
     *                   Component configuration ops                                  *
    \*--------------------------------------------------------------------------------*/
    
    /*--------------------------------------------------------------------------------*\
     *                  Set static component configuration                            *
    \*--------------------------------------------------------------------------------*/
    /**
     * Set the value of a configuration attribute for a component installed on a target
     * for the runtime target. 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @param componentName - identification for the component
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setComponentAttribute(String componentName, String name, String value)
        throws RegistryException
    {
        setComponentAttribute(componentName, mThisTarget, name, value);
    }
    
    /**
     * Set the value of a configuration attribute for a component installed on a target,
     *
     * @param targetName - target instance/cluster name.
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @param componentName - identification for the component
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setComponentAttribute(String componentName, String targetName, 
        String name, String value)
        throws RegistryException
    {
        try
        {
            ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                ComponentConfigType ccfg = comp.getComponentConfig();
                if ( ccfg == null )
                {
                    ObjectFactory factory = new ObjectFactory();
                    ccfg = factory.createComponentConfigType();
                    comp.setComponentConfig(ccfg);
                }
                List<PropertyType> properties = ccfg.getProperty();

                List<PropertyType> newProperties = new ArrayList<PropertyType>();

                PropertyType prop = mObjectFactory.createPropertyType();
                prop.setName(name);
                prop.setValue(value);

                newProperties.add(prop);

                merge(properties, newProperties);
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }        
    }
    
    /**
     * Set the value of a configuration attribute for a component installed on a target
     * for the runtime target. 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @param componentName - identification for the component
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setComponentAttributes(String componentName, Properties props)
        throws RegistryException
    {
        setComponentAttributes(componentName, mThisTarget, props);
    }
    
    /**
     * Set the value of a configuration attribute for a component installed on a target,
     *
     * @param targetName - target instance/cluster name.
     * @param properties - the properties to set
     * @param componentName - identification for the component
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setComponentAttributes(String componentName, String targetName, 
        Properties props)
        throws RegistryException
    {
        try
        {
            ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                ComponentConfigType ccfg = comp.getComponentConfig();
                if ( ccfg == null )
                {
                    ObjectFactory factory = new ObjectFactory();
                    ccfg = factory.createComponentConfigType();
                    comp.setComponentConfig(ccfg);
                }
                
                List<PropertyType> properties = ccfg.getProperty();
                int numProps = props.size();

                List<PropertyType> newProperties = new ArrayList<PropertyType>();
                java.util.Iterator itr = props.keySet().iterator();

                while ( itr.hasNext() )
                {
                    PropertyType prop = mObjectFactory.createPropertyType();

                    String key = (String) itr.next();
                    prop.setName(key);
                    prop.setValue((String)props.get(key));
                    newProperties.add(prop);
                }
                
                merge(properties, newProperties);
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }        
    }
    
    /*--------------------------------------------------------------------------------*\
     *                  Set/Add/Delete application  variables                         *
    \*--------------------------------------------------------------------------------*/
    /**
     * Add a set of application variables to the registry for the runtime target.
     *
     * @param appVars - list of application variables to be added to the registry
     * @exception RegistryException on errors in adding the application variables.
     */
    public void addComponentApplicationVariables(String componentName, 
		com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException
    {
        addComponentApplicationVariables(componentName, mThisTarget, appVars );
    }
    
    /**
     * Add a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be added to the registry
     * @param targetName - target to be updated
     * @exception RegistryException on errors in adding the application variables.
     */
    public void addComponentApplicationVariables(String componentName, String targetName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException
    {
        try
        {
            ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                ComponentConfigType ccfg = comp.getComponentConfig();
                if ( ccfg == null )
                {
                    ObjectFactory factory = new ObjectFactory();
                    ccfg = factory.createComponentConfigType();
                    comp.setComponentConfig(ccfg);
                }
                
                List<AppVariableType> variables = ccfg.getApplicationVariable();
                
                for ( com.sun.jbi.management.ComponentInfo.Variable appVar : appVars )
                {
                    AppVariableType var = mObjectFactory.createAppVariableType();

                    var.setName( appVar.getName() );
                    var.setValue((appVar.getValue()==null)? "null":appVar.getValue());
                    var.setType( appVar.getType() );
                    variables.add(var);
                }
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }        
    }
    
    /**
     * Update a set of application variables to the registry for the runtime target.
     *
     * @param appVars - list of application variables to be updated
     * @exception RegistryException on errors in updating the application variables.
     */
    public void updateComponentApplicationVariables(String componentName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException
    {
         updateComponentApplicationVariables(componentName, mThisTarget, appVars );
    }
    
    /**
     * Updated a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be updated
     * @param targetName - target to be updated
     * @exception RegistryException on errors in updating the application variables.
     */
    public void updateComponentApplicationVariables(String componentName, String targetName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException
    {
                
        try
        {
            ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                ComponentConfigType ccfg = comp.getComponentConfig();
                if ( ccfg == null )
                {
                    ObjectFactory factory = new ObjectFactory();
                    ccfg = factory.createComponentConfigType();
                    comp.setComponentConfig(ccfg);
                }
                
                List<AppVariableType> variables = ccfg.getApplicationVariable();
               
                for ( com.sun.jbi.management.ComponentInfo.Variable appVar : appVars )
                {
                    boolean updated = false;
                    for ( AppVariableType var : variables)
                    {
                        if ( var.getName().equals(appVar.getName()) )
                        {
                            var.setName( appVar.getName() );
                            var.setValue((appVar.getValue()==null)? "null":appVar.getValue());
                            var.setType( appVar.getType() );
                            updated = true;
                            break;
                        }
                    }
                    if ( !updated )
                    {
                        /** With the fix to Issue 780, we no longer perist all 
                         * variables in the registry, and only those which are 
                         * updated, we can safely add those which are not there
                         * in the registry, since an integrity check is already done
                         * earlier
                         */
                        AppVariableType var = mObjectFactory.createAppVariableType();

                        var.setName( appVar.getName() );
                        var.setValue((appVar.getValue()==null)? "null":appVar.getValue());
                        var.setType( appVar.getType() );
                        variables.add(var);
                    }
                }
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }        
    }
    
    /**
     * Delete a set of application variables from the registry for the runtime target.
     *
     * @param appVars - list of application variables to be deleted from the registry
     * @param names - the names of the variables to be deleted.
     * @exception RegistryException on errors in deleting the application variables.
     */
    public void deleteComponentApplicationVariables(String componentName, String[] names )
        throws  RegistryException
    {
         deleteComponentApplicationVariables(componentName, mThisTarget, names );
    }
    
    /**
     * Add a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be deleted from the registry
     * @param targetName - target to be updated
     * @param names - the names of the variables to be deleted.
     * @exception RegistryException on errors in deleting the application variables.
     */
    public void deleteComponentApplicationVariables(String componentName, String targetName, 
        String[] names )
        throws  RegistryException
    {
        try
        {
            ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                ComponentConfigType ccfg = comp.getComponentConfig();
                if ( ccfg == null )
                {
                    ObjectFactory factory = new ObjectFactory();
                    ccfg = factory.createComponentConfigType();
                    comp.setComponentConfig(ccfg);
                }
                
                List<AppVariableType> variables = ccfg.getApplicationVariable();

                for ( String name : names )
                {
                    AppVariableType tbd = null;
                    for ( AppVariableType var : variables)
                    {
                        if ( var.getName().equals(name) )
                        {
                            tbd = var;
                            break;
                        }
                    }
                    if ( tbd != null )
                    {
                        variables.remove(tbd);
                    }
                    else
                    {
                        // -- mLog.warning(mTranslator.getString(, appVar.getName()));
                    }
                }
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }        
    }
    
    /*--------------------------------------------------------------------------------*\
     *                  Set/Add/Delete Named Configurations                           *
    \*--------------------------------------------------------------------------------*/
    
    /**
     * The name of the application configuration is the value of the "configurationName"
     * property.
     */
    
    /**
     * Add a named application configuration to the registry for the component for the
     * implicit runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @exception RegistryException on errors in adding the application configuration.
     */
    public void addComponentApplicationConfiguration(String componentName, 
		Properties appConfig )
        throws  RegistryException
    {
        addComponentApplicationConfiguration(componentName, mThisTarget, appConfig);
    }
    
    /**
     * Add a named application configuration to the registry for the component for the
     * specified runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @param targetName 
     *                target name
     * @exception RegistryException on errors in adding the application configuration.
     */
    public void addComponentApplicationConfiguration( String componentName, String targetName,
		Properties appConfig )
        throws  RegistryException
    {
        try
        {
            String configName = (String) appConfig.get(Registry.APP_CONFIG_NAME_KEY);
                
            if ( configName == null )
            {
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_APP_CONFIG_PROPS_MISSING_NAME);
                throw new RegistryException(errMsg);
            }
            
            ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                ComponentConfigType ccfg = comp.getComponentConfig();
                if ( ccfg == null )
                {
                    ObjectFactory factory = new ObjectFactory();
                    ccfg = factory.createComponentConfigType();
                    comp.setComponentConfig(ccfg);
                }
                
                List<AppConfigType> configs = ccfg.getApplicationConfiguration();
                AppConfigType cfg = mObjectFactory.createAppConfigType();
                List<PropertyType> cfgPropList = cfg.getProperty();
                
                Set keys = appConfig.keySet();
                
                for ( Object key : keys )
                {
                    PropertyType prop = mObjectFactory.createPropertyType();
                    
                    String propKey = (String) key; 
                    prop.setName( propKey );
                    prop.setValue((String) appConfig.get(propKey));
                    cfgPropList.add(prop);
                }
                cfg.setName(configName);
                configs.add(cfg);
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }        
    }
    
    /**
     * Update a named application configuration set on a component 
     * for the implicit runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @exception RegistryException on errors in updating the application configuration.
     */
    public void updateComponentApplicationConfiguration(String componentName, 
        Properties appConfig )
        throws  RegistryException
    {
        updateComponentApplicationConfiguration(componentName, mThisTarget, appConfig);
    }
    
    
    /**
     * Update a named application configuration set on a component 
     * for the specified runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @param targetName 
     *                 target name
     * @exception RegistryException on errors in updating the application configuration.
     */
    public void updateComponentApplicationConfiguration(String componentName, String targetName,
        Properties appConfig )
        throws  RegistryException
    {
        try
        {
            String configName = (String) appConfig.get(Registry.APP_CONFIG_NAME_KEY);

            if ( configName == null )
            {
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_APP_CONFIG_PROPS_MISSING_NAME);
                throw new RegistryException(errMsg);
            }
            
            ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                ComponentConfigType ccfg = comp.getComponentConfig();
                if ( ccfg == null )
                {
                    ObjectFactory factory = new ObjectFactory();
                    ccfg = factory.createComponentConfigType();
                    comp.setComponentConfig(ccfg);
                }
                
                List<AppConfigType> configs = ccfg.getApplicationConfiguration();
                
                AppConfigType oldCfg = null;
                for ( AppConfigType config : configs )
                {
                    List<PropertyType> cfgPropList = config.getProperty();
                    
                    if ( checkContainsProperty(cfgPropList, Registry.APP_CONFIG_NAME_KEY, configName))
                    {
                        oldCfg =config;
                    }
                }
                
                if ( oldCfg != null )
                {
                    // remove the old config and add the updated one
                    //configs.remove(tbdCfg);
                    
                    //AppConfigType cfg = mObjectFactory.createAppConfigType();
                    List<PropertyType> cfgPropList = oldCfg.getProperty();

                    Set keys = appConfig.keySet();

                    for ( Object key : keys )
                    {
                        String propKey = (String) key;
                        Object valueObj = appConfig.get(propKey);
                        boolean keyFound = false;
                        if ( valueObj != null )
                        {
                            
                            for(PropertyType prop : cfgPropList)
                            {
                                if ( prop.getName().equals(propKey))
                                {
                                    prop.setValue(valueObj.toString() );
                                    keyFound = true;
                                }

                            }

                            if (!keyFound)
                            {
                                // -- This is a new property being added with the update
                                PropertyType prop = mObjectFactory.createPropertyType();
                                prop.setName( propKey );
                                prop.setValue(valueObj.toString());
                                cfgPropList.add(prop);
                            }
                        }
                    }
                    //cfg.setName(configName);
                    //configs.add(cfg);
                }
                else
                {
                    AppConfigType cfg = mObjectFactory.createAppConfigType();
                    List<PropertyType> cfgPropList = cfg.getProperty();

                    Set keys = appConfig.keySet();

                    for ( Object key : keys )
                    {
                        PropertyType prop = mObjectFactory.createPropertyType();

                        String propKey = (String) key; 
                        prop.setName( propKey );
                        prop.setValue((String) appConfig.get(propKey));
                        cfgPropList.add(prop);
                    }
                    cfg.setName(configName);
                    configs.add(cfg);
                }
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }
    }
    
    /**
     * Delete a named application configuration
     *
     * @param appConfigName 
     *          name of the application configuration to delete.
     * @param componentName
     *          component identification
     * @exception RegistryException on errors in deleting the application configuration.
     */
    public void deleteComponentApplicationConfiguration(String componentName, String appConfigName )
        throws  RegistryException
    {
        deleteComponentApplicationConfiguration(componentName, mThisTarget, appConfigName);
    }
    
    
    /**
     * Delete a named application configuration
     *
     * @param componentName
     *          component identification
     * @param targetName
     *          target name
     * @param appConfigName 
     *          the name of the configuration to be deleted.
     * @exception RegistryException on errors in deleting the application configuration.
     */
    public void deleteComponentApplicationConfiguration(String componentName, String targetName,
        String appConfigName )
        throws  RegistryException
    {
        try
        {
            ComponentRefType comp = mGenQuery.getComponent(componentName, targetName);

            mRegObjLM.acquireWriteLock();
            try
            {
                ComponentConfigType ccfg = comp.getComponentConfig();
                if ( ccfg == null )
                {
                    ObjectFactory factory = new ObjectFactory();
                    ccfg = factory.createComponentConfigType();
                    comp.setComponentConfig(ccfg);
                }
                
                List<AppConfigType> configs = ccfg.getApplicationConfiguration();
                
                AppConfigType tbdCfg = null;
                for ( AppConfigType config : configs )
                {
                    List<PropertyType> cfgPropList = config.getProperty();
                    
                    if ( checkContainsProperty(cfgPropList, Registry.APP_CONFIG_NAME_KEY, appConfigName))
                    {
                        tbdCfg = config;
                    }
                }
                
                if ( tbdCfg != null )
                {
                    // remove the config
                    configs.remove(tbdCfg);
                }
            }
            catch(RuntimeException rte)
            {
                mRegObjLM.releaseWriteLock();
                throw rte;
            }
            commit();
        }
        catch(Exception jex)
        {
            throw new RegistryException(jex);
        }
    }
    
    /*----------------------------------------------------------------------------------*\
     *                          Private Helpers                                         *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * @param componentName - name of component to be added to the domain
     */
    private void deleteComponent(String componentName)
    {
        Components compType = mJbiRegistry.getComponents();

        if ( compType != null )
        {
            DomainEntityType compToDel = null;
            List<DomainComponentType> components = compType.getComponent();
            for (DomainComponentType component : components  )
            {
                if ( component.getName().equals(componentName))
                {
                    compToDel = component;
                }
            }
            if (compToDel != null)
            {
                components.remove(compToDel);
            }
        }
    }
    
    /**
     * @param sharedLibraryName - name of service assembly to be removed
     */
    private void deleteSharedLibrary(String sharedLibraryName)
    {
        SharedLibraries slType = mJbiRegistry.getSharedLibraries();

        if ( slType != null )
        {
            DomainEntityType slToDel = null;
            List<DomainSharedLibraryType> sls = slType.getSharedLibrary();
            for ( DomainSharedLibraryType sl : sls  )
            {
                if ( sl.getName().equals(sharedLibraryName))
                {
                   slToDel = sl;
                }
            }
            if ( slToDel != null )
            {
                sls.remove(slToDel);
            }
        }
    }
    
    /**
     * Remove a service assembly from the domain. 
     *
     * @param serviceAssemblyName - service assembly name
     * @throws RegistryException on errors
     */
    private void deleteServiceAssembly(String serviceAssemblyName)
            throws RegistryException
    {
        ServiceAssemblies saType = mJbiRegistry.getServiceAssemblies();

        if ( saType != null )
        {
            DomainEntityType saToDel = null;
            List<DomainEntityType> sas = saType.getServiceAssembly();
            for ( DomainEntityType sa : sas  )
            {
                if ( sa.getName().equals(serviceAssemblyName))
                {
                    saToDel = sa;
                }
            }
            if ( saToDel != null )
            {
                sas.remove(saToDel);
            }
        }
     }

    /**
     * Add a ComponentRef to a cluster or a server
     */
    private void addComponentRef(ComponentInfo compInfo, InstalledComponentsListType iclt)
        throws RegistryException
    {
        try
        {
            ComponentRefType compRef = ObjectTranslator.getJaxbComponentRef(compInfo);
        
            iclt.getComponentRef().add(compRef);
        }
        catch(JAXBException jex)
        {
            throw new RegistryException(jex);
        }
    }
    
    
    /**
     * Remove a ComponentRef to a cluster or a server
     */
    private void removeComponentRef(String componentName, InstalledComponentsListType iclt)
    {
        if ( iclt != null )
        {
            ComponentRefType compToDel = null;
            
            List<ComponentRefType> components = iclt.getComponentRef();

            for ( ComponentRefType component : components)
            {
                if ( component.getNameRef().equals(componentName))
                {
                    compToDel = component;
                }
            }
            
            if ( compToDel != null )
            {
                components.remove(compToDel);
            }
        }
    }
    
    
    /**
     * Add a SharedLibraryRef to a cluster or a server
     */
    private void addSharedLibraryRef(ComponentInfo slInfo, InstalledComponentsListType iclt)
        throws RegistryException
    {
        try
        {
            SharedLibraryRefType slRef = ObjectTranslator.getJaxbSharedLibraryRef(slInfo);

            iclt.getSharedLibraryRef().add(slRef);
            
        }
        catch(JAXBException jex)
        {
            throw new RegistryException(jex);
        }
    }
    
    
    /**
     * Remove a SharedLibraryRef to a cluster or a server
     */
    private void removeSharedLibraryRef(String sharedLibraryName, InstalledComponentsListType iclt)
    {
        SharedLibraryRefType slToBeDeleted = null;
        if ( iclt != null )
        {
            List<SharedLibraryRefType> sls = iclt.getSharedLibraryRef();

            for ( SharedLibraryRefType sl : sls)
            {
                if ( sl.getNameRef().equals(sharedLibraryName))
                {
                    slToBeDeleted = sl;
                }
            }
            
            if (slToBeDeleted != null)
            {
                sls.remove(slToBeDeleted);
            }
        }  
    }
    
    /**
     * Add a ServiceAssemblyRef to a cluster or a server
     */
    private void addServiceAssemblyRef(String saName, InstalledComponentsListType iclt)
        throws RegistryException
    {
        try
        {
            ServiceAssemblyRefType saRef = ObjectTranslator.getJaxbServiceAssemblyRef(saName);

            iclt.getServiceAssemblyRef().add(saRef);
            
        }
        catch(JAXBException jex)
        {
            throw new RegistryException(jex);
        }
    }
    
    
    /**
     * Remove a ServiceAssemblyRef from a cluster or a server
     */
    private void removeServiceAssemblyRef(String saName, InstalledComponentsListType iclt)
    {
        ServiceAssemblyRefType saToBeDeleted = null;
        if ( iclt != null )
        {
            List<ServiceAssemblyRefType> sas = iclt.getServiceAssemblyRef();

            for ( ServiceAssemblyRefType sa : sas)
            {
                if ( sa.getNameRef().equals(saName))
                {
                    saToBeDeleted = sa;
                }
            }
            
            if (saToBeDeleted != null)
            {
                sas.remove(saToBeDeleted);
            }
        }  
    }
    
    /**
     * Commit the registry object after down grading the write to a read lock, finally
     * relinquish the read lock.
     */
    private void commit()
        throws RegistryException
    {
        mRegObjLM.downgradeWriteLock();
        try
        {
            mRegistry.commit();
        }
        catch ( com.sun.jbi.management.registry.RegistryException rex )
        {
            throw rex;
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
    }
    
    /**
     * Update the property list with the new entries
     * 
     * @param properties - list of component properties to be updated
     * @param newProperties - new component properties
     */
    private void merge(List<PropertyType> properties, List<PropertyType> newEntries )
    {
        for ( PropertyType newProp : newEntries)
        {
            boolean replaced = false;
            
            // If an entry with same name exists replace it
            for ( PropertyType oldProp : properties )
            {
                if ( oldProp.getName().equals(newProp.getName()))
                {
                    oldProp.setValue(newProp.getValue());
                    replaced = true;
                    break;
                }
            }
            
            if ( !replaced )
            {
                properties.add(newProp);
            }
        }
    }
    
    /**
     * @return true if the PropertyList contains a given property name value pair.
     */ 
    boolean checkContainsProperty(List<PropertyType> propList, 
        String name, String value)
    {
        for(PropertyType prop : propList)
        {
            if ( prop.getName().equals(name) && prop.getValue().equals(value))
            {
                return true;
            }
        }
        return false;
    }    
}
