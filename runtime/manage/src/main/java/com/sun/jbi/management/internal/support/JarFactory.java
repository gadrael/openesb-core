/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JarFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;


import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.jar.JarException;
import java.io.File;
import java.io.FileInputStream;
import java.util.jar.JarInputStream;

import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;




public class JarFactory
{
    
    /* Buffer to read data 
    *
    */
    private byte[] mBuffer;
    
    /* Unzip Directory 
    *
    */
    private String mDir;
    
    /**
      * This object provides utility methods to manipulate DOM tree.
      * @param dir Directory to unjar
      */
    public JarFactory (String dir)
    {
        
        this.mDir = dir;
        this.mBuffer = new byte[8092];
        
    }
    
    /**
     * Unjars a file.
     *
     * @param jarFile File to be unjared
     * @throws IOException if error trying to read entry.
     * @throws java.util.jar.JarException if error trying to read jar file.
     */
    public void unJar (File jarFile) throws IOException, JarException, ZipException
    { 
        // process all entries in that JAR file
        FileInputStream fis = new FileInputStream(jarFile);
        JarInputStream jis = new JarInputStream(fis);        
        unJar(jis);
        jis.close();
    }
    
    /**
     * Unjars a zip file using it's InputStream.
     *
     * @param zis zip input stream
     * @throws IOException if error trying to read entry.
     * @throws java.util.jar.JarException if error trying to read jar file.
     */
    public void unJar (java.util.zip.ZipInputStream zis) throws IOException, ZipException
    {
        
        // process all entries
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null )
        {
            getEntry (zis, zipEntry);
            zipEntry = zis.getNextEntry();
        }
    }
    
    /**
     * Gets one file entry from the zip input stream
     *
     * @param jarFile the JAR file reference to retrieve <code>entry</code> from.
     * @param entry the file from the JAR to extract.
     * @return the ZipEntry name
     * @throws IOException if error trying to read entry.
     */
    public String getEntry (ZipInputStream zis, ZipEntry entry) throws IOException
    {
        
        String entryName = entry.getName ();
        // if a directory, mkdir it (remember to create 
        // intervening subdirectories if needed!)
        if (entryName.endsWith ("/"))
        {
            new File (mDir, entryName).mkdirs ();
            return entryName;
        }
        
        File f = new File (mDir, entryName);
        
        if (!f.getParentFile ().exists ())
        {
            f.getParentFile ().mkdirs ();
        }
        
        // Must be a file; create output stream to the file
        FileOutputStream fostream = new FileOutputStream (f);
 
        // extract files
        int n = 0;
        while ((n = zis.read (mBuffer)) > 0)
        {
            fostream.write (mBuffer, 0, n);
        }
        
        try
        {
           // istream.close ();
            fostream.close ();
        }
        catch (IOException e)
        {
            // do nothing
        }
        return entryName;
    }
}
