/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentUnitInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  DeploymentUnit.java
 *
 * Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
 *
 *  Created on May 8, 2006, 5:07 PM
 */

package com.sun.jbi.management.system;

/**
 * Class which enacpsulates information on a Service Unit and its Target Component.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeploymentUnitInfo
{
    
    /**
     * Servuice Unit
     */
    private String mSuName;
    
    /**
     * Component
     */
    private String mCompName;
    
    /**
     * The Service Unit Jar Name.
     */
    private String mSuJarName;
    
    /**
     * The Service Unit Jar Location.
     */
    private String mSuJarPath;
    
    /** Creates a new instance of DeploymentUnit */
    public DeploymentUnitInfo (String suName, 
                            String componentName, 
                            String suJarName,
                            String saUnzipPath)
    {
        mSuName      = suName;
        mCompName    = componentName; 
        mSuJarName   = suJarName;
        mSuJarPath   = saUnzipPath + java.io.File.separator + suJarName; 
    }
    
    /**
     * @return the Service Unit name.
     */
    public String getServiceUnit()
    {
        return mSuName;
    }
    
    /**
     * @return the Target Component Name
     */
    public String getTargetComponent()
    {
        return mCompName;
    }
    
    /**
     * @return the location of the Service Unit jar
     */
    public String getServiceUnitJarPath()
    {
        return mSuJarPath;
    }
    
    /**
     * @return the Name of the Service Unit jar
     */
    public String getServiceUnitJarName()
    {
        return mSuJarName;
    }
    
    /**
     * @return the Service Unit Root Path
     */
    public String getServiceUnitRoot()
    {
        return getTargetComponent() + "_" + getServiceUnit();
    }
}
