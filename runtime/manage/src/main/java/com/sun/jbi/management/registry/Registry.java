/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Registry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Registry.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on July 20, 2005, 3:25 PM
 */

package com.sun.jbi.management.registry;

import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ServiceAssemblyQuery;

import java.io.ByteArrayInputStream;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public interface Registry extends com.sun.jbi.registry.Registry
{
    public static final String REGISTRY_FOLDER_PROPERTY = "com.sun.jbi.registry.folder";
    public static final String REGISTRY_FILE_PROPERTY = "com.sun.jbi.registry.file";
    public static final String REGISTRY_SYNC_FILE_PROPERTY = "com.sun.jbi.registry.syncfile";
    public static final String REGISTRY_READONLY_PROPERTY = "com.sun.jbi.registry.readonly";
    public static final String REGISTRY_LOCK_INTERVAL_PROPERTY = "com.sun.jbi.registry.lock.interval";
    
    public static final String CONFIG_SUFFIX      = "-config";
    public static final String APP_CONFIG_NAME_KEY = "configurationName";
    
    /**
     * Get the Updater instance to be used for updating the Registry.
     *
     * @return an instance of the Updater to be used to manipulate 
     * ( create / delete / modify ) contents of the registry.
     * @throws RegistryException if problems are encountered in creating
     * a Updater.
     */
    Updater getUpdater()
        throws RegistryException;
    
    /**
     * Get a Generic Query Instance
     * @return an instance of GenericQuery.
     * @throws RegistryException on errors.
     */
    GenericQuery getGenericQuery() throws RegistryException;
    
    /**
     * Get a ComponentQuery instance for a given target
     *
     * @param targetName - name identifying the target.
     * @return an instance of ComponentQuery.
     * @throws RegistryException on errors.
     */
    ComponentQuery getComponentQuery(String targetName)
        throws RegistryException;
    
    /**
     * Get the ComponentQuery specific to the target of the instance.
     * @return an instance of ComponentQuery.
     * @throws RegistryException on errors.
     */
    ComponentQuery getComponentQuery()
        throws RegistryException;
    
    
    /**
     * Get a ServiceAssemblyQuery instance for a given target
     *
     * @param targetName - name identifying the target.
     * @return an instance of ServiceAssemblyQuery.
     * @throws RegistryException on errors.
     */
    ServiceAssemblyQuery getServiceAssemblyQuery(String targetName)
        throws RegistryException;
    
    /**
     * Get the ServiceAssemblyQuery specific to the target of the instance.
     * @return an instance of ServiceAssemblyQuery.
     * @throws RegistryException on errors.
     */
    ServiceAssemblyQuery getServiceAssemblyQuery()
        throws RegistryException;
    
    /**
     * destroy the single instance.
     */
    void destroy();
    
    /**
     * Get the Registry Specification.
     */
    public RegistrySpec getRegistrySpec();
    
    /**
     * Get the Repository the registry instance uses.
     */
    public com.sun.jbi.management.repository.Repository getRepository();
    
    /**
     * Get a registry property.
     */
    public String getProperty(String propName);
    
    /**
     * Commit the changes to the registry.
     */
    public void commit()
        throws RegistryException;
    
    /**
     * Take a snapshot of the Registry.
     */
    public ByteArrayInputStream snapshot()
        throws RegistryException;
}
