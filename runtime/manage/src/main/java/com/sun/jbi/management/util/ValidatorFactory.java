/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ValidatorFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  LockManager.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 2, 2006, 4:49 PM
 */

package com.sun.jbi.management.util;

import com.sun.jbi.EnvironmentContext;


/**
 * A factory to create different validators.
 *
 * @author Sun Microsystems, Inc
 */
public class ValidatorFactory
{
    
    private static boolean sValidate;
    
    /**
     * Create and return a validator of the specified type.
     *
     * @return an archive validator of the required type
     */
    public static Validator createValidator(EnvironmentContext envCtx, ValidatorType type)
        throws Exception
    {
        Validator validator = null;
        switch ( type )
        {
            case COMPONENT :
                validator = new ComponentValidator(envCtx, sValidate);
                break;
                
            case SHARED_LIBRARY :
                validator = new SharedLibraryValidator(envCtx, sValidate);
                break;
             
            case SERVICE_ASSEMBLY :
                validator = new ServiceAssemblyValidator(envCtx, sValidate);
                break;
            
            case SERVICE_UNIT :
                //validator = new ServiceUnitValidator(sValidate);
                break;
                
            default :
                // this should never be the case
                break;
                
        }
        return validator;
    }
    
    /**
     * Set jbi.xml validation to true
     */
    public static void setDescriptorValidation(boolean validate)
    {
        sValidate = validate;
    }
    
}
