/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DynamicRuntimeConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.LocalStringKeys;

import java.util.Properties;

import javax.management.Descriptor;
import javax.management.modelmbean.ModelMBeanAttributeInfo;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.EnvironmentContext;

/**
 * DeploymentConfigurationFactory defines the configuration parameters required for 
 * DEPLOYMENT. Any new parameters would require updates to this class.
 * 
 * @author Sun Microsystems, Inc.
 */
public class DeploymentConfigurationFactory 
    extends ConfigurationFactory
{
    /** Attribute Names */
    public static final String SERVICE_UNIT_TIMEOUT = "serviceUnitTimeout";
    public static final String DEPLOYMENT_TIMEOUT   = "deploymentTimeout";
    public static final String ENABLE_AUTO_DEPLOY   = "autoDeployEnabled";
    public static final String ENABLE_AUTO_REDEPLOY = "autoRedeployEnabled";
    public static final String ENABLE_AUTO_UNDEPLOY = "autoUndeployEnabled";
    public static final String START_ON_DEPLOY      = "startOnDeploy";
    public static final String START_ON_VERIFY      = "startOnVerify";    
    public static final String AUTO_DEPLOY_DIR      = "autoDeployDir";
    
    private static final String AUTO_DEPLOY_DIR_NAME = "autodeploy";
    
    private static final int sNumAttributes = 8;
    
    /**
     * Constructor 
     *
     * @param defProps - default properties
     */
    public DeploymentConfigurationFactory(Properties defProps)
    {
        super(defProps, ConfigurationCategory.Deployment);
    }
    
    /**
     *
     */
    public ModelMBeanAttributeInfo[] createMBeanAttributeInfo()
    {
         ModelMBeanAttributeInfo[] attributeInfos = new ModelMBeanAttributeInfo[sNumAttributes];
         DescriptorSupport descr;
         String attrDescr;
         
         // -- Service Unit Timeout
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.SERVICE_UNIT_TIMEOUT_DESCR);
         descr.setAttributeName(SERVICE_UNIT_TIMEOUT);
         descr.setDisplayName(getString(LocalStringKeys.SERVICE_UNIT_TIMEOUT_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.SERVICE_UNIT_TIMEOUT_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.SERVICE_UNIT_TIMEOUT_DESCR));
         descr.setToolTip(getString(LocalStringKeys.SERVICE_UNIT_TIMEOUT_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.SERVICE_UNIT_TIMEOUT_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         
         // Check the defaults file
         String defValue = mDefaults.getProperty(getQualifiedKey(SERVICE_UNIT_TIMEOUT), "0");
         descr.setDefault(Integer.parseInt(defValue));
		 descr.setMinValue(0);
		 descr.setMaxValue(Integer.MAX_VALUE);
         descr.setUnit("milliseconds");
         
         attributeInfos[0] = new ModelMBeanAttributeInfo(
            // name                type   descr      R      W    isIs  Descriptor      
            SERVICE_UNIT_TIMEOUT, "int", attrDescr, true, true, false, descr);
         
         // -- Deployment Timeout
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.DEPLOYMENT_TIMEOUT_DESCR);
         descr.setAttributeName(DEPLOYMENT_TIMEOUT);
         descr.setDisplayName(getString(LocalStringKeys.DEPLOYMENT_TIMEOUT_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.DEPLOYMENT_TIMEOUT_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.DEPLOYMENT_TIMEOUT_DESCR));
         descr.setToolTip(getString(LocalStringKeys.DEPLOYMENT_TIMEOUT_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.DEPLOYMENT_TIMEOUT_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         
         defValue = mDefaults.getProperty(getQualifiedKey(DEPLOYMENT_TIMEOUT), "0");
         descr.setDefault(Integer.parseInt(defValue));
		 descr.setMinValue(0);
		 descr.setMaxValue(Integer.MAX_VALUE);
         descr.setUnit("milliseconds");

         attributeInfos[1] = new ModelMBeanAttributeInfo(
            // name             type   descr      R      W    isIs  Descriptor      
            DEPLOYMENT_TIMEOUT, "int", attrDescr, true, true, false, descr);
         
         // auto-deploy Enabled
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.ENABLE_AUTO_DEPLOY_DESCR);
         descr.setAttributeName(ENABLE_AUTO_DEPLOY);
         descr.setDisplayName(getString(LocalStringKeys.ENABLE_AUTO_DEPLOY_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.ENABLE_AUTO_DEPLOY_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.ENABLE_AUTO_DEPLOY_DESCR));
         descr.setToolTip(getString(LocalStringKeys.ENABLE_AUTO_DEPLOY_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.ENABLE_AUTO_DEPLOY_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defEnableAutoDeploy = mDefaults.getProperty(getQualifiedKey(ENABLE_AUTO_DEPLOY), "true");
         boolean autoDeployEnabled = Boolean.parseBoolean(defEnableAutoDeploy);
         descr.setDefault(autoDeployEnabled);
         
         attributeInfos[2] = new ModelMBeanAttributeInfo(
            // name             type       descr      R      W    isIs  Descriptor      
            ENABLE_AUTO_DEPLOY, "boolean", attrDescr, true, true, true, descr);
         
         // -  auto-undeploy enabled
         // This property is dependent on the auto-deploy enabled property. i.e. 
         // autoDeployEnabled    autoUndeployEnabled       effective autoUndeployEnabled
         //        0                     0                             0
         //        0                     1                             0         
         //        1                     0                             0
         //        1                     1                             1

         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.ENABLE_AUTO_UNDEPLOY_DESCR);
         descr.setAttributeName(ENABLE_AUTO_UNDEPLOY);
         descr.setDisplayName(getString(LocalStringKeys.ENABLE_AUTO_UNDEPLOY_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.ENABLE_AUTO_UNDEPLOY_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.ENABLE_AUTO_UNDEPLOY_DESCR));
         descr.setToolTip(getString(LocalStringKeys.ENABLE_AUTO_UNDEPLOY_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.ENABLE_AUTO_UNDEPLOY_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defEnableAutoUndeploy = mDefaults.getProperty(getQualifiedKey(ENABLE_AUTO_UNDEPLOY), "true");
         boolean autoUndeployEnabled = Boolean.parseBoolean(defEnableAutoUndeploy) && autoDeployEnabled;
         descr.setDefault(autoUndeployEnabled);
         
         attributeInfos[3] = new ModelMBeanAttributeInfo(
            // name                type       descr      R      W   isIs  Descriptor      
            ENABLE_AUTO_UNDEPLOY, "boolean", attrDescr, true, true, true, descr);
         
         // -  auto-redeploy enabled
         // This property is dependent on the auto-deploy and auto-undeploy enabled property. i.e. 
         // autoDeployEnabled    autoUndeployEnabled     autoRedeployEnabled  effective 
         //                                                                   autoRedeployEnabled
         //        0                     0                    0                   0
         //        0                     0                    1                   0
         //        0                     1                    0                   0
         //        0                     1                    1                   0
         //        1                     0                    0                   0 
         //        1                     0                    1                   0 
         //        1                     1                    0                   0
         //        1                     1                    1                   1
         
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.ENABLE_AUTO_REDEPLOY_DESCR);
         descr.setAttributeName(ENABLE_AUTO_REDEPLOY);
         descr.setDisplayName(getString(LocalStringKeys.ENABLE_AUTO_REDEPLOY_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.ENABLE_AUTO_REDEPLOY_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.ENABLE_AUTO_REDEPLOY_DESCR));
         descr.setToolTip(getString(LocalStringKeys.ENABLE_AUTO_REDEPLOY_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.ENABLE_AUTO_REDEPLOY_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defEnableAutoRedeploy = mDefaults.getProperty(getQualifiedKey(ENABLE_AUTO_REDEPLOY), "true");
         boolean autoRedeployEnabled = Boolean.parseBoolean(defEnableAutoRedeploy) && autoDeployEnabled&&autoUndeployEnabled;
         descr.setDefault(autoRedeployEnabled);
         
         attributeInfos[4] = new ModelMBeanAttributeInfo(
            // name                type       descr      R      W   isIs  Descriptor      
            ENABLE_AUTO_REDEPLOY, "boolean", attrDescr, true, true, true, descr);
         
          // Auto Deploy directory
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.AUTO_DEPLOY_DIR_DESCR);
         descr.setAttributeName(AUTO_DEPLOY_DIR);
         descr.setDisplayName(getString(LocalStringKeys.AUTO_DEPLOY_DIR_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.AUTO_DEPLOY_DIR_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.AUTO_DEPLOY_DIR_DESCR));
         descr.setToolTip(getString(LocalStringKeys.AUTO_DEPLOY_DIR_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.AUTO_DEPLOY_DIR_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(true);
         descr.setIsPassword(false);
         descr.setDefault(getAutoDeployDir());
         
         attributeInfos[5] = new ModelMBeanAttributeInfo(
            // name          type                descr      R      W    isIs  Descriptor      
            AUTO_DEPLOY_DIR, "java.lang.String", attrDescr, true, false, false, descr);
         
         // Start on deploy
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.START_ON_DEPLOY_DESCR);
         descr.setAttributeName(START_ON_DEPLOY);
         descr.setDisplayName(getString(LocalStringKeys.START_ON_DEPLOY_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.START_ON_DEPLOY_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.START_ON_DEPLOY_DESCR));
         descr.setToolTip(getString(LocalStringKeys.START_ON_DEPLOY_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.START_ON_DEPLOY_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defStartOnDeploy = mDefaults.getProperty(getQualifiedKey(START_ON_DEPLOY), "true");
         descr.setDefault(Boolean.parseBoolean(defStartOnDeploy));
         descr.setDefault(true);
         
         attributeInfos[6] = new ModelMBeanAttributeInfo(
            // name          type       descr      R      W    isIs  Descriptor      
            START_ON_DEPLOY, "boolean", attrDescr, true, true, true, descr);

         // Start on deploy
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.START_ON_VERIFY_DESCR);
         descr.setAttributeName(START_ON_VERIFY);
         descr.setDisplayName(getString(LocalStringKeys.START_ON_VERIFY_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.START_ON_VERIFY_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.START_ON_VERIFY_DESCR));
         descr.setToolTip(getString(LocalStringKeys.START_ON_VERIFY_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.START_ON_VERIFY_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defStartOnVerify = mDefaults.getProperty(getQualifiedKey(START_ON_VERIFY), "true");
         descr.setDefault(Boolean.parseBoolean(defStartOnVerify));
         descr.setDefault(true);
         
         attributeInfos[7] = new ModelMBeanAttributeInfo(
            // name          type       descr      R      W    isIs  Descriptor      
            START_ON_VERIFY, "boolean", attrDescr, true, true, true, descr);
         
         return attributeInfos;
    }
    
    /*--------------------------------------------------------------------------------*\
     *                         private helpers                                        *
    \*--------------------------------------------------------------------------------*/
    
    /**
     * @return the abosolute path to the auto-deploy directory
     */
    private String getAutoDeployDir()
    {
        // -- Default JBI autodeploy dir
        com.sun.jbi.EnvironmentContext envCtx = com.sun.jbi.util.EnvironmentAccess.getContext();
        StringBuffer deployDir = new StringBuffer(envCtx.getJbiInstanceRoot());
        deployDir.append(java.io.File.separator);
        deployDir.append(AUTO_DEPLOY_DIR_NAME);
        
        String defDeployDir = mDefaults.getProperty(getQualifiedKey(AUTO_DEPLOY_DIR), 
            deployDir.toString());
        
        String absDeployDir = 
            com.sun.jbi.management.util.PropertyFilter.filterProperties(defDeployDir);

        if ( absDeployDir != null )
        {
            absDeployDir = absDeployDir.replace('\\', '/');
        }
        
        return absDeployDir;
    }
}
