/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Registry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  RegistryDocument.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on July 20, 2005, 3:25 PM
 */

package com.sun.jbi.management.registry.xml;

import com.sun.jbi.management.ComponentInfo;
import com.sun.jbi.management.ConfigurationCategory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.util.Map;
import java.util.HashMap;
import java.util.Properties;

/**
 * RegistryDocument encapsulates a read only registry DOM. This class has user friendly
 * operations to get information from the registry w/o getting into the nitty-gritty 
 * details of parsing the registry XML doc. This class uses XPath expressions for faster
 * processing.
 *
 * @author Sun Microsystems, Inc.
 */
public class RegistryDocument
{
    /** The registry DOM */
    private Document mRegistryDocument;
    
    /** Domain literal */
    private static final String DOMAIN = "domain";
    
    
    public RegistryDocument(Document regDoc)
    {
        mRegistryDocument = regDoc;
    }
    
    /**
     * Get the value of a specific configuration attribute for a target belonging to a 
     * particular category. If the attribute is overriden then the override is returned.
     * If the attribute is missing or the registry configuration is empty a null value
     * is returned.
     *
     * @param target - identification for the target
     * @param category - configuration category
     * @param name - attribute name
     * @return the string value of the configuration attribute from the persisted 
     *         registry or null if the attribute is missing or configuration is empty
     */
    public String getConfigurationAttribute(String target, ConfigurationCategory category,
    	String attrName)
	{
        String value = null;
        if ( isAttributeOverriden(target, category, attrName) )
        {
            value = getAttribute(target, category, attrName);
        }
        else
        {
            value = getAttribute(DOMAIN, category, attrName);
        }
        return value;
    }
    
     /**
      * Get a map of all the configuration attributes for a particular category. If an 
      * attribute is overriden then the specific value replaces the domain configuration
      *
      * @param target - identification for the target
      * @param category - configuration category
      * @return the configuration attributes from the persisted registry or an empty list
      *         
      */
     public Map<String, String> getConfigurationAttributes(String target, ConfigurationCategory category)
     {
         Map<String, String> targetSpecificProps  = getProperties(target, category);   
         return targetSpecificProps;
     }
     
     /**
      * Get the static component configuration attributes for a component.
      */
     public Properties getComponentConfigurationAttributes(String target, boolean isServer,
        String componentName)
     {
         Element targetElement = getTarget(target, isServer);
         Element compRef = getComponentRef(targetElement, componentName);
         return getComponentConfigurationProperties(compRef);
     }
     
     /**
      * Get the application variables set on a specific component.
      *
      * @param isServer - flag indicating whether the target is a server or cluster. 
      * @param target   - target name
      * @param componentName - identification for the component.                  
      */
     public ComponentInfo.Variable[] getComponentApplicationVariables(String target, boolean isServer,
        String componentName)
     {
         Element targetElement = getTarget(target, isServer);
         Element compRef = getComponentRef(targetElement, componentName);
         return getComponentApplicationVariables(compRef);
     }
     
     /**
      * Get all the named application configurations.
      *
      * @param isServer 
      *          flag indicating whether the target is a server or cluster. 
      * @param target   
      *          target name
      * @param componentName 
      *          identification for the component.                  
      * @return a Map of configuration names and the named config properties.
      */
     public Map<String, Properties> getComponentApplicationConfiguration(String target, boolean isServer,
        String componentName)
     {
         Element targetElement = getTarget(target, isServer);
         Element compRef = getComponentRef(targetElement, componentName);
         return getComponentApplicationConfigurations(compRef);
     }

    /*--------------------------------------------------------------------------------*\
     *                              Private helpers                                   *
    \*--------------------------------------------------------------------------------*/
     
    /**
     * @return true if an attribute is overridden
     */
     
	private boolean isAttributeOverriden(String target, ConfigurationCategory category,
        String attribName)
    {
         NodeList list;
         return ( getConfig(target) != null );
    }
    
    /**
     * Get the value of a specific configuration attribute for a target belonging to a 
     * particular category. 
     *
     * @param target - identification for the target
     * @param category - configuration category
     * @param name - attribute name
     * @return the string value of the configuration attribute from the persisted 
     *         registry or null if the attribute is missing or configuration is empty
     */
    public String getAttribute(String target, ConfigurationCategory category,
    	String attribName)
    {
        String value = null;
        Map<String, String> properties = getProperties(target, category);
        if ( properties.containsKey(attribName) )
        {
            value =  properties.get(attribName);
        }
        return value;
    }
    
    /**
     * Get the Property nodes for a given target and configuration catregory
     *
     * @param target - target name
     * @param category - Configuration Category
     */
    private Map<String, String> getProperties(String target, ConfigurationCategory category)
    {
        NodeList nameList = null;
        NodeList valueList = null;
        Map properties = new Properties();

        Element targetConfig = getConfig(target);
        if ( targetConfig != null )
        {
            // -- Get the category
            NodeList configTypeList = targetConfig.getElementsByTagName("config-type");
            for ( int i = 0; i < configTypeList.getLength(); i++ )
            {
                Element configTypeElem = (Element) configTypeList.item(i);
                if ( ( category.toString() ).equals(configTypeElem.getAttribute("category")) )
                {
                    properties = getProperties(configTypeElem);
                }
            }
        }
        
        return properties;
    }
    
    /**
     *
     * @return the "config" element for the target if found, null otherwise.
     */
    private Element getConfig(String target)
    {
        Element targetConfig = null;

        if ( mRegistryDocument != null )
        {
            // -- Get the JBI element
            Element jbi = mRegistryDocument.getDocumentElement();
            Element configsElem = null;
            if ( jbi != null )
            {
                NodeList configs = jbi.getElementsByTagName("configs");

                if ( configs.getLength() >= 1 )
                {
                    configsElem = (Element) configs.item(0);
                }
            }

            if ( configsElem != null )
            {
                // -- Get the config element for the target
                NodeList configList = configsElem.getElementsByTagName("config");
                for ( int i = 0; i < configList.getLength(); i++ )
                {
                    Element configElem = (Element) configList.item(i);
                    if ( (target + "-config").equals(configElem.getAttribute("name")))
                    {
                        targetConfig = configElem;
                    }
                }
            }
        }
        return targetConfig;
    }
    
    /**
     *
     * @return the component configuration properties for the component installed on the target 
     * if found, null otherwise.
     */
    private Properties getComponentConfigurationProperties(Element compRef)
    {
        Properties properties = new Properties();
        
        if ( compRef != null )
        {
            // -- Get the component config element for the component
            NodeList componentConfigs = compRef.getElementsByTagName("component-config");
            Element componentCfgElem = null;
            if ( componentConfigs.getLength() >= 1 )
            {
                componentCfgElem = (Element) componentConfigs.item(0);
            }


            if ( componentCfgElem != null )
            {
                 // Get all the properties under this node
                NodeList propertyList = componentCfgElem.getElementsByTagName("property");
                for ( int j=0;  j < propertyList.getLength(); j++ )
                {
                    Element propertyElem = (Element) propertyList.item(j);

                    // Get Name
                    Element nameElem = (Element) propertyElem.getElementsByTagName("name").item(0);
                    String name =  ((Text) nameElem.getFirstChild()).getNodeValue();

                    // Get value
                    Element valueElem = (Element) propertyElem.getElementsByTagName("value").item(0);
                    String value = ((Text) valueElem.getFirstChild()).getNodeValue();
                    properties.put(name, value);
                }
            }
        }
        return properties;
    }
    

    /**
     *
     * @return the components application variables 
     * if found. An empty array is returned if there aren't any variables et on the 
     * component.
     *
     * @param compRef
     */
    ComponentInfo.Variable[] getComponentApplicationVariables(Element compRef)
    {
        ComponentInfo.Variable[] appVars = new ComponentInfo.Variable[]{};
        
        if ( compRef != null )
        {
            // -- Get the component config element for the component
            NodeList componentConfigs = compRef.getElementsByTagName("component-config");
            Element componentCfgElem = null;
            if ( componentConfigs.getLength() >= 1 )
            {
                componentCfgElem = (Element) componentConfigs.item(0);
            }


            if ( componentCfgElem != null )
            {
                 // Get all the application variables 
                NodeList varList = componentCfgElem.getElementsByTagName("application-variable");
                appVars = new ComponentInfo.Variable[varList.getLength()];
                
                for ( int j=0;  j < varList.getLength(); j++ )
                {
                    Element varElem = (Element) varList.item(j);

                    // Get Name
                    Element nameElem = (Element) varElem.getElementsByTagName("name").item(0);
                    String name =  ((Text) nameElem.getFirstChild()).getNodeValue();

                    // Get value
                    Element valueElem = (Element) varElem.getElementsByTagName("value").item(0);
                    String value = ((Text) valueElem.getFirstChild()).getNodeValue();
                    
                    // Get type 
                    Element typeElem = (Element) varElem.getElementsByTagName("type").item(0);
                    String type = ((Text) typeElem.getFirstChild()).getNodeValue();
                    
                    if ((value != null) && (value.compareTo("null") == 0))
                    {
                        appVars[j] = new ComponentInfo.Variable(name, null, type);
                    }
                    else
                    {
                        appVars[j] = new ComponentInfo.Variable(name, value, type);
                    }
                }
            }
        }
        return appVars;
    }

        
    /**
     *
     * @return the components application configurations 
     * if found. An empty Map is returned if there aren't any named configurations.
     *
     * @param compRef - conponent ref element
     */
    Map<String, Properties> getComponentApplicationConfigurations(Element compRef)
    {
        Map<String, Properties> configMap = new HashMap();
        
        if ( compRef != null )
        {
            // -- Get the component config element for the component
            NodeList componentConfigs = compRef.getElementsByTagName("component-config");
            Element componentCfgElem = null;
            if ( componentConfigs.getLength() >= 1 )
            {
                componentCfgElem = (Element) componentConfigs.item(0);
            }


            if ( componentCfgElem != null )
            {
                 // Get all the application configurations
                NodeList configList = componentCfgElem.getElementsByTagName("application-configuration");
                
                for ( int j=0;  j < configList.getLength(); j++ )
                {
                    Element configElem = (Element) configList.item(j);

                    String configName = configElem.getAttribute("name");
                    
                    Properties props = getProperties(configElem);
                    
                    configMap.put(configName, props);
                }
            }
        }
        return configMap;
    }
    
    /**
     *
     * @return the "component-ref" element for the component installed on the target 
     * if found, null otherwise.
     */
    private Element getComponentRef(Element targetElement, String compName)
    {
        Element compRef = null;
        if ( targetElement != null )
        {
            // -- Get the specific components reference
            NodeList componentRefsList = targetElement.getElementsByTagName("component-ref");
            for ( int i = 0; i < componentRefsList.getLength(); i++ )
            {
                Element componentElem = (Element) componentRefsList.item(i);
                if ( (compName).equals(componentElem.getAttribute("name-ref")))
                {
                    compRef = componentElem;
                }
            }

        }
        return compRef;
    }
    
    
    
    /**
     * @return the target reference element
     */
    public Element getTarget(String target, boolean isServer)
    {
        Element targetRef = null;
        
        if ( mRegistryDocument != null )
        {
            // -- Get the JBI element
            Element jbi = mRegistryDocument.getDocumentElement();
            Element targetsElem = null;
            if ( jbi != null )
            {
                NodeList targets = null;
                if ( isServer )
                {
                    targets = jbi.getElementsByTagName("servers");
                }
                else
                {
                    targets = jbi.getElementsByTagName("clusters");
                }

                if ( targets.getLength() >= 1 )
                {
                    targetsElem = (Element) targets.item(0);
                }
            }

            if ( targetsElem != null )
            {
                // -- Get the specific target reference
                NodeList targetList = null;
                
                if ( isServer )
                {
                    targetList = targetsElem.getElementsByTagName("server");
                }
                else
                {
                    targetList = targetsElem.getElementsByTagName("cluster");
                }
                
                for ( int i = 0; i < targetList.getLength(); i++ )
                {
                    Element targetElem = (Element) targetList.item(i);
                    if ( (target).equals(targetElem.getAttribute("name-ref")))
                    {
                        targetRef = targetElem;
                        break;
                    }
                }
            }
        }
        return targetRef;
    }
    
    /**
     * @return the properties from a single element
     */
    Properties getProperties(Element element)
    {
        Properties properties = new Properties();
        // Get all the properties under this node
        NodeList propertyList = element.getElementsByTagName("property");
        for ( int j=0;  j < propertyList.getLength(); j++ )
        {
            Element propertyElem = (Element) propertyList.item(j);

            // Get Name
            Element nameElem = (Element) propertyElem.getElementsByTagName("name").item(0);
            String name =  ((Text) nameElem.getFirstChild()).getNodeValue();

            // Get value
            Element valueElem = (Element) propertyElem.getElementsByTagName("value").item(0);
            String value = ((Text) valueElem.getFirstChild()).getNodeValue();
            properties.put(name, value);
        }
        
        return properties;
    }
    
   
}
