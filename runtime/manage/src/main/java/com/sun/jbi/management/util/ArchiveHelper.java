/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ArchiveHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ValidatorHelper.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 2, 2006, 4:49 PM
 */

package com.sun.jbi.management.util;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.message.MessageBuilder;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.jbi.JBIException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 * Helper class for validators
 *
 * @author Sun Microsystems, Inc
 */
public class ArchiveHelper
{
    private static final String JBI_XML      = "jbi.xml";
    private static final String JBI_XML_PATH = "META-INF/" + JBI_XML;

    /**
     * registry schema file
     */
    public static final String JBI_DESCRIPTOR_SCHEMA = "jbi.xsd";
    
    /**
     * registry schema subdir in JBI_HOME
     */
    public static final String JBI_DESCRIPTOR_SCHEMA_DIR = "schemas";
    
    
    private Jbi             mJbiXml;
    private Unmarshaller        mReader;
    
    private StringTranslator    mTranslator;
    private MessageBuilder      mMsgBuilder;
    
    /**
     * descriptor schema path
     */
    private final URL mDescSchema;
            
    
    /**
     */
    public ArchiveHelper(com.sun.jbi.EnvironmentContext envCtx)
        throws JBIException

    {    
        // setup JAXB
        try
        {
            mTranslator = envCtx.getStringTranslator("com.sun.jbi.management");
            mMsgBuilder = new MessageBuilder(mTranslator);
            JAXBContext jc = JAXBContext.newInstance( "com.sun.jbi.management.descriptor", 
                    Class.forName ("com.sun.jbi.management.descriptor.Jbi").getClassLoader());
            mReader = jc.createUnmarshaller();

            /*
            File schemaDir = new File(envCtx.getJbiInstallRoot(), JBI_DESCRIPTOR_SCHEMA_DIR);
            mDescSchema = new File(schemaDir, JBI_DESCRIPTOR_SCHEMA);           
            */
            mDescSchema = getClass().getClassLoader().getResource("schemas/jbi.xsd");
                            
        }
        catch ( Exception ex)
        {
            
            String exMsg = ex.getMessage();
            if ( mMsgBuilder != null )
            {
                exMsg = mMsgBuilder.buildExceptionMessage("ArchiveHelper<init>", ex);
            }
            throw new JBIException(exMsg);
        }
     
    }
    
    
    /**
     * Load the jbi.xml from the archive
     */
    public Jbi loadJbiXml(File archiveFile, boolean validate)
        throws JBIException
    {
        archiveExistenceCheck(archiveFile);
        
        ZipFile zip = null;
        try
        {
             zip = new ZipFile(archiveFile);
             ZipEntry jbiXmlEntry = zip.getEntry(JBI_XML_PATH);        
        
            // process JBI metadata
            if (jbiXmlEntry == null)
            {
                String[] params = new String[]{archiveFile.getAbsolutePath(),
                        JBI_XML_PATH};
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_NO_DESCRIPTOR_IN_ARCHIVE, params);
                
                String jbiMsg = mMsgBuilder.buildFrameworkMessage("loadJbiXml",
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), params, mMsgBuilder.getMessageToken(errMsg));
                throw new javax.jbi.JBIException(jbiMsg);      
            }
            
            if (validate) 
            {
                mReader.setSchema(
                    javax.xml.validation.SchemaFactory.newInstance(
                        javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(mDescSchema));
            }
                
            mJbiXml = loadJbiXml(archiveFile.getName(), zip.getInputStream(jbiXmlEntry));            
        }
        catch (java.io.IOException ex)
        {
            String exMsg = mMsgBuilder.buildExceptionMessage("loadJbiXml", ex);
            throw new JBIException(exMsg);
        }
        catch (org.xml.sax.SAXException ex)
        {
            String exMsg = mMsgBuilder.buildExceptionMessage("loadJbiXml", ex);
            throw new JBIException(exMsg);
        }
        finally
        {
            if ( zip != null )
            {
                try
                {
                    zip.close();
                }
                catch (java.io.IOException ioex)
                {
                    String exMsg = mMsgBuilder.buildExceptionMessage("loadJbiXml", ioex);
                    throw new JBIException(exMsg);
                }
            }
        }
        return mJbiXml;
    }
    
    
    /**
     * @return the JAXB jbi type for jbi.xml from the input stream
     */
    private Jbi loadJbiXml(String entryName, InputStream jbiXmlStream)
        throws JBIException
    {
        Jbi jbiXml = null;
        
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        
        try
        {
            try {
                Thread.currentThread().setContextClassLoader(
                        Class.forName ("com.sun.jbi.management.descriptor.Jbi").getClassLoader());
                
                jbiXml = (Jbi)mReader.unmarshal(jbiXmlStream);
            } catch (ClassNotFoundException ex) {}
        }        
        catch (javax.xml.bind.JAXBException jEx)
        {   
            String message = 
                    jEx.getLinkedException() != null ?
                    jEx.getLinkedException().getMessage() : jEx.getMessage(); 
            if (message == null)
            {
                message = jEx.toString();
            }            
            
            // -- schema validation failed      
            String[] params = new String[]{entryName, message};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_ARCHIVE_DESCRIPTOR_NOT_SCHEMA_VALID, params);
            String jbiMsg = mMsgBuilder.buildFrameworkMessage("loadJbiXml",
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), params, 
                mMsgBuilder.getMessageToken(errMsg));
            
            throw new JBIException(jbiMsg);
        } finally {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
        return jbiXml;
    }
    
    /**
     * @throws a JBIException if the archive file does not exist or is a zero length file.
     */
    private void archiveExistenceCheck(File archiveFile)
        throws javax.jbi.JBIException
    {
            if ( !archiveFile.exists() )
            {
                String[] params = new String[]{archiveFile.getAbsolutePath()};
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_ARCHIVE_NONEXISTENT, params);
                
                String jbiMsg = mMsgBuilder.buildFrameworkMessage("loadJbiXml",
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), params, mMsgBuilder.getMessageToken(errMsg));
                throw new javax.jbi.JBIException(jbiMsg);   
            }
            
            if ( archiveFile.length() == 0 )
            {
                String[] params = new String[]{archiveFile.getAbsolutePath()};
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_ARCHIVE_EMPTY, params);
                
                String jbiMsg = mMsgBuilder.buildFrameworkMessage("loadJbiXml",
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), params, mMsgBuilder.getMessageToken(errMsg));
                throw new javax.jbi.JBIException(jbiMsg);   
            }
    }
}
