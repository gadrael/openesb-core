/*  
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.Set;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.ArrayType;

/**
 * This class is used to store statistic information about a SA
 * @author Sun Microsystems, Inc.
 */
public class ServiceAssemblyStatistics
{
    /**
     * name of the sa
     */
    private String mName;
    
    /**
     * instance name 
     */
    private String mInstanceName;
    
    /**
     * time this SA was started last
     */
    private Date mLastStartupTime;
    
    /**
     * time this SA was stopped last
     */
    private Date mLastStopTime;
    
    /**
     * time this SA was shutdown last
     */
    private Date mLastShutdownTime;
    
    /**
     *  SA startup times
     */    
    private Value mStartupTime = new Value();
    
    /**
     * SA stop times
     */
    private Value mStopTime = new Value();
    
    /**
     * SA shutdown times
     */
    private Value mShutdownTime = new Value();
    
    /**
     * boolean to track if SA has been started before
     */
    private boolean mSAStarted = false;
    
    /**
     *  boolean to track if SA has been stopped before
     */
    private boolean mSAStopped = false;
    
    /**
     * boolean to track if SA has been shutdown before
     */
    private boolean mSAShutdown = false;
    
    /**
     * CompositeType for SU Stats
     */
    private CompositeType mServiceUnitStatsType;
    
    /**
     * CompositeType for SA Stats
     */
    private CompositeType mServiceAssemblyStatsType;
    
    /**
     * a list of SU stats
     */
    private Map<String, ServiceUnitStatistics> 
            mServiceUnitStatistics = new HashMap<String, ServiceUnitStatistics>();
    
    /** 
     * constructor 
     */
    ServiceAssemblyStatistics(String saName, String instanceName)
    throws Exception
    {
        mName = saName;
        mInstanceName = instanceName;
    }
    
    /**
     * get last startup time
     * @return Date last startup time
     */
    public Date getLastStartupTime()
    {
        return mLastStartupTime;
    }

    /**
     * get last stop time
     * @return Date last stop time
     */
    public Date getLastStopTime()
    {
        return mLastStopTime;
    }        

    /**
     * get last shutdown time
     * @return Date last stop time
     */
    public Date getLastShutdownTime()
    {
        return mLastShutdownTime;
    }       

    /** 
     * get startup times
     * @return Value startup times
     */
    public Value getStartupTime()
    {
        return mStartupTime;
    }

    /** 
     * get stop times
     * @return Value stop times
     */
    public Value getStopTime()
    {
        return mStopTime;
    }

    /** 
     * get shutdown times
     * @return Value shutdown times
     */
    public Value getShutdownTime()
    {
        return mShutdownTime;
    }
        
    /**
     * get the name of the su
     */
    public String getName()
    {
        return mName;
    }    
    
    /**
     * get the instance name
     * @return String the instance name
     */
    public String getInstanceName()
    {
        return mInstanceName;
    }
    
    /**
     * set the su list
     * @param serviceUnitStats su stats object list
     */
    public void setServiceUnitList(Map<String, ServiceUnitStatistics> serviceUnitStats)
    {
        mServiceUnitStatistics = serviceUnitStats;
    }   
    
    /**
     * this method is used to update the statistics of a SA
     * @param beginTime time when the start operation was begun
     * @param endTime time when the start operation finished
     * @param suTimes map of suNames and their startupTimes
     */
    public void updateStartupStatistics(Date beginTime, Date endTime, Map<String, Long> suTimes)
    {
        mSAStarted = true;
        mLastStartupTime = endTime;
        mStartupTime.addSample(endTime.getTime() - beginTime.getTime());
        
        Set<String> serviceUnits = mServiceUnitStatistics.keySet();
        for (String su : serviceUnits)
        {
            if (suTimes.containsKey(su))
            {
                mServiceUnitStatistics.get(su).updateStartupStatistics(suTimes.get(su));
            }    
        }
    }

    /**
     * this method is used to update the statistics of a SA
     * @param beginTime time when the stop operation was begun
     * @param endTime time when the stop operation finished
     * @param suTimes map of suNames and their stopTimes
     */
    public void updateStopStatistics(Date beginTime, Date endTime, Map<String, Long> suTimes)
    {
        mSAStopped = true;
        mLastStopTime =  endTime;
        mStopTime.addSample(endTime.getTime() - beginTime.getTime());
        
        Set<String> serviceUnits = mServiceUnitStatistics.keySet();
        for (String su : serviceUnits)
        {
            if (suTimes.containsKey(su))
            {
                mServiceUnitStatistics.get(su).updateStopStatistics(suTimes.get(su));
            }    
        }        
    }

    /**
     * this method is used to update the statistics of a SA
     * @param beginTime time when the shutdown operation was begun
     * @param endTime time when the shutdown operation finished
     * @param suTimes map of suNames and their shutdownTimes
     */
    public void updateShutdownStatistics(Date beginTime, Date endTime, Map<String, Long> suTimes)
    {
        mSAShutdown = true;
        mLastShutdownTime =  endTime;
        mShutdownTime.addSample(endTime.getTime() - beginTime.getTime());
        
        Set<String> serviceUnits = mServiceUnitStatistics.keySet();
        for (String su : serviceUnits)
        {
            if (suTimes.containsKey(su))
            {
                mServiceUnitStatistics.get(su).updateShutdownStatistics(suTimes.get(su));
            }    
        }   
    }

    /**
     * return the stats as composite data
     */
    public CompositeData getCompositeData()
    throws OpenDataException
    {

        Set<String> serviceUnits = mServiceUnitStatistics.keySet();
        CompositeData[] serviceUnitStats = new CompositeData[serviceUnits.size()];
        CompositeType serviceUnitStatsCompositeType = null;
        int i=0;
        
        for(String su : serviceUnits)
        {
            CompositeData cData = mServiceUnitStatistics.get(su).getCompositeData();
            serviceUnitStats[i++] = cData;
            serviceUnitStatsCompositeType = cData.getCompositeType();
        }
        long upTime = 0;
        if (getLastStartupTime() != null)
        {
            upTime = System.currentTimeMillis() - getLastStartupTime().getTime();
        }
        
        ArrayList<String> saStatsItemNames = new ArrayList<String>();
        ArrayList<String> saStatsItemDescriptions = new ArrayList<String>();
        ArrayList<OpenType> saStatsItemTypes = new ArrayList<OpenType>();
        ArrayList<Object> saStatsItemValues = new ArrayList<Object>();

        saStatsItemNames.add(INSTANCE_NAME);
        saStatsItemNames.add(SERVICE_ASSEMBLY_NAME);

        saStatsItemDescriptions.add(JBI_INSTANCE_NAME_DESCRIPTION);
        saStatsItemDescriptions.add(SERVICE_ASSEMBLY_NAME_DESCRIPTION);
        
        saStatsItemTypes.add(SimpleType.STRING);
        saStatsItemTypes.add(SimpleType.STRING);
        
        saStatsItemValues.add(getInstanceName());
        saStatsItemValues.add(getName());
        
        if (mSAStarted)
        {
            saStatsItemNames.add(SERVICE_ASSEMBLY_UPTIME);
            saStatsItemNames.add(SERVICE_ASSEMBLY_LAST_STARTUP_TIME);
            saStatsItemNames.add(SERVICE_ASSEMBLY_STARTUP_TIME);
            
            saStatsItemDescriptions.add(SERVICE_ASSEMBLY_UPTIME_DESCRIPTION);
            saStatsItemDescriptions.add(SERVICE_ASSEMBLY_LAST_STARTUP_TIME_DESCRIPTION);
            saStatsItemDescriptions.add(SERVICE_ASSEMBLY_STARTUP_TIME_DESCRIPTION);
            
            saStatsItemTypes.add(SimpleType.LONG);
            saStatsItemTypes.add(SimpleType.DATE);
            saStatsItemTypes.add(SimpleType.LONG);
            
            saStatsItemValues.add(upTime);
            saStatsItemValues.add(getLastStartupTime());
            saStatsItemValues.add((long)getStartupTime().getAverage());            
            
        }
        if (mSAStopped)
        {
            saStatsItemNames.add(SERVICE_ASSEMBLY_STOP_TIME);
            saStatsItemDescriptions.add(SERVICE_ASSEMBLY_STOP_TIME_DESCRIPTION);   
            saStatsItemTypes.add(SimpleType.LONG);
            saStatsItemValues.add((long)getStopTime().getAverage());
        }
        if(mSAShutdown)
        {
            saStatsItemNames.add(SERVICE_ASSEMBLY_SHUTDOWN_TIME);
            saStatsItemDescriptions.add(SERVICE_ASSEMBLY_SHUTDOWN_TIME_DESCTIPTION);   
            saStatsItemTypes.add(SimpleType.LONG);
            saStatsItemValues.add((long)getShutdownTime().getAverage());            
        }
        if (serviceUnitStats != null && serviceUnitStatsCompositeType != null)
        {
            saStatsItemNames.add(SERVICE_ASSEMBLY_SU_STATISTICS);
            saStatsItemDescriptions.add(SERVICE_ASSEMBLY_SU_STATISTICS_DESCRIPTION);   
            saStatsItemTypes.add(new ArrayType(1, serviceUnitStatsCompositeType));
            saStatsItemValues.add(serviceUnitStats);            
        }
        
               
        return new CompositeDataSupport(
                new CompositeType(
                    SU_STATISTICS_ITEM_NAME,
                    SU_STATISTICS_ITEM_DESCRIPTION,                    
                   (String[])saStatsItemNames.toArray(new String[]{}),
                   (String[])saStatsItemDescriptions.toArray(new String[]{}),
                   (OpenType[])saStatsItemTypes.toArray(new OpenType[]{})),
                (String[])saStatsItemNames.toArray(new String[]{}),
                (Object[])saStatsItemValues.toArray(new Object[]{}));            

        
       
    }
    
    
    class ServiceUnitStatistics
    {
        /**
         * su name
         */
        private String mName;
        
        /**
         *  SA startup times
         */    
        private Value mStartupTime = new Value();

        /**
         * SA stop times
         */
        private Value mStopTime = new Value();

        /**
         * SA shutdown times
         */
        private Value mShutdownTime = new Value();      
        
        /**
         * list of endpoints 
         */
        private List mEndpoints = new ArrayList();
        
        /**
         * boolean to track if this SU has been started before
         */
        private boolean mServiceUnitStarted = false;
         
        /**
         * boolean to track if this SU has been stopped before
         */
        private boolean mServiceUnitStopped = false;
        
        /**
         * boolean to track if this SU has been started before
         */
        private boolean mServiceUnitShutdown = false;
        
        
        /** 
         * constructor 
         */
        ServiceUnitStatistics(String suName)
        {
            mName = suName;
        }
    
        /** 
         * get startup times
         * @return Value startup times
         */
        public Value getStartupTime()
        {
            return mStartupTime;
        }
        
        /** 
         * get stop times
         * @return Value stop times
         */
        public Value getStopTime()
        {
            return mStopTime;
        }
        
        /** 
         * get shutdown times
         * @return Value shutdown times
         */
        public Value getShutdownTime()
        {
            return mShutdownTime;
        }        
        
        /**
         * get the name of the su
         */
        public String getName()
        {
            return mName;
        }
        
        /**
         * sets the list of endpoints in this SU
         * @param endpointList endpoints
         */
        public void setEndpointsList(List<String> endpoints)
        {
            mEndpoints = endpoints;
        }
        
        /**
         * This method is used to get a list of the endpoints 
         * in this SU
         */
        public List<String> getEndpointsList()
        {
            return mEndpoints;
        }
        
        
        /** 
         * This method is used to update the startup statistics 
         * for this SU.
         * @param startupTime startuptime
         */
        public void updateStartupStatistics(Long startupTime)
        {
            mServiceUnitStarted = true;
            mStartupTime.addSample(startupTime);
        }
        
        /** 
         * This method is used to update the stop time statistics 
         * for this SU.
         * @param stopTime stopTime
         */
        public void updateStopStatistics(Long stopTime)
        {
            mServiceUnitStopped = true;
            mStopTime.addSample(stopTime);
        }
        
        /** 
         * This method is used to update the startup statistics 
         * for this SU.
         * @param shutdownTime shutdownTime
         */
        public void updateShutdownStatistics(Long shutdownTime)
        {
            mServiceUnitShutdown = true;
            mShutdownTime.addSample(shutdownTime);
        }        
        
        /** 
         * This method is used to obtain a CompositeData represenation
         * of the collected statistics 
         * @return CompositeData
         */
        public CompositeData getCompositeData()
        throws OpenDataException
        {

            ArrayList<String> suStatsItemNames = new ArrayList<String>();
            ArrayList<String> suStatsItemDescriptions = new ArrayList<String>();
            ArrayList<OpenType> suStatsItemTypes = new ArrayList<OpenType>();
            ArrayList<Object> suStatsItemValues = new ArrayList<Object>();
            
            suStatsItemNames.add(SERVICE_UNIT_NAME);
            suStatsItemDescriptions.add(SERVICE_UNIT_NAME_DESCRIPTION);
            suStatsItemTypes.add(SimpleType.STRING);
            suStatsItemValues.add(getName());
            
            if(mServiceUnitStarted)
            {
                suStatsItemNames.add(SERVICE_UNIT_STARTUP_TIME);
                suStatsItemDescriptions.add(SERVICE_UNIT_STARTUP_TIME_DESCRIPTION);                
                suStatsItemTypes.add(SimpleType.LONG);                
                suStatsItemValues.add((long)getStartupTime().getAverage());
            }
            if (mServiceUnitStopped)
            {
                suStatsItemNames.add(SERVICE_UNIT_STOP_TIME);                
                suStatsItemDescriptions.add(SERVICE_UNIT_STOP_TIME_DESCRIPTION);     
                suStatsItemTypes.add(SimpleType.LONG);                
                suStatsItemValues.add((long)getStopTime().getAverage());                
            }
            if (mServiceUnitShutdown)
            {
                suStatsItemNames.add(SERVICE_UNIT_SHUTDOWN_TIME);     
                suStatsItemDescriptions.add(SERVICE_UNIT_SHUTDOWN_TIME_DESCRIPTION);   
                suStatsItemTypes.add(SimpleType.LONG);                                
                suStatsItemValues.add((long)getShutdownTime().getAverage());                
            }
 
            return new CompositeDataSupport(
                    new CompositeType(
                        SU_STATISTICS_ITEM_NAME,
                        SU_STATISTICS_ITEM_DESCRIPTION,                    
                       (String[])suStatsItemNames.toArray(new String[]{}),
                       (String[])suStatsItemDescriptions.toArray(new String[]{}),
                       (OpenType[])suStatsItemTypes.toArray(new OpenType[]{})),
                    (String[])suStatsItemNames.toArray(new String[]{}),
                    (Object[])suStatsItemValues.toArray(new Object[]{}));            
                    
        }                
    }
    
    class Value 
    {

        /**
         *  sum of all the values
         */
        private double  sum;
        
        /**
         * squared sum
         */
        private double  squaredsum;
        
        /**
         * min value
         */
        private long    min;
        
        /**
         * max value
         */
        private long    max;
         
        /**
         * count 
         */ 
        private long    count;
    
        /** reset value */
        public void zero()
        {
            sum = 0.0;
            squaredsum = 0.0;
            min = 0;
            max = 0;
            count = 0;
        }
    
        /**
         * add a sample
         * @param sample
         */
        public void addSample(long sample)
        {
            if (sample < min || count == 0)
            {
                min = sample;
            }
            if (sample > max || count == 0)
            {
                max = sample;
            }
            count++;
            sum += sample;
            squaredsum += ((double)sample)*((double)sample);
        }
        
        /**
         * get the average
         * @return double the average
         */
        public double getAverage()
        {
            return (sum / count);
        }
    
        /** 
         * get the minimum value 
         * @return long min value
         */
        public long getMin()
        {
            return (min);
        }
    
        /** 
         * get the maximum value 
         * @return long max value
         */        
        public long getMax()
        {
            return (max);
        }
    
        /** 
         * get the number of values 
         * @return long count
         */
        public long getCount()
        {
            return (count);
        }
    
    
        /** 
         * a stirng representation 
         */
        public String toString()
        {
            StringBuilder        sb = new StringBuilder();

            sb.append("Count("+count+")");
            sb.append("Min("+min+")");
            sb.append("Avg("+getAverage()+")");
            sb.append("Max("+max+")");

            return (sb.toString());
        }
        
    }
    
    
    /**
     * name for SA statistics table item
     */
    public static String SA_STATISTICS_ITEM_NAME = "ServiceAssemblyStatisticsItem";
    
    /**
     * description for SA statistics table item
     */
    public static String SA_STATISTICS_ITEM_DESCRIPTION = "SA statistics for an instance";    
    
    
    /**
     * name for SU statistics table item
     */
    public static String SU_STATISTICS_ITEM_NAME = "ServiceUnitStatisticsItem";
    
    /**
     * description for SU statistics table item
     */
    public static String SU_STATISTICS_ITEM_DESCRIPTION = "Service unit statistics for an instance";    
        
    
    /**
     * service unit name
     */
    public static String SERVICE_UNIT_NAME = "ServiceUnitName";
    
    /**
     * service unit startup time
     */
    public static String SERVICE_UNIT_STARTUP_TIME = "ServiceUnitStartupTime Avg (ms)";
    
    /**
     * service unit stop time
     */
    public static String SERVICE_UNIT_STOP_TIME = "ServiceUnitStopTime Avg (ms)";
    
    /**
     * service unit shutdown time
     */
    public static String SERVICE_UNIT_SHUTDOWN_TIME = "ServiceUnitShutdownTime Avg (ms)";

    /**
     * instance name
     */
    public static String INSTANCE_NAME = "InstanceName";
            
    /**
     * SA Name
     */
    public static String SERVICE_ASSEMBLY_NAME = "ServiceAssemblyName";
    
    /**
     * SA Uptime
     */
    public static String SERVICE_ASSEMBLY_UPTIME = "UpTime (ms)";
    
    /**
     * last startup time
     */
    public static String SERVICE_ASSEMBLY_LAST_STARTUP_TIME = "LastStartupTime";
    
    /**
     * SA startup time
     */
    public static final String SERVICE_ASSEMBLY_STARTUP_TIME = "StartupTime Avg (ms)";
    
    /**
     * SA stop time
     */
    public static final String SERVICE_ASSEMBLY_STOP_TIME = "StopTime Avg (ms)";
    
    /**
     * SA Shutdown time
     */
    public static final String SERVICE_ASSEMBLY_SHUTDOWN_TIME = "ShutdownTime Avg (ms)";
    
    /**
     * SA list of SU statistics
     */
    public static final String SERVICE_ASSEMBLY_SU_STATISTICS = "ServiceUnitStatistics";
    
    /** 
     * Service unit name description
     */
    public static final String SERVICE_UNIT_NAME_DESCRIPTION = "Name of the service unit";
    
    /**
     * Average of service unit startup times in ms.
     */
    public static final String SERVICE_UNIT_STARTUP_TIME_DESCRIPTION = "Service Unit StartupTime Avg (ms)";
    
    /**
     * Average of service unit stop times in ms.
     */
    public static final String SERVICE_UNIT_STOP_TIME_DESCRIPTION = "Service Unit StopTime Avg (ms)";
    
    /**
     * Average of service unit shutdown time
     */
    public static final String SERVICE_UNIT_SHUTDOWN_TIME_DESCRIPTION = "Service Unit ShutdownTime Avg (ms)";
    
    /**
     * instance name description
     */
    public static final String JBI_INSTANCE_NAME_DESCRIPTION = "JBI Instance Name";
    
    /**
     * SA name description
     */
    public static final String SERVICE_ASSEMBLY_NAME_DESCRIPTION = "Service Assembly Name";
    
    /**
     * SA uptime desctiption
     */
    public static final String SERVICE_ASSEMBLY_UPTIME_DESCRIPTION = "Uptime of service assembly in ms.";
    /**
     * SA startup time description
     */
    public static final String SERVICE_ASSEMBLY_LAST_STARTUP_TIME_DESCRIPTION = "Last Startup Time";
    
    /**
     * SA startup time - description 
     */
    public static final String SERVICE_ASSEMBLY_STARTUP_TIME_DESCRIPTION = "Startup Time Avg (ms)";
    
    /**
     * SA stop time - description
     */
    public static final String SERVICE_ASSEMBLY_STOP_TIME_DESCRIPTION = "Stop Time Avg (ms)";
    
    /**
     * SA shutdown time - description 
     */
    public static final String SERVICE_ASSEMBLY_SHUTDOWN_TIME_DESCTIPTION = "Shutdown Time Avg (ms)";
    
    /**
     * SU statistics list - description
     */
    public static final String SERVICE_ASSEMBLY_SU_STATISTICS_DESCRIPTION = "ServiceUnit Statistics";
    

}
