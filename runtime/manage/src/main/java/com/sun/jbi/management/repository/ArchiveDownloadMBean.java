/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ArchiveDownloadMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

/**
 *  Downloads archive bytes from the ESB repository.
 */
public interface ArchiveDownloadMBean
{
    /** 
     *  Returns the archive id of a service assembly in the repository.
     *  @param saName name of the service assembly
     *  @return archive ID, or null if the service assembly does not exist
     *  in the repository.
     */
    Object getServiceAssemblyArchiveId(String saName);
    
     /** 
     *  Returns the archive id of a component in the repository.
     *  @param componentName name of the component
     *  @return archive ID, or null if the component does not exist
     *  in the repository.
     */
    Object getComponentArchiveId(String componentName);
    
    /** 
     *  Returns the archive id of a shared library in the repository.
     *  @param sharedLibrayName name of the shared library
     *  @return archive ID, or null if the shared library does not exist
     *  in the repository.
     */
    Object getSharedLibraryArchiveId(String sharedLibrayName);
    
    /** Initiates the download of an archive identified by 'archiveId'.  In
     *  most situations, the archiveId will simply be the CAS-local path to
     *  the archive in the ESB repository.  The returned Object is used as
     *  a session id for downloading the archive bytes with
     *  <code>downloadBytes()</code>.
     *  @return download session id
     */
    Object initiateDownload(Object archiveId)
        throws java.io.IOException;
    
    /** Initiates the download of the Registry. 
     *  @return download session id
     */
    Object initiateRegistryDownload()
        throws java.io.IOException;
    
    /** Download <code>length</code> bytes from an existing download session.
     *  @return a byte array of the specified length.  If the remaining bytes
     *  in the session <= length, the byte array is sized appropriately.  If
     *  all bytes have been downloaded, the returned array length is 0.
     */
    byte[] downloadBytes(Object id, int length)
        throws java.io.IOException;
    
    /** Used to indicate that a download session is complete.
     */
    void terminateDownload(Object id)
        throws java.io.IOException;
}
