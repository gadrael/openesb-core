/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentQueryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ComponentQueryImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.registry.xml;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.registry.data.ServiceUnitInfoImpl;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.util.LockManager;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceUnitInfo;
import java.util.logging.Level;

import javax.jbi.component.Component;

/**
 * This class encapsulates queries which are common to 
 * all targets ( domain / server / cluster ).
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentQueryImpl
    implements com.sun.jbi.ComponentQuery
{
 
    private Jbi               mJbiRegistry;
    private RegistryImpl      mRegistry;
    private boolean           mValidate;
    private ManagementContext mMgtCtx;
    private Logger            mLogger;
    private StringTranslator  mTranslator;
    private GenericQueryImpl  mGenQuery;
    private ObjectFactory     mObjectFactory;
    private String            mTarget;
    private Component         mComponentInstance;
    private LockManager       mRegObjLM;
    
    private static String     TARGET_DOMAIN = "domain";
    
    public ComponentQueryImpl(Jbi jbi, ManagementContext mgtCtx, boolean validate,
        String targetName, RegistryImpl registry)
        throws RegistryException
    {
        mTarget = targetName;
        mJbiRegistry = jbi;
        mRegistry = registry;
        mValidate = validate;
        mMgtCtx = mgtCtx;
        mLogger = mMgtCtx.getLogger();
        mTranslator = 
            mMgtCtx.getEnvironmentContext().getStringTranslator("com.sun.jbi.management");
        
        mGenQuery = (GenericQueryImpl) mRegistry.getGenericQuery();
        
        mObjectFactory = new ObjectFactory();
        
        mRegObjLM = registry.getRegistryObjectLockManager();
    }

    /*--------------------------------------------------------------------------------*\
     *                          Query Ops                                             *
    \*--------------------------------------------------------------------------------*/
 
      /**
    * Get a list of component IDs for all registered components of a specified
    * type.
    * @param type The ComponentType: { BINDING, ENGINE, SHARED_LIBRARY, 
    * BINDINGS_AND_ENGINES, ALL }. ALL includes allcomponents regardless of their types.
    * ComponentInfo.BINDINGS_AND_ENGINES includes both bindings and engines.
    * @return A List of component IDs ( String ) of all registered components
    * of the requested type. Returns an empty list of no components were found.
    */
    public List<String> getComponentIds(ComponentType type)
    {
        List<String> list = new ArrayList();
       
        try
        {
            switch ( type )
            {
                case ALL:
                    list = getSharedLibraries(getAllComponents());
                    break;
                case BINDINGS_AND_ENGINES :
                    list = getAllComponents();
                    break;
                case BINDING :
                    list = getBindings();
                    break;
                case ENGINE :
                    list = getEngines();
                    break;    
                case SHARED_LIBRARY:
                    list = getSharedLibraries(list);
                    break;
            }
        
        }
        catch (RegistryException rex)
        {
            mLogger.severe(rex.getMessage());
        }
        
        return list;
    }
 
   /**
    * Get a list of component IDs for all components of a specified type with
    * a specified status.
    * @param type The ComponentType: { BINDING, ENGINE, SHARED_LIBRARY, 
    * BINDINGS_AND_ENGINES, ALL }. ALL includes allcomponents regardless of their types.
    * ComponentInfo.BINDINGS_AND_ENGINES includes both bindings and engines.
    * @param status The Component status: ComponentState{LOADED, SHUTDOWN, STARTED, 
    * STOPPED}
    * @return A List of component IDs ( String ) of all registered components
    * of the requested type with the requested status. Returns an empty list if
    * no components were found.
    */
    public List<String> getComponentIds(ComponentType type, ComponentState status)
    {
        List<String> list = new ArrayList();
       
        try
        {
            switch ( type )
            {
                case ALL:
                    list = getSharedLibraries(getAllComponents(status), status);
                    break;
                case BINDINGS_AND_ENGINES :
                    list = getAllComponents(status);
                    break;
                case BINDING :
                    list = getBindings(status);
                    break;
                case ENGINE :
                    list = getEngines(status);
                    break;    
                case SHARED_LIBRARY:
                    list = getSharedLibraries(list, status);
                    break;
            }
        
        }
        catch (RegistryException rex)
        {
            mLogger.severe(rex.getMessage());
        }
        
        return list;
    }
 
   /**
    * Get the ComponentInfo for a particular Component.
    * @param componentName The unique name of the component being retrieved.
    * @return The ComponentInfo for the requested component or null if the
    * component is not registered.
    */
    public ComponentInfo getComponentInfo(String componentName)
    {
     
        try
        {
            mLogger.log(Level.FINEST, "Registry retrieving component information for {0} on target {1}", new Object[]{componentName, mTarget});
            if ( mGenQuery.isComponentRegistered(componentName) )
            {   
                com.sun.jbi.ComponentInfo  compInfo = null;
                mRegObjLM.acquireReadLock();
                try
                {
                    if ( TARGET_DOMAIN.equals(mTarget) )
                    {
                        compInfo =  
                                ObjectTranslator.getRegistryComponent(componentName, mGenQuery);

                        ((com.sun.jbi.management.registry.data.ComponentInfoImpl) compInfo).setStatus(getStatus(componentName));
                    }
                    else
                    {
                        ComponentRefType compRef = mGenQuery.getComponent(componentName, mTarget);
                        compInfo = ObjectTranslator.getRegistryComponent(compRef, mGenQuery);   
                        // -- Update the Service Unit file paths
                        if ( compInfo != null )
                        {
                            //updateServiceUnits(compInfo.getServiceUnitList(), componentName);  // SU Path is set from ServiceUnitInfoImpl, on demand
                        }
                    }
                }
                finally
                {
                    mRegObjLM.releaseReadLock();
                    return compInfo;
                }
            }
            else
            {
                mLogger.log(Level.FINEST, "Component {0} not registered in domain", componentName);
            }
            
        }
        catch ( Exception rex )
        {
            rex.printStackTrace();
            mLogger.severe(rex.getMessage());
        }
        return null;   
    }
    
   /**
    * Get the ComponentInfo for a particular Shared Library.
    * @param sharedLibraryName The unique name of the Shared Library being
    * retrieved.
    * @return The ComponentInfo for the requested Shared Library or null if the
    * Shared Library is not registered.
    */
    public ComponentInfo getSharedLibraryInfo(String sharedLibraryName)
    {
        try
        {
            if ( mGenQuery.isSharedLibraryRegistered(sharedLibraryName))
            {
                if ( TARGET_DOMAIN.equals(mTarget) )
                {
                    ComponentInfo 
                        slInfo = ObjectTranslator.getRegistrySharedLibrary( mGenQuery, sharedLibraryName);
                    return slInfo;
                }
                else
                {
                    ComponentInfo slInfo = null;
                    mRegObjLM.acquireReadLock();
                    try
                    {
                        SharedLibraryRefType slRef = mGenQuery.getSharedLibrary(sharedLibraryName, mTarget);
                        slInfo = ObjectTranslator.getRegistrySharedLibrary(mGenQuery, slRef);
                    }
                    finally
                    {
                        mRegObjLM.releaseReadLock();
                    }
                    return slInfo;
                }
            }
            
        }
        catch ( RegistryException rex )
        {
            mLogger.severe(rex.getMessage());
        }
        return null;
    }
    

   /**
    * Get a list of component IDs that depend upon a specified Shared Library.
    * @param sharedLibraryName The unique name of the Shared Library.
    * @return A list of the component IDs of all components that depend upon the
    * Shared Library. If none are found, the list is empty.
    */
    public List getDependentComponentIds(String sharedLibraryName)
    {
        
        List<String> depComponents = new ArrayList();
        
        try
        {
            if ( mGenQuery.isSharedLibraryRegistered(sharedLibraryName) )
            {
                List<String> allComponents = getAllComponents();

                for ( String component : allComponents )
                {
                    com.sun.jbi.management.descriptor.Jbi jbi = mGenQuery.getComponentJbi(component);

                    List<com.sun.jbi.management.descriptor.Component.SharedLibrary> 
                        slList = jbi.getComponent().getSharedLibraryList();


                    for ( com.sun.jbi.management.descriptor.Component.SharedLibrary sl : slList )
                    {
                        if ( sl.getContent().equals(sharedLibraryName))
                        {
                            depComponents.add(component);
                        }
                    }
                }
            }
        }
        catch( RegistryException rex )
        {
            mLogger.severe(rex.getMessage());
        }
        return depComponents;
    }
    
   /**
    * Get the current status of a component.
    * @param componentName The unique component name.
    * @return The current status of the component: {SHUTDOWN, LOADED, STARTED, STOPPED}
    * @throws javax.jbi.JBIException if the component name is not registered.
    */
    public ComponentState getStatus(String componentName)
        throws javax.jbi.JBIException
    {
        if ( TARGET_DOMAIN.equals(mTarget))
        {
            List<String> targets = mGenQuery.getServersInstallingComponent(componentName);
            targets.addAll( mGenQuery.getClustersInstallingComponent(componentName) );
            List<ComponentState> states = new ArrayList<ComponentState>();
            for ( String target : targets )
            {
                states.add(mGenQuery.getComponentState(componentName, target));
            }
            return ComponentState.computeEffectiveState(states);   
        }
        else
        {
            return mGenQuery.getComponentState(componentName, mTarget);
        }
    }
    
    /*----------------------------------------------------------------------------------*\
     *                          Private Helpers                                         *
    \*----------------------------------------------------------------------------------*/
    
    private List<String> getAllComponents()
        throws RegistryException
    {
        List<String> components = new ArrayList();
        List<String> compNames  = new ArrayList();
        
        if ( !TARGET_DOMAIN.equals(mTarget) )
        {
            mRegObjLM.acquireReadLock();
            try
            {

                InstalledComponentsListType target = null;
                try
                {
                    target = getTarget(mTarget);
                }
                catch ( RegistryException rex)
                {
                    throw rex;
                }

                if ( target != null )
                {
                    List<ComponentRefType> compRefs =  target.getComponentRef();
                    for( ComponentRefType compRef : compRefs )
                    {
                        String compName = compRef.getNameRef();    
                        compNames.add(compName);
                    }
                }
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
            
        }
        else
        {
            mRegObjLM.acquireReadLock();
            try
            {
                List<DomainComponentType> compList = getDomainComponents();
                for( DomainComponentType comp : compList )
                {
                    String compName = comp.getName();
                    compNames.add(compName);
                }   
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
        }
        
        for( String compName : compNames )
        {
            if ( mGenQuery.isComponentRegistered(compName) )
            {
                components.add(compName);
            }
        }
        return components;
    }
    
    
    private List<String> getBindings()
        throws RegistryException
    {
        List<String> components = getAllComponents();
        List<String> bindings = new ArrayList();
        
        for( String compName : components )
        {
            
            if ( mGenQuery.isComponentRegistered(compName)  &&
                 mGenQuery.getComponentType(compName) == ComponentType.BINDING )
            {
                bindings.add(compName);
            }
        }
        
        return bindings;
    }
    
    
    private List<String> getEngines()
        throws RegistryException
    {
        List<String> components = getAllComponents();
        List<String> engines = new ArrayList();
        
        for( String compName : components )
        {
            if ( mGenQuery.isComponentRegistered(compName)  &&
                 mGenQuery.getComponentType(compName) == ComponentType.ENGINE )
            {
                engines.add(compName);
            }
        }
        
        return engines;
    }
    
    private List<String> getSharedLibraries(List<String> list)
        throws RegistryException
    {
        if ( list == null )
        {
            list = new ArrayList();
        }
        List<String> slNames = new ArrayList();
        
        if ( !TARGET_DOMAIN.equals(mTarget) )
        {
            InstalledComponentsListType target = null;
            
            mRegObjLM.acquireReadLock();
            try
            {
                target = getTarget(mTarget);
                
                if ( target != null )
                {
                    List<SharedLibraryRefType> slRefs =  target.getSharedLibraryRef();
                    for( SharedLibraryRefType slRef : slRefs )
                    {
                        String slName = slRef.getNameRef();
                        slNames.add(slName);
                    }
                }
            }
            catch ( RegistryException rex)
            {
                throw rex;
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
        }
        else
        {
            mRegObjLM.acquireReadLock();
            try
            {
                List<DomainSharedLibraryType> slList = getDomainSharedLibraries();
                for( DomainSharedLibraryType sl : slList )
                {
                    String slName = sl.getName();
                    slNames.add(slName);
                }
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
        }
        
        for ( String slName : slNames )
        {
            if ( mGenQuery.isSharedLibraryRegistered(slName) )
            {
                list.add(slName);
            }
        }
        return list;
    }
    
    private List<String> getSharedLibraries(List<String> list, ComponentState status)
        throws RegistryException
    {
        if ( list == null )
        {
            list = new ArrayList();
        }
        List<String> slNames = new ArrayList();
        
        // -- Add shared libraries only if status is SHUTDOWN
        
        if ( !(TARGET_DOMAIN.equals(mTarget)))
        {
            if ( ComponentState.SHUTDOWN == status )
            {
                InstalledComponentsListType target = null;
                mRegObjLM.acquireReadLock();
                try
                {
                    target = getTarget(mTarget);
                    if ( target != null )
                    {
                        List<SharedLibraryRefType> slRefs =  target.getSharedLibraryRef();
                        for( SharedLibraryRefType slRef : slRefs )
                        {
                            String slName = slRef.getNameRef();
                            slNames.add(slName);
                        }
                    }
                }
                catch ( RegistryException rex)
                {
                    throw rex;
                }
                finally
                {
                    mRegObjLM.releaseReadLock();
                }
                
                for ( String slName : slNames )
                {
                    if ( mGenQuery.isSharedLibraryRegistered(slName) )
                    {
                        list.add(slName);
                    }
                }
                
            }
        }
        else
        {
            // -- status is not considered for the domain target
            return getSharedLibraries(list);
        }
        return list;
    }
    
    private List<String> getAllComponents(ComponentState status)
        throws RegistryException
    {
        
        List<String> components = new ArrayList();
        List<String> compNames  = new ArrayList();
        
        if ( !(TARGET_DOMAIN.equals(mTarget)) )
        {
            
            InstalledComponentsListType target = null;
            mRegObjLM.acquireReadLock();
            try
            {
                target = getTarget(mTarget);
                if ( target != null )
                {
                    List<ComponentRefType> compRefs =  target.getComponentRef();
                    for( ComponentRefType compRef : compRefs )
                    {
                        ComponentState state = ComponentState.valueOfString(compRef.getState().value());
                        if ( state.equals(status) )
                        {
                            String compName = compRef.getNameRef();
                            compNames.add(compName);
                        }
                    }
                }
            }
            catch ( RegistryException rex)
            {   
                throw rex;
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
            
            for ( String compName : compNames )
            {
                if ( mGenQuery.isComponentRegistered(compName) )
                {
                    components.add(compName);
                }
            }
        }
        else
        {
            // -- State doesn't matter for "domain" target
           components = getAllComponents();
        }
        
        return components;
    }
    
    
    private List<String> getBindings(ComponentState status)
        throws RegistryException
    {
        List<String> components = getAllComponents(status);
        List<String> bindings = new ArrayList();
        
        for( String compName : components )
        {
            if ( mGenQuery.getComponentType(compName) == ComponentType.BINDING )
            {
                bindings.add(compName);
            }
        }
        
        return bindings;
    }
    
    
    private List<String> getEngines(ComponentState status)
        throws RegistryException
    {
        List<String> components = getAllComponents(status);
        List<String> engines = new ArrayList();
        
        for( String compName : components )
        {
            if ( mGenQuery.getComponentType(compName) == ComponentType.ENGINE )
            {
                engines.add(compName);
            }
        }
        
        return engines;
    }

    private InstalledComponentsListType getTarget(String targetName)
        throws RegistryException
    {
        InstalledComponentsListType target = null;    
        

        if ( mGenQuery.isTargetServer(targetName) )
        {
            List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

            for ( InstalledComponentsListType server : servers )
            {
                if ( server.getNameRef().equals(targetName))
                {
                    target = server;
                }
            }
        }
        else if ( mGenQuery.isTargetCluster(targetName) )
        {  
            List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();

            for ( InstalledComponentsListType cluster : clusters )
            {
                if ( cluster.getNameRef().equals(targetName))
                {
                    target = cluster;
                }
            }
        }
        if ( target == null )
        {
            mLogger.finest( mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_UNKNOWN_TARGET, mTarget));
        }
        return target;
    }
    
    private List<DomainComponentType> getDomainComponents()
        throws RegistryException
    {
        Components compType = mJbiRegistry.getComponents();
        
        if ( compType != null )
        {
            return compType.getComponent();
        }
        else
        {
            return new ArrayList();
        }
    }
    
    private List<DomainSharedLibraryType> getDomainSharedLibraries()
        throws RegistryException
    {
        
        SharedLibraries slType = mJbiRegistry.getSharedLibraries();
        
        if ( slType != null )
        {
            return slType.getSharedLibrary();
        }
        else
        {
            return new ArrayList();
        }
    }
    
    private List<DomainEntityType> getDomainServiceAssemblies()
        throws RegistryException
    {   
        ServiceAssemblies saType = mJbiRegistry.getServiceAssemblies();
        
        if ( saType != null )
        {
            return saType.getServiceAssembly();
        }
        else
        {
            return new ArrayList();
        }
    }
    
    /**
     * For each ServiceUnitInfo in the list add  the path to the Service Unit Root. 
     *
     * @param suList - List of ServiceUnitInfos, which need to be updated
     * @param componentName - the target component of the ServiceUnitInfos
     */
    private void updateServiceUnits(List<ServiceUnitInfo> suList, String componentName)
    {
        for ( ServiceUnitInfo suInfo : suList )
        {
            String suRelPath = suInfo.getServiceAssemblyName() + java.io.File.separator 
                            + suInfo.getName();
            String suArchiveDir = mRegistry.getRepository().
                findArchiveDirectory(
                com.sun.jbi.management.repository.ArchiveType.SERVICE_UNIT, suRelPath);
            
            String filePath = suArchiveDir + java.io.File.separator + componentName;
            ((ServiceUnitInfoImpl) suInfo).setFilePath(filePath);
        }
    }
}
