/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Updater.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Updater.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.registry;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceAssemblyInfo;
import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.management.ConfigurationCategory;

import java.util.Calendar;
import java.util.Map;
import java.util.Properties;

/**
 * This interface defines operations which clients can use to update the registry.
 *
 *
 * @author Sun Microsystems, Inc.
 */
 public interface Updater
{
    /*--------------------------------------------------------------------------------*\
     *                          Add / Remove Entities                                 *
    \*--------------------------------------------------------------------------------*/
    
    /**
     * Add a component to the runtime target
     *
     * @param componentInfo - component instance
     * @throws RegistryException on errors
     */
    void addComponent(ComponentInfo componentInfo) 
        throws RegistryException;
    
    /**
     * Add a component to a given target. If target = DOMAIN, then a simple component 
     * entry is created with the component name. If the target = SERVER / CLUSTER
     * a component-ref is added.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * not considered when targetType = DOMAIN.
     * @param componentInfo - component instance
     * @throws RegistryException on errors
     */
    void addComponent(String targetName, ComponentInfo componentInfo) 
        throws RegistryException;
    

    /**
     * Remove a component from the runtime's target
     *
     * @param componentName - component name
     * @throws RegistryException on errors
     */
    void removeComponent(String componentName) 
        throws RegistryException;
    
    /**
     * Remove a component from a given target. 
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param componentName - component name
     * @throws RegistryException on errors
     */
    void removeComponent(String targetName, String componentName) 
        throws RegistryException;
 
    /** 
     * Add a shared library to he runtime's target
     *
     * @param ComponentInfo - shared library info
     * @throws RegistryException on errors
     */
    void addSharedLibrary(ComponentInfo sharedLibraryInfo)
            throws RegistryException;
    
    /** 
     * Add a shared library to a given target. If target = DOMAIN, then a simple  
     * entry is created with the library name. If the target = SERVER / CLUSTER
     * a shared-library-ref is added.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param ComponentInfo - shared library info
     * @throws RegistryException on errors
     */
    void addSharedLibrary(String targetName, ComponentInfo sharedLibraryInfo)
            throws RegistryException;    
    

    /** 
     * Remove a shared library from the runtimes target
     *
     * @param sharedLibraryName - shared library name
     * @throws RegistryException on errors
     */
    void removeSharedLibrary(String sharedLibraryName)
            throws RegistryException;
    
    /** 
     * Remove a shared library from a given target.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param sharedLibraryName - shared library name
     * @throws RegistryException on errors
     */
    void removeSharedLibrary(String targetName, String sharedLibraryName)
            throws RegistryException;    
    
    /** 
     * Add a service assembly to he runtime's target
     *
     * @param saName - service assembly name 
     * @throws RegistryException on errors
     */
    void addServiceAssembly(String saName)
            throws RegistryException;
    
    /** 
     * Add a service assembly to a given target. If target = DOMAIN, then a simple  
     * entry is created with the assembly name. If the target = SERVER / CLUSTER
     * a service-assembly-ref is added.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param saName - service assembly name 
     * @throws RegistryException on errors
     */
    void addServiceAssembly(String targetName, String saName)
            throws RegistryException;    
    
    /**
     * Remove a service assembly from a given target. 
     *
     * @param serviceAssemblyName - service assembly name
     * @throws RegistryException on errors
     */
    void removeServiceAssembly(String serviceAssemblyName)
            throws RegistryException;
    
    /**
     * Remove a service assembly from a given target. 
     *
     * @param serviceAssemblyName - service assembly name
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @throws RegistryException on errors
     */
    void removeServiceAssembly(String targetName, String serviceAssemblyName)
            throws RegistryException;

    /**
     * @param serverNameRef - server name to be added
     */
    void addServer(String serverNameRef) 
        throws RegistryException;
    
    /**
     * @param serverNameRef - server name to be removed
     */
     void removeServer(String serverNameRef) 
        throws RegistryException;
        
    /**
     * @param clusterNameRef - cluster name to be added
     */
    void addCluster(String clusterNameRef) 
        throws RegistryException;
    
    /**
     * @param clusterNameRef - cluster name to be removed
     */
     void removeCluster(String clusterNameRef) 
        throws RegistryException;   
     
    /**
     * Add a domain shared library
     * 
     * @param slName        - name of the shared library
     * @param fileName      - component file name
     */
     void addSharedLibrary(String slName, String fileName, Calendar timestamp)
        throws RegistryException;
     
    /**
     * Add a domain component
     * 
     * @param componentName - name of the component
     * @param fileName      - component file name
     */
    void addComponent(String componentName, String fileName, Calendar timestamp)
        throws RegistryException;
    
    /**
     * Add a service assembly to the domain.
     *
     * @param saName - service assembly name
     * @param fileName - service assembly archive name
     * @throws RegistryException on errors
     */
    public void addServiceAssembly(String saName, String fileName, Calendar timestamp)
        throws RegistryException;     
    
    /*--------------------------------------------------------------------------------*\
     *                   Operations to update entries                                 *
    \*--------------------------------------------------------------------------------*/
    
    /**
     * Set the state of a component for the target at runtime.
     *
     * @param componentName - component name
     * @param state  - the state to set. 
     * @throws RegistryException on errors
     */
    void setComponentState(ComponentState state, String componentName) 
        throws RegistryException;
    
    /**
     * Set the state of a component in a server/cluster.
     *
     * @param targetName - either the server-name or cluster-name
     * @param componentName - component name
     * @param state  - the state to set. 
     * @throws RegistryException on errors
     */
    void setComponentState(String targetName,
            ComponentState state, String componentName) throws RegistryException;
    
    /**
     * Set the properties of a component in a server/cluster.
     *
     * @param targetName - either the server-name or cluster-name, this operation is 
     *        a no-op  when target = domain.
     * @param componentName - component name
     * @param props  - the properties to set. 
     * @throws RegistryException on errors
     */
    void setComponentProperties(String targetName,
            Map<String, String> state, String componentName) throws RegistryException;
   
    /**
     * Set the state of a ServiceUnit for the runtime target.
     *
     * @param componentName - component name
     * @param suName - service unit name
     * @param state  - the state to set. 
     * @throws RegistryException on errors
     */
    void setServiceUnitState(
        ServiceUnitState state, String componentName, String suName) 
                throws RegistryException;
    
    /**
     * Set the state of a ServiceUnit in a server / cluster.
     *
     * @param targetName - either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param componentName - component name
     * @param suName - service unit name
     * @param state  - the state to set. 
     * @throws RegistryException on errors 
     */ 
     void setServiceUnitState(String targetName,
        ServiceUnitState state, String componentName, String suName) 
                throws RegistryException;

    /**
     * Set the state of a ServiceAssembly for the runtime target.
     *
     * @param state  - the state to set.
     * @param saName - the service assembly name 
     * @throws RegistryException on errors
     */
    void setServiceAssemblyState(
        ServiceAssemblyState state, String saName)
                throws RegistryException;
   
    /**
     * Set the state of a ServiceAssembly in a server / cluster.
     *
     * @param targetName - either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param saName - service assembly name
     * @param state  - the state to set.
     * @throws RegistryException on errors
     */
    void setServiceAssemblyState(String targetName,
        ServiceAssemblyState state, String saName)
                throws RegistryException;
 
    /**
     * Add a ServiceUnit to a Component for the runtime target
     *
     * @param suInfo - ServiceUnitInfo
     * @param componentName - component name
     * @throws RegistryException on errors
     */
     void addServiceUnitToComponent(
        String componentName, ServiceUnitInfo suInfo) 
            throws RegistryException;
    
    /**
     * Add a ServiceUnit to a Component
     *
     * @param suInfo - ServiceUnitInfo
     * @param targetName - either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param componentName - component name
     * @throws RegistryException on errors
     */
     void addServiceUnitToComponent(String targetName,
        String componentName, ServiceUnitInfo suInfo) 
            throws RegistryException;
    
    /**
     * Remove a ServiceUnit from a Component for the runtime target
     *
     * @param suName - ServiceUnitInfo name
     * @param componentName - component name
     * @throws RegistryException on errors
     */
     void removeServiceUnitFromComponent(
        String componentName, String suName)
            throws RegistryException;
    
    /**
     * Remove a ServiceUnit from a Component
     *
     * @param suName - ServiceUnitInfo name
     * @param targetName - the server-name or cluster-name.
     * @param componentName - component name
     * @throws RegistryException on errors
     */
     void removeServiceUnitFromComponent(String targetName,
        String componentName, String suName)
            throws RegistryException;
    
    /**
     * Set the file name for the domain component.
     *
     * @param fileName - name of the component archive
     * @param componentName - component name
     */
     void setComponentFileName(String fileName, String componentName)
        throws RegistryException;
    
    /**
     * Set the file name for the domain shared library.
     *
     * @param fileName - name of the shared library archive
     * @param slName - shared library  name
     */
     void setSharedLibraryFileName(String fileName, String slName)
        throws RegistryException;
    
    /**
     * Set the file name for the domain service assembly.
     *
     * @param fileName - name of the service assembly archive
     * @param saName - service assembly name
     */
     void setServiceAssemblyFileName(String fileName, String saName)
        throws RegistryException;
     
    /*--------------------------------------------------------------------------------*\
     *                   Set runtime configuration attributes                         *
    \*--------------------------------------------------------------------------------*/
     
    /**
     * Set the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @exception RegistryException on errors in getting the attribute value
     */
    void setAttribute(ConfigurationCategory type, String name, String value)
        throws RegistryException;
    
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @exception RegistryException on errors in getting the attribute value
     */
    void setAttribute(String targetName, ConfigurationCategory type, String name, String value)
        throws RegistryException;
    
    /**
     * Delete the named attribute from the category if it exists, for the implicit target.
     * The attribute is deleted only if it is overriden for the target, if target is "domain"
     * the attribute is not deleted.
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     */
    void deleteAttribute(ConfigurationCategory type, String name)
        throws RegistryException;
    
    /**
     * Delete the named attribute from the category if it exists.
     * The attribute is deleted only if it is overriden for the target, if target is "domain"
     * the attribute is not deleted.
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     */
    void deleteAttribute(String targetName, ConfigurationCategory type, String name)
        throws RegistryException;
    
    /**
     * This method is used to set the upgrade-number attribute in the domain 
     * level entry for a component
     * @param componentName the component name
     * @param upgradeNumber the upgrade number
     * @throws RegistryException if the upgrade number could not be set
     */
    void setComponentUpgradeNumber(String componentName, java.math.BigInteger upgradeNumber)
        throws RegistryException;    
    
    /*--------------------------------------------------------------------------------*\
     *                   Component configuration ops                                  *
    \*--------------------------------------------------------------------------------*/
    
    /*--------------------------------------------------------------------------------*\
     *                  Set static component configuration                            *
    \*--------------------------------------------------------------------------------*/
    /**
     * Set the value of a configuration attribute for a component installed on a target
     * for the runtime target. 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @param componentName - identification for the component
     * @exception RegistryException on errors in setting the attribute value
     */
    void setComponentAttribute(String componentName, String name, String value)
        throws RegistryException;
    
    /**
     * Set the value of a configuration attribute for a component installed on a target,
     *
     * @param targetName - target instance/cluster name.
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @param componentName - identification for the component
     * @exception RegistryException on errors in setting the attribute value
     */
    void setComponentAttribute(String componentName, String targetName, 
        String name, String value)
        throws RegistryException;
    
    /**
     * Set the value of a configuration attribute for a component installed on a target
     * for the runtime target. 
     * @param props - properties to set
     * @param componentName - identification for the component
     * @exception RegistryException on errors in setting the attribute value
     */
    void setComponentAttributes(String componentName, Properties props)
        throws RegistryException;
    
    /**
     * Set the value of a configuration attribute for a component installed on a target,
     *
     * @param targetName - target instance/cluster name.
     * @param props - properties to set
     * @param componentName - identification for the component
     * @exception RegistryException on errors in setting the attribute value
     */
    void setComponentAttributes(String componentName, String targetName, 
        Properties props)
        throws RegistryException;
    
    
    /*--------------------------------------------------------------------------------*\
     *                  Set/Add/Delete application variables                          *
    \*--------------------------------------------------------------------------------*/
    /**
     * Add a set of application variables to the registry for the runtime target.
     *
     * @param appVars - list of application variables to be added to the registry
     * @exception RegistryException on errors in adding the application variables.
     */
    void addComponentApplicationVariables(String componentName, 
		com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException;
    
    /**
     * Add a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be added to the registry
     * @param targetName - target to be updated
     * @exception RegistryException on errors in adding the application variables.
     */
    void addComponentApplicationVariables(String componentName, String targetName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException;
    
    /**
     * Update a set of application variables to the registry for the runtime target.
     *
     * @param appVars - list of application variables to be updated
     * @exception RegistryException on errors in updating the application variables.
     */
    void updateComponentApplicationVariables(String componentName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException;
    
    /**
     * Updated a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be updated
     * @param targetName - target to be updated
     * @exception RegistryException on errors in updating the application variables.
     */
    void updateComponentApplicationVariables(String componentName, String targetName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException;
    
    /**
     * Delete a set of application variables from the registry for the runtime target.
     *
     * @param appVars - list of application variables to be deleted from the registry
     * @param names - the names of the variables to be deleted.
     * @exception RegistryException on errors in deleting the application variables.
     */
    void deleteComponentApplicationVariables(String componentName, String[] names )
        throws  RegistryException;
    
    /**
     * Add a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be deleted from the registry
     * @param targetName - target to be updated
     * @param names - the names of the variables to be deleted.
     * @exception RegistryException on errors in deleting the application variables.
     */
    void deleteComponentApplicationVariables(String componentName, String targetName, 
        String[] names )
        throws  RegistryException;
       
    /*--------------------------------------------------------------------------------*\
     *                  Set/Add/Delete Named Configurations                           *
    \*--------------------------------------------------------------------------------*/
    /**
     * Add a named application configuration to the registry for the component for the
     * implicit runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @exception RegistryException on errors in adding the application configuration.
     */
    void addComponentApplicationConfiguration(String componentName, 
		Properties appConfig )
        throws  RegistryException;
    
    /**
     * Add a named application configuration to the registry for the component for the
     * specified runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @param targetName 
     *                target name
     * @exception RegistryException on errors in adding the application configuration.
     */
    void addComponentApplicationConfiguration( String componentName, String targetName,
		Properties appConfig )
        throws  RegistryException;
    
    /**
     * Update a named application configuration set on a component 
     * for the implicit runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @exception RegistryException on errors in updating the application configuration.
     */
    void updateComponentApplicationConfiguration(String componentName, 
        Properties appConfig )
        throws  RegistryException;
    
    
    /**
     * Update a named application configuration set on a component 
     * for the specified runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @param targetName 
     *                 target name
     * @exception RegistryException on errors in updating the application configuration.
     */
    void updateComponentApplicationConfiguration(String componentName, String targetName,
        Properties appConfig )
        throws  RegistryException;
    
    /**
     * Delete a named application configuration
     *
     * @param appConfigName 
     *          name of the application configuration to delete.
     * @param componentName
     *          component identification
     * @exception RegistryException on errors in deleting the application configuration.
     */
    void deleteComponentApplicationConfiguration(String componentName, String appConfigName )
        throws  RegistryException;
    
    
    /**
     * Delete a named application configuration
     *
     * @param componentName
     *          component identification
     * @param targetName
     *          target name
     * @param appConfigName 
     *          the name of the configuration to be deleted.
     * @exception RegistryException on errors in deleting the application configuration.
     */
    void deleteComponentApplicationConfiguration(String componentName, String targetName,
        String appConfigName )
        throws  RegistryException;     
}
