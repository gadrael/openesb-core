/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegistryUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  GenericQueryImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.registry.xml;

import java.util.List;
import java.util.logging.Logger;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceAssemblyInfo;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.RepositoryException;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.system.ManagementContext;

/**
 * Utility class used by the registry implementation
 *
 * @author Sun Microsystems, Inc.
 */
public class RegistryUtil
{
    private static final String DOMAIN = "domain";
    
    private RegistryImpl     mRegistry;
    private ManagementContext mMgtCtx;
    private StringTranslator mTranslator;
    private Logger           mLog;
        
    public RegistryUtil(ManagementContext mgtCtx, RegistryImpl registry)
    {
        mRegistry = registry;
        mMgtCtx   = mgtCtx;
        mLog      = mgtCtx.getLogger();
        mTranslator = 
            mgtCtx.getEnvironmentContext().getStringTranslator("com.sun.jbi.management");
    }    
    
    /**
     * The components, shared libraries and service assemblies registered in the registry
     * should be present in the repository and vice versa. Any entity outside the
     * intersection set of the registry/repository will be deleted. 
     *
     * 
     */
    synchronized public void  syncWithRepository()
        throws Exception
    {
        String readOnly = mRegistry.getProperty(Registry.REGISTRY_READONLY_PROPERTY);
        if ( !Boolean.parseBoolean(readOnly) )
        {
            syncEntity(ArchiveType.SERVICE_ASSEMBLY);
            syncEntity(ArchiveType.COMPONENT);
            cleanOrphanedServiceAssemblies();
            syncEntity(ArchiveType.SHARED_LIBRARY);
        }
    }
    
         
    /**
     * Cleans up an entity from the registry and/or the repository.
     *
     * If a service assembly is registered in the repository and not in the registry
     * then the service assembly is removed from the domain entry as well as any other
     * installed entry.
     * @param type of entity 
     * @param name of the entity
     * @param isInRegistry indication
     * @param inInReposity indication
     * @throws RegistryException if there is any registry related error
     * @throws RepositoryException if there is any repository related error
     */
    public void cleanEntity(ArchiveType type, String name, boolean isInRegistry, boolean isInRepository)
        throws RegistryException, RepositoryException
    {
        String  missingFromReg;
        String  missingFromRepos;
        String  removingFromReg;
        String  removingFromRepos;

        missingFromReg =  LocalStringKeys.JBI_ADMIN_SHARED_LIBRARY_MISSING_IN_REGISTRY;
        removingFromReg = LocalStringKeys.JBI_ADMIN_REMOVING_SHARED_LIBRARY_FROM_REGISTRY;
        missingFromRepos = LocalStringKeys.JBI_ADMIN_SHARED_LIBRARY_MISSING_IN_REPOSITORY;
        removingFromRepos = LocalStringKeys.JBI_ADMIN_REMOVING_SHARED_LIBRARY_FROM_REPOSITORY;
        if (type.equals(ArchiveType.COMPONENT))
        {
            missingFromReg =  LocalStringKeys.JBI_ADMIN_COMPONENT_MISSING_IN_REGISTRY;
            removingFromReg = LocalStringKeys.JBI_ADMIN_REMOVING_COMPONENT_FROM_REGISTRY;
            missingFromRepos = LocalStringKeys.JBI_ADMIN_COMPONENT_MISSING_IN_REPOSITORY;
            removingFromRepos = LocalStringKeys.JBI_ADMIN_REMOVING_COMPONENT_FROM_REPOSITORY;
        }
        else if (type.equals(ArchiveType.SERVICE_ASSEMBLY))
        {
            missingFromReg =  LocalStringKeys.JBI_ADMIN_SERVICE_ASSEMBLY_MISSING_IN_REGISTRY;
            removingFromReg = LocalStringKeys.JBI_ADMIN_REMOVING_SERVICE_ASSEMBLY_FROM_REGISTRY;
            missingFromRepos = LocalStringKeys.JBI_ADMIN_SERVICE_ASSEMBLY_MISSING_IN_REPOSITORY;
            removingFromRepos = LocalStringKeys.JBI_ADMIN_REMOVING_SERVICE_ASSEMBLY_FROM_REPOSITORY;
        }

        synchronized(mRegistry)
        {
            if ( !isInRegistry && isInRepository)
            {
                mLog.warning(mTranslator.getString(missingFromReg, name));
                mLog.info(mTranslator.getString(removingFromRepos, name));
                mMgtCtx.getRepository().removeArchive(type, name);
            }

            if ( isInRegistry && !isInRepository )
            {
                mLog.warning(mTranslator.getString(missingFromRepos, name));
                mLog.info(mTranslator.getString(removingFromReg, name));

                Updater updater = mRegistry.getUpdater();
                List<String> serversAndClusters;
                (serversAndClusters = mRegistry.getGenericQuery().getServers()).addAll(
                    mRegistry.getGenericQuery().getClusters());

                if (type.equals(ArchiveType.SHARED_LIBRARY))
                {
                    for ( String sc : serversAndClusters )
                    {
                        updater.removeSharedLibrary(sc, name);
                    }
                    updater.removeSharedLibrary(DOMAIN, name);
                }
                else if (type.equals(ArchiveType.COMPONENT))
                {
                    for ( String sc : serversAndClusters )
                    {
                        updater.removeComponent(sc, name);
                    }
                    updater.removeComponent(DOMAIN, name);
                }
                else if (type.equals(ArchiveType.SERVICE_ASSEMBLY))
                {
                    for ( String sc : serversAndClusters )
                    {
                        ComponentQuery compQuery = mRegistry.getComponentQuery(sc);

                        List<String> comps = compQuery.getComponentIds(ComponentType.BINDINGS_AND_ENGINES); 
                        for ( String comp : comps )
                        {
                            ComponentInfo compInfo = compQuery.getComponentInfo(comp);
                            removeServiceAssemblyFromComponent(sc, compInfo, name);
                        }
                        updater.removeServiceAssembly(sc, name);
                    }
                    updater.removeServiceAssembly(DOMAIN, name);
                }
            }
        }
    }
    
    
    /*----------------------------------------------------------------------------------*\
     *                          Private Helpers                                         *
    \*----------------------------------------------------------------------------------*/
    
    private void  removeServiceAssemblyFromComponent(String target, ComponentInfo compInfo, String saName)
        throws RegistryException
    {
        List<ServiceUnitInfo> suInfos = compInfo.getServiceUnitList();
        Updater updater = mRegistry.getUpdater();
        
        for (ServiceUnitInfo suInfo : suInfos )
        {
            if ( saName.equals(suInfo.getServiceAssemblyName()) )
            {
                updater.removeServiceUnitFromComponent(target, compInfo.getName(),
                    suInfo.getName());
            }
        }
    }
    
    /**
     * Get the list of entities of type ArchiveType registered in the registry and in the repository,
     * any entity outside the intersection set of these two lists is deleted.
     */
    private void syncEntity(ArchiveType type)
        throws Exception
    {
        List<String> registeredEntities = null;
        
        if ( type.equals(ArchiveType.COMPONENT))
        {
            registeredEntities = mRegistry.getGenericQuery().getRegisteredComponents();
        }
        else if ( type.equals(ArchiveType.SERVICE_ASSEMBLY))
        {
            registeredEntities = mRegistry.getGenericQuery().getRegisteredServiceAssemblies();
        }
        else if ( type.equals(ArchiveType.SHARED_LIBRARY))
        {
            registeredEntities = mRegistry.getGenericQuery().getRegisteredSharedLibraries();
        }
             
        List<String>
            repositoryEntities = mMgtCtx.getRepository().getArchiveEntityNames(type);
        
        for ( String entity : registeredEntities )
        {
            if ( !(repositoryEntities.contains(entity)) )
            {
                // -- Remove the entity from the registry 
                cleanEntity(type, entity, true, false);
            }
            repositoryEntities.remove(entity);
        }
        
        // -- If the repositoryEntities list is non-empty, these are entities which
        // -- are in the repository only, clean the repository
        for ( String entity : repositoryEntities )
        {
                // -- Remove the entity from the repository
                cleanEntity(type, entity, false, true);
        }
    }
    
    /**
     * If after a service assembly is deployed on an instance / cluster, *all* target
     * components are wiped out, the service assembly reference in the server/cluster
     * ref is orphaned. If this is not a assembly with zero service units, then this 
     * service assembly reference needs to be cleaned otherwise the registry puts the
     * JBI runtime in a bad state.
     *
     * @exception on any errors
     */
    private void cleanOrphanedServiceAssemblies()
        throws Exception
    {
        // Get all the targets
        List<String> serversAndClusters;
            (serversAndClusters = mRegistry.getGenericQuery().getServers()).addAll(
                mRegistry.getGenericQuery().getClusters());

        // Look for orphaned service assemblies on each target, cleanup if one found
        for ( String sc : serversAndClusters )
        {
            cleanOrphanedServiceAssemblies(sc);
        }
    }
    
    /**
     * Clean up any orphaned service assemblies from this target
     *
     * @exception on errors
     */
    private void cleanOrphanedServiceAssemblies(String target)
        throws Exception
    {
        ServiceAssemblyQuery saQuery = mRegistry.getServiceAssemblyQuery(target);
        
        List<String> sas = saQuery.getServiceAssemblies();
        for ( String sa : sas )
        {
            ServiceAssemblyInfo saInfo = saQuery.getServiceAssemblyInfo(sa);
            
            if ( saInfo.getStatus() == ServiceAssemblyState.UNKNOWN )
            {
                // Clean this orphaned service assembly
                mRegistry.getUpdater().removeServiceAssembly(target, sa);
            }
        }
    }
}
