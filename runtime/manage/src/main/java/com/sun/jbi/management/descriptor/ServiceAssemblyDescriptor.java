/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyDescriptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ServiceAssemblyDescriptor.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 2, 2006, 4:49 PM
 */

package com.sun.jbi.management.descriptor;

import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.ServiceUnitInfo;

import com.sun.jbi.management.util.StringHelper;
import com.sun.jbi.management.registry.data.ServiceUnitInfoImpl;

import java.util.List;


/**
 *
 * This class encapsulates the JAXB model for the Service Assembly Descriptor
 *
 * @author Sun Microsystems, Inc
 */
public class ServiceAssemblyDescriptor
{

    private ServiceAssembly mSaType;
    
    /**
     * Constructs a ServiceAssemblyDescriptor.
     *
     * @param jbi - JAXB JBI descriptor handle
     * @throws an IllegalArgumentException if the JbiType passed in not that
     * for a component
     */
    public ServiceAssemblyDescriptor(Jbi jbi)
        throws IllegalArgumentException
    {
        mSaType = jbi.getServiceAssembly();
        
        if ( mSaType == null )
        {
            throw new IllegalArgumentException();
        }
    }
    
    /**
     * Get the name of the Service Assembly
     */
    public String getName()
    {
        if ( mSaType.getIdentification() != null )
        {
            return StringHelper.trim( mSaType.getIdentification().getName() );
        }
        else
        {
            return "";
        }
    }
    
    /**
     * @return the Service Assembly Description
     */
    public String getDescription()
    {
        if ( mSaType.getIdentification() != null )
        {
            return StringHelper.trim( mSaType.getIdentification().getDescription() );
        }
        else
        {
            return "";
        }
    }
    
    /**
     * @return a list of ServiceUnitInfo elements corresponding to the service
     * units in the Service Assembly. 
     */
    public List<ServiceUnitInfo> getServiceUnits()
    {
        
        List<ServiceUnitInfo> resultSus = new java.util.ArrayList<ServiceUnitInfo>();
        List<com.sun.jbi.management.descriptor.ServiceUnit>
            suList = mSaType.getServiceUnit();
                    
        for ( com.sun.jbi.management.descriptor.ServiceUnit su : suList )
        {
            ServiceUnitInfoImpl suInfo = new ServiceUnitInfoImpl();
            suInfo.setServiceAssemblyName(getName());
            suInfo.setName(StringHelper.trim(su.getIdentification().getName()));
            suInfo.setTargetComponent(StringHelper.trim(su.getTarget().getComponentName()));
            suInfo.setState(ServiceUnitState.UNKNOWN);

            resultSus.add(suInfo);
        }
        return resultSus;
    }
    
}
