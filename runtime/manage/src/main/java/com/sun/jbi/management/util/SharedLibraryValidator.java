/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SharedLibraryValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ArchiveValidator.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 2, 2006, 4:49 PM
 */

package com.sun.jbi.management.util;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.StringTranslator;

import com.sun.jbi.management.descriptor.SharedLibraryDescriptor;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.Component;
import com.sun.jbi.management.LocalStringKeys;

import java.util.logging.Logger;
import javax.jbi.JBIException;


/**
 * Validator for a shared library archives.
 *
 * @author Sun Microsystems, Inc
 */
public class SharedLibraryValidator
    implements Validator
{
    private Logger               mLog;
    private StringTranslator     mTranslator;
    private ArchiveHelper        mArchHelper;
    private boolean              mValidate;
    private MessageBuilder       mMsgBuilder;
    private static final String  EMPTY_STR = "";
  
    /**
     * @param ctx - the environment context
     * @param validate - flag to indicate whether jbi.xml should be validated based
     * on the schema
     */
    public SharedLibraryValidator(EnvironmentContext envCtx, boolean validate)
        throws Exception
    {
        mTranslator = envCtx.getStringTranslator("com.sun.jbi.management");
        mLog        = Logger.getLogger("com.sun.jbi.management");
        mArchHelper = new ArchiveHelper(envCtx);
        mValidate   = validate;
        mMsgBuilder = new MessageBuilder(mTranslator);
    }
    
    /**
     * Perform archive validation based on the type of the archive. 
     *
     * @param archivePath - absolute path to the archive.
     * @throws Exception if the archive is not valid. The exception message 
     *         has the details on what caused validation to fail.
     *
     */
    public void validate(java.io.File archivePath)
        throws Exception
    {
        validateDescriptor(archivePath);
    }
    
    /*---------------------------------------------------------------------------------*\
     *                          Private Helpers                                        *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Validate the jbi.xml component installation descriptor in the archive
     * 
     * @throws Exception if any inconsistencies are found.
     */
    private void  validateDescriptor(java.io.File archivePath)
        throws Exception
    {
        Jbi jbiXml = mArchHelper.loadJbiXml(archivePath, mValidate);
        SharedLibraryDescriptor descr = null;
        String[] params = new String[]{archivePath.getAbsolutePath()};
        
        try
        {
            descr = new SharedLibraryDescriptor(jbiXml);
        }
        catch( IllegalArgumentException iex )
        {
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INVALID_SHARED_LIBRARY_ARCHIVE_TYPE,
                archivePath);
            String jbiMsg = mMsgBuilder.buildFrameworkMessage("validateDescriptor",
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), params, mMsgBuilder.getMessageToken(errMsg));
            throw new JBIException(jbiMsg);
        }
        
        // -- Check Shared Library Class path, at least one class should be there
        if ( descr.getSharedLibraryClassPathElements().isEmpty() )
        {
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_EMPTY_SHARED_LIBRARY_CLASSPATH,
                archivePath);
            String jbiMsg = mMsgBuilder.buildFrameworkMessage("validateDescriptor",
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), params, mMsgBuilder.getMessageToken(errMsg));
            throw new JBIException(jbiMsg);
        }
    }
    
    
    
}
