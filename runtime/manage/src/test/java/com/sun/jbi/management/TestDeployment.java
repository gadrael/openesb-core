/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDeployment.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.system.ManagementContext;
import javax.jbi.management.DeploymentServiceMBean;

import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.StandardMBean;
import org.junit.Ignore;

/**
 * Tests the deployment (management) service.
 * @author Sun Microsystems, Inc.
 */
@Ignore
public class TestDeployment
{
   /** JMX Domain name for the jbi jmx server. */
    public static final String JBI_DOMAIN = "com.sun.jbi";

   /** JBI MBean Object type. */
    public static final String CONTROL_TYPE_KEY = "ControlType";

    /** JBI MBean Object type value. */
    public static final String DEPLOYER_CONTROL_TYPE_VALUE = "Deployer";

    /** JMX Default Port. */
    public static final int JMX_PORT_DEFAULT = 5555;
 
    /**
     * main routine.
     * @param anUnusedSetOfArgs unused.
     */
    public static void main(String[] anUnusedSetOfArgs)
    {
        DeploymentServiceMBean dsbean = null;
        try 
        {
            MBeanServer mbs = MBeanServerFactory.createMBeanServer(JBI_DOMAIN);
            System.out.println("In main. before start");
            EnvironmentContext mContext =
                new com.sun.jbi.management.system.ScaffoldEnvironmentContext();
            ManagementContext mgmtContext = new ManagementContext(mContext);
            dsbean = new com.sun.jbi.management.system.DeploymentService(mgmtContext);
            StandardMBean smbean = new StandardMBean(dsbean,
                                                     DeploymentServiceMBean.class);
            String mbeanRegisteredName = JBI_DOMAIN + ":" +
                              CONTROL_TYPE_KEY + "=" + DEPLOYER_CONTROL_TYPE_VALUE;
            System.out.println("MBEAN NAME : " + mbeanRegisteredName);
            mbs.registerMBean(smbean, new ObjectName(mbeanRegisteredName));
           
            // Create a JMXMP connector server
            
            System.out.println("\nCreate a JMXMP connector server");
            JMXServiceURL url = new JMXServiceURL("jmxmp", null, JMX_PORT_DEFAULT);
            JMXConnectorServer cs =
                JMXConnectorServerFactory.newJMXConnectorServer(url, null, mbs);
            
            // Start the JMXMP connector server
            //
            System.out.println("\nStart the JMXMP connector server");
            cs.start();
            System.out.println("\nJMXMP connector server successfully started");
            System.out.println("\nWaiting for incoming connections..."); 
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            System.out.println("\nDeploying..."); 
            //dsbean.deploy("C:\\temp\\ms1\\deploy\\AUCalc.jar");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        //System.out.println("In main. end");
    }
}
