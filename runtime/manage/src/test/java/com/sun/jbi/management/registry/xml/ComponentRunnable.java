/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentRunnable.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ComponentRunnable.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 15, 2005, 11:56 AM
 */

package com.sun.jbi.management.registry.xml;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceUnitState;

import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.data.ComponentInfoImpl;
import com.sun.jbi.management.registry.data.ServiceUnitInfoImpl;

import java.util.Calendar;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentRunnable implements Runnable
{
    private Registry mRegistry;
    private String   mTstComp;
    private boolean  mAdd;
    private Updater  mUpdater;
    
    /** Creates a new instance of ComponentRunnable */
    public ComponentRunnable (Registry registry, String componentName, boolean add)
        throws Exception
    {
        mUpdater = registry.getUpdater();
        mTstComp = componentName;
        mAdd = add;
    }
    
    public void run()
    {
        try
        {
            if (mAdd)
            {
                addComponent();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    
    
    public  void addComponent()
        throws Exception
    {
        try
        {

            // Add a Component
            ComponentInfo comp = createTestComponent(mTstComp);

            mUpdater.addComponent(mTstComp, mTstComp + "File", Calendar.getInstance());
            mUpdater.addComponent("another-server", comp);
        }
        catch ( Throwable ex)
        {
            ex.printStackTrace();
            throw new Exception(ex);
        }
    }
    
    private ComponentInfo createTestComponent(String compName)
    {
        ComponentInfoImpl component = new ComponentInfoImpl();
        
        component.setName(compName);
        component.setWorkspaceRoot("C:/foo");
        component.setInstallRoot("C:/foo");
        component.setComponentClassName("SunBPELEngine.class");
        component.setBootstrapClassLoaderSelfFirst(true);
        component.setClassLoaderSelfFirst(true);
        component.setProperty("time","seconds");
        component.setProperty("threads","10");
        component.setStatus(ComponentState.STOPPED);
        component.setComponentType(ComponentType.ENGINE);
        component.addServiceUnitInfo(createTestServiceUnit("SU1"));
        
        return component;
    }
    
    private ServiceUnitInfo createTestServiceUnit(String name)
    {
        ServiceUnitInfoImpl suinfo = new ServiceUnitInfoImpl();
        suinfo.setName(name);
        suinfo.setServiceAssemblyName("CompositeApplication");
        suinfo.setState(ServiceUnitState.STOPPED);
        
        return suinfo;
    }
}
