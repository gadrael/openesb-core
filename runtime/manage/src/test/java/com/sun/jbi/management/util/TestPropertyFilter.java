/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestPropertyFilter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

/**
 *
 * @author Sun Microsystems, Inc.
 */

public class TestPropertyFilter
    extends junit.framework.TestCase
{
   
    
    public TestPropertyFilter(String testName)
    {
        super(testName);
    }
    
    public void testFilterProperties()
        throws Exception
    {
        System.setProperty("Company.Name", "SUN MICRO" );
        System.setProperty("Stock.Value", "10.00" );
        
        String testString     = "The stock of ${Company.Name} has reached USD ${Stock.Value}";
        
        String result = PropertyFilter.filterProperties(testString);
        
        System.out.println(result);
        String expectedResult = "The stock of SUN MICRO has reached USD 10.00";
        
        assertEquals(expectedResult, result);
    }
    
    public void testFilterProperties1()
        throws Exception
    {
        
        String testString     = "It is a bright sunny day.";
        
        String result = PropertyFilter.filterProperties(testString);
        
        System.out.println(result);
        
        assertEquals(testString, result);
    }
    
    public void testFilterProperties2()
        throws Exception
    {
        
        String testString     = "It is a bright sunny ${day.";
        
        String result = PropertyFilter.filterProperties(testString);
        
        System.out.println(result);
        
        assertEquals(testString, result);
    }
    
    public void testFilterProperties3()
        throws Exception
    {
        System.setProperty("com.sun.aas.instanceRoot", "C:\\Sun\\AppServerEE\\domains\\cas");
     
        
        String testString     = "${com.sun.aas.instanceRoot}/jbi/components/jmsbinding/install_root";
        
        String result = PropertyFilter.filterProperties(testString);
        
        System.out.println(result);
        String expectedResult = "C:\\Sun\\AppServerEE\\domains\\cas/jbi/components/jmsbinding/install_root";
        
        assertEquals(expectedResult, result);
    }
    
    public void testReplacePropertyValues()
    {
        String testString = "C:\\Sun\\AppServerEE\\domains\\cas/jbi/components\\jmsbinding/install_root";
        System.setProperty("com.sun.aas.instanceRoot", "C:\\Sun\\AppServerEE\\domains\\cas");
        
        String result = PropertyFilter.replacePropertyValues(testString, 
            "com.sun.aas.instanceRoot");
        System.out.println(result);
        
        String expectedResult = "${com.sun.aas.instanceRoot}/jbi/components\\jmsbinding/install_root";
        
        assertEquals(expectedResult, result);
    }
}
