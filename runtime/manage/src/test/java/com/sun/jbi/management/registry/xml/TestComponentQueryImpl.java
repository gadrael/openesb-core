/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentQueryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryBuilder;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.system.Util;

import java.io.File;
import java.util.List;
import org.junit.After;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestComponentQueryImpl
{
    /**
     * The sample Configuration Directory.
     */
    private String mConfigDir = null;
    private File mRegFile;
    String mRegFilePath;
    String mRegGoodFilePath;
    String mEngineZipPath;
    String mBindingZipPath;
    String mSharedLibraryZipPath;
    String mServiceAssemblyZipPath;
    
    static final String ENGINE_NAME = "SunSequencingEngine";
    static final String BINDING_NAME = "SunJMSBinding";
    static final String SHARED_LIBRARY_NAME = "SunWSDLSharedLibrary";
    static final String SERVICE_ASSEMBLY_NAME = "CompositeApplication";
    
    @Before
    public void setUp()
        throws Exception
    {
        String srcroot = Util.getSourceRoot();
        mConfigDir = srcroot + "/target/test-classes/testdata";

        mRegFilePath              = mConfigDir + File.separator + "jbi-registry.xml";
        mRegGoodFilePath          = mConfigDir + File.separator + "jbi-registry-good.xml";
        mEngineZipPath            = mConfigDir + File.separator + "component.zip";
        mServiceAssemblyZipPath   = mConfigDir + File.separator + "service-assembly.zip";
        mSharedLibraryZipPath     = mConfigDir + File.separator + "wsdlsl.jar";
        mBindingZipPath           = mConfigDir + File.separator + "jmsbinding.jar";
        
        mRegFile = new File(mRegFilePath);
        
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        Util.fileCopy(mRegGoodFilePath, mRegFilePath);
    }

    @After
    public void tearDown()
        throws Exception
    {
        // -- restore registry.xml
       RegistryBuilder.destroyRegistry(); 
       Util.fileCopy(mRegGoodFilePath, mRegFilePath );
    }
    
    @Test
    public void getEngineComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        List<String> comps = query.getComponentIds(ComponentType.ENGINE);
        System.out.println(comps.toString());
        assertTrue(comps.contains(ENGINE_NAME));
        repository.purge();
    }
    
    @Test
    public void getBindingComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        List<String> comps = query.getComponentIds(ComponentType.BINDING);
        System.out.println(comps.toString());
        assertTrue(comps.contains(BINDING_NAME));
        repository.purge();
    }
    
    /**
     * Test getting the binding components for the domain target.
     */
    @Test
    public void getBindingComponentIds2()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("domain");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        List<String> comps = query.getComponentIds(ComponentType.BINDING);
        System.out.println(" Domain Binding Components " + comps.toString());
        assertTrue(comps.contains(BINDING_NAME));
        repository.purge();
    }
    
    @Test
    public void getBindingsAndEngineComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
           
        List<String> comps = query.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        System.out.println(comps.toString());
        assertTrue(comps.contains(BINDING_NAME));
        assertTrue(comps.contains(ENGINE_NAME));
        repository.purge();
    }
    
    @Test
    public void getSharedLibraryComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
           
        List<String> sls = query.getComponentIds(ComponentType.SHARED_LIBRARY);
        System.out.println(sls.toString());
        assertTrue(sls.contains(SHARED_LIBRARY_NAME));
        assertEquals(1, sls.size());
        repository.purge();
    }
    
    @Test
    public void getAllComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
           
        List<String> list = query.getComponentIds(ComponentType.ALL);
        System.out.println(list.toString());
        assertTrue(list.contains(SHARED_LIBRARY_NAME));
        assertTrue(list.contains(ENGINE_NAME));
        assertTrue(list.contains(BINDING_NAME));
        repository.purge();
    }
    
    @Test
    public void getAllStartedComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
           
        List<String> list = query.getComponentIds(ComponentType.ALL, ComponentState.STARTED);
        System.out.println(list.toString());
        assertTrue(list.contains(ENGINE_NAME));
        assertTrue(list.contains(BINDING_NAME));
        repository.purge();
    }
    
    @Test
    public void getComponentInfo()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        ComponentInfo comp = query.getComponentInfo(BINDING_NAME);
        
	if ( comp == null )
	   System.out.println("This is null");
	else
	   System.out.println("This is not null");

        assertNotNull(comp);
        System.out.println(comp.getName());
        System.out.println(comp.getDescription());
        System.out.println(comp.getInstallRoot());
        System.out.println(comp.getClassPathElements().toString());
        comp.getComponentType().toString().equals("BINDING");
        assertTrue(comp.getSharedLibraryNames().toString().equals("[SunWSDLSharedLibrary]"));
        repository.purge();
    }
    
    /**
     * Test getting from the domain
     */
    @Test
    public void getComponentInfo2()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("domain");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        ComponentInfo comp = query.getComponentInfo(BINDING_NAME);
        
        System.out.println("Domain Component");
        System.out.println(comp.getName());
        System.out.println(comp.getDescription());
        System.out.println(comp.getInstallRoot());
        System.out.println(comp.getClassPathElements().toString());
        comp.getComponentType().toString().equals("BINDING");
        assertTrue(comp.getSharedLibraryNames().toString().equals("[SunWSDLSharedLibrary]"));
        assertTrue("Started".equals(comp.getStatus().toString()));
        repository.purge();
    }
    
    @Test
    public void getDependentComponentIds()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
        
        List<String> depComps = query.getDependentComponentIds(SHARED_LIBRARY_NAME);
        assertTrue(BINDING_NAME.equals(depComps.get(0)));
        repository.purge();
    }
    
    @Test
    public void getDependentComponentIds2()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("domain");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
        
        List<String> depComps = query.getDependentComponentIds(SHARED_LIBRARY_NAME);
        assertTrue(BINDING_NAME.equals(depComps.get(0)));
        repository.purge();
    }
    
    @Test
    public void getSharedLibraryInstallationDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRepository();
        
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
        ComponentInfo comp = query.getSharedLibraryInfo(SHARED_LIBRARY_NAME);
        
        assertNotNull(comp);
        assertNotNull(comp.getInstallationDescriptor());
        assertTrue(comp.getInstallationDescriptor().indexOf(SHARED_LIBRARY_NAME) > 0);
        repository.purge();
    }
    
    @Test
    public void getComponentInstallationDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        ComponentInfo comp = query.getComponentInfo(BINDING_NAME);
        
        assertNotNull(comp.getInstallationDescriptor());
        assertTrue(comp.getInstallationDescriptor().indexOf(BINDING_NAME) > 0);
        repository.purge();
    }
    
    /**
     * This tests the fix for CR 6548857. For non-domain targets if the server-ref or
     * cluster-ref is missing in a fresh registry, the component query operations to
     * get components/shared libraries for that target should return an empty list
     * instead of throwing an "Unknown target : XXX " exception. 
     *
     * This tests the private getTarget() operation in ComponentQueryImpl. If the target
     * is missing a null value is expected and not an exception.
     */
    @Test
    public void getTarget()
        throws Exception
    {
        // Start with a blank registry
        RegistryBuilder.destroyRegistry();
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        Registry reg = Util.createRegistry(true);
        try
        {
            ComponentQueryImpl query = (ComponentQueryImpl) reg.getComponentQuery("someTarget");
            Class clazz = query.getClass();
            
            java.lang.reflect.Method 
                getTargetMtd = clazz.getDeclaredMethod("getTarget", 
                    new Class[]{Class.forName("java.lang.String")} );
            getTargetMtd.setAccessible(true);
            Object target = getTargetMtd.invoke(query, new Object[]{"someTarget"});
            assertNull(target);
        }
        catch ( Exception ex )
        {
            fail();
        }
    }
    
    /**
     * Test getting component configuration properties.
     */
    @Test
    public void getComponentConfiguration()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        java.util.Properties props = compInfo.getConfiguration();
        assertNotNull(props);
        assertEquals(props.get("HostName"), "tango");
        assertEquals(props.get("Port"), "5656");
        repository.purge();
    }
    
    /**
     * Test getting component configuration properties.
     */
    @Test
    public void getComponentConfigurationForClusterTarget()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("clusterA");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        java.util.Properties props = compInfo.getConfiguration();
        assertNotNull(props);
        assertFalse(props.isEmpty());
        assertEquals(props.get("HostName"), "Eight");
        assertEquals(props.get("Port"), "8888");
        repository.purge();
    }

    /**
     * Test getting non-existant component configuration properties.
     */
    @Test
    public void getEmptyComponentConfiguration()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(BINDING_NAME);
        assertNotNull(compInfo);
        java.util.Properties props = compInfo.getConfiguration();
        assertNotNull(props);
        assertTrue(props.isEmpty());
        repository.purge();
    }
    
    /**
     * Test getting component application variables
     */
    @Test
    public void getComponentApplicationVariables()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        
        com.sun.jbi.management.ComponentInfo.Variable[] vars = compInfo.getVariables();
        
        assertNotNull(vars);
        assertEquals(2, vars.length);
        
        repository.purge();
    }
    
    /**
     * Test getting component application variables
     */
    @Test
    public void getEmptyComponentApplicationVariables()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mBindingZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(BINDING_NAME);
        assertNotNull(compInfo);
        
        com.sun.jbi.management.ComponentInfo.Variable[] vars = compInfo.getVariables();
        
        assertNotNull(vars);
        assertEquals(0, vars.length);
        
        repository.purge();
    }
    
    /**
     * Test getting component application configuration names.
     */
    @Test
    public void getComponentApplicationConfigurationNames()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        
        String[] configNames = compInfo.getApplicationConfigurationNames();
        
        assertNotNull(configNames);
        assertEquals(2, configNames.length);
        
        repository.purge();
    }
        
    /**
     * Test getting component application configuration.
     */
    @Test
    public void getComponentApplicationConfiguration()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        ComponentQuery query = reg.getComponentQuery("server");
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mEngineZipPath);
        
        com.sun.jbi.management.ComponentInfo compInfo = 
            (com.sun.jbi.management.ComponentInfo) query.getComponentInfo(ENGINE_NAME);
        assertNotNull(compInfo);
        
        // -- Test whether the property values are read correctly
        java.util.Properties config1 = compInfo.getApplicationConfiguration("SEQ_CONFIG1");
        assertTrue(config1.getProperty("configurationName").equals("SEQ_CONFIG1"));
        assertTrue(config1.getProperty("initialContextFactory").equals("com.sonicsw.jndi.mfcontext.MFContextFactory"));
        assertTrue(config1.getProperty("connectionURL").equals("jndi://cfg1"));
        
        
        java.util.Properties config2 = compInfo.getApplicationConfiguration("SEQ_CONFIG2");
        assertTrue(config2.getProperty("configurationName").equals("SEQ_CONFIG2"));
        assertTrue(config2.getProperty("initialContextFactory").equals("com.sonicsw.jndi.mfcontext.MFContextFactory"));
        assertTrue(config2.getProperty("connectionURL").equals("jndi://cfg2")); 
        
        repository.purge();
    }
}
