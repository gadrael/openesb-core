/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestUpdaterImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.*;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.registry.GenericQuery;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.registry.data.ComponentInfoImpl;
import com.sun.jbi.management.registry.data.ServiceUnitInfoImpl;
import com.sun.jbi.management.system.Util;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.math.BigInteger;
import java.util.Map;
import java.util.Properties;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
// DBY 2013-09-12 
// We have to keep the exact order to manage test dependencies (bad practice)
public class TestUpdaterImpl
{
    /**
     * The sample Configuration Directory.
     */
    private static String mConfigDir = null;
    private static File mRegFile;
    private static File mRegBkupFile;
    static String mRegFilePath;
    static String mRegGoodFilePath;
    static String mRegBkupFilePath;
    static String mComponentZipPath;
    static String mSharedLibraryZipPath;
    static String mServiceAssemblyZipPath;
    
    static final String COMPONENT_NAME = "SunSequencingEngine";
    static final String SHARED_LIBRARY_NAME = "sun-wsdl-library";
    static final String SERVICE_ASSEMBLY_NAME = "CompositeApplication";

    static Registry reg = null;
    static Updater updater = null;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        String srcroot = Util.getSourceRoot();
        mConfigDir = srcroot + "/target/test-classes/testdata/";
        
        mRegFilePath        = mConfigDir + File.separator + "jbi-registry.xml";
        mRegGoodFilePath    = mConfigDir + File.separator + "jbi-registry-good.xml";
        mRegBkupFilePath    = mConfigDir + File.separator + "jbi-registry-backup.xml";
        mComponentZipPath   = mConfigDir + "component.zip";
        mServiceAssemblyZipPath   = mConfigDir + "service-assembly.zip";
        mSharedLibraryZipPath     = mConfigDir + "wsdlsl.jar";
      
        mRegFile = new File(mRegFilePath);
        mRegBkupFile = new File(mRegBkupFilePath);
        
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        if ( mRegBkupFile.exists())
        {
            mRegBkupFile.delete();
        }
        
        reg = Util.createRegistry();
        updater = reg.getUpdater();
    }
    
    /**
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void testA_addServer()
           throws Exception
    {
        updater.addServer("another-server");
    }
    
    /**
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void testB_addCluster()
           throws Exception
    {
        updater.addCluster("another-cluster");
    }
    
    /**
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void testC_addComponent()
           throws Exception
    {
        ComponentInfo comp = createTestComponent();
        
        updater.addComponent("another-server", comp);
        updater.addComponent("another-cluster", comp);
    }    
    
    /**
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void testD_addSharedLibrary()
           throws Exception
    {
        ComponentInfo sl = createTestSharedLibrary();
        
        updater.addSharedLibrary("another-server", sl);
        updater.addSharedLibrary("another-cluster", sl);
    }
    
    @Test
    public void testE_addServiceUnitToComponent()
        throws Exception
    {
        ServiceUnitInfo suInfo = createTestServiceUnit("SU2");
        updater.addServiceUnitToComponent("another-server", "SunBPELEngine", suInfo);
    }

    @Test
    public void testF_removeServiceUnitFromComponent()
        throws Exception
    {
        updater.removeServiceUnitFromComponent("another-server", "SunBPELEngine", "SU2");
    }
 
    @Test
    public void testG_removeSharedLibrary()
        throws Exception
    {
        updater.removeSharedLibrary("another-server",  "SunBPELEngineLibrary");
        updater.removeSharedLibrary("another-cluster", "SunBPELEngineLibrary");
        updater.removeSharedLibrary("domain", "SunBPELEngineLibrary");
    }
    
    @Test
    public void testH_removeComponent()
        throws Exception
    {
        updater.removeComponent("another-server",  "SunBPELEngine");
        updater.removeComponent("another-cluster", "SunBPELEngine");
        updater.removeComponent("domain", "SunBPELEngine");
    }
    
    @Test
    public void testI_removeServer()
           throws Exception
    {
        updater.removeServer("another-server");
    }
    
    @Test
    public void testJ_removeCluster()
           throws Exception
    {
        updater.removeCluster("another-cluster");
    }
    
    /** 
     * Test setting a configuration attribute.
     */
    @Test
    public void testK_setAttribute()
        throws Exception
    {
        updater.setAttribute("domain", ConfigurationCategory.System, "newAttribute", "newValue");
        
        // -- Verify
        GenericQuery query = reg.getGenericQuery();
        String value = query.getAttribute("domain", ConfigurationCategory.System, "newAttribute");
        
        assertEquals("newValue", value);
    }
    
    /** 
     * Test deleting a configuration attribute for target=domain, no action should
     * be taken
     */
    @Test
    public void testL_deleteDomainAttribute()
        throws Exception
    {
        updater.setAttribute("domain", ConfigurationCategory.System, "newAttribute", "newValue");
        updater.deleteAttribute("domain", ConfigurationCategory.System, "newAttribute");
        
        // -- Verify
        GenericQuery query = reg.getGenericQuery();
        String value = query.getAttribute("domain", ConfigurationCategory.System, "newAttribute");
        
        assertNotNull(value);
        assertEquals("newValue", value);
    }
    
    /** 
     * Test deleting a configuration attribute for non-domain target, no action should
     * be taken
     */
    @Test
    public void testM_deleteNonDomainAttribute()
        throws Exception
    {
        updater.addServer("server");
        updater.setAttribute("server", ConfigurationCategory.System, "newAttribute", "newValue");
        updater.deleteAttribute("server", ConfigurationCategory.System, "newAttribute");
        
        // -- Verify
        GenericQuery query = reg.getGenericQuery();
        assertFalse(query.isAttributeOverriden("server", ConfigurationCategory.System, "newAttribute"));
    }
    
    /**
     * This method is used to test the setComponentUpgradeNumber method
     * @throws Exception if the test could not be completed
     */
    @Test
    public void testN_setComponentUpgradeNumber()
    throws Exception
    {
        updater.setComponentUpgradeNumber("SunSequencingEngine", BigInteger.ONE);        
    }        
    
    /**
     * Test setting a component configuration attribute
     */
    @Test
    public void testO_setComponentAttributes()
        throws Exception
    {
        // Setup
        ComponentInfo comp = createTestComponent();
        updater.addServer("another-server");
        updater.addComponent("another-server", comp);
        
        java.util.Properties props = new java.util.Properties();
        props.put("p1", "v1");
        props.put("p2", "v2");
        updater.setComponentAttributes(comp.getName(), "another-server", props);
        
        // cleanup
        updater.removeComponent("another-server", comp.getName());
        updater.removeServer("another-server");
    }
    
    /**
     * Test adding application variables
     */
    @Test
    public void testP_addComponentApplicationVariables()
        throws Exception
    {
        // Setup
        ComponentInfo comp = createTestComponent();
        updater.addServer("another-server");
        updater.addComponent("another-server", comp);
        
        com.sun.jbi.management.ComponentInfo.Variable[] vars = 
            new com.sun.jbi.management.ComponentInfo.Variable[4];
        
        vars[0] = new com.sun.jbi.management.ComponentInfo.Variable("name","Peter","STRING");
        vars[1] = new com.sun.jbi.management.ComponentInfo.Variable("ID","007","NUMBER");
        vars[2] = new com.sun.jbi.management.ComponentInfo.Variable("password","56757X15","PASSWORD");
        vars[3] = new com.sun.jbi.management.ComponentInfo.Variable("isEmployee","true","BOOLEAN");
        
        
        updater.addComponentApplicationVariables(comp.getName(), "another-server", vars);
        
        // Verify
        RegistryDocument regDoc = new RegistryDocument(getRegistryDom());
        com.sun.jbi.management.ComponentInfo.Variable[] retrivedVars = 
            regDoc.getComponentApplicationVariables("another-server", true, comp.getName());
        
        assertEquals( 4, retrivedVars.length );
        
        // cleanup
        updater.removeComponent("another-server", comp.getName());
        updater.removeServer("another-server");
    }
    
    /**
     * Test updating application variables
     */
    @Test
    public void testQ_updateComponentApplicationVariables()
        throws Exception
    {
        // Setup
        ComponentInfo comp = createTestComponent();
        updater.addServer("another-server");
        updater.addComponent("another-server", comp);
        
        com.sun.jbi.management.ComponentInfo.Variable[] vars = 
            new com.sun.jbi.management.ComponentInfo.Variable[4];
        
        vars[0] = new com.sun.jbi.management.ComponentInfo.Variable("name","Peter","STRING");
        vars[1] = new com.sun.jbi.management.ComponentInfo.Variable("ID","007","NUMBER");
        vars[2] = new com.sun.jbi.management.ComponentInfo.Variable("password","56757X15","PASSWORD");
        vars[3] = new com.sun.jbi.management.ComponentInfo.Variable("isEmployee","true","BOOLEAN");
        
        
        updater.addComponentApplicationVariables(comp.getName(), "another-server", vars);
        
        com.sun.jbi.management.ComponentInfo.Variable[] updates = 
            new com.sun.jbi.management.ComponentInfo.Variable[2];
        
        updates[0] = new com.sun.jbi.management.ComponentInfo.Variable("name","Parker","STRING");
        updates[1] = new com.sun.jbi.management.ComponentInfo.Variable("phone","89786","NUMBER");
        
        updater.updateComponentApplicationVariables(comp.getName(), "another-server", updates);

        // Verify
        RegistryDocument regDoc = new RegistryDocument(getRegistryDom());
        com.sun.jbi.management.ComponentInfo.Variable[] retrivedVars = 
            regDoc.getComponentApplicationVariables("another-server", true, comp.getName());
        
        boolean updated = false;
        for ( com.sun.jbi.management.ComponentInfo.Variable var : retrivedVars )
        {
            if ( var.getName().equals("name") )
            {
                assertTrue(var.getValue().equals("Parker"));
                updated = true;
            }
        }
       
        assertTrue( updated );
        
        // cleanup
        updater.removeComponent("another-server", comp.getName());
        updater.removeServer("another-server");
    }
    
    /**
     * Test updating application variables
     */
    @Test
    public void testR_deleteComponentApplicationVariables()
        throws Exception
    {
        // Setup
        ComponentInfo comp = createTestComponent();
        updater.addServer("another-server");
        updater.addComponent("another-server", comp);
        
        com.sun.jbi.management.ComponentInfo.Variable[] vars = 
            new com.sun.jbi.management.ComponentInfo.Variable[4];
        
        vars[0] = new com.sun.jbi.management.ComponentInfo.Variable("name","Peter","STRING");
        vars[1] = new com.sun.jbi.management.ComponentInfo.Variable("ID","007","NUMBER");
        vars[2] = new com.sun.jbi.management.ComponentInfo.Variable("password","56757X15","PASSWORD");
        vars[3] = new com.sun.jbi.management.ComponentInfo.Variable("isEmployee","true","BOOLEAN");
        
        
        updater.addComponentApplicationVariables(comp.getName(), "another-server", vars);
        
        
        String [] varNames = new String[]{"name", "ID", "password"};
        
        
        updater.deleteComponentApplicationVariables(comp.getName(), "another-server", varNames);
        
        // Verify
        RegistryDocument regDoc = new RegistryDocument(getRegistryDom());
        com.sun.jbi.management.ComponentInfo.Variable[] retrivedVars = 
            regDoc.getComponentApplicationVariables("another-server", true, comp.getName());
        
        assertEquals(1, retrivedVars.length);
        assertTrue( vars[3].equals(retrivedVars[0]));
        
        // cleanup
        updater.removeComponent("another-server", comp.getName());
        updater.removeServer("another-server");
    }
    
    /**
     * Test adding application configuration
     */
    @Test
    public void testS_addComponentApplicationConfiguration()
        throws Exception
    {
        // Setup
        ComponentInfo comp = createTestComponent();
        updater.addServer("another-server");
        updater.addComponent("another-server", comp);
        
        Properties config1 = new Properties();
        config1.setProperty("configurationName","myName");
        config1.setProperty("prop1","value1");
        config1.setProperty("prop2","value2");
        
        
        updater.addComponentApplicationConfiguration(comp.getName(), "another-server", config1);
        
        // Verify
        RegistryDocument regDoc = new RegistryDocument(getRegistryDom());
        Map<String, Properties> configs = 
            regDoc.getComponentApplicationConfiguration("another-server", true, comp.getName());
        
        assertEquals( 1, configs.size() );
        
        // cleanup
        updater.removeComponent("another-server", comp.getName());
        updater.removeServer("another-server");
    }
    
    /**
     * Test adding application configuration
     */
    @Test
    public void testT_updateComponentApplicationConfiguration()
        throws Exception
    {
        // Setup
        ComponentInfo comp = createTestComponent();
        updater.addServer("another-server");
        updater.addComponent("another-server", comp);
        
        Properties config1 = new Properties();
        config1.setProperty("configurationName","myName");
        config1.setProperty("prop1","value1");
        config1.setProperty("prop2","value2");
        
        
        updater.addComponentApplicationConfiguration(comp.getName(), "another-server", config1);
        config1.setProperty("prop1", "updatedValue1");
        
        // Update
        updater.addComponentApplicationConfiguration(comp.getName(), "another-server", config1);
        
        // Verify
        RegistryDocument regDoc = new RegistryDocument(getRegistryDom());
        Map<String, Properties> configs = 
            regDoc.getComponentApplicationConfiguration("another-server", true, comp.getName());
        
        assertEquals( 1, configs.size() );
        
        Properties updatedProps = configs.get("myName");
        assertEquals(updatedProps.getProperty("prop1"), "updatedValue1");
        
        // cleanup
        updater.removeComponent("another-server", comp.getName());
        updater.removeServer("another-server");
    }

    /**
     * Test delete application configuration
     */
    @Test
    public void testU_deleteComponentApplicationConfiguration()
        throws Exception
    {
        // Setup
        ComponentInfo comp = createTestComponent();
        updater.addServer("another-server");
        updater.addComponent("another-server", comp);
        
        Properties config1 = new Properties();
        config1.setProperty("configurationName","myName");
        config1.setProperty("prop1","value1");
        config1.setProperty("prop2","value2");
        
        
        updater.addComponentApplicationConfiguration(comp.getName(), "another-server", config1);
        config1.setProperty("prop1", "updatedValue1");
        
        // Delete
        updater.deleteComponentApplicationConfiguration(comp.getName(), "another-server", "myName");
        
        // Verify
        RegistryDocument regDoc = new RegistryDocument(getRegistryDom());
        Map<String, Properties> configs = 
            regDoc.getComponentApplicationConfiguration("another-server", true, comp.getName());
        
        assertEquals( 0, configs.size() );
        
        // cleanup
        updater.removeComponent("another-server", comp.getName());
        updater.removeServer("another-server");
    }

    /**
     * Test adding application configuration with missing "configurationName"
     */
    @Test
    public void testV_addComponentApplicationConfigurationMissingName()
        throws Exception
    {
        // Setup
        ComponentInfo comp = createTestComponent();
        updater.addServer("another-server");
        updater.addComponent("another-server", comp);
        
        Properties config1 = new Properties();
        config1.setProperty("prop1","value1");
        config1.setProperty("prop2","value2");
        
        try
        {
            updater.addComponentApplicationConfiguration(comp.getName(), "another-server", config1);
            fail("RegistryException not thrown for missing configurationName key");
        }
        catch ( RegistryException ex)
        {
            assertTrue(true);            
        }

        // Verify config not added
        RegistryDocument regDoc = new RegistryDocument(getRegistryDom());
        Map<String, Properties> configs = 
            regDoc.getComponentApplicationConfiguration("another-server", true, comp.getName());

        assertEquals( 0, configs.size() );
        
        // cleanup
        updater.removeComponent("another-server", comp.getName());
        updater.removeServer("another-server");
    }
    
    /*----------------------------------------------------------------------------------*\
     *                              Private Helpers                                     *
    \*----------------------------------------------------------------------------------*/
    
    private ComponentInfo createTestComponent()
    {
        ComponentInfoImpl component = new ComponentInfoImpl();
        
        component.setName("SunBPELEngine");
        component.setWorkspaceRoot("C:/foo");
        component.setInstallRoot("C:/foo");
        component.setComponentClassName("SunBPELEngine.class");
        component.setBootstrapClassLoaderSelfFirst(true);
        component.setClassLoaderSelfFirst(true);
        component.setProperty("time","seconds");
        component.setProperty("threads","10");
        component.setStatus(ComponentState.STOPPED);
        component.setTimestamp(1000);
        component.setUpgradeNumber(1);
        component.setComponentType(ComponentType.ENGINE);
        component.addServiceUnitInfo(createTestServiceUnit("SU1"));
        
        return component;
    }
    
    private ServiceUnitInfo createTestServiceUnit(String name)
    {
        ServiceUnitInfoImpl suinfo = new ServiceUnitInfoImpl();
        suinfo.setName(name);
        suinfo.setServiceAssemblyName("CompositeApplication");
        suinfo.setState(ServiceUnitState.STOPPED);
        
        return suinfo;
    }
    
    private ComponentInfo createTestSharedLibrary()
    {
        ComponentInfoImpl sl = new ComponentInfoImpl();
        
        sl.setName("SunBPELEngineLibrary");
        sl.setClassLoaderSelfFirst(true);
        sl.setInstallRoot("D:/tmp");
        return sl;
    }
    
    private org.w3c.dom.Document getRegistryDom()
        throws Exception
    {
        DocumentBuilderFactory 
            dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        org.w3c.dom.Document document = db.parse(mRegFile);
        return document;
    }
}
