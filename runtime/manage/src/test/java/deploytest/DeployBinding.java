/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployBinding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package deploytest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;

/**
 * Dummy binding component used to test deployment.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeployBinding
    implements Component, ComponentLifeCycle, ServiceUnitManager, Bootstrap
{
    /**
     * Local copy of the component name.
     */
    protected String mComponentName;

    /**
     * Type of component.
     */
    protected String mComponentType = "Binding";

    /**
     * Local handle to the ComponentContext.
     */
    protected ComponentContext mContext;

    /**
     * Logger instance.
     */
    protected Logger mLog = Logger.getLogger("com.sun.jbi.management");

    //
    // Component methods
    //

    /**
     * Get the ComponentLifeCycle implementation instance for this Binding
     * Component.
     * @return the life cycle implementation instance.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        mLog.log(Level.INFO, "{0} getLifeCycle called", mComponentType);
        return this;
    }
 
    /**
     * Get the ServiceUnitManager implementation instance for this Binding
     * Component.
     * @return the Service Unit manager implementation instance.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        mLog.log(Level.INFO, "{0} {1} getServiceUnitManager called", new Object[]{mComponentType, mComponentName});
        return this;
    }

    /**
     * Resolve descriptor details for the specified reference, which is for a
     * service provided by this component.
     * @param ref the endpoint reference to be resolved.
     * @return the description for the specified reference.
     */
    public org.w3c.dom.Document getServiceDescription(
        javax.jbi.servicedesc.ServiceEndpoint ref)
    {
        mLog.log(Level.INFO, "{0} {1} getServiceDescription called", new Object[]{mComponentType, mComponentName});
        return null;
    }
    
    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually 
     * perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        mLog.log(Level.INFO, "{0} {1} isExchangeWithConsumerOkay called", new Object[]{mComponentType, mComponentName});
        return true;
    }
    
    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually 
     * interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        mLog.log(Level.INFO, "{0} {1} isExchangeWithProviderOkay called", new Object[]{mComponentType, mComponentName});
        return true;
    }

    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public javax.jbi.servicedesc.ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment epr)
    {
        mLog.log(Level.INFO, "{0} {1} resolveEndpointReference called", new Object[]{mComponentType, mComponentName});
        return null;
    }

    //
    // ComponentLifeCycle methods
    //

    /**
     * Initialize the Binding Component.
     * @param context the JBI component context created by the JBI framework.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void init(ComponentContext context)
        throws javax.jbi.JBIException
    {
        mComponentName = context.getComponentName();
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. This
     * implementation always returns null.
     * @return javax.management.ObjectName is always null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        mLog.log(Level.INFO, "{0} {1} getExtensionMBeanName called", new Object[]{mComponentType, mComponentName});
        return null;
    }

    /**
     * Start the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.log(Level.INFO, "{0} {1} start called", new Object[]{mComponentType, mComponentName});
    }

    /**
     * Stop the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLog.log(Level.INFO, "{0} {1} stop called", new Object[]{mComponentType, mComponentName});
    }

    /**
     * Shut down the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLog.log(Level.INFO, "{0} {1} shutDown called", new Object[]{mComponentType, mComponentName});
    }

    //
    // ServiceUnitManager methods
    //
 
    /**
     * Deploy a Service Unit.
     * @param serviceUnitName the name of the Service Unit being deployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return a deployment status message.
     * @throws javax.jbi.management.DeploymentException if the deployment
     * operation is unsuccessful.
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.log(Level.INFO, "{0} {1} deployed Service Unit {2}", new Object[]{mComponentType, mComponentName, serviceUnitName});
        
        return createDeployResult("deploy", true);
    }

    /**
     * Initialize the deployment.
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state.
     */
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.log(Level.INFO, "{0} {1} initialized Service Unit {2}", new Object[]{mComponentType, mComponentName, serviceUnitName});
    }

    /**
     * Shut down the deployment.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void shutDown(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.log(Level.INFO, "{0} {1} shut down Service Unit {2}", new Object[]{mComponentType, mComponentName, serviceUnitName});
    }

    /**
     * Start the deployment.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.log(Level.INFO, "{0} {1} started Service Unit {2}", new Object[]{mComponentType, mComponentName, serviceUnitName});
    }

    /**
     * Stop the deployment.
     * @param serviceUnitName the name of the Service Unit being stopped.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void stop(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.log(Level.INFO, "{0} {1} stopped Service Unit {2}", new Object[]{mComponentType, mComponentName, serviceUnitName});
    }

    /**
     * Undeploy a Service Unit from the component.
     * @param serviceUnitName the name of the Service Unit being undeployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return an undeployment status message.
     * @throws javax.jbi.management.DeploymentException if the undeployment
     * operation is unsuccessful.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.log(Level.INFO, "{0} {1} undeployed Service Unit {2}", new Object[]{mComponentType, mComponentName, serviceUnitName});
        return createDeployResult("undeploy", true);
    }

    public void onUninstall() 
        throws javax.jbi.JBIException
    {
    }

    public void onInstall() throws javax.jbi.JBIException
    {
        
    }

    public void init(javax.jbi.component.InstallationContext installationContext)
        throws javax.jbi.JBIException
    {
        
    }

    public void cleanUp() throws javax.jbi.JBIException
    {
        
    }
    
    /** Creates a (un)deployment result string.
     *  @param task 'deploy' or 'undeploy'
     */
    private String createDeployResult(String task, boolean isSuccess)
    {
        return "<component-task-result xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"
            + "<component-name>" + mComponentName + "</component-name>"
            + "<component-task-result-details>"
            + "<task-result-details>"
            + "<task-id>" + task + "</task-id>"
            + "<task-result>" + (isSuccess ? "SUCCESS" : "FAILURE") + "</task-result>"
            + "</task-result-details>"
            + "</component-task-result-details>"
            + "</component-task-result>";
    }
}
