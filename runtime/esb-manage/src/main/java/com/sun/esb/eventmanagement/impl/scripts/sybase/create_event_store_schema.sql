create table NOTIFICATION_EVENT (
    id int  identity,
	timeStamp bigint,
  	physicalHostName varchar(256), 
	environmentName varchar(256), 
	logicalHostName varchar(256), 
	serverType varchar(256), 
	serverName varchar(256), 
	componentType varchar(256), 
	componentProjectPathName varchar(1024), 
	componentName varchar(256), 
	eventType varchar(256), 
	severity int, 
	operationalState int, 
	messageCode varchar(256), 
	messageDetail varchar(4096), 
	observationalState int, 
	deploymentName varchar(256)
       );

create index  eventTime on NOTIFICATION_EVENT(timeStamp);

