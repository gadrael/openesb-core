package com.sun.esb.eventmanagement.impl;

import com.sun.esb.eventmanagement.api.InstanceIdentifier;
import com.sun.jbi.platform.PlatformContext;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class GlassfishV4InstanceIdentifier implements InstanceIdentifier {

    private static final Logger mLogger;
    private static final boolean isDebugEnabled;

    static {
        mLogger = Logger.getLogger(EventManagementControllerMBean.class.getName());
        isDebugEnabled = mLogger.isLoggable(Level.FINEST);

    }

    private final PlatformContext mPlatformContext;
    private final MBeanServerConnection mMBeanServer;

    private final static String SERVER_HTTP_LISTENER1 = "amx:pp=/domain/configs/config[server-config]/network-config/network-listeners,type=network-listener,name=http-listener-1";
    private final static String SERVER_ADMIN_LISTENER1 = "amx:pp=/domain/configs/config[server-config]/network-config/network-listeners,type=network-listener,name=admin-listener";

    private final static String BOOT_AMX = "amx-support:type=boot-amx";
    
    public GlassfishV4InstanceIdentifier(PlatformContext aPlatformContext) {
        this.mPlatformContext = aPlatformContext;
        this.mMBeanServer = aPlatformContext.getMBeanServer();
        forceAmxBoot();
    }

    private void forceAmxBoot() {
        try {
            ObjectName on = new ObjectName(BOOT_AMX);
            mMBeanServer.invoke(on, "bootAMX", new Object[]{}, new String []{});
        } catch (Exception e) {
            // should not fail since we running on glassfish & the mbean we query is part it. 
            if (isDebugEnabled) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public String getInstanceUniqueName() {
        return mPlatformContext.getInstanceName();
    }

    @Override
    public String getInstanceUniquePort() {
        return getInstanceHttpPort();
    }

    @Override
    public String getHttpPort() {
        String httpPortNumber = "8080";
        try {
            ObjectName on = new ObjectName(SERVER_HTTP_LISTENER1);
            httpPortNumber = (String) mMBeanServer.getAttribute(on, "Port");
        } catch (Exception e) {
            // should not fail since we running on glassfish & the mbean we query is part it. 
            if (isDebugEnabled) {
                e.printStackTrace();
            }
        }
        return httpPortNumber;
    }

    @Override
    public String getDomainAdminPort() {
        String httpPortNumber = null;
        boolean isDAS = mPlatformContext.isAdminServer();
        if(!isDAS) {
            return "";
        }
        
        try {
            ObjectName on = new ObjectName(SERVER_ADMIN_LISTENER1);
            httpPortNumber = (String) mMBeanServer.getAttribute(on, "Port");
        } catch (Exception e) {
            // should not fail since we running on glassfish & the mbean we query is part it. 
            if (isDebugEnabled) {
                e.printStackTrace();
            }
        }
        
        return httpPortNumber;
    }

    @Override
    public Boolean isSecureAdminPort() {
        boolean isSecureAdmin = false;
        boolean isDAS = mPlatformContext.isAdminServer();
        if(!isDAS) {
            return false;
        }
        
        try {
            ObjectName on = new ObjectName(SERVER_ADMIN_LISTENER1);
            isSecureAdmin = (Boolean) mMBeanServer.getAttribute(on, "Enabled");
        } catch (Exception e) {
            // should not fail since we running on glassfish & the mbean we query is part it. 
            if (isDebugEnabled) {
                e.printStackTrace();
            }
        }
        
        return isSecureAdmin;
    }

    @Override
    public String getInstanceHttpPort() {
        return getHttpPort();
    }

}
