/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventRepositoryConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.eventmanagement.impl;

public interface EventStoreConstants {
    
     static final String ALERTS_TASK_ID = "ALERT_MAMANGEMENT_TASK";

     static final  String ONE_OF_GLASSFISH_DOMAIN_NAMES = "com.sun.appserv"; 
     static final long EVENTID_SEED = 10000000; 
     static String EVENT_STORE_DB_NAME =  "EventStoreDb";

     static final String EVENT_POLICY_FILTER_TYPE = "alertsDB.removalPolicy";

     static final String EVENT_STORE_DELETE_EVENT_BY_ID = 
        "DELETE FROM NOTIFICATION_EVENT  where id = ? ";
     static final String EVENT_STORE_DELETE_EVENT_BY_DATE = 
        "DELETE FROM NOTIFICATION_EVENT  where TIMESTAMP < ? ";
     static final String EVENT_STORE_DELETE_EVENT_BY_LEVEL = 
        "DELETE FROM NOTIFICATION_EVENT  where severity >= ?  ";
    
     static final String EVENT_STORE_EVENT_COUNT = 
        "SELECT count(id) FROM NOTIFICATION_EVENT  ";

      static final String EVENT_STORE_GET_EVENT_DATES = 
        "SELECT TIMESTAMP FROM NOTIFICATION_EVENT   order by TIMESTAMP desc";
   
     static final String INSERT_STATEMENT_ORACLE ="insert into NOTIFICATION_EVENT (id, timeStamp, physicalHostName, environmentName," +
     		" logicalHostName, serverType, serverName, componentType, componentProjectPathName, componentName," +
     		" eventType, severity, operationalState, messageCode, messageDetail, observationalState, deploymentName) " +
     		" values ( autoincrement_id.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

     static final String INSERT_STATEMENT_OTHER ="insert into NOTIFICATION_EVENT ( timeStamp, physicalHostName, environmentName," +
     " logicalHostName, serverType, serverName, componentType, componentProjectPathName, componentName," +
     " eventType, severity, operationalState, messageCode, messageDetail, observationalState, deploymentName) " +
     " values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
     

}
