/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventForwardingHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.sun.jbi.management.util.FileHelper;

public class EventForwardingHelper {


     private static final Logger mLogger;
     private static boolean isDebugEnabled;
     static {
         mLogger = Logger.getLogger(EventForwardingHelper.class.getName());
         isDebugEnabled = mLogger.isLoggable(Level.FINEST);
     
     }
     private String mPropFile;
     
     private String mJbiInstallRoot;
     
     /**
     * 
     */
    public EventForwardingHelper(String aJbiInstallRoot) {
        mJbiInstallRoot = aJbiInstallRoot;
        mPropFile = mJbiInstallRoot+ File.separator + 
            EventManagementConstants.EVENT_CONFIG_PROPERTIES_FILE;
        
    }
    
    
    public Properties GetProperties() {
        File fIn = new File(mPropFile);
        Properties lprops = new Properties();
        if(fIn.exists() == false) {
            return lprops;
        }
        try {
            FileInputStream fis = new FileInputStream(fIn);
            lprops.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
        } 
        return lprops;
    }
    

    public boolean saveProperties(Properties aProperties) {
 
        File fout = new File(mPropFile);
        // get properties file directory - create one if does not exist
        if(!fout.getParentFile().exists()) {
            // found out the directory created by jbi registry
            // which on instances created after the forwarder
            // mbean is initialized.
            FileHelper.createFolder(fout.getParentFile());
        }
            
        try {
            FileOutputStream fos = new FileOutputStream(fout);
            aProperties.store(fos, "eventManagment.properties");
        } catch (FileNotFoundException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }

        return true;
    }

    
    
//    public void forwardEvent(Event event) {
//    	String lStorePath;
//        try {
//			lStorePath = initialize();
//            ObjectName lObjectName = 
//                new ObjectName(EventProperties.EVENTFORWARDER_MBEAN_NAME);
//            if ( mMBeanServer.isRegistered(lObjectName)  == false) {
//                if(registerEventForwarder(lObjectName,
//                			lStorePath) == false) {
//                    mLogger.warning("EventPublisher MBean is not registered. Event discarded.");
//                    return;
//                }
//            }
//
//            Object[] params = new Object[1];
//            params[0] = event;
//            String[] paramsSig = 
//                new String[] { "com.stc.eventmanagement.Event" };
//            if(isDebugEnabled) {
//                mLogger.finest("publish event: "+ event);
//            }
//            mMBeanServer.invoke(lObjectName,
//                    EventProperties.EVENT_FORWARD_OPERATION,params,paramsSig);
//                       
//        } catch(MalformedObjectNameException e) {
//           mLogger.log(Level.SEVERE,
//                   "EventPublisher MBean name is not valid: "+
//                   EventProperties.EVENTPUBLISHER_MBEAN_NAME,e);
//        } catch(InstanceNotFoundException e) {
//            mLogger.warning("EventPublisher MBean instance not found "+
//                    EventProperties.EVENTPUBLISHER_MBEAN_NAME);
//        } catch(Exception e) {
//            e.printStackTrace();
//        }       
//    }
   
//    private boolean registerEventForwarder(ObjectName aObjectName,
//    						String aStorePath) {
//        try {
//            EventForwarderMBean lEventForwarder = 
//            	new EventForwarderMBean(aStorePath);
//            mMBeanServer.registerMBean(lEventForwarder,aObjectName);
//            return true;
//        } catch (InstanceAlreadyExistsException e) {
//            // TODO Auto-generated catch block
//            mLogger.warning("Event Forwarder MBean is already registered.");
//        } catch (MBeanRegistrationException e) {
//            // TODO Auto-generated catch block
//            mLogger.warning("Event Forwarder MBean fail to register - " 
//                    + e.getLocalizedMessage());
//        } catch (NotCompliantMBeanException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//            mLogger.warning("Event Forwarder MBean is Invalid - " 
//                    + e.getLocalizedMessage());
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//            mLogger.warning("Event Forwarder MBean throw Exception - " 
//                    + e.getLocalizedMessage());
//        }
//        
//        return false;
//    }

//    private String initialize() throws Exception {
//        String lEventStoreDirPath = null;
//        String lAppDir = 
//            System.getProperty(EventManagementService.APPLICATION_DATA_DIR);
//        if(lAppDir == null) {
//            lAppDir =  System.getProperty(
//       			EventManagementService.CURRENT_DIR);
//            lEventStoreDirPath = lAppDir + File.separator 
//                + EventManagementService.EVENT_STORE_DIR;
//        } else {
//            // check if app dir end with file seperator and handle it
//            // correctly
//            if(lAppDir.endsWith(File.separator)) {
//                lEventStoreDirPath = lAppDir +  
//                    EventManagementService.EVENT_STORE_DIR;
//                
//            } else {
//                lEventStoreDirPath = lAppDir + File.separator 
//                    + EventManagementService.EVENT_STORE_DIR;
//            }
//        }
//       File lEventStoreDir = new File(lEventStoreDirPath);
//       if(lEventStoreDir.exists() == false) {
//           boolean lSuccess = lEventStoreDir.mkdirs();
//           if(lSuccess == false) {
//               throw new Exception("fail to create Seebeyond store directory");
//           }
//       }
//       return lEventStoreDirPath;
//    }

}
