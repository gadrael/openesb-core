/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AlertUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

import java.text.MessageFormat;

import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;

public class AlertUtil {
    private static I18NBundle I18NPACKAGEBUNDLE;

    /**
     * Creates a management message string and populates the exception
     * 
     * @param bundleKey
     * @param args
     *            of Strings
     * @param sourceException -
     *            the source exception to propagate
     * @return Exception object created with a valid XML Management Message
     */
    public static Exception createManagementException(String bundleKey, Object[] args,
            Throwable sourceException) {
        Exception exception = null;
        String xmlManagementMessage = JBIResultXmlBuilder.createJbiResultXml(
                getPackageBundle(), bundleKey, args, sourceException);
        exception = new Exception(xmlManagementMessage);
        return exception;
    }

    /**
     * gives the I18N bundle
     * 
     * @return I18NBundle object
     */
    public static I18NBundle getPackageBundle() {
        if (I18NPACKAGEBUNDLE == null) {
            I18NPACKAGEBUNDLE = new I18NBundle("com.sun.esb.eventmanagement.impl");
        }
        return I18NPACKAGEBUNDLE;
    }
    
    public static String constructMessage(String bundleKey) {
        StringBuffer buffer = new StringBuffer();
        String bundleIDKey = bundleKey+".ID";
        buffer.append(getPackageBundle().getMessage(bundleIDKey));
        buffer.append(" : ");
        buffer.append(getPackageBundle().getMessage(bundleKey));
        return buffer.toString();
    }

    public static String constructLogMessage(String bundleKey) {
        String logMessage = getPackageBundle().getMessage(bundleKey);
        return logMessage;
    }
    
    
    public static String constructMessage(String bundleKey,Object[] args) {
        String message = (getPackageBundle().getMessage(bundleKey,args));
        return message;
    }
}
