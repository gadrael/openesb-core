/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AlertLevelType.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.eventmanagement.impl;

public enum AlertLevelType {
    NONE("NONE"),INFO("INFO"),WARNING("WARNING"),
    MINOR("MINOR"),MAJOR("MAJOR"),CRITICAL("CRITICAL"),FATAL("FATAL");
    
    String alertLevel;

    
    /** @param protocolString */
    private AlertLevelType(String minAlertLevel) {
        this.alertLevel = minAlertLevel;
    }

    /** @return the protocol description */
    public String getDescription() {
        switch (this) {
            case NONE:
                return "No limitation on alerts persisted";
            case INFO:
                return "All Alerts except Info. level alerts can be persisted";
            case WARNING:
                return "All Alerts except Info. and warning level alerts can be persisted";
            case MINOR:
                return "All Alerts except Info,warning. and minor level alerts can be persisted";
            case MAJOR:
                return "Only Alerts with  major,critical and fatal level alerts can be persisted";
            case CRITICAL:
                return "Only Alerts with  critical and fatal level alerts can be persisted";
            case FATAL:
                return "Only Alerts with  fatal level alerts can be persisted";
        }
        return "";
    }

    /** @return the alert level string representation */
    public String getAlertLevel() {
        return alertLevel;
    }
    /** @return the alert level as int. this value must be kept in sync with the value
     *  defined in com.sun.caps.management.impl.alerts.AlertConstants.
     */
    public String getAlertLevelAsIntString() {
        switch (this) {
            case NONE:
                return "-1";
            case INFO:
                return  "5";
            case WARNING:
                return  "4";
            case MINOR:
                return  "3";
            case MAJOR:
                return  "2";
            case CRITICAL:
                return  "1";
            case FATAL:
                return  "0";
        }
        return "-1";
    }

}
