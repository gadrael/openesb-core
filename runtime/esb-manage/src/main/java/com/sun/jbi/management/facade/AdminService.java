/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.VersionInfo;

import javax.management.ObjectName;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.system.ManagementException;
import com.sun.jbi.management.util.FacadeMbeanHelper;


/**
 * This interface defines a set of administrative methods allowing a JMX-based
 * administrative tool to perform a variety of administrative tasks.
 * <ul>
 *   <li>Find component lifecycle MBean names for:
 *     <ul>
 *       <li>Individual components, by name</li>
 *       <li>All binding components</li>
 *       <li>All service engines</li>
 *     </ul>
 *   </li>
 *   <li>Find lifecyle MBeans names for implementation-defined services:
 *     <ul>
 *       <li>Individual implementation services, by name</li>
 *       <li>All implementation services</li>
 *     </ul>
 *   </li>
 *   <li>Query whether an individual component is a binding or engine</li>
 *   <li>Query implementation information (version etc.)</li>
 * </ul>
 *
 * @author Sun Microsystems, Inc.
 */
public class AdminService
    extends Facade
    implements com.sun.jbi.management.AdminServiceMBean
{
    /** The suffix used in ObjectNames for JBI schemaorg_apache_xmlbeans.system services. */
    private final String SERVICE_SUFFIX = "Service";
    
    /**
     * @param ctx - EnvironmentContext
     * @param asInfo - AppServer Information
     * @param target - administration target
     */
    public AdminService(EnvironmentContext ctx, String target)
        throws ManagementException
    {
        super(ctx, target);
    }
    
    /**
     * Lookup a schemaorg_apache_xmlbeans.system service {@link LifeCycleMBean} by name. System services
     * are implementation-defined services which can administered through JMX,
     * and have a life cycle.
     * <p>
     * System services are not related to service engines.
     * 
     * @param serviceName name of the schemaorg_apache_xmlbeans.system service; must be non-null and non-
     *        empty; values are implementation-dependent
     * @return JMX object name of the schemaorg_apache_xmlbeans.system service's LifeCycleMBean, or
     *         <code>null</code> if there is no schemaorg_apache_xmlbeans.system service with the given
     *         <code>name</code>.
     */
    public ObjectName getSystemService(String serviceName)
    {
        return mMBeanNames.getSystemServiceMBeanName(
                Enum.valueOf(MBeanNames.ServiceName.class,serviceName),
                getServiceType(serviceName), mTarget);
    }

    /**
     * Looks up all JBI schemaorg_apache_xmlbeans.system services {@link LifeCycleMBean}'s currently
     * installed. System services are implementation-defined services which can 
     * administered through JMX. System services are not related to service 
     * engines.
     * 
     * @return array of LifecycleMBean JMX object names of schemaorg_apache_xmlbeans.system services
     *         currently installed in the JBI implementation; must be non-null;
     *         may be empty
     */
    public ObjectName[] getSystemServices()
    {
        // Constructing this manually for now.
        ObjectName[] names = new ObjectName[3];
        
        // AdminService
        names[0] = mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.AdminService,
                MBeanNames.ServiceType.Admin,
                mTarget);
        
        // InstallationService
        names[1] = mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.InstallationService,
                MBeanNames.ServiceType.Installation,
                mTarget);
        
        // DeploymentService
        names[2] = mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.DeploymentService,
                MBeanNames.ServiceType.Deployment,
                mTarget);
        
        return names;
    }

    /**
     * Find the {@link ComponentLifeCycleMBean} of a JBI Installable Component 
     * by its unique name.
     * 
     * @param componentName the name of the engine or binding component; must be non-
     *        null and non-empty
     * @return the JMX object name of the component's life cycle MBean, or 
     *         <code>null</code> if there is no such component with the given 
     *         <code>name</code>
     */
    public ObjectName getComponentByName(String componentName)
    {
        ObjectName objName = null;
        
        try
        {
            if ( isBinding(componentName) )
            {
                objName = mMBeanNames.getBindingMBeanName(componentName, 
                        MBeanNames.ComponentServiceType.ComponentLifeCycle, mTarget);
            }
            else if ( isEngine(componentName) )
            {
                objName = mMBeanNames.getEngineMBeanName(componentName, 
                        MBeanNames.ComponentServiceType.ComponentLifeCycle, mTarget);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return objName;
    }

    /**
     * Get a list of Facade ComponentLifeCycleMBean's for all binding components 
     * currently installed on the target
     * 
     * @return array of JMX object names of component life cycle MBeans for all 
     *         installed binding components; must be non-null; may be empty
     */
    public ObjectName[] getBindingComponents()
    {
        ObjectName[] bindingComponents = new ObjectName[0];
        
        try
        {
            ObjectName[] names = FacadeMbeanHelper.
                getComponentLifeCycleObjectNames(mEnvCtx.getMBeanServer(),
                mEnvCtx.getMBeanNames(), mTarget);
            
            bindingComponents = filterComponentLifeCyleNames(names, ComponentType.BINDING);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        return bindingComponents;
        
    }

    /**
     * Get a list of {@link ComponentLifeCycleMBean}s for all service engines 
     * currently installed in the JBI schemaorg_apache_xmlbeans.system.
     * 
     * @return array of JMX object names of component life cycle MBeans for all 
     *         installed service engines; must be non-null; may be empty
     */
    public ObjectName[] getEngineComponents()
    {
        ObjectName[] engineComponents = new ObjectName[0];
        
        try
        {
            ObjectName[] names = FacadeMbeanHelper. 
                getComponentLifeCycleObjectNames(mEnvCtx.getMBeanServer(),
                mEnvCtx.getMBeanNames(), mTarget);
            
            engineComponents = filterComponentLifeCyleNames(names, ComponentType.ENGINE);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        return engineComponents;
        
    }    

    /**
     * Check if a given JBI component is a Binding Component.
     * 
     * @param componentName the unique name of the component; must be non-null
     *        and non-empty
     * @return <code>true</code> if the component is a binding component; 
     *         <code>false</code> if the component is a service engine or if
     *         there is no component with the given <code>componentName</code>
     *         installed in the JBI schemaorg_apache_xmlbeans.system
     */
    public boolean isBinding(String componentName)
    {
        boolean isBinding = false;
        try
        {
            isBinding =  ( ComponentType.BINDING == getGenericQuery().getComponentType(componentName)  ? true : false );
        }
        catch ( Exception rex )
        {
            isBinding = false;
        }
        return isBinding;
    }

    /**
     * Check if a given JBI component is a Service Engine.
     * 
     * @param componentName the unique name of the component; must be non-null
     *        and non-empty
     * @return <code>true</code> if the component is a service engine;
     *         <code>false</code> if the component is a binding component, or if
     *         there is no component with the given <code>componentName</code>
     *         installed in the JBI schemaorg_apache_xmlbeans.system
     */
    public boolean isEngine(String componentName)
    {
        
        boolean isEngine = false;
        try
        {
            isEngine =  ( ComponentType.ENGINE == getGenericQuery().getComponentType(componentName)  ? true : false );
        }
        catch ( Exception rex )
        {
            isEngine = false;
        }
        return isEngine;
    }

    /**
     * Return current version and other info about this JBI implementation. The 
     * contents of the returned string are implementation dependent.
     * 
     * @return information string about the JBI implementation, including 
     *         version information; must be non-null and non-empty
     */
    public String getSystemInfo()
    {
        VersionInfo vers = mEnvCtx.getVersionInfo();

        return (
            vers.fullProductName()
            + "\n"
            + vers.majorVersion() +
                "." + vers.minorVersion() +
                "(" + vers.buildNumber() + ")"
            + "\n"
            + vers.copyright()
            + "\n"
        );
    }
    
    /**
     * Query the component extension facade MBeanName.
     * 
     * @param componentName - component identification
     * @return JMX object name of the components Extension facade MBean, or 
     *         <code>null</code> if there is no such MBean registered.
     */
    public ObjectName getComponentExtensionFacadeMBean(String componentName)
    {
        // Start the framework ( come out of lazy init if needed )
        mEnvCtx.isFrameworkReady(true);
        
        com.sun.jbi.management.support.MBeanNamesImpl mbeanNames = 
                (com.sun.jbi.management.support.MBeanNamesImpl) mMBeanNames;
        
        ObjectName extensionMBeanObjectName = 
                mbeanNames.getComponentExtensionFacadeMBeanName(componentName, mTarget);
        
        return extensionMBeanObjectName;
    }
    
    /**
     * Filter out ObjectNames of components based on the provided type.
     *
     * @return the filtered array of ComponentLifeCycle ObjectNames of the required type 
     */
    private ObjectName[] filterComponentLifeCyleNames(ObjectName[] names, ComponentType type)
        throws Exception
    {
        java.util.List<ObjectName> resultObjNames = new java.util.ArrayList();
        
        for ( ObjectName compLCName : names )
        {
            String compName = compLCName.getKeyProperty(FacadeMbeanHelper.COMPONENT_NAME_KEY);
            if ( type == getGenericQuery().getComponentType(compName) )
            {
                resultObjNames.add(compLCName);
            }
        }
        
        return resultObjNames.toArray(new ObjectName[resultObjNames.size()]);
    }
    
    /** Utility method to construct the service type property of an ObjectName
     *  based on the service name.  The following convention is used at present:
     *  if serviceName = AdminService, then serviceType = Admin.
     */
    private MBeanNames.ServiceType getServiceType(String serviceName)
    {
        MBeanNames.ServiceType serviceType = null;
        
        if (serviceName.endsWith(SERVICE_SUFFIX))
        {
            String type = serviceName.replaceAll(SERVICE_SUFFIX, "");
            serviceType = Enum.valueOf(MBeanNames.ServiceType.class, type);
        }
        
        return serviceType;
    }
}
