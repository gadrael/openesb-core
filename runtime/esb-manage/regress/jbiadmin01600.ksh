#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01600.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                     Tests the facade Component Configuration MBean                        #
#                                                                                           #             
#                                                                                           #
# The test component used in this test registers a custom MBean                             #
#                                                                                           #
# The test :                                                                                #
# (a) Package, install and start the test component                                         #
# (b) Get/Set an attribute                                                                  #
#                                                                                           #
#############################################################################################

echo "jbiadmin01600 : Tests the facade Component Configuration MBean for target=server"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.test.component
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.component.configmbeaninstartstop
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.component.configmbeannoattrs

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-with-custom-mbean.jar
COMPONENT_NAME=manage-binding-1
COMPONENT_ARCHIVE1=$JV_SVC_TEST_CLASSES/dist/component-with-config-mbean-rgstrd-in-start.jar
COMPONENT_NAME1=test-component-in-start-stop
COMPONENT_ARCHIVE2=$JV_SVC_TEST_CLASSES/dist/component-with-new-config-mbean-expose-no-attrs.jar
COMPONENT_NAME2=test-component-config-mbean-no-attrs


# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE -Djbi.target="server" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="server" start-component
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE1 -Djbi.target="server" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1  -Djbi.target="server" start-component
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE2 -Djbi.target="server" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2  -Djbi.target="server" start-component

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1 -Djbi.target="server" -Djbi.config.param.name=HostName -Djbi.config.param.value=dummyhost set-component-configuration
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1 -Djbi.target="server" stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1 -Djbi.target="server" -Djbi.config.param.name=HostName -Djbi.config.param.value=dummyhost set-component-configuration

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2 -Djbi.target="server" -Djbi.config.param.name=HostName -Djbi.config.param.value=dummyhost set-component-configuration
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2 -Djbi.target="server" stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2 -Djbi.target="server" -Djbi.config.param.name=HostName -Djbi.config.param.value=dummyhost set-component-configuration

# Test (b) :
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml get.component.configuration

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="server" stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="server" shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="server" uninstall-component

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1  -Djbi.target="server" shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1  -Djbi.target="server" uninstall-component

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2  -Djbi.target="server" shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2  -Djbi.target="server" uninstall-component
