#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00012.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


# required when running a single test
# echo " "
# echo Starting node-agent : agent1
# asadmin start-node-agent --startinstances=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS agent1


echo "jbiadmin00016 : Test show-runtime-configuration for cluster instances."

. ./regress_defs.ksh

echo Creating cluster : cluster1
asadmin create-cluster --port $ASADMIN_PORT $ASADMIN_PW_OPTS cluster1
createClusterDelay

echo Creating instance : instance2
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster cluster1 --nodeagent agent1 instance2
createInstanceDelay

echo Starting instance : instance2
asadmin start-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS instance2
startInstanceDelay

echo " "
echo "executing show-runtime-configuration for instance1"
asadmin show-jbi-runtime-configuration --target instance1 --port $ASADMIN_PORT $ASADMIN_PW_OPTS

echo " "
echo "executing show-runtime-configuration for instance2"
asadmin show-jbi-runtime-configuration --target instance2 --port $ASADMIN_PORT $ASADMIN_PW_OPTS 2>&1

echo Stopping instance : instance2
asadmin stop-instance   --port $ASADMIN_PORT $ASADMIN_PW_OPTS instance2
stopInstanceDelay

echo Deleting instance : instance2
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS instance2
deleteInstanceDelay

echo Deleting cluster : cluster1
asadmin delete-cluster $ASADMIN_PW_OPTS --port $ASADMIN_PORT cluster1
deleteClusterDelay

# required when running a single test
# echo " "
# echo Stopping node-agent : agent1
# asadmin stop-node-agent agent1

