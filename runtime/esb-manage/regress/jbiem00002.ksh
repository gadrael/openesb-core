#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiem00001.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT


echo jbiem00002 - Test event management forwarding configuration DAS and instnace1

. ./regress_defs.ksh

echo Starting node-agent : agent1
asadmin start-node-agent --startinstances=false --user admin --passwordfile $JV_AS8BASE/passwords agent1
startAgentDelay

echo Starting instance : instance1
asadmin start-instance --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords instance1
startInstanceDelay

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiem00002.xml

echo Stopping node-agent : agent1
asadmin stop-node-agent agent1
stopAgentDelay
