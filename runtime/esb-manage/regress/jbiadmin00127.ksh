#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00127.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#          Tests undeploy/uninstallSharedLibrary with keep = true                           #
#############################################################################################

echo "jbiadmin00127 : Test undeploy and uninstallSharedLibrary with keep=true"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml

COMPONENT_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/component-binding1.jar
COMPONENT_NAME_1=manage-binding-1

COMPONENT_ARCHIVE_2=$JV_SVC_TEST_CLASSES/dist/component-binding2.jar
COMPONENT_NAME_2=manage-binding-2

SA_DEPLOY_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/deploy-sa.jar
SA_NAME=esbadmin00089-sa

SL_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/shared-library.jar
SL_NAME=TestSharedLibrary

# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_1  install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_1  start-component

$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_2  install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_2  start-component

$JBI_ANT_NEG -Djbi.deploy.file=$SA_DEPLOY_ARCHIVE deploy-service-assembly

# Test : undeploy with keep=true
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -DSA_NAME=$SA_NAME -f jbiadmin00127.xml undeploy.sa.with.keep
$JBI_ANT_NEG -Djbi.target="domain" list-service-assemblies

# Cleanup
$JBI_ANT_NEG -Djbi.service.assembly.name=$SA_NAME -Djbi.target="domain" undeploy-service-assembly
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_1  stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_1  shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_1 uninstall-component

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_2  stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_2  shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_2 uninstall-component

# Shared Library Test :
# Setup
$JBI_ANT_NEG -Djbi.install.file=$SL_ARCHIVE install-shared-library

# Test : uninstall with keep=true
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -DSL_NAME=$SL_NAME -f jbiadmin00127.xml uninstall.sl.with.keep
$JBI_ANT_NEG -Djbi.target="domain" list-shared-libraries

$JBI_ANT_NEG -Djbi.shared.library.name=$SL_NAME -Djbi.target="domain" uninstall-shared-library
