#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01600.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                     Tests the facade Component Configuration MBean                        #
#                                                                                           #             
#                                                                                           #
# The test component used in this test registers a configuration MBean. The configuration   #
# information is represented in the jbi.xml as per the new configuration schema.            #
#                                                                                           #
# The test :                                                                                #
# (a) Package, install and start the test component                                         #
# (b) Get/Set component configuration                                                       #
#                                                                                           #
#############################################################################################

echo "jbiadmin01606 : Tests the facade Component Configuration MBean for target=server with the new configuration schema."

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.new.test.component

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-with-new-config-mbean.jar
COMPONENT_NAME=manage-binding-1

NEW_APP_CONFIG_FILE=$JV_SVC_REGRESS/deploytest/new-app-config.properties
INCORRECT_NEW_APP_CONFIG_FILE=$JV_SVC_REGRESS/deploytest/incorrect-new-app-config.properties
PARTIALLY_UPDATED_APP_CONFIG_FILE=$JV_SVC_REGRESS/deploytest/partially-updated-app-config.properties

JBI_ANT_CMD="$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target=server"

# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="server" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="server" start-component


# Test (b) :
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml get.component.configuration

# Test adding a application configuration with not all fields present ( fix for issue # 149 ), 
#      this should fail since required field is not specified
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$INCORRECT_NEW_APP_CONFIG_FILE create-application-configuration
$JBI_ANT_CMD list-application-configurations

# Test updating a application configuration with only a subset of the fields updated
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$NEW_APP_CONFIG_FILE create-application-configuration
$JBI_ANT_CMD list-application-configurations
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$PARTIALLY_UPDATED_APP_CONFIG_FILE update-application-configuration
$JBI_ANT_CMD list-application-configurations               

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="server" stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="server" shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target="server" uninstall-component
