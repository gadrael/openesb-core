#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin02000.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin02000 - Synchronization test install/uninstall SL and Component

echo testname is jbiadmin02000
. ./regress_defs.ksh

my_test_domain=CAS
. $SRCROOT/antbld/regress/common_defs.ksh

echo `date` "Executing 2000a.xml..."
ant -d -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f jbiadmin02000a.xml

# Start sync instance 
echo Starting instance : sync
asadmin start-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS sync
syncInstanceDelay

echo Stopping instance : sync
asadmin stop-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS sync
syncInstanceDelay

sed -es/timestamp=\"\[0-9\]\*\"// -es/workspace=\"\.\*workspace\"// -es/install-root=\"\.\*install_root\"// $AS8BASE/nodeagents/agent1/sync/jbi/config/jbi-registry.xml

echo `date` "Executing 2000b.xml..."
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f jbiadmin02000b.xml

echo Starting instance : sync
asadmin start-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS sync
syncInstanceDelay

echo Stopping instance : sync
asadmin stop-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS sync
syncInstanceDelay

sed -es/timestamp=\"\[0-9\]\*\"// -es/workspace=\"\.\*workspace\"// -es/install-root=\"\.\*install_root\"// $AS8BASE/nodeagents/agent1/sync/jbi/config/jbi-registry.xml

echo `date` "Executing 2000c.xml..."
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f jbiadmin02000c.xml

echo Starting instance : sync
asadmin start-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS sync
syncInstanceDelay

echo Stopping instance : sync
asadmin stop-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS sync
syncInstanceDelay

sed -es/timestamp=\"\[0-9\]\*\"// -es/workspace=\"\.\*workspace\"// -es/install-root=\"\.\*install_root\"// $AS8BASE/nodeagents/agent1/sync/jbi/config/jbi-registry.xml
