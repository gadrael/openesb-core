#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin02999.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
. ./regress_defs.ksh

echo stopping instance instance1
asadmin stop-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS instance1
stopInstanceDelay

echo Stopping node-agent : agent1
asadmin stop-node-agent agent1

echo " "
echo list-instances
asadmin list-instances --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS

echo " "
echo list-node-agents
asadmin list-node-agents --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS


