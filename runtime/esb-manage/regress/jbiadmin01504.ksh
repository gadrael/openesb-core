#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01504.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# This tests is for component upgrade 
# A  component that implements the upgrade SPI is installed in server and a standalone instance
# The component is upgraded and it is verified that the component is functional and
# that the component can make changes to its workspace root and SU roots.
# This test verifies component upgrade in server instance and 
# a stand-alone instance instance1
####
echo "jbiadmin01504 : Test Component Upgrade."


. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01504.xml 1>&2

echo install the component in server and instance1
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/upgrade-original.jar
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 $JV_SVC_TEST_CLASSES/dist/upgrade-original.jar

echo deploy the SAs in server and instance1

asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/upgrade-sa1.jar
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/upgrade-sa2.jar
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=instance1 $JV_SVC_TEST_CLASSES/dist/upgrade-sa1.jar
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=instance1 $JV_SVC_TEST_CLASSES/dist/upgrade-sa2.jar

echo shut-down the component in server and instance1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Upgrade-Component
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 Upgrade-Component

#copy the workspace settings file for future checking
cp $JBI_DOMAIN_ROOT/jbi/components/Upgrade-Component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01500-CAS-settings.txt
cp $JBI_DOMAIN_ROOT/../../nodeagents/agent1/instance1/jbi/components/Upgrade-Component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01500-instance1-settings.txt

echo upgrade the component
$JBI_ANT -Djbi.component.name=Upgrade-Component -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/upgrade-modified.jar upgrade-component 2>&1

echo verify if the component upgrades to workspace has been retained
echo "new file created in workspace by upgrade" >  $SVC_TEST_CLASSES/upgrade.txt
diff $JBI_DOMAIN_ROOT/jbi/components/Upgrade-Component/install_root/workspace/upgrade.txt  $SVC_TEST_CLASSES/upgrade.txt 2>&1
diff $JBI_DOMAIN_ROOT/../../nodeagents/agent1/instance1/jbi/components/Upgrade-Component/install_root/workspace/upgrade.txt  $SVC_TEST_CLASSES/upgrade.txt 2>&1

echo verify the workspace dir has been preserved in server and instance1
diff $JBI_DOMAIN_ROOT/jbi/components/Upgrade-Component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01500-CAS-settings.txt 2>&1
diff $JBI_DOMAIN_ROOT/../../nodeagents/agent1/instance1/jbi/components/Upgrade-Component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01500-instance1-settings.txt 2>&1

echo verify that install root has been replaced in server and instance1
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Upgrade-Component
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 Upgrade-Component

echo undeploy the SAs from server and instance1
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Upgrade-SA1
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Upgrade-SA2
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=instance1 Upgrade-SA1
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=instance1 Upgrade-SA2

echo shutdown the component in server and instance1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Upgrade-Component
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 Upgrade-Component

echo uninstall the component from server and instance1
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT Upgrade-Component
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target instance1 Upgrade-Component

rm $SVC_TEST_CLASSES/jbiadmin01500-CAS-settings.txt
rm $SVC_TEST_CLASSES/jbiadmin01500-instance1-settings.txt
rm $SVC_TEST_CLASSES/upgrade.txt

