#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01240.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin01240 : Test for issue 37 starting an SA in a cluster that had a partially successful start"

####
# This test installs a binding component and deploys a SA.
# This SA will start successfully in only one instance in a machine(simulates access to shared resource).
# Using this SA start is made to fail in one instance, leading to a partially
# successful start operation. # After the partial success, a marker file is created to 
# allow further start operations to proceed and start-service-assembly is attempted again
####
. ./regress_defs.ksh

#package component and sa files
ant -q  -lib "$REGRESS_CLASSPATH" -f jbiadmin01240.xml 1>&2

echo Creating a new instance in CAS-cluster1
asadmin create-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords --cluster CAS-cluster1 --nodeagent agent1 CAS-cluster1-inst2 1>&2
createInstanceDelay

#CAS-cluster1-inst is running already
asadmin start-instance  --port $ASADMIN_PORT -u $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords CAS-cluster1-inst2 >&1
startInstanceDelay

#remove the shared file if it exists
if [ -f "$SVC_TEST_CLASSES/dist/shared-file.txt" ]; then
    rm $SVC_TEST_CLASSES/dist/shared-file.txt
fi
if [ -f "$SVC_TEST_CLASSES/dist/ignore-file.txt" ]; then
    rm $SVC_TEST_CLASSES/dist/ignore-file.txt
fi

#install the component, deploy and start the SA in cluster target
asadmin install-jbi-component  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --target=CAS-cluster1 --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/StartOneDeployment-Component.jar  2>&1
installComponentDelay

asadmin deploy-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 $JV_SVC_TEST_CLASSES/dist/cluster-partial-success-sa.jar 2>&1 
deploySaDelay

echo start the SA
$JBI_ANT  -Djbi.target=CAS-cluster1 -Djbi.service.assembly.name=cluster-partial-start-sa start-service-assembly

# now create a file that would let start to be successful in both instances of the cluster
echo test > $SVC_TEST_CLASSES/dist/ignore-file.txt
echo start the SA again
asadmin start-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 cluster-partial-start-sa 2>&1 
startSaDelay

# cleanup
asadmin shut-down-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 cluster-partial-start-sa 2>&1
stopSaDelay
asadmin undeploy-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 cluster-partial-start-sa 2>&1
undeploySaDelay
asadmin shut-down-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 StartOneDeploymentComponent 2>&1
stopComponentDelay
asadmin uninstall-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 StartOneDeploymentComponent 2>&1
uninstallComponentDelay

asadmin stop-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords CAS-cluster1-inst2 >&1
stopInstanceDelay

asadmin delete-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords CAS-cluster1-inst2 >&1
deleteInstanceDelay

rm -rf $AS8BASE/nodeagents/agent1/CAS-cluster1-inst2 
rm $SVC_TEST_CLASSES/dist/ignore-file.txt
rm $SVC_TEST_CLASSES/dist/shared-file.txt
