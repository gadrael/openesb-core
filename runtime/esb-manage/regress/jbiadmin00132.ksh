#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00132.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin00132 : Test fix for CR 6515975"

. ./regress_defs.ksh

SL_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/shared-library.jar
SL_NAME=TestSharedLibrary

#
# Setup :
#
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml runtests


# Install the shared library to two targets
$JBI_ANT -Djbi.install.file=$SL_ARCHIVE  -Djbi.target="server" install-shared-library
$JBI_ANT -Djbi.install.file=$SL_ARCHIVE  -Djbi.target="instance1" install-shared-library


# List the shared library in the domain
$JBI_ANT -Djbi.target="domain" list-shared-libraries

# List the shared library on "server"
$JBI_ANT -Djbi.target="server" list-shared-libraries

# List the shared library on "instance1"
$JBI_ANT -Djbi.target="instance1" list-shared-libraries


# Unnstall the shared library from instance1 with keep=false
$JBI_ANT_NEG -Djbi.shared.library.name=$SL_NAME -Djbi.target="instance1"  uninstall-shared-library

# List the shared library on "server"
$JBI_ANT -Djbi.target="server" list-shared-libraries

# List the shared library on "instance1"
$JBI_ANT -Djbi.target="instance1" list-shared-libraries


# Unnstall the shared library from domain with keep=false, should fail with an error
$JBI_ANT_NEG -Djbi.shared.library.name=$SL_NAME -Djbi.target="domain"  uninstall-shared-library

# List the shared library on "server"
$JBI_ANT -Djbi.target="server" list-shared-libraries

# List the shared library on "instance1"
$JBI_ANT -Djbi.target="instance1" list-shared-libraries

# Unnstall the shared library from server with keep=true
$JBI_ANT_NEG -Djbi.shared.library.name=$SL_NAME -Djbi.target="server"  -Djbi.keep.archive=true uninstall-shared-library


# List the shared library in the domain
$JBI_ANT_NEG -Djbi.target="domain" list-shared-libraries

# Unnstall the shared library from domain with keep=false, should succeed
$JBI_ANT_NEG -Djbi.shared.library.name=$SL_NAME -Djbi.target="domain"  uninstall-shared-library

# List the shared library in the domain
$JBI_ANT_NEG -Djbi.target="domain" list-shared-libraries

# List the shared library on "server"
$JBI_ANT -Djbi.target="server" list-shared-libraries

# List the shared library on "instance1"
$JBI_ANT -Djbi.target="instance1" list-shared-libraries
