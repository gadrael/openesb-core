#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00115.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin00115 : Negative tests for component and shared library install/uninstall"
. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml 

JBI_ANT="$JBI_ANT -Djbi.task.fail.on.error=false"
echo $JBI_ANT

COMPONENT_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/component-binding1.jar
COMPONENT_ARCHIVE_2=$JV_SVC_TEST_CLASSES/dist/component-binding2.jar
COMPONENT_ARCHIVE_3=$JV_SVC_TEST_CLASSES/dist/component-binding3.jar
SA_DEPLOY_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/deploy-sa.jar

COMPONENT1_NAME=manage-binding-1
COMPONENT2_NAME=manage-binding-2
SA_NAME=esbadmin00089-sa


# Test uninstall a component which has not been uninstalled
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_1  -Djbi.target="domain" install-component
$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME uninstall-component

# Test installing a component which requires a shared library but the
# shared library is not installed
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_3  install-component

# Test installing a duplicate component, second install should fail
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_1  install-component
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_1  install-component

$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_2  install-component

$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME start-component
$JBI_ANT -Djbi.component.name=$COMPONENT2_NAME start-component

# Test uninstalling a component with deployed service units
$JBI_ANT -Djbi.deploy.file=$SA_DEPLOY_ARCHIVE deploy-service-assembly
$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME uninstall-component
$JBI_ANT -Djbi.component.name=$COMPONENT2_NAME uninstall-component

# Teardown the test setup
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME undeploy-service-assembly
$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME stop-component
$JBI_ANT -Djbi.component.name=$COMPONENT2_NAME stop-component
$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME shut-down-component
$JBI_ANT -Djbi.component.name=$COMPONENT2_NAME shut-down-component

$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME uninstall-component
$JBI_ANT -Djbi.component.name=$COMPONENT2_NAME uninstall-component
