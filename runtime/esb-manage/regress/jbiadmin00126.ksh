#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00126.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#            Tests forceful service assembly undeploy and shutdown                          #
#                                                                                           #             
# This tests the forced service assembly undeploy and shutdown operation.                   #
#                                                                                           #
# The test component used in this test throws an exception on su undeployment.              #
#                                                                                           #
# The test :                                                                                #
# (a) Shutdown the service assembly normally - that should fail                             #
# (b) Shutdown the service assembly with force=true, the JBI Runtime should ignore          #
#     the component exceptions and go ahead with the shutdown which is a success.           #
# (d) Undeploy the service assembly normally - that should fail                             #
# (d) Undeploy the service assembly with force=true, the JBI Runtime should ignore          #
#     the component exceptions and go ahead with the undeploy which is a success.           #
#                                                                                           #
#                                                                                           #
#############################################################################################

echo "jbiadmin00126 : Tests forceful service assembly undeploy and shutdown"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml 
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00126.xml pkg.test.component

COMPONENT_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/bad-undeploy-binding1.jar
COMPONENT_NAME_1=manage-binding-1

COMPONENT_ARCHIVE_2=$JV_SVC_TEST_CLASSES/dist/bad-undeploy-binding2.jar
COMPONENT_NAME_2=manage-binding-2

SA_DEPLOY_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/deploy-sa.jar
SA_NAME=esbadmin00089-sa

# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_1  install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_1  start-component

$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_2  install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_2  start-component

$JBI_ANT_NEG -Djbi.deploy.file=$SA_DEPLOY_ARCHIVE deploy-service-assembly
$JBI_ANT_NEG -Djbi.service.assembly.name=$SA_NAME start-service-assembly

# Test (a) and (b) :
# shutdown normally, this should fail
$JBI_ANT_NEG -Djbi.service.assembly.name=$SA_NAME shut-down-service-assembly

# shutdowm forcefully, this should succeed
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -DSA_NAME=$SA_NAME -f jbiadmin00126.xml shut.down.sa.with.force

# Test (c) and (d) :
# undeploy normally, this should fail
$JBI_ANT_NEG -Djbi.service.assembly.name=$SA_NAME undeploy-service-assembly

# undeploy forcefully, this should succeed
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -DSA_NAME=$SA_NAME -f jbiadmin00126.xml undeploy.sa.with.force

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_1  stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_1  shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_1 uninstall-component

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_2  stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_2  shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME_2 uninstall-component
