#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00128.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#     Tests getting the object names of the custom MBeans registered by a component         #
#                                                                                           #             
#                                                                                           #
# The test component used in this test registers a custom MBean                             #
#                                                                                           #
# The test :                                                                                #
# (a) Package, install and start the test component                                         #
# (b) Query the custom MBean Names from the ComponentInformationMBean for the installed     #
#     component                                                                             #
#                                                                                           #
#############################################################################################

echo "jbiadmin00128 : Tests getting component custom MBean ObjectNames"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00128.xml pkg.test.component

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-with-custom-mbean.jar
COMPONENT_NAME=manage-binding-1


# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="instance1" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="instance1" start-component


# Test (b) :
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="instance1" -lib "$REGRESS_CLASSPATH" -f jbiadmin00128.xml get.component.configuration
#ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="instance1" -lib "$REGRESS_CLASSPATH" -f jbiadmin00128.xml get.component.loggers

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="instance1" stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="instance1" shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target="instance1" uninstall-component
