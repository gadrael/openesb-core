#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00129.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#################################################################################
#         Test fix for CR 6503120 & 6503180                                     #
#                                                                               #
#    The Test :                                                                 #
#                                                                               #
#    (a) The component is first installed to the domain.                        #
#    (b) A modified component jar is then installed to the server target. This  #
#        is expected to fail with an error that the file exists in the domain.  #
#    (c) A Service assembly is deployed to the domain                           #
#    (d) A modified service assembly is then deployed to the server. This       #
#        is expected to fail with an error that the file exists in the domain.  #
#    (e) A shared library is deployed to the domain                             #
#    (f) A modified shared library is then deployed to the server. This         #
#        is expected to fail with an error that the file exists in the domain.  #
#                                                                               #
#################################################################################

echo "jbiadmin00129 : Test fix for CR 6503120 & 6503180"

. ./regress_defs.ksh

COMPONENT_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/component-binding1.jar
COMPONENT_ARCHIVE_MODIFIED=$JV_SVC_TEST_CLASSES/dist/modified-component.jar
COMPONENT_NAME=manage-binding-1

SA_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/deploy-sa.jar
SA_ARCHIVE_MODIFIED=$JV_SVC_TEST_CLASSES/dist/modified-sa.jar
SA_NAME=esbadmin00089-sa

SL_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/shared-library.jar
SL_ARCHIVE_MODIFIED=$JV_SVC_TEST_CLASSES/dist/modified-sl.jar
SL_NAME=TestSharedLibrary

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml runtests

#
# Component test 
#

$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_1  -Djbi.target="domain" install-component
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00129.xml modify.test.component

# Test (b) :
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_MODIFIED  -Djbi.target="server" install-component

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target="domain" uninstall-component

#
# Service assembly test
#

$JBI_ANT_NEG -Djbi.deploy.file=$SA_ARCHIVE_1  -Djbi.target="domain" deploy-service-assembly
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00129.xml modify.test.service.assembly

# Test (d)
$JBI_ANT_NEG -Djbi.deploy.file=$SA_ARCHIVE_MODIFIED  -Djbi.target="server" deploy-service-assembly

$JBI_ANT_NEG -Djbi.service.assembly.name=$SA_NAME  -Djbi.target="domain" undeploy-service-assembly

#
# Shared library test
#

$JBI_ANT_NEG -Djbi.install.file=$SL_ARCHIVE -Djbi.target="domain" install-shared-library
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00129.xml modify.test.shared.library

# Test (f)
$JBI_ANT_NEG -Djbi.install.file=$SL_ARCHIVE_MODIFIED -Djbi.target="server" install-shared-library

$JBI_ANT_NEG -Djbi.shared.library.name=$SL_NAME -Djbi.target="domain" uninstall-shared-library
