/*
 * BEGIN_HEADER - DO NOT EDIT

 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.

 * You can obtain a copy of the license at
 * http://opensource.org/licenses/cddl1.php.
 * See the License for the specific language governing
 * permissions and limitations under the License.

 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * http://opensource.org/licenses/cddl1.php.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package org.openesb.runtime.tracking;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import org.openesb.runtime.tracking.util.NormalizedMessageUtil;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */
public class MessageTrackingInfo {

    private String comment;
    private Date date;
    private MessageExchange me;
    private Source inMessage;
    private Source outMessage;
    private Source faultMessage;
    private ServiceEndpoint endpoint;
    private String source;
    private String dest;
    private ExchangeStatus status;




    public MessageTrackingInfo(final MessageExchange exchange, final String comment) {

        this.me = exchange;
        this.comment = comment;
        this.date = Calendar.getInstance().getTime();

        try {
            this.inMessage = NormalizedMessageUtil.getInMessageCopy(me);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(MessageTrackingInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            this.outMessage = NormalizedMessageUtil.getOutMessageCopy(me);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(MessageTrackingInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            this.faultMessage = NormalizedMessageUtil.getFaultMessageCopy(me);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(MessageTrackingInfo.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.endpoint = me.getEndpoint();
        this.status = me.getStatus();

    }

    public String getComment() {
        return comment;
    }

    public Date getDate() {
        return date;
    }    

    public Source getInMessage(){
        return inMessage;
    }

    public Source getOutMessage(){
        return outMessage;
    }

    public Source getFaultMessage(){
        return faultMessage;
    }

    public String getExchangeId(){
        return me.getExchangeId();
    }

    public String getMessageTrackingId() {
        final String trId = (String)me.getProperty(MessageExchangeTracker.TRACKING_ID);
        return (trId == null) ? me.getExchangeId() : trId;
    }

    public ServiceEndpoint getEndpoint() {
        return endpoint;
    }

    public String getDestinationComponent() {
        return dest;
    }

    // Totally graceless hack, but we don't know that MessageExchange instance
    // is MessageExchangeProxy indeed.
    public void setDestinationComponent(String dest) {
        this.dest = dest;
    }

    public String getSourceComponent() {
        return source;
    }

    public void setSourceComponent(String source) {
        this.source = source;
    }

    public ExchangeStatus getStatus(){
        return this.status;
    }

}
