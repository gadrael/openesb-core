/*
  * BEGIN_HEADER - DO NOT EDIT

  * The contents of this file are subject to the terms
  * of the Common Development and Distribution License
  * (the "License").  You may not use this file except
  * in compliance with the License.

  * You can obtain a copy of the license at
  * http://opensource.org/licenses/cddl1.php.
  * See the License for the specific language governing
  * permissions and limitations under the License.

  * When distributing Covered Code, include this CDDL
  * HEADER in each file and include the License file at
  * http://opensource.org/licenses/cddl1.php.
  * If applicable add the following below this CDDL HEADER,
  * with the fields enclosed by brackets "[]" replaced with
  * your own identifying information: Portions Copyright
  * [year] [name of copyright owner]
 */


package org.openesb.runtime.tracking.settings;

import java.util.List;
import java.util.Map;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */

/**
 * This class represents message tracking settings:
 * Is tracking enabled for the whole JBI runtime
 * List of classes that implement MessageTrackingTaskRunnable
 * Number of threads in the MessageTrackingTaskExecutor to run MessageTrackingTaskRunnables
 *
 */
public class MessageTrackingSettings {

    private boolean enabled;
    private List<String> tasks;
    private int numberOfThreads;

    private MessageTrackingSettings(){
        enabled = false;
        tasks = null;
        numberOfThreads = 4; // Random generated number ;)
    }


    public static MessageTrackingSettings fromMap(Map<String, Object> rawSettings) 
            throws MessageTrackingSettingsException {
        if (rawSettings == null)
            throw new MessageTrackingSettingsException("Settings map is null.");

        MessageTrackingSettings mts = new MessageTrackingSettings();

        //mts.enabled = Boolean.valueOf((String)rawSettings.get("enabled")).booleanValue();
        mts.enabled = ((Boolean)rawSettings.get("enabled"));
        mts.numberOfThreads = ((Integer)rawSettings.get("numberOfThreads"));
        mts.tasks = (List<String>)rawSettings.get("tasks");

        return mts;
    }


    public static MessageTrackingSettings defaultSettings() {
        return new MessageTrackingSettings();

    }


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public List<String> getTasks() {
        return tasks;
    }

    public void setTasks(List<String> tasks) {
        this.tasks = tasks;
    }

    

}
