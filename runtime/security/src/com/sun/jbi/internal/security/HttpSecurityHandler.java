/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)HttpSecurityHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  HttpSecurityHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 19, 2004, 5:53 PM
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.binding.security.Endpoint;
import com.sun.jbi.binding.security.HttpErrorResponseException;

import java.io.IOException;

import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;


/**
 * No-op implementation of HttpsSecurityHandler.
 * 
 * @author Sun Microsystems, Inc.
 */
public class HttpSecurityHandler
    implements com.sun.jbi.binding.security.HttpSecurityHandler
{
    
    /**
     * Make this a secure Connection. The choice of using SSL3.0/TLS and the TrustStore
     * Keystore details should come from the endpoint/operation details.
     * @param serverURL Is the Server URL the secure connection is being made to.
     * @param endpoint is the Endpoint on behalf of which the secure connection
     * is being made.
     * @throws KeyStoreException when a problem is encountered accessing the KeyStore
     * @throws NoSuchAlgorithmException If the TLS algorithm is unknown
     * @throws KeyManagementException on KeyMamagement errors.
     * @throws CertificateException on certificate related problems.
     * @throws UnrecoverableKeyException If a required Key cannot be obtained from the 
     * store.
     * @throws java.io.IOException on IO realted errors.
     * @return an instance of a Secure URL Connection
     */
    public URLConnection createSecureClientConnection (URL serverURL, Endpoint endpoint)
        throws IOException, 
            KeyStoreException, 
            NoSuchAlgorithmException, 
            KeyManagementException, 
            CertificateException, 
            UnrecoverableKeyException
    {
        return serverURL.openConnection();
       
    }
    
    /**
     * Authenticate a HttpServletRequest.
     *
     * If the Endpoint requires SSL Client Authentication, this method gets the Client
     * Certificate from the request and authenticates the Sender. If a Client Certificate
     * is missing an exception is thrown.
     *
     * If the Endpoint does not require SSL Client Authentication none of the above steps
     * are performed and an empty Subject is returned. This method does not return a null
     * Subject to avoid NullPOinterExceptions.
     *
     * @param request is the HttpServletRequest.
     * @param endpoint is the targeted Endpoint
     * @param subject is the Sender Subject to be updated, if null a new one is created.
     * @throws HttpErrorResponseException when the processing results in a Http Error
     * response to be sent to the client.
     */
    public Subject authenticateSenderRequest (HttpServletRequest request, 
        Endpoint endpoint, Subject subject) throws HttpErrorResponseException
    {
        return new Subject();
    }
    
    /**
     * Authenticate the Sender Request by getting the Sender identity from the
     * X509Certificate
     *
     * @param cert is the trusted X.509 Certificate.
     * @param endpoint is the targeted Endpoint 
     * @throws HttpErrorResponseException when the processing results in a Http Error
     * response to be sent to the client.
     */
    public Subject authenticateSenderRequest(X509Certificate cert, 
        Endpoint endpoint, Subject subject)
            throws HttpErrorResponseException
    {
        return new Subject();
    }
   
    
}
