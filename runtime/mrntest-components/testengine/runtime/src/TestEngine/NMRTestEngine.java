/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRTestEngine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package TestEngine;

import com.sun.enterprise.ComponentInvocation;
import com.sun.enterprise.InvocationManager;
import com.sun.enterprise.Switch;

import com.sun.messaging.XAQueueConnectionFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.StringReader;

import java.util.logging.Logger;

import javax.activation.DataHandler;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ServiceUnitManager;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.jms.QueueBrowser;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.MessageConsumer;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
//import javax.jms.XAQueueConnectionFactory;
import javax.jms.XAQueueConnection;
import javax.jms.XAQueueSession;

import javax.naming.InitialContext;

import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;

/**
 * This is a servlet that acts as a Service Engine and tests the dynamic
 * registration support provided by JBI. It registers itself as an engine and
 * then performs a fixed set of message exchanges with another servlet that
 * acts as a Binding Component. That servlet is ESBBindingServlet.
 *
 * @author Sun Microsystems, Inc.
 */
public class NMRTestEngine
    implements Component, ComponentLifeCycle
{

   /**
    * Logger for the servlet.
    */
    private java.util.logging.Logger    mLog;

   /**
    * Handle to the XML Document Builder.
    */
    private DocumentBuilder             mDocumentBuilder;

   /**
    * Engine channel for NMS communications.
    */
    private DeliveryChannel             mChannel;

   /**
    * Engine's endpoint.
    */
    private ServiceEndpoint             mEngineEndpoint1;
    private ServiceEndpoint             mEngineEndpoint2;
    
   /**
    * Component LifeCycle.
    */
    private ComponentContext            mContext;
    
   /**
    * Runner Thread.
    */
    private Thread                      mRunnerThread;
    private boolean                     mRunning;
    
    /**
     * Transaction Manager.
     */
    
    private TransactionManager              mTM;
    
   /** SE Queue to send messages. */
    private Queue mDirectQueueEngine = null;
    private Queue mJNDIQueueEngine = null;

    /** QueueConnection for JMS connection */
    private XAQueueConnection mQDirectConn = null;
    private QueueConnection mQJNDIConn = null;

    /** Factory of QueueConnections for JMS Tests */
    private XAQueueConnectionFactory mQDirectFactory = null;
    private QueueConnectionFactory mQJNDIFactory = null;

    /** QueueSession to be used for sending/receiving messages */
    private XAQueueSession mDirectQueueSession = null;
    private QueueSession mJNDIQueueSession = null;
    
   /**
    * The engine's service name.
    */
    private static final QName ENGINE_SERVICE =
        new QName("nmr-test-engine_service");

   /**
    * Qualified name for the binding's service.
    */
    private static final QName BINDING_SERVICE =
        new QName("nmr-test-binding_service");

   /**
    *  The engine's endpoint name.
    */
    private static final String ENGINE_ENDPOINT1 = "nmr-test-engine_endpoint1";
    private static final String ENGINE_ENDPOINT2 = "nmr-test-engine_endpoint2";

   /**
    * JBI component name
    */
    private static final String JBI_COMPONENT_NAME = "NMRTestEngine";

   /**
    * JBI component description
    */
    private static final String JBI_COMPONENT_DESC = "NMRTestEngine acting as an SE";

    public NMRTestEngine()
    {
        mLog = Logger.getLogger(this.getClass().getPackage().getName());
    }
//--------------------------- Component Methods ------------------------------

    /**
     * Get the ComponentLifeCycle instance.
     * @return The ComponentLifeCycle instance for this component.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        mLog.info("Component getLifeCycle() called");
        return this;
    }

    /**
     * Get the ServiceUnitManager instance.
     * @return The ServiceUnitManager instance, which is always null for
     * this test.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        mLog.info("Component getServiceUnitManager() called");
        return null;
    }
    
    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually
     * perform the operation desired.
     *
     * @param endpoint
     * @param exchange
     *
     * @return  true if its OK.
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        /* Should not always return true
         * The capabilities of this component has to be checked first.
         * Not implemented right now
         */
        return true;
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually
     * interact with the the provider completely.
     *
     * @param endpoint
     * @param exchange
     *
     * @return  true if OK.
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        /* Should not always return true
         * The capabilities of this component has to be checked first.
         * Not implemented right now
         */
        return true;
    }

    /**
     * Returns a resolver for meta-data.
     *
     * @param ServiceEndpoint endpoint reference object.
     *
     * @return Descriptor for this engine.
     */
    public org.w3c.dom.Document getServiceDescription(
        javax.jbi.servicedesc.ServiceEndpoint ServiceEndpoint)
    {
        org.w3c.dom.Document desc = null;

        try
        {
            Source          source;
            DOMResult       dr;
            Transformer     transform;
            String          wsdl = 
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<definitions xmlns=\"http://www.w3.org/ns/wsdl\" " +
                        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                        "xsi:schemaLocation=\"http://www.w3.org/ns/wsdl wsdl20.xsd\" " +
                        "targetNamespace=\"http://sun.com/proxybinding.wsdl\">" +
                    "<interface name=\"testPBIF\">" +
                        "<operation name=\"transform\" style=\"http://www.w3.org/ns/wsdl/style/rpc\" " +
                                "pattern=\"http://www.w3.org/ns/wsdl/in-out\">"+
                            "<input messageLabel=\"A\" />" +
                            "<output messageLabel=\"B\" />" +
                        "</operation>" +
                    "</interface>" +    
                    "<binding name=\"testPB\" interface=\"tns:testPBIF\" " + 
 			 "type=\"http://sun.com/jbi/wsdl/proxybinding10\">" +
                        "<operation ref=\"transform\">" +
                            "<input messageLabel=\"A\">" +
                            "</input>" +
                            "<output messageLabel=\"B\">" +
                            "</output>" +
                        "</operation>" +
                    "</binding>" +
                    "<service name=\"testPBService\" interface=\"tns:testPBIF\">" +
                        "<documentation>" +
                            "Tests the ProxyBinding service." +
                        "</documentation>" +	
                        "<endpoint name=\"testPB\" binding=\"tns:testPB\"/>" +
                    "</service>" +
                "</definitions>";
            source = new StreamSource(new StringReader(wsdl));
            transform = TransformerFactory.newInstance().newTransformer();            
            transform.transform(source, dr = new DOMResult());
            desc = (Document)dr.getNode();
        }
        catch (Exception e)
        {
            mLog.info("NMRTestEngine: getServiceDescription failed: " + e);
        }

        return desc;
    }


    
    /**
     * Resolve the endpoint reference using the given capabilities of the
     * consumer. This is called by JBI when its trying to resove the given EPR
     * on behalf of the component.
     *
     * @param epr endpoint reference.
     *
     * @return  Service enpoint.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return mEngineEndpoint1;
    }

//----------------------- ComponentLifeCycle Methods -------------------------

    /**
     * Initializes the JBI LifeCycle. This creates/opens a delivery channel
     * for the servlet to use for interactions with the NMR.
     * @param context - the component context provided by the JBI framework.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void init(ComponentContext context) throws javax.jbi.JBIException
    {
        mLog.info("NMRTEstEngine: LifeCycle init() entered");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        try
        {
            mDocumentBuilder = dbf.newDocumentBuilder();
        }
        catch ( javax.xml.parsers.ParserConfigurationException pcEx )
        {
            throw new javax.jbi.JBIException(
                "Unable to create new DocumentBuilder instance: " + pcEx, pcEx);
        }

        getServiceDescription(null);

        mContext = context;
        mChannel = mContext.getDeliveryChannel();
        mEngineEndpoint1 = mContext.activateEndpoint(ENGINE_SERVICE, ENGINE_ENDPOINT1);
        mEngineEndpoint2 = mContext.activateEndpoint(ENGINE_SERVICE, ENGINE_ENDPOINT2);

        mLog.info("NMRTEstEngine: LifeCycle init() exited");
    }

    /**
     * Get the Extension MBean name.
     * @return The JMX object name of the Extension MBean for this SE,
     * which is always null in this test.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Start the JBI LifeCycle. This activates an inbound service and an
     * outbound service for use in message exchanges with the binding servlet.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void start() throws javax.jbi.JBIException
    {
        mLog.info("NMRTestEngine: LifeCycle start() entered");

        //
        //  Get Various JMS resources needed for the test.
        //
        try
        {
            mQDirectFactory = (XAQueueConnectionFactory) Class.forName("com.sun.messaging.XAQueueConnectionFactory").newInstance();
            mQDirectFactory.setProperty("imqBrokerHostPort", "9000");
            mQDirectFactory.setProperty("imqBrokerHostName", "localhost");
        }
        catch (Exception e)
        {
            throw new javax.jbi.JBIException("Can't get MQ factory: " + e);            
        }
        try
        {
            //
            //  Try a direct connection to JMS that bypasses the JMS RA.
            //
            mQDirectConn = mQDirectFactory.createXAQueueConnection();
            mDirectQueueSession = mQDirectConn.createXAQueueSession();
            mDirectQueueEngine = new com.sun.messaging.Queue("QUEUE_NMR_TEST_ENGINE");
            
            //
            //  Get JNDI connection to JMS that uses the JMS RA.
            //
            prepareThreadForXA();
            mQJNDIFactory = (QueueConnectionFactory) mContext.getNamingContext().lookup("jms/NMRTestQueueFactory");
            mQJNDIConn = mQJNDIFactory.createQueueConnection();
            mJNDIQueueEngine = (Queue) mContext.getNamingContext().lookup("jms/QUEUE_NMR_TEST_ENGINE2");
            mJNDIQueueSession = mQJNDIConn.createQueueSession(true, 0);
            
            //
            //  Get the Transaction Manager.
            //
            mTM = (TransactionManager)mContext.getTransactionManager();
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new javax.jbi.JBIException("Cant' create JMS resources: " + jEx);
        }
        catch (javax.naming.NamingException nEx)
        {
            throw new javax.jbi.JBIException("Cant' create JMS resources via JNDI: " + nEx);           
        }

        mRunning = true;
        mRunnerThread = new Thread (new EngineRunner());
        mRunnerThread.setName("NMRTestEngine-Runner");
        mRunnerThread.setDaemon(true);
        mRunnerThread.start();
        mLog.info("NMRTestEngine: LifeCycle start() exited");
    }
    
    /**
     * Stop the JBI LifeCycle. This deactivates the inbound and outbound
     * services.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void stop() throws javax.jbi.JBIException
    {
        mLog.info("NMRTestEngine: LifeCycle stop() entered");
        mRunning = false;
        mRunnerThread.interrupt();
        mContext.deactivateEndpoint(
            (javax.jbi.servicedesc.ServiceEndpoint) mEngineEndpoint1);
        mContext.deactivateEndpoint(
            (javax.jbi.servicedesc.ServiceEndpoint) mEngineEndpoint2);

        mLog.info("NMRTestEngine: LifeCycle stop() exited");
    }

    /**
     * Shutdown the JBI LifeCycle. This closes the engine channel.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void shutDown() throws javax.jbi.JBIException
    {
        mLog.info("NMRTestEngine: LifeCycle shutDown() entered");

        mChannel.close();

        mLog.info("NMRTestEngine: LifeCycle shutDown() exited");
    }

//----------------------------- Private Methods ------------------------------

    public void prepareThreadForXA()
    {
        InvocationManager im;

        im = Switch.getSwitch().getInvocationManager();
        if (im.getCurrentInvocation() == null)
        {
            im.preInvoke(new ComponentInvocation(this, this));
        }
     }
    
    /**
     * Create a Normalized Message.
     * @param exchange - a MessageExchange for which a message is to be created.
     * @param content - a String containing the message content.
     * @return The NormalizedMessage instance with the provided content.
     */
    private NormalizedMessage createMessage(MessageExchange exchange,
                                            String content)
    {
        NormalizedMessage msg = null;
        try
        {
            Document doc;
            Element elem;
            msg = exchange.createMessage();
            doc = mDocumentBuilder.newDocument();
            elem = doc.createElement("message");
            elem.appendChild(doc.createTextNode(content));
            doc.appendChild(elem);
            msg.setContent(new DOMSource(doc));
        }
        catch ( javax.jbi.messaging.MessagingException ex )
        {
            mLog.warning("MessagingException " + ex.toString());
        }
        return msg;
    }
   
    /**
     * Create a Normalized Message.
     * @param exchange - a MessageExchange for which a message is to be created.
     * @param content - a String containing the message content.
     * @return The NormalizedMessage instance with the provided content.
     */
    private Fault createFault(MessageExchange exchange,
                                            String content)
    {
        Fault fault = null;
        try
        {
            Document doc;
            Element elem;
            fault = exchange.createFault();
            doc = mDocumentBuilder.newDocument();
            elem = doc.createElement("message");
            elem.appendChild(doc.createTextNode(content));
            doc.appendChild(elem);
            fault.setContent(new DOMSource(doc));
        }
        catch ( javax.jbi.messaging.MessagingException ex )
        {
            mLog.warning("MessagingException " + ex.toString());
        }
        return fault;
    }


    class EngineRunner
            implements java.lang.Runnable
    {
        public void run() 
        {
            MessageExchange         me;
            
            mLog.info("NMRTestEngine: Start processing requests");
            prepareThreadForXA();
            
            while (mRunning)
            {
                try
                {
                    me = mChannel.accept();
                    if (me instanceof InOut)
                    {
                        InOut io = (InOut)me;

                        mLog.info("NMRTestEngine: start: " + io.getOperation());
                        Transaction     xact;
                        XAResource      resource;
                        Queue           queue;
                        QueueSession    session;
                        
                        try
                        {
                            xact = (Transaction)io.getProperty(MessageExchange.JTA_TRANSACTION_PROPERTY_NAME);
                            
                            if (io.getOperation().toString().indexOf("Direct") >= 0)
                            {
                                session = (QueueSession)mDirectQueueSession;
                                queue = mDirectQueueEngine;
                            }
                            else
                            {
                                session = mJNDIQueueSession;
                                queue = mJNDIQueueEngine;
                            }
                            resource = ((XAQueueSession)session).getXAResource();
                            xact.enlistResource(resource);

                            io.setOutMessage(createMessage(io, "NMRTestEngine InOut Local Commit"));

                            //
                            //  Queue message to our local resource.
                            //
                            MessageProducer mp = session.createProducer(queue);
                            TextMessage t = session.createTextMessage();;
                            t.setText("NMRTestBInding InOut " + io.getOperation().toString());
                            mp.send(t);
                            mp.close();

                            xact.delistResource(resource, XAResource.TMSUCCESS);
                            
                            mChannel.send(io);
                            io = (InOut) mChannel.accept();
                            if ( io.getStatus() != ExchangeStatus.DONE )
                            {
                                mLog.info("NMRTestEngine: Not DONE " + io.getStatus());
                            }
                        }
                        catch (Exception e)
                        {
                            mLog.info("NMRTestEngine: Exception " + e);
                        }
                        mLog.info("NMRTestEngine: end: " + io.getOperation());
                    }
                }      
                catch ( javax.jbi.messaging.MessagingException mEx )
                {
                    mLog.info("NMRTestEngine: Messaging error: " + mEx);
                }
            }
        
            mLog.info("NMRTestEngine: Processing stopping");
        }
    }

	String getAttachment(MessageExchange me)
	{
		NormalizedMessage		nm;
		DataHandler				a;
		StringBuffer			s = new StringBuffer();
		int						c;

		try
		{
			nm = me.getMessage("in");
			a = nm.getAttachment("Attachment1");
			InputStreamReader isr = new InputStreamReader(a.getInputStream());
			while ((c = isr.read()) != -1)
			{
				s.append((char)c);
			}
			return (s.toString());
		}
		catch (java.io.IOException ioEx)
		{
		}
		return (null);
	}
}
