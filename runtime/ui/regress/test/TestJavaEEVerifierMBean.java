/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJavaEEVerifierMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package test;
import javax.management.MBeanException;
import javax.management.openmbean.TabularData;
import com.sun.jbi.ui.common.JBIRemoteException;

/**
 * This class is used to provide a dummy MBean implementation 
 * for testing JavaEEVerifier integration.
 * @author Sun Microsystems, Inc.
 */
   
public interface TestJavaEEVerifierMBean
{    
    

    /***
     *  Verify  a service unit targeted to the JavaEE SE. This operation verifies the javaee/j2ee
     *  application and returns a CompositeData which has the verification status i.e. information
     *  related to the resources referenced by the application but are missing.
     *
     *  @param serviceAssemblyPath - this is the path to the SA.
     *  @param suZipName - this is the SU zip name
     *  @param target - identifies the target for the service unit verification. This could
     *           be the DAS server instance "server", a standalone or clustered instance name
     *           or cluster.
     *  @return a TabularData with the verification result. The CompositeType of TabularData
     *       The column names in composite data are - 
     *      "Ear Filename", "Referrence By","Referrence Class", 
     *      "JNDI Name", "JNDI Class Type", "Message", "Status"
     *
     */
    public TabularData verifyServiceUnit(String serviceAssemblyPath, String suZipName, String target) throws MBeanException;


}
