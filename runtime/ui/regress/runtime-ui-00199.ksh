#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)runtime-ui-00199.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# Stats Test
####

echo "runtime-ui-00199 : Stop servers/clusters."

. ./regress_defs.ksh

asadmin stop-instance --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords TestInstance1
asadmin stop-instance --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords TestInstance2

asadmin delete-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords  TestInstance1
asadmin delete-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords  TestInstance2
asadmin delete-cluster  --port $ASADMIN_PORT --user $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords  testcluster


echo Stopping node-agent : agent1
asadmin stop-node-agent agent1

testDelay 10
