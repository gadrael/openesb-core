/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RuntimeManagementServiceMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.runtime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.management.ObjectName;

import com.sun.esb.management.api.runtime.RuntimeManagementService;
import com.sun.esb.management.base.services.AbstractListStateServiceMBeansImpl;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.runtime.GenericsSupport;

/**
 * Defines operations for common runtime management services. Common runtime
 * management operations include listing component containers available in the
 * runtime, composite applications deployed, controlling lifecycle across the
 * runtime and composite applications, getting state of each container and
 * composite application, etc.
 * 
 * @author graj
 */
public class RuntimeManagementServiceMBeanImpl extends
        AbstractListStateServiceMBeansImpl implements RuntimeManagementService,
        Serializable {
    
    static final long serialVersionUID = -1L;
    
    /**
     * Constructor - Constructs a new instance of RuntimeManagementServiceMBeanImpl
     * 
     * @param anEnvContext
     */
    public RuntimeManagementServiceMBeanImpl(EnvironmentContext anEnvContext) {
        super(anEnvContext);
    }
    
    /**
     * return component info xml text that has only binding component infos.
     * 
     * @param targetName
     * @return the component info xml text.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listBindingComponents(java.lang.String)
     */
    public String listBindingComponents(String targetName)
            throws ManagementRemoteException {
        return listBindingComponents(null, null, null, targetName);
    }
    
    /**
     * return component info xml text that has only binding component infos
     * which satisfies the options passed to the method.
     * 
     * @param state
     *            return all the binding components that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the binding components that have a dependency on
     *            the specified shared library. null value to ignore this
     *            option.
     * @param serviceAssemblyName
     *            return all the binding components that have the specified
     *            service assembly deployed on them. null value to ignore this
     *            option.
     * @param targetName
     * @return xml text contain the list of binding component infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listBindingComponents(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String listBindingComponents(String state, String sharedLibraryName,
            String serviceAssemblyName, String targetName)
            throws ManagementRemoteException {
        return this.listComponents(ComponentType.BINDING, state,
                sharedLibraryName, serviceAssemblyName, targetName);
    }
    
    /**
     * returns a list of Service Assembly Infos in a xml format.
     * 
     * @param targetName
     * @return xml text containing the Service Assembly infos
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String)
     */
    public String listServiceAssemblies(String targetName)
            throws ManagementRemoteException {
        return listServiceAssemblies(null, targetName);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetName
     * @return xml string contain the list of service assembly infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String,
     *      java.lang.String)
     */
    public String listServiceAssemblies(String componentName, String targetName)
            throws ManagementRemoteException {
        return listServiceAssemblies(null, componentName, targetName);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param state
     *            to return all the service assemblies that are in the specified
     *            state. JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or
     *            null for ANY state
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetName
     * @return xml string contain the list of service assembly infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String listServiceAssemblies(String state, String componentName,
            String targetName) throws ManagementRemoteException {
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        this.validateUiServiceAssemblyInfoState(state);
        
        List saInfoList = this.getServiceAssemblyInfoList(
                toFrameworkServiceAssemblyState(state), componentName,
                targetName);
        
        return ServiceAssemblyInfo.writeAsXmlTextWithProlog(saInfoList);
    }
    
    /**
     * return component info xml text that has only service engine infos.
     * 
     * @param targetName
     * @return the component info xml text.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceEngines(java.lang.String)
     */
    public String listServiceEngines(String targetName)
            throws ManagementRemoteException {
        return this.listServiceEngines(null, null, null, targetName);
    }
    
    /**
     * return component info xml text that has only service engine infos which
     * satisfies the options passed to the method.
     * 
     * @param state
     *            return all the service engines that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the service engines that have a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return all the service engines that have the specified service
     *            assembly deployed on them. null value to ignore this option.
     * @param targetName
     * @return xml text contain the list of service engine component infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceEngines(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String listServiceEngines(String state, String sharedLibraryName,
            String serviceAssemblyName, String targetName)
            throws ManagementRemoteException {
        return listComponents(ComponentType.ENGINE, state, sharedLibraryName,
                serviceAssemblyName, targetName);
    }
    
    /**
     * return component info xml text that has only shared library infos.
     * 
     * @param targetName
     * @return the component info xml text.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraries(java.lang.String)
     */
    public String listSharedLibraries(String targetName)
            throws ManagementRemoteException {
        return listSharedLibraries(null, targetName);
    }
    
    /**
     * returns the list of Shared Library infos in the in a xml format
     * 
     * @param componentName
     *            to return only the shared libraries that are this component
     *            dependents. null for listing all the shared libraries in the
     *            schemaorg_apache_xmlbeans.system.
     * @param targetName
     * @return xml string contain the list of componentinfos for shared
     *         libraries.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraries(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String listSharedLibraries(String componentName, String targetName)
            throws ManagementRemoteException {
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        List frameworkCompInfoList = this
                .getFrameworkComponentInfoListForSharedLibraries(componentName,
                        targetName);
        List uiCompInfoList = new ArrayList();
        uiCompInfoList = this.toUiComponentInfoList(frameworkCompInfoList,
                targetName);
        return JBIComponentInfo.writeAsXmlText(uiCompInfoList);
    }
    
    /**
     * returns a list of Binding Component and Service Engine infos in xml
     * format, that are dependent upon a specified Shared Library
     * 
     * @param sharedLibraryName
     *            the shared library name
     * @param targetName
     * @return xml string containing the list of componentInfos
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraryDependents(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String listSharedLibraryDependents(String sharedLibraryName,
            String targetName) throws ManagementRemoteException {
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        Collection compNames = this.getComponentNamesDependentOnSharedLibrary(
                sharedLibraryName, targetName);
        return JBIComponentInfo.writeAsXmlText(this.toUiComponentInfoList(
                getFrameworkComponentInfoList(compNames, targetName),
                targetName));
    }
    
    /**
     * return component info xml text for the specified binding component if
     * exists. If no binding component with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the binding component to lookup
     * @param state
     *            return the binding component that is in the specified state.
     *            valid states are JBIComponentInfo.STARTED, STOPPED, INSTALLED
     *            or null for ANY state
     * @param sharedLibraryName
     *            return the binding component that has a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return the binding component that has the specified service
     *            assembly deployed on it. null value to ignore this option.
     * @param targetName
     * @return xml text contain the binding component info that confirms to the
     *         component info list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showBindingComponent(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String)
     */
    public String showBindingComponent(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        return this.showComponent(name, ComponentType.BINDING, state,
                sharedLibraryName, serviceAssemblyName, targetName);
    }
    
    /**
     * return service assembly info xml text for the specified service assembly
     * if exists. If no service assembly with that name exists, it returns the
     * xml with empty list.
     * 
     * @param name
     *            name of the service assembly to lookup
     * @param state
     *            return the service assembly that is in the specified state.
     *            JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or null for
     *            ANY state
     * @param componentName
     *            return the service assembly that has service units on this
     *            component.
     * @param targetName
     * @return xml string contain service assembly info that confirms to the
     *         service assembly list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showServiceAssembly(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String showServiceAssembly(String name, String state,
            String componentName, String targetName)
            throws ManagementRemoteException {
        logDebug("show ServiceAssembly info for : " + name);
        
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        validateUiServiceAssemblyInfoState(state);
        
        String saName = null;
        if (null != name && name.trim().length() > 0) {
            saName = name.trim();
        }
        
        List uiInfoList = new ArrayList();
        
        if (saName == null) {
            // return empty list
            return ServiceAssemblyInfo.writeAsXmlTextWithProlog(uiInfoList);
        }
        
        // get the service infos with the state and component dependency
        List saInfoList = getServiceAssemblyInfoList(
                toFrameworkServiceAssemblyState(state), componentName,
                targetName);
        
        // now check that this saName is there in saInfoList
        for (Iterator itr = saInfoList.iterator(); itr.hasNext();) {
            ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) itr.next();
            if (saInfo.getName().equals(saName)) {
                uiInfoList.add(saInfo);
                break;
            }
        }
        
        return ServiceAssemblyInfo.writeAsXmlTextWithProlog(uiInfoList);
    }
    
    /**
     * return component info xml text for the specified service engine if
     * exists. If no service engine with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the service engine to lookup
     * @param state
     *            return service engine that is in the specified state. valid
     *            states are JBIComponentInfo.STARTED, STOPPED, INSTALLED or
     *            null for ANY state
     * @param sharedLibraryName
     *            return service engine that has a dependency on the specified
     *            shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return the service engine that has the specified service
     *            assembly deployed on it. null value to ignore this option.
     * @param targetName
     * @return xml text contain the service engine component info that confirms
     *         to the component info list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showServiceEngine(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String)
     */
    public String showServiceEngine(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        return this.showComponent(name, ComponentType.ENGINE, state,
                sharedLibraryName, serviceAssemblyName, targetName);
    }
    
    /**
     * return component info xml text for the specified shared library if
     * exists. If no shared library with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the shared library to lookup
     * @param componentName
     *            return the shared library that is this component dependents.
     *            null to ignore this option.
     * @param targetName
     * @return xml string contain shared library component info that confirms to
     *         the component info list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showSharedLibrary(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String showSharedLibrary(String name, String componentName,
            String targetName) throws ManagementRemoteException {
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        ArrayList uiCompInfoList = new ArrayList();
        
        ComponentQuery componentQuery = this
                .getFrameworkComponentQuery(targetName);
        ComponentInfo componentInformation = null;
        if (componentQuery != null) {
            componentInformation = componentQuery.getSharedLibraryInfo(name);
            if ((componentInformation != null)
                    && (ComponentType.SHARED_LIBRARY == componentInformation
                            .getComponentType())) {
                uiCompInfoList.add(this.toUiComponentInfo(componentInformation,
                        targetName));
            }
        }
        
        return JBIComponentInfo.writeAsXmlText(uiCompInfoList);
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @return name of the component
     * @param force
     *            true to force shutdown
     * @param componentName
     *            name of the component
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownComponent(java.lang.String,
     *      boolean, java.lang.String)
     */
    public String shutdownComponent(String componentName, boolean force,
            String targetName) throws ManagementRemoteException {
        logDebug("shutting down Component " + componentName);
        
        ObjectName lifecycleObjectName = this
                .getComponentLifeCycleMBeanObjectName(componentName, targetName);
        
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(lifecycleObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        Object result = null;
        logDebug("Calling shutDown on Component LifecycleMBean "
                + lifecycleObjectName);
        if (force == true) {
            Object[] params = new Object[1];
            params[0] = force;
            
            String[] signature = new String[1];
            signature[0] = "boolean";
            result = this.invokeMBeanOperation(lifecycleObjectName, "shutDown",
                    params, signature);
        } else {
            result = this.invokeMBeanOperation(lifecycleObjectName, "shutDown");
        }
        
        return componentName;
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @return name of the component
     * @param componentName
     *            name of the component
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownComponent(java.lang.String,
     *      java.lang.String)
     */
    public String shutdownComponent(String componentName, String targetName)
            throws ManagementRemoteException {
        boolean force = false;
        return this.shutdownComponent(componentName, force, targetName);
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceShutdown
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownServiceAssembly(java.lang.String,
     *      boolean, java.lang.String)
     */
    public String shutdownServiceAssembly(String serviceAssemblyName,
            boolean forceShutdown, String targetName)
            throws ManagementRemoteException {
        return this.shutdownServiceAssemblyInternal(serviceAssemblyName,
                forceShutdown, targetName);
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @return result as a management message xml text
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String shutdownServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        boolean force = false;
        return this.shutdownServiceAssemblyInternal(serviceAssemblyName, force,
                targetName);
    }
    
    /**
     * starts component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#startComponent(java.lang.String,
     *      java.lang.String)
     */
    public String startComponent(String componentName, String targetName)
            throws ManagementRemoteException {
        logDebug("starting Component " + componentName);
        
        ObjectName lifecycleObjectName = this
                .getComponentLifeCycleMBeanObjectName(componentName, targetName);
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(lifecycleObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        Object result = null;
        logDebug("Calling start on Component LifecycleMBean "
                + lifecycleObjectName);
        result = invokeMBeanOperation(lifecycleObjectName, "start");
        
        return componentName;
    }
    
    /**
     * starts service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @return result as a management message xml text
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#startServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String startServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        logDebug("Starting Service Assembly " + serviceAssemblyName);
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(deploymentServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        logDebug("Calling start on DeploymentServiceMBean = "
                + deploymentServiceObjectName);
        
        Object resultObject = this.invokeMBeanOperation(
                deploymentServiceObjectName, "start", serviceAssemblyName);
        
        String result = null;
        if (resultObject != null) {
            result = resultObject.toString();
            if ((serviceAssemblyName == null)
                    || (result.contains("FAILED") == true)
                    || (result.contains("WARNING") == true)) {
                result = resultObject.toString();
            } else {
                result = serviceAssemblyName;
            }
            return result;
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.sasm.start.error", null, null);
            throw new ManagementRemoteException(exception);
            
        }
    }
    
    /**
     * stops component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @return name of the component
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#stopComponent(java.lang.String,
     *      java.lang.String)
     */
    public String stopComponent(String componentName, String targetName)
            throws ManagementRemoteException {
        logDebug("stopping Component " + componentName);
        
        ObjectName lifecycleObjectName = this
                .getComponentLifeCycleMBeanObjectName(componentName, targetName);
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(lifecycleObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        Object result = null;
        logDebug("Calling stop on Component LifecycleMBean "
                + lifecycleObjectName);
        result = this.invokeMBeanOperation(lifecycleObjectName, "stop");
        
        return componentName;
    }
    
    /**
     * stops service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @return result as a management message xml text
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#stopServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String stopServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        logDebug("Stopping Service Assembly " + serviceAssemblyName);
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(deploymentServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        logDebug("Calling stop on Deployment Service MBean = "
                + deploymentServiceObjectName);
        
        Object resultObject = this.invokeMBeanOperation(
                deploymentServiceObjectName, "stop", serviceAssemblyName);
        
        String result = null;
        if (resultObject != null) {
            result = resultObject.toString();
            if ((serviceAssemblyName == null)
                    || (result.contains("FAILED") == true)
                    || (result.contains("WARNING") == true)) {
                result = resultObject.toString();
            } else {
                result = serviceAssemblyName;
            }
            return result;
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.sasm.stop.error", null, null);
            throw new ManagementRemoteException(exception);
        }
    }
    
    /////////////////////////////////////////////
    // Start of Cumulative Operation Definitions
    /////////////////////////////////////////////
    
    /**
     * return component info xml text that has only binding component infos.
     * 
     * @param targetNames
     * @return the component info xml text as a Map of [targetName, xmlString].
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> listBindingComponents(String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listBindingComponents(targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * return component info xml text that has only binding component infos
     * which satisfies the options passed to the method.
     * 
     * @param state
     *            return all the binding components that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the binding components that have a dependency on
     *            the specified shared library. null value to ignore this
     *            option.
     * @param serviceAssemblyName
     *            return all the binding components that have the specified
     *            service assembly deployed on them. null value to ignore this
     *            option.
     * @param targetName
     * @return xml text contain the list of binding component infos as map of
     *         [targetName, xmlString]
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> listBindingComponents(String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listBindingComponents(state, sharedLibraryName, serviceAssemblyName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * returns a list of Service Assembly Infos in a xml format.
     * 
     * @param targetNames
     * @return xml text containing the Service Assembly infos as map of
     *         [targetName, xmlString]
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> listServiceAssemblies(String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listServiceAssemblies(targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * returns the list of service assembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetNames
     * @return xml string contain the list of service assembly infos as map of
     *         [targetName, xmlString]
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> listServiceAssemblies(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listServiceAssemblies(componentName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param state
     *            to return all the service assemblies that are in the specified
     *            state. JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or
     *            null for ANY state
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetNames
     * @return xml string contain the list of service assembly infos as map of
     *         [targetName, xmlString]
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> listServiceAssemblies(String state,
            String componentName, String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listServiceAssemblies(state, componentName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * return component info xml text that has only service engine infos.
     * 
     * @param targetName
     * @return the component info xml text as map of [targetName,xmlString].
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> listServiceEngines(String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listServiceEngines(targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * return component info xml text that has only service engine infos which
     * satisfies the options passed to the method.
     * 
     * @param state
     *            return all the service engines that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the service engines that have a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return all the service engines that have the specified service
     *            assembly deployed on them. null value to ignore this option.
     * @param targetName
     * @return xml text contain the map of service engine component infos as
     *         [targetName, xmlString]
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> listServiceEngines(String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listServiceEngines(state, sharedLibraryName, serviceAssemblyName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * return component info xml text that has only shared library infos.
     * 
     * @param targetName
     * @return the component info xml text as a map of [targetName, xmlString].
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> listSharedLibraries(String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listSharedLibraries(targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * returns the list of Shared Library infos in the in a xml format
     * 
     * @param componentName
     *            to return only the shared libraries that are this component
     *            dependents. null for listing all the shared libraries in the
     *            schemaorg_apache_xmlbeans.system.
     * @param targetName
     * @return xml string contains the map of componentinfos for shared
     *         libraries as [targetName, xmlString].
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> listSharedLibraries(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listSharedLibraries(componentName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * returns a list of Binding Component and Service Engine infos in xml
     * format, that are dependent upon a specified Shared Library
     * 
     * @param sharedLibraryName
     *            the shared library name
     * @param targetName
     * @return xml string containing the map of componentInfos as [targetName,
     *         xmlString]
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> listSharedLibraryDependents(
            String sharedLibraryName, String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.listSharedLibraryDependents(sharedLibraryName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * return component info xml text for the specified binding component if
     * exists. If no binding component with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the binding component to lookup
     * @param state
     *            return the binding component that is in the specified state.
     *            valid states are JBIComponentInfo.STARTED, STOPPED, INSTALLED
     *            or null for ANY state
     * @param sharedLibraryName
     *            return the binding component that has a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return the binding component that has the specified service
     *            assembly deployed on it. null value to ignore this option.
     * @param targetName
     * @return xml text contain the binding component info that confirms to the
     *         component info list xml grammer as a map of [targetName,
     *         xmlString].
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> showBindingComponent(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.showBindingComponent(name, state, sharedLibraryName, serviceAssemblyName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * return service assembly info xml text for the specified service assembly
     * if exists. If no service assembly with that name exists, it returns the
     * xml with empty list.
     * 
     * @param name
     *            name of the service assembly to lookup
     * @param state
     *            return the service assembly that is in the specified state.
     *            JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or null for
     *            ANY state
     * @param componentName
     *            return the service assembly that has service units on this
     *            component.
     * @param targetNames
     * @return xml string contain service assembly info that confirms to the
     *         service assembly list xml grammer as [targetName, xmlString] map.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> showServiceAssembly(String name, String state,
            String componentName, String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.showServiceAssembly(name, state, componentName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * return component info xml text for the specified service engine if
     * exists. If no service engine with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the service engine to lookup
     * @param state
     *            return service engine that is in the specified state. valid
     *            states are JBIComponentInfo.STARTED, STOPPED, INSTALLED or
     *            null for ANY state
     * @param sharedLibraryName
     *            return service engine that has a dependency on the specified
     *            shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return the service engine that has the specified service
     *            assembly deployed on it. null value to ignore this option.
     * @param targetName
     * @return xml text contain the service engine component info that confirms
     *         to the component info list xml grammer as a map of [targetName,
     *         xmlString].
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> showServiceEngine(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.showServiceEngine(name, state, sharedLibraryName, serviceAssemblyName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * return component info xml text for the specified shared library if
     * exists. If no shared library with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the shared library to lookup
     * @param componentName
     *            return the shared library that is this component dependents.
     *            null to ignore this option.
     * @param targetName
     * @return xml string contain shared library component info that confirms to
     *         the component info list xml grammer as a map of [targetName,
     *         xmlString].
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     */
    public Map<String, String> showSharedLibrary(String name,
            String componentName, String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.showSharedLibrary(name, componentName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        
        return (Map<String, String>) result;
        
    }
    
    /**
     * shuts down component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @return name of the component as [targetName, string] map
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> shutdownComponent(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.shutdownComponent(componentName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * shuts down component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param force
     *            true to force shutdown
     * @param targetNames
     * @return name of the component as [targetName, string] map
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> shutdownComponent(String componentName,
            boolean force, String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.shutdownComponent(componentName, force, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return result as a management message xml text as [targetName, string]
     *         map
     * 
     */
    public Map<String, String> shutdownServiceAssembly(
            String serviceAssemblyName, String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.shutdownServiceAssembly(serviceAssemblyName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceShutdown
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> shutdownServiceAssembly(
            String serviceAssemblyName, boolean forceShutdown,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.shutdownServiceAssembly(serviceAssemblyName, forceShutdown, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
    }
    
    /**
     * starts component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component as [targetName, string] map
     * 
     */
    public Map<String, String> startComponent(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.startComponent(componentName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * starts service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return result as a management message xml text as [targetName, string]
     *         map
     * 
     */
    public Map<String, String> startServiceAssembly(String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.startServiceAssembly(serviceAssemblyName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * stops component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @return name of the component as [targetName, string] map
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> stopComponent(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.stopComponent(componentName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * stops service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @return result as a management message xml text as [targetName, string]
     *         map
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> stopServiceAssembly(String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (String targetName : targetNames) {
            try {
                xmlString = this.stopServiceAssembly(serviceAssemblyName, targetName);
                if (xmlString != null) {
                    result.putIfAbsent(targetName, xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetName, xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    
    /////////////////////////////////////////////
    // End of Cumulative Operation Definitions
    /////////////////////////////////////////////
    
    /**
     * return component info xml text for the specified service engine if
     * exists. If no service engine with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the service engine to lookup
     * @param type
     *            ENGINE or BINDING
     * @param state
     *            to return all the service engines that are in the specified
     *            state. JBIComponentInfo.STARTED, STOPPED, INSTALLED or null
     *            for ANY state
     * @param sharedLibraryName
     *            to return all the service engines that have a dependency on
     *            the specified shared library. Could be null for not filtering
     *            the service engines for this dependency.
     * @param serviceAssemblyName
     *            to return all the service engines that have the specified
     *            service assembly deployed on them. Could be null for not
     *            filtering the service engines for this dependency.
     * @param targetName
     * @return xml string contain service engine component info
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    protected String showComponent(String name, ComponentType type,
            String state, String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        validateUiJBIComponentInfoState(state);
        
        ArrayList uiCompInfoList = new ArrayList();
        
        if (name == null || name.trim().length() <= 0) {
            // return empty list
            return JBIComponentInfo.writeAsXmlText(uiCompInfoList);
        }
        
        ComponentQuery componentQuery = this
                .getFrameworkComponentQuery(targetName);
        ComponentInfo componentInformation = null;
        if (componentQuery != null) {
            componentInformation = componentQuery.getComponentInfo(name);
        }
        
        if (componentInformation == null) {
            // component does not exits
            // return empty list
            return JBIComponentInfo.writeAsXmlText(uiCompInfoList);
        }
        
        ComponentType frameworkCompType = ComponentType.BINDINGS_AND_ENGINES;
        ComponentState frameworkCompState = ComponentState.UNKNOWN;
        
        if (type != ComponentType.ENGINE && type != ComponentType.BINDING) {
            frameworkCompType = ComponentType.BINDINGS_AND_ENGINES;
        } else {
            frameworkCompType = type;
        }
        
        if (state != null) {
            frameworkCompState = toFrameworkComponentInfoState(state);
        }
        
        List compInfoList = this
                .getFrameworkComponentInfoListForEnginesAndBindings(
                        frameworkCompType, frameworkCompState,
                        sharedLibraryName, serviceAssemblyName, targetName);
        
        if (compInfoList == null || compInfoList.size() <= 0) {
            // no components found for the rest of the query
            // return empty list
            return JBIComponentInfo.writeAsXmlText(uiCompInfoList);
        }
        
        // find if the component is in the filtered list or not.
        for (Iterator itr = compInfoList.iterator(); itr.hasNext();) {
            String compName = ((ComponentInfo) itr.next()).getName();
            if (compName.equals(componentInformation.getName())) {
                uiCompInfoList.add(this.toUiComponentInfo(componentInformation,
                        targetName));
                break;
            }
        }
        
        return JBIComponentInfo.writeAsXmlText(uiCompInfoList);
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param force
     * @param targetName
     * @return result as a management message xml text
     * @throws ManagementRemoteException
     *             on error
     */
    protected String shutdownServiceAssemblyInternal(
            String serviceAssemblyName, boolean force, String targetName)
            throws ManagementRemoteException {
        logDebug("Shutting down Service Assembly " + serviceAssemblyName);
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(deploymentServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        logDebug("Calling shutdown on Deployment Service MBean = "
                + deploymentServiceObjectName.toString());
        
        Object resultObject = null;
        
        if (force == false) {
            resultObject = invokeMBeanOperation(deploymentServiceObjectName,
                    "shutDown", serviceAssemblyName);
        } else {
            Object[] params = new Object[2];
            params[0] = serviceAssemblyName;
            params[1] = force;
            
            String[] signature = new String[2];
            signature[0] = "java.lang.String";
            signature[1] = "boolean";
            
            resultObject = invokeMBeanOperation(deploymentServiceObjectName,
                    "shutDown", params, signature);
        }
        
        String result = null;
        if (resultObject != null) {
            result = resultObject.toString();
            if ((serviceAssemblyName == null)
                    || (result.contains("FAILED") == true)
                    || (result.contains("WARNING") == true)) {
                result = resultObject.toString();
            } else {
                result = serviceAssemblyName;
            }
            return result;
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.sasm.shutdown.error", null, null);
            throw new ManagementRemoteException(exception);
        }
    }
    
}
