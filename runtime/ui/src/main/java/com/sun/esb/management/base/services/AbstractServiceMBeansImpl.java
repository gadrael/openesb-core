/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AbstractServiceMBeansImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.management.base.services;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.RuntimeMBeanException;
import javax.management.RuntimeOperationsException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.TargetType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.MBeanNames.ServiceName;
import com.sun.jbi.management.MBeanNames.ServiceType;
import com.sun.jbi.platform.PlatformContext;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.ToolsLogManager;
import com.sun.jbi.ui.common.Util;
import com.sun.jbi.ui.runtime.GenericsSupport;
import com.sun.jbi.util.ComponentConfigurationHelper;

/**
 * @author graj
 * 
 */
public abstract class AbstractServiceMBeansImpl implements Serializable {
    
    static final long                      serialVersionUID                    = -1L;
    
    /** i18n */
    private static I18NBundle              I18NBUNDLE                          = null;
    
    /** Jbi Environment context. */
    protected EnvironmentContext           environmentContext;
    
    /** Component Configuration helper */
    protected ComponentConfigurationHelper componentConfigurationHelper;
    
    protected static final String          DOMAIN_TARGET_KEY                   = "domain";
    
    protected static final String          SERVER_TARGET_KEY                   = "server";
    
    protected static final String          CUSTOM_CONFIGURATION_NAME_KEY       = "Configuration";
    
    protected static final String          CUSTOM_STATISTICS_NAME_KEY          = "Statistics";

    protected static final String          CUSTOM_ADMINISTRATION_NAME_KEY      = "Administration";

    protected static final String          CUSTOM_MANAGEMENT_ACTIONS_NAME_KEY  = "ManagementActions";

    protected static final String          CUSTOM_LOGGER_NAME_KEY              = "Logger";
    
    protected static final String          STARTED_STATE                       = "Started";
    
    protected static final String          RUNNING_STATE                       = "Running";
    
    protected static final String          EQUAL                               = "=";
    
    protected static final String          COLON                               = ":";
    
    protected static final String          COMMA                               = ",";
    
    protected static final char            PASSWORD_MASK_CHARACTER             = '*';
    
    protected static final String          COMPONENT_CONFIG_INSTANCE_ERROR_KEY = "com.sun.jbi.cluster.instance.error";
    
    /** JMX Domain name for the Enterprise Business Integration JMX MBeans */
    protected static final String          JMX_EBI_DOMAIN                      = "com.sun.ebi";
    
    /** Enterprise Business Integration JMX domain */
    protected static String                EBIJMXDOMAIN                        = null;
    
    protected static final String          SERVICE_TYPE_KEY                    = "ServiceType";
    
    protected static final String          IDENTIFICATION_NAME_KEY             = "IdentificationName";
    
    /**
     * 
     */
    public AbstractServiceMBeansImpl(EnvironmentContext anEnvContext) {
        this.environmentContext = anEnvContext;
        this.componentConfigurationHelper = new ComponentConfigurationHelper();
    }
    
    /**
     * logs the message
     * 
     * @param aMsg
     *            message string
     */
    protected static void log(String aMsg) {
        ToolsLogManager.getRuntimeLogger().info(aMsg);
    }
    
    /**
     * logs the message
     * 
     * @param aMsg
     *            message string
     */
    protected static void logDebug(String aMsg) {
        ToolsLogManager.getRuntimeLogger().fine(aMsg);
    }
    
    /**
     * logs the message
     * 
     * @param ex
     *            exception
     */
    protected static void logDebug(Exception ex) {
        ToolsLogManager.getRuntimeLogger()
                .log(Level.FINER, ex.getMessage(), ex);
    }
    
    /**
     * logs errors
     * 
     * @param ex
     *            exception
     */
    protected static void logError(Exception ex) {
        ToolsLogManager.getRuntimeLogger().log(Level.SEVERE, ex.getMessage(),
                ex);
    }
    
    /**
     * logs warnings
     * 
     * @param ex
     *            exception
     */
    public static void logWarning(Exception ex) {
        ToolsLogManager.getRuntimeLogger().log(Level.WARNING, ex.getMessage(),
                ex);
    }
    
    /**
     * @return the platform context
     */
    protected PlatformContext getPlatformContext() {
        PlatformContext platformContext = null;
        EnvironmentContext context = getEnvironmentContext();
        if (context != null) {
            platformContext = context.getPlatformContext();
        }
        return platformContext;
    }
    
    /**
     * @return the EnvironmentContext
     */
    protected static EnvironmentContext getEnvironmentContext() {
        return com.sun.jbi.util.EnvironmentAccess.getContext();
    }
    
    /**
     * gives the I18N bundle
     * 
     * @return I18NBundle object
     */
    protected static I18NBundle getI18NBundle() {
        // lazy initialize the JBI Client
        if (I18NBUNDLE == null) {
            I18NBUNDLE = new I18NBundle("com.sun.jbi.ui.runtime.mbeans");
        }
        return I18NBUNDLE;
    }
    
    /**
     * gives the I18N bundle
     * 
     * @param packageName
     * @return I18NBundle object
     */
    protected static I18NBundle getI18NBundle(String packageName) {
        // lazy initialize the JBI Client
        if (I18NBUNDLE == null) {
            I18NBUNDLE = new I18NBundle(packageName);
        }
        return I18NBUNDLE;
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param targetName
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws ManagementRemoteException
     *             on error
     */
    protected Object getAttributeValue(String targetName,
            ObjectName objectName, String attributeName)
            throws ManagementRemoteException {
        
        if ((DOMAIN_TARGET_KEY.equals(targetName) == true)
                || (SERVER_TARGET_KEY.equals(targetName) == true)) {
            return this.getAttributeValue(objectName, attributeName);
        }
        MBeanServerConnection connection = this
                .getMBeanServerConnection(targetName);
        return this.getAttributeValue(connection, objectName, attributeName);
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param mbeanServer
     *            connection to MBean Server
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws ManagementRemoteException
     *             on error
     */
    protected Object getAttributeValue(MBeanServer mbeanServer,
            ObjectName objectName, String attributeName)
            throws ManagementRemoteException {
        
        Object result = null;
        try {
            result = mbeanServer.getAttribute(objectName, attributeName);
        } catch (AttributeNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (InstanceNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (ReflectionException e) {
            throw new ManagementRemoteException(e);
        } catch (MBeanException mbeanEx) {
            throw ManagementRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        return result;
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param mbeanServer
     *            connection to MBean Server
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws ManagementRemoteException
     *             on error
     */
    protected Object getAttributeValue(MBeanServerConnection mbeanServer,
            ObjectName objectName, String attributeName)
            throws ManagementRemoteException {
        
        Object result = null;
        try {
            result = mbeanServer.getAttribute(objectName, attributeName);
        } catch (AttributeNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (InstanceNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (MBeanException e) {
            throw new ManagementRemoteException(e);
        } catch (ReflectionException e) {
            throw new ManagementRemoteException(e);
        } catch (IOException e) {
            throw new ManagementRemoteException(e);
        }
        return result;
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws ManagementRemoteException
     *             on error
     */
    protected Object getAttributeValue(ObjectName objectName,
            String attributeName) throws ManagementRemoteException {
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        Object result = null;
        try {
            result = mbeanServer.getAttribute(objectName, attributeName);
        } catch (AttributeNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (InstanceNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (ReflectionException e) {
            throw new ManagementRemoteException(e);
        } catch (MBeanException mbeanEx) {
            throw ManagementRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        return result;
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param targetName
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws ManagementRemoteException
     *             on error
     */
    protected void setAttributeValue(String targetName, ObjectName objectName,
            String attributeName, Object attributeValue)
            throws ManagementRemoteException {
        if ((DOMAIN_TARGET_KEY.equals(targetName) == true)
                || (SERVER_TARGET_KEY.equals(targetName) == true)) {
            this.setAttributeValue(objectName, attributeName, attributeValue);
        } else {
            MBeanServerConnection connection = this
                    .getMBeanServerConnection(targetName);
            this.setAttributeValue(connection, objectName, attributeName,
                    attributeValue);
        }
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws ManagementRemoteException
     *             on error
     */
    protected void setAttributeValue(ObjectName objectName,
            String attributeName, Object attributeValue)
            throws ManagementRemoteException {
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        Attribute attribute = null;
        attribute = new Attribute(attributeName, attributeValue);
        
        try {
            mbeanServer.setAttribute(objectName, attribute);
        } catch (InstanceNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (AttributeNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (InvalidAttributeValueException e) {
            throw new ManagementRemoteException(e);
        } catch (ReflectionException e) {
            throw new ManagementRemoteException(e);
        } catch (MBeanException mbeanEx) {
            throw ManagementRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param connection
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws ManagementRemoteException
     *             on error
     */
    protected void setAttributeValue(MBeanServerConnection connection,
            ObjectName objectName, String attributeName, Object attributeValue)
            throws ManagementRemoteException {
        
        Attribute attribute = null;
        attribute = new Attribute(attributeName, attributeValue);
        
        try {
            connection.setAttribute(objectName, attribute);
        } catch (InstanceNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (AttributeNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (InvalidAttributeValueException e) {
            throw new ManagementRemoteException(e);
        } catch (MBeanException e) {
            throw new ManagementRemoteException(e);
        } catch (ReflectionException e) {
            throw new ManagementRemoteException(e);
        } catch (IOException e) {
            throw new ManagementRemoteException(e);
        }
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param params
     *            parameters
     * @param signature
     *            signature of the parameters
     * @return result object
     * @throws ManagementRemoteException
     *             on user error
     */
    protected Object invokeMBeanOperation(ObjectName objectName,
            String operationName, Object[] params, String[] signature)
            throws ManagementRemoteException {
        // TODO: Add error code and message to each exception
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        Object result = null;
        
        try {
            
            result = mbeanServer.invoke(objectName, operationName, params,
                    signature);
            
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (MBeanException mbeanEx) {
            throw ManagementRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        
        return result;
        
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @param connection
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param params
     *            parameters
     * @param signature
     *            signature of the parameters
     * @return result object
     * @throws ManagementRemoteException
     *             on user error
     */
    protected Object invokeMBeanOperation(MBeanServerConnection connection,
            ObjectName objectName, String operationName, Object[] params,
            String[] signature) throws ManagementRemoteException {
        // TODO: Add error code and message to each exception
        Object result = null;
        
        try {
            
            result = connection.invoke(objectName, operationName, params,
                    signature);
            
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (MBeanException mbeanEx) {
            throw ManagementRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (IOException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        
        return result;
        
    }
    
    /**
     * invokes the operation on mbean on appropriate target
     * 
     * @param targetName
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param params
     *            parameters
     * @param signature
     *            signature of the parameters
     * @return result object
     * @throws ManagementRemoteException
     *             on user error
     */
    protected Object invokeMBeanOperation(String targetName,
            ObjectName objectName, String operationName, Object[] params,
            String[] signature) throws ManagementRemoteException {
        if ((DOMAIN_TARGET_KEY.equals(targetName) == true)
                || (SERVER_TARGET_KEY.equals(targetName) == true)) {
            return this.invokeMBeanOperation(objectName, operationName, params,
                    signature);
        }
        MBeanServerConnection connection = this
                .getMBeanServerConnection(targetName);
        return this.invokeMBeanOperation(connection, objectName, operationName,
                params, signature);
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return result object
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param param
     *            operation param of type String
     * @throws ManagementRemoteException
     *             on user error
     */
    protected Object invokeMBeanOperation(ObjectName objectName,
            String operationName, String param)
            throws ManagementRemoteException {
        Object[] params = new Object[1];
        params[0] = param;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        return invokeMBeanOperation(objectName, operationName, params,
                signature);
        
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return result object
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @throws ManagementRemoteException
     *             on user error
     */
    protected Object invokeMBeanOperation(ObjectName objectName,
            String operationName) throws ManagementRemoteException {
        Object[] params = new Object[0];
        
        String[] signature = new String[0];
        return invokeMBeanOperation(objectName, operationName, params,
                signature);
        
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return result object
     * @param attributeName
     *            attribute name
     * @param objectName
     *            object name
     * @throws ManagementRemoteException
     *             on user error
     */
    protected Object getMBeanAttribute(ObjectName objectName,
            String attributeName) throws ManagementRemoteException {
        // TODO: Add error code and message to each exception
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        Object result = null;
        
        try {
            
            result = mbeanServer.getAttribute(objectName, attributeName);
            
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (MBeanException mbeanEx) {
            throw ManagementRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        
        return result;
        
    }
    
    /**
     * Test whether an mbean is registered.
     * 
     * @param objectName
     * @return true when the mbean is registered, false otherwise
     * @throws ManagementRemoteException
     */
    protected boolean isMBeanRegistered(ObjectName objectName)
            throws ManagementRemoteException {
        boolean result = false;
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        try {
            result = mbeanServer.isRegistered(objectName);
        } catch (RuntimeException exception) {
            throw ManagementRemoteException.filterJmxExceptions(exception);
        }
        
        return result;
    }
    
    /**
     * Return the "verb" (the setXXX MBean operation) for the given log level
     * 
     * @param logLevel
     *            log level to set logger to
     * @return a MBean operation to set the logger to that level
     */
    protected String setLevel(Level logLevel) {
        if (null == logLevel) {
            return "setDefault";
        }
        if (Level.OFF.equals(logLevel)) {
            return "setOff";
        }
        if (Level.SEVERE.equals(logLevel)) {
            return "setSevere";
        }
        if (Level.WARNING.equals(logLevel)) {
            return "setWarning";
        }
        if (Level.INFO.equals(logLevel)) {
            return "setInfo";
        }
        if (Level.CONFIG.equals(logLevel)) {
            return "setConfig";
        }
        if (Level.FINE.equals(logLevel)) {
            return "setFine";
        }
        if (Level.FINER.equals(logLevel)) {
            return "setFiner";
        }
        if (Level.FINEST.equals(logLevel)) {
            return "setFinest";
        }
        if (Level.ALL.equals(logLevel)) {
            return "setAll";
        }
        // should never reach this statement
        return "setDefault";
    }
    
    /**
     * Test whether it is a valid target.
     * 
     * @param objectName
     * @return true when the mbean is registered, false otherwise
     * @throws ManagementRemoteException
     */
    protected boolean isValidTarget(ObjectName objectName)
            throws ManagementRemoteException {
        
        boolean result = false;
        MBeanServer mbeanServer = getEnvironmentContext().getMBeanServer();
        try {
            result = mbeanServer.isRegistered(objectName);
        } catch (RuntimeException exception) {
        }
        
        return result;
//        
//        return this.isValidTarget(objectName, DOMAIN_TARGET_KEY);
    }
    
    /**
     * Test whether it is a valid target.
     * 
     * @param objectName
     * @param targetName
     * @return true when the mbean is registered, false otherwise
     * @throws ManagementRemoteException
     */
    protected boolean isValidTarget(ObjectName objectName, String targetName)
            throws ManagementRemoteException {
        boolean result = false;
        if ((targetName.equals(DOMAIN_TARGET_KEY) == true)
                || (targetName.equals(SERVER_TARGET_KEY) == true)) {
            MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
            try {
                result = mbeanServer.isRegistered(objectName);
            } catch (RuntimeException exception) {
                // Ignore Exception
                // Denotes MBean is not registered
            }
        } else {
            MBeanServerConnection connection = this
                    .getMBeanServerConnection(targetName);
            try {
                result = connection.isRegistered(objectName);
            } catch (RuntimeException exception) {
                // Ignore Exception
                // Denotes MBean is not registered
            } catch (IOException exception) {
                // Ignore Exception
                // Denotes MBean is not registered
            }
        }
        
        return result;
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @param attributeValue
     *            attrbute name
     * @param attributeName
     *            attribute value
     * @param objectName
     *            object name
     * @throws ManagementRemoteException
     *             on user error
     */
    protected void setMBeanAttribute(ObjectName objectName,
            String attributeName, Object attributeValue)
            throws ManagementRemoteException {
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        try {
            Attribute attr = new Attribute(attributeName, attributeValue);
            mbeanServer.setAttribute(objectName, attr);
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (MBeanException mbeanEx) {
            throw ManagementRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * set the specified attribute values on the mbean attributes
     * 
     * @param attrList
     *            list of attributes
     * @param objectName
     *            object name
     * @throws ManagementRemoteException
     *             on user error
     */
    protected void setMBeanAttributes(ObjectName objectName,
            AttributeList attrList) throws ManagementRemoteException {
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        try {
            mbeanServer.setAttributes(objectName, attrList);
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
    }
    
    /**
     * set the specified attribute values on the mbean attributes
     * 
     * @param attrList
     *            list of attributes
     * @param objectName
     *            object name
     * @throws ManagementRemoteException
     *             on user error
     */
    protected AttributeList setMBeanAttributes(MBeanServerConnection mbeanServer,
            ObjectName objectName, AttributeList attrList)
            throws ManagementRemoteException {
        
        try {
            return mbeanServer.setAttributes(objectName, attrList);
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return Attribute List of the mbean to set
     * @param objectName
     *            mbean name
     * @param params
     *            a name value pair properties object contains the attribute
     *            name and its value as string.
     * @throws ManagementRemoteException
     *             on user error
     */
    @SuppressWarnings("unchecked")
    protected AttributeList constructMBeanAttributes(ObjectName objectName,
            Properties params) throws ManagementRemoteException {
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        AttributeList attrList = new AttributeList();
        try {
            MBeanInfo mbeanInfo = mbeanServer.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            Map attribInfoMap = new HashMap();
            for (int i = 0; i < mbeanAttrInfoArray.length; ++i) {
                MBeanAttributeInfo attrInfo = mbeanAttrInfoArray[i];
                attribInfoMap.put(attrInfo.getName(), attrInfo);
            }
            
            for (Iterator itr = params.keySet().iterator(); itr.hasNext();) {
                String attrName = (String) itr.next();
                String stringValue = params.getProperty(attrName);
                Object attrValueObj = null;
                
                // We can't validate properties if we don't have MBean attr info
                if (attribInfoMap.isEmpty()) {
                    attrValueObj = stringValue;
                } else {
                    MBeanAttributeInfo attrInfo = (MBeanAttributeInfo) attribInfoMap
                            .get(attrName);
                    if (attrInfo == null) {
                        String[] args = { attrName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.info.not.found",
                                        args, null);
                        throw new ManagementRemoteException(exception);
                        
                    }
                    
                    String type = attrInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attrValueObj = Util.newInstance(type, stringValue);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attrName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                }
                
                Attribute attr = new Attribute(attrName, attrValueObj);
                attrList.add(attr);
            }
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        return attrList;
        
    }

    /**
     * invokes the operation on mbean
     * 
     * @return Attribute List of the mbean to set
     * @param objectName
     *            mbean name
     * @param params
     *            a name value pair Map object contains the attribute
     *            name and its value.
     * @throws ManagementRemoteException
     *             on user error
     */
    @SuppressWarnings("unchecked")
    protected AttributeList constructMBeanAttributes(ObjectName objectName,
            Map<String /*attributeName*/, Object /*attributeValue*/> params) throws ManagementRemoteException {
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        AttributeList attrList = new AttributeList();
        try {
            MBeanInfo mbeanInfo = mbeanServer.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            Map attribInfoMap = new HashMap();
            for (int i = 0; i < mbeanAttrInfoArray.length; ++i) {
                MBeanAttributeInfo attrInfo = mbeanAttrInfoArray[i];
                attribInfoMap.put(attrInfo.getName(), attrInfo);
            }
            
            for (String attrName : params.keySet()) {
                Object attrValueObj = params.get(attrName);
                Attribute attr = new Attribute(attrName, attrValueObj);
                attrList.add(attr);
            }
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        return attrList;
        
    }
    
    
    /**
     * invokes the operation on mbean
     * 
     * @return Attribute List of the mbean to set
     * @param objectName
     *            mbean name
     * @param params
     *            a name value pair properties object contains the attribute
     *            name and its value as string.
     * @throws ManagementRemoteException
     *             on user error
     */
    @SuppressWarnings("unchecked")
    protected AttributeList constructMBeanAttributes(
            MBeanServerConnection mbeanServer, ObjectName objectName,
            Properties params) throws ManagementRemoteException {
        
        AttributeList attrList = new AttributeList();
        try {
            MBeanInfo mbeanInfo = mbeanServer.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            Map attribInfoMap = new HashMap();
            for (int i = 0; i < mbeanAttrInfoArray.length; ++i) {
                MBeanAttributeInfo attrInfo = mbeanAttrInfoArray[i];
                attribInfoMap.put(attrInfo.getName(), attrInfo);
            }
            
            for (Iterator itr = params.keySet().iterator(); itr.hasNext();) {
                String attrName = (String) itr.next();
                String stringValue = params.getProperty(attrName);
                Object attrValueObj = null;
                
                MBeanAttributeInfo attrInfo = (MBeanAttributeInfo) attribInfoMap
                        .get(attrName);
                if (attrInfo == null) {
                    String[] args = { attrName };
                    Exception exception = this
                            .createManagementException(
                                    "ui.mbean.install.config.mbean.attrib.info.not.found",
                                    args, null);
                    throw new ManagementRemoteException(exception);
                    
                }
                
                String type = attrInfo.getType();
                try {
                    // construct the value object using reflection.
                    attrValueObj = Util.newInstance(type, stringValue);
                } catch (Exception ex) {
                    String[] args = { stringValue, type, attrName };
                    Exception exception = this
                            .createManagementException(
                                    "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                    args, ex);
                    throw new ManagementRemoteException(exception);
                    
                }
                
                Attribute attr = new Attribute(attrName, attrValueObj);
                attrList.add(attr);
            }
            
            return attrList;
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * Creates a management message string and populates the exception
     * 
     * @param bundleKey
     * @param args
     *            of Strings
     * @param sourceException -
     *            the source exception to propagate
     * @return Exception object created with a valid XML Management Message
     */
    protected Exception createManagementException(String bundleKey,
            String[] args, Exception sourceException) {
        Exception exception = null;
        String xmlManagementMessage = JBIResultXmlBuilder.createJbiResultXml(
                getI18NBundle(), bundleKey, args, sourceException);
        exception = new Exception(xmlManagementMessage);
        return exception;
    }
    
    /**
     * Creates a management message.
     * 
     * @param taskId
     * @param successResult
     * @param msgType
     * @param msgCode
     * @param args
     * @return
     */
    protected String createManagementMessage(String taskId,
            boolean successResult, String msgType, String msgCode, Object[] args) {
        String msg = getI18NBundle().getMessage(msgCode, args);
        String xmlManagementMessage = JBIResultXmlBuilder.getInstance()
                .createJbiResultXml(taskId, successResult, msgType, msgCode,
                        msg, args);
        
        return xmlManagementMessage;
    }
    
    /**
     * returns the ObjectName for the DeploymentService Mbean of this component.
     * 
     * @param targetName
     * 
     * @return the ObjectName of the DeploymentService MBean or null.
     */
    protected ObjectName getDeploymentServiceMBeanObjectName(String targetName) {
        MBeanNames mbeanNames = this.environmentContext.getMBeanNames();
        
        return mbeanNames.getSystemServiceMBeanName(
                ServiceName.DeploymentService, ServiceType.Deployment,
                targetName);
    }
    
    /**
     * returns the ObjectName for the AdminService Mbean of this component.
     * 
     * @return the ObjectName of the InstallationService MBean or null.
     * @throws JBIRemoteException
     *             on error.
     */
    protected ObjectName getAdminServiceMBeanObjectName()
            throws ManagementRemoteException {
        try {
            MBeanNames mbeanNames = this.environmentContext.getMBeanNames();
            
            return mbeanNames.getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_ADMIN_SERVICE,
                    MBeanNames.CONTROL_TYPE_ADMIN_SERVICE);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * returns the ObjectName for the AdminService facade Mbean for a target
     * 
     * @param targetName
     * 
     * @return the ObjectName of the AdminService MBean or null.
     */
    protected ObjectName getAdminServiceMBeanObjectName(String targetName) {
        MBeanNames mbeanNames = this.environmentContext.getMBeanNames();
        
        return mbeanNames.getSystemServiceMBeanName(
                ServiceName.AdminService, ServiceType.Admin,
                targetName);
    }    
    
    /**
     * returns the ObjectName for the InstallationService Mbean of this
     * component.
     * 
     * @param targetName
     * @return the ObjectName of the InstallationService MBean or null.
     */
    protected ObjectName getInstallationServiceMBeanObjectName(String targetName) {
        MBeanNames mbeanNames = this.environmentContext.getMBeanNames();
        return mbeanNames.getSystemServiceMBeanName(
                ServiceName.InstallationService, ServiceType.Installation,
                targetName);
    }
    
    /**
     * Check if a target is valid or not
     * 
     * @param objectName
     * @param targetName
     * @throws ManagementRemoteException
     */
    protected void checkForValidTarget(ObjectName objectName, String targetName)
            throws ManagementRemoteException {
        boolean isRegistered = this.isValidTarget(objectName);
        if (isRegistered == false) {
            String[] args = { targetName };
            Exception exception = this.createManagementException(
                    "ui.mbean.invalid.target.error", args, null);
            throw new ManagementRemoteException(exception);
            
        }
    }
    
    /**
     * Check if a target is valid or not
     * 
     * @param objectName
     * @param targetName
     * @param useTargetNameFlag
     * @throws ManagementRemoteException
     */
    protected void checkForValidTarget(ObjectName objectName,
            String targetName, boolean useTargetNameFlag)
            throws ManagementRemoteException {
        boolean isRegistered = false;
        if (useTargetNameFlag == true) {
            isRegistered = this.isValidTarget(objectName, targetName);
        } else {
            isRegistered = this.isValidTarget(objectName);
        }
        if (isRegistered == false) {
            String[] args = { targetName };
            Exception exception = this.createManagementException(
                    "ui.mbean.invalid.target.error", args, null);
            throw new ManagementRemoteException(exception);
            
        }
    }
    
    /**
     * Check the type of the specified target
     * 
     * @param targetName
     * @return an enum representing the type of the target.
     */
    protected TargetType checkTargetType(String target) {
        if (true == this.getPlatformContext().isStandaloneServer(target)) {
            return TargetType.STANDALONE_SERVER;
        }
        
        if (true == this.getPlatformContext().isCluster(target)) {
            return TargetType.CLUSTER;
        }
        
        if (true == this.getPlatformContext().isClusteredServer(target)) {
            return TargetType.CLUSTERED_SERVER;
        }
        
        if (true == "domain".equals(target)) {
            return TargetType.DOMAIN;
        }
        
        return TargetType.INVALID_TARGET;
    }
    
    /**
     * Retrieve MBeanServerConnection for targets other than server
     * 
     * @param targetName
     * @return MBeanServerConnection
     * @throws ManagementRemoteException
     */
    protected MBeanServerConnection getMBeanServerConnection(String targetName)
            throws ManagementRemoteException {
        MBeanServerConnection connection = null;
        try {
            connection = getPlatformContext().getMBeanServerConnection(
                    targetName);
        } catch (Exception e) {
            throw new ManagementRemoteException(e);
        }
        return connection;
    }
    
    /**
     * gets the ebi jmx domain name
     * 
     * @return domain name
     */
    protected static String getEbiJmxDomain() {
        if (EBIJMXDOMAIN == null) {
            EBIJMXDOMAIN = System.getProperty("ebi.jmx.domain", JMX_EBI_DOMAIN);
        }
        return EBIJMXDOMAIN;
    }
    
    /**
     * Get the EBI Status MBean ObjectName
     * 
     * @param componentName
     * @param targetName
     * @return object name of ebi config MBean
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    protected ObjectName getEbiStatusMBeanObjectName(String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName objectName = null;
        Set<ObjectName> objectNameSet = null;
        String name = null;
        if ((targetName.equals(DOMAIN_TARGET_KEY) == true)
                || (targetName.equals(SERVER_TARGET_KEY) == true)) {
            MBeanServer server = this.environmentContext.getMBeanServer();
            
            name = getEbiJmxDomain() + ":" + SERVICE_TYPE_KEY + "=Status,"
                    + IDENTIFICATION_NAME_KEY + "=" + componentName + ",*";
            try {
                ObjectName objectNameFilter = new ObjectName(name);
                if (server != null) {
                    objectNameSet = server.queryNames(objectNameFilter, null);
                    for (ObjectName mBeanObjectName : objectNameSet) {
                        if (mBeanObjectName != null) {
                            objectName = mBeanObjectName;
                            break;
                        }
                    }
                }
            } catch (MalformedObjectNameException e) {
                throw new ManagementRemoteException(e);
            } catch (NullPointerException e) {
                throw new ManagementRemoteException(e);
            }
        } else {
            MBeanServerConnection connection = this
                    .getMBeanServerConnection(targetName);
            name = getEbiJmxDomain() + ":" + SERVICE_TYPE_KEY + "=Status,"
                    + IDENTIFICATION_NAME_KEY + "=" + componentName + ",*";
            try {
                ObjectName objectNameFilter = new ObjectName(name);
                if (connection != null) {
                    objectNameSet = connection.queryNames(objectNameFilter,
                            null);
                    for (ObjectName mBeanObjectName : objectNameSet) {
                        if (mBeanObjectName != null) {
                            objectName = mBeanObjectName;
                            break;
                        }
                    }
                }
            } catch (MalformedObjectNameException e) {
                throw new ManagementRemoteException(e);
            } catch (NullPointerException e) {
                throw new ManagementRemoteException(e);
            } catch (IOException e) {
                throw new ManagementRemoteException(e);
            }
        }
        return objectName;
    }
    
    /**
     * Get the attributes on the MBean
     * 
     * @param connection
     * @param objectName
     * @return
     * @throws ManagementRemoteException
     */
    protected Properties getMBeanAttributeValues(
            MBeanServerConnection connection, ObjectName objectName)
            throws ManagementRemoteException {
        Properties properties = new Properties();
        List<String> keyList = new ArrayList<String>();
        MBeanInfo mbeanInfo = null;
        try {
            mbeanInfo = connection.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            for (MBeanAttributeInfo attributeInfo : mbeanAttrInfoArray) {
                String key = attributeInfo.getName();
                if (key != null) {
                    Object value = null;
                    try {
                        value = connection.getAttribute(objectName, key);
                    } catch (AttributeNotFoundException e) {
                        throw new ManagementRemoteException(e);
                    } catch (InstanceNotFoundException e) {
                        throw new ManagementRemoteException(e);
                    } catch (MBeanException e) {
                        throw new ManagementRemoteException(e);
                    } catch (ReflectionException e) {
                        throw new ManagementRemoteException(e);
                    }
                    if (value != null) {
                        // Use getApplicationVariables to get application
                        // variable values
                        if (key.equals("ApplicationVariables")) {
                            // value = this
                            //     .convertToEnvironmentVariableString(value);
                            continue;
                        }
                    
                        // Use getApplicationConfigurations to get application
                        //             configuration values
                        if (key.equals("ApplicationConfigurations")) {
                            //value = this
                            //         .convertToEnvironmentVariableString(value);
                            continue;
                        }
                        properties.put(key, value + "");
                        keyList.add(key);
                    } else {
                        properties.put(key, "");
                        keyList.add(key);
                    }
                }
            }
        } catch (InstanceNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (IntrospectionException e) {
            throw new ManagementRemoteException(e);
        } catch (ReflectionException e) {
            throw new ManagementRemoteException(e);
        } catch (IOException e) {
            throw new ManagementRemoteException(e);
        }
        
        // Mask Sensitive password Strings
        /** TODO : With the facade layer we do not have the display data XML
         * When the common client is getting the attributes from the actual 
         * component MBean this path should be invoked
        if (keyList.size() > 0) {
            String xmlDataString = this.getComponentConfigurationDisplayData(
                    connection, objectName);
            keyList = this.getComponentConfigurationPasswordFields(
                    xmlDataString, GenericsSupport.toArray(keyList,
                            String.class));
            for (String key : keyList) {
                String sensitiveString = properties.getProperty(key);
                sensitiveString = this
                        .maskSensitiveString(sensitiveString, PASSWORD_MASK_CHARACTER);
                properties.setProperty(key, sensitiveString);
            }
        }
         */
        
        return properties;
    }

    /**
     * Get the attributes on the MBean
     * 
     * @param connection
     * @param objectName
     * @return
     * @throws ManagementRemoteException
     */
    protected Map<String /*attributeName*/, Object /*attributeValue*/> getMBeanAttributeValuesAsMap(
            MBeanServerConnection connection, ObjectName objectName)
            throws ManagementRemoteException {
        Map<String /*attributeName*/, Object /*attributeValue*/> properties = null;
        properties = new HashMap<String /*attributeName*/, Object /*attributeValue*/>();
        List<String> keyList = new ArrayList<String>();
        MBeanInfo mbeanInfo = null;
        try {
            mbeanInfo = connection.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            for (MBeanAttributeInfo attributeInfo : mbeanAttrInfoArray) {
                String key = attributeInfo.getName();
                if (key != null) {
                    Object value = null;
                    try {
                        value = connection.getAttribute(objectName, key);
                    } catch (AttributeNotFoundException e) {
                        throw new ManagementRemoteException(e);
                    } catch (InstanceNotFoundException e) {
                        throw new ManagementRemoteException(e);
                    } catch (MBeanException e) {
                        throw new ManagementRemoteException(e);
                    } catch (ReflectionException e) {
                        throw new ManagementRemoteException(e);
                    }
                    if (value != null) {
                        // Use getApplicationVariables to get application
                        // variable values
                        if (key.equals("ApplicationVariables")) {
                            // value = this
                            //     .convertToEnvironmentVariableString(value);
                            continue;
                        }
                    
                    // Use getApplicationConfigurations to get application
                    //             configuration values
                    if (key.equals("ApplicationConfigurations")) {
                        //value = this
                        //         .convertToEnvironmentVariableString(value);
                        continue;
                    }
                        properties.put(key, value);
                        keyList.add(key);
                    }
                }
            }
        } catch (InstanceNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (IntrospectionException e) {
            throw new ManagementRemoteException(e);
        } catch (ReflectionException e) {
            throw new ManagementRemoteException(e);
        } catch (IOException e) {
            throw new ManagementRemoteException(e);
        }
        
        // Mask Sensitive password Strings
        /** TODO : With the facade layer we do not have the display data XML
         * When the common client is getting the attributes from the actual 
         * component MBean this path should be invoked
        if (keyList.size() > 0) {
            String xmlDataString = this.getComponentConfigurationDisplayData(
                    connection, objectName);
            keyList = this.getComponentConfigurationPasswordFields(
                    xmlDataString, GenericsSupport.toArray(keyList,
                            String.class));
            for (String key : keyList) {
                String sensitiveString = properties.getProperty(key);
                sensitiveString = this
                        .maskSensitiveString(sensitiveString, PASSWORD_MASK_CHARACTER);
                properties.setProperty(key, sensitiveString);
            }
        }
         */
        
        return properties;
    }
    
    /**
     * set the specified attribute values on the mbean attributes
     * 
     * @param attrList
     *            list of attributes
     * @param objectName
     *            object name
     * @return management message response from setConfigurationAttributes
     * @throws ManagementRemoteException
     *             on user error
     */
    protected String setMBeanConfigAttributes(ObjectName objectName,
            AttributeList attrList) throws ManagementRemoteException {
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        return this.setMBeanConfigAttributes(mbeanServer, objectName, attrList);
    }
    
    /**
     * set the specified attribute values on the mbean attributes
     * 
     * @param MBeanServer
     *            the MBean server to use
     * @param attrList
     *            list of attributes
     * @param objectName
     *            object name
     * @return management message response from setConfigurationAttributes
     * @throws ManagementRemoteException
     *             on user error
     */
    protected String setMBeanConfigAttributes(
            MBeanServerConnection mbeanServer, ObjectName objectName,
            AttributeList attrList) throws ManagementRemoteException {
        try {
            return ((String) mbeanServer.invoke(objectName,
                    "setConfigurationAttributes", new Object[] { attrList },
                    new String[] { "javax.management.AttributeList" }));
        } catch (InstanceNotFoundException notFoundEx) {
            throw new ManagementRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new ManagementRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw ManagementRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw ManagementRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * Returns the stack-trace of the exception
     * 
     * @param exception
     * @return stack traces as a string
     */
    protected String getStackTrace(JBIRemoteException exception) {
        String buffer = "";
        if (exception != null) {
            JBIManagementMessage message = exception
                    .extractJBIManagementMessage();
            if (message != null) {
                buffer = message.getStackTrace();
            }
            if ((buffer == null) || (buffer.trim().length() == 0)) {
                buffer = "";
                StringBuffer cause = exception.getCauseStackTrace();
                if (cause != null) {
                    buffer += getI18NBundle().getMessage(
                            "ui.mbean.stacktrace.caused.by.info")
                            + ":\n" + cause.toString();
                    buffer += "\n";
                } else {
                    String[] elements = exception.getCauseMessageTrace();
                    if (elements != null) {
                        buffer += getI18NBundle().getMessage(
                                "ui.mbean.stacktrace.caused.by.info")
                                + ":\n";
                        for (String element : elements) {
                            if (element != null) {
                                buffer += element.toString();
                                buffer += "\n";
                            }
                        }
                    }
                }
                StackTraceElement[] elements = exception.getStackTrace();
                if (elements != null) {
                    buffer += getI18NBundle().getMessage(
                            "ui.mbean.stacktrace.stack.trace.info")
                            + ":\n";
                    for (StackTraceElement element : elements) {
                        if (element != null) {
                            buffer += element.toString();
                            buffer += "\n";
                        }
                    }
                }
            }
        }
        return buffer;
    }
    
    /**
     * Check to ensure that the target is a domain target
     * @throws ManagementRemoteException if target is domain
     */
    protected void domainTargetCheck(String target)
            throws ManagementRemoteException {
        if ("domain".equals(target)) {
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.component.configuration.domain.unsupported.error",
                            new String[0], null);
            throw new ManagementRemoteException(exception);
        }
    }
    
    /**
     * Returns a map of target names to an array of target instance names.
     * In the case cluster targets, the key contains the cluster target name, 
     * and the the value contains an array of the "target instance" names.
     * If it is not a cluster target, the key contains the targetName, and 
     * the value is null.
     * 
     * @return map of target names to array of target instance names
     * @throws ManagementRemoteException
     */
    protected Map<String /*targetName*/, String[] /*targetInstanceNames*/> listTargetNames() throws ManagementRemoteException {
        Map<String /*targetName*/, String[] /*targetInstanceNames*/> targetNameToTargetInstanceNameMap = null;
        targetNameToTargetInstanceNameMap = new HashMap<String /*targetName*/, String[] /*targetInstanceNames*/>();
        Set<String> clusters = this.getPlatformContext().getClusterNames();
        Set<String> servers = this.getPlatformContext().getStandaloneServerNames();
        
        // Add standalone targets
        for(String serverTargetName : servers) {
            targetNameToTargetInstanceNameMap.put(serverTargetName, null);
        }
        
        // Add cluster targets
        for(String clusterTargetName : clusters) {
            Set<String> serversInCluster = this.getPlatformContext().getServersInCluster(clusterTargetName);
            List<String> serverNamesList = new ArrayList<String>();
            if(serversInCluster != null) {
                for(String instanceName : serversInCluster) {
                    serverNamesList.add(instanceName);
                }
            }
            String[] serverNamesArray = GenericsSupport.toArray(serverNamesList, String.class);
            targetNameToTargetInstanceNameMap.put(clusterTargetName, serverNamesArray);
        }
        
        
        return targetNameToTargetInstanceNameMap;
    }
    
    
    /**
     * Checks to see if the Target (server, cluster) is up or down.
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return true if Target is up, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    protected boolean isTargetUp(String targetName)
            throws ManagementRemoteException {
        boolean isTargetUp = false;
        Set<String> clusters = this.getPlatformContext().getClusterNames();
        Set<String> servers = this.getPlatformContext()
                .getStandaloneServerNames();
        
        if (clusters.contains(targetName)) {
            Set<String> serversInCluster = this.getPlatformContext()
                    .getServersInCluster(targetName);
            
            for (String instanceName : serversInCluster) {
                if (this.getPlatformContext().isInstanceUp(instanceName)) {
                    isTargetUp = true;
                    break;
                }
            }
        } else if (servers.contains(targetName)) {
            isTargetUp = this.getPlatformContext().isInstanceUp(targetName);
        }
        
        return isTargetUp;
    }
    
    /**
     * Find Extension MBeans for targets that are up
     * 
     * @param componentName
     * @param extensionName
     * @param targetName
     * @return map of instanceName to ObjectNames[] the contain live instances
     * @throws ManagementRemoteException
     */
    protected Map<String /*instanceName*/, ObjectName[]> findLiveExtensionMBeanObjectNames(String componentName, String extensionName, String targetName) throws ManagementRemoteException {
        Map<String /*instanceName*/, ObjectName[]> resultMap = new HashMap<String /*instanceName*/, ObjectName[]>();
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        Map<String /*targetName*/, String[] /*instanceNames*/> targetToInstanceNamesMap = null;
        targetToInstanceNamesMap = listTargetNames();
        targetToObjectNamesMap = getComponentExtensionMBeanObjectNames(componentName, extensionName, targetName);
        if((targetToInstanceNamesMap != null) && (targetToObjectNamesMap != null)) {
            String[] targetInstanceNames = targetToInstanceNamesMap.get(targetName);
            if(targetInstanceNames != null) {
                // This is a Cluster target
                for(String instanceName : targetInstanceNames) {
                    if((this.isPlatformContextInstanceUp(instanceName) == true) 
                            && (this.isPlatformContextInstanceClustered(instanceName) == true)) {
                        // the instance is live, and it is a clustered instance
                        ObjectName[] objectName = targetToObjectNamesMap.get(targetName);
                        resultMap.put(instanceName, objectName);
                    }
                }
            } else {
                // This is a NOT cluster target
                if(this.isTargetUp(targetName) 
                        && (this.isPlatformContextClusteredServer(targetName) == false)) {
                    // The target is Live, and it is a stand-alone server
                    ObjectName[] objectName = targetToObjectNamesMap.get(targetName);
                    resultMap.put(targetName, objectName);
                }
            }
        }
        return resultMap;
    }

    /**
     * Gets the extension MBean object names
     * 
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    protected Map<String, ObjectName[]> getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName)
            throws ManagementRemoteException {
        Map<String, ObjectName[]> resultObject = null;
        logDebug("Get Component Extension MBeans ");
        
        ObjectName extensionMBeanObjectName = null;
        try {
            extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                    componentName, JBIAdminCommands.DOMAIN_TARGET_KEY);
        } catch (ManagementRemoteException exception) {
        }
        
        if (extensionMBeanObjectName == null) {
            return resultObject;
        }
        
        this.checkForValidTarget(extensionMBeanObjectName,
                JBIAdminCommands.DOMAIN_TARGET_KEY);
        
        logDebug("Calling getCustomMBeanNames on extensionMBeanObjectName = "
                + extensionMBeanObjectName);
        
        // return custom MBeans with CustomControlName=customName and
        // ControlType=Custom
        resultObject = (Map<String, ObjectName[]>) this.invokeMBeanOperation(
                extensionMBeanObjectName, "getCustomMBeanNames", extensionName);
        
        return resultObject;
    }
    
    /**
     * Gets the extension MBean object names
     * 
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return an array of ObjectName(s)
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    protected ObjectName[] getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName,
            String targetInstanceName) throws ManagementRemoteException {
        ObjectName[] resultObject = null;
        Map<String, ObjectName[]> customMBeansMap = null;
        logDebug("Get Component Extension MBeans ");
        
        ObjectName extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                componentName, JBIAdminCommands.DOMAIN_TARGET_KEY);
        if (extensionMBeanObjectName == null) {
            return resultObject;
        }
        
        this.checkForValidTarget(extensionMBeanObjectName,
                JBIAdminCommands.DOMAIN_TARGET_KEY);
        
        logDebug("Calling getCustomMBeanNames on extensionMBeanObjectName = "
                + extensionMBeanObjectName);
        
        // return custom MBeans with CustomControlName=customName and
        // ControlType=Custom
        customMBeansMap = (Map<String, ObjectName[]>) this
                .invokeMBeanOperation(extensionMBeanObjectName,
                        "getCustomMBeanNames", extensionName);
        if ((customMBeansMap != null)
                && ((targetName != null) || (targetInstanceName != null))) {
            // You now have the map of targetInstances to extension MBean names
            if (targetInstanceName != null) {
                resultObject = customMBeansMap.get(targetInstanceName);
            } else {
                resultObject = customMBeansMap.get(targetName);
            }
        }
        return resultObject;
    }
    
    
    /**
     * returns the ObjectName for the Extension Mbean of this component.
     * 
     * @param componentName
     * @param targetName
     * 
     * @return the ObjectName of the Extension MBean or null.
     */
    @SuppressWarnings("unchecked")
    protected ObjectName getExtensionMBeanObjectName(String componentName,
            String targetName) throws ManagementRemoteException {
        
        ObjectName adminSvcMBeanObjectName 
                    = this.getAdminServiceMBeanObjectName(targetName);
        
        this.checkForValidTarget(adminSvcMBeanObjectName,
                targetName);
        
        logDebug("Calling getComponentExtensionFacadeMBean on AdminService = "
                + adminSvcMBeanObjectName);
        
        ObjectName extensionMBeanObjectName = (ObjectName) this.invokeMBeanOperation(
                adminSvcMBeanObjectName, "getComponentExtensionFacadeMBean", componentName);
        
        logDebug("getComponentExtensionFacadeMBean on AdminService returned = "
                + extensionMBeanObjectName);
        
        return extensionMBeanObjectName;
    }
    

    /////////////////////////////
    // Platform Context methods
    /////////////////////////////

    /**
     * Get the instance name of the platform's administration server.  If the
     * platform does not provide a separate administration server, then this 
     * method returns the name of the local instance.
     * @return instance name of the administration server
     */
    protected String getPlatformContextAdminServerName() {
        return this.getPlatformContext().getAdminServerName();
    }
    
    /**
     * Determine whether this instance is the administration server instance.
     * @return <CODE>true</CODE> if this instance is the administration server,
     * <CODE>false</CODE> if not.
     */
    protected boolean isPlatformContextAdminServer() {
        return this.getPlatformContext().isAdminServer();
    }
    
    /**
     * Get the name of this instance.
     * @return the name of this server instance.
     */
    protected String getPlatformContextInstanceName() {
        return this.getPlatformContext().getInstanceName();
    }
        
    /**
     * Determine if the specified instance is up.
     * @return true if the instance is up and running, false otherwise
     */
    protected boolean isPlatformContextInstanceUp(String instanceName) {
        return this.getPlatformContext().isInstanceUp(instanceName);
    }
    
    /**
     * Determine whether multiple servers are permitted within this AS
     * installation.
     * @return true if multiple servers are permitted.
     */
    protected boolean platformContextSupportsMultipleServers() {
        return this.getPlatformContext().supportsMultipleServers();
    }
    
    /**
     * Get the Target Name. If the instance is not a clustered instance then
     * the target name is the instance name. If the instance is part of a
     * cluster then the target name is the cluster name.
     *
     * @return the target name. 
     */
    protected String getPlatformContextTargetName() {
        return this.getPlatformContext().getTargetName();
    }

    /**
     * Get the Target Name for a specified instance. If the instance is not
     * clustered the instance name is returned. This operation is invoked by
     * the JBI instance MBeans only.
     *
     * @return the target name. 
     */
    protected String getPlatformContextTargetName(String instanceName) {
        return this.getPlatformContext().getTargetName(instanceName);
    }
    
    /**
     * Get a set of the names of all the standalone servers in the domain.
     * @return a set of names of standalone servers in the domain.
     */
    protected Set<String> getPlatformContextStandaloneServerNames() {
        return this.getPlatformContext().getStandaloneServerNames();
    }
    
    /**
     * Get a set of the names of all the clustered servers in the domain.
     * @return a set of names of clustered servers in the domain.
     */
    protected Set<String> getPlatformContextClusteredServerNames() {
        return this.getPlatformContext().getClusteredServerNames();
    }

    /**
     * Get a set of the names of all the clusters in the domain.
     * @return a set of names of clusters in the domain.
     */
    protected Set<String> getPlatformContextClusterNames() {
        return this.getPlatformContext().getClusterNames();
    }
    
    /**
     * Get a set of the names of all the servers in the specified cluster.
     * @return a set of names of servers in the cluster.
     */
    protected Set<String> getPlatformContextServersInCluster(String clusterName) {
        return this.getPlatformContext().getServersInCluster(clusterName);
    }
    
    /**
     * Determine whether a target is a valid server or cluster name.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a valid
     * standalone server name or cluster name, <CODE>false</CODE> if not.
     */
    protected boolean isPlatformContextValidTarget(String targetName) {
        return this.getPlatformContext().isValidTarget(targetName);
    }
    
    /**
     * Determine whether a target is a cluster.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a cluster,
     * <CODE>false</CODE> if not.
     */
    protected boolean isPlatformContextCluster(String targetName) {
        return this.getPlatformContext().isCluster(targetName);
    }
    
    /**
     * Determine whether a target is a standalone server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a standalone
     * server, <CODE>false</CODE> if not.
     */
    protected boolean isPlatformContextStandaloneServer(String targetName) {
        return this.getPlatformContext().isStandaloneServer(targetName);
    }
    
    /**
     * Determine whether the target is a clustered server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a clustered
     * server, <CODE>false</CODE> if not.
     */
    protected boolean isPlatformContextClusteredServer(String targetName) {
        return this.getPlatformContext().isClusteredServer(targetName);
    }
    
    /**
     * Determine whether or not an instance is clustered.
     * @return <CODE>true</CODE> if the instance is clustered,
     * <CODE>false</CODE> if not.
     */
    protected boolean isPlatformContextInstanceClustered(String instanceName) {
        return this.getPlatformContext().isInstanceClustered(instanceName);
    }

    /**
     * Get a string representation of the DAS JMX RMI connector port.
     * @return the JMX RMI connector port as a (CODE>String</CODE>.
     */
    protected String getPlatformContextJmxRmiPort() {
        return this.getPlatformContext().getJmxRmiPort();
    }
    
    /**
     * Provides access to the platform's MBean server.
     * @return platform MBean server.
     */
    protected MBeanServer getPlatformContextMBeanServer() {
        return this.getPlatformContext().getMBeanServer();
    }
    
    /**
     * Get the full path to the platform's instance root directory.
     * @return platform instance root
     */
    protected String getPlatformContextInstanceRoot() {
        return this.getPlatformContext().getInstanceRoot();
    }
    
    /**
     * Get the full path to the platform's instaall root directory.
     * @return platform install root
     */
    protected String getPlatformContextInstallRoot() {
        return this.getPlatformContext().getInstallRoot();
    }
}
