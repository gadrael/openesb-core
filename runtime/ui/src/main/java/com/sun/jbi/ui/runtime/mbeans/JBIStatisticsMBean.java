/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIStatisticsMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.mbeans;

import javax.management.openmbean.TabularData;
import com.sun.jbi.ui.common.JBIRemoteException;

/**
 * This class is used to provide JBI statistics information to clients.
 * @author Sun Microsystems, Inc.
 */
   
public interface JBIStatisticsMBean
{    
    
    /**
     * This method is used to provide JBIFramework statistics in the
     * given target.
     * @param target target name.
     * @return TabularData table of framework statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String          - "InstanceName",
     *  Long            - "StartupTime",
     *  Long            - "UpTime"
     */
    public TabularData getFrameworkStats(String targetName)
    throws JBIRemoteException;
    
   /**
     * This method is used to provide a list of consuming endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of consuming endpoints 
     * @throws JBIRemoteException if the list of endpoints could not be obtained.    
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "Endpoints",
     */
    public TabularData getConsumingEndpointsForComponent(String componentName, String targetName)
    throws JBIRemoteException;

    
    /**
     * This method is used to provide a list of provisioning endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of provisioning endpoints 
     * @throws JBIRemoteException if the list of endpoints could not be obtained.
     *     
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "Endpoints",
     */
    public TabularData getProvidingEndpointsForComponent(String componentName, String targetName)
    throws JBIRemoteException; 
    
    /**
     * This method is used to provide statistics for the given component
     * in the given target
     * @param targetName target name
     * @param componentName component name
     * @return TabularData table of component statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     *
     * SimpleType.STRING   -   "InstanceName",
     * SimpleType.LONG     -   "ComponentUpTime",
     * SimpleType.LONG     -   "NumActivatedEndpoints",
     * SimpleType.LONG     -   "NumReceivedRequests",
     * SimpleType.LONG     -   "NumSentRequests",
     * SimpleType.LONG     -   "NumReceivedReplies",
     * SimpleType.LONG     -   "NumSentReplies",
     * SimpleType.LONG     -   "NumReceivedDONEs",
     * SimpleType.LONG     -   "NumSentDONEs",
     * SimpleType.LONG     -   "NumReceivedFaults",
     * SimpleType.LONG     -   "NumSentFaults",
     * SimpleType.LONG     -   "NumReceivedErrors",
     * SimpleType.LONG     -   "NumSentErrors",
     * SimpleType.LONG     -   "NumCompletedExchanges",
     * SimpleType.LONG     -   "NumActiveExchanges",
     * SimpleType.LONG     -   "NumErrorExchanges",
     * SimpleType.DOUBLE   -   "ME-ResponseTime-Avg", //time in ns
     * SimpleType.DOUBLE   -   "ME-ComponentTime-Avg", //time in ns
     * SimpleType.DOUBLE   -   "ME-DeliveryChannelTime-Avg", //time in ns
     * SimpleType.DOUBLE   -   "ME-MessageServiceTime-Avg", //time in ns                      
     * CompositeType       -   "ComponentExtensionStats"
     *
     */
    public TabularData getComponentStats(String componentName, String targetName)
    throws JBIRemoteException;
    
                
    /**
     * This method is used to provide statistic information about the given 
     * endpoint in the given target
     * @param targetName target name
     * @param endpointName the endpoint Name
     * @return TabularData table of endpoint statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     *
     * A providing endpoint will have
     *
     * SimpleType.STRING  - "InstanceName"
     * SimpleType.LONG    - "ActivationTime"
     * SimpleType.LONG    - "UpTime"
     * SimpleType.LONG    - "NumActiveExchanges"
     * SimpleType.LONG    - "NumReceivedRequests"
     * SimpleType.LONG    - "NumSentReplies"
     * SimpleType.LONG    - "NumReceivedDONEs"
     * SimpleType.LONG    - "NumSentDONEs"
     * SimpleType.LONG    - "NumReceivedFaults"
     * SimpleType.LONG    - "NumSentFaults"
     * SimpleType.LONG    - "NumReceivedErrors"
     * SimpleType.LONG    - "NumSentErrors"
     * SimpleType.STRING  - "ComponentName"
     * SimpleType.LONG    - "ME-ResponseTime-Avg" //time in ns
     * SimpleType.LONG    - "ME-ComponentTime-Avg" //time in ns
     * SimpleType.LONG    - "ME-DeliveryChannelTime-Avg" //time in ns
     * SimpleType.LONG    - "ME-MessageServiceTime-Avg" //time in ns
     * TabularType        - "PerformanceMeasurements" //OJC Components only
     *
     * A consuming endpoint will have
     *
     * SimpleType.STRING  - "InstanceName"
     * SimpleType.LONG    - "NumSentRequests"
     * SimpleType.LONG    - "NumReceivedReplies"
     * SimpleType.LONG    - "NumReceivedDONEs"
     * SimpleType.LONG    - "NumSentDONEs"
     * SimpleType.LONG    - "NumReceivedFaults"
     * SimpleType.LONG    - "NumSentFaults"
     * SimpleType.LONG    - "NumReceivedErrors"
     * SimpleType.LONG    - "NumSentErrors"
     * SimpleType.LONG    - "ME-StatusTime-Avg" //time in ns
     * SimpleType.LONG    - "ME-ComponentTime-Avg" //time in ns
     * SimpleType.LONG    - "ME-DeliveryChannelTime-Avg" //time in ns
     * SimpleType.LONG    - "ME-MessageServiceTime-Avg" //time in ns
     * TabularType        - "PerformanceMeasurements" //OJC Components only
     */
    public TabularData getEndpointStats(String endpointName, String targetName)
    throws JBIRemoteException;
    
    /**
     * This method is used to provide statistics about the message service in the
     * given target.
     * @param target target name.
     * @return TabularData table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "ListActiveChannels",
     *  String[]          - "ListActiveEndpoints"
     */
    public TabularData getNMRStats(String targetName)
    throws JBIRemoteException;
    
    
    /**
     * This method is used to provide statistics about a Service Assembly
     * in the given target.
     * @param target target name.
     * @param saName the service assembly name.
     * @return TabularData table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "SAName",
     *  Long              - "SAStartTime",
     *  Long              - "SAStopTime",
     *  Long              - "SAShutdownTime",
     *  CompositeData[]   - "SUTimes"
     *
     * Each entry in SUTimes would be of the following type
     *
     *  String           - "SUName"
     *  Long             - "SUStartTime"
     *  Long             - "SUStopTime"
     *  Long             - "SUShutdownTime"
     *  String[]         - "ListEndpoints"
     */
    public TabularData getServiceAssemblyStats(String saName, String targetName)
    throws JBIRemoteException;


}
