/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AbstractUIMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.mbeans;

import java.io.IOException;
import java.io.StringReader;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.jbi.management.DeploymentServiceMBean;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.Descriptor;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.RuntimeMBeanException;
import javax.management.RuntimeOperationsException;
import javax.management.modelmbean.ModelMBeanAttributeInfo;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.xml.parsers.ParserConfigurationException;


import org.xml.sax.SAXException;

import com.sun.esb.management.api.administration.AdministrationService;
import com.sun.esb.management.api.configuration.ConfigurationService;
import com.sun.esb.management.api.deployment.DeploymentService;
import com.sun.esb.management.api.installation.InstallationService;
import com.sun.esb.management.api.performance.PerformanceMeasurementService;
import com.sun.esb.management.api.runtime.RuntimeManagementService;
import com.sun.esb.management.impl.administration.AdministrationServiceImpl;
import com.sun.esb.management.impl.configuration.ConfigurationServiceImpl;
import com.sun.esb.management.impl.deployment.DeploymentServiceImpl;
import com.sun.esb.management.impl.installation.InstallationServiceImpl;
import com.sun.esb.management.impl.performance.PerformanceMeasurementServiceImpl;
import com.sun.esb.management.impl.runtime.RuntimeManagementServiceImpl;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.MBeanNames.ComponentServiceType;
import com.sun.jbi.management.MBeanNames.ServiceName;
import com.sun.jbi.management.MBeanNames.ServiceType;
import com.sun.jbi.platform.PlatformContext;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIArchive;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.common.ServiceUnitInfo;
import com.sun.jbi.ui.common.ToolsLogManager;
import com.sun.jbi.ui.common.Util;
import com.sun.jbi.ui.runtime.ComponentConfiguration;
import com.sun.jbi.ui.runtime.ComponentConfigurationParser;
import com.sun.jbi.ui.runtime.DisplayInformation;
import com.sun.jbi.ui.runtime.GenericsSupport;
import com.sun.jbi.util.ComponentConfigurationHelper;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Base class for Implementation of the UI MBean interface.
 * 
 * @author graj
 */
public class AbstractUIMBeanImpl {
    
    protected static final String STARTED_STATE                 = "Started";
    
    protected static final String RUNNING_STATE                 = "Running";
    
    private static final String   EQUAL                         = "=";
    
    private static final String   COLON                         = ":";
    
    private static final String   COMMA                         = ",";
    
    private static final char     PASSWORD_MASK_CHARACTER       = '*';

    private static final String   ENVIRONMENT_VARIABLES_KEY     = "EnvironmentVariables";
    
    protected static final String COMPONENT_CONFIG_INSTANCE_ERROR_KEY = "com.sun.jbi.cluster.instance.error";
    
    /**
     * any framework state
     */
    protected static final int    ANY_FRAMEWORK_COMPONENT_STATE = -1;
    
    /**
     * sa started state
     */
    protected static final String FRAMEWORK_SA_STARTED_STATE    = DeploymentServiceMBean.STARTED;
    
    /**
     * sa stopped state
     */
    protected static final String FRAMEWORK_SA_STOPPED_STATE    = DeploymentServiceMBean.STOPPED;
    
    /**
     * sa shutdown state
     */
    protected static final String FRAMEWORK_SA_SHUTDOWN_STATE   = DeploymentServiceMBean.SHUTDOWN;
    
    /**
     * any state
     */
    protected static final String FRAMEWORK_SA_ANY_STATE        = "any";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_STARTED_STATE    = "started";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_STOPPED_STATE    = "stopped";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_SHUTDOWN_STATE   = "shutdown";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_UNKNOWN_STATE    = "unknown";
    
    /** i18n */
    private static I18NBundle     sI18NBundle                   = null;
    
    /** Jbi Environment context. */
    protected EnvironmentContext    mEnvContext;
    
    /** Component Configuration helper */
    protected ComponentConfigurationHelper mCfgHlpr;
    
    protected transient AdministrationService    administrationService;
    
    protected transient ConfigurationService     configurationService;
    
    protected transient DeploymentService        deploymentService;
    
    protected transient InstallationService      installationService;
    
    protected transient RuntimeManagementService runtimeManagementService;
    
    protected transient PerformanceMeasurementService performanceMeasurementService;
    
    /**
     * namespace for Componennt version info node
     */
    protected static final String COMPONENT_VERSION_NS =
            "http://www.sun.com/jbi/descriptor/identification";
    
    /**
     * namespace for Componennt version info node
     */
    protected static final String COMPONENT_VERSION_NS_NEW =
            "http://www.sun.com/jbi/descriptor/identification/V1.0";
    
    /**
     * XPath query for new component version
     */
    protected static final String COMPONENT_VERSION_XPATH_QUERY =
            "/jbi:jbi/jbi:component/jbi:identification/identification:VersionInfo";
    
    /**
     * XPath query for new component version
     */
    protected static final String SHARED_LIBRARY_VERSION_XPATH_QUERY =
            "/jbi:jbi/jbi:shared-library/jbi:identification/identification:VersionInfo";

    /**
     * constructor
     * 
     * @param anEnvContext
     *            context
     */
    public AbstractUIMBeanImpl(EnvironmentContext anEnvContext) {
        this.mEnvContext = anEnvContext;
        this.mCfgHlpr = new ComponentConfigurationHelper();
    }
    
    /**
     * Get the Administration Service
     * 
     * @return administration Service
     */
    protected AdministrationService getAdministrationService() {
        if (this.administrationService == null) {
            MBeanServer server = this.mEnvContext.getMBeanServer();
            boolean isRemoteConnection = false;
            this.administrationService = new AdministrationServiceImpl(server,
                    isRemoteConnection);
        }
        return this.administrationService;
    }
    
    /**
     * Get the Configuration Service
     * 
     * @return configuration Service
     */
    protected ConfigurationService getConfigurationService() {
        if (this.configurationService == null) {
            boolean isRemoteConnection = false;
            MBeanServer server = this.mEnvContext.getMBeanServer();
            this.configurationService = new ConfigurationServiceImpl(server,
                    isRemoteConnection);
        }
        return this.configurationService;
    }
    
    /**
     * Get the Deployment Service
     * 
     * @return deployment Service
     */
    protected DeploymentService getDeploymentService() {
        if (this.deploymentService == null) {
            MBeanServer server = this.mEnvContext.getMBeanServer();
            boolean isRemoteConnection = false;
            this.deploymentService = new DeploymentServiceImpl(server,
                    isRemoteConnection);
        }
        return this.deploymentService;
    }
    
    /**
     * Get the Installation Service
     * 
     * @return installation Service
     */
    protected InstallationService getInstallationService() {
        if (this.installationService == null) {
            MBeanServer server = this.mEnvContext.getMBeanServer();
            boolean isRemoteConnection = false;
            this.installationService = new InstallationServiceImpl(server,
                    isRemoteConnection);
        }
        return this.installationService;
    }
    
    /**
     * Get the Runtime management Service
     * 
     * @return Runtime management Service
     */
    protected RuntimeManagementService getRuntimeManagementService() {
        if (this.runtimeManagementService == null) {
            MBeanServer server = this.mEnvContext.getMBeanServer();
            boolean isRemoteConnection = false;
            this.runtimeManagementService = new RuntimeManagementServiceImpl(
                    server, isRemoteConnection);
        }
        return this.runtimeManagementService;
    }
    
    /**
     * Get the Performance Measurement Service
     * 
     * @return Performance Measurement Service
     */
    protected PerformanceMeasurementService getPerformanceMeasurementService() {
        if (this.performanceMeasurementService == null) {
            MBeanServer server = this.mEnvContext.getMBeanServer();
            boolean isRemoteConnection = false;
            this.performanceMeasurementService = new PerformanceMeasurementServiceImpl(
                    server, isRemoteConnection);
        }
        return this.performanceMeasurementService;
    }

    /**
     * logs the message
     * 
     * @param aMsg
     *            message string
     */
    public static void log(String aMsg) {
        
        ToolsLogManager.getRuntimeLogger().info(aMsg);
    }
    
    /**
     * logs the message
     * 
     * @param aMsg
     *            message string
     */
    public static void logDebug(String aMsg) {
        ToolsLogManager.getRuntimeLogger().fine(aMsg);
    }
    
    /**
     * logs the message
     * 
     * @param ex
     *            exception
     */
    public static void logDebug(Exception ex) {
        ToolsLogManager.getRuntimeLogger()
                .log(Level.FINER, ex.getMessage(), ex);
    }
    
    /**
     * logs errors
     * 
     * @param ex
     *            exception
     */
    public static void logError(Exception ex) {
        ToolsLogManager.getRuntimeLogger().log(Level.SEVERE, ex.getMessage(),
                ex);
    }
    
    /**
     * logs warnings
     * 
     * @param ex
     *            exception
     */
    public static void logWarning(Exception ex) {
        ToolsLogManager.getRuntimeLogger().log(Level.WARNING, ex.getMessage(),
                ex);
    }
    
    /**
     * @return the platform context
     */
    protected PlatformContext getPlatformContext() {
        PlatformContext platformContext = null;
        EnvironmentContext context = getEnvironmentContext();
        if (context != null) {
            platformContext = context.getPlatformContext();
        }
        return platformContext;
    }
    
    /**
     * @return the EnvironmentContext
     */
    protected static EnvironmentContext getEnvironmentContext() {
        return com.sun.jbi.util.EnvironmentAccess.getContext();
    }
    
    /**
     * gives the I18N bundle
     * 
     * @return I18NBundle object
     */
    protected static I18NBundle getI18NBundle() {
        // lazzy initialize the JBI Client
        if (sI18NBundle == null) {
            sI18NBundle = new I18NBundle("com.sun.jbi.ui.runtime.mbeans");
        }
        return sI18NBundle;
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param mbeanServer
     *            connection to MBean Server
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws JBIRemoteException
     *             on error
     */
    protected Object getAttributeValue(MBeanServer mbeanServer,
            ObjectName objectName, String attributeName)
            throws JBIRemoteException {
        
        Object result = null;
        try {
            result = mbeanServer.getAttribute(objectName, attributeName);
        } catch (AttributeNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        } catch (MBeanException mbeanEx) {
            throw JBIRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
        return result;
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws JBIRemoteException
     *             on error
     */
    protected Object getAttributeValue(ObjectName objectName,
            String attributeName) throws JBIRemoteException {
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        Object result = null;
        try {
            result = mbeanServer.getAttribute(objectName, attributeName);
        } catch (AttributeNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        } catch (MBeanException mbeanEx) {
            throw JBIRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        return result;
    }
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws JBIRemoteException
     *             on error
     */
    protected void setAttributeValue(ObjectName objectName,
            String attributeName, Object attributeValue)
            throws JBIRemoteException {
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        Attribute attribute = null;
        attribute = new Attribute(attributeName, attributeValue);
        
        try {
            mbeanServer.setAttribute(objectName, attribute);
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (AttributeNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (InvalidAttributeValueException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        } catch (MBeanException mbeanEx) {
            throw JBIRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return result object
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param params
     *            parameters
     * @param signature
     *            signature of the parameters
     * @throws JBIRemoteException
     *             on user error
     */
    protected Object invokeMBeanOperation(ObjectName objectName,
            String operationName, Object[] params, String[] signature)
            throws JBIRemoteException {
        // TODO: Add error code and message to each exception
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        Object result = null;
        
        try {
            
            result = mbeanServer.invoke(objectName, operationName, params,
                    signature);
            
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (MBeanException mbeanEx) {
            throw JBIRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
        return result;
        
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return result object
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param param
     *            operation param of type String
     * @throws JBIRemoteException
     *             on user error
     */
    protected Object invokeMBeanOperation(ObjectName objectName,
            String operationName, String param) throws JBIRemoteException {
        Object[] params = new Object[1];
        params[0] = param;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        return invokeMBeanOperation(objectName, operationName, params,
                signature);
        
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return result object
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @throws JBIRemoteException
     *             on user error
     */
    protected Object invokeMBeanOperation(ObjectName objectName,
            String operationName) throws JBIRemoteException {
        Object[] params = new Object[0];
        
        String[] signature = new String[0];
        return invokeMBeanOperation(objectName, operationName, params,
                signature);
        
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return result object
     * @param attributeName
     *            attribute name
     * @param objectName
     *            object name
     * @throws JBIRemoteException
     *             on user error
     */
    protected Object getMBeanAttribute(ObjectName objectName,
            String attributeName) throws JBIRemoteException {
        // TODO: Add error code and message to each exception
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        Object result = null;
        
        try {
            
            result = mbeanServer.getAttribute(objectName, attributeName);
            
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (MBeanException mbeanEx) {
            throw JBIRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
        return result;
        
    }
    
    /**
     * This method is used to get MBeanInfo for the given mbean object
     * @return MBeanInfo object
     * @param mbeanName the objectname for the mbean
     * @throws JBIRemoteException 
     */
    protected MBeanInfo getMBeanInfo(
            ObjectName mbeanName)
    throws JBIRemoteException 
    {

        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
       
        try {
            
            return  (MBeanInfo) mbeanServer.getMBeanInfo(mbeanName);
            
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
        
    }    
    
    /**
     * Test whether an mbean is registered.
     * 
     * @param objectName
     * @return true when the mbean is registered, false otherwise
     * @throws JBIRemoteException
     */
    boolean isMBeanRegistered(ObjectName objectName) throws JBIRemoteException {
        boolean result = false;
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        try {
            result = mbeanServer.isRegistered(objectName);
        } catch (RuntimeException exception) {
            throw JBIRemoteException.filterJmxExceptions(exception);
        }
        
        return result;
    }
    
    /**
     * Test whether it is a valid target.
     * 
     * @param objectName
     * @return true when the mbean is registered, false otherwise
     * @throws JBIRemoteException
     */
    boolean isValidTarget(ObjectName objectName) throws JBIRemoteException {
        boolean result = false;
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        try {
            result = mbeanServer.isRegistered(objectName);
        } catch (RuntimeException exception) {
        }
        
        return result;
    }
    
    /**
     * Test whether it is a valid target.
     * 
     * @param targetName
     * @return true when target is a valid one, false otherwise
     * @throws JBIRemoteException
     */
    boolean isValidTarget(String target) throws JBIRemoteException {
       if (true == getPlatformContext().isStandaloneServer(target))
       {
           return true;
       }
       if (true == getPlatformContext().isCluster(target)) {
           return true;
       }
       if (true == getPlatformContext().isClusteredServer(target)) {
           return true;
       }
       if (true == "domain".equals(target)) {
           return true;
       }

       return false; 
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @param attributeValue
     *            attrbute name
     * @param attributeName
     *            attribute value
     * @param objectName
     *            object name
     * @throws JBIRemoteException
     *             on user error
     */
    protected void setMBeanAttribute(ObjectName objectName,
            String attributeName, Object attributeValue)
            throws JBIRemoteException {
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        try {
            Attribute attr = new Attribute(attributeName, attributeValue);
            mbeanServer.setAttribute(objectName, attr);
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (MBeanException mbeanEx) {
            throw JBIRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * set the specified attribute values on the mbean attributes
     * 
     * @param attrList
     *            list of attributes
     * @param objectName
     *            object name
     * @throws JBIRemoteException
     *             on user error
     */
    protected void setMBeanAttributes(ObjectName objectName,
            AttributeList attrList) throws JBIRemoteException {
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        try {
            mbeanServer.setAttributes(objectName, attrList);
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    
    /**
     * set the specified attribute values on the mbean attributes
     * 
     * @param attrList
     *            list of attributes
     * @param objectName
     *            object name
     * @return management message response from setConfigurationAttributes
     * @throws JBIRemoteException
     *             on user error
     */
    protected String setMBeanConfigAttributes(ObjectName objectName,
            AttributeList attrList) throws JBIRemoteException {
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        return this.setMBeanConfigAttributes(mbeanServer, objectName, attrList);
    }
    
    /**
     * set the specified attribute values on the mbean attributes
     * 
     * @param MBeanServer
     *            the MBean server to use
     * @param attrList
     *            list of attributes
     * @param objectName
     *            object name
     * @return management message response from setConfigurationAttributes
     * @throws JBIRemoteException
     *             on user error
     */
    protected String setMBeanConfigAttributes(MBeanServerConnection mbeanServer,
            ObjectName objectName, AttributeList attrList) throws JBIRemoteException {
        try {
            return  ((String) mbeanServer.invoke(objectName, "setConfigurationAttributes", 
                new Object[]{ attrList }, new String[]{"javax.management.AttributeList"}));
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
    }

    /**
     * set the specified attribute values on the mbean attributes
     * 
     * @param attrList
     *            list of attributes
     * @param objectName
     *            object name
     * @throws JBIRemoteException
     *             on user error
     */
    protected AttributeList setMBeanAttributes(MBeanServerConnection mbeanServer,
            ObjectName objectName, AttributeList attrList)
            throws JBIRemoteException {
        
        try {
            return mbeanServer.setAttributes(objectName, attrList);
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * Get the config extension MBean attribute values
     * 
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    protected Properties getConfigurationAttributeValues(ObjectName objectName)
            throws JBIRemoteException {
        Properties properties = new Properties();
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        try {
            properties = this.getMBeanAttributeValues(mbeanServer, objectName);
        } catch(JBIRemoteException exception) {
            properties = new Properties();
            JBIManagementMessage mgmtMsg = null;
            mgmtMsg = exception.extractJBIManagementMessage();
            properties.setProperty(COMPONENT_CONFIG_INSTANCE_ERROR_KEY, 
                    mgmtMsg.getMessage());
            
        }
        
        return properties;
    }
    
    /**
     * Get the attributes on the MBean
     * 
     * @param mbeanServer
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    protected Properties getMBeanAttributeValues(MBeanServer mbeanServer,
            ObjectName objectName) throws JBIRemoteException {
        Properties properties = new Properties();
        List<String> keyList = new ArrayList<String>();
        MBeanInfo mbeanInfo = null;
        try {
            mbeanInfo = mbeanServer.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            for (MBeanAttributeInfo attributeInfo : mbeanAttrInfoArray) {
                String key = attributeInfo.getName();
                if (key != null) {
                    Object value = this.getAttributeValue(objectName, key);
                    if (value != null) {
                        // -- Nikita : Use getApplicationVariables to get application
                        //             variable values
                        if ( key.equals("ApplicationVariables") ) {
                            //value = this
                            //         .convertToEnvironmentVariableString(value);
                            continue;
                        }
                        
                        // -- Nikita : Use getApplicationConfigurations to get application
                        //             configuration values
                        if ( key.equals("ApplicationConfigurations") ) {
                            //value = this
                            //         .convertToEnvironmentVariableString(value);
                            continue;
                        }
                        properties.put(key, value + "");
                        keyList.add(key);
                    }
                }
            }
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (IntrospectionException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        }
        
        return properties;
    }
    
       
    /**
     * Get the attributes on the MBean
     * 
     * @param mbeanServer
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    protected Map<String, Descriptor> getConfigurationDescriptors(
            ObjectName objectName) throws JBIRemoteException 
    {
        Map<String, Descriptor> descrMap = new HashMap();
        MBeanServer mbeanServer = mEnvContext.getMBeanServer();
        MBeanInfo mbeanInfo = null;
        try {
            mbeanInfo = mbeanServer.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            
            for (MBeanAttributeInfo attributeInfo : mbeanAttrInfoArray) 
            {
                if ( attributeInfo instanceof ModelMBeanAttributeInfo )
                {
                    ModelMBeanAttributeInfo modelAttrInfo
                            = (ModelMBeanAttributeInfo) attributeInfo;
                    descrMap.put(modelAttrInfo.getName(), modelAttrInfo.getDescriptor());
                }
            }
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (IntrospectionException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        }
        
        return descrMap;
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return Attribute List of the mbean to set
     * @param objectName
     *            mbean name
     * @param params
     *            a name value pair properties object contains the attribute
     *            name and its value as string.
     * @throws JBIRemoteException
     *             on user error
     */
    protected AttributeList constructMBeanAttributes(ObjectName objectName,
            Properties params) throws JBIRemoteException {
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        AttributeList attrList = new AttributeList();
        try {
            MBeanInfo mbeanInfo = mbeanServer.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            Map attribInfoMap = new HashMap();
            for (int i = 0; i < mbeanAttrInfoArray.length; ++i) {
                MBeanAttributeInfo attrInfo = mbeanAttrInfoArray[i];
                attribInfoMap.put(attrInfo.getName(), attrInfo);
            }
            
            for (Iterator itr = params.keySet().iterator(); itr.hasNext();) {
                String attrName = (String) itr.next();
                String stringValue = params.getProperty(attrName);
                Object attrValueObj = null;
                
                // We can't validate properties if we don't have MBean attr info
                if (attribInfoMap.isEmpty()) {
                    attrValueObj = stringValue;
                } else {
                    MBeanAttributeInfo attrInfo = (MBeanAttributeInfo) attribInfoMap
                            .get(attrName);
                    if (attrInfo == null) {
                        String[] args = { attrName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.info.not.found",
                                        args, null);
                        throw new JBIRemoteException(exception);
                        
                    }
                    
                    String type = attrInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attrValueObj = Util.newInstance(type, stringValue);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attrName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new JBIRemoteException(exception);
                        
                    }
                }
                
                Attribute attr = new Attribute(attrName, attrValueObj);
                attrList.add(attr);
            }
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        return attrList;
        
    }
    
    /**
     * invokes the operation on mbean
     * 
     * @return Attribute List of the mbean to set
     * @param objectName
     *            mbean name
     * @param params
     *            a name value pair properties object contains the attribute
     *            name and its value as string.
     * @throws JBIRemoteException
     *             on user error
     */
    protected AttributeList constructMBeanAttributes(
            MBeanServerConnection mbeanServer, ObjectName objectName,
            Properties params) throws JBIRemoteException {
        
        AttributeList attrList = new AttributeList();
        try {
            MBeanInfo mbeanInfo = mbeanServer.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            Map attribInfoMap = new HashMap();
            for (int i = 0; i < mbeanAttrInfoArray.length; ++i) {
                MBeanAttributeInfo attrInfo = mbeanAttrInfoArray[i];
                attribInfoMap.put(attrInfo.getName(), attrInfo);
            }
            
            for (Iterator itr = params.keySet().iterator(); itr.hasNext();) {
                String attrName = (String) itr.next();
                String stringValue = params.getProperty(attrName);
                Object attrValueObj = null;
                
                MBeanAttributeInfo attrInfo = (MBeanAttributeInfo) attribInfoMap
                        .get(attrName);
                if (attrInfo == null) {
                    String[] args = { attrName };
                    Exception exception = this
                            .createManagementException(
                                    "ui.mbean.install.config.mbean.attrib.info.not.found",
                                    args, null);
                    throw new JBIRemoteException(exception);
                    
                }
                
                String type = attrInfo.getType();
                try {
                    // construct the value object using reflection.
                    attrValueObj = Util.newInstance(type, stringValue);
                } catch (Exception ex) {
                    String[] args = { stringValue, type, attrName };
                    Exception exception = this
                            .createManagementException(
                                    "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                    args, ex);
                    throw new JBIRemoteException(exception);
                    
                }
                
                Attribute attr = new Attribute(attrName, attrValueObj);
                attrList.add(attr);
            }
            
            return attrList;
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * Creates a management message string and populates the exception
     * 
     * @param bundleKey
     * @param args
     *            of Strings
     * @param sourceException -
     *            the source exception to propagate
     * @return Exception object created with a valid XML Management Message
     */
    Exception createManagementException(String bundleKey, String[] args,
            Exception sourceException) {
        Exception exception = null;
        String xmlManagementMessage = JBIResultXmlBuilder.createJbiResultXml(
                AbstractUIMBeanImpl.getI18NBundle(), bundleKey, args,
                sourceException);
        exception = new Exception(xmlManagementMessage);
        return exception;
    }
    
    /**
     * Creates a management message.
     *
     * @
     *
     */
    String createManagementMessage(String taskId, boolean successResult,
            String msgType, String msgCode, Object[] args)
    {
        String msg = AbstractUIMBeanImpl.getI18NBundle().getMessage(msgCode, args);
        String xmlManagementMessage = JBIResultXmlBuilder.getInstance().createJbiResultXml(
            taskId, successResult, msgType, msgCode, msg, args);
        
        return xmlManagementMessage;
    }
    
    /**
     * check for shared library
     * 
     * @return true if it is shared library else false.
     * @param zipPath
     *            file path
     */
    public boolean isSharedLibraryArchive(String zipPath) {
        try {
            JBIArchive jbiZip = new JBIArchive(zipPath);
            return jbiZip.isSharedLibraryArchive();
        } catch (Exception ex) {
            logDebug(ex);
            return false;
        }
    }
    
    /**
     * finds the component id property in the object name
     * 
     * @param jmxObjectName
     *            jmx obj name
     * @return componentId of the component if embbeded in object name else
     *         null.
     */
    protected String getComponentNameFromJmxObjectName(ObjectName jmxObjectName) {
        String componentName = null;
        try {
            // TODO change COMPONENT_ID_KEY to Component Name when mgmt api
            // changes.
            componentName = ((ObjectName) jmxObjectName)
                    .getKeyProperty(JBIJMXObjectNames.COMPONENT_ID_KEY);
            
        } catch (NullPointerException nullEx) {
            componentName = null;
        }
        return componentName;
    }
    
    /**
     * returns the ObjectName for the AdminService Mbean of this component.
     * 
     * @return the ObjectName of the InstallationService MBean or null.
     * @throws JBIRemoteException
     *             on error.
     */
    public ObjectName getAdminServiceMBeanObjectName()
            throws JBIRemoteException {
        try {
            MBeanNames mbeanNames = this.mEnvContext.getMBeanNames();
            
            return mbeanNames.getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_ADMIN_SERVICE,
                    MBeanNames.CONTROL_TYPE_ADMIN_SERVICE);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
    }
    
    /**
     * returns the ObjectName for the InstallationService Mbean of this
     * component.
     * 
     * @return the ObjectName of the InstallationService MBean or null.
     */
    public ObjectName getInstallationServiceMBeanObjectName() {
        MBeanNames mbeanNames = this.mEnvContext.getMBeanNames();
        
        return mbeanNames.getSystemServiceMBeanName(
                MBeanNames.SERVICE_NAME_INSTALL_SERVICE,
                MBeanNames.CONTROL_TYPE_INSTALL_SERVICE);
    }
    
    /**
     * returns the ObjectName for the InstallationService Mbean of this
     * component.
     * 
     * @param targetName
     * @return the ObjectName of the InstallationService MBean or null.
     */
    public ObjectName getInstallationServiceMBeanObjectName(String targetName) {
        
        MBeanNames mbeanNames = this.mEnvContext.getMBeanNames();
        return mbeanNames.getSystemServiceMBeanName(
                ServiceName.InstallationService, ServiceType.Installation,
                targetName);
    }
    
    /**
     * returns the ObjectName for the DeploymentService Mbean of this
     * component.
     * 
     * @param targetName
     * 
     * @return the ObjectName of the DeploymentService MBean or null.
     */
    public ObjectName getDeploymentServiceMBeanObjectName(String targetName) {
        MBeanNames mbeanNames = this.mEnvContext.getMBeanNames();
        
        return mbeanNames.getSystemServiceMBeanName(
                ServiceName.DeploymentService, ServiceType.Deployment,
                targetName);
    }
    
    /**
     * returns the ObjectName for the LoggingService Mbean of this target.
     * 
     * @param targetName
     * 
     * @return the ObjectName of the LoggingService MBean or null.
     */
    public ObjectName getLoggingServiceMBeanObjectName(String targetName) {
        MBeanNames mbeanNames = this.mEnvContext.getMBeanNames();
        
        
        ObjectName 
                loggingMBeanObjectName = mbeanNames.getSystemServiceMBeanName(
                                    MBeanNames.ServiceName.ConfigurationService, 
                                    MBeanNames.ServiceType.Logger,
                                    targetName);
         /**
        String loggingString = JBIJMXObjectNames.JMX_JBI_DOMAIN + COLON
                + MBeanNames.INSTANCE_NAME_KEY + EQUAL + targetName + COMMA
                + JBIJMXObjectNames.SERVICE_NAME_KEY + EQUAL
                + MBeanNames.SERVICE_NAME_LOGGING_SERVICE + COMMA
                + JBIJMXObjectNames.CONTROL_TYPE_KEY + EQUAL
                + MBeanNames.CONTROL_TYPE_LOGGING_SERVICE + COMMA
                + JBIJMXObjectNames.COMPONENT_TYPE_KEY + EQUAL
                + JBIJMXObjectNames.SYSTEM_COMPONENT_TYPE_VALUE;
         
        
        try {
            loggingMBeanObjectName = new ObjectName(loggingString);
        } catch (Exception exception) {
            logDebug(exception);
        }*/
        return loggingMBeanObjectName;
    }
    
    /**
     * Looks up the cascaded LoggerMBeans for all JBI Framework
     * System Services for a specific instance
     * 
     * @return array of object names for all schemaorg_apache_xmlbeans.system service LoggerMBeans.
     */
    public ObjectName[] getSystemLoggerMBeans(String instanceName)
    {
        MBeanNames mbn = mEnvContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.INSTANCE_NAME_KEY  + "=" + instanceName;
        tmp += "," + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_SYSTEM;
        tmp += "," + mbn.CONTROL_TYPE_KEY   + "=" + mbn.CONTROL_TYPE_LOGGER;
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:
        ObjectName mbeanPattern = null;
        try
        {
            mbeanPattern = new ObjectName(tmp);
        }
        catch(javax.management.MalformedObjectNameException mex)
        {
            logDebug(mex.getMessage());
            return new ObjectName[0];
        }
        
        java.util.Set resultSet 
                = mEnvContext.getMBeanServer().queryNames(mbeanPattern, null);
        ObjectName[] names = new ObjectName[0];
        
        if ( !resultSet.isEmpty() )
        {
            names = (ObjectName[]) resultSet.toArray(names);
        }
        else
        {
            logDebug("Logger MBeans with ObjectName pattern "
                        + tmp + " not found.");
        }

        return names;
    }
    
    /**
     * 
     * @param targetName
     * @param configurationType
     * @return
     * @throws JBIRemoteException
     */
    public ObjectName getConfigurationMBeanObjectName(String targetName,
            String configurationType) throws JBIRemoteException {
        ObjectName configurationMBeanObjectName = null;
        String objectNameString = JBIJMXObjectNames.JMX_JBI_DOMAIN + COLON
                + JBIJMXObjectNames.TARGET_KEY + EQUAL + targetName + COMMA
                + JBIJMXObjectNames.SERVICE_NAME_KEY + EQUAL
                + JBIJMXObjectNames.CONFIGURATION_SERVICE + COMMA
                + JBIJMXObjectNames.SERVICE_TYPE_KEY + EQUAL
                + configurationType;
        
        try {
            configurationMBeanObjectName = new ObjectName(objectNameString);
        } catch (MalformedObjectNameException exception) {
            throw new JBIRemoteException(exception);
        } catch (NullPointerException exception) {
            throw new JBIRemoteException(exception);
        }
        return configurationMBeanObjectName;
    }
    
    /**
     * Retrieves the password fields in the given XML String
     * 
     * @param xmlDataString
     * @param keys
     * @return
     */
    List getComponentConfigurationPasswordFields(String xmlDataString,
            String[] keys) throws JBIRemoteException {
        List<String> passwordFieldList = new ArrayList<String>();
        ComponentConfiguration configuration = null;
        ComponentConfigurationParser parser = null;
        try {
            parser = ComponentConfigurationParser.parseFromString(
                    xmlDataString, keys);
            configuration = parser.getComponentConfiguration();
            if (configuration != null) {
                Entry<String, DisplayInformation> entry = null;
                String key = null;
                DisplayInformation value = null;
                Set<Entry<String, DisplayInformation>> set = configuration
                        .getDisplayDetailsMap().entrySet();
                Iterator<Entry<String, DisplayInformation>> iterator = set
                        .iterator();
                while (iterator.hasNext() == true) {
                    entry = iterator.next();
                    if (entry != null) {
                        key = entry.getKey();
                        value = entry.getValue();
                        if (value != null) {
                            if (value.isPasswordField() == true) {
                                passwordFieldList.add(key);
                            }
                        }
                    }
                }
            }
        } catch (MalformedURLException e) {
            throw new JBIRemoteException(e);
        } catch (ParserConfigurationException e) {
            throw new JBIRemoteException(e);
        } catch (SAXException e) {
            throw new JBIRemoteException(e);
        } catch (URISyntaxException e) {
            throw new JBIRemoteException(e);
        } catch (IOException e) {
            throw new JBIRemoteException(e);
        }
        
        return passwordFieldList;
    }
    
    /**
     * Retrieve component configuration display data as an XML String
     * 
     * @param server
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    String getComponentConfigurationDisplayData(MBeanServer server,
            ObjectName objectName) throws JBIRemoteException {
        Object[] params = null;
        String[] signature = null;
        String xmlDataString = "";
        
        try {
            xmlDataString = (String) server.invoke(objectName,
                    "retrieveConfigurationDisplayData", params, signature);
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (MBeanException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        }
        return xmlDataString;
    }
    
    /**
     * Retrieve component configuration display schema as an XML String
     * 
     * @param server
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    String getComponentConfigurationDisplaySchema(MBeanServer server,
            ObjectName objectName) throws JBIRemoteException {
        Object[] params = null;
        String[] signature = null;
        String xmlSchemaString = "";
        
        try {
            xmlSchemaString = (String) server.invoke(objectName,
                    "retrieveConfigurationDisplaySchema", params, signature);
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (MBeanException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        }
        
        return xmlSchemaString;
    }
    
    /**
     * Retrieve component configuration display data as an XML String
     * 
     * @param connection
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    String getComponentConfigurationDisplayData(
            MBeanServerConnection connection, ObjectName objectName)
            throws JBIRemoteException {
        Object[] params = null;
        String[] signature = null;
        String xmlDataString = "";
        
        try {
            xmlDataString = (String) connection.invoke(objectName,
                    "retrieveConfigurationDisplayData", params, signature);
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (MBeanException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        } catch (IOException e) {
            throw new JBIRemoteException(e);
        }
        return xmlDataString;
    }
    
    /**
     * Retrieve component configuration display schema as an XML String
     * 
     * @param connection
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    String getComponentConfigurationDisplaySchema(
            MBeanServerConnection connection, ObjectName objectName)
            throws JBIRemoteException {
        Object[] params = null;
        String[] signature = null;
        String xmlSchemaString = "";
        
        try {
            xmlSchemaString = (String) connection.invoke(objectName,
                    "retrieveConfigurationDisplaySchema", params, signature);
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (MBeanException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        } catch (IOException e) {
            throw new JBIRemoteException(e);
        }
        return xmlSchemaString;
    }
    
    /**
     * Get the attributes on the MBean
     * 
     * @param connection
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    protected Properties getMBeanAttributeValues(
            MBeanServerConnection connection, ObjectName objectName)
            throws JBIRemoteException {
        Properties properties = new Properties();
        List<String> keyList = new ArrayList<String>();
        MBeanInfo mbeanInfo = null;
        try {
            mbeanInfo = connection.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            for (MBeanAttributeInfo attributeInfo : mbeanAttrInfoArray) {
                String key = attributeInfo.getName();
                if (key != null) {
                    Object value = null;
                    try {
                        value = connection.getAttribute(objectName, key);
                    } catch (AttributeNotFoundException e) {
                        throw new JBIRemoteException(e);
                    } catch (InstanceNotFoundException e) {
                        throw new JBIRemoteException(e);
                    } catch (MBeanException e) {
                        throw new JBIRemoteException(e);
                    } catch (ReflectionException e) {
                        throw new JBIRemoteException(e);
                    }
                    if (value != null) {
                        if (key.equals("ApplicationVariables")) 
                        {
                            // Use getApplicationVariables to get app var data
                            // value = this
                            //     .convertToEnvironmentVariableString(value);
                            continue;
                        }
                        if (key.equals("ApplicationConfigurations")) 
                        {
                            // Use getApplicationConfigurations to get app config data
                            // value = this
                            //     .convertToEnvironmentVariableString(value);
                            continue;
                        }
                        properties.put(key, value + "");
                        keyList.add(key);
                    }
                }
            }
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (IntrospectionException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        } catch (IOException e) {
            throw new JBIRemoteException(e);
        }
        
        // Mask Sensitive password Strings
        /** TODO : With the facade layer we do not have the display data XML
         * When the common client is getting the attributes from the actual 
         * component MBean this path should be invoked
        if (keyList.size() > 0) {
            String xmlDataString = this.getComponentConfigurationDisplayData(
                    connection, objectName);
            keyList = this.getComponentConfigurationPasswordFields(
                    xmlDataString, GenericsSupport.toArray(keyList,
                            String.class));
            for (String key : keyList) {
                String sensitiveString = properties.getProperty(key);
                sensitiveString = this
                        .maskSensitiveString(sensitiveString, PASSWORD_MASK_CHARACTER);
                properties.setProperty(key, sensitiveString);
            }
        }
         */
        
        return properties;
    }
    
    /**
     * Sets environment variables as name=,type=, value=;
     * 
     * @param valueObject
     * @return environment variable string
     */
    String convertToEnvironmentVariableString(Object valueObject) {
        String result = "";
        TabularData tabularData = null;
        if (valueObject instanceof TabularData) {
            tabularData = (TabularData) valueObject;
            
            for (Iterator dataIterator = tabularData.values().iterator(); dataIterator
                    .hasNext();) {
                CompositeData compositeData = (CompositeData) dataIterator
                        .next();
                CompositeType compositeType = compositeData.getCompositeType();
                for (Iterator itemIterator = compositeType.keySet().iterator(); itemIterator
                        .hasNext();) {
                    String keyName = (String) itemIterator.next();
                    String keyType = (String) itemIterator.next();
                    String keyValue = (String) itemIterator.next();
                    
                    String name = (String) compositeData.get(keyName);
                    String type = (String) compositeData.get(keyType);
                    String value = (String) compositeData.get(keyValue);
                    result += ("Name=" + name + ", Type=" + type
                            + ", Value=" + value + "; ");
                }
            }
        }
        
        return result;
    }
    
    /**
     * 
     * @param environmentVariableMap
     * @param environmentVariableRowType
     * @return
     * @throws OpenDataException
     */
    TabularData getEnvironmentVariables(
            Map<String, String[]> environmentVariableMap,
            CompositeType environmentVariableRowType) throws OpenDataException {
        TabularData tabularData = createEnvironmentVariableTabularStructure();
        for (String name : environmentVariableMap.keySet()) {
            String[] metadata = (String[]) environmentVariableMap.get(name);
            String value = metadata[0];
            String type = metadata[1];
            
            Object[] data = new Object[] { name, value, type };
            
            CompositeData rowData = new CompositeDataSupport(
                    environmentVariableRowType, new String[] { "name", "value",
                        "type" }, data);
            tabularData.put(rowData);
        }
        
        return tabularData;
    }
    
    /**
     * 
     * @param tabularDataIn
     * @return
     * @throws InvalidAttributeValueException
     * @throws OpenDataException
     * @throws MBeanException
     */
    Map<String, String[]> setEnvironmentVariables(TabularData tabularDataIn)
            throws InvalidAttributeValueException, OpenDataException,
            MBeanException {
        String attrName = ENVIRONMENT_VARIABLES_KEY;
        Map<String, String[]> environmentVariableMap = new HashMap<String, String[]>();
        String mapKey = null;
        TabularData newTabularData = tabularDataIn;
        List items = new ArrayList();
        CompositeType rowType = tabularDataIn.getTabularType().getRowType();
        
        try {
            newTabularData = tabularDataIn;
        } catch (Exception ex) {
            throw new InvalidAttributeValueException("Invalid arguments");
        }
        
        for (Iterator rowTypeItems = rowType.keySet().iterator(); rowTypeItems
                .hasNext();) {
            String item = (String) (rowTypeItems).next();
            items.add(item);
        }
        
        int itemSize = items.size();
        if (itemSize != 3) {
            throw new InvalidAttributeValueException("Invalid Item Size: "
                    + itemSize);
        }
        
        // getting the row index
        List rowIndex = tabularDataIn.getTabularType().getIndexNames();
        int rowIndexSize = rowIndex.size();
        if (rowIndexSize > 1) {
            throw new InvalidAttributeValueException("Invalid Row Index Size: "
                    + rowIndexSize);
        } else if (rowIndex.size() <= 0) {
            throw new InvalidAttributeValueException("Invalid Row Index Size: "
                    + rowIndexSize);
        } else {
            mapKey = (String) rowIndex.get(0);
            if (mapKey == null || "".equals(mapKey)) {
                throw new InvalidAttributeValueException(
                        "Invalid Row Index Key: " + mapKey);
            }
        }
        
        for (Iterator dataIter = tabularDataIn.values().iterator(); dataIter
                .hasNext();) {
            CompositeData rowData = (CompositeData) dataIter.next();
            
            String name = null;
            String value = null;
            String type = null;
            for (ListIterator<String> itemIt = items.listIterator(); itemIt
                    .hasNext();) {
                String item = itemIt.next();
                if (mapKey.equals(item)) {
                    name = (String) rowData.get(item);
                } else if ("value".equals(item)) {
                    value = (String) rowData.get(item);
                } else {
                    type = (String) rowData.get(item);
                }
            }
            environmentVariableMap.put(name, new String[] { value, type });
        }
        return environmentVariableMap;
    }
    
    /**
     * 
     * @return
     * @throws OpenDataException
     */
    TabularData createEnvironmentVariableTabularStructure()
            throws OpenDataException {
        TabularData tabularData = null;
        CompositeType mEnvVarRowType = null;
        TabularType mEnvVarTabularType = null;
        
        String[] envVarRowAttrNames = { "name", "value", "type" };
        String[] envVarRowAttrDesc = { "Environment variable name",
                "Environment variable value", "Environment variable type" };
        OpenType[] envVarRowAttrTypes = { SimpleType.STRING, SimpleType.STRING,
                SimpleType.STRING };
        String[] envVarRowIndex = { "name" };
        
        if (mEnvVarRowType == null) {
            mEnvVarRowType = new CompositeType("NameValuePair",
                    "Environment variable name and value pair",
                    envVarRowAttrNames, envVarRowAttrDesc, envVarRowAttrTypes);
        }
        
        if (mEnvVarTabularType == null) {
            mEnvVarTabularType = new TabularType("EnvironmentVariableList",
                    "List of environment name and value pairs", mEnvVarRowType,
                    envVarRowIndex);
        }
        
        tabularData = new TabularDataSupport(mEnvVarTabularType);
        
        return tabularData;
    }
    
    /**
     * 
     * @param sensitiveString
     * @param maskCharacter
     * @return
     */
    String maskSensitiveString(String sensitiveString, char maskCharacter) {
        String result = "";
        if (sensitiveString == null) {
            return result;
        }
        for (int index = 0; index < sensitiveString.length(); index++) {
            result += maskCharacter;
        }
        return result;
    }
    
    /**
     * 
     * @param componentName
     * @param targetName
     * @return
     */
    Map<String /* targetInstanceName */, Properties /* configProps */> getComponentConfigurationProperties(
            String componentName, String targetName) {
        Map<String /* targetInstanceName */, Map<MBeanServerConnection, ObjectName>> targetInstanceMap = null;
        Map<MBeanServerConnection, ObjectName> serverObjectNameMap = null;
        Map<String /* targetName */, Properties /* configProps */> result = null;
        
        result = new HashMap<String /* targetInstanceName */, Properties /* configProps */>();
        try {
            targetInstanceMap = this
                    .getComponentConfigurationMBeanNamesPerTargetInstance(
                            componentName, targetName);
            for (Entry<String /* targetInstanceName */, Map<MBeanServerConnection, ObjectName>> entry : targetInstanceMap
                    .entrySet()) {
                String targetInstanceName = entry.getKey();
                if (targetInstanceName != null) {
                    serverObjectNameMap = entry.getValue();
                    if (serverObjectNameMap != null) {
                        MBeanServerConnection connection = null;
                        ObjectName objectName = null;
                        for (Entry<MBeanServerConnection, ObjectName> innerEntry : serverObjectNameMap
                                .entrySet()) {
                            connection = innerEntry.getKey();
                            objectName = innerEntry.getValue();
                            if ((connection != null) && (objectName != null)) {
                                
                                Properties properties = null;
                                try {
                                    properties = getMBeanAttributeValues(
                                        connection, objectName);
                                result.put(targetInstanceName, properties);
                                } catch(JBIRemoteException exception) {
                                    properties = new Properties();
                                    JBIManagementMessage mgmtMsg = null;
                                    mgmtMsg = exception.extractJBIManagementMessage();
                                    properties.setProperty(COMPONENT_CONFIG_INSTANCE_ERROR_KEY, 
                                            mgmtMsg.getMessage());
                                    result.put(targetInstanceName, properties);
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
        } catch (JBIRemoteException e) {
        } catch (Exception e) {
        }
        
        return result;
    }
    
    /**
     * 
     * @param componentName
     * @param configurationValues
     * @param targetName
     * @return
     * @throws JBIRemoteException
     */
    Properties /* targetInstanceName, success/failure */
    setComponentConfigurationProperties(String componentName,
            Properties /* configProps */configurationValues, String targetName)
            throws JBIRemoteException {
        Map<String /* targetInstanceName */, Map<MBeanServerConnection, ObjectName>> targetInstanceMap = null;
        Map<MBeanServerConnection, ObjectName> serverObjectNameMap = null;
        AttributeList list = null;
        Properties result = new Properties();
        
        try {
            targetInstanceMap = this
                    .getComponentConfigurationMBeanNamesPerTargetInstance(
                            componentName, targetName);
            for (Entry<String /* targetInstanceName */, Map<MBeanServerConnection, ObjectName>> entry : targetInstanceMap
                    .entrySet()) {
                String targetInstanceName = entry.getKey();
                if (targetInstanceName != null) {
                    serverObjectNameMap = entry.getValue();
                    if (serverObjectNameMap != null) {
                        MBeanServerConnection connection = null;
                        ObjectName objectName = null;
                        for (Entry<MBeanServerConnection, ObjectName> innerEntry : serverObjectNameMap
                                .entrySet()) {
                            connection = innerEntry.getKey();
                            objectName = innerEntry.getValue();
                            if ((connection != null) && (objectName != null)) {
                                try {
                                    list = this.constructMBeanAttributes(
                                            connection, objectName,
                                            configurationValues);
                                    this.setMBeanAttributes(connection,
                                            objectName, list);
                                    result.put(targetInstanceName,
                                            componentName);
                                } catch (Exception ex) {
                                    Exception exception = this
                                            .createManagementException(
                                                    "ui.mbean.install.config.mbean.error.set.attrs.error",
                                                    null, ex);
                                    JBIRemoteException remoteException = new JBIRemoteException(
                                            exception);
                                    result.put(targetInstanceName,
                                            remoteException.getMessage());
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException exception) {
            throw new JBIRemoteException(exception);
        } catch (Exception exception) {
            throw new JBIRemoteException(exception);
        }
        return result;
    }
    
    /**
     * Retrieve object names & connection per target instance for config MBeans
     * 
     * @param targetName -
     *            target name
     * @param componentName -
     *            component name
     * @return an array of ObjectNames of the Custom MBeans registered by this
     *         component on an instance
     * @throws IOException,
     *             Exception on errors
     */
    private Map<String /* targetInstanceName */, Map<MBeanServerConnection, ObjectName>> getComponentConfigurationMBeanNamesPerTargetInstance(
            String componentName, String targetName) throws IOException,
            Exception {
        Map<String /* targetInstanceName */, Map<MBeanServerConnection, ObjectName>> result = null;
        Map<MBeanServerConnection, ObjectName> serverObjectNameMap = null;
        
        result = new HashMap<String /* targetInstanceName */, Map<MBeanServerConnection, ObjectName>>();
        serverObjectNameMap = new HashMap<MBeanServerConnection, ObjectName>();
        
        // Custom MBeanName filter is :
        // com.sun.jbi:JbiName=instanceName,CustomControlName=customName,ComponentName=mComponentName,ControlType=Custom,*
        // com.sun.ebi:ServiceType=Configuration,InstallationType=bindingComponents,IdentificationName=sun-http-binding
        String name = "com.sun.ebi:ServiceType=Configuration,IdentificationName="
                + componentName + ",*";
        ObjectName objectNameFilter = new ObjectName(name);
        // Get MBeanServer connections to different target instances
        PlatformContext platformContext = this.getPlatformContext();
        MBeanServerConnection server = null;
        // Ensure we have a valid platform context
        // also check for valid targetName
        Set<ObjectName> objectNameSet = null;
        if ((platformContext != null)
                && (true == platformContext.isValidTarget(targetName))) {
            if (true == platformContext.isStandaloneServer(targetName)) {
                server = platformContext.getMBeanServerConnection(targetName);
                if (server != null) {
                    objectNameSet = server.queryNames(objectNameFilter, null);
                    for (ObjectName objectName : objectNameSet) {
                        if (objectName != null) {
                            serverObjectNameMap.put(server, objectName);
                            result.put(targetName, serverObjectNameMap);
                            break;
                        }
                    }
                }
                
            } else {
                Set<String> targetInstanceNames = platformContext
                        .getServersInCluster(targetName);
                for (String targetInstanceName : targetInstanceNames) {
                    if (targetInstanceName != null) {
                        server = platformContext
                                .getMBeanServerConnection(targetInstanceName);
                        if (server != null) {
                            objectNameSet = server.queryNames(objectNameFilter,
                                    null);
                            for (ObjectName objectName : objectNameSet) {
                                if (objectName != null) {
                                    serverObjectNameMap.put(server, objectName);
                                    result.put(targetInstanceName,
                                            serverObjectNameMap);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * returns the ObjectName for the Extension Mbean of this component.
     * 
     * @param componentName
     * @param targetName
     * 
     * @return the ObjectName of the Extension MBean or null.
     */
    public ObjectName getExtensionMBeanObjectName(String componentName,
            String targetName) throws JBIRemoteException {
        ObjectName extensionMBeanObjectNamePattern = null;
        MBeanNames mbeanNames = this.mEnvContext.getMBeanNames();
        
        String objectNamePatternString = JBIJMXObjectNames.JMX_JBI_DOMAIN
                + COLON + JBIJMXObjectNames.TARGET_KEY + EQUAL + targetName
                + COMMA + JBIJMXObjectNames.COMPONENT_ID_KEY + EQUAL
                + componentName + COMMA + JBIJMXObjectNames.SERVICE_TYPE_KEY
                + EQUAL + ComponentServiceType.Extension;
        
        try {
            extensionMBeanObjectNamePattern = new ObjectName(
                    objectNamePatternString);
        } catch (MalformedObjectNameException exception) {
            return null;
        } catch (NullPointerException exception) {
            return null;
        }
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        Set objectNames = mbeanServer.queryNames(
                extensionMBeanObjectNamePattern, null);
        
        if (objectNames.isEmpty()) {
            String[] args = { componentName,
                    extensionMBeanObjectNamePattern.toString() };
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.extension.mbean.not.found.with.query",
                            args, null);
            throw new JBIRemoteException(exception);
            
        }
        
        if (objectNames.size() > 1) {
            String[] args = { componentName,
                    extensionMBeanObjectNamePattern.toString() };
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.extension.mbean.not.found.with.query",
                            args, null);
            throw new JBIRemoteException(exception);
            
        }
        
        ObjectName extensionMBeanObjectName = null;
        extensionMBeanObjectName = (ObjectName) objectNames.iterator().next();
        logDebug("extensionMBeanObjectName Found : " + extensionMBeanObjectName);
        
        return extensionMBeanObjectName;
    }
    
    Map<String /* propertyKeyName */, Properties> getRuntimeConfigurationMetadataInternal(
            String targetName) throws JBIRemoteException {
        Properties properties = null;
        Map<String /* propertyKeyName */, Properties> result = null;
        result = new HashMap<String /* propertyKeyName */, Properties>();
        
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        String SYSTEM_TYPE = "System";
        String INSTALLATION_TYPE = "Installation";
        String DEPLOYMENT_TYPE = "Deployment";
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        Object attributeValueObject = null;
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeName != null) {
                    properties = new Properties();
                    if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                        Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                                .getDescriptor();
                        String[] fields = descriptor.getFieldNames();
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((fieldName != null) && (value != null)) {
                                properties.setProperty(fieldName, value + "");
                            }
                        }
                    }
                    result.put(attributeName, properties);
                }
            }
        }
        
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeName != null) {
                    properties = new Properties();
                    if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                        Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                                .getDescriptor();
                        String[] fields = descriptor.getFieldNames();
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((fieldName != null) && (value != null)) {
                                properties.setProperty(fieldName, value + "");
                            }
                        }
                    }
                    result.put(attributeName, properties);
                }
            }
        }
        
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeName != null) {
                    properties = new Properties();
                    if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                        Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                                .getDescriptor();
                        String[] fields = descriptor.getFieldNames();
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((fieldName != null) && (value != null)) {
                                properties.setProperty(fieldName, value + "");
                            }
                        }
                    }
                    result.put(attributeName, properties);
                }
            }
        }
        
        return result;
    }
    
    boolean setRuntimeConfigurationInternal(Properties properties,
            String targetName) throws JBIRemoteException {
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        final String SYSTEM_TYPE = "System";
        final String INSTALLATION_TYPE = "Installation";
        final String DEPLOYMENT_TYPE = "Deployment";
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        Object attributeValueObject = null;
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        
        Map<String /* Name */, Object /* Type */> attributeNameTypeMap = new HashMap<String /* Name */, Object /* Type */>();
        
        Properties metaDataProperties = null;
        // Validate to ensure there is no poison data
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new JBIRemoteException(exception);
                        
                    }
                }
            }
        }
        
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new JBIRemoteException(exception);
                        
                    }
                }
            }
        }
        
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new JBIRemoteException(exception);
                        
                    }
                }
            }
        }
        
        // Make sure the properties does not have bogus key values
        // passed in.
        Set keySet = properties.keySet();
        Set attributeSet = attributeNameTypeMap.keySet();
        
        for (Object keyObject : keySet) {
            String key = (String) keyObject;
            if (attributeSet.contains(key) == false) {
                // throw an exception
                String[] args = { key };
                Exception exception = this
                        .createManagementException(
                                "ui.mbean.runtime.config.mbean.attrib.key.invalid.error",
                                args, null);
                throw new JBIRemoteException(exception);
            }
        }
        
        // Retrieve runtime configuration metadata
        final String IS_STATIC_KEY = "isStatic";
        boolean isRestartRequired = false;
        Map<String /* propertyKeyName */, Properties> metadata = null;
        metadata = getRuntimeConfigurationMetadataInternal(targetName);
        
        // Set Values
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new JBIRemoteException(exception);
                        
                    }
                    this.setAttributeValue(systemObjectName, attributeName,
                            attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new JBIRemoteException(exception);
                        
                    }
                    this.setAttributeValue(installationObjectName,
                            attributeName, attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new JBIRemoteException(exception);
                        
                    }
                    this.setAttributeValue(deploymentObjectName, attributeName,
                            attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        return isRestartRequired;
    }
    
    Properties getDefaultRuntimeConfigurationInternal(String targetName)
            throws JBIRemoteException {
        final String DEFAULT_VALUE_KEY = "default";
        Properties properties = new Properties();
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        String SYSTEM_TYPE = "System";
        String INSTALLATION_TYPE = "Installation";
        String DEPLOYMENT_TYPE = "Deployment";
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    for (String fieldName : fields) {
                        Object value = descriptor.getFieldValue(fieldName);
                        if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                && (value != null)) {
                            properties.setProperty(attributeName, value + "");
                        }
                    }
                }
            }
        }
        
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    for (String fieldName : fields) {
                        Object value = descriptor.getFieldValue(fieldName);
                        if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                && (value != null)) {
                            properties.setProperty(attributeName, value + "");
                        }
                    }
                }
            }
        }
        
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    for (String fieldName : fields) {
                        Object value = descriptor.getFieldValue(fieldName);
                        if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                && (value != null)) {
                            properties.setProperty(attributeName, value + "");
                        }
                    }
                }
            }
        }
                
        return properties;
    }
    
    Properties getRuntimeConfigurationInternal(String targetName)
            throws JBIRemoteException {
        Properties properties = new Properties();
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        String SYSTEM_TYPE = "System";
        String INSTALLATION_TYPE = "Installation";
        String DEPLOYMENT_TYPE = "Deployment";
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new JBIRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new JBIRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new JBIRemoteException(exception);
        }
        
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(systemObjectName,
                        attributeName);
                properties.setProperty(attributeName, attributeValue + "");
            }
        }
        
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(
                        installationObjectName, attributeName);
                properties.setProperty(attributeName, attributeValue + "");
            }
        }
        
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(deploymentObjectName,
                        attributeName);
                properties.setProperty(attributeName, attributeValue + "");
            }
        }
                
        return properties;
    }
    
    /**
     * returns the ObjectName for the DeployerMBean for the component.
     * 
     * @return the ObjectName for the lifecycle Mbean.
     * @param componentName
     *            of a binding or engine component.
     * @throws JBIRemoteException
     *             on error
     */
    public ObjectName findDeployerMBean(String componentName)
            throws JBIRemoteException {
        
        ObjectName deployerObjectNamePattern = null;
        try {
            deployerObjectNamePattern = JBIJMXObjectNames
                    .getComponentDeployerMBeanObjectNamePattern(componentName);
        } catch (MalformedObjectNameException ex) {
            throw new JBIRemoteException(ex);
        }
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        Set objectNames = mbeanServer.queryNames(deployerObjectNamePattern,
                null);
        
        if (objectNames.isEmpty()) {
            String[] args = { componentName,
                    deployerObjectNamePattern.toString() };
            Exception exception = this.createManagementException(
                    "ui.mbean.deployer.mbean.not.found.with.query", args, null);
            throw new JBIRemoteException(exception);
            
        }
        
        if (objectNames.size() > 1) {
            String[] args = { componentName,
                    deployerObjectNamePattern.toString() };
            Exception exception = this.createManagementException(
                    "ui.mbean.multiple.deployer.mbeans.found.with.query", args,
                    null);
            throw new JBIRemoteException(exception);
            
        }
        
        ObjectName deployerObjectName = (ObjectName) objectNames.iterator()
                .next();
        logDebug("DeployerMBean Found : " + deployerObjectName);
        
        return deployerObjectName;
        
    }
    
    /**
     * returns the ObjectName for the lifecycle Mbean of this component.
     * 
     * @return the ObjectName for the lifecycle Mbean.
     * @param componentName
     *            of a binding or engine component.
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     */
    public ObjectName getComponentLifeCycleMBeanObjectName(
            String componentName, String targetName) throws JBIRemoteException {
        
        if (!isExistingComponent(componentName, targetName)) {
            String[] args = { componentName, targetName };
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.lifecycle.mbean.not.found.with.query",
                            args, null);
            throw new JBIRemoteException(exception);
        }
        
        ObjectName lifecycleObjectNamePattern = null;
        try {
            lifecycleObjectNamePattern = JBIJMXObjectNames
                    .getComponentLifeCycleMBeanObjectNamePattern(componentName,
                            targetName);
        } catch (MalformedObjectNameException ex) {
            throw new JBIRemoteException(ex);
        }
        
        MBeanServer mbeanServer = this.mEnvContext.getMBeanServer();
        
        Set objectNames = mbeanServer.queryNames(lifecycleObjectNamePattern,
                null);
        
        if (objectNames.isEmpty()) {
            String[] args = { componentName, targetName };
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.lifecycle.mbean.not.found.with.query",
                            args, null);
            throw new JBIRemoteException(exception);
            
        }
        
        if (objectNames.size() > 1) {
            String[] args = { componentName, targetName };
            Exception exception = this.createManagementException(
                    "ui.mbean.multiple.lifecycle.mbeans.found.with.query",
                    args, null);
            throw new JBIRemoteException(exception);
        }
        
        ObjectName lifecyleObjectName = (ObjectName) objectNames.iterator()
                .next();
        logDebug("LifecyleMBean : " + lifecyleObjectName);
        
        return lifecyleObjectName;
        
    }
    
    /**
     * return framework query
     * 
     * @param targetName
     * 
     * @return framework query interface
     */
    protected ComponentQuery getFrameworkComponentQuery(String targetName) {
        ComponentQuery componentQuery = null;
        /*
         * ==============================================================
         * According to IN=100359 at
         * http://inf.central.sun.com/inf/integrationReport.jsp?id=100359
         * 
         * The ComponentQueryImpl has been updated to support the "domain"
         * targetName as well, but is not plugged into the JBI Framework.
         * ==============================================================
         */
        // if (JBIAdminCommands.DOMAIN_TARGET_KEY.equals(targetName) == false) {
        // componentQuery = this.mEnvContext.getComponentQuery();
        // }
        componentQuery = this.mEnvContext.getComponentQuery(targetName);
        return componentQuery;
    }
    
    /**
     * checks the component name in the registry
     * 
     * @return true if component exists else false.
     * @param componentName
     *            name of the component
     * @param targetName
     */
    public boolean isExistingComponent(String componentName, String targetName) {
        List list = new ArrayList();
        ComponentQuery componentQuery = this
                .getFrameworkComponentQuery(targetName);
        if (componentQuery != null) {
            list = componentQuery
                    .getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        }
        return list.contains(componentName);
    }
    
    /**
     * check for shared namespace
     * 
     * @return true if it is namespace id else false.
     * @param sharedLibraryName
     *            id String
     * @param targetName
     */
    public boolean isExistingSharedLibrary(String sharedLibraryName,
            String targetName) {
        List list = new ArrayList();
        ComponentQuery componentQuery = this
                .getFrameworkComponentQuery(targetName);
        if (componentQuery != null) {
            list = componentQuery.getComponentIds(ComponentType.SHARED_LIBRARY);
        }
        return list.contains(sharedLibraryName);
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param slibName
     *            shared library name.
     * @param targetName
     */
    public Collection getComponentNamesDependentOnSharedLibrary(
            String slibName, String targetName) {
        List componentNames = new ArrayList();
        try {
            ComponentQuery componentQuery = this
                    .getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                componentNames = componentQuery
                        .getDependentComponentIds(slibName);
            }
        } catch (Exception ex) {
            // log exception
            logDebug(ex);
        }
        return componentNames;
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param saId
     *            service assembly name.
     * @param targetName
     */
    public Collection getComponentNamesDependentOnServiceAssembly(String saId,
            String targetName) {
        try {
            String[] componentNames = (String[]) this.invokeMBeanOperation(this
                    .getDeploymentServiceMBeanObjectName(targetName),
                    "getComponentsForDeployedServiceAssembly", saId);
            if (componentNames == null) {
                componentNames = new String[0];
            }
            return new HashSet(Arrays.asList(componentNames));
        } catch (Exception ex) {
            // log exception
            logDebug(ex);
            // empty set
            return new HashSet();
        }
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param frameworkCompType
     *            component type
     * @param frameworkCompStatus
     *            component state
     * @param targetName
     */
    public Set getComponentNamesWithStatus(ComponentType frameworkCompType,
            ComponentState frameworkCompStatus, String targetName) {
        List componentIdList = new ArrayList();
        ComponentQuery componentQuery = getFrameworkComponentQuery(targetName);
        if (componentQuery != null) {
            if (frameworkCompStatus == ComponentState.UNKNOWN) {
                // ANY STATE
                componentIdList = componentQuery
                        .getComponentIds(frameworkCompType);
            } else {
                componentIdList = componentQuery.getComponentIds(
                        frameworkCompType, frameworkCompStatus);
            }
        }
        
        // now retain only ones that has auID deployed.
        return new HashSet(componentIdList);
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param frameworkCompType
     *            component type
     * @param frameworkCompStatus
     *            component state
     * @param saName
     *            service assembly name
     * @param targetName
     */
    public Collection getComponentNamesDependentOnServiceAssembly(
            ComponentType frameworkCompType,
            ComponentState frameworkCompStatus, String saName, String targetName) {
        
        Set compNameSet = new HashSet(getComponentNamesWithStatus(
                frameworkCompType, frameworkCompStatus, targetName));
        Set saNameDepCompNameSet = new HashSet(
                this.getComponentNamesDependentOnServiceAssembly(saName,
                        targetName));
        // now retain only ids in the compIdSet that are in compIdSetWithAuId
        compNameSet.retainAll(saNameDepCompNameSet);
        return compNameSet;
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param frameworkCompType
     *            component type
     * @param frameworkCompStatus
     *            component state
     * @param slibName
     *            shared library name
     * @param targetName
     */
    public Collection getComponentNamesDependentOnSharedLibrary(
            ComponentType frameworkCompType,
            ComponentState frameworkCompStatus, String slibName,
            String targetName) {
        
        Set compNameSet = new HashSet(this.getComponentNamesWithStatus(
                frameworkCompType, frameworkCompStatus, targetName));
        Set slibNameDepCompNameSet = new HashSet(
                this.getComponentNamesDependentOnSharedLibrary(slibName,
                        targetName));
        // now retain only ids in the compIdSet that are in compIdSetWithSnsd
        compNameSet.retainAll(slibNameDepCompNameSet);
        return compNameSet;
    }
    
    /**
     * list of component names. this method requires non null inputs
     * 
     * @return list of component names.
     * @param frameworkCompType
     *            component type
     * @param frameworkCompStatus
     *            component state
     * @param slibName
     *            shared library name
     * @param saName
     *            service assembly name
     * @param targetName
     */
    public Collection getComponentNamesDependentOnSharedLibraryAndServiceAssembly(
            ComponentType frameworkCompType,
            ComponentState frameworkCompStatus, String slibName, String saName,
            String targetName) {
        
        Set compNameSet = new HashSet(this.getComponentNamesWithStatus(
                frameworkCompType, frameworkCompStatus, targetName));
        Set slibNameDepCompNameSet = new HashSet(
                this.getComponentNamesDependentOnSharedLibrary(slibName,
                        targetName));
        Set saNameDepCompNameSet = new HashSet(
                this.getComponentNamesDependentOnServiceAssembly(saName,
                        targetName));
        // intersection of SLIB and SA
        slibNameDepCompNameSet.retainAll(saNameDepCompNameSet);
        // intersection of type, status, SLIB, SA
        compNameSet.retainAll(slibNameDepCompNameSet);
        return compNameSet;
    }
    
    /**
     * updates the state in the service unit info
     * 
     * @param suInfo
     *            service unit info object
     * @param ObjectName
     *            deploymentServiceMBean
     */
    protected void updateServiceUnitInfoState(ServiceUnitInfo suInfo,
            ObjectName deploymentServiceMBean) {
        suInfo.setState(ServiceUnitInfo.UNKNOWN_STATE);
        
        String compName = suInfo.getDeployedOn();
        String suName = suInfo.getName();
        try {
            // locate the deployer mbean on the component
            if (deploymentServiceMBean == null) {
                this.logDebug("DeployerMBean not found for component "
                        + compName);
                return;
            }
            
            // get the state of the service unit from the deployer mbean
            // String getServiceUnitState(String componentName, String
            // serviceUnitName)
            Object[] params = new Object[2];
            params[0] = compName;
            params[1] = suName;
            
            String[] signature = new String[2];
            signature[0] = "java.lang.String";
            signature[1] = "java.lang.String";
            
            String frameworkState = (String) invokeMBeanOperation(
                    deploymentServiceMBean, "getServiceUnitState", params,
                    signature);
            
            this.logDebug("Framework State = " + frameworkState
                    + " for Service UNIT = " + suName);
            
            String uiState = AbstractUIMBeanImpl
                    .toUiServiceUnitState(frameworkState);
            
            suInfo.setState(uiState);
            
        } catch (Exception ex) {
            // ignore the exception and log it
            this.log(AbstractUIMBeanImpl.getI18NBundle().getMessage(
                    "ui.mbean.exception.getting.service.unit.state", suName,
                    compName, ex.getMessage()));
            this.logDebug(ex);
        }
    }
    
    /**
     * update the state in each service unit info in service assembly info
     * 
     * @param saInfo
     *            service assembly info object
     * @param targetName
     * @throws com.sun.jbi.ui.common.JBIRemoteException
     *             on error
     */
    protected void updateEachServiceUnitInfoStateInServiceAssemblyInfo(
            ServiceAssemblyInfo saInfo, String targetName)
            throws JBIRemoteException {
        List list = saInfo.getServiceUnitInfoList();
        if (list == null) {
            return;
        }
        
        // /////////////////////////////////////////////////////
        // As of Nov 09'06 the runtime team says:
        // To get the actual runtime state of a ServiceAssembly
        // invoke the getState() operation on the facade
        // DeploymentServiceMBean(). You should not be using the
        // DeployerMBean for anything, since you do not have
        // access to the remote instance DeployerMBeans and
        // it is not a standard MBean.
        //
        // Note that the DeploymentServiceMBean returns the
        // state as a String (see
        // javax.jbi.management.DeploymentServiceMBean for
        // the string values returned
        // {Started, Stopped, Shutdown}
        // TODO: Need to comment out the following code once the runtime
        // provides a way to get Service Unit State
        ObjectName deploymentServiceMBean = this
                .getDeploymentServiceMBeanObjectName(targetName);
        for (Iterator itr = list.iterator(); itr.hasNext();) {
            ServiceUnitInfo suInfo = (ServiceUnitInfo) itr.next();
            updateServiceUnitInfoState(suInfo, deploymentServiceMBean);
        }
        
        // /////////////////////////////////////////////////////
    }
    
    /**
     * update state in service assembly info
     * 
     * @param saInfo
     *            service assembly info
     * @param targetName
     * @throws com.sun.jbi.ui.common.JBIRemoteException
     *             on error
     */
    protected void updateServiceAssemblyInfoState(ServiceAssemblyInfo saInfo,
            String targetName) throws JBIRemoteException {
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        
        String saName = saInfo.getName();
        String frameworkState = FRAMEWORK_SA_ANY_STATE;
        // if (targetName.equals(JBIAdminCommands.DOMAIN_TARGET_KEY) == false) {
        try
        {
            frameworkState = (String) invokeMBeanOperation(
                    deploymentServiceObjectName, "getState", saName);
        } catch (Exception ex) {
            logDebug("issue 30: state invalid: " + ex.getMessage());
            frameworkState = "Unknown";
        }
        // }
        this.logDebug("Framework State = " + frameworkState
                + " for Service Assembly = " + saName);
        String uiState = AbstractUIMBeanImpl
                .toUiServiceAssemblyState(frameworkState);
        saInfo.setState(uiState);
    }
    
    /**
     * list of service assembly infos
     * 
     * @param frameworkState
     *            state
     * @param componentName
     *            name
     * @param targetName
     * @throws com.sun.jbi.ui.common.JBIRemoteException
     *             on error
     * @return list of service assembly infos
     */
    protected List getServiceAssemblyInfoList(String frameworkState,
            String componentName, String targetName) throws JBIRemoteException {
        String compName = null;
        
        if (componentName != null) {
            compName = componentName.trim();
        }
        
        String[] saNames = new String[0];
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        
        // filter them by component name
        if (compName == null || compName.length() <= 0) {
            this
                    .logDebug("getting all the deployed service assemblies with state "
                            + frameworkState);
            
            saNames = (String[]) getMBeanAttribute(deploymentServiceObjectName,
                    "DeployedServiceAssemblies");
        } else {
            this
                    .logDebug("getting all the deployed service assemblies for the comp "
                            + compName);
            
            if (false == this.isExistingComponent(compName, targetName)) {
                String[] args = { compName };
                Exception exception = this.createManagementException(
                        "ui.mbean.component.id.does.not.exist", args, null);
                throw new JBIRemoteException(exception);
                
            }
            
            saNames = (String[]) invokeMBeanOperation(
                    deploymentServiceObjectName,
                    "getDeployedServiceAssembliesForComponent", compName);
        }
        
        // construct service assembly infos from the descriptor
        
        List saInfoByCompNameList = new ArrayList();
        
        for (int i = 0; i < saNames.length; ++i) {
            String saName = saNames[i];
            logDebug("getting deployment descriptor for " + saName);
            String saDDText = (String) invokeMBeanOperation(
                    deploymentServiceObjectName,
                    "getServiceAssemblyDescriptor", saName);
            ServiceAssemblyInfo saInfo = ServiceAssemblyInfo
                    .createFromServiceAssemblyDD(new StringReader(saDDText));
            // update the state of the service assembly from runtime.
            this.updateServiceAssemblyInfoState(saInfo, targetName);
            
            saInfoByCompNameList.add(saInfo);
        }
        
        // && filter them by state.
        
        List saInfoList = new ArrayList();
        
        if (frameworkState.equalsIgnoreCase(this.FRAMEWORK_SA_ANY_STATE)) {
            saInfoList.addAll(saInfoByCompNameList);
            
        } else {
            // filter by specific state
            String uiState = this.toUiServiceAssemblyState(frameworkState);
            for (Iterator itr = saInfoByCompNameList.iterator(); itr.hasNext();) {
                ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) itr.next();
                if (uiState.equalsIgnoreCase(saInfo.getState())) {
                    saInfoList.add(saInfo);
                }
            }
        }
        
        // update Each ServiceUnit's State In ServiceAssembly;
        
        for (Iterator itr = saInfoList.iterator(); itr.hasNext();) {
            ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) itr.next();
            
            updateEachServiceUnitInfoStateInServiceAssemblyInfo(saInfo,
                    targetName);
        }
        
        return saInfoList;
        
    }
    
    /**
     * list of ui component infos
     * 
     * @return list of ui component infos
     * @param frameworkCompType
     *            type of the component
     * @param targetName
     */
    public List getUiComponentInfoList(ComponentType frameworkCompType,
            String targetName) throws JBIRemoteException {
        Set compNameSet = this.getComponentNamesWithStatus(frameworkCompType,
                ComponentState.UNKNOWN, targetName);
        List frameworkCompList = this.getFrameworkComponentInfoList(
                new ArrayList(compNameSet), targetName);
        List uiCompList = new ArrayList();
        for (Iterator itr = frameworkCompList.iterator(); itr.hasNext();) {
            ComponentInfo frameworkCompInfo = (ComponentInfo) itr.next();
            JBIComponentInfo uiCompInfo = toUiComponentInfo(frameworkCompInfo,
                    targetName);
            uiCompList.add(uiCompInfo);
        }
        return uiCompList;
    }
    
    /**
     * convert to ui component info list
     * 
     * @return ui component info list
     * @param frameworkCompList
     *            framework component info list
     */
    public List toUiComponentInfoList(List frameworkCompList, String targetName)
            throws JBIRemoteException {
        List uiCompList = new ArrayList();
        for (Iterator itr = frameworkCompList.iterator(); itr.hasNext();) {
            ComponentInfo frameworkCompInfo = (ComponentInfo) itr.next();
            JBIComponentInfo uiCompInfo = toUiComponentInfo(frameworkCompInfo,
                    targetName);
            uiCompList.add(uiCompInfo);
        }
        return uiCompList;
    }
    
    /**
     * return component info object
     * 
     * @return object
     * @param frameworkCompInfo
     *            framework component info
     * @param targetName
     */
    public JBIComponentInfo toUiComponentInfo(ComponentInfo frameworkCompInfo,
            String targetName) throws JBIRemoteException {
        
        String state = JBIComponentInfo.UNKNOWN_STATE;
        
        String componentName = frameworkCompInfo.getName();
        String componentDescription = frameworkCompInfo.getDescription();
        
        ComponentState componentStatus = ComponentState.UNKNOWN;
        ComponentType componentType = frameworkCompInfo.getComponentType();
        // Figure out the component state
        if ((false == ComponentType.BINDING.equals(componentType))
                && (false == ComponentType.ENGINE.equals(componentType))
                && (false == ComponentType.BINDINGS_AND_ENGINES
                        .equals(componentType))) {
            componentStatus = frameworkCompInfo.getStatus();
            state = toUiComponentInfoState(componentStatus);
        } else {
            // /////////////////////////////////////////////////////////
            // According to the runtime team (as of Nov 09'06):
            // The ComponentQuery.getStatus() or
            // ComponentQuery.getComponentInfo().getStatus()
            // to get the state of the component actually
            // returns the desired state of the component
            // persisted in the registry.
            //
            // The getCurrentState() operation of the
            // ComponentLifeCycleMBean returns the actual runtime
            // state for a component, which can be used to get the
            // actual runtime state of the component.
            // Therefore the component state is retrieved from the
            // facade ComponentLifeCycleMBean instead of
            // using ComponentQuery
            //
            // Use the LifeCycleMBean for the domain target too.
            // The getCurrentState() for the domain target should
            // return the most advanced state of the component on all targets.
            try {
                ObjectName lifeCycleMBeanObjectName = null;
                lifeCycleMBeanObjectName = this
                        .getComponentLifeCycleMBeanObjectName(componentName,
                                targetName);
                state = (String) this.getMBeanAttribute(
                        lifeCycleMBeanObjectName, "CurrentState");
            } catch (JBIRemoteException exception) {
                // do not rethrow it. The state is already defaulted to
                // JBIComponentInfo.UNKNOWN_STATE
                componentStatus = ComponentState.UNKNOWN;
                state = JBIComponentInfo.UNKNOWN_STATE;
            }
            // /////////////////////////////////////////////////////////
        }
        if (true == RUNNING_STATE.equals(state)) {
            state = JBIComponentInfo.STARTED_STATE;
        }
        
        String type = toUiComponentInfoType(componentType);

        // Get descripor string to greb version and build numbers out
        String componentVersion = "";
        String buildNumber = "";
        
        try {
            String descriptorXml = frameworkCompInfo.getInstallationDescriptor();
            InputSource inputSource = new InputSource(new StringReader(descriptorXml));
            String queryString = "";
            if ( ComponentType.BINDING.equals(componentType)
                || ComponentType.ENGINE.equals(componentType)
                || ComponentType.BINDINGS_AND_ENGINES.equals(componentType) ) {
                queryString = COMPONENT_VERSION_XPATH_QUERY;
            } else if ( ComponentType.SHARED_LIBRARY.equals(componentType) ) {
                queryString = SHARED_LIBRARY_VERSION_XPATH_QUERY;
            }

            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            xpath.setNamespaceContext(new IdentificationNSContext());

            Node rtnNode = (Node) xpath.evaluate(queryString,
                                                 inputSource,
                                                 XPathConstants.NODE);
            
            if (rtnNode != null) {
                NamedNodeMap attrs = rtnNode.getAttributes();
                Node verAttr = attrs.getNamedItem("specification-version");
                if (verAttr != null) {
                    componentVersion = verAttr.getNodeValue();
                }
                
                Node buildAttr = attrs.getNamedItem("build-number");
                if (buildAttr != null) {
                    buildNumber = buildAttr.getNodeValue();
                }
            } else {
                // Try the new version attr name
                inputSource = new InputSource(new StringReader(descriptorXml));
                xpath.setNamespaceContext(new IdentificationNewNSContext());
                rtnNode = (Node) xpath.evaluate(queryString,
                                                inputSource,
                                                XPathConstants.NODE);
                if (rtnNode != null) {
                    NamedNodeMap attrs = rtnNode.getAttributes();
                    Node verAttr = attrs.getNamedItem("component-version");
                    if (verAttr != null) {
                        componentVersion = verAttr.getNodeValue();
                    }
                    
                    Node buildAttr = attrs.getNamedItem("build-number");
                    if (buildAttr != null) {
                        buildNumber = buildAttr.getNodeValue();
                    }
                } 
            }
        } catch (Exception exp) {
            throw new JBIRemoteException(exp);
            
        } 

        JBIComponentInfo compInfo = new JBIComponentInfo(type, // Integer.toString(comp.getComponentType()),
                state, // Integer.toString(comp.getStatus()),
                componentName, componentDescription);
        compInfo.setComponentVersion(componentVersion);
        compInfo.setBuildNumber(buildNumber);

        return compInfo;
    }
    
    /**
     * converts to deployment service service assembly state.
     * 
     * @return state
     * @param uiState
     *            state
     */
    public static String toFrameworkServiceAssemblyState(String uiState) {
        if (uiState == null) {
            return FRAMEWORK_SA_ANY_STATE;
        }
        
        String saState = uiState.trim();
        
        if (saState.length() <= 0) {
            return FRAMEWORK_SA_ANY_STATE;
        }
        
        if (ServiceAssemblyInfo.STARTED_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_STARTED_STATE;
        } else if (ServiceAssemblyInfo.STOPPED_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_STOPPED_STATE;
        } else if (ServiceAssemblyInfo.SHUTDOWN_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_SHUTDOWN_STATE;
        } else { // any other value
            return FRAMEWORK_SA_ANY_STATE;
        }
    }
    
    /**
     * converts to ui service assembly state
     * 
     * @return state
     * @param frameworkState
     *            state
     */
    public static String toUiServiceAssemblyState(String frameworkState) {
        String uiState = ServiceAssemblyInfo.UNKNOWN_STATE;
        
        if (frameworkState == null) {
            return ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        
        String saState = frameworkState.trim();
        
        if (saState.length() <= 0) {
            return ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        
        if (FRAMEWORK_SA_STARTED_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.STARTED_STATE;
        } else if (FRAMEWORK_SA_STOPPED_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.STOPPED_STATE;
        } else if (FRAMEWORK_SA_SHUTDOWN_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.SHUTDOWN_STATE;
        } else {
            uiState = ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        return uiState;
    }
    
    /**
     * converts to ui service unit state
     * 
     * @return state
     * @param frameworkState
     *            state
     */
    public static String toUiServiceUnitState(String frameworkState) {
        String uiState = ServiceUnitInfo.UNKNOWN_STATE;
        
        if (frameworkState == null) {
            return ServiceUnitInfo.UNKNOWN_STATE;
        }
        
        String suState = frameworkState.trim();
        
        if (suState.length() <= 0) {
            return ServiceUnitInfo.UNKNOWN_STATE;
        }
        
        if (FRAMEWORK_SU_STARTED_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.STARTED_STATE;
        } else if (FRAMEWORK_SU_STOPPED_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.STOPPED_STATE;
        } else if (FRAMEWORK_SU_SHUTDOWN_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.SHUTDOWN_STATE;
        } else {
            uiState = ServiceUnitInfo.UNKNOWN_STATE;
        }
        return uiState;
    }
    
    /**
     * converts state
     * 
     * @return state
     * @param frameworkCompState
     *            state
     */
    public static String toUiComponentInfoState(
            ComponentState frameworkCompState) {
        String uiState = JBIComponentInfo.UNKNOWN_STATE;
        switch (frameworkCompState) {
            case SHUTDOWN:
                uiState = JBIComponentInfo.SHUTDOWN_STATE;
                break;
            case STARTED:
                uiState = JBIComponentInfo.STARTED_STATE;
                break;
            case STOPPED:
                uiState = JBIComponentInfo.STOPPED_STATE;
                break;
            default:
                uiState = JBIComponentInfo.UNKNOWN_STATE;
                break;
        }
        return uiState;
    }
    
    /**
     * converts type
     * 
     * @return int type
     * @param uiCompType
     *            type
     */
    public static ComponentType toFrameworkComponentInfoType(String uiCompType) {
        if (JBIComponentInfo.BINDING_TYPE.equalsIgnoreCase(uiCompType)) {
            return ComponentType.BINDING;
        } else if (JBIComponentInfo.ENGINE_TYPE.equalsIgnoreCase(uiCompType)) {
            return ComponentType.BINDING;
        } else if (JBIComponentInfo.SHARED_LIBRARY_TYPE
                .equalsIgnoreCase(uiCompType)) {
            return ComponentType.SHARED_LIBRARY;
        } else {
            return ComponentType.BINDINGS_AND_ENGINES;
        }
    }
    
    /**
     * converts type
     * 
     * @return type
     * @param frameworkCompType
     *            type
     */
    public static String toUiComponentInfoType(ComponentType frameworkCompType) {
        String uiType = JBIComponentInfo.UNKNOWN_TYPE;
        switch (frameworkCompType) {
            case BINDING:
                uiType = JBIComponentInfo.BINDING_TYPE;
                break;
            case ENGINE:
                uiType = JBIComponentInfo.ENGINE_TYPE;
                break;
            case SHARED_LIBRARY:
                uiType = JBIComponentInfo.SHARED_LIBRARY_TYPE;
                break;
            default:
                uiType = JBIComponentInfo.UNKNOWN_TYPE;
                break;
        }
        return uiType;
    }
    
    /**
     * validates the state value component info
     * 
     * @state component state. valid values null, "", "started", "stopped",
     *        "shutdown"
     * @throws JBIRemoteException
     *             on validation failure
     */
    public void validateUiJBIComponentInfoState(String state)
            throws JBIRemoteException {
        if (state == null || state.length() == 0) {
            // all states
            return;
        }
        String stateValue = state.toString();
        
        if (!(JBIComponentInfo.STARTED_STATE.equalsIgnoreCase(stateValue)
                || JBIComponentInfo.STOPPED_STATE.equalsIgnoreCase(stateValue)
                || JBIComponentInfo.SHUTDOWN_STATE.equalsIgnoreCase(stateValue) || JBIComponentInfo.UNKNOWN_STATE
                .equalsIgnoreCase(stateValue))) {
            String[] args = { state };
            Exception exception = this.createManagementException(
                    "ui.mbean.list.error.invalid.state", args, null);
            throw new JBIRemoteException(exception);
            
        }
        
    }
    
    /**
     * validates the state value for service assembly
     * 
     * @state component state. valid values null, "", "started", "stopped",
     *        "shutdown"
     * @throws JBIRemoteException
     *             on validation failure
     */
    public void validateUiServiceAssemblyInfoState(String state)
            throws JBIRemoteException {
        if (state == null || state.length() == 0) {
            // all sates
            return;
        }
        String stateValue = state;
        
        if (!(ServiceAssemblyInfo.STARTED_STATE.equalsIgnoreCase(stateValue)
                || ServiceAssemblyInfo.STOPPED_STATE
                        .equalsIgnoreCase(stateValue)
                || ServiceAssemblyInfo.SHUTDOWN_STATE
                        .equalsIgnoreCase(stateValue) || ServiceAssemblyInfo.UNKNOWN_STATE
                .equalsIgnoreCase(stateValue))) {
            String[] args = { state };
            Exception exception = this.createManagementException(
                    "ui.mbean.list.error.invalid.state", args, null);
            throw new JBIRemoteException(exception);
            
        }
    }
    
    // //////////////////////////////////////////////////////
    // -- Adjusted for ComponentType and ComponentState --
    // //////////////////////////////////////////////////////
    /**
     * framework states
     * 
     * @return state
     * @param uiCompState
     *            state
     */
    public static ComponentState toFrameworkComponentInfoState(
            String uiCompState) {
        if (uiCompState == null) {
            return ComponentState.UNKNOWN;
        }
        
        String compState = uiCompState.trim();
        
        if (compState.length() <= 0) {
            return ComponentState.UNKNOWN;
        }
        
        if (JBIComponentInfo.SHUTDOWN_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.SHUTDOWN;
        } else if (JBIComponentInfo.STARTED_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.STARTED;
        } else if (JBIComponentInfo.STOPPED_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.STOPPED;
        } else { // any other value
            return ComponentState.UNKNOWN;
        }
    }
    
    /**
     * list of componentinfo objects. this methods check for valid inputs and
     * take only a valid inputs. for example slib, saName both can be null
     * 
     * @param frameworkCompType
     *            component type.
     * @param frameworkCompState
     *            component state
     * @param sharedLibraryName
     *            shared library name
     * @param serviceAssemblyName
     *            service assembly name
     * @param targetName
     * @throws com.sun.jbi.ui.common.JBIRemoteException
     *             on error
     * @return list of component info
     */
    protected List getFrameworkComponentInfoListForEnginesAndBindings(
            ComponentType frameworkCompType, ComponentState frameworkCompState,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws JBIRemoteException {
        String slibName = null;
        String saName = null;
        
        if (sharedLibraryName != null && sharedLibraryName.trim().length() > 0) {
            slibName = sharedLibraryName.trim();
        }
        
        if (serviceAssemblyName != null
                && serviceAssemblyName.trim().length() > 0) {
            saName = serviceAssemblyName.trim();
        }
        
        logDebug("getFrameworkComponentInfoForEnginesAndBindings : Params : "
                + frameworkCompType + ", " + frameworkCompState + ", "
                + slibName + ", " + saName);
        
        Collection componentNames = new HashSet();
        // there are 4 options with snsId,auId (00,01,10,11)
        if (slibName == null && saName == null) {
            // 00
            componentNames = getComponentNamesWithStatus(frameworkCompType,
                    frameworkCompState, targetName);
        } else if (slibName == null && saName != null) {
            // 01
            componentNames = getComponentNamesDependentOnServiceAssembly(
                    frameworkCompType, frameworkCompState, saName, targetName);
            
        } else if (slibName != null && saName == null) {
            // 10
            componentNames = getComponentNamesDependentOnSharedLibrary(
                    frameworkCompType, frameworkCompState, slibName, targetName);
            
        } else if (slibName != null && saName != null) {
            // 11
            componentNames = getComponentNamesDependentOnSharedLibraryAndServiceAssembly(
                    frameworkCompType, frameworkCompState, slibName, saName,
                    targetName);
            
        } else {
            // not possible.
            componentNames = new HashSet();
        }
        
        return this.getFrameworkComponentInfoList(componentNames, targetName);
    }
    
    /**
     * framework component info list
     * 
     * @param compNameList
     *            list of component names
     * @param targetName
     * @return framework component info list
     */
    public List getFrameworkComponentInfoList(Collection compNameList,
            String targetName) {
        ArrayList compInfoList = new ArrayList();
        try {
            ComponentQuery componentQuery = getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                for (Iterator itr = compNameList.iterator(); itr.hasNext();) {
                    ComponentInfo componentInfo = componentQuery
                            .getComponentInfo((String) itr.next());
                    if (componentInfo != null) {
                        compInfoList.add(componentInfo);
                    }
                }
            }
        } catch (Exception ex) {
            // TODO propagate the exception to client.
            logDebug(ex);
        }
        return compInfoList;
    }
    
    /**
     * this methods check for valid inputs and take only a valid inputs. for
     * example slib, saName both can be null
     * 
     * @param componentName
     *            name
     * @param targetName
     * @throws com.sun.jbi.ui.common.JBIRemoteException
     *             on error
     * @return list of componentInfo objects
     */
    protected List getFrameworkComponentInfoListForSharedLibraries(
            String componentName, String targetName) throws JBIRemoteException {
        String compName = null;
        
        if (componentName != null && componentName.trim().length() > 0) {
            compName = componentName.trim();
        }
        
        logDebug("getFrameworkComponentInfoForSharedLibraries: Params : "
                + compName);
        
        List slibNames = new ArrayList();
        List uiCompInfoList = new ArrayList();
        List frameworkCompInfoList = new ArrayList();
        ComponentQuery componentQuery = null;
        if (compName == null) {
            logDebug("Listing All Shared Libraries in the schemaorg_apache_xmlbeans.system");
            componentQuery = this.getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                slibNames = componentQuery
                        .getComponentIds(ComponentType.SHARED_LIBRARY);
            }
        } else {
            logDebug("Listing Shared Libraries for the component " + compName);
            componentQuery = this.getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                ComponentInfo componentInfo = componentQuery
                        .getComponentInfo(compName);
                if (componentInfo == null) {
                    String[] args = { compName };
                    Exception exception = this.createManagementException(
                            "ui.mbean.component.id.does.not.exist", args, null);
                    throw new JBIRemoteException(exception);
                    
                }
                slibNames = componentInfo.getSharedLibraryNames();
            }
        }
        
        frameworkCompInfoList = this
                .getFrameworkComponentInfoListForSharedLibraryNames(slibNames,
                        targetName);
        
        return frameworkCompInfoList;
        
    }
    
    /**
     * list of component infos
     * 
     * @param targetName
     * @return list of component infos
     * @param slibNameList
     *            shared library names
     */
    public List getFrameworkComponentInfoListForSharedLibraryNames(
            Collection slibNameList, String targetName) {
        ArrayList compInfoList = new ArrayList();
        try {
            ComponentQuery componentQuery = this
                    .getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                for (Iterator itr = slibNameList.iterator(); itr.hasNext();) {
                    ComponentInfo componentInfo = componentQuery
                            .getSharedLibraryInfo((String) itr.next());
                    if (componentInfo != null) {
                        compInfoList.add(componentInfo);
                    }
                }
            }
        } catch (Exception ex) {
            // TODO propagate the exception to client.
            logDebug(ex);
        }
        return compInfoList;
    }
    
    /**
     * Retrieve the Component Installation descriptor for the server target.
     * 
     * The Installation descriptor will never be different for a component name
     * in a JBI schemaorg_apache_xmlbeans.system because there's only one component allowed with that same
     * name. Therefore, this implies a registry lookup for the server target.
     * 
     * @param Component
     *            name as a string
     * @param targetName
     * @return the Component Installation descriptor as a string
     * @throws JBIRemoteException
     *             on error
     */
    public String getInstallationDescriptor(String componentName,
            String targetName) {
        ComponentQuery componentQuery = null;
        ComponentInfo componentInfo = null;
        String jbiXml = null;
        
        componentQuery = this.mEnvContext.getComponentQuery(targetName);
        if (componentQuery != null) {
            componentInfo = componentQuery.getComponentInfo(componentName);
            if (componentInfo != null) {
                jbiXml = componentInfo.getInstallationDescriptor();
            }
        }
        return jbiXml;
    }
    
    /**
     * Retrieve the Shared Library Installation descriptor for the server
     * target.
     * 
     * The Installation descriptor will never be different for a library name in
     * a JBI schemaorg_apache_xmlbeans.system because there's only one library allowed with that same
     * name. Therefore, this implies a registry lookup for the domain target.
     * 
     * @param libraryName
     *            name as a string
     * @param targetName
     * @return the Library Installation descriptor as a string
     * @throws JBIRemoteException
     *             on error
     */
    public String getSharedLibraryDescriptor(String libraryName,
            String targetName) {
        ComponentQuery componentQuery = null;
        ComponentInfo componentInfo = null;
        String jbiXml = null;
        
        componentQuery = this.mEnvContext.getComponentQuery(targetName);
        if (componentQuery != null) {
            componentInfo = componentQuery.getSharedLibraryInfo(libraryName);
            if (componentInfo != null) {
                jbiXml = componentInfo.getInstallationDescriptor();
            }
        }
        return jbiXml;
    }

    /*
     * Inner class defined for component identification namespace
     */
    class IdentificationNSContext implements NamespaceContext
    {

        /**
         * This method maps the prefix jbi and config to the right URIs
         */
        public String getNamespaceURI(String prefix)
        {
            if (prefix.equals("jbi"))
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
            else if (prefix.equals("identification"))
            {
                return COMPONENT_VERSION_NS;
            }
            else
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
        }

        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        public Iterator getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }
    }

    /*
     * Inner class defined for component identification namespace
     */
    class IdentificationNewNSContext implements NamespaceContext
    {

        /**
         * This method maps the prefix jbi and config to the right URIs
         */
        public String getNamespaceURI(String prefix)
        {
            if (prefix.equals("jbi"))
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
            else if (prefix.equals("identification"))
            {
                return COMPONENT_VERSION_NS_NEW;
            }
            else
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
        }

        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        public Iterator getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }
    }
}
