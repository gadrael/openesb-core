/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)VerifierUtils.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.verifier.util;


import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;

import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JarFactory;
import com.sun.jbi.ui.common.JBIArchive;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.ServiceAssemblyDD;
import com.sun.jbi.ui.common.ToolsLogManager;
import com.sun.jbi.ui.runtime.verifier.LocalStringKeys;
import com.sun.jbi.ui.runtime.verifier.VerifierException;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * This class has utility methods for the Verifier
 */
public class VerifierUtils
{
    /** R/W buffer size for copying archive files. */
    private static final int BUFFER_SIZE = 8 * 1024;
    
    /**
     * resource bundle
     */
    private I18NBundle mResourceBundle;
    
    /** The namespaceURI represented by the prefix <code>xmlns</code>. */
    public static final String NS_URI_XMLNS = "http://www.w3.org/2000/xmlns/";
    
    /** su descruptor path **/
    private static final String SU_DESC_PATH = "META-INF" + File.separator + "jbi.xml";
    
    /**interface name **/
    private static final String INTERFACE_NAME = "interface-name";
            
    /**endpoint name **/
    private static final String ENDPOINT_NAME = "endpoint-name";
    
    /**service name **/
    private static final String SERVICE_NAME = "service-name";
    
    /**name attribute **/
    private static final String NAME = "name";
    
    /** target ns */
    private static final String TARGET_NS = "targetNamespace";
    
    /** sun-javaee-engine */
    private static final String JAVAEE_SERVICE_ENGINE = "sun-javaee-engine";
    
    /** WSDL version number constant: WSDL 1.1 */
    private static final short WSDL11 = 11;
    
    /** WSDL version number constant: WSDL 2.0 */
    private static final short WSDL20 = 20;
   
    /** Constructor */
    public VerifierUtils (I18NBundle resourceBundle)
    {
        this.mResourceBundle = resourceBundle;
    }
  
    /**
     * This method is used to cleanup the contents of the verifier tmp dir
     * @param tmpDir the temp dir
     */
    public boolean cleanup(String tmpDir)
    {
        return cleanDirectory(new File(tmpDir));
    }
   
    /**
     * Removes all files and child directories in the specified directory.
     * @return false if unable to delete the file
     */
    private boolean cleanDirectory(File dir)
    {
        File[] tmps = dir.listFiles();
        for (File tmp : tmps) {
            if (tmp.isDirectory()) {
                // -- Even if a single file / dir in a parent folder cannot be deleted
                // -- break the recursion as there is no point in continuing the loop
                if (!cleanDirectory(tmp)) {
                    return false;
                }
            }
            if (!tmp.delete()) {
                return false;
            }
        }
        return true;
    }   
 
    /**
     * This method is used to get a list of WSDL files in a SA
     * @param saDir the dir where the SA is extracted
     * @returns Map<String, List> a map of suName and list of WSDLs in the SU
     * @throws VerifierException if the wsdl list could not be obtained
     */
     public Map<String, File[]> getWSDLs(String saDirPath)
     throws VerifierException
     {
         Map<String, File[]> wsdlMap = new HashMap();
         try
         {
             File saDir = new File(saDirPath);
             if (!saDir.exists())
             {
                 throw new VerifierException(
                         mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_SA_NOT_EXTRACTED));
             }
             File[] sus = saDir.listFiles();
             for (File su : sus) {
                 StringBuffer logInfo = new StringBuffer();
                 logInfo.append("WSDLs in " + su.getName() + ": ");
                 if (su.isDirectory()) {
                     List<File> wsdls = getWSDLFiles(su);
                     for(File wsdl: wsdls)
                     {
                         logInfo.append(wsdl.getName());
                     }ToolsLogManager.getRuntimeLogger().finer(logInfo.toString());
                     wsdlMap.put(su.getName(), (File[])wsdls.toArray(new File[wsdls.size()]));
                 }
             }
             return wsdlMap;
         }
         catch (Exception ex)
         {
             throw new VerifierException(ex);
         }
     }

    /**
     * This method is used to get a list of WSDL files in a SA
     * @param archive the JBIArchive object
     * @returns Map<String, String[]> a map of suName and list of WSDLs in the SU Jar
     * @throws VerifierException if the wsdl list could not be obtained
     */
     public Map<String, String[]> getWSDLs(JBIArchive archive)
     throws VerifierException
     {
         Map<String, String[]> wsdlMap = new HashMap();

         try
         {
             JarFile saJar = new JarFile(archive.getJarFile().getName());
             if (!(new File(saJar.getName())).exists())
             {
                 throw new VerifierException(
                         mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_INVALID_SA));
             }

             ServiceAssemblyDD saDesc =
                  (ServiceAssemblyDD) archive.getJbiDescriptor();
             List<ServiceAssemblyDD.ServiceUnitDD> suDescs =
                  saDesc.getServiceUnitDDList();
             for(ServiceAssemblyDD.ServiceUnitDD suDesc: suDescs)
             {
                String suName = suDesc.getName();
                String suArtifactZipName = suDesc.getArtifactZipName();
                ZipEntry suZipEntry = saJar.getEntry(suArtifactZipName);
                if (suZipEntry == null)
                {
                    throw new VerifierException(
                            mResourceBundle.getMessage(
                            LocalStringKeys.VERIFIER_SU_ARTIFACT_MISSING,
                            new Object[] { suArtifactZipName }));
                }

                ZipInputStream suEntryInStream =
                    new ZipInputStream(saJar.getInputStream(suZipEntry));
                StringBuffer logInfo = new StringBuffer();
                logInfo.append("WSDLs in " + suName + ": ");
                ZipEntry wsdlZipEntry = null;

                List<String> wsdls = new ArrayList();
                while ((suEntryInStream != null) &&
                       (wsdlZipEntry = suEntryInStream.getNextEntry()) != null)
                {
                    if (wsdlZipEntry.getName().endsWith(".wsdl"));
                    {
                        int i = 0;
                        byte[] buffer = new byte[512];
                        ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
                        while ( (i = suEntryInStream.read(buffer)) > 0)
                        {
                            outBuffer.write(buffer, 0, i);
                        }
                        wsdls.add(new String(outBuffer.toString()));
                        outBuffer.close();
                    }
                }
                suEntryInStream.close();
                ToolsLogManager.getRuntimeLogger().finer(logInfo.toString());
                wsdlMap.put(
                    suName, 
                    (String[])wsdls.toArray(new String[wsdls.size()]));
             }

             saJar.close();
             return wsdlMap;
         }
         catch (Exception ex)
         {
             throw new VerifierException(ex);
         }
     }

     public Map<String, String> getSUJarEntryMap(JBIArchive archive)
     throws VerifierException
     {
         Map<String, String> suNameToSUJarEntryMap = new HashMap();

         try
         {
             JarFile saJar = new JarFile(archive.getJarFile().getName());
             if (!(new File(saJar.getName())).exists())
             {
                 throw new VerifierException(
                         mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_INVALID_SA));
             }

             ServiceAssemblyDD saDesc =
                  (ServiceAssemblyDD) archive.getJbiDescriptor();
             List<ServiceAssemblyDD.ServiceUnitDD> suDescs =
                  saDesc.getServiceUnitDDList();
             for(ServiceAssemblyDD.ServiceUnitDD suDesc: suDescs)
             {
                suNameToSUJarEntryMap.put(suDesc.getName(),
                                          suDesc.getArtifactZipName());
             }

             saJar.close();
             return suNameToSUJarEntryMap;
         }
         catch (Exception ex)
         {
             throw new VerifierException(ex);
         }
     }

     /**
      * This method is used to return a list of config names used in the given
      * SU
      * @param suDir the dir where the SU is extracted
      * @return Map map of endpoint names against config objects
      */
     public Map<String, List<String>> getEndpointConfigMap(String suDir)
     throws VerifierException
     {
         Map<String, List<String>> configMap = new HashMap();
         try
         {
             File descriptor = new File(suDir, SU_DESC_PATH);
             if (!descriptor.exists())
             {
                 throw new VerifierException(
                         mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_SA_NOT_EXTRACTED));
             }
             InputSource inputSource =
                    new InputSource(new FileInputStream(descriptor));

             List endpointNames = new ArrayList();

             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();
             //inner class for namespace prefix to uri mapping
             xpath.setNamespaceContext(new WSDLNamespaceContext());

             NodeList consumes = (NodeList) xpath.evaluate(
                     "/jbi:jbi/jbi:services/jbi:consumes",
                     inputSource,
                     XPathConstants.NODESET);
             //create a new input stream otherwise there will be a read error
             inputSource =
                    new InputSource(new FileInputStream(descriptor));
             NodeList provides = (NodeList) xpath.evaluate(
                     "/jbi:jbi/jbi:services/jbi:provides",
                     inputSource,
                     XPathConstants.NODESET);

             for (int i=0; i<consumes.getLength(); i++)
             {
                 configMap.putAll(getConfigsUsedInNode(consumes.item(i)));
             }
             for (int i=0; i<provides.getLength(); i++)
             {
                 configMap.putAll(getConfigsUsedInNode(provides.item(i)));
             }
             return configMap;
         }
         catch (javax.xml.xpath.XPathExpressionException xpathEx)
         {
            throw new VerifierException(xpathEx.toString());
         }
         catch (java.io.IOException ioEx)
         {
            throw new VerifierException(ioEx.toString());
         }      
         catch (Exception ex)
         {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
         }
     }
 
     /**
      * This method is used to return a list of config names used in the given 
      * SU
      * @param not know yet
      * @return Map map of endpoint names against config objects
      */
     public Map<String, List<String>> getEndpointConfigMap(JBIArchive archive, String suJarEntryName)
     throws VerifierException
     {
         Map<String, List<String>> configMap = new HashMap();
         try
         {
             JarFile saJar = new JarFile(archive.getJarFile().getName());
             if (!(new File(saJar.getName())).exists())
             {
                 throw new VerifierException(
                         mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_INVALID_SA));
             }

             ZipEntry suZipEntry = saJar.getEntry(suJarEntryName);
             if (suZipEntry == null)
             {
                 throw new VerifierException(
                            mResourceBundle.getMessage(
                            LocalStringKeys.VERIFIER_SU_ARTIFACT_MISSING,
                            new Object[] { suJarEntryName }));
             }

             ZipInputStream suEntryInStream =
                    new ZipInputStream(saJar.getInputStream(suZipEntry));
             ZipEntry ddZipEntry = null;
             String outputString = null;
             while ((suEntryInStream != null) &&
                       (ddZipEntry = suEntryInStream.getNextEntry()) != null)
             {
                 if (ddZipEntry.getName().endsWith("jbi.xml"))
                 {
                     int i = 0;
                     byte[] buffer = new byte[512];
                     ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
                     while ( (i = suEntryInStream.read(buffer)) > 0)
                     {
                        outBuffer.write(buffer, 0, i);
                     }
                     outputString = new String(outBuffer.toString());
                     outBuffer.close();
                     break;
                 }
             }
             suEntryInStream.close();

             if (outputString == null)
             {
                  throw new VerifierException(
                            mResourceBundle.getMessage(
                            LocalStringKeys.VERIFIER_SU_ARTIFACT_MISSING,
                            new Object[] { suJarEntryName }));
             }
             InputSource inputSource =
                         new InputSource(new StringReader(outputString));

             List endpointNames = new ArrayList();
             
             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();
             //inner class for namespace prefix to uri mapping
             xpath.setNamespaceContext(new WSDLNamespaceContext());

             NodeList consumes = (NodeList) xpath.evaluate(
                     "/jbi:jbi/jbi:services/jbi:consumes",
                     inputSource,
                     XPathConstants.NODESET);           
             //create a new input stream otherwise there will be a read error 
             inputSource = new InputSource(new StringReader(outputString));              
             NodeList provides = (NodeList) xpath.evaluate(
                     "/jbi:jbi/jbi:services/jbi:provides",
                     inputSource,
                     XPathConstants.NODESET);   
             
             for (int i=0; i<consumes.getLength(); i++)
             {
                 configMap.putAll(getConfigsUsedInNode(consumes.item(i)));
             }
             for (int i=0; i<provides.getLength(); i++)
             {
                 configMap.putAll(getConfigsUsedInNode(provides.item(i)));
             }
             
             saJar.close();
             return configMap;
         }
         catch (javax.xml.xpath.XPathExpressionException xpathEx)
         {
            throw new VerifierException(xpathEx.toString());
         }
         catch (java.io.IOException ioEx)
         {
            throw new VerifierException(ioEx.toString());
         }       
         catch (Exception ex)
         {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
         }             
     }
     
     /**
      * This method is used to get a list of config objects used in the given node
      * @param Node the node 
      * @return Map<String, List<String>> endpoint name mapped to config list
      */
     private Map<String, List<String>> getConfigsUsedInNode(Node node)
     throws VerifierException
     {
      
         try
         {
             Map<String, List<String>> configMap = 
                     new HashMap<String, List<String>>();
             String URI = "";
             
             String interfaceName = getAttributeValue(node, INTERFACE_NAME);
             if (interfaceName != null)
             {
                String prefix = interfaceName.substring(0, interfaceName.indexOf(":"));
                URI = node.lookupNamespaceURI(prefix);
             }

             List configList = new ArrayList();                 

             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();
             //inner class for namespace prefix to uri mapping
             xpath.setNamespaceContext(new WSDLNamespaceContext());

             NodeList config = (NodeList)xpath.evaluate(
                     "config:application-config",
                     node,
                     XPathConstants.NODESET);
                     
             for(int j=0; j<config.getLength(); j++)
             {
                configList.add(getAttributeValue(config.item(j), NAME));
             }
             
             String serviceName = null;
             String epName = null;
             
             serviceName = getAttributeValue(node, SERVICE_NAME);
             if (serviceName != null)
             {
                //get rid of the prefix                 
                 serviceName = serviceName.substring(serviceName.indexOf(":")+1);
             }
             
             epName = getAttributeValue(node, ENDPOINT_NAME);


             String endpointName = 
                     createEndpointName(URI, serviceName, epName);
            ToolsLogManager.getRuntimeLogger().log(Level.FINER, "Adding config list to endpoint {0}{1}", new Object[]{endpointName, configList});                 
            configMap.put(endpointName, configList);
            return configMap;
         }
        catch (javax.xml.xpath.XPathExpressionException xpathEx)
        {
            throw new VerifierException(xpathEx.toString());
        }
        catch (Exception ex)
        {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
        }                 
      
     }

     /**
      * This method is used to retrieve a list of all .wsdl files in a
      * dir
      * @param dir the root dir
      * @returns List<Files> the list of files that end with .wsdl
      */
     private List<File> getWSDLFiles(File dir)
     {
         List wsdlFiles = new ArrayList();
         File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                wsdlFiles.addAll(getWSDLFiles(file));
            } else if (file.getName().endsWith(".wsdl")) {
                wsdlFiles.add(file);
            }
        }
        return wsdlFiles;
     }

     /**
      * This method is used to get the list of endpoint names from a wsdl file
      * @param File the wsdl file
      * @returns String[] the endpoint names
      * @throws VerifierException if the endpointName could not be obtained
      */
     public String[] getEndpointName(File wsdlFile)
     throws VerifierException
     {
         try
         {
             List endpointNames = new ArrayList();
             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();

             InputSource inputSource =
                    new InputSource(new FileInputStream(wsdlFile));
             short wsdlVer = getWSDLVersionNumber(wsdlFile);

             //inner class for namespace prefix to uri mapping
             NodeList ports = null;
             if (wsdlVer == WSDL11)
             {
                 xpath.setNamespaceContext(new WSDL11NamespaceContext());

                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-11:definitions/wsdl-11:service/wsdl-11:port",
                     inputSource,
                     XPathConstants.NODESET);
             }
             else if (wsdlVer == WSDL20)
             {
                 xpath.setNamespaceContext(new WSDL20NamespaceContext());
                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-20:description/wsdl-20:service/wsdl-20:endport",
                     inputSource,
                     XPathConstants.NODESET);
             }
             else
             {
                 throw new VerifierException(
                     mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_WRONG_WSDL_VERSION));
             }

             String serviceName = getServiceName(wsdlFile);

             String namespaceURI = null;
             if (ports.getLength() > 0)
             {
                 Node port = ports.item(0);
                 Document doc = port.getOwnerDocument();
                 Element definitions = doc.getDocumentElement();
                 namespaceURI = definitions.getAttribute(TARGET_NS);
             }

             int length = ports.getLength();
             for (int i=0; i<length; i++)
             {
                 String endpointName =
                         createEndpointName(
                             namespaceURI,
                             serviceName,
                             getAttributeValue(
                                ports.item(i),
                                "name"));
                ToolsLogManager.getRuntimeLogger().log(Level.FINER, "Adding endpoint {0}", endpointName);
                endpointNames.add(endpointName);
             }

             String[] endpoints = new String[endpointNames.size()];
             endpointNames.toArray(endpoints);
             return endpoints;

        }
        catch (javax.xml.xpath.XPathExpressionException xpathEx)
        {
            throw new VerifierException(xpathEx.toString());
        }
        catch (java.io.IOException ioEx)
        {
            throw new VerifierException(ioEx.toString());
        }      
        catch (Exception ex)
        {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
        }

     }

     /**
      * This method is used to get the list of endpoint names from a wsdl file
      * @param wsdlString the wsdl string
      * @returns String[] the endpoint names
      * @throws VerifierException if the endpointName could not be obtained
      */
     public String[] getEndpointName(String wsdlString)
     throws VerifierException
     {
         try
         {
             short wsdlVer = getWSDLVersionNumber(wsdlString);

             List endpointNames = new ArrayList();
             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();
            
             //inner class for namespace prefix to uri mapping
             NodeList ports = null;
             InputSource inputSource =
                     new InputSource(new StringReader(wsdlString));

             if (wsdlVer == WSDL11)
             {
                 xpath.setNamespaceContext(new WSDL11NamespaceContext());
                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-11:definitions/wsdl-11:service/wsdl-11:port",
                     inputSource,
                     XPathConstants.NODESET);
             }
             else if (wsdlVer == WSDL20)
             {
                 xpath.setNamespaceContext(new WSDL20NamespaceContext());
                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-20:description/wsdl-20:service/wsdl-20:endport",
                     inputSource,
                     XPathConstants.NODESET);
             }
             else
             {
                 throw new VerifierException(
                     mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_WRONG_WSDL_VERSION));
             }

             String serviceName = getServiceName(wsdlString);
            
             String namespaceURI = null;
             if (ports.getLength() > 0)
             {
                 Node port = ports.item(0);
                 Document doc = port.getOwnerDocument();
                 Element definitions = doc.getDocumentElement();
                 namespaceURI = definitions.getAttribute(TARGET_NS);
             }
             
             int length = ports.getLength();
             for (int i=0; i<length; i++)
             {
                 String endpointName = 
                         createEndpointName(
                             namespaceURI,
                             serviceName,
                             getAttributeValue(
                                ports.item(i), 
                                "name"));
                ToolsLogManager.getRuntimeLogger().log(Level.FINER, "Adding endpoint {0}", endpointName);                 
                endpointNames.add(endpointName);
             }
             
             String[] endpoints = new String[endpointNames.size()];
             endpointNames.toArray(endpoints);
             return endpoints;

        } 
        catch (javax.xml.xpath.XPathExpressionException xpathEx)
        {
            throw new VerifierException(xpathEx.toString());
        }
        catch (Exception ex)
        {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
        }                 
        
     }
     
     /**
      * This method is used to create the endpoint name from 
      * the namespace URI, service name and endpoint name
      * The three items are concatenated with a comma between the,
      * If the namespace URI is empty it is omitted
      * @param namespace the namespace URI
      * @param servicename the service name
      * @param endpointName the endpoint name
      * @returns String the endpointName
      */
     
     public String createEndpointName(
             String namespace, 
             String serviceName, 
             String endpointName)
     {
         StringBuffer epname = new StringBuffer();
         if (namespace != null)
         {
             epname.append(namespace);             
             if (!namespace.endsWith("/"))
             {
                 epname.append("/");
             }
             epname.append(",");
         }
         epname.append(serviceName);
         epname.append(",");
         epname.append(endpointName);
         return epname.toString();
     }
    
     /**
      * This method is used to get the service name of a wsdl file
      * @param File wsdlFile
      * @return String the target namespace
      * @throws VerifierException
      */
     private String getServiceName(File wsdlFile)
     throws VerifierException
     {
         try
         {
             short wsdlVer = getWSDLVersionNumber(wsdlFile);
             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();
             InputSource inputSource =
                     new InputSource(new FileInputStream(wsdlFile));

             //inner class for namespace prefix to uri mapping
             if (wsdlVer == WSDL11)
             {
                 xpath.setNamespaceContext(new WSDL11NamespaceContext());

                 return
                     xpath.evaluate(
                        "/wsdl-11:definitions/wsdl-11:service/attribute::name",
                        inputSource);
             }
             else if (wsdlVer == WSDL20)
             {
                 xpath.setNamespaceContext(new WSDL20NamespaceContext());

                 return
                     xpath.evaluate(
                        "/wsdl-20:description/wsdl-20:service/attribute::name",
                        inputSource);
             }
             else
             {
                 throw new VerifierException(
                     mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_WRONG_WSDL_VERSION));
             }
        }
        catch (javax.xml.xpath.XPathExpressionException xpathEx)
        {
            throw new VerifierException(xpathEx.toString());
        }
        catch (Exception ex)
        {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
        }
     }
 
     /**
      * This method is used to get the service name of a wsdl file
      * @param wsdlString
      * @return String the target namespace
      * @throws VerifierException
      */ 
     private String getServiceName(String wsdlString)
     throws VerifierException     
     {
         try
         {
             short wsdlVer = getWSDLVersionNumber(wsdlString);
             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();
             InputSource inputSource = 
                     new InputSource(new StringReader(wsdlString));            
            
             //inner class for namespace prefix to uri mapping
             if (wsdlVer == WSDL11)
             {
                 xpath.setNamespaceContext(new WSDL11NamespaceContext());
   
                 return 
                     xpath.evaluate(
                        "/wsdl-11:definitions/wsdl-11:service/attribute::name",
                        inputSource);
             }
             else if (wsdlVer == WSDL20)
             {
                 xpath.setNamespaceContext(new WSDL20NamespaceContext());
  
                 return
                     xpath.evaluate(
                        "/wsdl-20:description/wsdl-20:service/attribute::name",
                        inputSource);
             }
             else
             {
                 throw new VerifierException(
                     mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_WRONG_WSDL_VERSION));
             }
         } 
         catch (javax.xml.xpath.XPathExpressionException xpathEx)
         {
             throw new VerifierException(xpathEx.toString());
         }    
         catch (Exception ex)
         {
             //if the supplied document is bad, we may end up with a NPE
             throw new VerifierException(ex.toString());
         }              
     }

    /**
     * This method is used to get the value of the given attribute
     * @param node the node
     * @param attribute the attribute name
     * @param String the attribute value
     */
     public String getAttributeValue(Node node, String attribute)
     {
         if (node != null)
         {
             NamedNodeMap attrbs = node.getAttributes();
             Node attrb = attrbs.getNamedItem(attribute);
             return attrb.getNodeValue();
        }
         return null;
     }
     
     /**
      * This method is used to get the list of application variables
      * used in the given endpoint in the wsdl file
      * @param File the wsdl file
      * @param endpointname the endpoint name
      * @returns List list of application variables
      * @throws VerifierException if the list could not be obtained
      */
     public List getApplicationVariables(File wsdlFile, String endpointName)
     throws VerifierException
     {

         try
         {
             short wsdlVer = getWSDLVersionNumber(wsdlFile);
             String epName = null;
             if (endpointName != null)
             {
                epName = endpointName.substring(endpointName.lastIndexOf(",") + 1);
             }
             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();

             InputSource inputSource =
                    new InputSource(new FileInputStream(wsdlFile));
             List appVarList = new ArrayList();
             NodeList ports = null;
             //inner class for namespace prefix to uri mapping
             if (wsdlVer == WSDL11)
             {
                 xpath.setNamespaceContext(new WSDL11NamespaceContext());

                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-11:definitions/wsdl-11:service/wsdl-11:port",
                     inputSource,
                     XPathConstants.NODESET);
             }
             else if (wsdlVer == WSDL20)
             {
                 xpath.setNamespaceContext(new WSDL20NamespaceContext());

                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-20:description/wsdl-20:service/wsdl-20:endport",
                     inputSource,
                     XPathConstants.NODESET);
             }
             else
             {
                 throw new VerifierException(
                     mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_WRONG_WSDL_VERSION));
             }

             int length = ports.getLength();
             for (int i=0; i<length; i++)
             {
                 if (epName.equals(getAttributeValue(ports.item(i), "name")))
                 {
                    appVarList.addAll(getAppVars(ports.item(i)));
                 }
             }

             ToolsLogManager.getRuntimeLogger().log(Level.FINER, "Application Variables used in {0} are: {1}", new Object[]{epName, appVarList});
             return appVarList;

        }
        catch (javax.xml.xpath.XPathExpressionException xpathEx)
        {
            throw new VerifierException(xpathEx.toString());
        }
        catch (java.io.IOException ioEx)
        {
            throw new VerifierException(ioEx.toString());
        }      
        catch (Exception ex)
        {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
        }

     }
 
     /**
      * This method is used to get the list of application variables 
      * used in the given endpoint in the wsdl file
      * @param File the wsdl file
      * @param endpointname the endpoint name
      * @returns List list of application variables
      * @throws VerifierException if the list could not be obtained
      */
     public List getApplicationVariables(String wsdlString, String endpointName)
     throws VerifierException
     {
         try
         {
             short wsdlVer = getWSDLVersionNumber(wsdlString);
             String epName = null;
             if (endpointName != null)
             {
                epName = endpointName.substring(endpointName.lastIndexOf(",") + 1);
             }
             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();
             InputSource inputSource = 
                    new InputSource(new StringReader(wsdlString));            
            
             List appVarList = new ArrayList();                    
            
             //inner class for namespace prefix to uri mapping
             NodeList ports = null;
             if (wsdlVer == WSDL11)
             {
                 xpath.setNamespaceContext(new WSDL11NamespaceContext());
   
                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-11:definitions/wsdl-11:service/wsdl-11:port",
                     inputSource,
                     XPathConstants.NODESET);
             }
             else if (wsdlVer == WSDL20)
             {
                 xpath.setNamespaceContext(new WSDL20NamespaceContext());
  
                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-20:description/wsdl-20:service/wsdl-20:endport",
                     inputSource,
                     XPathConstants.NODESET);          
             }
             else
             {
                 throw new VerifierException(
                     mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_WRONG_WSDL_VERSION));
             }
             
             int length = ports.getLength();
             for (int i=0; i<length; i++)
             {
                 if (epName.equals(getAttributeValue(ports.item(i), "name")))
                 {
                    appVarList.addAll(getAppVars(ports.item(i)));
                 }
             }
             
             ToolsLogManager.getRuntimeLogger().log(Level.FINER, "Application Variables used in {0} are: {1}", new Object[]{epName, appVarList});
             return appVarList;

        } 
        catch (javax.xml.xpath.XPathExpressionException xpathEx)
        {
            throw new VerifierException(xpathEx.toString());
        }
        catch (Exception ex)
        {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
        }                 
        
     }
          
    /**
     * This method is used to get a list of application variables 
     * used in a node
     * @param Node the node
     * @returns List the list of app vars
     */ 
     private List getAppVars(Node node)
     {
         List applicationVariables = new ArrayList();
         try
         {
             //1. get the node's content
             applicationVariables.addAll(
                     getAppVars(node.getNodeValue()));
                     
             //2. get the attributes content
             if (node.hasAttributes())
             {
                 NamedNodeMap attrs = node.getAttributes();
                 for(int i=0; i<attrs.getLength(); i++)
                 {
                     applicationVariables.addAll(
                             getAppVars(attrs.item(i).getNodeValue()));
                 }
             }
             
             //3.get the children's content
             if (node.hasChildNodes())
             {
                NodeList children = node.getChildNodes();
                for(int i=0; i<children.getLength();i++)
                {
                    applicationVariables.addAll(getAppVars(children.item(i)));
                }
             }

         }
         catch(Exception ex)
         {
             if (ex.getMessage() != null)
             {
                ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
             }
         }
         return applicationVariables;         
     }

     private short getWSDLVersionNumber(InputSource inputSource11,
                                        InputSource inputSource20)
         throws VerifierException
     {
         try
         {
             XPath xpath11 = XPathFactory.newInstance().newXPath();
             String expression11 = "/wsdl-11:definitions";
             xpath11.setNamespaceContext(new WSDL11NamespaceContext());
             NodeList nodes = (NodeList) xpath11.evaluate(expression11,
                                                  inputSource11,
                                                  XPathConstants.NODESET);

             if ((nodes != null) && (nodes.getLength() > 0))
             {
                 return WSDL11;
             }

             XPath xpath20 = XPathFactory.newInstance().newXPath();
             String expression20 = "/wsdl-20:description";
             xpath20.setNamespaceContext(new WSDL20NamespaceContext());
             nodes = (NodeList) xpath20.evaluate(expression20,
                                          inputSource20,
                                          XPathConstants.NODESET);
             if ((nodes != null) && (nodes.getLength() > 0))
             {
                 return WSDL20;
             }
         }
         catch(XPathExpressionException exp)
         {
             throw new VerifierException(
                            mResourceBundle.getMessage(
                                LocalStringKeys.VERIFIER_INVALID_WSDL_FORMAT));
         }

         throw new VerifierException(
                            mResourceBundle.getMessage(
                                LocalStringKeys.VERIFIER_INVALID_WSDL_FORMAT));
     }

     private short getWSDLVersionNumber(File inputFile)
         throws VerifierException
     {
         try
         {
             InputSource inputSource11 =
                 new InputSource(new FileInputStream(inputFile));
             InputSource inputSource20 =
                 new InputSource(new FileInputStream(inputFile));
             return getWSDLVersionNumber(inputSource11, inputSource20);
         }
         catch(FileNotFoundException exp)
         {
             throw new VerifierException(
                            mResourceBundle.getMessage(
                                LocalStringKeys.VERIFIER_WSDL_FILE_NOT_FOUND,
                                new Object[] { (""+inputFile).toString() }));
         }
     }

     private short getWSDLVersionNumber(String inputString)
         throws VerifierException
     {
         InputSource inputSource11 =
             new InputSource(new StringReader(inputString));
         InputSource inputSource20 =
             new InputSource(new StringReader(inputString));
         return getWSDLVersionNumber(inputSource11, inputSource20);
     }
    
     /** 
      * This method is used to get a list of application variables 
      * present in the given String
      * @param content the string
      * @returns List the list of application variables
      *
      */
     public List getAppVars(String content)
     {
         List appVars = new ArrayList();
         try
         {
             if (content != null && content.indexOf("$") != -1)
             {
                 String[] tokens = content.split("[$}]");
                 for (String token : tokens) {
                     if (token.indexOf("{") != -1) {
                         String[] variables = token.split("[{]");
                         for (String variable : variables) {
                             if (variable.length() > 0) {
                                 appVars.add(variable);
                             }
                         }
                     }
                 }
             }
         }
         catch(Exception ex)
         {
             //continue with processing
             if (ex.getMessage()!=null)
             {
                ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());             
             }
         }
         return appVars;
     }
     
    /**
     * This method is used to find out if the given endpoint's port definition has 
     * an associated child element configure connectivity information
     * @param String wsdlString
     * @param endpointName endpointName
     * @return true if configured false otherwise.
     */
     public boolean isPortConfiguredForEndpoint(String wsdlString, String endpointName)
     throws VerifierException             
     {
         try
         {
             short wsdlVer = getWSDLVersionNumber(wsdlString);
             String epName = null;
             if (endpointName != null)
             {
                epName = endpointName.substring(endpointName.lastIndexOf(",") + 1);
             }
             XPathFactory factory = XPathFactory.newInstance();
             XPath xpath = factory.newXPath();
             InputSource inputSource = 
                    new InputSource(new StringReader(wsdlString));            
            
             //inner class for namespace prefix to uri mapping
             NodeList ports = null;
             if (wsdlVer == WSDL11)
             {
                 xpath.setNamespaceContext(new WSDL11NamespaceContext());
   
                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-11:definitions/wsdl-11:service/wsdl-11:port",
                     inputSource,
                     XPathConstants.NODESET);
             }
             else if (wsdlVer == WSDL20)
             {
                 xpath.setNamespaceContext(new WSDL20NamespaceContext());
  
                 ports = (NodeList) xpath.evaluate(
                     "/wsdl-20:description/wsdl-20:service/wsdl-20:endport",
                     inputSource,
                     XPathConstants.NODESET);          
             }
             else
             {
                 throw new VerifierException(
                     mResourceBundle.getMessage(
                         LocalStringKeys.VERIFIER_WRONG_WSDL_VERSION));
             }
             
             int length = ports.getLength();
             for (int i=0; i<length; i++)
             {
                 if (epName.equals(getAttributeValue(ports.item(i), "name")))
                 {
                    if (ports.item(i).hasChildNodes())
                    {
                        return true;
                    }
                 }
             }
             
             ToolsLogManager.getRuntimeLogger().log(Level.FINER, "Endpoint {0} has no child elements for port definition", endpointName);
             return false;

        } 
        catch (javax.xml.xpath.XPathExpressionException xpathEx)
        {
            throw new VerifierException(xpathEx.toString());
        }
        catch (Exception ex)
        {
            //if the supplied document is bad, we may end up with a NPE
            throw new VerifierException(ex.toString());
        }                 
    }

    /**
      * This method maps the prefix jbi and config to the right URIs
      */
    class WSDLNamespaceContext implements NamespaceContext
    {

        /**
         * This method maps the prefix jbi and config to the right URIs
         */
        public String getNamespaceURI(String prefix)
        {
            if (prefix.equals("jbi"))
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
            else if (prefix.equals("config"))
            {
                return "http://www.sun.com/jbi/descriptor/configuration";
            }
            else
            {
                return NS_URI_XMLNS;
            }
        }

        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        public Iterator getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }
    }

    /**
      * Namespace mapper class. Maps the prefix wsdl-11 to
      *  "http://schemas.xmlsoap.org/wsdl/"
      */
    class WSDL11NamespaceContext implements NamespaceContext 
    {

        /**
         * This method maps the prefix wsdl-11 to the URI
         *  "http://schemas.xmlsoap.org/wsdl/"
         */
        public String getNamespaceURI(String prefix) 
        {
            if (prefix.equals("wsdl-11"))
            {
                return "http://schemas.xmlsoap.org/wsdl/";
            }
            else
            {
                return NS_URI_XMLNS;
            }
        }

        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        public Iterator getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }
    }

    /**
      * Namespace mapper class. Maps the prefix wsdl-20 to
      *  "http://www.w3.org/ns/wsdl/"
      */
    class WSDL20NamespaceContext implements NamespaceContext
    {

        /**
         * This method maps the prefix wsdl-20 to the URI
         *  "http://www.w3.org/ns/wsdl/"
         */
        public String getNamespaceURI(String prefix)
        {
            if (prefix.equals("wsdl-20"))
            {
                return "http://www.w3.org/ns/wsdl";
            }
            else
            {
                return NS_URI_XMLNS;
            }
        }

        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        public Iterator getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }
    }                
}
