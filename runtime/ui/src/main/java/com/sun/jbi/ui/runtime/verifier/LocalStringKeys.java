/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.verifier;

/**
 * This interface contains the property keys used for looking up message
 * text in the LocalStrings resource bundle.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{
    /** invalid input     */
    static final String VERIFIER_INVALID_INPUT = 
        "VERIFIER_INVALID_INPUT";    
    
    /** invalid SA archive     */
    static final String VERIFIER_INVALID_SA = 
        "VERIFIER_INVALID_SA";
    
    /** su zip  missing */
    static final String VERIFIER_SU_ARTIFACT_MISSING =
        "VERIFIER_SU_ARTIFACT_MISSING";
    
    /** SA not extracted */
    static final String VERIFIER_SA_NOT_EXTRACTED =  
        "VERIFIER_SA_NOT_EXTRACTED";
    
    /** component could not be started */
    static final String VERIFIER_COMPONENT_NOT_STARTED =
        "VERIFIER_COMPONENT_NOT_STARTED";

    /** no component lifecycle object */
    static final String VERIFIER_NO_COMPONENT_LIFECYCLE_OBJECT =
        "VERIFIER_NO_COMPONENT_LIFECYCLE_OBJECT";

    /** many component lifecycle object */
    static final String VERIFIER_MANY_COMPONENT_LIFECYCLE_OBJECT =
        "VERIFIER_MANY_COMPONENT_LIFECYCLE_OBJECT";
        
    /** endpoint resolved */
    static final String VERIFIER_ENDPOINT_RESOLVED =
        "VERIFIER_ENDPOINT_RESOLVED";
    
    /** endpoint could not be resolved as component is not started */
    static final String VERIFIER_ENDPOINT_NOT_VERIFIED =
            "VERIFIER_ENDPOINT_NOT_VERIFIED";
    
    /** endpoint could not be resolved as component is not installed */
    static final String VERIFIER_COMPONENT_NOT_INSTALLED =
            "VERIFIER_COMPONENT_NOT_INSTALLED";
        
    /** no extension mbean for component*/
    static final String VERIFIER_NO_COMPONENT_EXTENSION_OBJECT =
            "VERIFIER_NO_COMPONENT_EXTENSION_OBJECT";
    
    /** many extension mbean for component*/
    static final String VERIFIER_MANY_COMPONENT_EXTENSION_OBJECT =
            "VERIFIER_MANY_COMPONENT_EXTENSION_OBJECT";
    
    /** app vars not resolved*/
    static final String VERIFIER_APP_VARS_NOT_RESOLVED =
            "VERIFIER_APP_VARS_NOT_RESOLVED";    
    
    /** app configs not resolved*/
    static final String VERIFIER_APP_CONFIGS_NOT_RESOLVED =
            "VERIFIER_APP_CONFIGS_NOT_RESOLVED";    
    
    /** mbean not found */
    static final String VERIFIER_INVALID_MBEAN =
            "VERIFIER_INVALID_MBEAN";
    
    /** issues in determining status */
    static final String VERIFIER_ENDPOINT_STATUS_NOT_DETERMINED =
            "VERIFIER_ENDPOINT_STATUS_NOT_DETERMINED";
    
    /** component could not be shut down */
    static final String VERIFIER_COMPONENT_NOT_SHUTDOWN =
        "VERIFIER_COMPONENT_NOT_SHUTDOWN";    
    
    /** verifier template zip could not be created */
    static final String VERIFIER_TEMPLATE_ZIP_NOT_CREATED =
        "VERIFIER_TEMPLATE_ZIP_NOT_CREATED";
    
    /** verifier template zip exists and could not be deleted */
    static final String VERIFIER_TEMPLATE_ZIP_EXISTS =
        "VERIFIER_TEMPLATE_ZIP_EXISTS";    
    
    /** java ee verifier EAR file not identified */
    static final String VERIFIER_JAVAEE_EAR_FILE_NOT_FOUND =
        "VERIFIER_JAVAEE_EAR_FILE_NOT_FOUND";     
    
    /** export configuration - application is not deployed in target */
    static final String VERIFIER_EXPORT_CONFIG_APP_NOT_DEPLOYED =
        "VERIFIER_EXPORT_CONFIG_APP_NOT_DEPLOYED";   
    
    /** application could not be located **/
    static final String VERIFIER_APPLICATION_NOT_LOCATED =
         "VERIFIER_APPLICATION_NOT_LOCATED";
    
    /** could not collect list of app vars */
    static final String VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_VARS =
         "VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_VARS";
    
    /** could not collect list of app configs **/
    static final String VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_CONFIGS =
          "VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_CONFIGS";
    
    /** could not get the app config type */
    static final String VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_CONFIG_TYPE =
          "VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_CONFIG_TYPE";
    
    /** issue in exporting app config */
    static final String VERIFIER_ERROR_EXPORTING_APP_CONFIG =
          "VERIFIER_ERROR_EXPORTING_APP_CONFIG";
    
    /** issue in retrieving app. var value */
    static final String VERIFIER_ISSUE_GETTING_APP_VAR_VALUE =
            "VERIFIER_ISSUE_GETTING_APP_VAR_VALUE";
    
    /** issue in retrieving app. config value */
    static final String VERIFIER_ISSUE_GETTING_APP_CONFIG_VALUE =
            "VERIFIER_ISSUE_GETTING_APP_CONFIG_VALUE";    
    
    /** issue in exporting app. var value */
    static final String VERIFIER_ISSUE_EXPORTING_APP_VAR_VALUE =
            "VERIFIER_ISSUE_EXPORTING_APP_VAR_VALUE";
            
    /** issue in exporting app. config value */
    static final String VERIFIER_ISSUE_EXPORTING_APP_CONFIG_VALUE =
            "VERIFIER_ISSUE_EXPORTING_APP_CONFIG_VALUE";
            
    /** issue in exporting app. config value */
    static final String ANT_SCRIPT_BEGIN_HEADERS =
            "ANT_SCRIPT_BEGIN_HEADERS";

    /** prop file header */
    static final String VERIFIER_PROP_FILE_HEADER =
            "VERIFIER_PROP_FILE_HEADER";

    /** prop file header */
    static final String VERIFIER_PROP_FILE_HEADER1 =
            "VERIFIER_PROP_FILE_HEADER1";
    
    /** javaee su verification failed */
    static final String VERIFIER_JAVAEE_VERIFICATION_FAILED =
            "VERIFIER_JAVAEE_VERIFICATION_FAILED";

    /** Not be able to find right WSDL version */
    static final String VERIFIER_WRONG_WSDL_VERSION =
            "VERIFIER_WRONG_WSDL_VERSION";

    /** Not be able to find valid WSDL format */
    static final String VERIFIER_INVALID_WSDL_FORMAT =
            "VERIFIER_INVALID_WSDL_FORMAT";

    /** Not be able to find WSDL file */
    static final String VERIFIER_WSDL_FILE_NOT_FOUND =
            "VERIFIER_WSDL_FILE_NOT_FOUND";
}
