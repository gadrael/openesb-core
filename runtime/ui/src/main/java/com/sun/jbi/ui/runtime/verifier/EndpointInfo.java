/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.verifier;

import java.util.List;

/**
 * This class is used as a model for information about an Endpoint
 */
public class EndpointInfo {
    
  
    /**
     * endpoint name
     */
    private String mName;
    
    /**
     * corresponding component 
     */
    private String mComponentName;
    
    /**
     * corresponding SU name
     */
    private String mServiceUnitName;
    
    /**
     * list of application variables
     */
    private List mAppVarList;
    
    /**
     * list of application configurations
     */
    private List mAppConfigList;
    
   /**
    * used to indicate if this endpoint is configured in the wsdl 
    * or in the jbi.xml 
    */    
    private boolean mIsEndpointConfigured;
    
    /**
     * Constructor
     */
    public EndpointInfo()
    {
        
    }
    
    /**
     * Constructor
     */
    public EndpointInfo(
            String endpointName, 
            String componentName,
            String serviceUnitName,
            List appVarList,
            List appConfigList)
    {
        this.mName = endpointName;
        this.mComponentName = componentName;
        this.mServiceUnitName = serviceUnitName;
        this.mAppVarList = appVarList;
        this.mAppConfigList = appConfigList;
    }
    
    /**
     * getter for endpointName
     * @returns String the endpointName
     */
    public String getEndpointName()
    {
        return mName;
    }

    /**
     * getter for componentName
     * @returns String the componentName
     */
    public String getComponentName()
    {
        return mComponentName;
    }
    
    /**
     * getter for serviceunitname
     * @returns String the serviceunitname
     */
    public String getServiceUnitName()
    {
        return mServiceUnitName;
    }
    
    /**
     * getter for list of application variables used
     * @returns List list of application variables 
     * used by this endpoint
     */
    public List getApplicationVariables()
    {
        return mAppVarList;
    }
    
    /**
     * getter for list of application configurations used
     * @returns List list of application configurations associated 
     * with this endpoint
     */
    public List getApplicationConfigurations()
    {
        return mAppConfigList;
    }
    
    /**
     * setter  for endpointName
     * @param name the endpointName
     */
    public void setEndpointName(String name)
    {
        mName = name;
    }

    /**
     * setter for componentName
     * @param componentName the componentName
     */
    public void setComponentName(String componentName)
    {
        mComponentName = componentName;
    }
    
    /**
     * setter for serviceunitname
     * @param serviceunitName the serviceunitname
     */
    public void setServiceUnitName(String serviceunitName)
    {
        mServiceUnitName = serviceunitName;
    }
    
    /**
     * setter for list of application variables used
     * @param appVarList list of application variables 
     * used by this endpoint
     */
    public void setApplicationVariables(List appVarList)
    {
        mAppVarList = appVarList;
    }
    
    /**
     * setter for list of application configurations used
     * @param appConfigList list of application configurations associated 
     * with this endpoint
     */
    public void setApplicationConfigurations(List appConfigList)
    {
        mAppConfigList = appConfigList;
    }
    
    /**
     *  This method is used to get the value of mIsEndpointConfigured
     *  attribute
     *  @return boolean mIsEndpointConfigured
     */
    public boolean getEndpointConfigured()
    {
        return mIsEndpointConfigured;
    }
    
    /**
     * This method is used to set the value of mIsEndpointConfigured
     * attribute
     * @param isConfigured 
     */
    public void setEndpointConfigured(boolean isConfigured)
    {
        mIsEndpointConfigured = isConfigured;
    }
       
    /**
     * This method provides a String representation for an EndpointInfo object
     */
    public String toString()
    {
        StringBuffer result = new StringBuffer();
        result.append(" EndpointName=" + mName +
               " ServiceUnitName=" + mServiceUnitName +
               " ComponentName=" + mComponentName);
        if (mAppVarList != null)
        {
            result.append("Application Variables: " + mAppVarList);
        }
        if( mAppConfigList != null)
        {
            result.append("Application Configuration List" + mAppConfigList);
        }
        return result.toString();
    }
 }
