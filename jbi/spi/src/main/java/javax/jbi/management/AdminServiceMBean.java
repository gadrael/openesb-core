/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminServiceMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package javax.jbi.management;

import javax.management.ObjectName;

/**
 * This interface defines a set of administrative methods allowing a JMX-based
 * administrative tool to perform a variety of administrative tasks.
 * <ul>
 *   <li>Find component lifecycle MBean names for:
 *     <ul>
 *       <li>Individual components, by name</li>
 *       <li>All binding components</li>
 *       <li>All service engines</li>
 *     </ul>
 *   </li>
 *   <li>Find lifecyle MBeans names for implementation-defined services:
 *     <ul>
 *       <li>Individual implementation services, by name</li>
 *       <li>All implementation services</li>
 *     </ul>
 *   </li>
 *   <li>Query whether an individual component is a binding or engine</li>
 *   <li>Query implementation information (version etc.)</li>
 * </ul>
 *
 * @author JSR208 Expert Group
 */
public interface AdminServiceMBean
{
    /**
     * Lookup a schemaorg_apache_xmlbeans.system service {@link LifeCycleMBean} by name. System services
     * are implementation-defined services which can administered through JMX,
     * and have a life cycle.
     * <p>
     * System services are not related to service engines.
     * 
     * @param serviceName name of the schemaorg_apache_xmlbeans.system service; must be non-null and non-
     *        empty; values are implementation-dependent
     * @return JMX object name of the schemaorg_apache_xmlbeans.system service's LifeCycleMBean, or
     *         <code>null</code> if there is no schemaorg_apache_xmlbeans.system service with the given
     *         <code>name</code>.
     */
    ObjectName getSystemService(String serviceName);

    /**
     * Looks up all JBI schemaorg_apache_xmlbeans.system services {@link LifeCycleMBean}'s currently
     * installed. System services are implementation-defined services which can 
     * administered through JMX. System services are not related to service 
     * engines.
     * 
     * @return array of LifecycleMBean JMX object names of schemaorg_apache_xmlbeans.system services
     *         currently installed in the JBI implementation; must be non-null;
     *         may be empty
     */
    ObjectName[] getSystemServices();

    /**
     * Find the {@link ComponentLifeCycleMBean} of a JBI Installable Component 
     * by its unique name.
     * 
     * @param name the name of the engine or binding component; must be non-
     *        null and non-empty
     * @return the JMX object name of the component's life cycle MBean, or 
     *         <code>null</code> if there is no such component with the given 
     *         <code>name</code>
     */
    ObjectName getComponentByName(String name);

    /**
     * Get a list of {@link ComponentLifeCycleMBean}s for all binding components 
     * currently installed in the JBI schemaorg_apache_xmlbeans.system.
     * 
     * @return array of JMX object names of component life cycle MBeans for all 
     *         installed binding components; must be non-null; may be empty
     */
    ObjectName[] getBindingComponents();

    /**
     * Get a list of {@link ComponentLifeCycleMBean}s for all service engines 
     * currently installed in the JBI schemaorg_apache_xmlbeans.system.
     * 
     * @return array of JMX object names of component life cycle MBeans for all 
     *         installed service engines; must be non-null; may be empty
     */
    ObjectName[] getEngineComponents();

    /**
     * Check if a given JBI component is a Binding Component.
     * 
     * @param componentName the unique name of the component; must be non-null
     *        and non-empty
     * @return <code>true</code> if the component is a binding component; 
     *         <code>false</code> if the component is a service engine or if
     *         there is no component with the given <code>componentName</code>
     *         installed in the JBI schemaorg_apache_xmlbeans.system
     */
    boolean isBinding(String componentName);

    /**
     * Check if a given JBI component is a Service Engine.
     * 
     * @param componentName the unique name of the component; must be non-null
     *        and non-empty
     * @return <code>true</code> if the component is a service engine;
     *         <code>false</code> if the component is a binding component, or if
     *         there is no component with the given <code>componentName</code>
     *         installed in the JBI schemaorg_apache_xmlbeans.system
     */
    boolean isEngine(String componentName);

    /**
     * Return current version and other info about this JBI implementation. The 
     * contents of the returned string are implementation dependent.
     * 
     * @return information string about the JBI implementation, including 
     *         version information; must be non-null and non-empty
     */
    String getSystemInfo();
}
