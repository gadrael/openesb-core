/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LifeCycleMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package javax.jbi.management;

/**
 * LifeCycleMBean is a base interface that defines standard life cycle controls 
 * for JBI implementation services (which are implementation-specific), and JBI
 * components (bindings and engines).
 *
 * @author JSR208 Expert Group
 */
public interface LifeCycleMBean
{
    /**
     * Start the item.
     * 
     * @exception javax.jbi.JBIException if the item fails to start.
     */
    void start() throws javax.jbi.JBIException;

    /**
     * Stop the item. This suspends current messaging activities.
     * 
     * @exception javax.jbi.JBIException if the item fails to stop.
     */
    void stop() throws javax.jbi.JBIException;

    /**
     * Shut down the item. This releases resources and returns the item
     * to an uninitialized state.
     *
     * @exception javax.jbi.JBIException if the item fails to shut down.
     */
    void shutDown() throws javax.jbi.JBIException;

    /**
     * Get the current state of this managed compononent.
     * 
     * @return the current state of this managed component (must be one of the 
     *         string constants defined by this interface)
     */
    String getCurrentState();

    /** Value returned by {@link #getCurrentState()} for a shutdown component. */
    final static String SHUTDOWN = "Shutdown";

    /** Value returned by {@link #getCurrentState()} for a stopped component. */
    final static String STOPPED  = "Stopped";

    /** Value returned by {@link #getCurrentState()} for a running component. */
    final static String STARTED = "Started";

    /** Value returned by {@link #getCurrentState()} for a component in an
     * unknown state. */
    final static String UNKNOWN  = "Unknown";
}
