/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.installer;

import java.io.File;
/**
 *
 * This class stores all the constants required
 */
public class InstallConstants {

    public static final String JBI_CORE_INSTALLER_JAR = "jbi-core-installer.jar";
    public static final String JBI_INSTALL_SCRIPT = "build.xml";
    public static final String JBI_INSTALL_TARGET = "install-jbi-core";
    public static final String AS_INSTALL_DIR = "appserver.install.dir";
    public static final String AS_DOMAIN_ROOT = "appserver.jbi.domain.root";
    public static final String RESOURCE_BUNDLE = "com.sun.jbi.installer.InstallerMessages";

    //the following two entries are relative to appserver install dir
    public static final String JBI_INSTALL_DIR = "jbi";
}
