/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIXMLHandler.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.upgrade;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * This class is the XML parsing for jbi.xml
 */
public class JBIXMLHandler extends DefaultHandler
{

    String mBuildNumber = null;

    /**
     * Callback method
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws SAXException
     */
    public void startElement(String uri,
                         String localName,
                         String qName,
                         Attributes attributes)
        throws SAXException
    {
        String buildNumber = attributes.getValue(uri, "build-number");
        if (buildNumber == null)
        {
            buildNumber = attributes.getValue("", "build-number");
        }

        if (buildNumber != null)
        {
            mBuildNumber = buildNumber;
        }
    }

    public String getBuildNumber()
    {
        return mBuildNumber;
    }
} 
