/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UpgradeToolUtil.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.upgrade;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;

import java.util.jar.JarFile;
import java.util.jar.JarEntry;
import java.util.jar.JarEntry;
import java.util.jar.JarException;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;

import java.util.ResourceBundle;
import java.util.Enumeration;

import java.text.MessageFormat;

/**
 *
 * This is a utility class 
 */
public class UpgradeToolUtil
{
    /** R/W buffer size for copying archive files. */
    private static final int BUFFER_SIZE = 8 * 1024;

    private ResourceBundle mResourceBundle = null;

    /**
     * Constructor
     * @param resourceBundle
     */
    public UpgradeToolUtil(ResourceBundle resourceBundle)
    {
        if (resourceBundle == null)
        {
            throw new RuntimeException("resourceBundle can not be null.");
        }

        mResourceBundle = resourceBundle;
    }

    /**
     * Public method extractDomainNameFromDoaminRoot
     * @param domainRoot
     * @return String
     * @throws RuntimeException
     */
    public String extractDomainNameFromDoaminRoot(String domainRoot)
        throws RuntimeException
    {
        if (domainRoot == null)
        {
            String expMsg =
                this.getResourceBundle().getString(
                    UpgradeToolConstants.DOMAIN_ROOT_NOT_FOUND);

            throw new RuntimeException(expMsg);
        }
        else if (domainRoot.trim().endsWith(File.separator))
        {
            domainRoot =
                domainRoot.trim().substring(0,
                                      domainRoot.trim().length() - 1);
        }
        else
        {
            domainRoot = domainRoot.trim();
        }

        int lastSeparatorIndex = domainRoot.lastIndexOf(File.separatorChar);
        if (lastSeparatorIndex == -1)
        {
            lastSeparatorIndex = domainRoot.lastIndexOf('/');
        }

        return domainRoot.substring(lastSeparatorIndex, domainRoot.length());
    }

    /**
     * Public method getInstallJarPathInAppServerInstallRoot
     * @param appServerInstallRoot
     * @param domainName
     * @param componentDir
     * @param compName
     */
    public String getInstallJarPathInAppServerInstallRoot(
                                      String appServerInstallRoot,
                                      String componentDir,
                                      String compName)
        throws RuntimeException
    {
        File compDir = new File(appServerInstallRoot +
                                File.separator +
                                UpgradeToolConstants.JBI_INSTALL_DIR +
                                File.separator +
                                componentDir +
                                File.separator +
                                compName.trim());

        if (!compDir.exists())
        {
            return null;
        }

        File [] jarFiles = compDir.listFiles(new JarFilenameFilter());

        if ((jarFiles == null) || (jarFiles.length == 0))
        {
            return null;
        }
        else if (jarFiles.length >= 2)
        {
            return null;
        }

        return jarFiles[0].toString();
    }

    /**
     * Public method getJarPathInComponentRoot
     * @param domainRoot
     * @param componentDir
     * @param compName
     * @throws RuntimeException
     */
    public File getJarPathInComponentRoot(
                                      String domainRoot,
                                      String componentDir,
                                      String compName)
        throws RuntimeException
    {
        File compDir = new File(domainRoot +
                                File.separator +
                                UpgradeToolConstants.JBI_INSTALL_DIR +
                                File.separator +
                                componentDir +
                                File.separator +
                                compName.trim());

        if (!compDir.exists())
        {
            String expMsg =
                getI18NMessage(
                    UpgradeToolConstants.COMPONENT_DIR_NOT_FOUND,
                    new String [] { compDir.toString() });

            throw new RuntimeException(expMsg);
        }

        File [] jarFiles = compDir.listFiles(new JarFilenameFilter());

        if ((jarFiles == null) || (jarFiles.length == 0))
        {
            String expMsg =
                this.getResourceBundle().getString(
                    UpgradeToolConstants.COMPONENT_INSTALL_JAR_NOT_FOUND);

            throw new RuntimeException(expMsg);
        }
        else if (jarFiles.length >= 2)
        {
            String expMsg =
                getI18NMessage(
                    UpgradeToolConstants.MORE_THAN_TWO_COMPONENT_INSTALLJARS_FOUND);

            throw new RuntimeException(expMsg);
        }

        return jarFiles[0];
    }


    /**
     * Utility method that writes all data read from the input stream to
     * the output stream.  All streams are closed at the end of this method.
     * @param input the input stream
     * @param output the output stream
     * @throws IOException
     */
    public void transfer(InputStream input, OutputStream output)
        throws java.io.IOException
    {
        byte[] buf = new byte[BUFFER_SIZE];

        for (int count = 0; count > -1; count = input.read(buf))
        {
            output.write(buf, 0, count);
        }

        output.flush();
        input.close();
        output.close();
    }

    /**
     * Public method copyDir
     * @param src
     * @param dest
     * @throws RuntimeException
     */
    public void copyDir(String src, String dest)
        throws RuntimeException
    {
        File srcDir = new File(src);
        if (!srcDir.exists())
        {
            String expMsg =
                getI18NMessage(
                    UpgradeToolConstants.DIRECTORY_NOT_FOUND,
                    new String [] { srcDir.toString() } );

            throw new RuntimeException(expMsg);
        }

        File destDir = new File(dest);
        if (!destDir.exists())
        {
            destDir.mkdir();
        }

        File [] subDirs = srcDir.listFiles(new CopyDirFileFilter(dest));
        for (int i = 0; i < subDirs.length; ++i)
        {
            copyDir(src + File.separator + subDirs[i].getName(),
                    dest + File.separator + subDirs[i].getName());
        }
    }

    /**
     * Public utility method clearComponentInstallRootDir
     * @param path
     * @param sysCompFlag
     */
    public void clearComponentInstallRootDir(File path, boolean sysCompFlag)
    {
        File [] subDirs =
            path.listFiles(new ClearComponentInstallRootDirFilter(sysCompFlag));

        if ((subDirs == null) || (subDirs.length == 0))
        {
            path.delete();
        }
        else
        {
            for (int i=0; i < subDirs.length; ++i)
            {
                clearComponentInstallRootDir(subDirs[i], sysCompFlag);
                subDirs[i].delete();
            }
        }
    }

    /**
     * Public utility method clearSADir
     * @param path
     * @param skipFile
     */
    public void clearSADir(File path, String skipFile)
    {
        File [] subDirs =
            path.listFiles(new ClearSADirFilter(skipFile));

        if ((subDirs == null) || (subDirs.length == 0))
        {
            path.delete();
        }
        else
        {
            for (int i=0; i < subDirs.length; ++i)
            {
                clearSADir(subDirs[i], skipFile);
                subDirs[i].delete();
            }
        }
    }

    /**
     * Unjars a file.
     *
     * @param jarFile File to be unjared
     * @throws IOException if error trying to read entry.
     * @throws java.util.jar.JarException if error trying to read jar file.
     */
    public void unJar (File jarFile, String destDir)
        throws IOException, JarException, ZipException
    {

        // process all entries in that JAR file
        JarFile jar = new JarFile (jarFile);
        unJar(jar, destDir);
        jar.close();
    }
   
    /**
     * Unjars a file.
     *
     * @param jar JarFile to be unjared
     * @throws IOException if error trying to read entry.
     * @throws java.util.jar.JarException if error trying to read jar file.
     */
    public void unJar (java.util.zip.ZipFile jar, String destDir)
        throws IOException, ZipException
    {

        // process all entries in that JAR file
        Enumeration all = jar.entries ();
        while (all.hasMoreElements ())
        {
            getEntry (jar, ((ZipEntry) (all.nextElement ())), destDir);
        }
    }

    /**
     * Gets one file <code>entry</code> from <code>jarFile</code>.
     *
     * @param jarFile the JAR file reference to retrieve <code>entry</code> from.
     * @param entry the file from the JAR to extract.
     * @return the ZipEntry name
     * @throws IOException if error trying to read entry.
     */
    public String getEntry (ZipFile jarFile, ZipEntry entry, String destDir)
        throws IOException
    {
        byte[] theBuffer = new byte[8092];;

        String entryName = entry.getName ();
        // if a directory, mkdir it (remember to create
        // intervening subdirectories if needed!)
        if (entryName.endsWith ("/"))
        {
            new File (destDir, entryName).mkdirs ();
            return entryName;
        }

        File f = new File (destDir, entryName);

        if (!f.getParentFile ().exists ())
        {
            f.getParentFile ().mkdirs ();
        }

        // Must be a file; create output stream to the file
        FileOutputStream fostream = new FileOutputStream (f);
        InputStream istream = jarFile.getInputStream (entry);

        // extract files
        int n = 0;
        while ((n = istream.read (theBuffer)) > 0)
        {
            fostream.write (theBuffer, 0, n);
        }

        try
        {
            istream.close ();
            fostream.close ();
        }
        catch (IOException e)
        {
            // do nothing
        }
        return entryName;
    }

    /**
     * Filter class JarFilenameFilter
     */
    class JarFilenameFilter implements FilenameFilter
    {
        public boolean accept(File dir, String name)
        {
            if ((name != null) && name.endsWith("jar"))
            {
                return true;
            }
            else if ((name != null) && name.endsWith("zip"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /**
     * Filter class WorkSpaceDirFilter which will filter out 
     * the workspace for schemaorg_apache_xmlbeans.system components when clearing dir
     */
    class WorkSpaceDirFilter implements FilenameFilter
    {
        public boolean accept(File dir, String name)
        {
            if ((name != null) &&
                (name.compareTo(UpgradeToolConstants.COMPONENT_WORKSPACE) == 0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    /**
     * Filter class ClearComponentInstallRootDirFilter
     */
    class ClearComponentInstallRootDirFilter implements FilenameFilter
    {
        boolean mSysComponent = true;

        public ClearComponentInstallRootDirFilter(boolean theFlag)
        {
            mSysComponent = theFlag;
        }

        public boolean accept(File dir, String name)
        {
            File theFile = new File(dir + File.separator + name);
            if ((mSysComponent == true) && (name.compareTo("workspace") == 0))
            {
                return false;
            }
            else if (theFile.isDirectory())
            {
                return true;
            }
            else
            {
                theFile.delete();
                return false;
            }
        }
    }

    /**
     * Filter class ClearComponentInstallRootDirFilter which
     * filter out a specified file
     */
    class ClearSADirFilter implements FilenameFilter
    {
        String mSkipFile = null;

        public ClearSADirFilter(String skipFile)
        {
            mSkipFile = skipFile;
        }

        public boolean accept(File dir, String name)
        {
            File theFile = new File(dir + File.separator + name);
            if (theFile.isDirectory())
            {
                return true;
            }
            else if (mSkipFile.compareTo(name) == 0)
            {
                return false;
            }
            else
            {
                theFile.delete();
                return false;
            }
        }
    }

    /**
     * Filter class CopyDirFileFilter which will be used
     * when doing copy-dir
     */
    class CopyDirFileFilter implements FilenameFilter
    {
        private String mDestDir = null;

        public CopyDirFileFilter(String destDir)
        {
            mDestDir = destDir;
        }

        public boolean accept(File dir, String name)
        {
           File theFile = new File(dir + File.separator + name);
           if ((new File(dir + File.separator + name)).isDirectory())
           {
               return true;
           }
           else
           {
               File outputFile =
                   new File(mDestDir + File.separator + name);
               try
               {
                   if (outputFile.createNewFile())
                   {
                       FileOutputStream outStream =
                           new FileOutputStream(outputFile);
                       FileInputStream inStream =
                           new FileInputStream(theFile);

                       transfer(inStream, outStream);

                       inStream.close();
                       outStream.close();
                   }
                   else
                   {
                       String expMsg =
                           getI18NMessage(
                               UpgradeToolConstants.CAN_NOT_CREATE_FILE,
                               new String [] { outputFile.toString() } );

                       throw new RuntimeException(expMsg);
                   }
               }
               catch(IOException exp)
               {
                    String expMsg =
                           getI18NMessage(
                               UpgradeToolConstants.IO_EXCEPTION_DURING_FILE_COPY,
                               new String [] { dir.toString(), mDestDir } );

                    throw new RuntimeException(expMsg);
               }
               return false;
           }
        }
    }

    private ResourceBundle getResourceBundle()
    {
        return mResourceBundle;
    }

    /** gets the i18n message
     * @param aI18NMsg String.
     * @param aArgs Object[]
     * @return formated i18n string.
     */
    private static String getFormattedMessage(
    String aI18NMsg, Object[] aArgs )
    {
        String formattedI18NMsg = aI18NMsg;
        try
        {
            MessageFormat mf = new MessageFormat(aI18NMsg);
            formattedI18NMsg = mf.format(aArgs);
        } catch (Exception ex)
        { }
        return formattedI18NMsg;
    }

    /** gets the i18n message
     * @param aI18NKey i18n key
     * @param anArgsArray array of arguments for the formatted string
     * @return formatted i18n string
     */
    public String getI18NMessage( String aI18NKey, Object[] anArgsArray )
    {
        String i18nMessage = getResourceBundle().getString(aI18NKey);
        if ( anArgsArray != null )
        {
            return getFormattedMessage(i18nMessage, anArgsArray);
        } else
        {
            return i18nMessage;
        }
    }

    /** gets the i18n message
     * @param aI18NKey i18n key
     * @return i18n string
     */
    public String getI18NMessage( String aI18NKey )
    {
        return getI18NMessage( aI18NKey, null );
    }

}
