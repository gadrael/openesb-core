/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

/**
 * This class holds the static data used in configuration file.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConfigData
{
    /**
     * The name of the schema file.
     */
    public static final String SCHEMA_FILE = "endpoints.xsd";

    /**
     * The name of the jbi schema file for jbi.xml in an SU.
     */
    public static final String JBI_SCHEMA_FILE = "jbi.xsd";

    /**
     * The element name for endpoint in the XML config file.
     */
    public static final String ENDPOINT = "endpoint";

    /**
     * The element name for endpoint name in the XML config file.
     */
    public static final String ENDPOINTNAME = "endpoint-name";

    /**
     * The element name for service name in the XML config file.
     */
    public static final String SERVICENAME = "service-qname";

    /**
     * The element name for input folder in the XML config file.
     */
    public static final String INPUTDIR = "input-dir";

    /**
     * The element name for output folder in the XML config file.
     */
    public static final String OUTPUTDIR = "output-dir";

    /**
     * The element name for processed folder in the XML config file.
     */
    public static final String PROCESSEDDIR = "processed-dir";

    /**
     * The element name for output file name prefix in the XML config file.
     */
    public static final String OUTPUTPREFIX = "output-file-prefix";

    /**
     * The element name for output file extension in the XML config file.
     */
    public static final String OUTPUTEXTENSION = "output-file-extension";

    /**
     * The element name for input file pattern in the XML config file.
     */
    public static final String INPUTPATTERN = "input-pattern";

    /**
     * The element name for endpoint type in the XML config file.
     */
    public static final String ENDPOINT_TYPE = "endpoint-role";

    /**
     * The element name which identifies the input-message-type.
     */
    public static final String INPUT_MESSAGE_TYPE = "input-message-type";

    /**
     * The element name which identifies the output-message-type.
     */
    public static final String OUTPUT_MESSAGE_TYPE = "output-message-type";

    /**
     * Denotes an attachment message type.
     */
    public static final String ATTACHEMENT_TYPE = "attachment";

    /**
     * Denotes an XML message type.
     */
    public static final String XML_TYPE = "xml";

    /**
     * The element name for operastion in the XML config file.
     */
    public static final String OPERATION = "operation";

    /**
     * Role of endpoint.
     */
    public static final int CONSUMER = 0;

    /**
     * Role of endpoint.
     */
    public static final int PROVIDER = 1;

    /**
     * The element name for MEP in the XML config file.
     */
    public static final String MEP = "mep";

    /**
     * Namespace URI corresponding to a QName.
     */
    public static final String NAMESPACE_URI = "namespace-uri";

    /**
     * Token for storing the local name corresponding to QName.s.
     */
    public static final String LOCAL_PART = "local-part";

    /**
     * Namespace of the service.
     */
    public static final String SERVICE_NAMESPACE = "service-namespace";

    /**
     * Tag name for service.
     */
    public static final String SERVICE = "service";

    /**
     * Default message type for file binding messages.
     */
    public static final String DEFAULT_MESSAGE_TYPE = "xml";

    /**
     * Namespace for operation.
     */
    public static final String OPERATION_NAMESPACE = "operation-namespace";

    /**
     * Local name of the service.
     */
    public static final String SERVICE_LOCALNAME = "service-local-name";

    /**
     * Token denoting string name.
     */
    public static final String NAME = "name";

    /**
     * Namespace for file binding.
     */
    public static final String FILENAMESPACE = "http://sun.com/jbi/wsdl/file10";

    /**
     * Binding name.
     */
    public static final String BINDING_NAME = "binding-name";

    /**
     * Extension of done file.
     */
    public static final String DONE_EXTENSION = "done";

    /**
     * Extension of error file.
     */
    public static final String ERROR_EXTENSION = "err";

    /**
     * Separator char for the file name.
     */
    public static final String SEPARATOR = "_";

    /**
     * Tracking id property.
     */
    public static final String TRACKINGID_PROPERTY = "TRACKINGID";

    /**
     * targetNamespace attribute in XML.
     */
    public static final String TARGET_NAMESPACE = "targetNamespace";

    /**
     * String for provider role.
     */
    public static final String PROVIDER_STRING = "provider";

    /**
     * String for consumer role.
     */
    public static final String CONSUMER_STRING = "consumer";

    /**
     * Type attribute for binding in WSDL.
     */
    public static final String WSDL_BINDING_TYPE = "type";

    /**
     * WSDL input folder location.
     */
    public static final String WSDL_INPUT_LOCATION = "input-location";

    /**
     * WSDL processed folder location.
     */
    public static final String WSDL_PROCESSED_LOCATION = "processed-location";

    /**
     * WSDL output folder location.
     */
    public static final String WSDL_OUTPUT_LOCATION = "output-location";

    /**
     * Denotes the number of files that will be picked up per folder.
     */
    public static final int BATCH_SIZE = 3;

    /**
     * File extension set in message exchange.
     */
    public static final String EXTENSION_PROPERTY = "FILEEXTENSION";

    /**
     * Filename  property set in message exchage.
     */
    public static final String FILENAME_PROPERTY = "FILENAME";

    /**
     * Interface name in the XML file.
     */
    public static final String INTERFACE = "interface";

    /**
     * Namespace URI for the interface name.
     */
    public static final String INTERFACE_NAMESPACE = "interface_namespace";

    /**
     * Interface local name.
     */
    public static final String INTERFACE_LOCALNAME = "interface_localname";

    /**
     * Deployment descriptor file
     */
    public static final String DEPLOY_DESCRIPTOR = "jbi.xml";
}
