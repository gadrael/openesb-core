/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileBindingSUManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import com.sun.jbi.binding.file.util.ConfigData;
import com.sun.jbi.binding.file.util.DeployHelper;
import com.sun.jbi.binding.file.util.StringTranslator;
import com.sun.jbi.common.Util;
import com.sun.jbi.common.management.ComponentMessageHolder;
import com.sun.jbi.common.management.ManagementMessageBuilder;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.management.DeploymentException;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * Implementd the deployment lifecycle contract with the framework.
 *
 * @author Sun Microsystems, Inc.
 */
public class FileBindingSUManager
    implements javax.jbi.component.ServiceUnitManager, FileBindingResources
{
    /**
     * ComponentContext
     */
    private ComponentContext mContext;

    /**
     * Deploy helper.
     */
    private DeployHelper mHelper;

    /**
     * Endpoint manager.
     */
    private EndpointManager mManager;

    /**
     * File binding resolver.
     */
    private FileBindingResolver mResolver;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * Deployer messages.
     */
    private ManagementMessageBuilder mBuildManagementMessage;

    /**
     * Helper for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * Creates a new FileBindingSUManager object.
     */
    public FileBindingSUManager()
    {
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
    }

    /**
     * Returns a list of all service units (SUs) currently deployed in to the
     * named component.  This method is a convenient wrapper for the same
     * operation in the DeployerMBean.
     *
     * @return array of SU UUID strings.
     */
    public String [] getDeployments()
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();

        return dr.getAllDeployments();
    }

    /**
     * Creates a new instance of FileBindingSUManager.
     *
     * @param manager endpoint manager
     * @param reslv resolver.
     */
    public void setValues(
        EndpointManager manager,
        FileBindingResolver reslv)
    {
        mContext = FileBindingContext.getInstance().getContext();
        mManager = manager;
        mResolver = reslv;
        mBuildManagementMessage = Util.createManagementMessageBuilder();
    }

    /**
     * This method is called by the framework when a deployment request comes
     * for the file binding.
     *
     * @param suId service unit id.
     * @param suPath service unit path.
     *
     * @return status message.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    public String deploy(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();
        String retMsg = null;

        if (dr.getDeploymentStatus(suId))
        {
            mLog.severe(mTranslator.getString(FBC_DUPLICATE_DEPLOYMENT, suId));
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "FBC_30001", suId,
                    mTranslator.getString(FBC_DUPLICATE_DEPLOYMENT, suId), null));
        }

        /**
         * You have to call initialize deployments here because, thats where
         * the schema check and data check are made, you want to fail the
         * deployment if the artifact fails validation. The same stuff is done
         * in init also during component starts , but if you dont do it here u
         * cannot fail deployments because of invalid data
         */
        String warning = null;
        try
        {
             warning = initializeDeployment(suId, suPath);

            /*
             * Do not start deployments here.
             * The Deployment service will call init and start separately
             * after finidhing the deploy method
             */
        }
        catch (DeploymentException de)
        {
            throw de;
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "FBC_30009", suId,
                    mTranslator.getString(FBC_XML_STRING_CREATION_FAILED, suId),
                    e));
        }

        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("deploy");
            compMsgHolder.setTaskResult("SUCCESS");
            if (warning != null)                
            {
                if (!warning.trim().equals(""))
                {
                    compMsgHolder.setStatusMessageType("WARNING");
                }
            }
            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(FBC_XML_STRING_CREATION_FAILED));
            mLog.severe(e.getMessage());
        }

        return retMsg;
    }

    /**
     * This method is called by the framework when the deployment has to be \
     * initialised , just before starting it.
     *
     * @param suId service unit id.
     * @param suPath service unit path.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    public void init(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        /*
         * This may be called as soon as the deployment has hapened
         * or when the component starts
         * When the component starts up, we have to redeploy this SU
         * internally. This is how simple bindings and engines work. In case
         * of grown up components they might have their own ways of tracking
         * SU deployments and may not have to initialize deployments
         * everytime the component starts.
         */
        DeploymentRegistry dr = DeploymentRegistry.getInstance();

        if (!dr.getDeploymentStatus(suId))
        {
            String warning = null;
            try
            {
                warning = initializeDeployment(suId, suPath);
            }
            catch (DeploymentException de)
            {
                throw de;
            }
            catch (Exception e)
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "FBC_30009", suId,
                        mTranslator.getString(FBC_XML_STRING_CREATION_FAILED,
                            suId), e));
            }
            
            if (warning != null)
            {
                if (!warning.trim().equals(""))
                {
                    mLog.warning(warning);
                }
            }
        }
    }

    /**
     * Shuts down the deplyment.
     *
     * @param str deployment id.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     */
    public void shutDown(String str)
        throws javax.jbi.management.DeploymentException
    {
    }

    /**
     * Starts   a deployment.
     *
     * @param suId service unit id.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    public void start(String suId)
        throws javax.jbi.management.DeploymentException
    {
        try
        {
            mManager.startDeployment(suId);
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "FBC_30009", suId,
                    mTranslator.getString(FBC_XML_STRING_CREATION_FAILED, suId),
                    e));
        }

        if (!mManager.isValid())
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "FBC_30009", suId, mManager.getError(), null));
        }
    }

    /**
     * Stops the service unit.
     *
     * @param suId service unit id.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    public void stop(String suId)
        throws javax.jbi.management.DeploymentException
    {
        mManager.stopDeployment(suId);

        DeploymentRegistry dr = DeploymentRegistry.getInstance();

        if (!mManager.isValid())
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "FBC_30009", suId, mManager.getError(), null));
        }

        try
        {
            List l = dr.getEndpoints(suId);
            Iterator iter = l.iterator();

            while (iter.hasNext())
            {
                EndpointBean eb = (EndpointBean) iter.next();
                ServiceEndpoint er = eb.getServiceEndpoint();
                String epname = null;

                if (eb.getRole() == ConfigData.PROVIDER)
                {
                    epname = eb.getUniqueName();
                }
                else
                {
                    QName nam =
                        new QName(eb.getValue(ConfigData.SERVICE_NAMESPACE),
                            eb.getValue(ConfigData.SERVICE_LOCALNAME));
                    epname = nam.toString();
                }

                dr.deregisterEndpoint(epname);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "undeploy", "FAILED",
                    "FBC_30006", "", e.getMessage(), e));
        }
    }

    /**
     * Undeploys the service unit.
     *
     * @param suId service unit id.
     * @param suPath service unit path.
     *
     * @return status message.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    public String undeploy(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();
        String retMsg = null;

        try
        {
            /*
             * The deployment should be stopped by the time we come here
             */
            List l = dr.getEndpoints(suId);
            Iterator iter = l.iterator();

            while (iter.hasNext())
            {
                EndpointBean eb = (EndpointBean) iter.next();
                ServiceEndpoint er = eb.getServiceEndpoint();
                String epname = null;

                if (eb.getRole() == ConfigData.PROVIDER)
                {
                    epname = eb.getUniqueName();
                }
                else
                {
                    QName nam =
                        new QName(eb.getValue(ConfigData.SERVICE_NAMESPACE),
                            eb.getValue(ConfigData.SERVICE_LOCALNAME));
                    epname = nam.toString();
                }

                if (!dr.isDeployed(epname))
                {
                    throw new DeploymentException(createExceptionMessage(
                            mContext.getComponentName(), "undeploy", "FAILED",
                            "FBC_30006", "", "", null));
                }

                dr.deregisterEndpoint(epname);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "undeploy", "FAILED",
                    "FBC_30006", "", e.getMessage(), e));
        }

        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("undeploy");
            compMsgHolder.setTaskResult("SUCCESS");

            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(FBC_XML_STRING_CREATION_FAILED));
            mLog.severe(e.getMessage());
        }

        return retMsg;
    }

    /**
     * helper method to create XML exception string.
     *
     * @param compid Component id.
     * @param oper operation like deploy or undeploy
     * @param status success r failure
     * @param loctoken some failure string token like SEQ_300001
     * @param locparam parameters for error message.
     * @param locmessage error message.
     * @param exObj stack trace of exception.
     *
     * @return XML string.
     */
    private String createExceptionMessage(
        String compid,
        String oper,
        String status,
        String loctoken,
        String locparam,
        String locmessage,
        Throwable exObj)
    {
        String [] locParams = new String[1];
        locParams[0] = locparam;

        ComponentMessageHolder msgMap =
            new ComponentMessageHolder("EXCEPTION_MSG");

        msgMap.setComponentName(compid);
        msgMap.setTaskName(oper);
        msgMap.setTaskResult(status);
        msgMap.setLocToken(1, loctoken);
        msgMap.setLocParam(1, locParams);
        msgMap.setLocMessage(1, locmessage);
        msgMap.setExceptionObject(exObj);

        String retMsg = null;

        try
        {
            retMsg = mBuildManagementMessage.buildComponentMessage(msgMap);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(FBC_XML_STRING_CREATION_FAILED));
            mLog.severe(e.getMessage());
        }

        return retMsg;
    }

    /**
     * Initializes the deployments, parses and loads the config file.
     *
     * @param suId service unit id.
     * @param suPath service unit path.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    private String initializeDeployment(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        mHelper = new DeployHelper(suId, suPath, mResolver);
        mHelper.doDeploy(true);

        if (!mHelper.isValid())
        {
            mLog.severe(mHelper.getError());

            Exception e = mHelper.getException();

            if (e != null)
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "FBC_30005", "", mHelper.getError(), e));
            }
            else
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "FBC_30005", "", mHelper.getError(), null));
            }
        }
        
        return mHelper.getWarning();
    }

    /**
     * Starts the deployment.
     *
     * @param suId service unit id.
     *
     * @return status message.
     */
    private String startDeployment(String suId)
    {
        String retMsg = null;
        mManager.startDeployment(suId);

        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("deploy");
            compMsgHolder.setTaskResult("SUCCESS");

            if (!mManager.isValid())
            {
                compMsgHolder.setTaskResult("FAILED");
                compMsgHolder.setLocMessage(1, mManager.getError());
            }
            else if (!mManager.getWarning().trim().equals(""))
            {
                compMsgHolder.setStatusMessageType("WARNING");
                compMsgHolder.setLocMessage(1, mManager.getWarning());
            }

            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(FBC_XML_STRING_CREATION_FAILED));
            mLog.severe(e.getMessage());
        }

        return retMsg;
    }
}
