/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDLFileReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.EndpointBean;
import com.sun.jbi.binding.file.FileBindingResolver;
import com.sun.jbi.binding.file.FileBindingResources;


import com.sun.jbi.wsdl2.Binding;
import com.sun.jbi.wsdl2.BindingOperation;
import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Endpoint;
import com.sun.jbi.wsdl2.Interface;
import com.sun.jbi.wsdl2.InterfaceOperation;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.namespace.QName;


/**
 * Reads the configuration WSDL file and loads the data into the EndpointBean
 * objects.
 *
 * @author Sun Microsystems, Inc.
 */
public class WSDLFileReader
    extends UtilBase
    implements FileBindingResources
{
    /**
     * WSDL Description object
     */
    private Description mDefinition;

    /**
     * Resolver object.
     */
    private FileBindingResolver mResolver;

    /**
     * Helper class for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * The list of endpoints as configured in the config file. This list
     * contains  all the endpoints and their attibutes
     */
    private EndpointBean [] mEndpointInfoList = null;

    /**
     * The total number of end points in the config file
     */
    private int mTotalEndpoints = 0;

    /**
     * Creates a new WSDLFileReader object.
     *
     * @param rslv resolver object
     */
    public WSDLFileReader(FileBindingResolver rslv)
    {
        setValid(true);
        mResolver = rslv;
        mTranslator = new StringTranslator();
    }

    /**
     * Returns the Bean object corresponding to the endpoint.
     *
     * @param endpoint endpoint name.
     *
     * @return endpoint Bean object.
     */
    public EndpointBean getBean(String endpoint)
    {
        /*Search for the bean corresponding to the service and endpoint name
         */
        for (int j = 0; j < mEndpointInfoList.length; j++)
        {
            String tmp = mEndpointInfoList[j].getUniqueName();

            if (tmp.trim().equals(endpoint))
            {
                return mEndpointInfoList[j];
            }
        }

        return null;
    }

    /**
     * Gets the endpoint list corresponding to a config file.
     *
     * @return End point Bean list
     */
    public EndpointBean [] getEndpoint()
    {
        return mEndpointInfoList;
    }

    /**
     * Returns the total number of endopoints in the wsld file.
     *
     * @return int number of endpoints.
     */
    public int getEndpointCount()
    {
        return mTotalEndpoints;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Description doc)
    {
        mDefinition = doc;
        loadServicesList();
    }

    /**
     * Util method that gets the  attribute value.
     *
     * @param map attribute node map
     * @param attr attribute name
     *
     * @return attribute value
     */
    private String getAttrValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Sets the direction to inbound or outbound.
     *
     * @param map node map.
     * @param eb endpoint bean.
     */
    private void setDirection(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigData.ENDPOINT_TYPE);

        if (val.trim().equalsIgnoreCase(ConfigData.PROVIDER_STRING))
        {
            eb.setRole(ConfigData.PROVIDER);
            eb.setValue(ConfigData.ENDPOINT_TYPE, val);
        }
        else if (val.trim().equalsIgnoreCase(ConfigData.CONSUMER_STRING))
        {
            eb.setRole(ConfigData.CONSUMER);
            eb.setValue(ConfigData.ENDPOINT_TYPE, val);
        }
        else
        {
            setError(getError() + "\n\t"
                + mTranslator.getString(FBC_WSDL_INVALID_DIRECTION, val));
            eb.setValue(ConfigData.ENDPOINT_TYPE, "");
            setValid(false);

            return;
        }
    }

    /**
     * Gets a node array corresponding to a node name.
     *
     * @param nodeList node list
     * @param name node name
     *
     * @return Node array
     */
    private Node [] getElementNodeArrayByName(
        NodeList nodeList,
        String name)
    {
        Node [] retNode = null;
        List nodeArr = new LinkedList();
        NodeList tmpNodeList = nodeList.item(0).getChildNodes();

        for (int g = 0; g < tmpNodeList.getLength(); g++)
        {
            Node n = tmpNodeList.item(g);

            if ((n != null) && (n.getNodeType() == Node.ELEMENT_NODE))
            {
                if ((n.getLocalName().equals(name)))
                {
                    nodeArr.add(n);
                }
            }
        }

        // for loop
        retNode = new Node[nodeArr.size()];

        for (int k = 0; k < nodeArr.size(); k++)
        {
            retNode[k] = (Node) nodeArr.get(k);
        }

        return retNode;
    }

    /**
     * Method to determine if the binding information in the Document fragment
     * is a file binding or not
     *
     * @param df document fragment
     *
     * @return true if file binding.
     */
    private boolean isFileBinding(DocumentFragment df)
    {
        NamedNodeMap n = df.getFirstChild().getAttributes();

        for (int g = 0; g < n.getLength(); g++)
        {
            if (n.item(g).getLocalName().equals(ConfigData.WSDL_BINDING_TYPE))
            {
                if (n.item(g).getNodeValue().equals(ConfigData.FILENAMESPACE))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Sets the input pattern.
     *
     * @param map Map containing endpoint info.
     * @param eb Bean attributes.
     */
    private void setInputPattern(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigData.INPUTPATTERN);

        if (val == null)
        {
            val = "";
        }

        eb.setValue(ConfigData.INPUTPATTERN, val);
    }

    /**
     * Sets the input, output and processed location.
     *
     * @param map Map containing enpoint info
     * @param eb bean attributes.
     * @param attribute attribute name.
     * @param configdata config data.
     */
    private void setLocation(
        NamedNodeMap map,
        EndpointBean eb,
        String attribute,
        String configdata)
    {
        String val = getValue(map, attribute);

        File f = null;

        try
        {
            f = new File(new URI(val));
        }
        catch (IllegalArgumentException ie)
        {
            setValid(false);
            setError(getError() + " "
                + mTranslator.getString(FBC_NOT_VALID, val) + "\n"
                + ie.getMessage());
            setException(ie);

            return;
        }
        catch (URISyntaxException use)
        {
            setValid(false);
            setError(getError() + "\n\t"
                + mTranslator.getString(FBC_WSDL_INVALID_URI, val) + "\n"
                + use.getMessage());
            setException(use);

            return;
        }

        eb.setValue(configdata, f.getAbsolutePath());
    }

    /**
     * Sets operations from the WSLD doc.
     *
     * @param eb Endpoint bean
     * @param b binding
     */
    private void setOperations(
        EndpointBean eb,
        Binding b)
    {
        Interface bindingif = b.getInterface();
        Node [] allOpNodes =
            getElementNodeArrayByName(b.toXmlDocumentFragment().getChildNodes(),
                "operation");

        for (int oper = 0; oper < b.getOperationsLength(); oper++)
        {
            BindingOperation boper = b.getOperation(oper);
            QName bindingopername = boper.getInterfaceOperation();
            String input = null;
            String output = null;
            String mep = null;
            String ext = null;
            String prefix = null;

            for (int k = 0; k < allOpNodes.length; k++)
            {
                NamedNodeMap nm = allOpNodes[k].getAttributes();

                if (nm != null)
                {
                    String val = getAttrValue(nm, "ref");

                    if ((val != null)
                            && val.equals(bindingopername.getLocalPart().trim()))
                    {
                        input = getValue(nm, ConfigData.INPUT_MESSAGE_TYPE);
                        output = getValue(nm, ConfigData.OUTPUT_MESSAGE_TYPE);

                        ext = getValue(nm, ConfigData.OUTPUTEXTENSION);

                        prefix = getValue(nm, ConfigData.OUTPUTPREFIX);

                        break;
                    }
                }
            }

            if ((input == null) || (input.trim().equals("")))
            {
                input = ConfigData.DEFAULT_MESSAGE_TYPE;
            }

            if ((output == null) || (output.trim().equals("")))
            {
                output = ConfigData.DEFAULT_MESSAGE_TYPE;
            }

            int ifoperlength = bindingif.getOperationsLength();

            for (int j = 0; j < ifoperlength; j++)
            {
                InterfaceOperation op = bindingif.getOperation(j);
                QName ifopername = op.getQualifiedName();

                if (bindingopername.getLocalPart().trim().equals(ifopername.getLocalPart()
                                                                               .trim()))
                {
                    mep = op.getPattern();

                    if (mep != null)
                    {
                        eb.addOperation(ifopername.getNamespaceURI(),
                            ifopername.getLocalPart(), mep, input, output, ext,
                            prefix);
                    }
                }
            }
        }
    }

    /**
     * Sets the output extension.
     *
     * @param map Node map.
     * @param eb endpoint bean.
     */
    private void setOutputExtension(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigData.OUTPUTEXTENSION);

        if (val == null)
        {
            val = "xml";
        }

        eb.setValue(ConfigData.OUTPUTEXTENSION, val);
    }

    /**
     * Sets the output file prefix.
     *
     * @param map Node map.
     * @param eb bean attributes.
     */
    private void setOutputPrefix(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigData.OUTPUTPREFIX);

        if (val == null)
        {
            val = "out";
        }

        eb.setValue(ConfigData.OUTPUTPREFIX, val);
    }

    /**
     * Gets the valus specified by attr from the Map.
     *
     * @param map Node map.
     * @param attr attribute.
     *
     * @return value.
     */
    private String getValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItemNS(ConfigData.FILENAMESPACE, attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Loads the endpoint information.
     *
     * @param ep Endpoint object
     * @param eb endpoint bean.
     */
    private void loadEndpointBean(
        Endpoint ep,
        EndpointBean eb)
    {
        DocumentFragment docfrag = ep.toXmlDocumentFragment();
        Node epnode = docfrag.getFirstChild();
        NamedNodeMap epattributes = epnode.getAttributes();
        setDirection(epattributes, eb);

        if (eb.getRole() == ConfigData.CONSUMER)
        {
            setLocation(epattributes, eb, ConfigData.WSDL_INPUT_LOCATION,
                ConfigData.INPUTDIR);
            setLocation(epattributes, eb, ConfigData.WSDL_PROCESSED_LOCATION,
                ConfigData.PROCESSEDDIR);
        }

        setLocation(epattributes, eb, ConfigData.WSDL_OUTPUT_LOCATION,
            ConfigData.OUTPUTDIR);
        setInputPattern(epattributes, eb);

        if (!isValid())
        {
            setError("\n\tEndpoint : " + ep.getName() + getError());
        }
    }

    /**
     * Parses the config files and loads them into bean objects.
     */
    private void loadServicesList()
    {
        int services = mDefinition.getServicesLength();
        List eplist = new LinkedList();

        for (int i = 0; i < services; i++)
        {
            try
            {
                com.sun.jbi.wsdl2.Service ser = mDefinition.getService(i);
                int endpoints = ser.getEndpointsLength();

                for (int k = 0; k < endpoints; k++)
                {
                    Endpoint ep = ser.getEndpoint(k);
                    Binding b = ep.getBinding();

                    if (!isFileBinding(b.toXmlDocumentFragment()))
                    {
                        continue;
                    }

                    EndpointBean eb = new EndpointBean();
                    loadEndpointBean(ep, eb);
                    eb.setValue(ConfigData.SERVICE_NAMESPACE,
                        ser.getQName().getNamespaceURI());
                    eb.setValue(ConfigData.SERVICE_LOCALNAME,
                        ser.getQName().getLocalPart());
                    eb.setValue(ConfigData.SERVICENAME,
                        ser.getQName().toString());
                    eb.setValue(ConfigData.BINDING_NAME, b.getName());
                    eb.setValue(ConfigData.ENDPOINTNAME, ep.getName());
                    eb.setValue(ConfigData.INTERFACE_LOCALNAME,
                        ep.getBinding().getInterface().getQName().getLocalPart());
                    eb.setValue(ConfigData.INTERFACE_NAMESPACE,
                        ep.getBinding().getInterface().getQName()
                          .getNamespaceURI());
                  //  eb.setWsdlDefintion(mDefinition);
                    eb.setDeploymentType("WSDL20");
                    setOperations(eb, b);

                    if (!isValid())
                    {
                        setError(("\n\t" + "Service : "
                            + ser.getQName().toString() + getError()));
                        eplist.clear();

                        return;
                    }

                    eplist.add(eb);
                    mResolver.addEndpointDoc(ser.getQName().toString()
                        + ep.getName(), mDefinition.toXmlDocument());
                }
            }
            catch (Exception e)
            {
                setError(mTranslator.getString(FBC_WSDL_ENDPOINT_ERROR,
                        e.getMessage()));
                setException(e);
                setValid(false);
                e.printStackTrace();

                return;
            }
        }

        mEndpointInfoList = new EndpointBean[eplist.size()];

        for (int x = 0; x < eplist.size(); x++)
        {
            mEndpointInfoList[x] = (EndpointBean) eplist.get(x);
        }

        mTotalEndpoints = eplist.size();
    }
}
