/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileBindingUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import java.io.File;

import java.util.Hashtable;


/**
 * File binding util class. Util class for doing useful task in file binding.
 *
 * @author Sun Microsystems, Inc.
 */
public class FileBindingUtil
{
    /**
     * Tracking id.
     */
    private static short sTrackingId = 0;

    /**
     * buffer length
     */
    private static final short BUF = 5;

    /**
     * Entries of files that have already been read.
     */
    private static Hashtable sFileEntries;

    /**
     * Util method to get the base name of a filename.
     *
     * @param file File object.
     *
     * @return base name of the file.
     */
    public static String getBaseName(File file)
    {
        String fname = file.getName();
        int k = fname.lastIndexOf('.');

        if (k == -1)
        {
            return fname;
        }

        if (k == 0)
        {
            return "";
        }

        return fname.substring(0, k);
    }

    /**
     * Util method to get an extension from a file name.
     *
     * @param file File object
     *
     * @return the extension of filename.
     */
    public static String getExtension(File file)
    {
        String fname = file.getName();
        int k = fname.lastIndexOf('.');

        if (k == -1)
        {
            return "";
        }

        return fname.substring(k + 1);
    }

    /**
     * Returns the file path from tracking id.
     *
     * @param trackid Track id.
     *
     * @return File path corresponding to track id.
     */
    public static String getFilePath(String trackid)
    {
        String path = null;

        try
        {
            path = (String) sFileEntries.get(trackid);
        }
        catch (Exception e)
        {
            ;
        }

        return path;
    }

    /**
     * Method to generate tracking id.
     *
     * @return tracking id.
     */
    public static synchronized String getTrackingId()
    {
        sTrackingId++;

        if (sTrackingId >= Short.MAX_VALUE)
        {
            sTrackingId = 0;
        }

        return padString(Short.toString(sTrackingId));
    }

    /**
     * Adds an entry to the file entries table.
     *
     * @param trackid track id.
     * @param filepath File path.
     */
    public static void addEntry(
        String trackid,
        String filepath)
    {
        if (sFileEntries == null)
        {
            sFileEntries = new Hashtable();
        }

        try
        {
            sFileEntries.put(trackid, filepath);
        }
        catch (Exception e)
        {
            ;
        }
    }

    /**
     * Checks if the file has already been picked.
     *
     * @param file File
     *
     * @return true if not picked.
     */
    public static boolean canAdd(File file)
    {
        boolean canadd = true;
        String filename = file.getAbsolutePath();

        try
        {
            if (sFileEntries.containsValue(filename))
            {
                canadd = false;
            }
        }
        catch (Exception e)
        {
            ;
        }

        return canadd;
    }

    /**
     * Moves the file to processed folder.
     *
     * @param trk tracking id to be attached to file name.
     * @param destfolder destination folder
     * @param file file name
     *
     * @return moved file name.
     */
    public static String moveFile(
        String trk,
        String destfolder,
        String file)
    {
        if ((destfolder == null) || (file == null))
        {
            return null;
        }
        else if ((destfolder.trim().equals("")) || (file.trim().equals("")))
        {
            return null;
        }
        else
        {
            File destFile = new File(destfolder);
            File f = new File(file);

            return moveFile(trk, destFile, f);
        }
    }

    /**
     * Moves the file to processed folder.
     *
     * @param trk tracking id to be attached to file name.
     * @param destfolder destination folder
     * @param file file name
     *
     * @return moved file name.
     */
    public static String  moveFile(
        String trk,
        File destfolder,
        File file)
    {
        String movedfilename = null;

        if ((destfolder == null) || (file == null))
        {
            return null;
        }

        if ((!destfolder.exists()) || (!destfolder.canWrite()))
        {
            file.delete();
        }
        else
        {
            movedfilename = destfolder.getAbsolutePath()
                        + File.separatorChar + getBaseName(file)
                        + ConfigData.SEPARATOR + trk + "." + getExtension(file);
            boolean success = file.renameTo(new File(movedfilename));

            if (!success)
            {
                file.delete();
                movedfilename = null;
            }
        }

        return movedfilename;
    }

    /**
     * Removes entry from the files table.
     *
     * @param trackid Trackid
     */
    public static void removeEntry(String trackid)
    {
        String path = null;

        try
        {
            path = (String) sFileEntries.remove(trackid);
        }
        catch (Exception e)
        {
            ;
        }
    }

    /**
     * Util method for padding with zeros.
     *
     * @param s string to be padded.
     *
     * @return padded string.
     */
    private static String padString(String s)
    {
        StringBuffer sb = new StringBuffer(s);
        int len = sb.length();

        for (int i = 0; i < (BUF - len); i++)
        {
            sb.insert(0, '0');
        }

        return sb.toString();
    }
}
