/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileListing.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import java.io.File;
import java.io.FilenameFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;


/**
 * Helper class for listing files / folders.
 *
 * @author Sun Microsystems, Inc.
 */
public final class FileListing
{
    /**
     * Flag to indicate recursive listing.
     */
    private static boolean sRecurse = false;

    /**
     * Filter class to be used.
     */
    private static FilenameFilter sFilter = null;

    /**
     * Logger object.
     */
    private static Logger sLog =
        Logger.getLogger("com.sun.jbi.binding.file.util");

    /**
     * Gets the list of files in a folder.
     *
     * @param aDir folder name.
     * @param bRecurse will recurse if true.
     * @param aFilter filname filter.
     *
     * @return List of file names.
     */
    public static synchronized List getFileListing(
        File aDir,
        boolean bRecurse,
        FilenameFilter aFilter)
    {
        sRecurse = bRecurse;
        sFilter = aFilter;

        if ((aDir == null) || (aDir.getName().trim().equals("")))
        {
            sLog.severe("Directory is null");

            return null;
        }

        return getListing(aDir);
    }

    /**
     * Recursively walk a directory tree and return a List of all Files found;
     * the List is sorted using File.compareTo.
     *
     * @param aDir is a valid directory, which can be read.
     *
     * @return array of folder names
     */
    public static synchronized String [] getFolderListing(File aDir)
    {
        File [] dirs = aDir.listFiles();

        if (dirs == null)
        {
            return null;
        }

        List filesDirs = Arrays.asList(dirs);

        if (filesDirs == null)
        {
            return null;
        }

        Iterator filesIter = filesDirs.iterator();
        String [] result = new String[filesDirs.size()];
        File file = null;
        int counter = 0;

        while (filesIter.hasNext())
        {
            file = (File) filesIter.next();

            if (!file.isFile())
            {
                result[counter] = file.getAbsolutePath();
                counter++;
            }
        }

        return result;
    }

    /**
     * Util method to get the list of folders.
     *
     * @param aDir folder name for which contents have to be listed.
     *
     * @return array of folder names.
     */
    public static synchronized String [] getFolderNameListing(File aDir)
    {
        File [] dirs = aDir.listFiles();

        if (dirs == null)
        {
            return null;
        }

        List filesDirs = Arrays.asList(dirs);

        if (filesDirs == null)
        {
            return null;
        }

        Iterator filesIter = filesDirs.iterator();
        String [] result = new String[filesDirs.size()];
        File file = null;
        int counter = 0;

        while (filesIter.hasNext())
        {
            file = (File) filesIter.next();

            if (!file.isFile())
            {
                result[counter] = file.getName();
                counter++;
            }
        }

        return result;
    }

    /**
     * Returns a WSDL file from the deployment artifact.
     *
     * @param path Path of the ASA
     *
     * @return WSDL file path
     */
    public static String getWSDLFile(String path)
    {
        InputFileFilter myFilter = new InputFileFilter();
        myFilter.setFilterexpression(".*\\.wsdl");

        String wsdlfile = null;

        try
        {
            File dir = new File(path);
            File [] filesAndDirs = dir.listFiles(myFilter);

            if (filesAndDirs == null)
            {
                wsdlfile = null;
            }
            else
            {
                if (filesAndDirs.length > 0)
                {
                    wsdlfile = filesAndDirs[0].getAbsolutePath();
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

        return wsdlfile;
    }

    /**
     * Returns an XML file path from the deployment artifact.
     *
     * @param path path of the deployment artifact.
     *
     * @return XML file path.
     */
    public static String getXMLFile(String path)
    {
        InputFileFilter myFilter = new InputFileFilter();
        myFilter.setFilterexpression(".*\\.xml");

        String xmlfile = null;

        try
        {
            File dir = new File(path);
            File [] filesAndDirs = dir.listFiles(myFilter);

            if (filesAndDirs == null)
            {
                xmlfile = null;
            }
            else
            {
                if (filesAndDirs.length > 0)
                {
                    xmlfile = filesAndDirs[0].getAbsolutePath();
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

        return xmlfile;
    }

    /**
     * Recursively walk a directory tree and return a List of all Files found;
     * the List is sorted using File.compareTo.
     *
     * @param aDir is a valid directory, which can be read.
     *
     * @return list of files.
     */
    private static List getListing(File aDir)
    {
        List result = new ArrayList();
        File [] filesAndDirs = aDir.listFiles(sFilter);

        if (filesAndDirs == null)
        {
            return null;
        }

        List filesDirs = Arrays.asList(filesAndDirs);

        if (filesDirs == null)
        {
            return null;
        }

        Iterator filesIter = filesDirs.iterator();
        File file = null;

        while (filesIter.hasNext())
        {
            file = (File) filesIter.next();

            if (!file.isFile())
            {
                if (sRecurse)
                {
                    List deeperList = getListing(file);
                    result.addAll(deeperList);
                }
            }
            else
            {
                if (FileBindingUtil.canAdd(file))
                {
                    result.add(file);
                }

                if (result.size() >= ConfigData.BATCH_SIZE)
                {
                    return result;
                }
            }
        }

        return result;
    }
}
