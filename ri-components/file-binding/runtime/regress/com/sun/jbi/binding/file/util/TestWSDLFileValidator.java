/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWSDLFileValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestWSDLFileValidator extends TestCase
{
    /**
     * Creates a new TestWSDLFileValidator object.
     *
     * @param testName   
     */
    public TestWSDLFileValidator(java.lang.String testName)
    {
        super(testName);
    }

    /**
     *
     *
     * @return  NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestWSDLFileValidator.class);

        return suite;
    }

    /**
     * Test of getDocument method, of class
     * com.sun.jbi.binding.file.util.WSDLFileValidator.
     */
    public void testGetDocument()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getError method, of class
     * com.sun.jbi.binding.file.util.WSDLFileValidator.
     */
    public void testGetError()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getException method, of class
     * com.sun.jbi.binding.file.util.WSDLFileValidator.
     */
    public void testGetException()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of isValid method, of class
     * com.sun.jbi.binding.file.util.WSDLFileValidator.
     */
    public void testIsValid()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of validate method, of class
     * com.sun.jbi.binding.file.util.WSDLFileValidator.
     */
    public void testValidate()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    // TODO add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
