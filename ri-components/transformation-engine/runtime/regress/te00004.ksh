#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)te00004.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "te00004.ksh- DEPLOY transformation engine test"

. ./regress_defs.ksh

$JBI_ANT -Djbi.deploy.file=$JV_SRCROOT/ri-components/transformation-engine/runtime/regress/deploy/xsltdeploysol.zip deploy-service-assembly

echo "Doing WSDL Based Deployment."

$JBI_ANT -Djbi.deploy.file=$JV_SRCROOT/ri-components/transformation-engine/runtime/regress/deploy/wsdl-xsltdeploysol.zip deploy-service-assembly

echo "Doing WSDL 1.1 Based Deployment."

$JBI_ANT -Djbi.deploy.file=$JV_SRCROOT/ri-components/transformation-engine/runtime/regress/deploy/wsdl11-xsltdepl.zip deploy-service-assembly

echo "Doing CSV Deployment over TE."

$JBI_ANT -Djbi.deploy.file=$JV_SRCROOT/ri-components/transformation-engine/runtime/regress/deploy/csvdeploysol.zip deploy-service-assembly

echo "Completed deploying to xslt engine"

#Starting SA - SunTransformationEngine_Service_Assembly
$JBI_ANT -Djbi.service.assembly.name=SunTransformationEngine_Service_Assembly start-service-assembly

#Starting SA - SunTransformationEngine_CSV_Assembly
$JBI_ANT -Djbi.service.assembly.name=SunTransformationEngine_CSV_Assembly start-service-assembly

#Start Data Feeder Engine so that it will send XML input to TE
echo "Starting DataFeederEngine"
$JBI_ANT -Djbi.component.name=DataFeederEngine start-component

#Stop SA - SunTransformationEngine_Service_Assembly"
$JBI_ANT -Djbi.service.assembly.name=SunTransformationEngine_Service_Assembly stop-service-assembly

#Shutdown SA - SunTransformationEngine_Service_Assembly"
$JBI_ANT -Djbi.service.assembly.name=SunTransformationEngine_Service_Assembly shut-down-service-assembly

#Stop SA - SunTransformationEngine_CSV_Assembly"
$JBI_ANT -Djbi.service.assembly.name=SunTransformationEngine_CSV_Assembly stop-service-assembly

#Shutdown SA - SunTransformationEngine_CSV_Assembly"
$JBI_ANT -Djbi.service.assembly.name=SunTransformationEngine_CSV_Assembly shut-down-service-assembly

#Stop Data Feeder Engine
$JBI_ANT -Djbi.component.name=DataFeederEngine stop-component

#Shutdown Data Feeder Engine
$JBI_ANT -Djbi.component.name=DataFeederEngine shut-down-component

#UninstallData Feeder Engine
$JBI_ANT -Djbi.component.name=DataFeederEngine uninstall-component

#Cleanup Feeder engine files
cd $SRCROOT/ri-components/transformation-engine/runtime/regress/bld/test-classes

rm -f feed.jar
rm -f *.xml
