/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestTransformationProcessorFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import junit.framework.*;
import java.util.logging.Logger;


/**
 * DOCUMENT ME!
 *
 * @author root
 */
public class  TestTransformationProcessorFactory extends TestCase
{

    /**
     * DOCUMENT ME!
     */
    private TransformationProcessorFactory mTransformationProcessorFactory;

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite( TestTransformationProcessorFactory.class);
        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
    }

    /**
     * Test of getImpl method, of class com.sun.jbi.engine.xslt.util.TransformationProcessorFactory.
     */
    public void testGetImpl()
    {
        /*
	System.out.println("testGetImpl");
        TransformationProcessor processor  = TransformationProcessorFactory.getImpl();
        assertNotNull("Retrieved processor is Null", processor);
        */
    }

    /**
     * Test for comparing the Impl, of class com.sun.jbi.engine.xslt.util.TransformationProcessorFactory.
     */
    public void testCompareImpl()
    {
       /*
        System.out.println("testCompare");
        TransformationProcessor processor = TransformationProcessorFactory.getImpl();
        TransformationProcessor processor2 = TransformationProcessorFactory.getImpl();
        assertNotNull("Retrieved processor is not Null", processor);
        assertEquals("Retrieved processor are not same", processor , processor2);
        */
    }

}
