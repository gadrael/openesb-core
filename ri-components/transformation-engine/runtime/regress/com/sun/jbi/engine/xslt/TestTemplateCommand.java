/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestTemplateCommand.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import junit.framework.*;
import java.util.logging.Logger;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.Templates;
import java.io.FileInputStream;


/**
 * DOCUMENT ME!
 *
 * @author root
 */
public class TestTemplateCommand extends TestCase
{

    /**
     * DOCUMENT ME!
     */
    private TransformationImpl mTransformationImpl;
    /**
     * DOCUMENT ME!
     */
    private String mXslt;
    /**
     * DOCUMENT ME!
     */
    private Source mSource;
    /**
     * DOCUMENT ME!
     */
    private Source mBadSource;
    /**
     * DOCUMENT ME!
     */
    private String mBadXslt;
    /**
     * Creates a new TestTemplateCommand object.
     *
     * @param testName DOCUMENT ME!
     */
    public TestTemplateCommand(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestTemplateCommand.class);
        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        String srcroot = System.getProperty("junit.srcroot");
        try
        {
            mXslt = srcroot + "/engine/xslt/regress/data/test.xslt";
            mSource = new StreamSource( new FileInputStream(mXslt));
            mBadXslt = srcroot + "/engine/xslt/regress/data/bad.xslt";
            mBadSource = new StreamSource (new FileInputStream(mBadXslt));
        }
        catch (Exception jbiException)
        {
            jbiException.printStackTrace();
            fail("Source creation failed");
        }
    }

    /**
     * Test of exec method, of class com.sun.jbi.engine.xslt.TemplateCommand
     */
    public void testExec()
    {
        System.out.println("testExec");
        /*TemplateCommand mTemplateCommand = new TemplateCommand("key1", mSource );
        mTemplateCommand.execute();
        Templates tpl = (Templates)TemplateRegistry.getRegistry().get("key1");
        assertNotNull("Template creation failed", tpl);*/
    }

    /**
     * Test of badXslt method, of class com.sun.jbi.engine.xslt.TemplateCommand
     */
    /*
    public void testBadXslt()
    {
        System.out.println("testExec with bad xslt");
        TemplateCommand mTemplateCommand = new TemplateCommand("key1", mBadSource );
        try
        { 
           mTemplateCommand.execute();
        }
        catch(Exception ex)
        {
           ex.printStackTrace();
           return; 
        }
        fail("Exception expected");
    }
    */
}
