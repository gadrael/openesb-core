/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TemplateCommand.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import com.sun.jbi.engine.xslt.framework.Command;
import com.sun.jbi.engine.xslt.util.StringTranslator;
import com.sun.jbi.engine.xslt.TransformationEngineContext;

import java.util.logging.Logger;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;


/**
 * This command contains the request response implementation.
 *
 * @author Sun Microsystems, Inc.
 */
public class TemplateCommand
    implements Command, TEResources
{
    /**
     * This is logger instance used for logging information.
     */
    private Logger mLogger = null;

    /**
     *    
     */
    private Source mSource = null;

    /**
     *    
     */
    private String mService = null;

    /**
     *    
     */
    private StringTranslator mTranslator = null;

    /**
     * Construtor for creating TemplateCommand Object.
     *
     * @param key for replying back to sender
     * @param xslt received by the sender.
     */
    public TemplateCommand(
        String key,
        Source xslt)
    {
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());

        // Do any command specific init.
        mLogger = TransformationEngineContext.getInstance().getLogger("");
        mLogger.info(mTranslator.getString(
                TEResources.TEMPLATECOMMAND_CONSTRUCTOR_START));
        mSource = xslt;
        mService = key;
        mLogger.info(mTranslator.getString(
                TEResources.TEMPLATECOMMAND_CONSTRUCTOR_END));
    }

    /**
     * This is called by workmanager in a free thread. It extracts the symbol
     * from the normalized message, processes it and generates response or
     * fault depending upon the input message.
     */
    public void execute()
    {
        mLogger.info(mTranslator.getString(
                TEResources.TEMPLATECOMMAND_EXECUTE_START));

        TransformerFactory tfactory = null;
        TransformationImpl impl = null;

        while (tfactory == null)
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.GET_TRANSFORMER_FACTORY));

            try
            {
                impl =
                    (TransformationImpl) TransformationProcessorFactory.getImpl();
                tfactory = (TransformerFactory) (impl.getTransformerFactory());
            }
            catch (NullPointerException ex)
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.GOT_NULLPOINTEREXCEPTION));
            }
        }

        try
        {
            Templates tpl = tfactory.newTemplates(mSource);

            //impl.putTransformerFactory(tfactory);
            TemplateRegistry.getRegistry().put(mService, tpl);
        }
        catch (TransformerConfigurationException ex)
        {
            mLogger.severe(mTranslator.getString(TEResources.COMPILATION_OF_XSLT));
        }
        finally
        {
            impl.putTransformerFactory(tfactory);
        }

        mLogger.info(mTranslator.getString(
                TEResources.TEMPLATECOMMAND_EXECUTE_END));
    }
}
