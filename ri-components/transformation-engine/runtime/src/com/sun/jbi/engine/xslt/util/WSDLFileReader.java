/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDLFileReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.wsdl2.Binding;
import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Document;
import com.sun.jbi.wsdl2.Endpoint;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import com.sun.jbi.engine.xslt.TransformationEngineContext;


/**
 * Reads the configuration file service.wsdl and loads the data into the
 * ConfigBean objects.
 *
 * @author Sun Microsystems, Inc.
 */
public class WSDLFileReader
    extends UtilBase
{
    /**
     *
     */
    private Description mDefinition;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     *
     */
    private StringTranslator mStringTranslator;

    /**
     * The list of endpoints as configured in the config file. This list
     * contains all the encpoints and their attibutes
     */
    private ConfigBean [] mServiceList = null;

    /**
     * The total number of end points in the config file
     */
    private int mTotalServices = 0;

    /**
     * Creates a new WSDLFileReader object.
     */
    public WSDLFileReader()
    {
        setValid(true);

        // mResolver = rslv;
        mLog = TransformationEngineContext.getInstance().getLogger("");
        mStringTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
    }

    /**
     * Gets the endpoint list corresponding to a config file.
     *
     * @return End point Bean list
     */
    public ConfigBean [] getServices()
    {
        return mServiceList;
    }

    /**
     * Returns the total number of endopoints in the config file.
     *
     * @return int number of endpoints.
     */
    public int getServicesCount()
    {
        return mTotalServices;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Description doc)
    {
        mDefinition = doc;
        loadServicesList();
    }

    /**
     * DOCUMENT ME!
     *
     * @param map NOT YET DOCUMENTED
     * @param attr NOT YET DOCUMENTED
     *
     * @return NOT YET DOCUMENTED
     */
    private String getAttrValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Utility method for setting the Bean object from the XML Nodes.
     *
     * @param node The node which needs to be
     * @param sb The end point bean object which has to be updated
     * @param tagName the tag name which needs to be read.
     */
    private void setByTagName(
        Node node,
        ConfigBean sb,
        String tagName)
    {
        Element ele = (Element) node;

        //        mLog.finer(" Element: "+ele.toString());
        //        mLog.finer(" tagName: "+tagName);
        NodeList namelist =
            ele.getElementsByTagNameNS(ConfigData.TE_NAMESPACE, tagName);

        if (namelist != null)
        {
            for (int i = 0; i < namelist.getLength(); i++)
            {
                Element name = (Element) namelist.item(i);
                String sValue = null;

                try
                {
                    sValue =
                        ((Node) (name.getChildNodes().item(0))).getNodeValue()
                         .trim();
                }
                catch (NullPointerException ne)
                {
                    ne.printStackTrace();

                    return;
                }

                mLog.finer("** " + tagName + " = " + sValue + " **");
                sb.setValue(tagName, sValue);
            }
        }
        else
        {
            mLog.finer("namelist null for tagname " + tagName);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param nodeList - Node List
     * @param name - element name (need not be an extension element)
     *
     * @return Node matching node.
     */
    private Node getElementNodeByName(
        NodeList nodeList,
        String name)
    {
        Node retNode = null;

        for (int g = 0; g < nodeList.getLength(); g++)
        {
            Node n = nodeList.item(g);

            if ((n != null) && (n.getNodeType() == Node.ELEMENT_NODE))
            {
                //mLog.finer("Node:"+n.getNodeName());
                if ((n.getLocalName().equals(name)))
                {
                    retNode = n;

                    break;
                }
            }
        }

        // outer for loop
        return retNode;
    }

    /**
     * Mehotd to determine the binding information in the Document fragment
     *
     * @param df document fragment
     *
     * @return boolean TRUE/FALSE if the Binding is of type TE
     */
    private boolean isTEBinding(DocumentFragment df)
    {
        NamedNodeMap n = df.getFirstChild().getAttributes();

        for (int g = 0; g < n.getLength(); g++)
        {
            if (n.item(g).getLocalName().equals("type"))
            {
                if (n.item(g).getNodeValue().equals(ConfigData.SERVICE_ENGINE_TYPE))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * DOCUMENT ME!
     *
     * @param map NOT YET DOCUMENTED
     * @param attr NOT YET DOCUMENTED
     *
     * @return NOT YET DOCUMENTED
     */
    private String getValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItemNS(ConfigData.TE_NAMESPACE, attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * DOCUMENT ME!
     *
     * @param ep NOT YET DOCUMENTED
     * @param eb NOT YET DOCUMENTED
     */
    private void loadConfigBean(
        Endpoint ep,
        ConfigBean eb)
    {
        DocumentFragment docfrag = ep.toXmlDocumentFragment();

        //Endpoint Node.
        Node epnode = docfrag.getFirstChild();
        NamedNodeMap epattributes = epnode.getAttributes();

        Node colh =
            getElementNodeByName(epnode.getChildNodes(),
                ConfigData.COLUMN_HEADERS);

        if (colh != null)
        {
            setByTagName(epnode, eb, ConfigData.COLUMN_HEADERS);
            setByTagName(colh, eb, ConfigData.COLUMN_HEADER);
        }

        if (!isValid())
        {
            setError("\n\tEndpoint : " + ep.getName() + getError());
        }
    }

    /**
     * Parses the config files and loads them into bean objects.
     */
    private void loadServicesList()
    {
        int services = mDefinition.getServicesLength();
        List eplist = new LinkedList();

        //mLog.finer("Found "+services+" Service(s).");
        for (int i = 0; i < services; i++)
        {
            try
            {
                com.sun.jbi.wsdl2.Service ser = mDefinition.getService(i);

                mLog.finer("Service ser= "+ser.toXmlString());
                int endpoints = ser.getEndpointsLength();

                for (int k = 0; k < endpoints; k++)
                {
                    Endpoint ep = ser.getEndpoint(k);
                    Binding b = ep.getBinding();

                    mLog.finer("Binding b= "+b.toXmlString());
                    if (!isTEBinding(b.toXmlDocumentFragment()))
                    {
                        continue;
                    }

                    ConfigBean eb = new ConfigBean();
                    //loadConfigBean(ep, eb);
                    eb.setValue(ConfigData.SERVICE_NAMESPACE, ser.getQName().getNamespaceURI());
                    eb.setValue(ConfigData.TARGET_NAMESPACE, ser.getQName().getNamespaceURI());

                    //eb.setValue(ConfigData.SERVICE_NAME,ser.getQName().getLocalPart());
                    eb.setValue(ConfigData.SERVICENAME_LOCALPART, ser.getQName().getLocalPart());
                    eb.setValue(ConfigData.ENDPOINT, ep.getName());

                    if (!isValid())
                    {
                        setError(("\n\t" + "Service : "
                            + ser.getQName().toString() + getError()));
                        eplist.clear();

                        return;
                    }

                    eplist.add(eb);
		    TransformationEngineContext.getInstance().addEndpointDoc(ser.getQName().toString()+ ep.getName(), mDefinition.toXmlDocument());
                }
            }
            catch (Exception e)
            {
                setError("Error occurred during endpoint validation "
                    + e.getMessage());
                setException(e);
                setValid(false);

                return;
            }
        }

        mServiceList = new ConfigBean[eplist.size()];

        for (int x = 0; x < eplist.size(); x++)
        {
            mServiceList[x] = (ConfigBean) eplist.get(x);
        }

        mTotalServices = eplist.size();
    }
}
