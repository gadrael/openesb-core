/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.engine.xslt.TEResources;
import com.sun.jbi.engine.xslt.TransformationEngineContext;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import javax.jbi.JBIException;
import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.MessagingException;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * This class manages the service requests and dispatches the requests to be
 * executed to work manager.
 *
 * @author Sun Microsystems, Inc.
 */
public final class ServiceManager
    implements TEResources
{
    /**
     * Handle to the Work Manager instance.
     */
    private static ServiceManager sServiceManager = null;

    /**
     * Handle to the EngineChannel instance.
     */
    /**
     * Handle to bucket of service instance.
     */
    private static Hashtable sServiceBucket = null;

    /**
     * Handle to bucket of all deployed instances.
     */
    private static Hashtable sDeployBucket = null;

    /**
     * Internal handle to javax.jbi.component.ComponentContext
     */
    private ComponentContext mContext = null;

    /**
     * Internal handle to the logger instance.
     */
    private Logger mLogger;

    /**
     *    
     */
    private StringTranslator mTranslator =
        new StringTranslator("com.sun.jbi.engine.xslt",
            this.getClass().getClassLoader());

    /**
     * Creates a new instance of ServiceManager.
     */
    private ServiceManager()
    {
        init();
    }

    /**
     * Returns a handle to the Service Manager instance.
     *
     * @return a service manager instance
     */
    public static ServiceManager getInstance()
    {
        if (sServiceManager == null)
        {
            sServiceManager = new ServiceManager();
        }

        return sServiceManager;
    }

    /**
     * DOCUMENT ME!
     *
     * @param context NOT YET DOCUMENTED
     */
    public void setComponentContext(
        javax.jbi.component.ComponentContext context)
    {
        mContext = context;
        if (mLogger == null)
        {
            mLogger = TransformationEngineContext.getInstance().getLogger("");        
        }
    }

    /**
     * Gets the status of the asa id.
     *
     * @param asId ASA Id
     *
     * @return true if deployed , false otherwise.
     */
    public boolean getDeploymentStatus(String asId)
    {
        Set keyset = sDeployBucket.keySet();
        Iterator iter = keyset.iterator();

        while (iter.hasNext())
        {
            String ser = (String) iter.next();

            if (ser.trim().equals(asId))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * DOCUMENT ME!
     *
     * @param svcname NOT YET DOCUMENTED
     *
     * @return NOT YET DOCUMENTED
     */
    public Service getService(String svcname)
    {
        Service service = (Service) sServiceBucket.get(svcname);

        return service;
    }

    /**
     * DOCUMENT ME!
     *
     * @param bean NOT YET DOCUMENTED
     */
    public void createService(ConfigBean bean)
    {
        // Overides existing service with same service name
        String serviceName = null;

        // Overides existing service with same service name
        String namespace = null;

        // Overides existing service with same service name
        String endpoint = null;

        Vector vec = (Vector) bean.getValue(ConfigData.SERVICENAME_LOCALPART);

        if (vec != null)
        {
            serviceName = (String) (vec.elementAt(0));
        }

        //Changed to use namespace-uri element from service.xml instead of 
        //targetNamespace
        Vector vec2 = (Vector) bean.getValue(ConfigData.SERVICE_NAMESPACE);

        if (vec2 != null)
        {
            mLogger.finer("There are "+vec2.size()+" namespace attributes set for this service.");
            namespace = (String) (vec2.elementAt(0));
            if(namespace != null)
            {
                mLogger.finer("namespace in createService is " + namespace);
            }
            else
            {
                mLogger.finer("namespace in createService is NULL");
            }
        }

        Vector vec3 = (Vector) bean.getValue(ConfigData.ENDPOINT);

        if (vec3 != null)
        {
            endpoint = (String) (vec3.elementAt(0));
        }

        if (serviceName != null)
        {
            serviceName = serviceName.trim();

            QName que = new QName(namespace, serviceName);
            Service newService = new Service(bean);

            if (newService != null)
            {
                newService.setName(que.toString());
                newService.setEndpoint(endpoint);
                sServiceBucket.put(que.toString(), (Service) newService);
                mLogger.finer(mTranslator.getString(TEResources.SERVICE_CREATED, que.toString(), endpoint));
                mLogger.finer("Got delimiter" + newService.getType());
                //mLogger.finer("Activated service " + que.toString() + " endpoint " + endpoint);
            }
            else
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.ERR_CASE_SERVIVCE_COULD_NOT_BE_CREATED)
                    + que.toString());
            }
        }
        else
        {
            mLogger.severe(mTranslator.getString(TEResources.SERVICE_NULL));
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param serviceUnitID NOT YET DOCUMENTED
     * @param beans NOT YET DOCUMENTED
     */
    public void createServices(
        String serviceUnitID,
        ConfigBean [] beans)
    {
        int length = beans.length;

        for (int i = 0; i < length; i++)
        {
            ConfigBean bean = (ConfigBean) beans[i];
            createService(bean);
        }

        sDeployBucket.put(serviceUnitID, beans);
    }

    /**
     * DOCUMENT ME!
     *
     * @param bean NOT YET DOCUMENTED
     */
    public void destroyService(ConfigBean bean)
    {
        String svcname = null;
        String namespace = null;

        Vector vec = (Vector) bean.getValue(ConfigData.SERVICENAME_LOCALPART);

        if (vec != null)
        {
            svcname = (String) (vec.elementAt(0));
        }

        Vector vec2 = (Vector) bean.getValue(ConfigData.SERVICE_NAMESPACE);

        if (vec2 != null)
        {
            namespace = (String) (vec2.elementAt(0));
        }

        if (svcname != null)
        {
            svcname = svcname.trim();
            namespace = namespace.trim();

            QName que = new QName(namespace, svcname);

            //stopService(bean);
            mLogger.finer(mTranslator.getString(TEResources.SERVICE_DESTROYED, que.toString()) );
            sServiceBucket.remove(svcname);
        }
        else
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.SERVICENAME_NULL_CANT_DESTROY));
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param serviceUnitID NOT YET DOCUMENTED
     */
    public void destroyServices(String serviceUnitID)
    {
        ConfigBean [] beans = (ConfigBean []) sDeployBucket.get(serviceUnitID);

        if (beans == null)
        {
            return;
        }

        int length = beans.length;

        for (int i = 0; i < length; i++)
        {
            destroyService((ConfigBean) beans[i]);
        }

        sDeployBucket.remove(serviceUnitID);
    }

    /**
     *
     */
    public void startAllServices()
    {
        mLogger.info(mTranslator.getString(TEResources.START_ALL_SERVICE));

        Collection list = (Collection) sServiceBucket.values();

        if (list == null)
        {
            return;
        }

        Iterator all = list.iterator();

        while (all.hasNext())
        {
            Service service = (Service) all.next();

            if (service != null)
            {
                try
                {
                    QName name = QName.valueOf(service.getName());
                    mLogger.finer("QName activated is =" + name);

                    ServiceEndpoint ref =
                        mContext.activateEndpoint(name, service.getEndpoint());
                    mLogger.info("Service activated is ="
                        + ref.getServiceName() + " endpoint "
                        + ref.getEndpointName());
                    service.setReference(ref);
                    mLogger.finer(mTranslator.getString(TEResources.TE_SERVICE)
                        + name + ":" + service.getEndpoint()
                        + mTranslator.getString(TEResources.IS_ACTIVATED));
                    service.start();
                }
                catch (MessagingException ex)
                {
                    mLogger.severe(mTranslator.getString(
                            TEResources.SERVICE_COULD_NOT_BE_ACTIVATED)
                        + service.getName());
                }
                catch (JBIException jbie)
                {
                    mLogger.severe(mTranslator.getString(
                            TEResources.SERVICE_COULD_NOT_BE_ACTIVATED)
                        + service.getName());
                }
            }
            else
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.RETRIVED_SERVICE_NULL));
            }
        }

        mLogger.info(mTranslator.getString(TEResources.STARTALLSERVICE_END));
    }

    /**
     * DOCUMENT ME!
     *
     * @param bean NOT YET DOCUMENTED
     */
    public void startService(ConfigBean bean)
    {
        String svcname = null;
        String namespace = null;

        Vector vec = (Vector) bean.getValue(ConfigData.SERVICENAME_LOCALPART);

        if (vec != null)
        {
            svcname = (String) (vec.elementAt(0));
        }

        //Changed to use Servie Namespace (namespace-uri element in service.xml)
        Vector vec2 = (Vector) bean.getValue(ConfigData.SERVICE_NAMESPACE);

        if (vec2 != null)
        {
            namespace = (String) (vec2.elementAt(0));
        }

        if (svcname != null)
        {
            svcname = svcname.trim();
            namespace = namespace.trim();

            QName que = new QName(namespace, svcname);
            Service service = getService(que.toString());

            if (service != null)
            {
                try
                {
                    QName name = QName.valueOf(service.getName());
                    mLogger.finer("QName activated is =" + name);

                    ServiceEndpoint ref =
                        mContext.activateEndpoint(name, service.getEndpoint());
                    mLogger.info("Service activated is ="
                        + ref.getServiceName() + ":" + service.getEndpoint());
                    service.setReference(ref);
                    mLogger.finer(mTranslator.getString(TEResources.TE_SERVICE)
                        + name
                        + mTranslator.getString(TEResources.IS_ACTIVATED));
                    service.start();
                }
                catch (MessagingException ex)
                {
                    mLogger.severe(mTranslator.getString(
                            TEResources.SERVICE_COULD_NOT_BE_ACTIVATED)
                        + svcname);
                }
                catch (JBIException jbie)
                {
                    mLogger.severe(mTranslator.getString(
                            TEResources.SERVICE_COULD_NOT_BE_ACTIVATED)
                        + service.getName());
                }
            }
            else
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.SERVICE_RETREVAL_FAILED));
            }
        }
        else
        {
            mLogger.severe(mTranslator.getString(TEResources.SERVICENAME_NULL));
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param serviceUnitID service unit ID
     */
    public void startServices(String serviceUnitID)
    {
        //Get all the deployed services for this serviceUnitID
        ConfigBean [] beans = (ConfigBean []) sDeployBucket.get(serviceUnitID);

        if (beans == null)
        {
            return;
        }

        //Start them
        int length = beans.length;

        for (int i = 0; i < length; i++)
        {
            ConfigBean bean = (ConfigBean) beans[i];
            startService(bean);
        }
    }

    /**
     *
     */
    public void stopAllServices()
    {
        Collection list = (Collection) sServiceBucket.values();

        if (list == null)
        {
            return;
        }

        Iterator all = list.iterator();

        while (all.hasNext())
        {
            Service service = (Service) all.next();
            String svcname = (String) service.getName();

            if (svcname != null)
            {
                svcname = svcname.trim();

                ServiceEndpoint ref = service.getReference();

                if( ref != null)
                {
                    try
                    {
                        mContext.deactivateEndpoint(ref);
                    }
                    catch (Exception ex)
                    {
                        mLogger.severe(mTranslator.getString(
                                TEResources.FAILED_TO_DEACTIVATE));
                        ex.printStackTrace();
                    }
                }
                else
                {
                    continue;
                }

                service.stop();
                mLogger.finer(mTranslator.getString(
                        TEResources.SERVICE_IS_BEING_STOPPED) + svcname);
            }
            else
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.SERVICE_RETREVAL_FAILED_STOP_FAILED));
            }
        }

        mLogger.info(mTranslator.getString(TEResources.ALL_SERVICES_STOPPED));
    }

    /**
     * Stops the Work Manager. The method changes the state of the instance to
     * "STOP". The Work Manager thread keeps checking the state of the
     * instance and it starts the clean up when its state is "STOP".
     *
     * @param bean ConfigBean object
     */
    public void stopService(ConfigBean bean)
    {
        String svcname = null;
        String namespace = null;

        //Changed the service.xsd to represent service-name in QName format.
        Vector vec = (Vector) bean.getValue(ConfigData.SERVICENAME_LOCALPART);

        if (vec != null)
        {
            svcname = (String) (vec.elementAt(0));
        }

        Vector vec2 = (Vector) bean.getValue(ConfigData.SERVICE_NAMESPACE);

        if (vec2 != null)
        {
            namespace = (String) (vec2.elementAt(0));
        }

        if (svcname != null)
        {
            svcname = svcname.trim();
            namespace = namespace.trim();

            QName que = new QName(namespace, svcname);
            Service service = getService(que.toString());

            if (service != null)
            {
                ServiceEndpoint ref = service.getReference();
		if(ref != null)
		{

                    try
                    {
                        mContext.deactivateEndpoint(ref);
                    }
                    catch (Exception ex)
                    {
                        mLogger.severe(mTranslator.getString(
                                TEResources.FAILED_TO_DEACTIVATE));
                        ex.printStackTrace();
                    }

                    service.stop();
                    mLogger.info(mTranslator.getString(TEResources.SERVICE_IS)
                        + service.getName()
                        + mTranslator.getString(TEResources.BEING_STOPPED));
                }
                else
		{
		  mLogger.severe("ServiceManager. NULL ServiceEndpoint. Check...");
		}
            }
            else
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.SERVICE_RETREVAL_FAILED_STOP_FAILED));
            }
        }
        else
        {
            mLogger.severe(mTranslator.getString(TEResources.SERVICENAME_NULL));
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param serviceUnitID NOT YET DOCUMENTED
     */
    public void stopServices(String serviceUnitID)
    {
        //Get All Deployed Services for this serviceUnitID and stop them
        ConfigBean [] beans = (ConfigBean []) sDeployBucket.get(serviceUnitID);

        if (beans == null)
        {
            return;
        }

        int length = beans.length;

        for (int i = 0; i < length; i++)
        {
            ConfigBean bean = (ConfigBean) beans[i];
            stopService(bean);
        }
    }

    /**
     * Initializes the Logger.
     */
    private void init()
    {
        sDeployBucket = new Hashtable();
        sServiceBucket = new Hashtable();
    }
}
