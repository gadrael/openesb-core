/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Service.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.engine.xslt.TEResources;

import java.util.Enumeration;
import java.util.Vector;

import javax.jbi.servicedesc.ServiceEndpoint;


/**
 * 
 *
 * @author Sun Microsystems Inc.
 */
public class Service
    implements TEResources
{
    /**
     *    
     */
    private ConfigBean mConfigBean = null;

    /**
     *    
     */
    private ServiceEndpoint mReference = null;

    /**
     *    
     */
    private String mDelimiter = null;

    /**
     *    
     */
    private String mEndpoint = null;

    /**
     *    
     */
    private String mId = null;

    /**
     *    
     */
    private String mName = null;

    /**
     *    
     */
    private String mNamespace = null;

    /**
     *    
     */
    private String mStatus = ConfigData.NONACTIVE;

    /**
     *    
     */
    private String mType = null;

    /**
     *    
     */
    private String mXsltFile = null;

    /**
     *    
     */
    private StringTranslator mTranslator = null;

    /**
     *    
     */
    private Vector mColNames = null;

    /**
     *    
     */
    private boolean mFirst = false;
    
    /**
     *    
     */
    private String mDeploymentType = null;

    /**
     * Creates a new Service object.
     *
     * @param bean ConfigBean object.
     */
    public Service(ConfigBean bean)
    {
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        mConfigBean = bean;
        init();
    }

    /**
     * DOCUMENT ME!
     *
     * @param list NOT YET DOCUMENTED
     */
    public void setColumnNames(Vector list)
    {
        mColNames = list;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public Vector getColumnNames()
    {
        return mColNames;
    }

    /**
     * DOCUMENT ME!
     *
     * @param separator NOT YET DOCUMENTED
     */
    public void setColumnSeparator(String separator)
    {
        mDelimiter = separator;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getColumnSeparator()
    {
        return mDelimiter;
    }

    /**
     * DOCUMENT ME!
     *
     * @param id NOT YET DOCUMENTED
     */
    public void setDeploymentId(String id)
    {
        mId = id;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getDeploymentId()
    {
        return mId;
    }

    /**
     * DOCUMENT ME!
     *
     * @param endname NOT YET DOCUMENTED
     */
    public void setEndpoint(String endname)
    {
        mEndpoint = endname;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getEndpoint()
    {
        return mEndpoint;
    }

    /**
     * DOCUMENT ME!
     *
     * @param val NOT YET DOCUMENTED
     */
    public void setFirstRowColHeaders(String val)
    {
        if (val != null)
        {
            mFirst = Boolean.getBoolean(val);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public boolean getFirstRowColHeaders()
    {
        return mFirst;
    }

    /**
     * DOCUMENT ME!
     *
     * @param svcname NOT YET DOCUMENTED
     */
    public void setName(String svcname)
    {
        mName = svcname;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getName()
    {
        return mName;
    }

    /**
     * DOCUMENT ME!
     *
     * @param name NOT YET DOCUMENTED
     */
    public void setNamespace(String name)
    {
        mNamespace = name;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getNamespace()
    {
        return mNamespace;
    }

    /**
     * DOCUMENT ME!
     *
     * @param ref NOT YET DOCUMENTED
     */
    public void setReference(ServiceEndpoint ref)
    {
        mReference = ref;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public ServiceEndpoint getReference()
    {
        return mReference;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getStatus()
    {
        return mStatus;
    }

    /**
     * DOCUMENT ME!
     *
     * @param file NOT YET DOCUMENTED
     */
    public void setStyleSheet(String file)
    {
        mXsltFile = file;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getStyleSheet()
    {
        return mXsltFile;
    }

    /**
     * Sets the deployment type (wsdl_11, wsdl_20 or xml)
     *
     * @param type NOT YET DOCUMENTED
     */
    public void setDeploymentType(String type)
    {
        mDeploymentType = type;
    }

    /**
     * Return the type of the deployment (wsdl_11, wsdl_20 or xml)
     *
     * @return NOT YET DOCUMENTED
     */
    public String getDeploymentType()
    {
        return mDeploymentType;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @param type NOT YET DOCUMENTED
     */
    public void setType(String type)
    {
        mType = type;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String getType()
    {
        return mType;
    }

    /**
     *
     */
    public void init()
    {
        String stylesheet = null;
        String deploymentid = null;
        String deploymentType = null;
        String delimiter = null;
        String firstRow = null;
        String namespace = null;

        mStatus = ConfigData.ACTIVE;

        Vector vec =
            (Vector) mConfigBean.getValue(mTranslator.getString(
                    TEResources.XSLTFILE));

        if (vec != null)
        {
            stylesheet = (String) (vec.elementAt(0));
        }

        Vector vec2 =
            (Vector) mConfigBean.getValue(mTranslator.getString(
                    TEResources.DEPLOYMENT_ID));

        if (vec2 != null)
        {
            deploymentid = (String) (vec2.elementAt(0));
        }

        Vector someVec = (Vector) mConfigBean.getValue(ConfigData.DEPLOYMENT_TYPE);
        if(someVec !=null)
        {
            deploymentType = (String) (someVec.elementAt(0));
        }
        
        setDeploymentType(deploymentType);
        setStyleSheet(stylesheet);
        setDeploymentId(deploymentid);

        Vector vec3 =
            (Vector) mConfigBean.getValue(ConfigData.COLUMN_SEPARATOR);

        if (vec3 != null)
        {
            String temp = (String) (vec3.elementAt(0));

            if ((temp != null) && (temp.length() > 0))
            {
                delimiter = temp;
                System.out.println("#########Retived delimiter is ="
                    + delimiter + "*");
            }
        }

        Vector headers =
            (Vector) mConfigBean.getValue(ConfigData.COLUMN_HEADER);
        Vector vec4 =
            (Vector) mConfigBean.getValue(ConfigData.FIRST_ROW_COL_HEADERS);

        if (vec4 != null)
        {
            firstRow = (String) (vec4.elementAt(0));
        }

        setColumnSeparator(delimiter);
        System.out.println("#####Retived delimiter is==" + delimiter + "*");

        if (delimiter == null)
        {
            setType(mTranslator.getString(TEResources.XML));
        }
        else
        {
            setType(mTranslator.getString(TEResources.NON_XML));
        }

        System.out.println("######Type" + getType());
        setFirstRowColHeaders(firstRow);

        if (!getFirstRowColHeaders())
        {
            setColumnNames(headers);
        }

        //Changed to use Servie Namespace (service-namespace element in service.xml)
        Vector vec5 =
            ((Vector) mConfigBean.getValue(ConfigData.SERVICE_NAMESPACE));

        if (vec5 != null)
        {
            setNamespace((String) vec5.elementAt(0));
        }
    }

    /**
     *
     */
    public void start()
    {
        mStatus = ConfigData.START;
    }

    /**
     *
     */
    public void stop()
    {
        mStatus = ConfigData.STOP;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public String toString()
    {
	//Changed variable name from enum to enumKeys as enum is a keyword in JDK 1.5
        Enumeration enumKeys = mConfigBean.getTable().keys();
        StringBuffer buff = new StringBuffer();

        while (enumKeys.hasMoreElements())
        {
            Object key = enumKeys.nextElement();
            buff.append("\n|  Key =");
            buff.append(key.toString());

            Vector vec = (Vector) mConfigBean.getTable().get(key);
            buff.append("Value=");
            buff.append(vec.toString());
            buff.append("|");
        }

        return buff.toString();
    }
}
