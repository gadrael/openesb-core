/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TemplateRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import com.sun.jbi.engine.xslt.util.StringTranslator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Templates;


/**
 * This command contains the request response implementation.
 *
 * @author Sun Microsystems, Inc.
 */
public final class TemplateRegistry
    implements TEResources
{
    /**
     *    
     */
    private static TemplateRegistry sTemplateRegistry = null;

    /**
     *    
     */
    private Map mMap;

    /**
     * Construtor for creating TemplateRegistry Object.
     */
    private TemplateRegistry()
    {
        // Do any command specific init.
        //TEMPLATEREGISTRY_CONSTRUCTOR_START));
        mMap = Collections.synchronizedMap(new HashMap());

        //TEMPLATEREGISTRY_CONSTRUCTOR_END));
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static TemplateRegistry getRegistry()
    {
        if (sTemplateRegistry == null)
        {
            sTemplateRegistry = new TemplateRegistry();
        }

        return sTemplateRegistry;
    }

    /**
     * DOCUMENT ME!
     *
     * @param key NOT YET DOCUMENTED
     *
     * @return NOT YET DOCUMENTED
     */
    public Templates get(String key)
    {
        return (Templates) mMap.get(key);
    }

    /**
     * DOCUMENT ME!
     *
     * @param key NOT YET DOCUMENTED
     * @param tpl NOT YET DOCUMENTED
     */
    public void put(
        String key,
        Templates tpl)
    {
        mMap.put(key, tpl);
    }
}
