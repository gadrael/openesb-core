/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FooEngine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package testengine2;

import org.w3c.dom.DocumentFragment;

import java.io.File;

import java.util.logging.Logger;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.RobustInOnly;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * This class implements EngineLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Sun Microsystems, Inc.
 */
public class FooEngine implements ComponentLifeCycle, Component
{
    /**
     * In Only MEP.
     */
    public static final String IN_ONLY =
        "http://www.w3.org/ns/wsdl/in-only";

    /**
     * In Out MEP.
     */
    public static final String IN_OUT = "http://www.w3.org/ns/wsdl/in-out";

    /**
     * In Optional Out MEP.
     */
    public static final String IN_OPTIONAL_OUT =
        "http://www.w3.org/ns/wsdl/in-opt-out";

/**
     * Robust In Only MEP.
     */
    public static final String ROBUST_IN_ONLY =
        "http://www.w3.org/ns/wsdl/robust-in-only";

    /**
     *    
     */
    private static final String INOUT_SERVICE_NAME = "test:inoutservice";

    /**
     *    
     */
    private static final String INONLY_SERVICE_NAME = "test:inonlyservice";

    /**
     *    
     */
    private static final String ROBUSTINONLY_SERVICE_NAME =
        "test:robustinonlyservice";

    /**
     *    
     */
    private static final String OUTBOUND_SERVICE_NAME =
        "AdaptionService";

    
    private static final String OUTBOUND_SERVICE_NAMESPACE =
        "http://www.sun.com/jbi/servicelist.wsdl";
    /**
     *    
     */
    private static final String ENDPOINT = "testendpoint";

    /**
     *    
     */
    public Thread mRecv;

    /**
     *    
     */
    private ServiceEndpoint mEndpoint;

    /**
     *
     */

    /**
     *    
     */
    private DeliveryChannel mChannel;
    
    /**
     *    
     */
    private MessageExchangeFactory mFactory;

    /**
     * Environment conext passed down from framework to this stockquote engine.
     */
    private ComponentContext mContext = null;

    /**
     * Refernce to logger.
     */
    private Logger mLog = null;

    /**
     *    
     */
    private MessageExchange mReq;

    /**
     *    
     */
    private ServiceEndpoint mInonlyReference;

    /**
     *    
     */
    private ServiceEndpoint mInoutReference;

    /**
     *    
     */
    private ServiceEndpoint mOutboundReference;

    /**
     *    
     */
    private ServiceEndpoint mRobustinonlyReference;

    /**
     *    
     */
    private boolean mRunFlag = true;

    /**
     * Get the JMX ObjectName for the DeploymentMBean for this BPE. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the DeploymentMBean or null if
     *         there is no DeploymentMBean.
     */
    public javax.management.ObjectName getDeploymentMBeanName()
    {
        return null;
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Initialize the test engine. This performs initialization required
     *
     * @param jbiContext the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the stockquote engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext jbiContext)
        throws javax.jbi.JBIException
    {
        mLog = Logger.getLogger(this.getClass().getPackage().getName());
        mLog.info("test engine INIT invoked");

        if (jbiContext == null)
        {
            throw (new javax.jbi.JBIException("Context is null",
                new NullPointerException()));
        }

        /* Assign values */
        mContext = jbiContext;
        Payload.mDefaultPath =
            mContext.getInstallRoot() + File.separatorChar + "testengine2"
            + File.separatorChar + "payload.xml";
        mLog.info(" INIT METHOD FINISHED ");
    }

    /**
     * Shutdown the SE. This performs cleanup before the SE is terminated.
     * Once this has been called, init() must be called before the SE can be
     * started again with a call to start().
     *
     * @throws javax.jbi.JBIException if the SE is unable to shut down.
     */
    public void shutDown() throws javax.jbi.JBIException
    {
        mLog.info("test Engine Shutdown-Start");
        mLog.info("test Engine Shutdown-End");
    }

    /**
     */
    public void start() throws javax.jbi.JBIException
    {
        mLog.info(" Test  ENGINE INVOKED ");

        try
        {
            mChannel = mContext.getDeliveryChannel();
            mFactory = mChannel.createExchangeFactory();
            /*mOutboundReference =
                mChannel.activateEndpoint(new QName(OUTBOUND_SERVICE_NAMESPACE,
                        OUTBOUND_SERVICE_NAME), OUTBOUND_SERVICE_NAME);
             */
        }
        catch (MessagingException me)
        {
            me.printStackTrace();
        }

        if (mChannel == null)
        {
            mLog.severe("Cannot get Engine Channel from context ");

            return;
        }

        doTestOutIn();

        //  doTestOutOnly();
        /*
           doTestGoodRobustOutOnly();
        
           doTestBadRobustOutOnly();
        
           mRecv = new Thread(new Receiver());
           mRecv.start();
        
           String compRoot = mContext.getComponentRoot();
         */
        mLog.info(" START FINISHED  ");
    }

    /**
     */
    public void stop() throws javax.jbi.JBIException
    {
        mLog.info("STOP INVOKED ");
        mRunFlag = false;

        try
        {
            mRecv.interrupt();
            mRecv.join();
        }
        catch (Exception e)
        {
            ;
        }

        mLog.info(" STOP FINISHED ");
    }


    /**
     *
     */
    private void doTestGoodInOnly()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            InOnly inonly = (InOnly) mReq;

            if (inonly.getInMessage() == null)
            {
                mLog.info("Test-GOODINONLY-Failed");

                return;
            }

            if (inonly.getInMessage().getContent() == null)
            {
                mLog.info("Test-GOODINONLY-Failed");

                return;
            }

            inonly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inonly);
            mLog.info("Test-GOODINONLY-Passed");
        }
        catch (Exception e)
        {
            mLog.severe("Test-GOODINONLY-threw-Exception" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestGoodInOut()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            InOut inout = (InOut) mReq;

            if (inout.getStatus() == ExchangeStatus.DONE)
            {
                mLog.info("Test-GOODINOUT-Passed");

                return;
            }

            if (inout.getStatus() == ExchangeStatus.ERROR)
            {
                mLog.info("Test-GOODINOUT-Failed");

                return;
            }

            if (inout.getInMessage() == null)
            {
                mLog.info("Test-GOODINOUT-Failed");

                return;
            }

            inout.setOutMessage(inout.getInMessage());
            mChannel.send(inout);
        }
        catch (Exception e)
        {
            mLog.severe("Test-GOODINOUT-threw-Exception" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestGoodRobustInOnly()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            RobustInOnly robinonly = (RobustInOnly) mReq;

            if (robinonly.getInMessage() == null)
            {
                mLog.info("Test-GOODROBUSTINONLY-Failed");
                robinonly.setError(new Exception("Message is null"));
                mChannel.send(robinonly);

                return;
            }

            if (robinonly.getInMessage().getContent() == null)
            {
                mLog.info("Test-GOODROBUSTINONLY-Failed");

                return;
            }

            robinonly.setStatus(ExchangeStatus.DONE);
            mChannel.send(robinonly);
            mLog.info("Test-GOODROBUSTINONLY-Passed");
        }
        catch (Exception e)
        {
            mLog.severe("Test-GOODROBUSTINONLY-threw-Exception"
                + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestOutIn()
    {
        mLog.info("Test engine sending good out-in message to engine");

        NormalizedMessage outMsg;
        InOut inout;

        try
        {
            // create the exchange
            inout = mFactory.createInOutExchange();
            outMsg = inout.createMessage();

            // set the stuff we know
            //outIn.setService(mOutboundReference);
            inout.setOperation(new QName(OUTBOUND_SERVICE_NAMESPACE,"inputxml"));

            //ServiceEndpoint [] mEndpoint;
            ServiceEndpoint  mEndpoint;

            // lookup the endpoint reference and set on exchange
            mEndpoint = mContext.getEndpointsForService(
                    new QName(OUTBOUND_SERVICE_NAMESPACE, OUTBOUND_SERVICE_NAME))[0];

/*
            for (int i = 0; i < mEndpoint.length; i++)
            {
                if (mEndpoint[i].isLocal())
                {
                    outIn.setEndpoint(mEndpoint[i]);
                }
            }
*/
            // set the payload
            Payload.setPayload(outMsg);
            inout.setService(new QName(OUTBOUND_SERVICE_NAMESPACE, OUTBOUND_SERVICE_NAME));
            inout.setEndpoint(mEndpoint);
            // set the message on the exchange
            inout.setInMessage(outMsg);

            // send the exchange
            mChannel.send(inout);

            // receive the response
            inout = (InOut) mChannel.accept();

            if (inout.getStatus() == ExchangeStatus.ERROR)
            {
                mLog.info("Test-SEQ-GOODOUTIN-Failed");

                return;
            }
            else if (inout.getStatus() == ExchangeStatus.ACTIVE)
            {
                if (inout.getOutMessage() != null)
                {
                    mLog.info("Test-SEQ-GOODOUTIN-Passed");
                }
                else
                {
                    mLog.info("Test-SEQ-GOODOUTIN-Failed");
                }
            }

            inout.setStatus(ExchangeStatus.DONE);
            mChannel.send(inout);
        }
        catch (Exception e)
        {
            mLog.severe("Test-SEQ-GOODOUTIN-threw-Exception");
            e.printStackTrace();
        }
    }

    /**
     *   
     *
     * @author Sun Microsystems, Inc.
     * 
     */
    public class Receiver implements Runnable
    {
        /**
         *
         */
        public void run()
        {
            mLog.info("Test engine receiver started");

            try
            {
                while (mRunFlag)
                {
                    mReq = mChannel.accept();
                    mLog.info("Got message i test ebgine");

                    if (mReq instanceof InOut)
                    {
                        doTestGoodInOut();
                    }
                    else if (mReq instanceof InOnly)
                    {
                        doTestGoodInOnly();
                    }
                    else if (mReq instanceof RobustInOnly)
                    {
                        doTestGoodRobustInOnly();
                    }
                }
            }
            catch (Exception e)
            {
                mLog.severe("Exception at receiver");
                mLog.info("Test engine receiver ended");
            }
        }
    }
    
    
    
    public javax.jbi.component.ComponentLifeCycle getLifeCycle() 
    {
        return this;
    }
    
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager() 
    {
        return null;
    }
    
    public org.w3c.dom.Document getServiceDescription(
            javax.jbi.servicedesc.ServiceEndpoint ServiceEndpoint) 
    {
        return null;
    }
    
     /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
}
