/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMessageRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.framework.Servicelist;

import junit.framework.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.namespace.QName;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestMessageRegistry extends TestCase
{
    /**
     * Creates a new TestMessageRegistry object.
     *
     * @param testName
     */
    public TestMessageRegistry(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestMessageRegistry.class);

        return suite;
    }

    /**
     * Test of clearRegistry method, of class
     * com.sun.jbi.engine.sequencing.MessageRegistry.
     */
    public void testClearRegistry()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of deregisterExchange method, of class
     * com.sun.jbi.engine.sequencing.MessageRegistry.
     */
    public void testDeregisterExchange()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getInstance method, of class
     * com.sun.jbi.engine.sequencing.MessageRegistry.
     */
    public void testGetInstance()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServicelist method, of class
     * com.sun.jbi.engine.sequencing.MessageRegistry.
     */
    public void testGetServicelist()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of listActiveExchanges method, of class
     * com.sun.jbi.engine.sequencing.MessageRegistry.
     */
    public void testListActiveExchanges()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of registerService method, of class
     * com.sun.jbi.engine.sequencing.MessageRegistry.
     */
    public void testRegisterService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
