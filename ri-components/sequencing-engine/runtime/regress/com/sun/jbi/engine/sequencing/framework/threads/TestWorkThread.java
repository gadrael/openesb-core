/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWorkThread.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.framework.threads;

import junit.framework.*;

import java.util.logging.Logger;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestWorkThread extends TestCase
{
    /**
     * Creates a new TestWorkThread object.
     *
     * @param testName
     */
    public TestWorkThread(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestWorkThread.class);

        return suite;
    }

    /**
     * Test of cease method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testCease()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of clearCommand method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testClearCommand()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of doWork method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testDoWork()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getCommand method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testGetCommand()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of isWorkAssigned method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testIsWorkAssigned()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of processCommand method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testProcessCommand()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setCommand method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testSetCommand()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setLogger method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testSetLogger()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setSleepTime method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testSetSleepTime()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setState method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkThread.
     */
    public void testSetState()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
