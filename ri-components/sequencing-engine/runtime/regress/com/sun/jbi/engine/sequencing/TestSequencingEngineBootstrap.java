/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSequencingEngineBootstrap.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import junit.framework.*;

import java.util.logging.Logger;



/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestSequencingEngineBootstrap extends TestCase
{
    /**
     * Creates a new TestSequencingEngineBootstrap object.
     *
     * @param testName
     */
    public TestSequencingEngineBootstrap(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestSequencingEngineBootstrap.class);

        return suite;
    }

    /**
     * Test of getExtensionMBeanName method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineBootstrap.
     */
    public void testGetExtensionMBeanName()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of init method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineBootstrap.
     */
    public void testInit()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of onInstall method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineBootstrap.
     */
    public void testOnInstall()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of onUninstall method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineBootstrap.
     */
    public void testOnUninstall()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
