/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.util;

/**
 * Thi class has all the configuration information.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConfigData
{
    /**
     *
     */

    /**
     *    
     */
    public static final String LOG_FILE = "sequencing.log";

    /**
     *
     */

    /**
     *    
     */
    public static final String CONFIG_FILE_NAME = "servicelist.xml";

    /**
     *
     */

    /**
     *    
     */
    public static final String SCHEMA_FILE = "servicelist.xsd";

    /**
     *
     */

    /**
     *    
     */
    public static final String APPLICATIONS_FOLDER = "applications";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICELIST = "service-list";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICE = "service";

    /**
     *
     */

    /**
     *    
     */
    public static final String LIST_SERVICE = "list-service";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICE_NAME = "service-name";

    /**
     *
     */

    /**
     *    
     */
    public static final String NAMESPACE_URI = "namespace-uri";

    /**
     *
     */

    /**
     *    
     */
    public static final String LOCAL_PART = "local-part";

    /**
     *
     */

    /**
     *    
     */
    public static final String LIST_OPERATION = "list-operation";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICENAME = "name";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICE_LOCALNAME = "name";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICE_NAMESPACE = "namespace";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICEID = "service-id";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICELIST_DESCRIPTION = "list-description";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICELIST_OPERATION = "list-operation";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICE_DESCRIPTION = "description";

    /**
     *
     */

    /**
     *    
     */
    public static final String TIMEOUT = "timeout";

    /**
     *
     */

    /**
     *    
     */
    public static final String OPERATION = "operation";

    /**
     *
     */

    /**
     *    
     */
    public static final String OPERATION_NAMESPACE = "operation-namespace";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICELIST_OPERATION_NAMESPACE =
        "list-operation-namespace";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICELISTATTR = "list-attributes";

    /**
     * Service list local name.
     */
    public static final String SERVICELISTNAME = "service-name";

    /**
     * Namespace for tag for servicelist namespace.
     */
    public static final String SERVICELISTNAMESPACE = "service-namespace";

    /**
     *
     */

    /**
     *    
     */
    public static final String LOCAL_ENDPOINT = "localendpoint";

    /**
     *
     */

    /**
     *    
     */
    public static final String LIST_MEP = "list-mep";

    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICE_MEP = "mep";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    public static final String LIST_ENDPOINT = "endpoint-name";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    public static final String SERVICE_ENDPOINT = "endpoint-name";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    public static final String SEQ_RESOLVER_ID = "SEQ_RESOLVER_ID";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    public static final String REQUEST_SEQ_ID = "REQUEST_SEQ_ID";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    public static final int REQUEST_TYPE = 0;

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    public static final int RESPONSE_TYPE = 1;

    /**
     * The name of the jbi schema file for jbi.xml in an SU.
     */
    public static final String JBI_SCHEMA_FILE = "jbi.xsd";

    /**
     * Deployment descriptor file
     */
    public static final String DEPLOY_DESCRIPTOR = "jbi.xml";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    public static final String RESPONSE_SEQ_ID = "RESPONSE_SEQ_ID";

    /**
     * Describes the list interface in the XML document
     */
    public static final String LIST_INTERFACE = "list-interface";

    /**
     * Describes the interface under service nodes in a document.
     */
    public static final String INTERFACE = "interface-name";
}
