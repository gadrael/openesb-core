/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;
import com.sun.jbi.engine.sequencing.util.StringTranslator;
import com.sun.jbi.engine.sequencing.util.UtilBase;

import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * Manages all the stopping and starting of services.
 *
 * @author Sun Microsystems, Inc.
 */
public class ServiceManager
    extends UtilBase
    implements SequencingEngineResources
{
    /**
     * Wait time for thread.
     */
    private static final long WAIT_TIME = 2000;

    /**
     * Binding Channel.
     */
    private ComponentContext mContext;

    /**
     * Deployment Registry.
     */
    private DeploymentRegistry mRegistry;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * List of active services.
     */
    private Vector mActiveServices;

    /**
     * Creates a new ServiceManager object.
     */
    public ServiceManager()
    {
        mRegistry = DeploymentRegistry.getInstance();
        mActiveServices = new Vector();
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
    }

    /**
     * Sets the channel object.
     *
     * @param ctx engine channel object.
     */
    public void setContext(ComponentContext ctx)
    {
        mContext = ctx;
    }

    /**
     * Returns the number of services actually running.
     *
     * @return int number of running services.
     */
    public int getRunningServicesCount()
    {
        int count = 0;

        return count;
    }

    /**
     * Activates all the registered Services.
     */
    public void activateServices()
    {
        /*Query Registry and get services
         */
        Iterator iter = mRegistry.listAllServices();

        if (iter == null)
        {
            mLog.info(mTranslator.getString(SEQ_NO_SERVICES));

            return;
        }

        while (iter.hasNext())
        {
            String ep = (String) iter.next();
            ServicelistBean eb = mRegistry.findService(ep);

            if (eb == null)
            {
                mLog.severe(mTranslator.getString(SEQ_START_SERVICE_FAILED, ep));
                setError(mTranslator.getString(SEQ_START_SERVICE_FAILED, ep));

                continue;
            }

            startDeployment(eb.getDeploymentId());
        }

        mLog.fine(mTranslator.getString(SEQ_ACTIVATE_SERVICES_DONE));
    }

    /**
     * Starts a deployment.
     *
     * @param asid sub-assembly id
     */
    public void startDeployment(String asid)
    {
        mLog.fine(mTranslator.getString(SEQ_START_DEPLOYMENT, asid));

        ServicelistBean eb = (ServicelistBean) mRegistry.getService(asid);
        clear();

        if (eb == null)
        {
            mLog.severe(mTranslator.getString(
                    SEQ_START_DEPLOYMENT_FAILED_BEANNULL, asid));

            setError(mTranslator.getString(
                    SEQ_START_DEPLOYMENT_FAILED_BEANNULL, asid));

            return;
        }

        try
        {
            ServiceEndpoint ref =
                mContext.activateEndpoint(new QName(eb.getNamespace(),
                        eb.getServicename()), eb.getEndpointName());
            eb.setServiceReference(ref);
            mLog.fine(mTranslator.getString(SEQ_ACTIVATE_INBOUND_SUCCESS,
                    eb.getServicename()));
        }
        catch (javax.jbi.JBIException me)
        {
            mLog.severe(mTranslator.getString(SEQ_ACTIVATE_INBOUND_FAILED,
                    eb.getServicename()));
            mLog.severe(me.getMessage());
            setError(mTranslator.getString(SEQ_ACTIVATE_INBOUND_FAILED,
                    eb.getServicename()));
            setException(me);
        }

        mLog.fine(mTranslator.getString(SEQ_START_DEPLOYMENT_SUCCESS, asid));
    }

    /**
     * Stops all running services.
     */
    public void stopAllServices()
    {
        if (mActiveServices == null)
        {
            return;
        }

        if (mActiveServices.isEmpty())
        {
            return;
        }

        MessageRegistry reg = MessageRegistry.getInstance();

        if (!reg.isEmpty())
        {
            try
            {
                Thread.sleep(WAIT_TIME);
            }
            catch (Exception e)
            {
                ;
            }
        }

        if (!reg.isEmpty())
        {
            mLog.info(mTranslator.getString(SEQ_STOP_ACTIVE_EXCHANGE));
        }
        else
        {
            mLog.info(mTranslator.getString(SEQ_STOP_EXCHANGES_DONE));
        }
    }

    /**
     * Stops a deployment.
     *
     * @param asid sub-assembly id
     */
    public void stopDeployment(String asid)
    {
        super.clear();

        Iterator iter = mActiveServices.iterator();
        mLog.info(mTranslator.getString(SEQ_STOP_DEPLOYMENT, asid));

        ServicelistBean sb = mRegistry.getService(asid);

        if (sb == null)
        {
            setError(mTranslator.getString(SEQ_STOP_DEPLOYMENT_FAILED));
        }

        if (sb.isExecuting())
        {
            setError(mTranslator.getString(SEQ_DEPLOYMENT_EXECUTING, asid));

            return;
        }

        try
        {
            mContext.deactivateEndpoint(sb.getServiceReference());
        }
        catch (Exception e)
        {
            setError("Cannot deactivate endpoint");
            setException(e);
        }

        mLog.info(mTranslator.getString(SEQ_STOP_DEPLOYMENT_SUCCESS, asid));
    }
}
