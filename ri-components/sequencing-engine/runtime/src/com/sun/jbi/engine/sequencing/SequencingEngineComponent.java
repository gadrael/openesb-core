/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SequencingEngineComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import org.w3c.dom.DocumentFragment;

import javax.jbi.component.Component;
import javax.jbi.servicedesc.ServiceEndpoint;


/**
 * This class implements the component contract with the JBI.
 *
 * @author Sun Microsystems, Inc.
 */
public class SequencingEngineComponent
    implements Component
{
    /**
     * Lifecycle object.
     */
    private SequencingEngineLifeCycle mLifeCycle;

    /**
     * SU Manager.
     */
    private SequencingEngineSUManager mSUManager;

    /**
     * Creates a new instance of SequencingEngineComponent
     */
    public SequencingEngineComponent()
    {
        mLifeCycle = new SequencingEngineLifeCycle();
        mSUManager = new SequencingEngineSUManager();
        mLifeCycle.setSUManager(mSUManager);
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually
     * perform the operation desired.
     *
     * @param endpoint
     * @param exchange
     *
     * @return  true if Ok.
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually
     * interact with the the provider completely.
     *
     * @param endpoint
     * @param exchange
     *
     * @return  true if OK.
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }

    /**
     * Returns the lifecycle object.
     *
     * @return ComponentLifecycle.
     */
    public javax.jbi.component.ComponentLifeCycle getLifeCycle()
    {
        return mLifeCycle;
    }

    /**
     * Returns the service description.
     *
     * @param endpoint service endpoint.
     *
     * @return document.
     */
    public org.w3c.dom.Document getServiceDescription(
        javax.jbi.servicedesc.ServiceEndpoint endpoint)
    {
        return null;
    }

    /**
     * Returns a SU Mangaer object.
     *
     * @return Su Manager for SE.
     */
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager()
    {
        return mSUManager;
    }

    /**
     * Resolve the endpoint reference using the given capabilities of the
     * consumer. This is called by JBI when its trying to resove the given EPR
     * on behalf of the component.
     *
     * @param epr endpoint reference.
     *
     * @return  service endpoint.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
}
