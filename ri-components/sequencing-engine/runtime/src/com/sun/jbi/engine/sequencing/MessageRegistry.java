/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.framework.Servicelist;
import com.sun.jbi.engine.sequencing.util.StringTranslator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;


/**
 * This is a registry which maintains the exchnage Id and the servicelist
 * runtime  object so that responses can be correlated.
 *
 * @author Sun Microsystems, Inc.
 */
public final class MessageRegistry
    implements SequencingEngineResources
{
    /**
     * Singleton reference.
     */
    private static MessageRegistry sMe;

    /**
     *  Timeout.
     */
    private static final int TIMEDOUT_MESSAGE_COUNT = 10000;

    /**
     * List of all registered exchanges.
     */
    private HashMap mExchanges;

    /**
     * Logger object
     */
    private Logger mLog;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * List of exchanges that timedout.
     */
    private Vector mTimeOutExchanges;

    /**
     * Private constructor.
     */
    private MessageRegistry()
    {
        mExchanges = new HashMap();
        mTimeOutExchanges = new Vector();
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
    }
    
    public void dump()
    {
        mLog.info("**** PRINTING MSG REGISTRY ****** ");
        mLog.info(mExchanges.keySet().toString());
        mLog.info("***** PRINTING TIMED OUT REGISTRY **** ");
        mLog.info(mTimeOutExchanges.toString());
    }

    /**
     * Used to grab a reference of this object.
     *
     * @return an initialized MessageRegistry reference
     */
    public static synchronized MessageRegistry getInstance()
    {
        if (sMe == null)
        {
            sMe = new MessageRegistry();
        }

        return sMe;
    }

    /**
     * Chesks if there any active message exchanges.
     *
     * @return true if no exchanges are active.
     */
    public boolean isEmpty()
    {
        return mExchanges.isEmpty();
    }

    /**
     * Retrieves the servicelist runtime object corresponding to each exchange
     * ID.
     *
     * @param exchangeId message xchnage
     *
     * @return servicelist bean object
     */
    public Servicelist getServicelist(String exchangeId)
    {
        Servicelist l;
        l = (Servicelist) mExchanges.get(exchangeId);

        return l;
    }

    /**
     * Checks if timed out.
     *
     * @param exc exchangeid.
     *
     * @return true if timed out.
     */
    public synchronized boolean isTimedOut(String exc)
    {
        if (mTimeOutExchanges.contains(exc))
        {
            return true;
        }

        return false;
    }

    /**
     * Removes all the entries from the registry.
     */
    public void clearRegistry()
    {
        if (mExchanges == null)
        {
            return;
        }

        mExchanges = new HashMap();
    }

    /**
     * Removes an exchange from the registry.
     *
     * @param exchangeId exchange id
     */
    public synchronized void deregisterExchange(String exchangeId)
    {
        mLog.fine(mTranslator.getString(SEQ_DEREGISTER_EXCHANGE, exchangeId));

        Servicelist l = (Servicelist) mExchanges.remove(exchangeId);

        if (l == null)
        {
            mLog.warning(mTranslator.getString(SEQ_DEREGISTER_EXCHANGE_FAILED,
                    exchangeId));
        }
    }

    /**
     * Removes an exchange from the registry.
     *
     * @param exchangeId exchange id
     */
    public synchronized void deregisterTimedOutExchange(String exchangeId)
    {
        mLog.fine(mTranslator.getString(SEQ_DEREGISTER_EXCHANGE, exchangeId));

        if (!mTimeOutExchanges.remove(exchangeId))
        {
            mLog.warning(mTranslator.getString(SEQ_DEREGISTER_EXCHANGE_FAILED,
                    exchangeId));
        }
    }

    /**
     * List all the active exchanges.
     *
     * @return iterator
     */
    public Iterator listActiveExchanges()
    {
        if (mExchanges == null)
        {
            return null;
        }

        Set tmp = mExchanges.keySet();

        if (tmp == null)
        {
            return null;
        }

        return tmp.iterator();
    }

    /**
     * Registers a message Exchange.
     *
     * @param exchnageId exchnage id
     * @param list servicelist
     */
    public synchronized void registerExchange(
        String exchnageId,
        Servicelist list)
    {
        mLog.info(mTranslator.getString(SEQ_REGISTER_EXCHANGE, exchnageId));
        mExchanges.put(exchnageId, list);
    }

    /**
     * Registers a timedout message Exchange.
     *
     * @param exchnageId exchnage id
     */
    public synchronized void registerTimedOutExchange(String exchnageId)
    {
        mLog.info(mTranslator.getString(SEQ_REGISTER_EXCHANGE, exchnageId));

        try
        {
            if (mTimeOutExchanges.size() >= TIMEDOUT_MESSAGE_COUNT)
            {
                mTimeOutExchanges.removeElementAt(0);
            }

            mTimeOutExchanges.add(exchnageId);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
