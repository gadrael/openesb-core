/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SequencingEngineResources.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

/**
 * Interface for localization keys.
 *
 * @author Sun Microsystems, Inc.
  */
public interface SequencingEngineResources
{
    /**
     * inboudn failed.
     */
    String SEQ_ACTIVATE_INBOUND_FAILED = "SEQ_ACTIVATE_INBOUND_FAILED";

    /**
     * actvate inbound.
     */
    String SEQ_ACTIVATE_INBOUND_SERVICE = "SEQ_ACTIVATE_INBOUND_SERVICE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ACTIVATE_INBOUND_SUCCESS = "SEQ_ACTIVATE_INBOUND_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ACTIVATE_OUTBOUND_FAILED = "SEQ_ACTIVATE_OUTBOUND_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ACTIVATE_OUTBOUND_SUCCESS = "SEQ_ACTIVATE_OUTBOUND_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ACTIVATE_SERVICES_DONE = "SEQ_ACTIVATE_SERVICES_DONE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ACTIVATE_SERVICE_DONE = "SEQ_ACTIVATE_SERVICE_DONE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ACTIVATING_SERVICES = "SEQ_ACTIVATING_SERVICES";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ADMIN_MBEAN_FAILED = "SEQ_ADMIN_MBEAN_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_BEAN_NOT_SET = "SEQ_BEAN_NOT_SET";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CANNOT_ACTIVATE = "SEQ_CANNOT_ACTIVATE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CANNOT_ACTIVATE_ENDPOINT = "SEQ_CANNOT_ACTIVATE_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CANNOT_DEACTIVATE_ENDPOINT = "SEQ_CANNOT_DEACTIVATE_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CANNOT_GET_ENDPOINT = "SEQ_CANNOT_GET_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CANNOT_PARSE = "SEQ_CANNOT_PARSE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CANNOT_PROCESS1 = "SEQ_CANNOT_PROCESS1";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CANNOT_PROCESSRESPONSE = "SEQ_CANNOT_PROCESSRESPONSE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CANNOT_SEND = "SEQ_CANNOT_SEND";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CHANNEL_NOT_SET = "SEQ_CHANNEL_NOT_SET";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CLEAN_FAILED = "SEQ_CLEAN_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CLOSE_FAILED = "SEQ_CLOSE_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CONFIGDATA_ERROR = "SEQ_CONFIGDATA_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CONFIGFILE_PARSE_ERROR = "SEQ_CONFIGFILE_PARSE_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CONFIGFILE_UNAVAILABLE = "SEQ_CONFIGFILE_UNAVAILABLE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CONFIGFILE_VALID = "SEQ_CONFIGFILE_VALID";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CONFIGFILE_WARNING = "SEQ_CONFIGFILE_WARNING";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CONFIG_FILE_CHECK = "SEQ_CONFIG_FILE_CHECK";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CONTENT_NULL = "SEQ_CONTENT_NULL";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_CORRUPTED_MESSAGE = "SEQ_CORRUPTED_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DEFAULT_TIMEOUT = "SEQ_DEFAULT_TIMEOUT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DEPLOYMENT_EXECUTING = "SEQ_DEPLOYMENT_EXECUTING";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DEPLOY_FAILED = "SEQ_DEPLOY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DEREGISTER_DEPLOYERMBEAN_FAILED =
        "SEQ_DEREGISTER_DEPLOYERMBEAN_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DEREGISTER_EXCHANGE = "SEQ_DEREGISTER_EXCHANGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DEREGISTER_EXCHANGE_FAILED = "SEQ_DEREGISTER_EXCHANGE_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DEREGISTER_SERVICE = "SEQ_DEREGISTER_SERVICE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DEREGISTER_SERVICE_FAILED = "SEQ_DEREGISTER_SERVICE_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DUPLICATE_DEPLOYMENT = "SEQ_DUPLICATE_DEPLOYMENT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_DUPLICATE_ENDPOINT = "SEQ_DUPLICATE_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ENGINECHANNEL_FAILED = "SEQ_ENGINECHANNEL_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ENGINE_SHUTTING_DOWN = "SEQ_ENGINE_SHUTTING_DOWN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_EXCHANGE_DONE = "SEQ_EXCHANGE_DONE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_EXCHANGE_ERROR = "SEQ_EXCHANGE_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_EXCHANGE_NOT_SET = "SEQ_EXCHANGE_NOT_SET";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FAILED_WRITE = "SEQ_FAILED_WRITE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FILE_WRITE_FAILED = "SEQ_FILE_WRITE_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FINE_DOING_DEPLOY = "SEQ_FINE_DOING_DEPLOY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FINE_EXECUTING_SERVICE = "SEQ_FINE_EXECUTING_SERVICE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FINE_EXIT_EXECUTE = "SEQ_FINE_EXIT_EXECUTE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FINE_SEND_ACTIVE = "SEQ_FINE_SEND_ACTIVE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FINE_SEND_MESSAGE = "SEQ_FINE_SEND_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FINE_SERVICE_EXECUTION_DONE = "SEQ_FINE_SERVICE_EXECUTION_DONE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FRAMEWORK_BUSYPOOL_SIZE = "SEQ_FRAMEWORK_BUSYPOOL_SIZE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FRAMEWORK_FREEPOOL_SIZE = "SEQ_FRAMEWORK_FREEPOOL_SIZE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FRAMEWORK_INVALID_SERVICENAME =
        "SEQ_FRAMEWORK_INVALID_SERVICENAME";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FRAMEWORK_STARTPOOL = "SEQ_FRAMEWORK_STARTPOOL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FRAMEWORK_THREAD_PROCESS = "SEQ_FRAMEWORK_THREAD_PROCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FRAMWEORK_RUNNING_THREAD = "SEQ_FRAMWEORK_RUNNING_THREAD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_FRAMWEORK_SHUTDOWN_THREAD = "SEQ_FRAMWEORK_SHUTDOWN_THREAD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_IGNORE_ENDPOINT = "SEQ_IGNORE_ENDPOINT";

    /**
     *    
     */
    String SEQ_INCONSISTENT_CONSUMER_DATA = "SEQ_INCONSISTENT_CONSUMER_DATA";

    /**
     *    
     */
    String SEQ_INCONSISTENT_DATA = "SEQ_INCONSISTENT_DATA";

    /**
     *    
     */
    String SEQ_INCONSISTENT_PROVIDER_DATA = "SEQ_INCONSISTENT_PROVIDER_DATA";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INIT_FAILED = "SEQ_INIT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INIT_START = "SEQ_INIT_START";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INOPTIONALOUT_NOT_SUPPORTED = "SEQ_INOPTIONALOUT_NOT_SUPPORTED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INPUTFOLDER_NOTEXIST = "SEQ_INPUTFOLDER_NOTEXIST";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_BINDINGCONTEXT = "SEQ_INVALID_BINDINGCONTEXT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_COMPROOT = "SEQ_INVALID_COMPROOT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_CONFIG_FILE = "SEQ_INVALID_CONFIG_FILE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_CONFIG_FILE_INFO = "SEQ_INVALID_CONFIG_FILE_INFO";

    /**
     *    
     */
    String SEQ_INVALID_DD = "SEQ_INVALID_DD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_ENDPOINT = "SEQ_INVALID_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_ENDPOINTTYPE = "SEQ_INVALID_ENDPOINTTYPE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_ENDPOINT_TYPE = "SEQ_INVALID_ENDPOINT_TYPE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_INPUTFOLDER = "SEQ_INVALID_INPUTFOLDER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_MEP = "SEQ_INVALID_MEP";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_MESSAGE = "SEQ_INVALID_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_OPERATION = "SEQ_INVALID_OPERATION";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_OUTPUTFOLDER = "SEQ_INVALID_OUTPUTFOLDER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_PATTERN = "SEQ_INVALID_PATTERN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_PROCESSEDFOLDER = "SEQ_INVALID_PROCESSEDFOLDER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_SERVICE = "SEQ_INVALID_SERVICE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_SERVICEID = "SEQ_INVALID_SERVICEID";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_SERVICELIST_NAME = "SEQ_INVALID_SERVICELIST_NAME";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_INVALID_XML = "SEQ_INVALID_XML";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_LIST_COMPLETED = "SEQ_LIST_COMPLETED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_LIST_COMPLETED_ERROR = "SEQ_LIST_COMPLETED_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_LOAD_CONFIG_FAILED = "SEQ_LOAD_CONFIG_FAILED";

    /**
     *    
     */
    String SEQ_LOAD_DD_FAILED = "SEQ_LOAD_DD_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_MESSAGE_CREATION_FAILED = "SEQ_MESSAGE_CREATION_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_MESSAGE_RECEIVED = "SEQ_MESSAGE_RECEIVED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_MOVE_FAILED = "SEQ_MOVE_FAILED";

    /**
     *    
     */
    String SEQ_NO_DD = "SEQ_NO_DD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_NO_DEPLOYMENTS = "SEQ_NO_DEPLOYMENTS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_NO_FREE_THREAD = "SEQ_NO_FREE_THREAD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_NO_INPUT_MESSAGE = "SEQ_NO_INPUT_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_NO_SERVICE = "SEQ_NO_SERVICE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_NO_SERVICES = "SEQ_NO_SERVICES";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_NULL_EXCHANGE = "SEQ_NULL_EXCHANGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_NULL_OUT_MESSAGE = "SEQ_NULL_OUT_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_OUTPUTFOLDER_NOTEXIST = "SEQ_OUTPUTFOLDER_NOTEXIST";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESSEDFOLDER_NOTEXIST = "SEQ_PROCESSEDFOLDER_NOTEXIST";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESSED_FAILED = "SEQ_PROCESSED_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESSED_SUCCESS = "SEQ_PROCESSED_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESSING_RESPONSE = "SEQ_PROCESSING_RESPONSE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESS_INONLY_FAILED = "SEQ_PROCESS_INONLY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESS_INOUT_FAILED = "SEQ_PROCESS_INOUT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESS_OUTONLY_FAILED = "SEQ_PROCESS_OUTONLY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESS_ROBUSTINONLY_FAILED = "SEQ_PROCESS_ROBUSTINONLY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_PROCESS_ROBUSTOUTONLY_FAILED =
        "SEQ_PROCESS_ROBUSTOUTONLY_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_REASON1 = "SEQ_REASON1";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_INONLY = "SEQ_RECEIVED_INONLY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_INOPTIONALOUT = "SEQ_RECEIVED_INOPTIONALOUT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_INOUT = "SEQ_RECEIVED_INOUT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_INVALID_MESSAGE = "SEQ_RECEIVED_INVALID_MESSAGE";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_MESSAGE = "SEQ_RECEIVED_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_OUTIN = "SEQ_RECEIVED_OUTIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_OUTONLY = "SEQ_RECEIVED_OUTONLY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_OUTOPTIONALIN = "SEQ_RECEIVED_OUTOPTIONALIN";

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_RESPONSE = "SEQ_RECEIVED_RESPONSE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_ROBUSTINONLY = "SEQ_RECEIVED_ROBUSTINONLY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_ROBUSTOUTONLY = "SEQ_RECEIVED_ROBUSTOUTONLY";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVED_UNSUPPORTED_MEP = "SEQ_RECEIVED_UNSUPPORTED_MEP";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVER_ERROR = "SEQ_RECEIVER_ERROR";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVER_START = "SEQ_RECEIVER_START";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVER_STOP = "SEQ_RECEIVER_STOP";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_RECEIVER_STOPPED = "SEQ_RECEIVER_STOPPED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_REGISTER_DEPLOYERMBEAN_FAILED =
        "SEQ_REGISTER_DEPLOYERMBEAN_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_REGISTER_EXCHANGE = "SEQ_REGISTER_EXCHANGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_REGISTER_SERVICE = "SEQ_REGISTER_SERVICE";

    /**
     *    
     */
    String SEQ_RESUME_TX = "SEQ_RESUME_TX";

    /**
     *    
     */
    String SEQ_RESUME_TX_FAILED = "SEQ_RESUME_TX_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_ROBUSTINONLY_NOTSUPPORTED = "SEQ_ROBUSTINONLY_NOTSUPPORTED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_SENDING_MESSAGE = "SEQ_SENDING_MESSAGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_SEND_FAILED = "SEQ_SEND_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_SEND_LIST_RESPONSE = "SEQ_SEND_LIST_RESPONSE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_SERVICE_NOT_HOSTED = "SEQ_SERVICE_NOT_HOSTED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_SERVICE_TIMED_OUT = "SEQ_SERVICE_TIMED_OUT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_SEVERE_CANNOT_GET_SERVICE_INFO =
        "SEQ_SEVERE_CANNOT_GET_SERVICE_INFO";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_SHUTDOWN_BEGIN = "SEQ_SHUTDOWN_BEGIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_SHUTDOWN_END = "SEQ_SHUTDOWN_END";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_START_BEGIN = "SEQ_START_BEGIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_START_DEPLOYMENT = "SEQ_START_DEPLOYMENT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_START_DEPLOYMENT_FAILED = "SEQ_START_DEPLOYMENT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_START_DEPLOYMENT_FAILED_BEANNULL =
        "SEQ_START_DEPLOYMENT_FAILED_BEANNULL";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_START_DEPLOYMENT_SUCCESS = "SEQ_START_DEPLOYMENT_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_START_END = "SEQ_START_END";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_START_SERVICE_FAILED = "SEQ_START_SERVICE_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_ACTIVE_EXCHANGE = "SEQ_STOP_ACTIVE_EXCHANGE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_BEGIN = "SEQ_STOP_BEGIN";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_DEPLOYMENT = "SEQ_STOP_DEPLOYMENT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_DEPLOYMENT_FAILED = "SEQ_STOP_DEPLOYMENT_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_DEPLOYMENT_SUCCESS = "SEQ_STOP_DEPLOYMENT_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_END = "SEQ_STOP_END";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_ENDPOINTTHREAD = "SEQ_STOP_ENDPOINTTHREAD";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_EXCHANGES_DONE = "SEQ_STOP_EXCHANGES_DONE";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_POLLING = "SEQ_STOP_POLLING";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_STOP_WARNING = "SEQ_STOP_WARNING";

    /**
     *    
     */
    String SEQ_SUSPEND_TX = "SEQ_SUSPEND_TX";

    /**
     *    
     */
    String SEQ_SUSPEND_TX_FAILED = "SEQ_SUSPEND_TX_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_TASK_FAILED = "SEQ_TASK_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_TRY_LATER = "SEQ_TRY_LATER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_TRY_OUTBOUND_ENDPOINT = "SEQ_TRY_OUTBOUND_ENDPOINT";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_UNJAR_FAILED = "SEQ_UNJAR_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_UNSUPPORTED_MEP = "SEQ_UNSUPPORTED_MEP";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_UNSUPPORTED_OPER = "SEQ_UNSUPPORTED_OPER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_UNSUPPORTED_PATTERN_OPER = "SEQ_UNSUPPORTED_PATTERN_OPER";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_UPDATE_NM_FAILED = "SEQ_UPDATE_NM_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_WRITE_ERROR_FAILED = "SEQ_WRITE_ERROR_FAILED";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_WRITE_ERROR_SUCCESS = "SEQ_WRITE_ERROR_SUCCESS";

    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    String SEQ_XML_STRING_CREATION_FAILED = "SEQ_XML_STRING_CREATION_FAILED";
}
