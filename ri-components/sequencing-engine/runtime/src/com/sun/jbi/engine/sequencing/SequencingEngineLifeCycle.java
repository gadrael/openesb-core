/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SequencingEngineLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.util.StringTranslator;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.messaging.DeliveryChannel;


/**
 * This class implements EngineLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Sun Microsystems, Inc.
 */
public class SequencingEngineLifeCycle
    implements ComponentLifeCycle, SequencingEngineResources
{
    /**
     * Component context passed down from framework to this sequencing engine.
     */
    private ComponentContext mContext = null;

    /**
     * Engine channel
     */
    private DeliveryChannel mChannel;

    /**
     * Deployment registry to keep track of deployments.
     */
    private DeploymentRegistry mRegistry;

    /**
     * Refernce to logger.
     */
    private Logger mLog = null;

    /**
     * Receiver object for this engine.
     */
    private MessageReceiver mMessageReceiver;

    /**
     * Deployer object.
     */
    private SequencingEngineSUManager mSUManager;

    /**
     * Service manager.
     */
    private ServiceManager mManager;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * Thread object for receiver.
     */
    private Thread mReceiverThread;

    /**
     * Denotes if init is a success / failure.
     */
    private boolean mInitSuccess = false;

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Sets the SU manager.
     *
     * @param manager  su manager.
     */
    public void setSUManager(SequencingEngineSUManager manager)
    {
        mSUManager = manager;
    }

    /**
     * Initialize the sequence engine. This performs initialization required by
     * the sequencing engine but does not make it ready to process messages.
     * This method is called immediately after installation of the sequencing
     * engine. It is also called when the JBI framework is starting up, and
     * any time the sequencing engine is being restarted after previously
     * being shut down through a call to shutdown().
     *
     * @param jbiContext the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the sequencing engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext jbiContext)
        throws javax.jbi.JBIException
    {
        mTranslator = new StringTranslator();

        if (jbiContext == null)
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                    SEQ_INVALID_BINDINGCONTEXT));
        }

        /* Assign values */
        mContext = jbiContext;

        SequencingEngineContext seqContext =
            SequencingEngineContext.getInstance();
        seqContext.setContext(mContext);
        mLog = mContext.getLogger("", null);
        seqContext.setLogger(mLog);
        mLog.info(mTranslator.getString(SEQ_INIT_START));
        mManager = new ServiceManager();
        mSUManager.setValues(mContext, mManager);

        try
        {
            mRegistry = DeploymentRegistry.getInstance();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mInitSuccess = true;
    }

    /**
     * Shutdown the SE. This performs cleanup before the SE is terminated. Once
     * this has been called, init() must be called before the SE can be
     * started again with a call to start().
     *
     * @throws javax.jbi.JBIException if the SE is unable to shut down.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLog.info(mTranslator.getString(SEQ_SHUTDOWN_BEGIN));
        mSUManager = null;
        mManager = null;
        mRegistry.clearRegistry();

        /* Unregister any other MBeans
         */
        mLog.info(mTranslator.getString(SEQ_SHUTDOWN_END));
    }

    /**
     * Start the seqeuncing engine. This makes the SE ready to  process
     * messages. This method is called after init() completes when the JBI
     * framework is starting up, and when the sequencing engine is being
     * restarted after a previous call to shutdown().  If stop() was called
     * previously but shutdown() was not, start() can be called without a call
     * to init().
     *
     * @throws javax.jbi.JBIException if the sequencing engine is unable to
     *         start.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.info(mTranslator.getString(SEQ_START_BEGIN));

        if (!mInitSuccess)
        {
            mLog.severe(mTranslator.getString(SEQ_INIT_FAILED));

            return;
        }

        mChannel =
            SequencingEngineContext.getInstance().getContext()
                                   .getDeliveryChannel();

        if (mChannel == null)
        {
            mLog.severe(mTranslator.getString(SEQ_ENGINECHANNEL_FAILED));

            return;
        }

        mManager.setContext(mContext);
        mMessageReceiver = new MessageReceiver(mChannel);
        mReceiverThread = new Thread(mMessageReceiver);
        mReceiverThread.start();
        mLog.info(mTranslator.getString(SEQ_START_END));
    }

    /**
     * Stop the sequqencing engine. This makes the SE stop accepting  messages
     * for processing. After a call to this method, start() can be called
     * again without first calling init().
     *
     * @throws javax.jbi.JBIException if the sequencing engine is unable to
     *         stop.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLog.info(mTranslator.getString(SEQ_STOP_BEGIN));

        try
        {
            if (mMessageReceiver != null)
            {
                mMessageReceiver.stopNewRequests();
            }

            if (mMessageReceiver != null)
            {
                mMessageReceiver.stopReceiving();
            }

            if (mChannel != null)
            {
                mChannel.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mLog.info(mTranslator.getString(SEQ_STOP_END));
    }
}
