<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@taglib uri="http://www.sun.com/web/ui" prefix="ui" %>
<f:view>
  <ui:page>
    <ui:head title="Inline Help Test" />      
    <ui:body styleClass="DefBdy">
      <ui:form id="helpForm">         
        <ui:masthead id="Masthead" productImageURL="../images/webconsole.png"
            productImageDescription="Java Web Console" userInfo="test_user"
            serverInfo="test_server">
          <f:facet name="helpLink">
            <ui:helpWindow windowTitle="Help Window" pageTitle="Help"
                 jspPathPrefix="/faces"
                 mastheadImageUrl="/images/webconsole.png"
                 helpFile="sunwebconsole.html"
                 mastheadImageDescription="Sun Java Web Console Logo"
                 toolTip="Help for This Page (Opens a New Window)" />
          </f:facet>               
        </ui:masthead>            
        <ui:breadcrumbs id="breadcrumbs">
          <ui:hyperlink url="../index.jsp" text="Test App Index" />          
          <ui:hyperlink url="../help/index.jsp" text="Help Test" />
        </ui:breadcrumbs>
        <ui:contentPageTitle title="Inline Help Test" />
        <div class="ConMgn">
        <p>        
        <!-- Page Inline Help -->
        <table>
          <tr>
            <td>
              <ui:label text="Page Inline Help" labelLevel="1" />
            </td>
          </tr>
          <tr>
            <td>
              <ui:helpInline id="pageHelp1" type="page">
                <f:verbatim>
                   Page inline help provides brief text that can be enhanced by 
                   a link to additional information using ui:helpWindow tag.
                   &nbsp;&nbsp;                   
                </f:verbatim>
                <ui:helpWindow windowTitle="Window Title Param"
                    jspPathPrefix="/faces"
                    pageTitle="Page Title Param"
                    mastheadImageUrl="/images/webconsole.png"
                    mastheadImageDescription="Sun Java Web Console Logo"            
                    helpFile="accessibility.html"
                    toolTip="Help for This Page (Opens a New Window)"
                    linkIcon="true" linkText="Click for more about Accessibility." />
              </ui:helpInline>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          <tr>
            <td>
              <ui:label text="Page Inline Help (rendered = false)" labelLevel="1" />
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          <tr>
            <td>
              <ui:helpInline id="pageHelp2" type="page" rendered="false">
                <f:verbatim>This inline page help this help text shouldn't be rendered.</f:verbatim>
              </ui:helpInline>
            </td>
          </tr>
          <tr>
            <td>
              <ui:label text="Page Inline Help (visible = false)" labelLevel="1" />
            </td>
          </tr>
          <tr>
            <td>
              <ui:helpInline id="pageHelp3" type="page" visible="false">
                <f:verbatim>This inline page help should be rendered but not visible.</f:verbatim>
              </ui:helpInline>
            </td>
          </tr>
        </table>
        </p>
        <!-- Field Inline Help -->
        <p>
        <table>
          <tr>
            <td>
              <ui:label text="Field Inline Help" labelLevel="1" />
            </td>
          </tr>
          <tr>
            <td>
              <ui:label text="Your Name:" for="NameInput1_field" />
            </td>
            <td>
              <ui:textField id="NameInput1_field"  />
              <ui:helpInline id="fieldHelp1" type="field" text="Type your name in this field." />
            </td>
          </tr>
          <tr>
            <td colpan="2">&nbsp;</td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td>
              <ui:label text="Field Inline Help (rendered = false)" labelLevel="1" />
            </td>
          </tr>
          <tr>
            <td>
              <ui:label text="Your Name:" for="NameInput2_field" />
            </td>
            <td>
              <ui:textField id="NameInput2_field"  />                
              <ui:helpInline id="fieldHelp2" type="field" rendered="false" text="This inline field help should not be rendered." />
            </td>
          </tr>
          <tr>
            <td colpan="2">&nbsp;</td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td>
              <ui:label text="Field Inline Help (visible = false)" labelLevel="1" />
            </td>
          </tr>
          <tr>
            <td>
              <ui:label text="Your Name:" for="NameInput3_field" />
            </td>
            <td>
              <ui:textField id="NameInput3_field"  />
              <ui:helpInline id="fieldHelp3" type="field" visible="false" text="This inline field help should be rendered but not visible." />              
            </td>
          </tr>
          <tr>
            <td colpan="2">&nbsp;</td>
          </tr>        
          <tr>
            <td>
              <ui:label text="Field Inline Help (component binding)" labelLevel="1" />
            </td>
          </tr>
          <tr>
            <td>
              <ui:label text="Your Name:" for="NameInput4_field" />
            </td>
            <td>
              <ui:textField id="NameInput4_field"  />
              <ui:helpInline binding="#{HelpBean.inline}" />
            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>            
          </tr>
        </table>
        </p>        
        </div>        
      </ui:form>      
      
    </ui:body> 
  </ui:page>
</f:view>
