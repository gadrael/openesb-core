<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)pom.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project>
    <parent>
        <artifactId>jbi-common</artifactId>
        <groupId>net.open-esb.core</groupId>
        <version>1.0</version>
        <relativePath>jbi-common</relativePath>
    </parent>
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.open-esb.core</groupId>
    <artifactId>build-common</artifactId>
    <packaging>pom</packaging>
    <name>build-common</name>
    <version>1.0</version>
    <description>build-common - parent for all jbi build and test poms.</description>
    <build>
        <plugins>
            <plugin> 
                <artifactId>maven-source-plugin</artifactId> 
                <version>2.0.4</version>
                <configuration> 
                    <attach>true</attach>
                    <excludeResources>true</excludeResources>
                </configuration> 
                <executions> 
                    <execution> 
                        <id>${project.artifactId}-generate-source-jar</id>
                        <phase>package</phase>
                        <goals><goal>jar</goal></goals> 
                    </execution> 
                </executions> 
            </plugin> 
        </plugins>
        <resources>
            <resource>
                <directory>src</directory>
                <excludes>
                    <!-- 
                     ! project-wide excludes for build resource copies.
                     ! add <resources> in local pom to override.
                     -->
                    <exclude>**/*.java</exclude>
                    <exclude>**/checkstyle.suppressions</exclude>
                    <exclude>**/package.html</exclude>
                </excludes>
            </resource>
        </resources>
        <testResources>
            <testResource>
                <directory>regress</directory>
                <excludes>
                    <!-- 
                     ! project-wide excludes for test resource copies.
                     ! add <testresources> in local pom to override.
                     -->
                    <exclude>**/*.java</exclude>
                    <exclude>**/*.ant</exclude>
                    <exclude>**/*.bsh</exclude>
                    <exclude>**/*.ksh</exclude>
                    <exclude>**/*.ref</exclude>
                </excludes>
            </testResource>
        </testResources>
        <pluginManagement>
            <!-- add common configurations for build & test.  versions are declared in parent pom. -->
            <plugins>
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <inherited>true</inherited>
                    <configuration>
                        <forkMode>pertest</forkMode>
                        <!-- shows total/fail/error summaries on stdout:  -->
                        <printSummary>true</printSummary>
                        <!-- this gives test class name & time elapsed in report files:  -->
                        <reportFormat>plain</reportFormat>
                        <!-- this sends test stdout to reportsDirectory/testName-output.txt:  -->
                        <redirectTestOutputToFile>true</redirectTestOutputToFile>
                        <reportsDirectory>${project.build.directory}/test-reports</reportsDirectory>
                        <excludes>
                            <exclude>**/TestRegisteredFailure.java</exclude>
                        </excludes>
                        <systemProperties>
                            <property>
                                <name>com.sun.jbi.home</name>
                                <value>${JBI_HOME}</value>
                            </property>
                            <property>
                                <name>junit.srcroot</name>
                                <value>${SRCROOT}</value>
                            </property>
                            <property>
                                <name>junit.as8base</name>
                                <value>${AS8BASE}</value>
                            </property>
                        </systemProperties>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
    <dependencies>
        <!-- common dependencies for all build and test projects: -->
        <dependency>
            <groupId>ant</groupId>
            <artifactId>ant-nodeps</artifactId>
            <version>1.6.5</version>
            <type>jar</type>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <artifactId>ant-nodeps</artifactId>
                    <groupId>ant</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>3.8.1</version>
            <type>jar</type>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <artifactId>junit</artifactId>
                    <groupId>junit</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>ant</groupId>
            <artifactId>ant-junit</artifactId>
            <version>1.6.5</version>
            <type>jar</type>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <artifactId>ant-junit</artifactId>
                    <groupId>ant</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>commons-beanutils</groupId>
            <artifactId>commons-beanutils</artifactId>
            <version>1.7.0</version>
            <type>jar</type>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <artifactId>commons-beanutils</artifactId>
                    <groupId>commons-beanutils</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>commons-collections</groupId>
            <artifactId>commons-collections</artifactId>
            <version>3.1</version>
            <type>jar</type>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <artifactId>commons-collections</artifactId>
                    <groupId>commons-collections</groupId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>
    <reporting>
        <outputDirectory>bld/site</outputDirectory>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
                <configuration>
                    <!-- setting this to true will aggregate all sub-project source into one report.
                     !   this should be done in the top report only.
                     -->
                    <aggregate>false</aggregate>
                    <bottom>Copyright (c) 2004-2007 Sun Microsystems Inc.  All Rights Reserved.</bottom>
                    <windowTitle>Open-ESB ${project.version} Reference</windowTitle>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <minmemory>128m</minmemory>
                    <maxmemory>512m</maxmemory>
                  <failOnError>false</failOnError>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <configuration>
                    <enableRulesSummary>true</enableRulesSummary>
                    <configLocation>${SRCROOT}/build-common/jbi-common/checkstyle/checkstyle.xml</configLocation>
                </configuration>
            </plugin>
        </plugins>
    </reporting>
</project>
