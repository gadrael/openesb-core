#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)common_defs.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for management regression tests.

#determine PATH separator character:
unset PS; PS=':' ; _doscnt=`echo $PATH | grep -c ';'` ; [ $_doscnt -ne 0 ] && PS=';' ; unset _doscnt

testDelay()
#this local sleep function can be adjusted based on how fast your machine is
{
    #set platform speed factor.  i.e., 3 means 3 times faster than normal.
    if [ "$REGRESS_SPEED_FACTOR" = "" ]; then
        REGRESS_SPEED_FACTOR=1
    fi

#echo REGRESS_SPEED_FACTOR=$REGRESS_SPEED_FACTOR

    #if n seconds not supplied, then return:
    [ -z "$1" ] && return 1

    if [ $REGRESS_SPEED_FACTOR -eq 0 ]; then
        _testDelay=0
    else
        _testDelay=`echo "$1 $REGRESS_SPEED_FACTOR /p" | dc`
    fi

    #minimum wait is one second:
    [ $_testDelay -eq 0 ] && _testDelay=1

    sleep $_testDelay
    return $?
}

start_domain()
#utility routine to start the current domain.
#return 0 if successful
{
    echo Starting domain $JBI_DOMAIN_NAME
    cmd="asadmin start-domain -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS $JBI_DOMAIN_NAME"
    1>&2 eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
        return 1
    else
        bldwait -nomsg -max 600 $JBI_DOMAIN_STARTED
        [ $? -ne 0 ] && bldmsg -error -p $0 error starting JBI - admin service did not start && return 1
    fi

    #send it a command to *really* start it:
    cmd="asadmin list-jbi-shared-libraries -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT"
    1>&2 eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
        return 1
    fi
    return 0
}

shutdown_domain()
#utility routine to shutdown the current domain.
#return 0 if successful
{
    echo Stopping domain $JBI_DOMAIN_NAME
    cmd="asadmin stop-domain -t $JBI_DOMAIN_NAME"
    1>&2 eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
        return 1
    else
        bldwait -nomsg -max 600 $JBI_DOMAIN_STOPPED
        [ $? -ne 0 ] && bldmsg -error -p $0 error starting JBI - admin service did not start && return 1
    fi

    return 0
}

####
#set OPENESB_BUILD - this is 1 if we are building in maven env., otherwise 0:
####
export OPENESB_BUILD
if [ -r $SRCROOT/build-common/project.xml ]; then
    OPENESB_BUILD=1
elif [ -r $SRCROOT/build-common/pom.xml ]; then
    OPENESB_BUILD=1
else
    OPENESB_BUILD=0
fi

##########
#appserver installation type:
##########
export IS_EE8_INSTALL IS_EE9_INSTALL IS_PE8_INSTALL IS_PE9_INSTALL IS_GLASSFISH_INSTALL
if [ -r "$AS8BASE/LICENSE_EE.txt" ]; then
    IS_EE_INSTALL=1
    IS_EE8_INSTALL=1
    IS_EE9_INSTALL=0
    IS_PE_INSTALL=0
    IS_PE8_INSTALL=0
    IS_PE9_INSTALL=0
    IS_GLASSFISH_INSTALL=0
elif [ -r "$AS8BASE/CDDLv1.0.txt" ]; then
    IS_EE_INSTALL=0
    IS_EE8_INSTALL=0
    IS_EE9_INSTALL=0
    IS_PE_INSTALL=1
    IS_PE8_INSTALL=0
    IS_PE9_INSTALL=1
    IS_GLASSFISH_INSTALL=1
else
    #assume PE 8.x install
    IS_EE_INSTALL=0
    IS_EE8_INSTALL=0
    IS_EE9_INSTALL=0
    IS_PE_INSTALL=1
    IS_PE8_INSTALL=1
    IS_PE9_INSTALL=0
    IS_GLASSFISH_INSTALL=0
fi

#source the appserver definitions:
if [ -r $AS8BASE/config/asenv.conf ]; then
    . $AS8BASE/config/asenv.conf
else
    #this in until we can figure out a better solution for converting asenv.bat.
    export AS_INSTALL
    AS_INSTALL="$AS8BASE"
    if [ "$FORTE_PORT" = "cygwin" ]; then
        #note - this variable is always in native format.  RT 8/25/06
        AS_INSTALL=`cygpath -m "$AS_INSTALL"`
    fi
    #note - AS_DEF_DOMAINS_PATH variable is always in native format.  RT 8/25/06
    export AS_DEF_DOMAINS_PATH
    AS_DEF_DOMAINS_PATH="$AS_INSTALL/domains"
fi

#add the appserver tools to our path:
if [ "$FORTE_PORT" = "cygwin" ]; then
    PATH="$AS8BASE/bin${PS}$PATH"
else
    PATH="$AS_INSTALL/bin${PS}$PATH"
fi

#get rid of /usr/ucb in our path, as it contains some obsolete tools on some platforms:
PATH=`optpath -rm /usr/ucb`

########
#compute JBI_PORT_OFFSET for all TEST PORTS:
########
export JBI_PORT_OFFSET
if [ "$JBI_PORT_OFFSET" = "" ]; then
    JBI_PORT_OFFSET=`bldhost -userportoffset $JBI_USER_NAME`
    if [ $? -ne 0 ]; then
        JBI_PORT_OFFSET=0
    fi
fi

#this is our current test domain:
export JBI_DOMAIN_NAME
if [ "$my_test_domain" != "" ]; then
    JBI_DOMAIN_NAME="$my_test_domain"
else
    JBI_DOMAIN_NAME=JBITest
fi

export JBI_DOMAIN_DIR
if [ "$my_domain_dir" != "" ]; then
    #user is supplying domain dir:
    JBI_DOMAIN_DIR="$my_domain_dir"
else
    #use the appserver default domain directory:
    if [ "$FORTE_PORT" = "cygwin" ]; then
        JBI_DOMAIN_DIR=$AS8BASE/domains
    else
        JBI_DOMAIN_DIR=$AS_DEF_DOMAINS_PATH
    fi
fi

export JBI_DOMAIN_ROOT
JBI_DOMAIN_ROOT=$JBI_DOMAIN_DIR/$JBI_DOMAIN_NAME

#source our domain definition file if it exists:
if [ -r "$JBI_DOMAIN_ROOT/jbi/config/jbienv.sh" ] ; then
    #note - this sets our PATH and JBI_CLASSPATH for jbi command wrappers:
    . "$JBI_DOMAIN_ROOT/jbi/config/jbienv.sh"

else
    #if the jbienv file is missing, then we are starting up this JBI domain for
    #the first time.  We therefore define the following bootstrap variables locally, for
    #scripts that are starting the jbi runtime for the first time:
    export JBI_DOMAIN_STARTED
    JBI_DOMAIN_STARTED="$JBI_DOMAIN_ROOT/jbi/tmp/.jbi_admin_running"
    export JBI_DOMAIN_STOPPED
    JBI_DOMAIN_STOPPED="$JBI_DOMAIN_ROOT/jbi/tmp/.jbi_admin_stopped"
    export JBI_DOMAIN_PROPS
    JBI_DOMAIN_PROPS="$JBI_DOMAIN_ROOT/jbi/config/jbienv.properties"

    #this needs to be the classpath separator, not the shell path separator:
    export JBI_PS
    if [ "$FORTE_PORT" = "cygwin" ]; then
        JBI_PS=";"
    else
        JBI_PS="$PS"
    fi

    export JBI_HOME
    if [ "$FORTE_PORT" = "cygwin" ]; then
        JBI_HOME="$AS8BASE/jbi"
    else
        JBI_HOME="$AS_INSTALL/jbi"
    fi
fi

#JBI_HOME is used as a unix-style var in our regress env.
#be careful not to set jvm vars based on this - use JV_JBI_HOME.  RT 10/3/06
if [ "$FORTE_PORT" = "cygwin" ]; then
    JBI_HOME=`cygpath -u "$JBI_HOME"`
fi

####
#add some local vars:
####

#######
#EE KIT?
#######
export IS_EE_INSTALL
IS_EE_INSTALL=0
[ -r $AS_INSTALL/LICENSE_EE.txt ] && IS_EE_INSTALL=1

#this is the only way to get the asadmin port number, short of using JMX:
export ASADMIN_PORT
ASADMIN_PORT=`grep 'id="admin-listener"' $JBI_DOMAIN_ROOT/config/domain.xml | head -1 | sed -e 's/^.* port="\([0-9][0-9]*\)".*/\1/'`
#this is the only way to get the HTTP listener port, short of using JMX:
export DOMAIN_HTTP_PORT
DOMAIN_HTTP_PORT=`grep 'id="http-listener-1"' $JBI_DOMAIN_ROOT/config/domain.xml | head -1 | sed -e 's/^.* port="\([0-9][0-9]*\)".*/\1/'`

export DOMAIN_HTTP_PORT2
DOMAIN_HTTP_PORT2=`grep 'id="http-listener-2"' $JBI_DOMAIN_ROOT/config/domain.xml | head -1 | sed -e 's/^.* port="\([0-9][0-9]*\)".*/\1/'`

export DOMAIN_HTTP_PORT3
DOMAIN_HTTP_PORT3=`grep 'id="http-listener-3"' $JBI_DOMAIN_ROOT/config/domain.xml | head -1 | sed -e 's/^.* port="\([0-9][0-9]*\)".*/\1/'`

#this is the only way to get the JMX port, short of using JMX:
JBI_ADMIN_PORT=`grep '<jmx-connector' $JBI_DOMAIN_ROOT/config/domain.xml | head -1 | sed -e 's/^.* port="\([0-9][0-9]*\)".*/\1/'`

# Added for jms binding regression tests
export JMS_ADMIN_PORT
JMS_ADMIN_PORT=`grep 'jms-host admin' $JBI_DOMAIN_ROOT/config/domain.xml | head -1 | sed -e 's/^.* port="\([0-9][0-9]*\)".*/\1/'`

# our test domains have the same user/password for jms
export JMS_ADMIN_USER JMS_ADMIN_PASSWORD
JMS_ADMIN_USER=admin
JMS_ADMIN_PASSWORD=admin

####
#set SVC_LOC variables.  (fundamental assumption is that this file is sourced from regress dir):
####
export SVC_LOC SVC_ROOT SVC_REGRESS SVC_BLD SVC_TEST_CLASSES SVC_CLASSES
export JV_SVC_ROOT JV_SVC_REGRESS JV_SVC_BLD JV_SVC_TEST_CLASSES JV_SVC_CLASSES

CANNONICAL_SRCROOT="`cd $SRCROOT && pwd`"
SVC_LOC=`pwd | sed -e "s|$CANNONICAL_SRCROOT[\\\/]*||" | sed -e 's|[\\\/]*regress||'`

SVC_ROOT=$SRCROOT/$SVC_LOC
SVC_REGRESS=$SVC_ROOT/regress
SVC_BLD=$SVC_ROOT/bld
if [ $OPENESB_BUILD -eq 1 ]; then
    SVC_TEST_CLASSES=$SVC_BLD/test-classes
    SVC_CLASSES=$SVC_BLD/classes
else
    SVC_TEST_CLASSES=$SVC_BLD/regress
    SVC_CLASSES=$SVC_BLD/src
fi

if [ "$FORTE_PORT" = "cygwin" ]; then
    JV_SVC_ROOT=`cygpath -m "$SVC_ROOT"`
    JV_SVC_REGRESS=`cygpath -m "$SVC_REGRESS"`
    JV_SVC_BLD=`cygpath -m "$SVC_BLD"`
    JV_SVC_TEST_CLASSES=`cygpath -m "$SVC_TEST_CLASSES"`
    JV_SVC_CLASSES=`cygpath -m "$SVC_CLASSES"`
else
    JV_SVC_ROOT=$SVC_ROOT
    JV_SVC_REGRESS=$SVC_REGRESS
    JV_SVC_BLD=$SVC_BLD
    JV_SVC_TEST_CLASSES=$SVC_TEST_CLASSES
    JV_SVC_CLASSES=$SVC_CLASSES
fi

########
#convert some cygwin vars only used by the shell.
#variables derived from the jbienv files are in JV_ (dos) format.
########

export JV_JBI_DOMAIN_ROOT JV_JBI_HOME JV_JBI_DOMAIN_DIR JV_JBISE_BASE
export JV_JBI_DOMAIN_PROPS JV_JBI_DOMAIN_STARTED JV_JBI_DOMAIN_STOPPED JV_AS8BASE

if [ "$FORTE_PORT" = "cygwin" ]; then
    #normalize the non-JV vars:
    JBI_DOMAIN_ROOT=`cygpath -u "$JBI_DOMAIN_ROOT"`
    JBI_DOMAIN_DIR=`cygpath -u "$JBI_DOMAIN_DIR"`
    JBI_DOMAIN_PROPS=`cygpath -u "$JBI_DOMAIN_PROPS"`
    JBI_DOMAIN_STARTED=`cygpath -u "$JBI_DOMAIN_STARTED"`
    JBI_DOMAIN_STOPPED=`cygpath -u "$JBI_DOMAIN_STOPPED"`

    #create the JV_ vars:
    JV_JBI_DOMAIN_ROOT=`cygpath -m "$JBI_DOMAIN_ROOT"`
    JV_JBI_HOME=`cygpath -m "$JBI_HOME"`
    JV_JBI_DOMAIN_DIR=`cygpath -m "$JBI_DOMAIN_DIR"`
    JV_JBI_DOMAIN_PROPS=`cygpath -m "$JBI_DOMAIN_PROPS"`
    JV_JBI_DOMAIN_STARTED=`cygpath -m "$JBI_DOMAIN_STARTED"`
    JV_JBI_DOMAIN_STOPPED=`cygpath -m "$JBI_DOMAIN_STOPPED"`
    JV_AS8BASE=`cygpath -m "$AS8BASE"`
    JV_JBISE_BASE=`cygpath -m "$JV_SRCROOT/install/jbise"`
else
    #non-cygwin platform:
    JV_JBI_DOMAIN_ROOT="$JBI_DOMAIN_ROOT"
    JV_JBI_HOME="$JBI_HOME"
    JV_JBI_DOMAIN_DIR="$JBI_DOMAIN_DIR"
    JV_JBI_DOMAIN_PROPS="$JBI_DOMAIN_PROPS"
    JV_JBI_DOMAIN_STARTED="$JBI_DOMAIN_STARTED"
    JV_JBI_DOMAIN_STOPPED="$JBI_DOMAIN_STOPPED"
    JV_AS8BASE="$AS8BASE"
    JV_JBISE_BASE="$JV_SRCROOT/install/jbise"
fi

#our test domains all use the same user/password:
export AS_ADMIN_USER AS_ADMIN_PASSWD ASADMIN_PW_OPTS
AS_ADMIN_USER=admin
AS_ADMIN_PASSWD=adminadmin
if [ -r "$AS8BASE/passwords" ]; then
    ASADMIN_PW_OPTS="--passwordfile $JV_AS8BASE/passwords"
else
    ASADMIN_PW_OPTS="--password $AS_ADMIN_PASSWD"
fi

#JBI_ADMIN_XML is only passed to ant.
#On cygwin, it needs to be in native format.  RT 10/3/06
export JBI_ADMIN_XML
if [ "$FORTE_PORT" = "cygwin" ]; then
    JBI_ADMIN_XML=`cygpath -m "$JBI_HOME/bin/jbi_admin.xml"`
else
    JBI_ADMIN_XML="$JBI_HOME/bin/jbi_admin.xml"
fi

#allow env. def to experiment with security parameters.  RT 10/13/06
export JBI_ANT_ARGS
if [ "$JBI_ANT_ARGS" = "" ]; then
    JBI_ANT_ARGS="-Djbi.secure=false"
fi

#base definition for JBI_ANT command - append to as required.  RT 10/3/06
JBI_ANT="asant -emacs -q -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$ASADMIN_PORT $JBI_ANT_ARGS "

######
#JBISE DEFINITIONS:
######
export JBISE_ADMIN_PORT
JBISE_ADMIN_PORT=`expr $JBI_PORT_OFFSET + 8699`

JBISE_HOME="$JBISE_BASE/jbi"
JV_JBISE_HOME="$JV_JBISE_BASE/jbi"
JBISE_JMX_URL="service:jmx:rmi:///jndi/rmi://localhost:$JBISE_ADMIN_PORT/jmxrmi"

####
#ant properties for JBISE jregress scripts:
####
JBISE_PROPS="-Djmx.provider=$JBISE_JMX_URL -Djbi.port=$JBISE_ADMIN_PORT -DJBI_HOME=$JV_JBISE_HOME -DJBI_DOMAIN_ROOT=$JV_JBISE_HOME/server"

JBISE_ANT="ant -q -f $JV_JBISE_HOME/bin/jbi_admin.xml -lib $JV_JBISE_HOME/lib/jbi-ant-tasks.jar -lib $JV_JBISE_HOME/lib/jbi-admin-common.jar $JBISE_PROPS"

start_jbise()
#Usage:  start_jbise [properties ...]
{
    java -jar -Dinstall.root="$JV_JBISE_HOME" -Dconnector.port=$JBISE_ADMIN_PORT $* "$JV_JBISE_HOME/lib/jbise_framework.jar" start
}

shutdown_jbise()
{
    java -jar -Dinstall.root="$JV_JBISE_HOME" -Dconnector.port=$JBISE_ADMIN_PORT "$JV_JBISE_HOME/lib/jbise_framework.jar" stop
}

#######
#define common test artifats:
#######
export JBI_KIT_DIR JBI_KIT_FB JBI_KIT_JB JBI_KIT_SB JBI_KIT_SB_WAR JBI_KIT_SMP JBI_KIT_SE JBI_KIT_TE JBI_KIT_WSDL

JBI_KIT_DIR=$JV_JBI_HOME/components
JBI_KIT_FB=$JBI_KIT_DIR/bindings/filebinding/installer/filebinding.jar
JBI_KIT_JB=$JBI_KIT_DIR/bindings/jmsbinding/installer/jmsbinding.jar
JBI_KIT_SB=$JBI_KIT_DIR/bindings/soapbinding/installer/soapbinding.jar
JBI_KIT_SB_WAR=$JBI_KIT_DIR/bindings/soapbinding/installer/SunJBISOAPBinding.war
JBI_KIT_SMP=$JBI_KIT_DIR/sharedlibraries/soap/soapmsgprocessor.jar
JBI_KIT_SE=$JBI_KIT_DIR/engines/sequencingengine/installer/sequencingengine.jar
#changing location of wsdlsl.jar to reflect contents of kenai
JBI_KIT_TE=$JBI_KIT_DIR/engines/transformationengine/installer/transformationengine.jar
#JBI_KIT_WSDL=$JV_JBI_HOME/sharedlibraries/wsdl/installer/wsdlsl.jar

createAgentDelay()
{
    testDelay 4
}
createClusterDelay()
{
    testDelay 4
}
createInstanceDelay()
{
    testDelay 6
}
deleteAgentDelay()
{
    testDelay 4
}
deleteClusterDelay()
{
    testDelay 4
}
deleteInstanceDelay()
{
    testDelay 4
}
deploySaDelay()
{
    testDelay 2
}
installComponentDelay()
{
    testDelay 2
}
startAgentDelay()
{
    testDelay 10
}
startComponentDelay()
{
    testDelay 4
}
startInstanceDelay()
{
    testDelay 15
}
startSaDelay()
{
    testDelay 2
}
stopAgentDelay()
{
    testDelay 10
}
stopComponentDelay()
{
    testDelay 2
}
stopInstanceDelay()
{
    testDelay 10
}
stopSaDelay()
{
    testDelay 2
}
syncInstanceDelay()
{
    testDelay 30
}
undeploySaDelay()
{
    testDelay 2
}
uninstallComponentDelay()
{
    testDelay 2
}
autoInstallDelay()
{
    testDelay 25
}
autoDeployDelay()
{
    testDelay 25
}
